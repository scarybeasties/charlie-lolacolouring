using UnityEngine;
using System.Collections;
using System.IO;

public class DemoScript : MonoBehaviour {
	
	public Texture2D texture;
	bool savedScreenshot = false;
	bool savedExistingImage = false;
	bool hideGUI = false;
	
	void Start ()
	{
		// call backs
		ScreenshotManager.ScreenshotFinishedSaving += ScreenshotSaved;	
		ScreenshotManager.ImageFinishedSaving += ImageAssetSaved;
	}
	
	void OnGUI ()
	{
		GUILayout.Label("Example scene showing: \n1. how to save a screenshot\n" +
							"2. how to save an image from your assets");
		
		// this if statement will remove the GUI buttons from
		// the screen for the screenshot
		if(!hideGUI)
		{
			// option 1 - save a screenshot
			if(GUILayout.Button ("Take Screenshot", GUILayout.Width (200), GUILayout.Height(80)))
			{	
				// the most important line - this is how you grab a screenshot!
				StartCoroutine(ScreenshotManager.Save("MyScreenshot", "ScreenshotApp", true));
				StartCoroutine(HideGUI());
			}
			
			if(savedScreenshot) GUILayout.Label ("Screenshot was successfully saved");
			
			GUILayout.Space(40);
			
			// option 2 - save an existing image asset
			GUILayout.Label(texture);
			
			if(GUILayout.Button ("Save " + texture.name, GUILayout.Width (200), GUILayout.Height(80)))
			{	
				// encode your texture to a png byte array
				byte[] bytes = texture.EncodeToPNG();
				StartCoroutine(ScreenshotManager.SaveExisting(bytes, "MyImage", true));
			}
			
			if(savedExistingImage) GUILayout.Label(texture.name + " was successfully saved");
		}
	}
	
	IEnumerator HideGUI ()
	{
		hideGUI = true;
		
		yield return new WaitForEndOfFrame();
		
		hideGUI = false;	
	}
	
	void ScreenshotSaved(string path)
	{
		Debug.Log ("screenshot finished saving to " + path);
		savedScreenshot = true;
	}
	
	void ImageAssetSaved(string path)
	{
		Debug.Log (texture.name + " finished saving to " + path);
		savedExistingImage = true;
	}
}