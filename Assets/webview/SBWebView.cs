﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SBWebViewMessage
{
    public string rawMessage;
    public string path;
    public Dictionary<string, string> args;
}

public class SBWebView : MonoBehaviour
{

    private const int PAGE_TIMEOUT = 20;

    public delegate void SBWebViewEvent(SBWebView view, SBWebViewMessage message);
    public event SBWebViewEvent OnReceivedMessage;
    public delegate void SBWebViewLoadCompleteDelegate(SBWebView view, bool success, string errorMessage);
    public event SBWebViewLoadCompleteDelegate OnLoadComplete;
    public delegate void SBWebViewClosedDelegate(SBWebView view);
    public event SBWebViewClosedDelegate OnWebViewShouldClose;

    public Vector2 WebViewSize = new Vector2(366, 117);
    public Vector2 WebViewCenter = new Vector2(0, 35);
    public Action PageLoaded;
    public Action PageClosed;
    public Action<string> JavascriptSent;
    public float ScaleFactor = 2;
    WebViewObject webViewObject;
    private bool _showingPage;
    private bool _setup;
    private bool _loaded;
    public static bool shouldSelfManange;

    public void Setup()
    {
        webViewObject = (new GameObject("WebViewObject")).AddComponent<WebViewObject>();
        webViewObject.ClearCache();

        webViewObject.transform.SetParent(transform);
        //        ReInit();

        webViewObject.SetVisibility(true);

    }

    public void ShouldSelfManage()
    {
        shouldSelfManange = true;
    }

    public void ReInit()
    {
        if (_setup && IsOnline())
        {
            return;
        }

        _setup = true;
        webViewObject.Init(
            cb: (msg) =>
            {
                Debug.Log(msg);
                if (msg == "close")
                {

                    Close();
                    //                    Close();
                }
                else if (msg.Contains("exitlink"))
                {
                    //                    ShowExternalLink(msg);

                    //ShowExternalLink(msg);
                    string urlQuery = msg.Replace("exitlink?", "");
                    Dictionary<string, string> parms = DecodeQueryParameters(urlQuery);

                    SBWebViewMessage message = new SBWebViewMessage();
                    message.rawMessage = msg;
                    message.path = "exitlink";
                    message.args = parms;

                    if (OnReceivedMessage != null)
                    {
                        OnReceivedMessage(this, message);
                    }
                }
                else if (msg.Contains("load"))
                {

                    Loaded();

                    //                    Loaded();
                }
                //                Debug.Log(string.Format(transform.parent.name +  " CallFromJS[{0}]", msg));
            }, transparent: true,
            err: (msg) =>
            {
                //                Debug.Log(string.Format(transform.parent.name +  " CallOnError[{0}]", msg));
                //                Close();

                Debug.Log(msg);

                Close();

            },
            ld: (msg) =>
            {
                //                Debug.Log(string.Format(transform.name +  " CallOnLoad[{0}]", msg));
                if (msg == "about:blank")
                {
                    //                    Close();

                    Close();

                }
                else
                {
                    //                    Loaded();

                    Loaded();

                }

            },
            enableWKWebView: Application.isEditor);
    }

    IEnumerator LoadPageTimeout()
    {
        yield return new WaitForSeconds(PAGE_TIMEOUT);
        Close();
    }

    public void KillCallbacks()
    {
        webViewObject.Init(null, true, "", null, null);
    }

    //called by the software backbutton
    public void ExternalClose()
    {
        if (!SBWebView.shouldSelfManange)
        {
            PageClosed();
        }
        else
        {
            Close();
        }
    }

    public void Close()
    {
        Debug.Log("Closed");
        HidePage();
        ToggleVisibility(false);
        if (PageClosed != null)
        {
            PageClosed();
        }

        if (OnWebViewShouldClose != null)
        {
            OnWebViewShouldClose(this);
        }
    }

    public void Loaded()
    {
        if (_loaded)
        { return; }

        _loaded = true;

        if (PageLoaded != null)
        {
            PageLoaded();
        }

        Show();

        if (OnLoadComplete != null)
        {
            OnLoadComplete(this, true, "");
        }

        StopCoroutine("LoadPageTimeout");
    }

    public bool ShowingPage()
    {
        return _showingPage;
    }

    public void LoadBanner(string URL, bool forceSize = false)
    {
        ReInit();
        webViewObject.scaleFactor = ScaleFactor;
        webViewObject.SetSize(WebViewSize, WebViewCenter);
        if (forceSize)
        {
            webViewObject.SetSize(WebViewSize, WebViewCenter);
        }

        if (IsOnline())
        {
            LoadPageWithURL(URL);
        }
        else
        {
            LoadPageWithURL(URL, true);
        }

        //        ReInit();
    }

    private bool IsOnline()
    {

        NetworkReachability e = Application.internetReachability;
        if (e != NetworkReachability.NotReachable)
        {
            return true;
        }

        return false;
    }

    private void QueueFullSize()
    {
        ReInit();
        webViewObject.SetMargins(0, 0, 0, 0);
    }

    public void LoadPage(string URL)
    {
        QueueFullSize();
        if (IsOnline())
        {
            LoadPageWithURL(URL);
        }
        else
        {
            LoadPageWithURL(URL, true);
        }
    }

    //private int GetPlatformAsInt()
    //{
    //    int id = -1;
    //    switch (GameService.BuildSettings.platform)
    //    {
    //        case SB_BuildPlatforms.iOS:
    //            id = 0;
    //            break;
    //        case SB_BuildPlatforms.GooglePlay:
    //            id = 1;
    //            break;
    //        case SB_BuildPlatforms.Amazon:
    //            id = 2;
    //            break;
    //    }
    //    return id;
    //}

    private void LoadPageWithURL(string page, bool offline = false)
    {
        StopCoroutine("LoadPageTimeout");
        StartCoroutine("LoadPageTimeout");

        if (!offline)
        {
            page += "&wv=1";
        }

        _loaded = false;
        //        ShowPage();

        //        Debug.Log(page);


#if !UNITY_WEBPLAYER

        if (!offline)
        {
            Debug.Log(page);
            webViewObject.LoadURL(page);
        }
        else
        {
            Debug.Log("---- loading offline");
            StartCoroutine(LoadOfflinePage(page));
        }
#if !UNITY_ANDROID

        webViewObject.EvaluateJS(
            "window.addEventListener('load', function() {" +
            "   window.Unity = {" +
            "       call:function(msg) {" +
            "           var iframe = document.createElement('IFRAME');" +
            "           iframe.setAttribute('src', 'unity:' + msg);" +
            "           document.documentElement.appendChild(iframe);" +
            "           iframe.parentNode.removeChild(iframe);" +
            "           iframe = null;" +
            "       }" +
            "   }" +
            "}, false);");



#endif
#else

        webViewObject.EvaluateJS(
        "parent.$(function() {" +
        "   window.Unity = {" +
        "       call:function(msg) {" +
        "           parent.unityWebView.sendMessage('WebViewObject', msg)" +
        "       }" +
        "   };" +
        "});");
#endif
    }

    IEnumerator LoadOfflinePage(string page)
    {
        string dest = System.IO.Path.Combine(Application.persistentDataPath, page);
        string webviewPath = System.IO.Path.Combine(Application.persistentDataPath, "WebViews");
        if (!System.IO.Directory.Exists(webviewPath))
        {
            //The manifest file is created during the build process, and placed in the resources folder. This is a simple text file with the path to each asset in the web views folder.
            //This is needed as you cant look into the streaming assets folder by code, you need to know whats in it.
            string result = Resources.Load<TextAsset>("Manifest").text;

            char[] chrArr = new char[Environment.NewLine.Length];
            int n = 0;
            foreach (char chr in Environment.NewLine)
            {
                chrArr[n] = chr;
                n++;
            }

            string[] files = result.Split(chrArr);
            foreach (string path in files)
            {
                if (path == "")
                {
                    continue;
                }

                //string p = path.Remove(0,1); // OLD CODE
                string p = path.Substring(1, path.Length - 1); // NEW CODE - there's a newline character at the end of the path, so we need to get a substring from index 1 to length-2

                string src = System.IO.Path.Combine(Application.streamingAssetsPath, p);
                string dst = System.IO.Path.Combine(Application.persistentDataPath, p);

                byte[] r = null;
                if (src.Contains("://"))
                {  // for Android
                    var www = new WWW(src);
                    yield return www;
                    r = www.bytes;
                }
                else
                {
                    r = System.IO.File.ReadAllBytes(src);
                }

                string p2 = Application.persistentDataPath + path;
                p2 = p2.Remove(p2.LastIndexOf("/"), p2.Length - p2.LastIndexOf("/"));
                System.IO.Directory.CreateDirectory(p2);
                System.IO.File.WriteAllBytes(dst, r);

            }
        }
        webViewObject.LoadURL("file://" + dest.Replace(" ", "%20"));
    }

    public void ToggleVisibility(bool visible)
    {
        webViewObject.SetVisibility(visible);
    }

    public void HidePage()
    {
        //      ToggleVisibility(false);
        _showingPage = false;
    }

    public void Show()
    {
        ToggleVisibility(true);
        _showingPage = true;
    }


    public Dictionary<string, string> DecodeQueryParameters(string uri)
    {
        Dictionary<string, string> dict = new Dictionary<string, string>();
        foreach (string item in uri.Split('&'))
        {
            string[] parts = item.Replace("?", "").Split('=');
            dict.Add(parts[0], parts[1]);
        }

        return dict;
    }

    void OnDestroy()
    {
        webViewObject.SetVisibility(false);
        Destroy(webViewObject.gameObject);
        webViewObject = null;
    }

}