﻿Shader "Masking/Alpha Test Depth Mask" 
{ 
	Properties 
	{ 
	    _MainTex ("Texture", 2D) = ""
	    _Cutoff ( "Alpha Cutoff", Float) = 0.5
	}
	 
	Category 
	{
	    Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	    ColorMask 0
	    
		SubShader 
	    {
		    Pass 
		    {
		        AlphaTest Less 0.5
		        SetTexture[_MainTex]
		        {
		        	ConstantColor [_Color]
		        }
		    }
		}
		    
	    SubShader 
	    {
	    	Pass {
			    Cull Front
			    
			    AlphaTest Greater [_Cutoff]

				GLSLPROGRAM
			    
			    uniform sampler2D _MainTex;
			    uniform float _Cutoff;
			    
			    varying vec4 textureCoordinates;
			 
			    #ifdef VERTEX
			    void main() {
			        gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
			        textureCoordinates = gl_MultiTexCoord0.xy;
			    }
			   	#endif
			 
			    #ifdef FRAGMENT
			    uniform lowp sampler2D _MainTex;
			    void main() {
			        
			      	gl_FragColor = texture2D(_MainTex, vec2(textureCoordinates));
			       
			    }
			    #endif      
			    
			    ENDGLSL
		    }
	    }
	}
}