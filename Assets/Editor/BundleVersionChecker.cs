﻿using UnityEngine;
using UnityEditor;
using System.IO;

/*
 * Get app bundle version
 * Source: http://stackoverflow.com/questions/17208261/get-app-bundle-version-in-unity3d
 */


/**
 * This class checks the bundle version from the Unity build settings, 
 * and writes out the current bundle version value to a .cs file.
 * 
 * The .cs file can then be used by the app at runtime, whenever we want to view the current bundle version.
 * (We can't check the buildSettings bundle version at runtime, as it's only available to the IDE)
 */ 
[InitializeOnLoad]
public class BundleVersionChecker
{
	/// <summary>
	/// Class name to use when referencing from code.
	/// </summary>
	const string ClassName = "CurrentBundleVersion";
	
	const string TargetCodeFile = "Assets/Config/" + ClassName + ".cs";
	
	static BundleVersionChecker () {
		#if UNITY_IOS
		string bundleVersion = PlayerSettings.shortBundleVersion;
		#else
		string bundleVersion = PlayerSettings.bundleVersion;
		#endif

		string lastVersion = CurrentBundleVersion.version;

		BundleVersionVO bundleVersionVO = new BundleVersionVO(bundleVersion);
		BundleVersionVO lastVersionVO = new BundleVersionVO(lastVersion);

		Debug.Log("bundleVersion: " + bundleVersion + ", bundleVersionVO: " + bundleVersionVO.ToString());
		Debug.Log("lastVersion: " + lastVersion + ", lastVersionVO: " + lastVersionVO.ToString());
		Debug.Log("bundleVersionVO.IsGreaterThan(lastVersionVO): " + bundleVersionVO.IsGreaterThan(lastVersionVO));

		if (lastVersion != bundleVersion) 
		{
			Debug.Log ("Found new bundle version " + bundleVersion + " replacing code from previous version " + lastVersion +" in file \"" + TargetCodeFile + "\"");
			CreateNewBuildVersionClassFile (bundleVersion);
		}
	}
	
	static string CreateNewBuildVersionClassFile (string bundleVersion) 
	{
		using (StreamWriter writer = new StreamWriter (TargetCodeFile, false)) 
		{
			
			string code = GenerateCode (bundleVersion);
			writer.WriteLine ("{0}", code);
			writer.Close();

			// GT - commenting this out, as it uses try/catch, which will cause the app to crash on some android devices
			/*
			try 
			{
				string code = GenerateCode (bundleVersion);
				writer.WriteLine ("{0}", code);

			} catch (System.Exception ex) {
				string msg = " threw:\n" + ex.ToString ();
				Debug.LogError (msg);
				EditorUtility.DisplayDialog ("Error when trying to regenerate class", msg, "OK");
			}
			*/
		}
		return TargetCodeFile;
	}
	
	/// <summary>
	/// Regenerates (and replaces) the code for ClassName with new bundle version id.
	/// </summary>
	/// <returns>
	/// Code to write to file.
	/// </returns>
	/// <param name='bundleVersion'>
	/// New bundle version.
	/// </param>
	static string GenerateCode (string bundleVersion) {
		string code = "public static class " + ClassName + "\n{\n";
		code += System.String.Format ("\tpublic static readonly string version = \"{0}\";", bundleVersion);
		code += "\n}\n";
		return code;
	}
}