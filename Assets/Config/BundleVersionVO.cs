﻿
public class BundleVersionVO
{
	public int major = 1;
	public int minor = 0;
	public int revision = 0;
	public string version = "";
	public string date = "";
	
	public BundleVersionVO(string versionStr, string dateStr="")
	{
		string[] versionStringArr = versionStr.Split(new char[] { '.' });
		
		major = int.Parse(versionStringArr[0]);
		if(versionStringArr.Length > 1) minor = int.Parse(versionStringArr[1]);
		if(versionStringArr.Length > 2) revision = int.Parse(versionStringArr[2]);

		version = versionStr;
		date = dateStr;
	}
	
	public bool IsGreaterThan(BundleVersionVO otherVersion)
	{
		if(major > otherVersion.major)
		{
			return true;
		}
		else if(major == otherVersion.major)
		{
			if(minor > otherVersion.minor)
			{
				return true;
			}
			else if(minor == otherVersion.minor)
			{
				if(revision > otherVersion.revision)
				{
					return true;
				}
			}
		}
		
		
		return false;
	}
	
	override public string ToString()
	{
		string str = "major: " + major + ", minor: " + minor + ", revision: " + revision + ", date: " + date;
		return str;
	}
}