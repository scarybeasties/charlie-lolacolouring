
/**
 * Common javascript functions for the promo area.
 * File is using 'jsx' extension to avoid problems 
 * with Unity thinking that it's a class file 
 */

var _itemClickthroughs = [];

var platform = (getURLParameter("pl") == null ? null : parseInt(getURLParameter("pl")));
var locale = (getURLParameter("loc") == null ? null : parseInt(getURLParameter("loc")));

function documentLoaded()
{
	InitBanner();
}

// Returns the value of a url param.  Null is returned if param does not exist
function getURLParameter(name)
{
	return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}

// Send the escaped clickthrough URL to the UniWebView plugin
function OnClickItem(n)
{
	if( platform == null )
		return;
	
	if(window.ga)
	{
		if(window.console) console.log('Webview | ParentsAreaPromo link:' + _itemClickthroughs[n][platform]);
		ga('send', 'event', 'Webview | ParentsAreaPromo', 'OnClickItem', 'link:' + _itemClickthroughs[n][platform]);
	}
	
	if(_itemClickthroughs.length > 0)
	{
		document.location = "uniwebview://exitlink?url=" + _itemClickthroughs[n][platform];
	}
}




var scope = this;
var _currentBanner = Math.floor(Math.random() * _itemClickthroughs.length);
function InitBanner()
{
	console.log("InitBanner _currentBanner: " + _currentBanner);
	document.getElementById("banner").className = "item item" + _currentBanner;

	// Stop here, if there's no clickthrough urls
	if(_itemClickthroughs.length == 0) return;
	
	setTimeout(function()
			{
				scope.BannerFinished();
			}, 5000);
}

function BannerFinished()
{
	_currentBanner++;
	if(_currentBanner > _itemClickthroughs.length-1) _currentBanner = 0;

	InitBanner();
}

function BannerClicked()
{
	console.log("BannerClicked " + _currentBanner);
	OnClickItem(_currentBanner);
}