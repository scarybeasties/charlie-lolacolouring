
/**
 * Common javascript functions for the parents area.
 * File is using 'jsx' extension to avoid problems 
 * with Unity thinking that it's a class file 
 */
 
var legalFooter = "Charlie and Lola Colouring © 2017 BBC Worldwide Limited. Developed by Scary Beasties Limited.<br/>Published by BBC Worldwide Limited. Charlie and Lola ™ and © Lauren Child 2005. Charlie and Lola is produced by Tiger Aspect Productions.<br/>For more information go to <a href='javascript:OpenExternalLink(\"http://www.charlieandlola.com\")'>www.charlieandlola.com</a>. BBC and Cbeebies are trade marks of the British Broadcasting Corporation<br/>and are used under licence. Cbeebies logo © BBC 2006. BBC logo © BBC 1996.";


var platform = 0;
var locale = 0;

var _itemClickthroughs = [];

function documentLoaded()
{
}

// Validate platform
switch(parseInt(getURLParameter("pl")))
{
	case 0: // ios
	case 1: // android (google play)
	case 2: // android (amazon)
		platform = parseInt(getURLParameter("pl"));
		break;
		
	default:
		platform=0;
		break;
}

// Validate locale
switch(parseInt(getURLParameter("loc")))
{
	case 0: // en
	case 1: // en (add other locale here..)
		locale = parseInt(getURLParameter("loc"));
		break;
		
	default:
		locale=0;
		break;
}

// TEMP!! Output vars
//legalFooter += " [pl=" + platform + ", loc=" + locale + "]";

// Returns the value of a url param.  Null is returned if param does not exist
function getURLParameter(name)
{
	return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}

// Let the webview know that the webview should be closed
function backButtonPressed()
{
	Unity.call('close');
}

// Passes a string to UniWebView, to open it via the Unity app
function OpenExternalLink(str)
{
	if(str.length > 0)
	{
		if( platform == null )
			document.location = str;
		else
			document.location = "unity:exitlink?url=" + escape(str);
	}
}

// Send the escaped clickthrough URL to the UniWebView plugin
function OnClickItem(n)
{
	if( platform == null )
		return;
		
	if(_itemClickthroughs.length > 0)
	{
		document.location = "unity:exitlink?url=" + _itemClickthroughs[n][platform];
	}
}