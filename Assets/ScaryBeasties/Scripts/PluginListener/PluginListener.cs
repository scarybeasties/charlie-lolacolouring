﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Utils;
using ScaryBeasties.Cam;

namespace ScaryBeasties.PluginListener
{
	public class PluginListener : MonoBehaviour 
	{
		private static SB_TextField _debug;

		void Awake()
		{
			DontDestroyOnLoad(transform.gameObject);
		}

		void Start()
		{
			//_debug = GameObject.Find("debug").GetComponent<SB_TextField>();
			//_debug.text = "PluginListener started";
		}

		/**
		 * Handle incoming strings from SB_iOS_Utils.m
		 */
		public void HandleIncoming(string str)
		{
			//_debug.text = _debug.text+"\nHandleIncoming: " + str;

			#if USES_CAMERA
			if(str == "camera_allowed")
			{
				SB_Camera.CAMERA_ALLOWED = true;
			}
			else if(str == "camera_denied")
			{
				SB_Camera.CAMERA_ALLOWED = false;
			}
			#endif
		}
	}
}
