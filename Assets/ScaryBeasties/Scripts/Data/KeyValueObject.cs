﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * A class to store objects under a key string lookup, using the Dictionary object.
 * 
 * c# does not support dynamic ExpandoObjects, so this is a work-around..
 */ 
namespace ScaryBeasties.Data
{
	//public class KeyValueObject : Object
	public class KeyValueObject
	{
		private Dictionary<string, object> _dictionary = new Dictionary<string, object>();

		/** Store an object under a given key */
		public void SetKeyValue(string key, object value) 
		{
			if(ContainsKey(key))
			{
				_dictionary.Remove(key);
			}

			_dictionary.Add(key, value);
		}

		/** 
		 * Returns an object for a given key. 
		 * A null object is returned if there's no object for that key.
		 */
		public object GetValue(string key)
		{
			//object obj = null;

			if(ContainsKey(key))
			{
				return _dictionary[key];
			}
			else
			{
				// Key was not found in the _dictionary
			}

			return null;
		}
		
		public int Count
		{
			get { return _dictionary.Count; }
		}

		public bool ContainsKey(string key)
		{
			return _dictionary.ContainsKey(key);
		}
		
		public ArrayList Keys
		{
			get
			{
				ArrayList keys = new ArrayList();
				foreach(KeyValuePair<string, object> key in _dictionary)
				{
					keys.Add(key.Key);
				}
				
				return keys;
			}
		}
		
		public void RemoveKey(string key)
		{
			_dictionary.Remove(key);
		}
		
		public void Clear()
		{
			_dictionary.Clear();
		}

		public string ToString()
		{
			string str = "";

			foreach(KeyValuePair<string, object> key in _dictionary)
			{
				str += key.Key + ":" + key.Value + ", ";
			}
			
			return str;
		}

		
		public string ToXML()
		{
			string str = "\n\t\t\t\t<kvobject>";

			foreach(KeyValuePair<string, object> key in _dictionary)
			{
				str += "\n\t\t\t\t\t<data key='" + key.Key + "' value='" + key.Value + "' />";
			}

			str += "\n\t\t\t\t</kvobject>";

			return str;
		}
	}
}