﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System;
using ScaryBeasties.Utils;

namespace ScaryBeasties.Data
{
	public class SB_XML_Parser
	{
		private XmlDocument _xml = null;

		/**
		 * Constructor
		 */
		public SB_XML_Parser(string xmlString=null)
		{
			if(xmlString != null)
			{
				_xml= new XmlDocument();
				_xml.LoadXml(xmlString);
			}
		}

		public XmlDocument xml 
		{
			get
			{
				if(_xml == null)
				{
					string filename = Application.dataPath + SB_Globals.CONFIG_XML_PATH;
					string configXMLString = System.IO.File.ReadAllText(filename, System.Text.Encoding.UTF8);

					_xml= new XmlDocument();
					_xml.LoadXml(configXMLString);
				}

				return _xml;
			}
		}

		/**
		 * Returns a list of XmlNode objects, for a given nodePath
		 * 
		 * @string nodePath The xml node names to traverse, separated by a '/'
		 * example: 'config/links'
		 */
		public XmlNodeList GetNodeList(string nodePath)
		{
			return xml.SelectNodes(nodePath);
		}

		public string GetNodeAttribute(XmlNode node, string attributeName)
		{
			return node.Attributes.GetNamedItem(attributeName).Value;
		}
		
		public string GetNodeValue(XmlNode node, string attributeName)
		{
			return node.InnerText;
		}

		public XmlNode GetNode(string nodePath)
		{
			return xml.SelectNodes(nodePath)[0];
		}

		public XmlNode GetNode(string nodePath, string attributeName, string attributeValue)
		{
			XmlNodeList nodeList = xml.SelectNodes(nodePath);
			foreach(XmlElement node in nodeList)
			{
				if(node.GetAttribute(attributeName) == attributeValue)
				{
					return node;
				}
			}

			return null;
		}
	}
}