using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace ScaryBeasties.Appstore
{
	public class AppstoreHandler
	{
		#if UNITY_IOS
		[DllImport ("__Internal")] private static extern void _OpenAppInStore(int appID, string campaignToken, string affiliateToken, string providerToken);
		#endif


		private static AppstoreHandler _instance = null;
		public static AppstoreHandler instance
		{
			get 
			{
				if(_instance == null)
				{
					_instance = new AppstoreHandler();
				}
				
				return _instance;
			}
		}

		void Awake()
		{
			if(!Application.isEditor)
			{

			}
			else
			{
				Debug.Log("AppstoreHandler:: Cannot open Appstore in Editor.");
			}
		}

		public void openAppInStore(AppstoreGameDataVO appstoreDataVO)
		{
			if(!Application.isEditor)
			{
				#if UNITY_IOS
				_OpenAppInStore(appstoreDataVO.appID, appstoreDataVO.campaignToken, appstoreDataVO.affiliateToken, appstoreDataVO.providerToken);
				#endif

				#if UNITY_ANDROID
				//Application.OpenURL(appstoreDataVO.appID);
				#endif
			}
			else
			{
				Debug.Log("AppstoreHandler:: Cannot open Appstore in Editor.");
			}
		}

		public void appstoreClosed()
		{
			Debug.Log("AppstoreHandler:: Appstore closed.");
		}
	}	
}