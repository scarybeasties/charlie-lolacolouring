﻿/**
 * AppstoreGameDataVO
 * 
 * A value object, used to store
 * the details for appstore games
 * 
 */

namespace ScaryBeasties.Appstore
{
	public class AppstoreGameDataVO
	{
		private int _appID;
		private string _campaignToken = "";
		private string _affiliateToken = "";
		private string _providerToken = "";
		
		public AppstoreGameDataVO(int appID, string campaignToken="", string affiliateToken="", string providerToken="")
		{
			_appID = appID;
			_campaignToken = campaignToken;
			_affiliateToken = affiliateToken;
			_providerToken = providerToken;
		}
		
		// Getters
		public int appID{		get {return _appID;}		}
		public string campaignToken{	get {return _campaignToken;}	}
		public string affiliateToken{	get {return _affiliateToken;}	}
		public string providerToken{	get {return _providerToken;}	}
		
		override public string ToString()
		{
			var str = "appID: " + appID.ToString() + ", " + 
					"campaignToken: " + campaignToken + ", " + 
					"affiliateToken: " + affiliateToken + ", " + 
					"providerToken: " + providerToken;

			return str;
		}
	}
}