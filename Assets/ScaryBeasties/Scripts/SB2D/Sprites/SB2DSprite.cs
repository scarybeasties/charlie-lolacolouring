﻿using UnityEngine;
using System.Collections;
using System;

namespace ScaryBeasties.SB2D
{
    public class SB2DSprite : MonoBehaviour
    {
        protected Texture2D _texture;
        protected Sprite _sprite;
        protected SpriteRenderer _spriteRenderer;

        // An object which stores bespoke data relating to this sprite, which might be useful
        protected UnityEngine.Object _data;
        protected string _assetName = "";
        protected string _assetFileName = "";
        protected bool _loadedSuccessfully = false;

        // Image loader
        protected SB2DImageLoader _imageLoader;
        protected Action<SB2DSprite> _callback;




        // Initialise the new SB2DSprite
        public virtual void Init(UnityEngine.Object data, Action<SB2DSprite> callback = null)
        {
            _data = data;
            _callback = callback;

            _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        }

        // Getters
        public Texture2D Texture { get { return _texture; } }
        public virtual Vector2 Size { get { return new Vector2(_texture.width, _texture.height); } }
        public string AssetName { get { return _assetName; } set { _assetName = value; } }
        public string AssetFileName { get { return _assetFileName; } }
        public UnityEngine.Object Data { get { return _data; } }
        public bool LoadedSuccessfully { get { return _loadedSuccessfully; } }

        public string GetFileName()
        {
            int idx = AssetFileName.LastIndexOf("/");
            return AssetFileName.Substring(idx + 1);
        }

        // Load an image from remote server
        public void LoadImage(string filePath)
        {
            DoLoadImage(filePath, false);
        }

        // Load an image from remote server
        public void LoadReadableImage(string filePath)
        {
            DoLoadReadableImage(filePath, false);
        }

        private void DoLoadReadableImage(string filePath, bool loadFromLocalFileSystem)
        {
            _assetFileName = filePath;

            //Debug.LogWarning ("[SB2DSprite] LoadImage: "  +filePath);
            CreateImageLoader();
            _imageLoader.LoadReadableImage(filePath, OnImageLoaderCallback, loadFromLocalFileSystem);
        }

        // Load an image from the local file system, prepends 'file://' to the filepath
        public void LoadImageFromDisk(string filePath)
        {
            DoLoadImage(filePath, true);
        }

        public void LoadReadableImageFromDisk(string filePath)
        {
            DoLoadReadableImage(filePath, true);
        }

        private void DoLoadImage(string filePath, bool loadFromLocalFileSystem)
        {
            _assetFileName = filePath;

            //Debug.LogWarning ("[SB2DSprite] LoadImage: "  +filePath);
            CreateImageLoader();
            _imageLoader.LoadImage(filePath, OnImageLoaderCallback, loadFromLocalFileSystem);
        }

        public void LoadBytes(byte[] bytes)
        {
            CreateImageLoader();
            _imageLoader.LoadBytes(bytes, OnImageLoaderCallback);
        }

        // Handle the success/fail of the image loader
        private void OnImageLoaderCallback(bool success)
        {
            _loadedSuccessfully = success;

            if (success)
            {
                MakeTexture();
            }
            else
            {
                Error("Error loading image: " + _assetFileName);
            }

            //DestroyImageLoader();

            if (_callback != null)
            {
                _callback(this);
            }
        }

        // Add texture to sprite renderer
        protected virtual void MakeTexture()
        {
            _texture = _imageLoader.Texture;

            //------------------------------------------------
            // GT - none of these resizing methods look nice.....
            //_texture = ResizeTextureUtil.ResizeTexture(_texture, ImageFilterMode.Average,_scaler);
            //_texture = ResizeTextureUtil.ResizeTexture(_texture, ImageFilterMode.Biliner,_scaler);
            //_texture = ResizeTextureUtil.ResizeTexture(_texture, ImageFilterMode.Nearest,_scaler);
            //------------------------------------------------

            //int wScaled = Mathf.RoundToInt(_texture.width * _scaler);
            //int hScaled = Mathf.RoundToInt(_texture.height * _scaler);
            //_sprite = Sprite.Create(_texture, new Rect(0f, 0f, wScaled, hScaled), new Vector2(0.5f, 0.5f),1f);

            if (_spriteRenderer != null)
            {
                _sprite = Sprite.Create(_texture, new Rect(0f, 0f, _texture.width, _texture.height), new Vector2(0.5f, 0.5f), 1f, 0, SpriteMeshType.FullRect);
                _spriteRenderer.sprite = _sprite;

            }
        }

        private void CreateImageLoader()
        {
            if (_imageLoader == null)
                _imageLoader = (SB2DImageLoader)gameObject.AddComponent<SB2D.SB2DImageLoader>();
        }

        private void DestroyImageLoader()
        {
            if (_imageLoader != null)
            {
                //_imageLoader.Dispose();
                Destroy(_imageLoader);
            }
            _imageLoader = null;
        }

        protected void Error(string msg)
        {
            Debug.LogError("[SB2DSprite]" + msg);
        }

        protected virtual void OnDestroy()
        {
            if (_texture != null) _texture = null;
            if (_sprite != null) _sprite = null;
            if (_spriteRenderer != null) _spriteRenderer = null;
            _data = null;
            _callback = null;

            DestroyImageLoader();
        }
    }
}