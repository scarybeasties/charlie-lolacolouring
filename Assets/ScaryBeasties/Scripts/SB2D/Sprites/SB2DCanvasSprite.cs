﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

namespace ScaryBeasties.SB2D
{
	public class SB2DCanvasSprite : SB2DSprite
	{
		RectTransform _rect;
		Image _image;

		public Action<SB2DCanvasSprite> OnCanvasSpriteCreated;
		
		public float Width
		{
			set {_rect.sizeDelta = new Vector2(value, Height); }
			get { return Math.Abs(_rect.rect.width*Scale.x); }
		}
		
		public float Height
		{
			set {_rect.sizeDelta = new Vector2(Width, value); }
			get { return Math.Abs(_rect.rect.height*Scale.y); }
		}
		
		public Vector2 Position
		{
			set { _rect.localPosition = value; }
			get { return _rect.localPosition; }
		}
		
		public float Rotation
		{
			set {
					Vector3 rot = _rect.eulerAngles;
					rot.z = value;
					_rect.eulerAngles = rot;
				}
			get { return _rect.eulerAngles.z; }
		}
		
		public Vector2 Scale
		{
			set { _rect.localScale = value; }
			get { return _rect.localScale; }
		}

		public RectTransform RectTransform
		{
			get { return _rect; }
		}
		/*
		public Rect Rect
		{
			get 
			{
				//return new Bounds(_rect.rect.center, _rect.rect.size);
				return _rect.rect;
			}
		}
		*/
		public Bounds Bounds
		{
			get 
			{
				return new Bounds(_rect.rect.center, _rect.rect.size);
			}
		}
		
		override public Vector2 Size		{	get{return new Vector2(Width, Height); }}
		
		override public void Init(UnityEngine.Object data, Action<SB2DSprite> callback=null)
		{
			_data = data;
			_callback = callback;
			
			_rect = gameObject.GetComponent<RectTransform> ();
			_image = gameObject.GetComponent<Image> ();
		}

		public Rect GetRect(float scale=1.0f)
		{
			float w = Width*scale;
			float h = Height*scale;
			float left = RectTransform.position.x - (w*0.5f);
			float top = RectTransform.position.y - (h*0.5f);

			return new Rect(left, top, w, h);
		}
		
		public bool HitTestPoint(Vector2 point, float scale=1.0f)
		{



			float w = Math.Abs(Width * scale);
			float h = Math.Abs(Height * scale);
			float left = RectTransform.position.x - (w*0.5f);
			float top = RectTransform.position.y - (h*0.5f);
			Rect rect = new Rect(left, top, w, h);

			//Debug.LogWarning("HitTestPoint rect: " + rect);
			//Debug.LogWarning("HitTestPoint point: " + point);
			//Bounds bounds = new Bounds(transform.position, size);
			
			if(point.x >= rect.xMin && point.x <= rect.xMax)
			{
				if(point.y >= rect.yMin && point.y <= rect.yMax)
				{
					return true;
				}
			}

			return false;


		}

		public ArrayList GetTransformedCorners(){

			ArrayList points = new ArrayList ();
			/*points.Add ( this.transform.TransformPoint (new Vector2 (0 - (_texture.width / 2.0f), 0 - (_texture.height / 2.0f))));
			points.Add ( this.transform.TransformPoint (new Vector2 (0 + (_texture.width / 2.0f), 0 - (_texture.height / 2.0f))));
			points.Add ( this.transform.TransformPoint (new Vector2 (0 - (_texture.width / 2.0f), 0 + (_texture.height / 2.0f))));
			points.Add ( this.transform.TransformPoint (new Vector2 (0 + (_texture.width / 2.0f), 0 + (_texture.height / 2.0f))));
			*/
			float tscale = 1.0f;
			if (tk2dSystem.CurrentPlatform == "1x") {
				tscale = 2.0f;
			}

			points.Add ( this.transform.TransformPoint (new Vector2 (0 - (_texture.width / 2.0f)*tscale, 0 - (_texture.height / 2.0f)*tscale)));
			points.Add ( this.transform.TransformPoint (new Vector2 (0 + (_texture.width / 2.0f)*tscale, 0 - (_texture.height / 2.0f)*tscale)));
			points.Add ( this.transform.TransformPoint (new Vector2 (0 - (_texture.width / 2.0f)*tscale, 0 + (_texture.height / 2.0f)*tscale)));
			points.Add ( this.transform.TransformPoint (new Vector2 (0 + (_texture.width / 2.0f)*tscale, 0 + (_texture.height / 2.0f)*tscale)));

			return points;

		}

		public bool PixelHitTestPoint(Vector2 point, float scale=1.0f)
		{	
			/*
			float w = Math.Abs(Width * scale);
			float h = Math.Abs(Height * scale);
			float left = RectTransform.position.x - (w*0.5f);
			float top = RectTransform.position.y - (h*0.5f);
			Rect rect = new Rect(left, top, w, h);
			*/
			/*
			float tscale = 1.0f;
			if (tk2dSystem.CurrentPlatform == "1x") {
				tscale = 0.5f;
			}
			point = new Vector2 (point.x * tscale, point.y * tscale);*/

			Vector2 vec = this.transform.InverseTransformPoint (point);

			if (tk2dSystem.CurrentPlatform == "1x") {
				vec = new Vector2(vec.x/2,vec.y/2);
			}

			vec = new Vector2 (vec.x+ (_texture.width / 2.0f), vec.y + (_texture.height / 2.0f));
			//Debug.LogError("VEC "+vec+" width "+_texture.width+" height "+_texture.height);
			//return false;

			//Debug.LogWarning("HitTestPoint point: " + point);


			int px = (int)vec.x;
			int py = (int)vec.y;

			if (px < 0)
				return false;
			if (py < 0)
				return false;
			if (px >= _texture.width)
				return false;
			if (py >= _texture.height)
				return false;

			//return true;
			Color tcol = _texture.GetPixel (px, py);
			if (tcol.a > 0)
				return true;
		

			return false;
			
			
		}
		
		// Add texture to sprite renderer
		override protected void MakeTexture()
		{
			base.MakeTexture();
			
			float texWidth = _texture.width;
			float texHeight = _texture.height;
			
			if(_image != null)
			{
				_sprite = Sprite.Create(_texture, new Rect(0f, 0f, texWidth, texHeight), new Vector2(0.5f, 0.5f),1f);
				_image.sprite = _sprite;
			}
			
			// Scale up '1x' sprites
			if(tk2dSystem.CurrentPlatform == "1x")
			{
				texWidth *= 2f;
				texHeight *= 2f;
			}
			_rect.sizeDelta = new Vector2(texWidth, texHeight);

			// When new sprites are first created, they have a localScale of zero, so we need to set it to 1
			Vector2 rescaled = transform.localScale;
			if(rescaled == Vector2.zero)
			{
				rescaled.x = 1.0f;
				rescaled.y = 1.0f;
			}
			transform.localScale = rescaled;
			
			if(OnCanvasSpriteCreated != null) OnCanvasSpriteCreated(this);
		}
		
		public string ToXML(string injectNodes="")
		{
			string xml = "";
			xml += "\t\t\t<sprite>\n";
			if(injectNodes.Length > 0) xml += injectNodes;
			xml += "\t\t\t\t<asset file=\"" + AssetName + "\"/>\n";
			xml += "\t\t\t\t<size width=\"" + Width + "\" height=\"" + Height + "\" />\n";
			xml += "\t\t\t\t<position x=\"" + Position.x + "\" y=\"" + Position.y + "\" />\n";
			xml += "\t\t\t\t<rotation z=\"" + Rotation + "\" />\n";
			xml += "\t\t\t\t<scale x=\"" + Scale.x + "\" y=\"" + Scale.y + "\" />\n";
			xml += "\t\t\t</sprite>\n";
			return xml;
		}
		
		public string ToString()
		{
			string str = AssetFileName + "," +
						//Asset + "," +
						Width + "," +
						Height + "," +
						Position.x + "," +
						Position.y + "," +
						Rotation + "," +
						Scale.x + "," +
						Scale.y;
			
			return str;
		}
		
		override protected void OnDestroy()
		{
			if(_rect != null) _rect = null;
			if(_image != null) _image = null;

			OnCanvasSpriteCreated = null;
			
			base.OnDestroy();
		}
	}
}