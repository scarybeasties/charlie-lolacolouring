﻿
using UnityEngine;

namespace ScaryBeasties.SB2D
{
	public class SB2DScaledSprite : SB2DSprite 
	{
		private float _scaler = 1f;

		// Getters
		public float scaler				{	get{return _scaler;}}
		public float ScaledWidth		{	get{return _scaler * Texture.width;}}
		public float ScaledHeight		{	get{return _scaler * Texture.height;}}

		void Awake()
		{
			switch(tk2dSystem.CurrentPlatform)
			{
			case "2x": 
				_scaler = 0.5f;	break;
				
			case "4x": 
				_scaler = 0.25f;	break;
				
			default: 
				_scaler = 1f;	break;
			}
		}

		// Apply a scale to the texture, once it's loaded.
		// This is so that we can have low res / high res textures on capable devices
		override protected void MakeTexture()
		{
			base.MakeTexture();

			// TODO: find a nicer way to scale the sprite down..
			Vector3 rescaled = transform.localScale;
			rescaled.x = _scaler;
			rescaled.y = _scaler;
			transform.localScale = rescaled;
		}
	}
}