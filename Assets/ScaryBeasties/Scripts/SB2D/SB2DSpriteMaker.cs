﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.SB2D;
using UnityEngine.UI;

namespace ScaryBeasties.SB2D
{
	public class SB2DSpriteMaker
	{
		const string DEFAULT_SPRITE_NAME = "SB2DSprite";
		const string DEFAULT_SPRITE_CLASS = "SB2DSprite";
		
		const string SCALED_SPRITE_CLASS = "SB2DScaledSprite";
		const string CANVAS_SPRITE_CLASS = "SB2DCanvasSprite";

		public static SB2DSprite Create(GameObject parent, System.Action<SB2DSprite> callback=null)
		{
			return Init (parent, DEFAULT_SPRITE_NAME, null, callback, parent.transform.position, DEFAULT_SPRITE_CLASS);
		}
		
		public static SB2DSprite Create(GameObject parent, string name, System.Action<SB2DSprite> callback=null)
		{
			return Init (parent, name, null, callback, parent.transform.position, DEFAULT_SPRITE_CLASS);
		}

		public static SB2DSprite Create(GameObject parent, string name, Object data, System.Action<SB2DSprite> callback=null)
		{
			return Init (parent, name, data, callback, parent.transform.position, DEFAULT_SPRITE_CLASS);
		}
		
		public static SB2DSprite Create(GameObject parent, string name, Object data, Vector3 position, System.Action<SB2DSprite> callback=null)
		{
			return Init (parent, name, data, callback, position, DEFAULT_SPRITE_CLASS);
		}



		public static SB2DScaledSprite CreateScaledSprite(GameObject parent, System.Action<SB2DSprite> callback=null)
		{
			return (SB2DScaledSprite)Init (parent, DEFAULT_SPRITE_NAME, null, callback, parent.transform.position, SCALED_SPRITE_CLASS);
		}
		
		public static SB2DScaledSprite CreateScaledSprite(GameObject parent, string name, System.Action<SB2DSprite> callback=null)
		{
			return (SB2DScaledSprite)Init (parent, name, null, callback, parent.transform.position, SCALED_SPRITE_CLASS);
		}
		
		public static SB2DScaledSprite CreateScaledSprite(GameObject parent, string name, Object data, System.Action<SB2DSprite> callback=null)
		{
			return (SB2DScaledSprite)Init (parent, name, data, callback, parent.transform.position, SCALED_SPRITE_CLASS);
		}
		
		public static SB2DScaledSprite CreateScaledSprite(GameObject parent, string name, Object data, Vector3 position, System.Action<SB2DSprite> callback=null)
		{
			return (SB2DScaledSprite)Init (parent, name, data, callback, position, SCALED_SPRITE_CLASS);
		}
		
		
		
		public static SB2DCanvasSprite CreateCanvasSprite(GameObject parent, System.Action<SB2DSprite> callback=null)
		{
			return (SB2DCanvasSprite)Init (parent, DEFAULT_SPRITE_NAME, null, callback, parent.transform.position, CANVAS_SPRITE_CLASS);
		}
		
		public static SB2DCanvasSprite CreateCanvasSprite(GameObject parent, string name, System.Action<SB2DSprite> callback=null)
		{
			return (SB2DCanvasSprite)Init (parent, name, null, callback, parent.transform.position, CANVAS_SPRITE_CLASS);
		}
		
		public static SB2DCanvasSprite CreateCanvasSprite(GameObject parent, string name, Object data, System.Action<SB2DSprite> callback=null)
		{
			return (SB2DCanvasSprite)Init (parent, name, data, callback, parent.transform.position, CANVAS_SPRITE_CLASS);
		}
		
		public static SB2DCanvasSprite CreateCanvasSprite(GameObject parent, string name, Object data, Vector3 position, System.Action<SB2DSprite> callback=null)
		{
			return (SB2DCanvasSprite)Init (parent, name, data, callback, position, CANVAS_SPRITE_CLASS);
		}
		

		// Initialise the new SB2DSprite
		static SB2DSprite Init(GameObject parent, string name, Object data, System.Action<SB2DSprite> callback, Vector3 position, string spriteClass)
		{
			if(parent.transform == null)
			{
				Debug.LogError("ERRRROOOOOOOOOORRRRRRRRRRRR parent.transform = null!!!!!!!");
			}
		
			GameObject spriteGameObject = new GameObject ();
			spriteGameObject.transform.SetParent (parent.transform,true);
            spriteGameObject.transform.localScale = Vector3.one;
			spriteGameObject.name = name;

            Debug.Log("Sprite Maker " + spriteClass);
			
			if(spriteClass == CANVAS_SPRITE_CLASS)
			{
				spriteGameObject.AddComponent<Image>();
			}
			else
			{
				spriteGameObject.AddComponent<SpriteRenderer> ();
			}
			
			spriteGameObject.transform.position = position;

            SB2DSprite sb2dSr = (SB2DSprite)spriteGameObject.AddComponent(System.Type.GetType("ScaryBeasties.SB2D."+spriteClass));
            //SB2DSprite sb2dSr = (SB2DSprite)UnityEngineInternal.APIUpdaterRuntimeServices.AddComponent(spriteGameObject, "Assets/ScaryBeasties/Scripts/SB2D/SB2DSpriteMaker.cs (104,36)", spriteClass);
            sb2dSr.Init(data, callback);

			return sb2dSr;
		}
	}
}
