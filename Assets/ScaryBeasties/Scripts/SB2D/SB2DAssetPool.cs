﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Data;

public class SB2DAssetPool
{
	private KeyValueObject _texturesKV = new KeyValueObject();

	private static SB2DAssetPool _instance = null;
	public static SB2DAssetPool instance
	{
		get 
		{
			if(_instance == null)
			{
				_instance = new SB2DAssetPool();
			}
			
			return _instance;
		}
	}
	
	public void AddTextureBytes(string filename, byte[] bytes)
	{
		if(!_texturesKV.ContainsKey(filename))
		{
			_texturesKV.SetKeyValue(filename, bytes);
		}
	}
	
	public byte[] GetTextureBytes(string filename)
	{
		if(_texturesKV.ContainsKey(filename))
		{
			byte[] bytes = (byte[])_texturesKV.GetValue(filename);
			return bytes;
		}
		
		return null;
	}
	
	public void Clear()
	{
		ArrayList keys = _texturesKV.Keys;
		
		int k=0;
		while(_texturesKV.Count > 0)
		{
			_texturesKV.RemoveKey((string)keys[k]);
			k++;
		}
		_texturesKV.Clear();
		_texturesKV = new KeyValueObject();
		
	}
}
