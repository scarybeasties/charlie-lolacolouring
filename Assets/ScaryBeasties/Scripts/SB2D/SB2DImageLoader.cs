﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

namespace ScaryBeasties.SB2D
{
    public class SB2DImageLoader : MonoBehaviour
    {
        private Texture2D _texture;
        private Action<bool> _callback;

        public Texture2D Texture { get { return _texture; } }

        public void LoadImage(string filePath, Action<bool> callback, bool loadFromLocalFileSystem = true)
        {
            //Debug.LogWarning ("[SB2DImageLoader] LoadImage: "  +filePath);
            _callback = callback;

            if (loadFromLocalFileSystem)
            {
                filePath = "file://" + filePath;
            }
            StartCoroutine(DoLoadImage(filePath, callback));
        }

        public void LoadReadableImage(string filePath, Action<bool> callback, bool loadFromLocalFileSystem = true)
        {
            //Debug.LogWarning ("[SB2DImageLoader] LoadImage: "  +filePath);
            _callback = callback;

            if (loadFromLocalFileSystem)
            {
                filePath = "file://" + filePath;
            }
            StartCoroutine(DoLoadReadableImage(filePath, callback));
        }

        public void LoadBytes(byte[] bytes, Action<bool> callback)
        {
            _callback = callback;

            // IMPORTANT! TextureFormat.RGBA32 must be set, for the images to look good on screen.
            //				mipmap must be set to false, otherwise the image looks blurry
            _texture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            _texture.wrapMode = TextureWrapMode.Clamp;
            _texture.LoadImage(bytes);

            DoCallback(true);
        }

        IEnumerator DoLoadImage(string filePath, Action<bool> callback)
        {
            _callback = callback;
#if UNITY_ANDROID
            WWW www = new WWW(filePath);
            yield return www;

            if (www.isDone)
            {
                if (www.bytes.Length == 0)
                {
                    // error
                    Debug.LogError("ERROR LOADING FILE " + www.error);
                    DoCallback(false);
                }
                else
                {
                    // success
                    //Debug.Log ("DONE LOADING FILE '"+filePath+"',  bytesDownloaded: " + www.bytesDownloaded + ", bytes.Length: " + www.bytes.Length);
                    //LoadBytes(www.bytes, callback);
                    _callback = callback;
                    _texture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
                    _texture.LoadImage(www.bytes);
                    _texture.wrapMode = TextureWrapMode.Clamp;
                    DoCallback(true);
                }
            }

            www.Dispose();
            www = null;
#else
            filePath = filePath.Replace("file:///", "file://");
            filePath = filePath.Replace("file://", "/");

            Debug.Log("Do Load Image -- filePath " + filePath);
            if (File.Exists(filePath))
            {
                Debug.Log("Do Load Image -- exists " + filePath);
                _callback = callback;
                _texture = new Texture2D(2, 2);
                _texture.LoadImage(File.ReadAllBytes(filePath), true);
                _texture.wrapMode = TextureWrapMode.Clamp;
                yield return null;
                DoCallback(true);
            }
            else
            {
                DoCallback(false);
            }
#endif
        }

        IEnumerator DoLoadReadableImage(string filePath, Action<bool> callback)
        {
            _callback = callback;

            // TEST!
#if UNITY_ANDROID
            filePath = filePath.Replace("file://", "file:///");
            WWW www = new WWW(filePath);
            yield return www;

            if (www.isDone)
            {
                if (www.bytes.Length == 0)
                {
                    // error
                    Debug.LogError("ERROR LOADING FILE " + www.error);
                    DoCallback(false);
                }
                else
                {
                    // success
                    //Debug.Log ("DONE LOADING FILE '"+filePath+"',  bytesDownloaded: " + www.bytesDownloaded + ", bytes.Length: " + www.bytes.Length);
                    //LoadBytes(www.bytes, callback);
                    _callback = callback;
                    _texture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
                    _texture.LoadImage(www.bytes);
                    _texture.wrapMode = TextureWrapMode.Clamp;
                    DoCallback(true);
                }
            }

            www.Dispose();
            www = null;
#else
            filePath = filePath.Replace("file:///", "file://");
            filePath = filePath.Replace("file://", "/");

            Debug.Log("Do Load Image -- filePath " + filePath);
            if (File.Exists(filePath))
            {
                Debug.Log("Do Load Image -- exists " + filePath);
                _callback = callback;
                _texture = new Texture2D(2, 2);
                _texture.LoadImage(File.ReadAllBytes(filePath), false);
                _texture.wrapMode = TextureWrapMode.Clamp;
                yield return null;
                DoCallback(true);
            }
            else
            {
                DoCallback(false);
            }
#endif
        }

        void DoCallback(bool success)
        {
            if (_callback != null)
            {
                _callback(success);
            }
        }

        void Dispose()
        {
            if (_texture != null)
            {
                Destroy(_texture);
            }
        }

        void OnDestroy()
        {
            Dispose();

            _callback = null;
            _texture = null;
        }
    }
}