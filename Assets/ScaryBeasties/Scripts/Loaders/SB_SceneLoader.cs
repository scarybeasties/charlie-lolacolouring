using UnityEngine;
using System.Collections;
using System.Timers;
using ScaryBeasties.Base;
using ScaryBeasties.Data;

namespace ScaryBeasties.Loaders
{
	public class SB_SceneLoader : ScriptableObject
	{
		public const string LOADER_PROGRESS = "loaderProgress";
		public const string LOADER_COMPLETE = "loaderComplete";

		public delegate void LoadingCompleteDelegate(SB_SceneLoader loader, string eventType);
		public event LoadingCompleteDelegate OnLoadingComplete;

		public delegate void LoadingProgressDelegate(SB_SceneLoader loader, string eventType);
		public event LoadingProgressDelegate OnLoadingProgress;

		private AsyncOperation _asyncOperation;

		bool _isLoadingLevel = false;

		public void loadLevel(string levelId, KeyValueObject sceneConfig=null)
		{
			// Stop if we are already loading a level
			if(_isLoadingLevel) return;

			_isLoadingLevel = true;
			SB_BaseScene.SCENE_CONFIG_OBJECT = sceneConfig;

			//---------------------------------------------
			// The following code seems to crash, when testing in XCode:
			// (..needs further investigation)

			SB_Globals.nextSceneName = levelId;
			//SB_Globals.nextSceneKVObject = sceneConfig;

			Debug.Log ("[SB_SceneLoader] Application.LoadLevelAsync: " + levelId);
			//_asyncOperation = Application.LoadLevelAsync(levelId);
			//Application.LoadLevel(levelId);
			Application.LoadLevel(SB_Globals.SCENE_EMPTY_LOADER);

			// Call 'CheckProgress', so that delegates are called immediately
			//CheckProgress();

			//---------------------------------------------
			//Application.LoadLevel(levelId);
		}

		public void loadLevelAdditive(string levelId)
		{
			// Stop if we are already loading a level
			if(_isLoadingLevel) return;
			
			_isLoadingLevel = true;

			Debug.Log ("[SB_SceneLoader] Application.LoadLevelAdditiveAsync: " + levelId);
			_asyncOperation = Application.LoadLevelAdditiveAsync(levelId);

			// Call 'CheckProgress', so that delegates are called immediately
			CheckProgress();
		}

		public float getProgress()
		{
			if(_asyncOperation == null) return 0.0f;
			return _asyncOperation.progress;
		}
		
		public float getProgressPercentage()
		{
			if(_asyncOperation == null) return 0.0f;
			return Mathf.Round(_asyncOperation.progress*100);
		}

		public void CheckProgress()
		{
			Debug.Log ("[SB_SceneLoader] CheckProgress " + getProgressPercentage() + "%");

			if(_asyncOperation != null)
			{
				if(_asyncOperation.isDone)
				{
					_isLoadingLevel = false;
					// Call the delgate
					if(OnLoadingComplete != null) OnLoadingComplete(this, LOADER_COMPLETE);
				}
				else 
				{
					if(OnLoadingProgress != null) OnLoadingProgress(this, LOADER_PROGRESS);
				}
			}
		}

		public void OnDestroy()
		{
			_asyncOperation = null;

			// Nullify delegates
			OnLoadingComplete = null;
			OnLoadingProgress = null;
			//Debug.LogWarning("LOADER DETROYED");
		}
	}
}