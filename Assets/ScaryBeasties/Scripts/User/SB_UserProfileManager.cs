﻿
using UnityEngine;
using ScaryBeasties.Stickers;
using ScaryBeasties.Games.Common.VO;
using System.Xml;
using ScaryBeasties.Data;
using ScaryBeasties.Utils;
using System.Collections;
using System.IO;

namespace ScaryBeasties.User
{
    public class SB_UserProfileManager
    {
        private SB_ProfileVO _currentProfile = null;

        private static SB_UserProfileManager _instance = null;
        public static SB_UserProfileManager instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SB_UserProfileManager();
                }

                return _instance;
            }
        }

        /**
		 * Constructor - Should never be called directly.
		 * Use 'SB_UserProfileManager.instance' to get the singleton
		 */
        public SB_UserProfileManager()
        {
            if (SB_Globals.TEST_MODE)
            {
                // Testing locally, so try and load an existing profile..
                CurrentProfile = GetProfileVO(1);
                if (CurrentProfile == null)
                {
                    // Create a dummy profile
                    CurrentProfile = new SB_ProfileVO(1, "test_user", "Avatar_Main_Duck", GetDefaultStickers(), CreateEmptyGameData());
                }
            }
        }

        /**
		 * Returns the user profile object which is in current use
		 */
        public SB_ProfileVO CurrentProfile
        {
            set
            {
                _currentProfile = value;
            }

            get
            {
                return _currentProfile;
            }
        }

        /**
		 * Saves user profile name and chosen character type.
		 * @SB_ProfileVO profileVO A Profile Value Object, containing the data to be saved.
		 * 							If name is passed in as a blank string, then the stored 
		 * 							data is effectively being deleted.
		 */
        public void SaveProfileData(SB_ProfileVO profileVO)
        {
            Debug.LogWarning("----------- SaveProfileData -----------");
            Debug.LogWarning("profileVO: " + profileVO.ToString());

            string filename = GetProfileFilename(profileVO.profileId);
            byte[] bytes = SB_Utils.StringToByteArray(profileVO.ToXML());

            string folder = GetProfileFolder(profileVO.profileId);
            DirectoryInfo dir = new DirectoryInfo(folder);

            if (!dir.Exists)
            {
                Debug.Log("subdirectory doesnt exist, create subdirectory");
                dir.Create();
            }
            else
            {
                Debug.Log("subdirectory already exists");
            }

            Debug.LogWarning("Saving profile data to path: " + filename);
            System.IO.File.WriteAllBytes(filename, bytes);
        }

        public SB_ProfileVO DeleteProfileData(SB_ProfileVO profileVO)
        {
            StickerManager.instance.Reset();

            // Reset the name and avatar Id, but not the profile Id.
            // We need the profile Id, to save the blank data
            profileVO.Reset();
            profileVO.collectedStickers = GetDefaultStickers();

            SaveProfileData(profileVO);

            string path = GetProfileFolder(profileVO.profileId);
            DirectoryInfo dir = new DirectoryInfo(path);
            if (dir.Exists)
            {
                Debug.Log("Delete contents of directory: " + dir.FullName);
                dir.Delete(true);
            }
            else
            {
                Debug.Log("Directory doesn't exist");
            }

            // Reset the Globals profile VO to a VO with empty data
            //SB_Globals.SELECTED_PROFILE_VO = null;
            _currentProfile = null;

            // Return the VO, as it still contains the Id of the deleted profile
            return profileVO;
        }

        public static IEnumerator LoadTexture(string fname, Texture2D tex)
        {
#if UNITY_ANDROID
            WWW www = new WWW("file:///" + SB_UserProfileManager.instance.CurrentProfileFolder + fname + ".png");
            //Debug.LogWarning("LOADING "+SB_UserProfileManager.instance.CurrentProfileFolder + fname+".png");
            yield return www;
            if (string.IsNullOrEmpty(www.error))
            {
                byte[] bytes = www.bytes;
                //Debug.LogWarning("LOADING BYTES "+bytes.Length);
                //tex = www.texture;
                tex.LoadImage(bytes);
            }
#else
            string fileToLoad = SB_UserProfileManager.instance.CurrentProfileFolder + fname + ".png";
            if (File.Exists(fileToLoad))
            {
                tex.LoadImage(File.ReadAllBytes(fileToLoad), true);
                tex.wrapMode = TextureWrapMode.Clamp;
                yield return null;
            }
            else
            {
                yield return null;
            }
#endif
            
        }

            /**
             * Returns the path to the folder where user generated content is saved
             */
            private string GetProfileFolder(int id)
            {
                string folder = Application.persistentDataPath + "/user" + id + "/";
                return folder;
            }

            /**
             * Returns the path to the userprofile XML file
             */
            private string GetProfileFilename(int id)
            {
                string filename = Application.persistentDataPath + "/userprofile" + id + ".xml";
                return filename;
            }
        /**
		 * Returns the path to the folder where user generated content is saved, for the current user
		 */
        public string CurrentProfileFolder
        {
            get
            {
                return GetProfileFolder(_currentProfile.profileId);
            }
        }

        /**
		 * Returns a SB_ProfileVO object, for a given profileId
		 */
        public SB_ProfileVO GetProfileVO(int profileId)
        {
            StickerManager.instance.Reset();

            SB_ProfileVO profileVO;

            string filename = GetProfileFilename(profileId);
            if (System.IO.File.Exists(GetProfileFilename(profileId)))
            {
                // Load and parse the user profile XML
                byte[] bytes = System.IO.File.ReadAllBytes(filename);
                string userProfileString = SB_Utils.ByteArrayToString(bytes);

                Debug.LogWarning("---------------------------------------");
                Debug.LogWarning(filename);
                Debug.LogWarning("---------------------------------------");

                XmlDocument userXml = new XmlDocument();
                userXml.LoadXml(userProfileString);

                XmlNode userNode = userXml.SelectNodes("userprofile")[0];

                GameDataVO[] gameData = new GameDataVO[SB_Globals.GAME_ORDER.Length];
                XmlNodeList games = userXml.SelectNodes("userprofile/games/game");
                int g = 0;

                foreach (XmlElement game in games)
                {
                    int id = int.Parse(game.GetAttribute("id"));
                    string name = game.GetAttribute("name");
                    bool hasBeenPlayed = bool.Parse(game.GetAttribute("hasBeenPlayed"));
                    int skillLevel = int.Parse(game.GetAttribute("skillLevel"));
                    int sessionSecsPlayed = int.Parse(game.GetAttribute("sessionSecsPlayed"));
                    int totalSecsPlayed = int.Parse(game.GetAttribute("totalSecsPlayed"));
                    int numberOfPlays = int.Parse(game.GetAttribute("numberOfPlays"));
                    bool unlockRequired = bool.Parse(game.GetAttribute("unlockRequired"));

                    XmlNodeList userkvobject = game.SelectNodes("userContent/kvobject/data");
                    KeyValueObject userContent = new KeyValueObject();
                    foreach (XmlElement kvobject in userkvobject)
                    {
                        string key = kvobject.GetAttribute("key");
                        string value = kvobject.GetAttribute("value");
                        userContent.SetKeyValue(key, value);
                    }

                    gameData[g] = new GameDataVO(id, name, hasBeenPlayed, skillLevel, sessionSecsPlayed, totalSecsPlayed, numberOfPlays, userContent);
                    gameData[g].unlockRequired = unlockRequired;
                    g++;
                }

                KeyValueObject installedPacks = new KeyValueObject();
                XmlNodeList installedPacksKVObject = userXml.SelectNodes("userprofile/iap_packs_installed/kvobject/data");
                foreach (XmlElement kvobject in installedPacksKVObject)
                {
                    string key = kvobject.GetAttribute("key");
                    string value = kvobject.GetAttribute("value");
                    installedPacks.SetKeyValue(key, value);
                }

                profileVO = new SB_ProfileVO(profileId,
                                             userNode.Attributes.GetNamedItem("name").Value,
                                             userNode.Attributes.GetNamedItem("avatarId").Value,
                                             userNode.Attributes.GetNamedItem("collectedStickers").Value,
                                             gameData,
                                             installedPacks);

                profileVO.tutorialNum = int.Parse(userNode.Attributes.GetNamedItem("tutorialNum").Value);

#if UNITY_EDITOR
                // Disable the tutorial, while running in the Editor
                //profileVO.tutorialNum=100;//11;
#endif

                Debug.LogWarning("profileVO: " + profileVO.ToString());
            }
            else
            {
                Debug.LogWarning("Filename not found! " + filename);

                // Create a new, empty profile VO object
                profileVO = new SB_ProfileVO(profileId, "", "", GetDefaultStickers(), CreateEmptyGameData());
                SaveProfileData(profileVO);
            }

            // Profle data has loaded.
            // We need to update the StickerManager, so that it knows which sticker ids have been collected
            if (profileVO.collectedStickers.Length > 0)
            {
                char[] splitchar = { ',' };
                string[] collectedStickers = profileVO.collectedStickers.Split(splitchar);

                Debug.LogWarning("profileVO.collectedStickers: '" + profileVO.collectedStickers + "'");
                for (var n = 0; n < collectedStickers.Length; n++)
                {
                    int stickerId = int.Parse(collectedStickers[n]);

                    StickerDataVO stickerData = StickerManager.instance.GetSticker(stickerId);
                    stickerData.collected = true;
                    StickerManager.instance.UpdateSticker(stickerData);
                }
            }
            else
            {
                Debug.LogWarning("** User has not collected any stickers");
            }
            /*
			string ulPacks = StickerManager.instance.GetUnlockedPacks();
			if (ulPacks.Length > 0)
			{
				char[] splitchar = { ',' };
				string[] unlockedPacks = ulPacks.Split(splitchar);
				Debug.LogWarning("** User has not unlocked " + unlockedPacks.Length + " packs");

				for (int n=0; n<unlockedPacks.Length; n++) 
				{
					StickerManager.instance.UnlockPack(int.Parse(unlockedPacks[n]));
				}
			}
			else
			{
				Debug.LogWarning("** User has not unlocked any packs");
			}
			*/

            int numPacks = StickerManager.instance.GetNumPacks();
            for (int p = 0; p < numPacks; p++)
            {
                if (PackIsUnlocked(p))
                {
                    StickerManager.instance.UnlockPack(p);
                }
            }

            return profileVO;
        }

        /** 
		 * Return a comma seperated string, containing the default sticker id's
		 */
        public string GetDefaultStickers()
        {
            string defaultStickers = "";
            ArrayList unlockledStickers = StickerManager.instance.GetUnlockedStickers();

            int n = 0;
            foreach (StickerDataVO sticker in unlockledStickers)
            {
                Debug.LogWarning("----UNLOCKED STICKER: " + sticker.id);
                defaultStickers += sticker.id.ToString();
                n++;

                if (n < unlockledStickers.Count) defaultStickers += ",";
            }

            return defaultStickers;
        }

        /**
		 * Create an array of empty GameDataVO objects, which just contain the game ID and name
		 */
        public GameDataVO[] CreateEmptyGameData()
        {
            GameDataVO[] gameData = new GameDataVO[SB_Globals.GAME_ORDER.Length];

            XmlNodeList games = SB_Globals.CONFIG_XML.GetNodeList("config/games/game");
            int g = 0;

            foreach (XmlElement game in games)
            {
                int id = int.Parse(game.GetAttribute("id"));
                string name = game.GetAttribute("name");

                gameData[g] = new GameDataVO(id, name);
                g++;
            }

            return gameData;
        }

        /**
		 * Unlocks a content pack, and saves the data in
		 * each user XML file
		 * 
		 * Once a pack is unlocked, it's available to all users,
		 * so there's no need to save the data under each user profile.
		 */
        public void UnlockPack(int packId)
        {
            // Check if pack is already unlocked
            if (PackIsUnlocked(packId)) return;

            string packsStr = PlayerPrefs.GetString("UnlockedPacks");
            if (packsStr != null && packsStr.Length > 0)
            {
                packsStr += ",";
            }
            packsStr += packId.ToString();
            PlayerPrefs.SetString("UnlockedPacks", packsStr);

            StickerManager.instance.UnlockPack(packId);
        }

        /**
		 * Returns a boolean, checks if a pack is unlocked
		 */
        public bool PackIsUnlocked(int packId)
        {
            string packsStr = PlayerPrefs.GetString("UnlockedPacks");
            if (packsStr.Length > 0)
            {
                char[] splitchar = { ',' };
                string[] unlockedPacks = packsStr.Split(splitchar);

                for (var n = 0; n < unlockedPacks.Length; n++)
                {
                    int pId = int.Parse(unlockedPacks[n]);

                    if (pId == packId)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /**
		 * Used during testing, to delete 'UnlockedPacks'
		 */
        public void ResetPacks()
        {
            PlayerPrefs.DeleteKey("UnlockedPacks");
        }


        public int NumberOfActiveProfiles(int maxProfiles)
        {

            int numActive = 0;

            for (int n = 1; n <= maxProfiles; n++)
            {
                SB_ProfileVO profile = GetProfileVO(n);
                if (profile.name != "")
                {
                    numActive++;
                }
            }

            return numActive;
        }

    }
}