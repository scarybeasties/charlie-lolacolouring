﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ScaryBeasties.Games.Common.VO;
using ScaryBeasties.Data;

namespace ScaryBeasties.User
{
	public class SB_ProfileVO
	{
		private int _profileId = 0;
		private string _name = "";
		private string _avatarId = "";

		// A comma separated list of collected sticker id's
		private string _collectedStickers = "";

		// A comma separated list of unlocked pack id's
		private string _unlockedPacks = "";

		// Stores a custom profile picture taken using the webcam
		private Texture2D _customProfilePic;

		private GameDataVO[] _gameData;
		private KeyValueObject _installedIAPPacks;

		private int _tutorialNum = 0;
		
		public SB_ProfileVO(int profileId, string name="", string avatarId="", string collectedStickers="", GameDataVO[] gameDataVoArray=null, KeyValueObject installedIAPPacks=null)
		{
			_profileId = profileId;
			_name = name;
			_avatarId = avatarId;
			_collectedStickers = collectedStickers;
			_unlockedPacks = unlockedPacks;
			_gameData = gameDataVoArray;
			_installedIAPPacks = installedIAPPacks;
			if(_installedIAPPacks == null) _installedIAPPacks = new KeyValueObject();
			
			_unlockedPacks = PlayerPrefs.GetString ("UnlockedPacks");

			if(avatarId == "custom")
			{
				//StartCoroutine(LoadCustomProfilePicture());
				LoadCustomProfilePicture();
			}
		}

		//IEnumerator LoadCustomProfilePicture() 
		void LoadCustomProfilePicture() 
		{
			_customProfilePic = new Texture2D(100, 120);

			byte[] bytes = System.IO.File.ReadAllBytes(Application.persistentDataPath + "/custom" + _profileId + ".png");
			_customProfilePic.LoadImage(bytes);
		}

		/** 
		 * Reset the properties of this VO.
		 * Used whenever a profile is deleted.
		 */
		public void Reset()
		{
			_name = "";
			_avatarId = "";
			_collectedStickers = "";
			_customProfilePic = null;
			_tutorialNum = 0;
			/**
			 * NOTE: 'unlockedPacks' does not get reset, as packs
			 * 		 need to remain unlocked, even if the profile is deleted.
			 */ 
			foreach(GameDataVO gameDataVO in gameData)
			{
				gameDataVO.Reset();
			}
		}

		/**
		 * This is used during testing, to clear 
		 * all unlocked packs from the profile
		 */ 
		public void ResetUnlockedPacks()
		{
			unlockedPacks = "";
		}
		
		public int profileId					{	get {return _profileId;}	}
		public string name						{	get {return _name;}					set{_name = value;}}
		public string avatarId					{	get {return _avatarId;}				set{_avatarId = value;}}
		public string collectedStickers			{	get {return _collectedStickers;}	set{_collectedStickers = value;}}
		public string unlockedPacks				{	get {return _unlockedPacks;}		set{_unlockedPacks = value;}}
		public Texture2D customProfilePic		{	get {return _customProfilePic;}	}
		public GameDataVO[] gameData			{	get {return _gameData;}	}
		public KeyValueObject installedIAPPacks	{	get {return _installedIAPPacks;}	set{_installedIAPPacks = value;}}
		public int tutorialNum						{	get {return _tutorialNum;}					set{_tutorialNum = value;}}

		public string ToString()
		{
			string str = 
					"profileId: " + profileId + 
					", name: " + name + 
					", avatarId: " + avatarId + 
					", tutorialNum: " + tutorialNum + 
					", collectedStickers: " + collectedStickers;

					foreach(GameDataVO gameDataVO in gameData)
					{
						if(gameDataVO != null)
						{
							str += ", " + gameDataVO.ToString();
						}
					}
					
					str += _installedIAPPacks.ToString();
			
			return str;
		}
		
		public string ToXML()
		{
			string str = 
				"<userprofile profileId='" + profileId + "'" +
					" name='" + name + "'" +
					" avatarId='" + avatarId + "'" +
					" tutorialNum='" + tutorialNum +"'" +
					" collectedStickers='" + collectedStickers + "'" +
					">";
			
			str += "\n\t<games>";
			foreach(GameDataVO gameDataVO in gameData)
			{
				if(gameDataVO != null)
				{
					str +=  gameDataVO.ToXML();
				}
			}
			str += "\n\t</games>";
			
			str += "\n\t<iap_packs_installed>";
			str += _installedIAPPacks.ToXML();
			str += "\n\t</iap_packs_installed>";
			
			str += "\n</userprofile>";
			
			return str;
		}
	}
}