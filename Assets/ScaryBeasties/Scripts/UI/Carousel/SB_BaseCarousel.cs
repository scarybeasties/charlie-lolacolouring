using UnityEngine;
using System.Collections;
using ScaryBeasties.Base;
using System.Collections.Generic;
using ScaryBeasties.Utils;
using ScaryBeasties.Tweening;

[System.Serializable]
public class CarouselOffsets
{
	public bool useOffsetX = true;
	public float left = 0f;
	public float right = 0f;

	public bool useOffsetY = false;
	public float top = 0f;
	public float bottom = 0f;
}

[System.Serializable]
public class ScrollLimits
{
	public bool enabled = false;
	public int limitX = 0;
	public int limitY = 0;
}

public class SB_BaseCarousel : SB_Base {
	
	
	//Definitions
	public enum AlignmentMode 
	{
		HORIZONTAL,
		VERTICAL
	}
	
	
	public const int POSITION_TOP =0;
	public const int POSITION_BOTTOM =1;
	public const int POSITION_LEFT =2;
	public const int POSITION_RIGHT =3;
	public const int POSITION_CENTER =4;
	public const int POSITION_OFF_TOP =5;
	public const int POSITION_OFF_BOTTOM =6;
	public const int POSITION_OFF_LEFT =7;
	public const int POSITION_OFF_RIGHT =8;
	
	public AlignmentMode alignment = AlignmentMode.HORIZONTAL;
	
	//bounds of carousel
	protected Bounds bounds;
	private Bounds unscaledBounds;
	
	//object gap
	public float _gap=10.0f;
	public float _maxWidth=-1.0f;	//-1 is unlimited
	private float _carouselWidth=0.0f;
	private float _carouselMax=0.0f;
	private float _carouselMin = 0.0f;
	
	//content layer
	protected ArrayList _items = new ArrayList();
	protected GameObject _contentLayer;
	private bool _scrollable=false;
	
	//Speeds
	Vector3 _targetPos;
	private float _snapSpeed=1000.0f;
	
	//Carousel scrolling
	private float _scrollSpeed=0.0f;
	private float _scrollMax=500.0f;
	private float _dragDist=50.0f;
	
	private float _scrollDecel = 100.0f;
	private ArrayList _lastDrags = new ArrayList();
	private int _dragSensitivity=20;
	public float _bounce =0.2f;
	
	protected bool _mouseDown = false;
	protected bool _dragging = false;
	
	private bool _dragable=true;
	
	//Item picking
	private float _itemHoldRate = 0.001f;
	private float _itemHold =0.0f;
	private int _itemSelected =-1;
	private GameObject _selectedItem;
	private int _selectedItemNum=-1;
	public float _selectScale=1.2f;
	
	//Bounds if defined
	/*
	public bool useOffsetX=true;
	public float offsetXLeft = 120.0f;
	public float offsetXRight = 10.0f;
	public bool useOffsetY=false;
	public float offsetYTop = 500.0f;
	public float offsetYBottom = 10.0f;
	*/

	public CarouselOffsets carouselOffsets;
	public ScrollLimits scrollLimits;
	public bool snapToClosestCentralItem=false;
	private bool _itemSnapDone=false;

	private bool lockOffsetXMin = true;
	private bool lockOffsetXMax = true;
	private bool lockOffsetYTop = false;
	private bool lockOffsetYBottom = true;
	
	public bool buttonMode = false;
	public bool masked =false;
	
	private int _intialItem = -1;
	private Vector3 _initialPos;
	
	private float isScrollingDist = 10.0f;
	
	private bool _movingToPoint=false;
	private Vector3 _startPos;
	private Vector3 _endPos;
	private float _startTime;
	private float _endTime;
	
	private bool _resizing=false;
	private bool _returning=false;
	
	private Vector3 _carouselInitialPos;
	
	
	public delegate void OnSelectItemEvent(GameObject objectReference);
	public event OnSelectItemEvent OnSelectItem;
	
	public delegate void OnDragItemEvent(GameObject objectReference);
	public event OnDragItemEvent OnDragItem;
	
	public bool Paused = false;
	
	private const float BASE_LOCK_OFF = -1000.0f;
	private float baseLockPos = -1000.0f;

	private float _carouselHeight=-100000.0f;
	
	private SB_CarouselDataItem _centralCarouselDataItem = null;
	private GameObject _centralItem = null;
	private float _itemWidth;

	// Use this for initialization
	override protected void Start () {
		//base.Start();
		_contentLayer=transform.Find("Content").gameObject;
		_carouselInitialPos=_contentLayer.transform.position;
		SetupBounds();
	}

	public bool MouseDown
	{
		get{return _mouseDown;}
	}
	
	public int GetItemCount()
	{
		return _items.Count;
	}
	
	// Returns the number of carousel items which are currently active
	public int GetActiveItemCount()
	{
		if(_items == null) return 0;
		
		int count = 0;
		foreach(SB_CarouselDataItem itm in _items)
		{
			if(itm.isActive())
				count++;
		}
		
		return count;
	}
	
	public GameObject GetContentLayer()
	{
		return _contentLayer;
	}
	
	public void SetCarouselHeight(float ch){
		_carouselHeight=ch;
	}
	
	public void CarouselMove()
	{
		SetupBounds ();
		_targetPos=_contentLayer.transform.position;
		
		for (int i=0; i<_items.Count; i++) 
		{
			SB_CarouselDataItem tobj = (SB_CarouselDataItem)_items [i];
			tobj.targetPos=tobj.item.transform.position;
		}
	}
	
	public virtual void SetupBounds()
	{	
		GameObject cbound = transform.Find("CarouselBounds").gameObject;
		
		Vector3 bpos = cbound.transform.position;
		//Vector3 bsize = new Vector3(cbound.transform.localScale.x*transform.localScale.x,cbound.transform.localScale.y*transform.localScale.y);
		Vector3 bsize = new Vector3(cbound.transform.localScale.x,cbound.transform.localScale.y);
		
		//float swidth = Screen.width;
		float swidth = (float)tk2dCamera.Instance.ScreenExtents.width;
		if(carouselOffsets.useOffsetX){
			float minx = carouselOffsets.left;
			float maxx = swidth-carouselOffsets.right;
			float wid = maxx-minx;
			bpos = new Vector3(minx+(wid/2)-(swidth/2),bpos.y,bpos.z);
			bsize = new Vector3(wid,bsize.y,bsize.z);
		}
		
		if(carouselOffsets.useOffsetY){
			float miny = carouselOffsets.bottom;
			float maxy = (float)tk2dCamera.Instance.ScreenExtents.height-carouselOffsets.top;
			float hgt = maxy-miny;
			bpos = new Vector3(bpos.x,miny+(hgt/2)-((float)tk2dCamera.Instance.ScreenExtents.height/2),bpos.z);
			bsize = new Vector3(bsize.x,hgt,bsize.z);
		}
		
		print("set bounds("+bpos+","+bsize+")");
		
		bounds = new Bounds(bpos,bsize);
		cbound.SetActive(false);
		
		
	}
	
	public void LockItemsToBottom(float ypos){
		//baseLockPos=ypos;
		//PositionAtBase();
		for (int i=0; i<_items.Count; i++) {
			SB_CarouselDataItem tobj = (SB_CarouselDataItem)_items [i];
			Bounds tb = tobj.item.GetComponent<SB_CarouselDataItem> ().GetBounds ();
			GameObject cbound = transform.Find("CarouselBackground").gameObject;
			tk2dBaseSprite tbs = cbound.GetComponent<tk2dBaseSprite>();
			//warn("CAR HEIGHT "+tbs.GetBounds().size.y);
			float ymin = cbound.transform.position.y-(tbs.GetUntrimmedBounds().size.y/1.0f);
			tobj.item.transform.position=new Vector3(tobj.item.transform.position.x,ymin+ypos,tobj.item.transform.position.z);
			tobj.targetPos=tobj.item.transform.position;
		}
	}
	/*
	public void PositionAtBase(){
		if(baseLockPos>BASE_LOCK_OFF){
			for (int i=0; i<_items.Count; i++) {
				SB_CarouselDataItem tobj = (SB_CarouselDataItem)_items [i];
				Bounds tb = tobj.item.GetComponent<tk2dBaseSprite> ().GetBounds ();
				GameObject cbound = transform.Find("CarouselBackground").gameObject;
				tk2dBaseSprite tbs = cbound.GetComponent<tk2dBaseSprite>();
				float ymin = cbound.transform.position.y-(tbs.GetBounds().size.y/1.0f);
				tobj.item.transform.position=new Vector3(tobj.item.transform.position.x,ymin+baseLockPos,tobj.item.transform.position.z);
				tobj.targetPos=tobj.item.transform.position;
			}
		}
	}*/
	
	public void SetupBoundsWithYVals(float ymin, float ymax){
		
		GameObject cbound = transform.Find("CarouselBounds").gameObject;
		Vector3 bpos = cbound.transform.position;
		//Vector3 bsize = new Vector3(cbound.transform.localScale.x*transform.localScale.x,cbound.transform.localScale.y*transform.localScale.y);
		Vector3 bsize = new Vector3(cbound.transform.localScale.x,cbound.transform.localScale.y);
		
		//float swidth = Screen.width;
		float swidth = (float)tk2dCamera.Instance.ScreenExtents.width;
		if(carouselOffsets.useOffsetX){
			float minx = carouselOffsets.left;
			float maxx = swidth-carouselOffsets.right;
			float wid = maxx-minx;
			bpos = new Vector3(minx+(wid/2)-(swidth/2),bpos.y,bpos.z);
			bsize = new Vector3(wid,bsize.y,bsize.z);
		}
		
		float miny = ymin;
		float maxy = ymax;
		float hgt = maxy-miny;
		bpos = new Vector3(bpos.x,miny+(hgt/2)-((float)tk2dCamera.Instance.ScreenExtents.height/2),bpos.z);
		bsize = new Vector3(bsize.x,hgt,bsize.z);
		
		
		bounds = new Bounds(bpos,bsize);
		cbound.SetActive(false);
		
		
	}
	
	// Initialise the carousel
	public void SetupItems(List<GameObject> items){
		
		//_items=items;
		
		if (alignment == AlignmentMode.HORIZONTAL) {
			
			float xpos=0;
			foreach(GameObject gameObject in items)
			{
				SB_CarouselDataItem cDataItem = gameObject.GetComponent<SB_CarouselDataItem>();

				ScaleObject(gameObject);
				
				float itemWidth = (cDataItem.GetBounds().size.x * gameObject.transform.localScale.x)/2;
				_itemWidth = itemWidth;
				
				//xpos+=((cDataItem.GetBounds().size.x*gameObject.transform.localScale.x)/2);
				xpos+=_itemWidth;

				//gameObject.transform.parent=_contentLayer.transform;
				gameObject.transform.SetParent(_contentLayer.transform);
				float zpos = 1.0f;
				if(masked) zpos = 51.0f;
				Vector3 pos = new Vector3(xpos-cDataItem.GetBounds().center.x,bounds.center.y,zpos);
				gameObject.transform.position = pos;
				//log ("pos " + pos);
				//gameObject.transform.position=new Vector3(xpos,bounds.center.y,zpos);
				
				
				
				gameObject.GetComponent<SB_CarouselDataItem>().SetCarouselWidth(gameObject.GetComponent<SB_CarouselDataItem>().GetBounds().size.x);//*gameObject.transform.localScale.x);
				
				xpos+=((gameObject.GetComponent<SB_CarouselDataItem>().GetBounds().size.x*gameObject.transform.localScale.x)/2)+_gap;
				
				SB_CarouselDataItem ditem = gameObject.GetComponent<SB_CarouselDataItem>();
				ditem.item=gameObject;
				_items.Add (ditem);
				ditem.scale = gameObject.transform.localScale.x;
				ditem.defaultPos = gameObject.transform.localPosition;
				ditem.targetPos = pos;
			
				
				//log ("ditem.scale: " + ditem.scale + ", ditem.defaultPos: " + ditem.defaultPos + ", local pos: " + gameObject.transform.localPosition);
				
			}
			
			SetCarouselWidth();
			_targetPos=new Vector3(bounds.center.x-_carouselWidth/2,_contentLayer.transform.position.y,_contentLayer.transform.position.z);
			_contentLayer.transform.position=_targetPos;
		}
		
		for (int i=0; i<_items.Count; i++) {
			SB_CarouselDataItem tobj = (SB_CarouselDataItem)_items [i];
			tobj.position = i;
			//tobj.item.transform.position = new Vector3(tobj.item.transform.position.x-_contentLayer.transform.position.x,tobj.item.transform.position.y,tobj.item.transform.position.z);
			tobj.targetPos=tobj.item.transform.position;
			
			//warn ("ITEM POS "+tobj.targetPos+" "+tobj.item.transform.position);
		}
		_targetPos = _contentLayer.transform.position;
		
		
		//warn ("CAROUSEL SETUP "+_carouselWidth+" "+_contentLayer.transform.position+" "+bounds.center.x);
	}
	
	public void AddObjectAt(int index, bool doResize = true)
	{
		SB_CarouselDataItem tobjtest = (SB_CarouselDataItem)_items [index];
		
		if(!tobjtest.active)
		{
			if (alignment == AlignmentMode.HORIZONTAL) {
				float movewidth = 0.0f;
				for (int i=index; i<_items.Count; i++) {
					SB_CarouselDataItem tobj = (SB_CarouselDataItem)_items [i];
					Bounds tb = tobj.item.GetComponent<SB_CarouselDataItem> ().GetBounds ();
					
					if (i == index) 
					{
						tobj.active = true;
						tobj.item.SetActive (true);
						movewidth = (tb.size.x * tobj.scale) + _gap;
						ScaleObject(tobj.item);
					}
					else 
					{
						//tobj.item.transform.position= new Vector3 (tobj.item.transform.position.x + movewidth, tobj.item.transform.position.y, tobj.item.transform.position.z);
						tobj.item.transform.position= new Vector3 (tobj.targetPos.x + movewidth, tobj.targetPos.y, tobj.item.transform.position.z);
						
						tobj.targetPos = tobj.item.transform.position;
						//tobj.targetPos = new Vector3 (tobj.item.transform.position.x + movewidth, tobj.item.transform.position.y, tobj.item.transform.position.z);
					}
				}
			}
			SetCarouselWidth ();
			//_contentLayer.transform.position = _targetPos;

			if(doResize) _resizing=true;
		}
	}
	
	private void ScaleObject(GameObject obj){
		
		//if(obj.GetComponent<tk2dBaseSprite>()!=null){
		SB_CarouselDataItem carouselDataItem = obj.GetComponent<SB_CarouselDataItem>();
		Bounds tmpobj = carouselDataItem.GetBounds();
		
		if(alignment==AlignmentMode.HORIZONTAL)
		{
			float sc=1.0f;

			//error("CAROUSEL BOUND CHECK " + obj.name +", tmpobj.size.y: "+tmpobj.size.y+", bounds.size.y : "+bounds.size.y + ", _maxWidth: " + _maxWidth);
			if(carouselDataItem.forceScaleInCarousel)
			{
				sc = carouselDataItem.scale;
				obj.transform.localScale=new Vector3(sc,sc,1);
			}
			
			//log("tmpobj.size.y: " + tmpobj.size.y + ", bounds.size.y: " + bounds.size.y);
			
			if(tmpobj.size.y>bounds.size.y || carouselDataItem.resizeToCarouselHeight)
			{
				sc = bounds.size.y/tmpobj.size.y;
				if(carouselDataItem.forceScaleInCarousel)
				{
					//error("Y > bounds! Force scaling of carousel item");
					sc = carouselDataItem.scale;
				}
				//log("	\tAppying scale: " + sc);
				obj.transform.localScale=new Vector3(sc,sc,1);
				
			}
			
			if(_maxWidth>0){
				if(tmpobj.size.x*sc>_maxWidth){
					sc = _maxWidth/(tmpobj.size.x);
					if(carouselDataItem.forceScaleInCarousel)
					{
						//error("X > max width! Force scaling of carousel item");
						sc = carouselDataItem.scale;
					}
					obj.transform.localScale=new Vector3(sc,sc,1);
					
				}
			}
			
			//log("-----------sc: " + sc + ", carouselDataItem.forceScaleInCarousel: " + carouselDataItem.forceScaleInCarousel);
			
		}
		//}
		
	}

	public float GetCarouselWidth()
	{
		return _carouselWidth;
	}

	public float GetCarouselMax()
	{
		return _carouselMax;
	}

	public float GetItemWidth()
	{
		return _itemWidth;
	}

	private void SetCarouselWidth(){
		
		
		float xpos = 0.0f;
		if (alignment == AlignmentMode.HORIZONTAL) {
			
			for(int i=0;i<_items.Count;i++){
				SB_CarouselDataItem tobj = (SB_CarouselDataItem)_items [i];
				Bounds tb = tobj.GetBounds();//tobj.item.GetComponent<SB_CarouselDataItem> ().GetBounds ();
				
				if(tobj.isActive()){
					
					//xpos+=((tb.size.x*tobj.scale))+_gap;
					
					
					xpos+=((tobj.GetCarouselWidth()*tobj.scale))+_gap;
					//xpos+=((tb.size.x))+_gap;
					//xpos+=((tb.size.x))+_gap;
				}
				
				//if((i==0)
				//_carouselMax=0-((tb.size.x*gameObject.transform.localScale.x));
			//	Debug.LogError("WIDTH "+i+" "+tb.size.x+" "+tobj.scale+" "+xpos);
				
			}
			_carouselWidth=xpos-_gap;
			_carouselMax=_carouselWidth;
			_scrollable=true;
			if (_carouselWidth <= bounds.size.x) {
				_scrollable = false;
				//_contentLayer.transform.position=new Vector3(0-_carouselWidth/2,_contentLayer.transform.position.y,_contentLayer.transform.position.z);
				_targetPos=new Vector3(bounds.center.x-_carouselWidth/2,_contentLayer.transform.position.y,_contentLayer.transform.position.z);
				
				
			}
			else{
				
				
			}
			//_contentLayer.transform.position = _targetPos;
		}
		
	}
	
	public void RemoveItem(SB_CarouselDataItem tobj,bool imediate=false, bool destroyItem=false)
	{
		int foundPos = -1;
		for (int i=0; i<_items.Count; i++) {
			SB_CarouselDataItem tobj2 = (SB_CarouselDataItem)_items [i];
			
			if(tobj==tobj2){
				
				foundPos=i;
				i=_items.Count;
				
			}
		}
		if(foundPos>=0){
			RemoveItemAt(foundPos,imediate,destroyItem);
		}
	}
	
	public void RemoveItemAt(int num,bool imediate=false, bool destroyItem=false)
	{
		
		SB_CarouselDataItem tobjtest = (SB_CarouselDataItem)_items [num];
		if(tobjtest.active){
			if (alignment == AlignmentMode.HORIZONTAL) {
				float movewidth = 0.0f;
				for (int i=num; i<_items.Count; i++) {
					SB_CarouselDataItem tobj = (SB_CarouselDataItem)_items [i];
					Bounds tb = tobj.item.GetComponent<SB_CarouselDataItem> ().GetBounds ();
					
					if (i == num) {
						
						movewidth = (tb.size.x * tobj.scale) + _gap;
						
						tobj.active = false;
						tobj.item.SetActive (false);
						
						if(destroyItem)
						{
							ClearItemAt(num);
						}
					} else {
						//tobj.item.transform.position= new Vector3 (tobj.item.transform.position.x - movewidth, tobj.item.transform.position.y, tobj.item.transform.position.z);
						
						tobj.targetPos = new Vector3 (tobj.item.transform.position.x - movewidth, tobj.item.transform.position.y, tobj.item.transform.position.z);
						if(imediate) tobj.transform.position=tobj.targetPos;
					}
				}
			}
			SetCarouselWidth ();
			if(!imediate) _resizing=true;
		}
		//_contentLayer.transform.position=_targetPos;
	}
	
	public void ReturnItem(SB_CarouselDataItem tobj){
		
	}
	
	public void ClearCarousel(){
		while(_items.Count>0){
			ClearItemAt (0);
		}
		_selectedItemNum=-1;
		_selectedItem=null;
		_contentLayer.transform.position=_carouselInitialPos;
		//_contentLayer.transform.position=new Vector3(0,0,_contentLayer.transform.position.z);
		_targetPos=_contentLayer.transform.position;
		
	}
	
	public void ClearItemAt(int index){
		SB_CarouselDataItem tobj = (SB_CarouselDataItem)_items [index];
		Destroy (tobj.item);
		_items.RemoveAt(index);
		tobj = null;
	}
	
	public void MoveOutOfCarousel(GameObject targ){
		
		while(_items.Count>0){
			SB_CarouselDataItem tobj = (SB_CarouselDataItem)_items [0];
			tobj.active = true;
			tobj.item.SetActive (true);
			tobj.item.transform.localScale=new Vector3(1,1,1);
			tobj.gameObject.transform.parent = targ.transform;
			tobj.gameObject.transform.position=new Vector3(tobj.gameObject.transform.position.x,-1000.0f,tobj.gameObject.transform.position.z);
			_items.RemoveAt(0);
		}
		_selectedItemNum=-1;
		_selectedItem=null;
		_contentLayer.transform.position=_carouselInitialPos;
		//_contentLayer.transform.position=new Vector3(0,0,_contentLayer.transform.position.z);
		_targetPos=_contentLayer.transform.position;
		
		
	}
	
	private int GetItemAtPoint(Vector3 pos){
		if (alignment == AlignmentMode.HORIZONTAL) {
			if((pos.y>bounds.min.y)&&(pos.y<bounds.max.y)){
				for(int i=0;i<_items.Count;i++){
					SB_CarouselDataItem tobj = (SB_CarouselDataItem) _items[i];
					if(tobj.isActive()){
						//Bounds tb = tobj.GetBounds();
						float xpos = tobj.transform.position.x;
						//float wid = ((tb.size.x+_gap)/2)*tobj.scale;
						//float wid = (((tb.size.x)/2)*tobj.scale)+(_gap/2);
						float wid=(tobj.GetCarouselWidth()/2.0f)*tobj.scale;
						if((pos.x>=xpos-wid)&&(pos.x<=xpos+wid)){
							
							return i;
						}
					}
				}
				
			}
		}
		
		return -1;
	}

	public ArrayList GetItems()
	{
		return _items;
	}
	
	
	public SB_CarouselDataItem GetItemAtIndex(int num)
	{
		if(_items != null)
		{
			if(num >= 0 && num < _items.Count)
			{
				return (SB_CarouselDataItem) _items[num];
			}
		}
		
		return null;
	}
	
	public SB_CarouselDataItem GetTapped(){
		
		if(_intialItem==_selectedItemNum){
			return GetSelected();
		}
		return null;
	}
	
	public SB_CarouselDataItem GetSelected(){
		if(_selectedItemNum>=0){
			if(_selectedItemNum<_items.Count){
				return (SB_CarouselDataItem) _items[_selectedItemNum];
			}
		}
		return null;
	}
	
	public bool IsInCarousel(Vector3 pos){
		if(_carouselHeight>-100000.0f){
			if(pos.y<_carouselHeight) return true;
		}
		else if ((pos.x > bounds.min.x) && (pos.x < bounds.max.x)) {
			if ((pos.y > bounds.min.y) && (pos.y < bounds.max.y)) {
				return true;
			}
		}
		return false;
	}
	
	private void ResetDrag(){
		while(_lastDrags.Count>0){
			_lastDrags.RemoveAt(0);
		}
	}

	private void ResetItem()
	{
		//log("ResetItem");

		_itemSelected=-1;
		_itemHold=0.0f;
		//_intialItem=-1;

		ResetItems();
	}

	public void ResetItems()
	{
		if(_centralItem != null)
		{
			// Reset item positions
			foreach(SB_CarouselDataItem cDataItem in _items)
			{
				cDataItem.transform.localPosition = cDataItem.defaultPos;
				cDataItem.transform.localScale = new Vector3(cDataItem.scale, cDataItem.scale, 1);
			}
		}
		_centralItem = null;
	}

	void MoveToPoint(GameObject obj, Vector3 pos, float spd){
		float dist = SB_Utils.GetDistance(obj.transform.position,pos);
		if(dist<spd*Time.deltaTime){
			obj.transform.position=new Vector3(pos.x,obj.transform.position.y,obj.transform.position.z);
		}
		else{
			/*float dx = pos.x-obj.transform.position.x;
			float dy = pos.y-obj.transform.position.y;
			float sp = spd*Time.deltaTime;
			obj.transform.position=new Vector3(obj.transform.position.x+((dx/dist)*sp),obj.transform.position.y+((dy/dist)*sp),obj.transform.position.z);
			float dx2 = pos.x-obj.transform.position.x;
			float dy2 = pos.y-obj.transform.position.y;*/
			
			float dx = pos.x-obj.transform.position.x;
			//float dy = pos.y-obj.transform.position.y;
			float sp = spd*Time.deltaTime;
			obj.transform.position=new Vector3(obj.transform.position.x+((dx/dist)*sp),obj.transform.position.y,obj.transform.position.z);
			float dx2 = pos.x-obj.transform.position.x;
			//float dy2 = pos.y-obj.transform.position.y;
			if(((dx2<0)&&(dx>0))||((dx<0)&&(dx2>0))){
				obj.transform.position=new Vector3(pos.x,obj.transform.position.y,obj.transform.position.z);
			}
			/*if(((dx2<0)&&(dx>0))||((dx<0)&&(dx2>0))||((dy2<0)&&(dy>0))||((dy<0)&&(dy2>0))){
				obj.transform.position=pos;
			}*/
		}
	}
	// Update is called once per frame
	void Update () {
		
		//PositionAtBase();
		
		//warn ("Update() Paused:" + Paused +", _mouseDown: " + _mouseDown + ", _movingToPoint: " + _movingToPoint + ", _dragable: " + _dragable + ", _returning: " + _returning);
		
		if(!Paused)
		{
			/*
			// GT
			if (Input.GetMouseButtonDown (0)) 
			{
				if(snapToClosestCentralItem)
				{
					_itemSnapDone = false;
				}
			}
			// END
			*/
			/*
			if(_scrollSpeed != 0 && snapToClosestCentralItem)
			{
				SB_CarouselDataItem centralItem = GetClosestCentralItem();
				warn("CENTRAL: " + centralItem.GetComponentInChildren<tk2dSprite>().CurrentSprite.name);
			}
			*/
			
			if(_returning){
				
			}
			else if(!_movingToPoint){
				bool moving = false;
				if(_resizing){
					//Debug.LogWarning("RESIZING");
					_resizing=false;
					for (int i=0; i<_items.Count; i++) {
						SB_CarouselDataItem tobj = (SB_CarouselDataItem) _items[i];
						//if(tobj.isActive()){
						if(alignment==AlignmentMode.HORIZONTAL){
							//if(tobj.targetPos.x>tobj.item.transform.position.x){
							/*if(tobj.item.transform.position.x!=tobj.targetPos.x){
								//tobj.item.transform.position= new Vector3(tobj.item.transform.position.x+Time.deltaTime*_snapSpeed,tobj.item.transform.position.y,tobj.item.transform.position.z);
								MoveToPoint(tobj.item,tobj.targetPos,_snapSpeed);
								//if(tobj.targetPos.x<=tobj.item.transform.position.x){
								if(tobj.item.transform.position.x==tobj.targetPos.x){
									tobj.item.transform.position = tobj.targetPos;
								}
								else{
									moving=true;
											_resizing=true;
								}
							}
							else */
							
							if(tobj.targetPos.x<tobj.item.transform.position.x){
								
								tobj.item.transform.position= new Vector3(tobj.item.transform.position.x-Time.deltaTime*_snapSpeed,tobj.item.transform.position.y,tobj.item.transform.position.z);
								
								if(tobj.targetPos.x>=tobj.item.transform.position.x){
									tobj.item.transform.position = new Vector3(tobj.targetPos.x,tobj.item.transform.position.y,tobj.item.transform.position.z);
								}
								else{
									moving=true;
									_resizing=true;
								}
							}
							else if(tobj.targetPos.x>tobj.item.transform.position.x){
								
								tobj.item.transform.position= new Vector3(tobj.item.transform.position.x+Time.deltaTime*_snapSpeed,tobj.item.transform.position.y,tobj.item.transform.position.z);
								
								if(tobj.targetPos.x<=tobj.item.transform.position.x){
									tobj.item.transform.position = new Vector3(tobj.targetPos.x,tobj.item.transform.position.y,tobj.item.transform.position.z);
								}
								else{
									moving=true;
									_resizing=true;
								}
							}
						}
						//}
					}
					
					//if(!moving){
					if(!_resizing){
						if (_contentLayer.transform.position != _targetPos) {
							if(alignment==AlignmentMode.HORIZONTAL){
								float mv=0.0f;
								if(_targetPos.x>_contentLayer.transform.position.x){
									_contentLayer.transform.position= new Vector3(_contentLayer.transform.position.x+Time.deltaTime*_snapSpeed,_contentLayer.transform.position.y,_contentLayer.transform.position.z);
									moving=true;
									_resizing=true;
									mv=0-Time.deltaTime*_snapSpeed;
									if(_targetPos.x<_contentLayer.transform.position.x){
										_contentLayer.transform.position = _targetPos;
									}
								}
								else if(_targetPos.x<_contentLayer.transform.position.x){
									_contentLayer.transform.position= new Vector3(_contentLayer.transform.position.x-Time.deltaTime*_snapSpeed,_contentLayer.transform.position.y,_contentLayer.transform.position.z);
									moving=true;
									_resizing=true;
									mv=Time.deltaTime*_snapSpeed;
									if(_targetPos.x>_contentLayer.transform.position.x){
										_contentLayer.transform.position = _targetPos;
									}
								}
								if(_resizing){
									for (int i=0; i<_items.Count; i++) {
										SB_CarouselDataItem tobj = (SB_CarouselDataItem) _items[i];
										//tobj.targetPos=new Vector3(tobj.targetPos.x-mv,tobj.targetPos.y,tobj.targetPos.z);
										tobj.targetPos=tobj.item.transform.position;
									}
								}
							}
							
						}
					}
				}
				else{
					//}
					
					//if (!moving) {
					Vector3 mousepos = Camera.main.ScreenToWorldPoint (UnityEngine.Input.mousePosition);
					
					//if(_dragable){
					if(_dragable && IsInCarousel(mousepos))
					{
						if (Input.GetMouseButtonDown (0)) 
						{

							if(snapToClosestCentralItem)
							{
								_itemSnapDone = false;
							}

						

							_mouseDown=true;
							ResetDrag();

							ResetItem ();
							_lastDrags.Add(mousepos);
							_scrollSpeed=0.0f;
							_intialItem= GetItemAtPoint (mousepos);
							_initialPos=mousepos;
							
							
						} else if (Input.GetMouseButtonUp (0)) {
							//Vector3 mousepos = Camera.main.ScreenToWorldPoint (UnityEngine.Input.mousePosition);
							/*int inum = GetItemAtPoint (mousepos);
					if (inum >= 0) {
						RemoveItemAt (inum);
					}
					*/
							_mouseDown=false;
							SB_Tweener.StopAllTweens();
						}
						_dragging=false;
						if(_mouseDown){
							if(IsInCarousel(mousepos)){
								if(alignment==AlignmentMode.HORIZONTAL){
									if(_scrollable){
										//if(_lastDrags.Count>=_dragSensitivity){
										/*	Vector3 lastDrag = (Vector3) _lastDrags[0];
							float vx = mousepos.x-lastDrag.x;
										//float dragDist = 100.0f;//bounds.size.x*30;
										float ps = vx/_dragDist;//(dragDist*Time.deltaTime);
							if(ps>1.0f) ps = 1.0f;
							else if(ps<-1.0f) ps=-1.0f;
										_scrollSpeed=ps*_scrollMax;*/
										Vector3 lastDrag = (Vector3) _lastDrags[_lastDrags.Count-1];
										float vx = mousepos.x-lastDrag.x;
										_scrollSpeed= vx;//
									}
									//}
								}
								
								_lastDrags.Add(mousepos);
								if(_lastDrags.Count>_dragSensitivity){
									_lastDrags.RemoveAt(0);
									
								}
								
								float dist = SB_Utils.GetDistance(mousepos,_initialPos);
								if(dist>isScrollingDist){
									_intialItem=-1;
								}
								_dragging=true;
								
								int inum = GetItemAtPoint (mousepos);
								if (inum >= 0) {
									if(inum==_itemSelected){
										_itemHold+=Time.deltaTime;
										if(_itemHold>=_itemHoldRate){
											
											SelectItem(_itemSelected);
										}
									}
									else{
										_itemSelected=inum;
									}
								}
								else
								{
									ResetItem();
									_intialItem=-1;
								}
							}
							else{
								_intialItem=-1;
							}
							
							
						}
					}
					if(!_dragging){
						if(_scrollSpeed>0){
							//warn ("----------->");

							_scrollSpeed-=Time.deltaTime*_scrollDecel;
							if(_scrollSpeed<0)
							{
								_scrollSpeed=0;
								//warn ("FINISHED (a)");
							}
						}
						else if(_scrollSpeed<0){
							//warn ("<-----------");

							_scrollSpeed+=Time.deltaTime*_scrollDecel;
							if(_scrollSpeed>0)
							{
								_scrollSpeed=0;
								//warn ("FINISHED (b)");
							}
						}
					}

					//warn ("_scrollSpeed " + _scrollSpeed);
					
					if(_scrollSpeed!=0)
					{
						float clpos = _contentLayer.transform.position.x;//+bounds.center.x;
						float minX = bounds.min.x;
						float maxX = (0-_carouselMax)+bounds.max.x;

						if(scrollLimits.enabled)
						{
							minX = scrollLimits.limitX;
							maxX = -(_carouselMax-scrollLimits.limitX);
						}

						/*
						float minX = 0;
						float maxX = -_carouselMax;
						*/

						if(clpos+_scrollSpeed>minX){
							_scrollSpeed*=-_bounce;
								_contentLayer.transform.position= new Vector3(minX,_contentLayer.transform.position.y,_contentLayer.transform.position.z);

							}
						else if(clpos+_scrollSpeed<maxX){
							_scrollSpeed*=-_bounce;
								_contentLayer.transform.position= new Vector3(maxX,_contentLayer.transform.position.y,_contentLayer.transform.position.z);
							
						}
						else{
							//_contentLayer.transform.position= new Vector3(_contentLayer.transform.position.x+Time.deltaTime*_scrollSpeed,_contentLayer.transform.position.y,_contentLayer.transform.position.z);
							_contentLayer.transform.position= new Vector3(_contentLayer.transform.position.x+_scrollSpeed,_contentLayer.transform.position.y,_contentLayer.transform.position.z);
							
						}
						for (int i=0; i<_items.Count; i++) {
							SB_CarouselDataItem tobj = (SB_CarouselDataItem) _items[i];
							tobj.targetPos=tobj.item.transform.position;
						}
						
						_targetPos=_contentLayer.transform.position;
					}

					else
					{
						//error("ScrollToItemClosestToCenter _scrollSpeed: " + _scrollSpeed + ", _mouseDown: " + _mouseDown + ", snapToClosestCentralItem: " + snapToClosestCentralItem + ", _itemSnapDone: " + _itemSnapDone + ", _centralItem: " + _centralItem);
						if(snapToClosestCentralItem) BeginScrollToCentralItem();
					}

					
				}
			}
			else{	//SCROLLING TO A POSTION
				_movingToPoint=DoScrolling();
			}
		}
	}

	void BeginScrollToCentralItem ()
	{
		// Scroll speed is zero, snap to closest item
		if(_scrollSpeed == 0 && !_mouseDown && snapToClosestCentralItem && !_itemSnapDone)
		{
			//log("SNAPPING TO CLOSEST ITEM");
			
			ScrollToItemClosestToCenter();
			_itemSnapDone = true;
		}
	}

	public SB_CarouselDataItem GetClosestCentralItem()
	{
		SB_CarouselDataItem closestCentralItem = null;

		Vector3 center = transform.position;
		Vector3 targetPos = center;
		float shortestDist = 9999999f;
		
		foreach(SB_CarouselDataItem cDataItem in _items)
		{
			GameObject g = cDataItem.gameObject;
			float dist = SB_Utils.GetDistance(g.transform.position, center);
			
			if(dist < shortestDist)
			{
				shortestDist = dist;
				closestCentralItem = cDataItem;
			}
			
			//warn(g.name + ", dist: " + dist);
		}
		
		//warn(closestCentralItem.GetComponentInChildren<tk2dSprite>().CurrentSprite.name + ", shortestDist: " + shortestDist);

		return closestCentralItem;
	}
	
	void ScrollToItemClosestToCenter()
	{
		//warn("DoScrollToItemClosestToCenter");

		if(_centralItem != null) return;

		Vector3 targetPos = transform.position;
		_centralCarouselDataItem = GetClosestCentralItem();
		_centralItem = _centralCarouselDataItem.gameObject;

		tk2dSprite tk2dSpr = _centralItem.GetComponentInChildren<tk2dSprite>();
		string sprName = "custom";
		if(tk2dSpr != null) sprName = tk2dSpr.CurrentSprite.name;
		//log("----------- CENTRAL ITEM:" + sprName);

		targetPos.x = _centralItem.transform.parent.position.x - _centralItem.transform.position.x;
		float duration = 0.5f;
		ScrollToPoint(targetPos, duration);
		//log ("targetpos: " + targetPos);

		// Reposition other items, to left and right of central item
		bool foundCentralItem=false;
		int n = 0;
		int i = _items.Count;
		foreach(SB_CarouselDataItem cDataItem in _items)
		{
			i--;
			if(cDataItem == _centralCarouselDataItem)
			{
				//log ("CENTRAL item [" + n + "] current position: " + cDataItem.transform.position + ", localPosition: " + cDataItem.transform.localPosition+ ", defaultPos: " + cDataItem.defaultPos + ", position: " + cDataItem.position);
				foundCentralItem = true;
			}
			else
			{
				Vector3 newPosition = cDataItem.transform.position;
				//log ("item [" + n + "] current position: " + cDataItem.transform.position + ", defaultPos: " + cDataItem.defaultPos + ", _gap: " + _gap + ", _itemWidth: " + _itemWidth + ", position: " + cDataItem.position);
				if(!foundCentralItem)
				{
					// shift left
					newPosition.x -= _gap*2;
				}
				else
				{
					// shift right
					newPosition.x += _gap*2;
				}

				//log ("item [" + n + "] current position: " + cDataItem.transform.position + ", defaultPos: " + cDataItem.defaultPos + ", newPosition.x: " + newPosition.x +", _gap: " + _gap + ", _itemWidth: " + _itemWidth + ", position: " + cDataItem.position + ", carousel x: " + _centralItem.transform.parent.position.x);

				SB_Tweener.RemoveAllTweensOf(cDataItem.gameObject);
				SB_Tweener.TweenPosition(cDataItem.gameObject, duration*0.25f, newPosition);
			}

			n++;
		}
	}

	private bool DoScrolling()
	{
		float stime = Time.time-_startTime;
		
		
		
		float posx =0.0f;
		float posy =0.0f;
		
		
		float enddistx = _endPos.x-_startPos.x;
		float enddisty = _endPos.y-_startPos.y;
		stime/=_endTime/2;
		if(stime<1){
			posx = (enddistx/2)*stime*stime+_startPos.x;
			posy = (enddisty/2)*stime*stime+_startPos.y;
		}
		else{
			stime--;
			posx = -(enddistx/2)*(stime*(stime-2)-1)+_startPos.x;
			posy = -(enddisty/2)*(stime*(stime-2)-1)+_startPos.y;
		}
		
		if(Mathf.Abs(posx) == Mathf.Infinity || float.IsNaN(posx)) posx = 0f;
		if(Mathf.Abs(posy) == Mathf.Infinity || float.IsNaN(posy)) posy = 0f;
		
		_contentLayer.transform.position = new Vector3(posx,posy,_contentLayer.transform.position.z);
		
		if(Time.time>=_startTime+_endTime){
			
			_contentLayer.transform.position=_endPos;
			CarouselMove();

			if(snapToClosestCentralItem) BeginScrollToCentralItem();

			return false;
		}
		return true;
	}
	
	public void ScrollToPoint(int pos, float time)
	{
		ScrollToPoint(GetPosition(pos), time);
	}

	public void ScrollToPoint(Vector3 pos, float time)
	{
		//log ("----------------------------------");
		//log ("ScrollToPoint - targetPos: " + pos + ", _scrollable: " + _scrollable);
		//log ("----------------------------------");

		if(_scrollable){
			_movingToPoint=true;
			_startPos=_contentLayer.transform.position;
			_endPos = pos;
			_startTime=Time.time;
			_endTime = time;
			
		}
		else{
			_movingToPoint=false;
		}
		
	}
	
	public void ScrollFromTo(int spos, int epos, float time){
		
		if(_scrollable){
			_movingToPoint=true;
			_startPos=GetPosition(spos);
			_contentLayer.transform.position=_startPos;
			_endPos = GetPosition(epos);
			_startTime=Time.time;
			_endTime = time;
			
		}
		else{
			_movingToPoint=false;
		}
		
	}
	
	public void ScrollOn(int epos, float time){
		//if(_scrollable){
		_movingToPoint=true;
		if(epos==POSITION_LEFT)
		{
			_startPos=new Vector3((0-(_carouselMax+_carouselWidth)),_contentLayer.transform.position.y,_contentLayer.transform.position.z);
		}
		else if(epos==POSITION_RIGHT){
			_startPos=new Vector3(bounds.max.x,_contentLayer.transform.position.y,_contentLayer.transform.position.z);
		}
		else if (epos==POSITION_CENTER){
			_startPos=new Vector3((0-(_carouselMax+_carouselWidth)),_contentLayer.transform.position.y,_contentLayer.transform.position.z);
		}
		
		if(_scrollable) _endPos = GetPosition(epos);
		else _endPos = new Vector3(bounds.center.x-_carouselWidth/2,_contentLayer.transform.position.y,_contentLayer.transform.position.z);
		
		
		_contentLayer.transform.position=_startPos;
		_startTime=Time.time;
		_endTime = time;
		
		/*}
		else{
			_movingToPoint=false;
		}*/
		
	}
	
	public void PlaceAtPoint(int pos){
		
		//if(_scrollable){
		
		_contentLayer.transform.position=GetPosition(pos);
		_targetPos=_contentLayer.transform.position;
		CarouselMove();
		//}
		
		
	}

	public void PlaceAtXPos(float pos){
		
		//if(_scrollable){
		
		_contentLayer.transform.position=new Vector3(pos,_contentLayer.transform.position.y,_contentLayer.transform.position.z);
		_targetPos=_contentLayer.transform.position;
		CarouselMove();
		//}
		
		
	}
	
	public Vector3 GetPosition(int pos)
	{
		//log("GetPosition pos: " + pos + ", bounds.max.x: " + bounds.max.x + ", bounds.min.x: " + bounds.min.x + ", _carouselWidth: " + _carouselWidth + ", _carouselMin " + _carouselMin + ", _carouselMax: " + _carouselMax);
		
		switch(pos)
		{
		case POSITION_LEFT:
			return new Vector3(bounds.min.x,_contentLayer.transform.position.y,_contentLayer.transform.position.z);
			break;
			
		case POSITION_RIGHT:
			return new Vector3((0-_carouselMax)+bounds.max.x,_contentLayer.transform.position.y,_contentLayer.transform.position.z);
				break;

		case POSITION_CENTER:
			return new Vector3(0 - _itemWidth, _contentLayer.transform.position.y,_contentLayer.transform.position.z);
			break;
			
		case POSITION_OFF_LEFT:
			return new Vector3((0-_carouselMax)+bounds.max.x,_contentLayer.transform.position.y,_contentLayer.transform.position.z);
			
			break;
			
		case POSITION_OFF_RIGHT:
			return new Vector3(bounds.max.x,_contentLayer.transform.position.y,_contentLayer.transform.position.z);
			
			break;
		}
		
		
		
		return _contentLayer.transform.position;
		
	}
	
	public void UnselectItem()
	{
		if(_selectedItem == null) return;

		if(_selectedItemNum>=0){
			SB_CarouselDataItem tobjold = (SB_CarouselDataItem) _items[_selectedItemNum];
			_selectedItem.transform.localScale=new Vector3(tobjold.scale,tobjold.scale,tobjold.scale);
		}
		_selectedItemNum=-1;
		_selectedItem=null;
		//_intialItem=-1;

		// GT
		ResetItem();
	}

	void SelectItem(int num)
	{	
		UnselectItem();
		
		SB_CarouselDataItem tobj = (SB_CarouselDataItem) _items[num];
		if(!tobj.locked)
		{
			if(tobj.isSelectable) // GT
			{
				_selectedItemNum=num;
				_selectedItem = tobj.item;
				if(_selectedItem.transform.localScale.x==tobj.scale){
					_selectedItem.transform.localScale=new Vector3(tobj.scale*_selectScale,tobj.scale*_selectScale,tobj.scale*_selectScale);
				}
			}

			NotifyItemDrag();

			ResetItem();
			
			if(_selectedItemNum!=_intialItem){
				_intialItem=-1;
			}
		}
	}
	
	public void ReturnItem(int id){
		
	}
	
	public void Disable(){
		_dragable=false;
		_dragging=false;
		UnselectItem();
	}
	
	public void Enable(){
		_dragable=true;
		_dragging=false;
		_mouseDown = false;
		_scrollable=true;
		if (_carouselWidth <= bounds.size.x) {
			_scrollable = false;
		}
		
	}
	
	
	
	
	void NotifyItemSelect() 
	{
		if ( _selectedItem != null )
		{
			if( OnSelectItem != null )
			{
				OnSelectItem(_selectedItem);
			}
		}
	}
	
	void NotifyItemDrag() 
	{
		if( _selectedItem != null ) 
		{
			if( OnDragItem != null )
			{
				OnDragItem(_selectedItem);
			}
		}
	}
	
	void OnDestroy()
	{
		ClearCarousel();
		
		_items = null;
		_contentLayer = null;
		_lastDrags = null;
		_selectedItem = null;
		
		OnSelectItem = null;
		OnDragItem = null;
		//log ("CAROUSEL REMOVED");
	}
}
