﻿using UnityEngine;
using System.Collections;
using System;

public class SB_CarouselDataItem : MonoBehaviour 
{
	public object data = null;
	public string objectRef = ""; // the name of the object this represents.
	public int objectId = 0;	//ID of the object
	public int position;
	public bool visible=true;
	public bool active=true;
	public bool isSelectable=true;
	public bool locked=false;
	public GameObject item, boundsItem;
	public bool forceScaleInCarousel = false;
	public bool resizeToCarouselHeight = false;
	public float scale=1.0f;
	
	public Vector3 defaultPos, targetPos;

	private Bounds bounds = new Bounds(new Vector3(0,0,0),new Vector3(100.0f,100.0f,1.0f));
	private bool _boundsSet = false;
	
	private float _cwidth = 0;

	//public string oldParent = "";
	// Update is called once per frame
	void Update () {
		
	}

	public void SetCarouselWidth(float wid){
		_cwidth=wid;
	}

	public float GetCarouselWidth(){
		return _cwidth;
	}
	
	public bool isActive(){
		if(!active) return false;
		if (!visible) return false;
		return true;
	}

	public void SetBounds(Bounds tb){
		bounds = tb;
		_boundsSet=true;

	}

	public Bounds GetBounds(){

		if(_boundsSet) return bounds;

		tk2dBaseSprite bsprite = gameObject.GetComponent<tk2dBaseSprite>();
		if(item != null) bsprite = item.GetComponent<tk2dBaseSprite>();
		if(boundsItem != null) bsprite = boundsItem.GetComponent<tk2dBaseSprite>();

		if(bsprite!=null){
			return bsprite.GetBounds();
		}

		return bounds;

	}



	protected void OnDestroy()
	{
		//Debug.LogWarning("REMOVED CAROUSEL ITEM "+objectRef);
		item = null;
		data = null;
	}
}
