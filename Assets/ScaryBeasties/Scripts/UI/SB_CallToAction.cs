﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Data;
using ScaryBeasties.Sound;
using ScaryBeasties;
using ScaryBeasties.Utils;

public class SB_CallToAction : MonoBehaviour
{

	private float _alpha;
	private string _url;
	private float _speed = 2f;
	bool _isFading;
	tk2dSprite _sprite;


	// Use this for initialization
	void Start ()
	{
		_sprite = transform.Find ("Sprite").GetComponent<tk2dSprite> ();
		_alpha = 0;
		_sprite.color = new Color (1, 1, 1, _alpha);
		_isFading = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (_isFading && _sprite.color.a < 1f) {
			_alpha += Time.deltaTime * _speed;
			_sprite.color = new Color (1, 1, 1, _alpha);
		} else
			_isFading = false;
	}

	public void Show ()
	{
		_isFading = true;
	}

	public KeyValueObject GetKeyValueObject ()
	{
		KeyValueObject kvObject = new KeyValueObject ();
		kvObject.SetKeyValue ("name", "Webview_MoreGames");
		kvObject.SetKeyValue ("url", SB_Utils.GetNodeValueFromConfigXML ("config/links/link", "parents_more_games"));
		kvObject.SetKeyValue ("url_offline", SB_Utils.GetNodeValueFromConfigXML ("config/links/link", "parents_more_games_offline"));
		SB_SoundManager.instance.ClearVOQueue ();
		return kvObject;
	}
}
