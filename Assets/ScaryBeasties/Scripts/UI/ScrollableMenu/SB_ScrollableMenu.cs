﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ScaryBeasties.Utils;
using System;
using ScaryBeasties.Cam;

namespace ScaryBeasties.UI.ScrollableMenu
{
    public enum VerticalAlignmentOptions
    {
        Top,
        Middle,
        Bottom
    }

    public enum HorziontalAlignmentOptions
    {
        Left,
        Middle,
        Right
    }

    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(ScrollRect))]
    [RequireComponent(typeof(Image))]
    [RequireComponent(typeof(Mask))]
    public class SB_ScrollableMenu : MonoBehaviour
    {
        public const float MENU_POSITION_TOP = 1f;
        public const float MENU_POSITION_BOTTOM = 0f;
        public const float MENU_POSITION_CENTER = 0.5f;
        public const float MENU_POSITION_LEFT = -1f;
        public const float MENU_POSITION_RIGHT = 1f;

        public GameObject ContentHolder;
        public SB_Bounds Bounds;
        public int ScrollTolerance = 20;    // How many px can user scroll, before the input is ignored
        public bool ScaleContentsToFit = false;
        public int Padding = 0;

        private bool _mouseDown = false;
        private Vector3 _mouseDownPos; // Store mouse down position, whenever an item is pressed
        private bool _doMouseReleasedCallback = true;

        // The rect transform for this GameObject
        private RectTransform _rectTransform;
        private ScrollRect _scrollRect;

        // The rect transform for the content holder GameObject
        private RectTransform _contentHolderRectTransform;

        private float _scrollToTargetPos = 0;
        public float _scrollSpeed = 2f;
        //public float _scrollSpeedMultiplier = 1f;

        public VerticalAlignmentOptions VerticalAlignment;
        public HorziontalAlignmentOptions HorizontalAlignment;

        private const int STATE_IDLE = 0;
        private const int STATE_HORIZIONTAL_SCROLL_TO_POSITION = 1;
        private const int STATE_VERTICAL_SCROLL_TO_POSITION = 2;
        private const int STATE_SCROLLING_TO_NORMALISED_POSITION = 3;
        private const int STATE_DRAGGING = 4;
        private int _state = STATE_IDLE;

        public ArrayList Items;

        public Action OnStartDragCallback;
        public Action<SB_ScrollableMenuItem> OnPressCallback;
        public Action<SB_ScrollableMenuItem> OnReleaseCallback;
        public Action<int> OnItemRemovedCallback;
        public Action OnItemThumbsLoaded;

        // Getters
        public float Width { get { return _rectTransform.rect.width; } }
        public float Height { get { return _rectTransform.rect.height; } }
        public RectTransform RectTransform { get { return _contentHolderRectTransform; } }
        public ScrollRect ScrollRect { get { return _scrollRect; } }

        public float ContentHolderWidth { get { return _contentHolderRectTransform.rect.width; } }
        public float ContentHolderHeight { get { return _contentHolderRectTransform.rect.height; } }
        public RectTransform ContentHolderRectTransform { get { return _contentHolderRectTransform; } }

        public float ContentHolderTop { get { return -(Mathf.Ceil(ContentHolderHeight - Height) * 0.5f); } }
        public float ContentHolderBottom { get { return (Mathf.Ceil(ContentHolderHeight - Height) * 0.5f); } }
        public float ContentHolderLeft { get { return (Mathf.Ceil(_contentHolderRectTransform.rect.x + ContentHolderWidth)); } }

        private bool _canvasReady = false;

        private int _thumbLoadedCount = 0;
        private int _startThumbIndex = 0;
        private int _numThumbsToLoad = 0;

        //TAP TESTING
        private float _tapStart = 0;
        private const float TAP_TIME_TOLERANCE = 0.5f;
        private Vector2 _tapPosition;
        private const float TAP_DIST_TOLERANCE = 10.0f;

        private const float SCROLLING_UPDATE_MAX = 0.15f;
        private float _scrollingUpdateCounter = SCROLLING_UPDATE_MAX;

        private int _scrollLimit = 2500;

        void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
            _scrollRect = GetComponent<ScrollRect>();

            _contentHolderRectTransform = ContentHolder.transform.GetComponent<RectTransform>();

            Items = new ArrayList();

            //_scrollRect.StopMovement();
        }

        void Start()
        {
            StartCoroutine(WaitThenResizeCanvas());
        }

        IEnumerator WaitThenResizeCanvas()
        {
            yield return new WaitForEndOfFrame();
            _canvasReady = true;
            ResizeCanvas();
        }

        public void Enable()
        {
            _scrollRect.enabled = true;

            for (int n = 0; n < Items.Count; n++)
            {
                SB_ScrollableMenuItem itm = (SB_ScrollableMenuItem)Items[n];
                itm.enabled = true;
            }
        }

        public void Disable()
        {
            _scrollRect.enabled = false;

            for (int n = 0; n < Items.Count; n++)
            {
                SB_ScrollableMenuItem itm = (SB_ScrollableMenuItem)Items[n];
                itm.enabled = false;
            }
        }

        public float GetContentHolderScaler()
        {
            if (Items == null || Items.Count == 0) return 1f;

            SB_ScrollableMenuItem item = GetItemAt(0);
            if (item != null)
            {
                float scaleAmount = 1f;
                if (_scrollRect.horizontal)
                {
                    scaleAmount = Height / (item.Height + Padding);
                    Debug.LogWarning("Canvas Height: " + Height + ", item height: " + item.Height + ", scaleAmount: " + scaleAmount);
                }
                else if (_scrollRect.vertical)
                {
                    scaleAmount = Width / (item.Width + Padding);
                    Debug.LogWarning("Canvas Width: " + Width + ", item Width: " + item.Width + ", scaleAmount: " + scaleAmount);
                }

                return scaleAmount;
            }

            return 1f;
        }

        /**
		 * Resize the rect transform, to match the Bounds
		 */
        void ResizeCanvas()
        {
            if (!_canvasReady) return;

            if (Bounds != null)
            {
                _rectTransform.sizeDelta = new Vector2(0, Bounds.Height);
                _rectTransform.localPosition = Bounds.Center;

                // Set the parent canvas size to match the width & height of the Bounds
                RectTransform canvasRect = transform.parent.GetComponent<RectTransform>();
                canvasRect.sizeDelta = new Vector2(Bounds.Width, Bounds.Height);

                // TODO: Check this hasn't broken anything!
                _contentHolderRectTransform.localPosition = Vector2.zero;
            }

            Debug.LogWarning("<color=green>---Bounds.Width: " + Bounds.Width + ", canvas w: " + Width + ", h: " + Height + "</color>");


            if (ScaleContentsToFit)
            {
                Debug.LogWarning("---Scaling contents to fit");
                SB_ScrollableMenuItem item = GetItemAt(0);
                if (item != null)
                {
                    Vector3 newScale = _contentHolderRectTransform.localScale;
                    newScale.x = GetContentHolderScaler();
                    newScale.y = GetContentHolderScaler();
                    _contentHolderRectTransform.localScale = newScale;
                }
            }

            Vector2 contentHolderPivot = _contentHolderRectTransform.pivot;
            if (_scrollRect.horizontal)
            {
                if (Items.Count == 1) contentHolderPivot.x = 0.5f; // Align to center
                else
                {
                    if (HorizontalAlignment == HorziontalAlignmentOptions.Left) contentHolderPivot.x = 0f;
                    else if (HorizontalAlignment == HorziontalAlignmentOptions.Right) contentHolderPivot.x = 1f;
                    else if (HorizontalAlignment == HorziontalAlignmentOptions.Middle) contentHolderPivot.x = 0.5f;
                }
            }
            if (_scrollRect.vertical)
            {
                if (Items.Count == 1) contentHolderPivot.y = 0.5f; // Align to center
                else
                {
                    if (VerticalAlignment == VerticalAlignmentOptions.Top) contentHolderPivot.x = 0f;
                    else if (VerticalAlignment == VerticalAlignmentOptions.Bottom) contentHolderPivot.x = 1f;
                    else if (VerticalAlignment == VerticalAlignmentOptions.Middle) contentHolderPivot.x = 0.5f;
                }
            }
            _contentHolderRectTransform.pivot = contentHolderPivot;
        }

        public void AddItem(SB_ScrollableMenuItem item, int index = -1)
        {
            if (Items == null)
            {
                Items = new ArrayList();
            }

            Items.Add(item);
            item.transform.SetParent(ContentHolder.transform);

            if (index > -1)
            {
                item.transform.SetSiblingIndex(index);
            }

            ResizeCanvas();
        }

        void OnMouseDown()
        {
            if (!MouseReleasedInsideBounds())
            {
                //Log("OnMouseDown (OUTSIDE OF BOUNDS!)");
                return;
            }

            if (!_scrollRect.enabled)
            {
                return;
            }

            //Log("OnMouseDown");

            _mouseDown = true;
            _state = STATE_DRAGGING;
            if (OnStartDragCallback != null) OnStartDragCallback();

            _mouseDownPos = Camera.main.ScreenToWorldPoint(UnityEngine.Input.mousePosition);
            _doMouseReleasedCallback = true;

            _tapStart = Time.time;
            _tapPosition = new Vector2(_mouseDownPos.x, _mouseDownPos.y);

            SB_ScrollableMenuItem menuItem = GetMenuItemAtPoint(_mouseDownPos);

            if (menuItem != null)
            {
                Log("menuItem " + menuItem.id);
                // Call the delgate
                if (OnPressCallback != null) OnPressCallback(menuItem);
            }
            else
            {
                Log("menuItem is NULL");
            }
        }

        private bool IsTap(Vector2 pos)
        {

            if (Time.time - _tapStart > TAP_TIME_TOLERANCE)
                return false;

            if (Vector2.Distance(_tapPosition, pos) > TAP_DIST_TOLERANCE)
                return false;

            return true;

        }

        void OnMouseUp()
        {
            //Log("OnMouseUp");

            _mouseDown = false;
            _state = STATE_IDLE;

            if (!_doMouseReleasedCallback || !MouseReleasedInsideBounds()) return;


            if (!_scrollRect.enabled)
            {
                return;
            }

            Vector2 mousePos = Camera.main.ScreenToWorldPoint(UnityEngine.Input.mousePosition);

            if (!IsTap(mousePos))
                return;

            SB_ScrollableMenuItem menuItem = GetMenuItemAtPoint(mousePos);

            if (menuItem != null)
            {
                Debug.LogWarning("HandleOnMenuItemReleased - menuItem " + menuItem.id);

                // Call the delgate
                if (OnReleaseCallback != null) OnReleaseCallback(menuItem);
            }
            else
            {
                Log("menuItem is NULL");
            }
        }

        /**
		 * Has mouse been released within the scroll bounds?
		 */
        bool MouseReleasedInsideBounds()
        {
            Vector2 mouseUpPos = Camera.main.ScreenToWorldPoint(UnityEngine.Input.mousePosition);
            if (Bounds != null)
            {
                //Debug.LogWarning("---> mouseUpPos: "+ mouseUpPos + ", Bounds.Bounds: " + Bounds.Bounds);
                return Bounds.Contains(mouseUpPos);
            }

            return false;
        }

        /**
		 * Returns the SB_ScrollableMenuItem at the point where the user pressed
		 */
        SB_ScrollableMenuItem GetMenuItemAtPoint(Vector2 pos)
        {
            Log("------------------------------------------------------");
            Log("GetMenuItemAtPoint() pos: " + pos);

            for (int n = 0; n < Items.Count; n++)
            {
                SB_ScrollableMenuItem itm = (SB_ScrollableMenuItem)Items[n];

                Log("itm[" + n + "] " + itm.Area());

                if (itm.HitTest(_mouseDownPos))
                {
                    Log("PRESSED " + itm.id);
                    return itm;
                }
            }

            return null;
        }

        /**
		 * Reposition the scroll rect, so the contents are top/bottom.
		 * This is run via a coroutine, which waits for the end of the frame, before repositioning.
		 */
        public void SetContentsNormalisedPosition(float pos)
        {
            StartCoroutine(WaitThenSetContentsNormalisedPosition(pos));
        }

        IEnumerator WaitThenSetContentsNormalisedPosition(float pos)
        {
            yield return new WaitForEndOfFrame();

            if (_scrollRect.vertical) _scrollRect.verticalNormalizedPosition = pos;
            if (_scrollRect.horizontal) _scrollRect.horizontalNormalizedPosition = pos;
        }

        public void ScrollToNormalisedPosition(float startPos, float endPos)
        {
            SetContentsNormalisedPosition(startPos);
            _scrollToTargetPos = endPos;

            _state = STATE_SCROLLING_TO_NORMALISED_POSITION;
        }

        public void ScrollToNormalisedPosition(float endPos)
        {
            _scrollToTargetPos = endPos;
            //_scrollSpeedMultiplier = 1f;

            _state = STATE_SCROLLING_TO_NORMALISED_POSITION;
        }
        /*
		public void SnapTo(RectTransform target)
		{
			Canvas.ForceUpdateCanvases();
			
			ContentHolderRectTransform.anchoredPosition = (Vector2)_scrollRect.transform.InverseTransformPoint(ContentHolderRectTransform.position) - (Vector2)_scrollRect.transform.InverseTransformPoint(target.position);
		}
		*/

        public void HorizontalScrollTo(float endPos)
        {
            _scrollToTargetPos = endPos;
            _state = STATE_HORIZIONTAL_SCROLL_TO_POSITION;
        }

        public void VerticalScrollTo(float endPos)
        {
            _scrollToTargetPos = endPos;
            _state = STATE_VERTICAL_SCROLL_TO_POSITION;
        }

        /**
		 * How far can items be scrolled, before being unloaded?
		 */
        public int ScrollLimit
        {
            set { _scrollLimit = value; }
            get { return _scrollLimit; }
        }

        void Update()
        {
            if (!SB_UIControls.instance.IsTouchingUIButton())
            {
                CheckInput();
            }

            //Debug.LogWarning("Update - _state: " + _state + ", _mouseDown: " + _mouseDown);
            //Debug.LogWarning("Update - _state: " + _state + ", _scrollRect.velocity: " + _scrollRect.velocity);

            UpdateScrollingCounter();

            switch (_state)
            {
                case STATE_IDLE:
                    //UpdateIdle();
                    break;

                case STATE_SCROLLING_TO_NORMALISED_POSITION:
                    UpdateScrollToNormalisedPosition();
                    break;

                case STATE_HORIZIONTAL_SCROLL_TO_POSITION:
                    UpdateHorizontalScrollToPosition();
                    break;

                case STATE_VERTICAL_SCROLL_TO_POSITION:
                    UpdateVerticalScrollToPosition();
                    break;

                case STATE_DRAGGING:
                    UpdateDragging();
                    break;
            }
        }

        void CheckInput()
        {

            if (!_mouseDown && UnityEngine.Input.GetMouseButtonDown(0))
            {
                OnMouseDown();
            }
            else if (_mouseDown && UnityEngine.Input.GetMouseButtonUp(0))
            {
                OnMouseUp();
            }
        }

        //public void UpdateScrollingCounter(bool forceCheck=false)
        void UpdateScrollingCounter()
        {
            _scrollingUpdateCounter -= Time.deltaTime;
            if (_scrollingUpdateCounter <= 0f)
            {
                //Debug.LogWarning("_scrollRect.velocity.x " + _scrollRect.velocity.x);

                //_scrollingUpdateCounter = SCROLLING_UPDATE_MAX;

                // While menu is scrolling, get the menu items which are within view
                //if(_scrollRect.velocity.x != 0f || _scrollRect.velocity.y != 0f || forceCheck)
                //{
                if (Items != null)
                {
                    for (int n = 0; n < Items.Count; n++)
                    {
                        SB_ScrollableMenuItem item = (SB_ScrollableMenuItem)Items[n];

                        if (item != null)
                        {
                            Vector3 itemWorldPos = Camera.main.ScreenToWorldPoint(item.transform.position);
                            //Debug.LogWarning("ITEM ["+n+"] position: " + item.transform.position + ", itemWorldPos: " + itemWorldPos);

                            float pos = item.transform.position.x;
                            if (_scrollRect.vertical)
                            {
                                pos = item.transform.position.y;
                            }

                            if (pos < _scrollLimit / 2 && pos > -_scrollLimit / 2)
                            {
                                //Debug.LogWarning("--> ABOUT TO LOAD ITEM " + item.index);
                                if (!item.ImageIsLoaded)
                                {
                                    item.LoadImage();
                                    break;
                                }
                            }
                            else
                            {
                                if (item.ImageIsLoaded)
                                {
                                    item.UnloadImage();
                                }

                            }
                        }
                    }
                }
                //}
            }
        }

        void UpdateScrollToNormalisedPosition()
        {
            /*
			if(_scrollRect.verticalNormalizedPosition > _scrollToTargetPos)
			{
				_scrollRect.velocity = new Vector2(0f, -400f);
				if(_scrollRect.verticalNormalizedPosition <= _scrollToTargetPos)
				{
					_scrollRect.verticalNormalizedPosition = _scrollToTargetPos;
					_state = STATE_IDLE;
				}
			}
			else if(_scrollRect.verticalNormalizedPosition < _scrollToTargetPos)
			{
				_scrollRect.velocity = new Vector2(0f, 400f);
				if(_scrollRect.verticalNormalizedPosition >= _scrollToTargetPos)
				{
					_scrollRect.verticalNormalizedPosition = _scrollToTargetPos;
					_state = STATE_IDLE;
				}
			}
			return;
			*/
            //-------------------------------------

            //Log("UpdateScrollToNormalisedPosition() verticalNormalizedPosition: " + _scrollRect.verticalNormalizedPosition + ", horizontalNormalizedPosition: " + _scrollRect.horizontalNormalizedPosition +  ", _scrollToTargetPos: " + _scrollToTargetPos);

            float moveAmount = _scrollSpeed * Time.deltaTime;
            /*
			_scrollSpeedMultiplier *= 0.9995f;
			if(moveAmount < 0.01f) moveAmount = 0.01f;
			*/

            if (_scrollRect.vertical)
            {
                if (_scrollRect.verticalNormalizedPosition > _scrollToTargetPos)
                {
                    _scrollRect.verticalNormalizedPosition -= moveAmount;
                    if (_scrollRect.verticalNormalizedPosition <= _scrollToTargetPos)
                    {
                        _scrollRect.verticalNormalizedPosition = _scrollToTargetPos;
                        _state = STATE_IDLE;
                    }
                }
                else if (_scrollRect.verticalNormalizedPosition < _scrollToTargetPos)
                {
                    _scrollRect.verticalNormalizedPosition += moveAmount;
                    if (_scrollRect.verticalNormalizedPosition >= _scrollToTargetPos)
                    {
                        _scrollRect.verticalNormalizedPosition = _scrollToTargetPos;
                        _state = STATE_IDLE;
                    }
                }
            }

            if (_scrollRect.horizontal)
            {
                if (_scrollRect.horizontalNormalizedPosition > _scrollToTargetPos)
                {
                    //Log ("--- Scrolling left moveAmount: " + moveAmount);
                    _scrollRect.horizontalNormalizedPosition -= moveAmount;
                    if (_scrollRect.horizontalNormalizedPosition <= _scrollToTargetPos)
                    {
                        _scrollRect.horizontalNormalizedPosition = _scrollToTargetPos;
                        _state = STATE_IDLE;
                    }

                    if (_contentHolderRectTransform.position.x > Bounds.Left)
                    {
                        _contentHolderRectTransform.position = new Vector2(Bounds.Left, _contentHolderRectTransform.position.y);
                        _state = STATE_IDLE;
                    }
                }
                else if (_scrollRect.horizontalNormalizedPosition < _scrollToTargetPos)
                {
                    //Log ("--- Scrolling right moveAmount: " + moveAmount);
                    _scrollRect.horizontalNormalizedPosition += moveAmount;
                    if (_scrollRect.horizontalNormalizedPosition >= _scrollToTargetPos)
                    {
                        _scrollRect.horizontalNormalizedPosition = _scrollToTargetPos;
                        _state = STATE_IDLE;
                    }

                    float max = Bounds.Right - Bounds.Width;
                    if (_contentHolderRectTransform.position.x < max)
                    {
                        _contentHolderRectTransform.position = new Vector2(max, _contentHolderRectTransform.position.y);
                        _state = STATE_IDLE;
                    }
                }
            }
        }

        void UpdateHorizontalScrollToPosition()
        {
            float moveAmount = _scrollSpeed * Time.deltaTime;
            moveAmount *= 100f;

            Canvas.ForceUpdateCanvases();

            Vector2 targetPos = ContentHolderRectTransform.anchoredPosition;

            if (ContentHolderRectTransform.anchoredPosition.x > _scrollToTargetPos)
            {
                targetPos.x -= moveAmount;
                if (targetPos.x <= _scrollToTargetPos)
                {
                    targetPos.x = _scrollToTargetPos;
                    _state = STATE_IDLE;
                }

                Debug.Log("---> moveAmount: " + moveAmount + ", _scrollToTargetPos: " + _scrollToTargetPos + ", targetPos: " + targetPos + ", _scrollRect.horizontalNormalizedPosition: " + _scrollRect.horizontalNormalizedPosition);
                ContentHolderRectTransform.anchoredPosition = targetPos;

                /*
				if(_contentHolderRectTransform.position.x > Bounds.Left)
				{
					_contentHolderRectTransform.position = new Vector2(Bounds.Left, _contentHolderRectTransform.position.y);
					_state = STATE_IDLE;
				}
				*/
            }
            else if (ContentHolderRectTransform.anchoredPosition.x < _scrollToTargetPos)
            {
                targetPos.x += moveAmount;
                if (targetPos.x >= _scrollToTargetPos)
                {
                    targetPos.x = _scrollToTargetPos;
                    _state = STATE_IDLE;
                }

                Debug.Log("<--- moveAmount: " + moveAmount + ", _scrollToTargetPos: " + _scrollToTargetPos + ", targetPos: " + targetPos + ", _scrollRect.horizontalNormalizedPosition: " + _scrollRect.horizontalNormalizedPosition);
                ContentHolderRectTransform.anchoredPosition = targetPos;

                /*
				float max = Bounds.Right - Bounds.Width;
				if(_contentHolderRectTransform.position.x < max)
				{
					_contentHolderRectTransform.position = new Vector2(max, _contentHolderRectTransform.position.y);
					_state = STATE_IDLE;
				}
				*/
            }

            /*
			if(_scrollRect.horizontalNormalizedPosition < 0 || _scrollRect.horizontalNormalizedPosition >= 1)
			{
				targetPos.x = _scrollToTargetPos;
				ContentHolderRectTransform.anchoredPosition = targetPos;
				_state = STATE_IDLE;
			}
			*/
            //ContentHolderRectTransform.anchoredPosition = (Vector2)_scrollRect.transform.InverseTransformPoint(ContentHolderRectTransform.position) - (Vector2)_scrollRect.transform.InverseTransformPoint(target.position);

        }

        void UpdateVerticalScrollToPosition()
        {
            float moveAmount = _scrollSpeed * Time.deltaTime;
            moveAmount *= 100f;

            Canvas.ForceUpdateCanvases();

            Vector2 targetPos = ContentHolderRectTransform.anchoredPosition;

            if (ContentHolderRectTransform.anchoredPosition.y > _scrollToTargetPos)
            {
                targetPos.y -= moveAmount;

                if (targetPos.y <= _scrollToTargetPos || _scrollRect.verticalNormalizedPosition > 1)
                {
                    targetPos.y = _scrollToTargetPos;
                    _state = STATE_IDLE;
                }

                ContentHolderRectTransform.anchoredPosition = targetPos;
            }
            else if (ContentHolderRectTransform.anchoredPosition.y < _scrollToTargetPos)
            {
                targetPos.y += moveAmount;

                if (targetPos.y >= _scrollToTargetPos)
                {
                    targetPos.y = _scrollToTargetPos;
                    _state = STATE_IDLE;
                }

                ContentHolderRectTransform.anchoredPosition = targetPos;
            }
        }

        void UpdateDragging()
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(UnityEngine.Input.mousePosition);
            if (SB_Utils.GetDistance(_mouseDownPos, mousePos) > ScrollTolerance)
            {
                _doMouseReleasedCallback = false;
            }
        }

        public void LoadThumbs(int fromIndex, int numToLoad)
        {
            Debug.Log("[" + this.GetType() + "] LoadThumbs fromIndex: " + fromIndex + ", numToLoad: " + numToLoad);
            _startThumbIndex = fromIndex;
            LoadThumbs(numToLoad);
        }

        public void LoadThumbs(int numToLoad = -1)
        {
            _thumbLoadedCount = 0;

            if (numToLoad == -1) numToLoad = Items.Count;
            _numThumbsToLoad = numToLoad;
            LoadNextThumb();
        }

        private void LoadNextThumb()
        {
            SB_ScrollableMenuItem item = GetItemAt(_startThumbIndex + _thumbLoadedCount);
            if (item != null && _thumbLoadedCount < _numThumbsToLoad)
            {
                item.OnImgLoaded = OnThumbLoaded;
                item.LoadImage();
            }
            else
            {
                Log("ALL THUMBS LOADED!! _thumbLoadedCount: " + _thumbLoadedCount + " / " + _numThumbsToLoad);
                if (OnItemThumbsLoaded != null)
                {
                    OnItemThumbsLoaded();
                }
            }
        }

        private void OnThumbLoaded(SB_ScrollableMenuItem item)
        {
            item.OnImgLoaded = null;
            _thumbLoadedCount++;
            LoadNextThumb();
        }

        public void ShowItems(bool val)
        {
            if (Items != null)
            {
                for (int n = 0; n < Items.Count; n++)
                {
                    SB_ScrollableMenuItem itm = (SB_ScrollableMenuItem)Items[n];
                    itm.gameObject.SetActive(val);
                }
            }
        }

        public SB_ScrollableMenuItem GetItemAt(int n)
        {
            if (Items != null)
            {
                if (n < Items.Count)
                {
                    return (SB_ScrollableMenuItem)Items[n];
                }

                return null;
            }

            return null;
        }

        public void RemoveItem(SB_ScrollableMenuItem item)
        {
            if (Items != null)
            {
                for (int n = 0; n < Items.Count; n++)
                {
                    SB_ScrollableMenuItem itm = (SB_ScrollableMenuItem)Items[n];
                    if (itm == item)
                    {
                        RemoveItemAt(n);
                        break;
                    }
                }
            }
        }

        public void RemoveItemAt(int n)
        {
            if (Items != null)
            {
                SB_ScrollableMenuItem itm = (SB_ScrollableMenuItem)Items[n];
                Log("--- DESTROYING MENU ITEM! ");
                Destroy(itm.gameObject);
                Items.RemoveAt(n);

                if (OnItemRemovedCallback != null) OnItemRemovedCallback(Items.Count);
            }
        }

        public void RemoveAllItems()
        {
            if (Items != null)
            {
                while (Items.Count > 0)
                {
                    RemoveItemAt(0);
                }

                Items.Clear();
                Items = new ArrayList();

                if (OnItemRemovedCallback != null) OnItemRemovedCallback(Items.Count);
            }
        }

        public float GetPaddingX()
        {
            return ContentHolder.transform.GetComponent<HorizontalLayoutGroup>().padding.left;
        }

        /**
		 * Returns the centered x position of a menu item, within the menu.
		 * This is used so we can scroll the menu to a particular item, and have it centered
		*/
        public float GetCenteredXPos(int itemIndex)
        {
            float pos = 0f;

            float w = ContentHolderRectTransform.sizeDelta.x;
            float itemWidth = w / Items.Count;
            float paddingX = GetPaddingX();

            Log("w: " + w + ", itemWidth: " + itemWidth + ", paddingX: " + paddingX);
            pos = itemWidth * (itemIndex + 0.5f) + (paddingX * itemIndex + 1);

            float scaler = 1.0f;


            // NOTE: MagicNumber = 1 / 4:3 ContentHolderRectTransform.localScale.x value
            float magicNumber = 0.9495f;
            scaler = ContentHolderRectTransform.localScale.x * magicNumber;
            //Log("----------> ContentHolderRectTransform.localScale.x: " + ContentHolderRectTransform.localScale.x + ", scaler: " + scaler);

            pos *= scaler;

            return -pos;
        }

        void Log(string msg)
        {
            //Debug.LogWarning("[" + this.GetType() + "] " + msg);
        }

        void OnDestroy()
        {
            ContentHolder = null;

            _rectTransform = null;
            _scrollRect = null;

            if (Items != null)
            {
                Items.Clear();
                Items = null;
            }

            OnStartDragCallback = null;
            OnPressCallback = null;
            OnReleaseCallback = null;
            OnItemRemovedCallback = null;
            OnItemThumbsLoaded = null;
        }
    }
}