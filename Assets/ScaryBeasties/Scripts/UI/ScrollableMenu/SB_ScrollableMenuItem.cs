﻿using UnityEngine;
using System.Collections;
using System;
using ScaryBeasties.Utils;

namespace ScaryBeasties.UI.ScrollableMenu
{
    public class SB_ScrollableMenuItem : MonoBehaviour
    {
        public string id = "";
        public int index = 0;
        public bool enabled = true;
        public object data = null;

        public string imgPath = "";
        public Action<SB_ScrollableMenuItem> OnImgLoaded;

        bool _mouseDown = false;

        public bool ImageIsLoaded = false;

        protected virtual void Start()
        {
            transform.localScale = Vector3.one;
        }

        public float Width { get { return GetComponent<RectTransform>().rect.width; } }
        public float Height { get { return GetComponent<RectTransform>().rect.height; } }

        public float Left { get { return transform.position.x - Width * 0.5f; } }
        public float Right { get { return transform.position.x + Width * 0.5f; } }
        public float Top { get { return transform.position.y + Height * 0.5f; } }
        public float Bottom { get { return transform.position.y - Height * 0.5f; } }

        public string Area()
        {
            return "[" + id + "] l: " + Left + ", r: " + Right + ", t: " + Top + ", b: " + Bottom + " -- w: " + Width + ", h: " + Height;
        }

        public bool HitTest(Vector2 point)
        {
            //Debug.LogWarning("---> mouseUpPos: "+ mouseUpPos + ", ScrollableMenuBounds.Bounds: " + ScrollableMenuBounds.Bounds);
            if (point.x > Left &&
               point.x < Right &&
               point.y < Top &&
               point.y > Bottom)
            {
                return true;
            }

            return false;
        }

        public virtual void LoadImage()
        {
            ImageIsLoaded = true;
            // To be overridden by sublcass
        }

        public virtual void UnloadImage()
        {
            // To be overridden by sublcass
        }

        protected virtual void ImageLoaded()
        {

            if (OnImgLoaded != null)
            {
                OnImgLoaded(this);
            }
        }

        protected virtual void OnDestroy()
        {
            data = null;
            OnImgLoaded = null;
        }
    }
}