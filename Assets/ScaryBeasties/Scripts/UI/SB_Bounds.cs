﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Utils;

namespace ScaryBeasties.UI
{
	public class SB_Bounds : MonoBehaviour 
	{
		Transform _top;
		Transform _bottom;
		Transform _left;
		Transform _right;
		
		/*
		void Awake()
		{
			_top = transform.Find("Top");
			_bottom = transform.Find("Bottom");
			_left = transform.Find("Left");
			_right = transform.Find("Right");
			
			#if !UNITY_EDITOR
			_top.gameObject.SetActive(false);
			_bottom.gameObject.SetActive(false);
			_left.gameObject.SetActive(false);
			_right.gameObject.SetActive(false);
			#endif
		}
		*/
		
		void Start()
		{
			_top = transform.Find("Top");
			_bottom = transform.Find("Bottom");
			_left = transform.Find("Left");
			_right = transform.Find("Right");
			
			/*
			#if !UNITY_EDITOR
			_top.gameObject.SetActive(false);
			_bottom.gameObject.SetActive(false);
			_left.gameObject.SetActive(false);
			_right.gameObject.SetActive(false);
			#endif
			*/
			
			HideTransform(_top);
			HideTransform(_bottom);
			HideTransform(_left);
			HideTransform(_right);
			
			Log("---> BOUNDS w: " + Width + ", h: " + Height + ", Center: " + Center + " --- Bounds: " + Bounds);
			Log("---> BOUNDS l: " + Left + ", r: " + Right + ", t: " + Top + " b: " + Bottom);
		}
		
		void HideTransform(Transform tr)
		{
			SpriteRenderer sr = tr.GetComponent<SpriteRenderer>();
			
			if(sr != null)
			{
				sr.color = new Color(0,0,0,0);
			}
		}
		
		/*
		void OnGUI()
		{
			Vector2 mouseUpPos = Camera.main.ScreenToWorldPoint(UnityEngine.Input.mousePosition);
			
			bool result = false;
			if(mouseUpPos.x > Left && mouseUpPos.x < Right && mouseUpPos.y < Top && mouseUpPos.y > Bottom)
			{
				result = true;
			}
			
			Log("---> mouse pos: "+ mouseUpPos + ", l: " + Left + ", r: " + Right + ", t: " + Top + " b: " + Bottom + " - RESULT: " + result);
		}
		*/
		public Bounds Bounds
		{
			get 
			{
				return new Bounds(Center, new Vector2(Width*2, Height*2));
			}
		}
		public float Width	{get {return SB_Utils.GetDistance(_left.localPosition, _right.localPosition);}}
		public float Height	{get {return SB_Utils.GetDistance(_top.localPosition, _bottom.localPosition);}}
		
		public Vector2 Center	{get {return new Vector2(Left + Width*0.5f, Top - Height*0.5f);}}
		
		public float Left 	{get {return _left.localPosition.x;}}
		public float Right 	{get {return _right.localPosition.x;}}
		public float Top 	{get {return _top.localPosition.y;}}
		public float Bottom {get {return _bottom.localPosition.y;}}
		
		public bool Contains(Vector2 point)
		{
			//Debug.LogWarning("---> mouseUpPos: "+ mouseUpPos + ", Bounds.Bounds: " + Bounds.Bounds);
			if(point.x > Left && 
			   point.x < Right && 
			   point.y < Top && 
			   point.y > Bottom)
			{
				return true;
			}
			
			return false;
		}
		
		public string Area()
		{
			return "l: " + Left + ", r: " + Right + ", t: " + Top + ", b: " + Bottom + " -- w: " + Width + ", h: " + Height;
		}
		
		void Log(string msg)
		{
			Debug.LogWarning("[SB_Bounds] " + msg);
		}
		
		void OnDestroy()
		{
			_top = null;
			_bottom = null;
			_left = null;
			_right = null;
		}		
	}
}