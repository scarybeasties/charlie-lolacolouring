﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Utils;
using System;

public class SB_TweenedItem : MonoBehaviour 
{
	public enum TweenType
	{
		Position,
		Scale,
		Alpha
	}

	private Vector3 _targetVec;
	private float _targetAlpha = 1f;
	private float _xVal = 0f;
	private float _yVal = 0f;
	private float _speed = 0f;
	private float _angle = 0f;
	private float _elapsed = 0f;
	private float _duration = 0f;
	private TweenType _tweenType;

	private bool _tweenFinished = false;
	private Action<SB_TweenedItem> _tweenFinishedCallback;

	public bool Paused = false;

	public void MoveTo(Vector3 target, float duration, Action<SB_TweenedItem> callback=null)
	{
		_tweenType = TweenType.Position;

		_targetVec = target;
		_duration = duration;
		_tweenFinishedCallback = callback;

		float distanceFromDestination = SB_Utils.GetDistance(transform.position, _targetVec);
		_speed = distanceFromDestination/duration;

		_angle = SB_Utils.GetAngle(transform.position, _targetVec);
		Vector3 steps = SB_Utils.GetXY(_speed, _angle);
		_xVal = steps.x;
		_yVal = steps.y;

		Log("MoveTo " + _targetVec + ", vx: " + _xVal + ", vy: " + _yVal + ", speed: " + _speed + ", angle: " + _angle);
	}

	public void ScaleTo(Vector3 target, float duration, Action<SB_TweenedItem> callback=null)
	{
		_tweenType = TweenType.Scale;

		_targetVec = target;
		_duration = duration;
		_tweenFinishedCallback = callback;

		Vector3 scaleDifference = target - transform.localScale;
		_xVal = scaleDifference.x / duration;
		_yVal = scaleDifference.y / duration;
	}

	public void AlphaTo(float target, float duration, Action<SB_TweenedItem> callback=null)
	{
		_tweenType = TweenType.Alpha;
		
		_targetAlpha = target;
		_duration = duration;
		_tweenFinishedCallback = callback;

		SpriteRenderer sprRenderer = transform.GetComponent<SpriteRenderer>();
		if(sprRenderer != null)
		{
			float difference = _targetAlpha - sprRenderer.color.a;
			_xVal = difference / duration;
		}
	}

	public void Kill()
	{
		Destroy (this);
	}

	void Update () 
	{
		if(Paused) return;

		_elapsed += Time.deltaTime;

		if(_tweenType == TweenType.Position) UpdatePosition();
		if(_tweenType == TweenType.Scale) UpdateScale();
		if(_tweenType == TweenType.Alpha) UpdateAlpha();
	}

	void UpdatePosition ()
	{
		if(Paused) return;

		Vector3 pos = transform.position;
		pos.x += _xVal*Time.deltaTime;
		pos.y += _yVal*Time.deltaTime;
		transform.position = pos;
		
		float distanceFromDestination = SB_Utils.GetDistance(transform.position, _targetVec);
		//if(distanceFromDestination < 0.1 || 
		if(_elapsed >= _duration)
		{
			_xVal = 0;
			_yVal = 0;
			
			transform.position = _targetVec;
			TweenFinished();
		}
	}

	void UpdateScale ()
	{
		Vector3 scale = transform.localScale;
		scale.x += _xVal*Time.deltaTime;
		scale.y += _yVal*Time.deltaTime;
		transform.localScale = scale;

		if(_elapsed >= _duration)
		{
			_xVal = 0;
			_yVal = 0;

			transform.localScale = _targetVec;
			TweenFinished();
		}
	}

	void UpdateAlpha ()
	{
		SpriteRenderer sprRenderer = transform.GetComponent<SpriteRenderer>();
		if(sprRenderer == null)
		{
			TweenFinished();
			return;
		}

		Color col = sprRenderer.color;
		col.a += _xVal*Time.deltaTime;
		sprRenderer.color = col;
		
		if(_elapsed >= _duration)
		{
			_xVal = 0;
			
			col.a = _targetAlpha;
			sprRenderer.color = col;
			TweenFinished();
		}
	}

	void TweenFinished()
	{
		if(_tweenFinishedCallback != null)
		{
			_tweenFinishedCallback(this);
			_tweenFinishedCallback = null;
		}

		_tweenFinished = true;

		Log ("FINISHED!");
		Kill();
	}

	void Log(string msg)
	{
		Debug.Log("[SB_TweenedItem] " + msg);
	}

	void OnDestroy()
	{

	}
}
