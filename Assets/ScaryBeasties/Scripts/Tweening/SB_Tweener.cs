﻿using UnityEngine;
using System.Collections;
using System;

namespace ScaryBeasties.Tweening
{
	public class SB_Tweener
	{
		static ArrayList _tweens = new ArrayList();

		public static void TweenPosition(GameObject obj, float duration, Vector3 target, Action<SB_TweenedItem> callback=null)
		{
			Log("TweenPosition " + obj + ", dur: " + duration);
			SB_TweenedItem tweenedItem = (SB_TweenedItem)obj.AddComponent<SB_TweenedItem>();
			tweenedItem.MoveTo(target, duration, callback);

			_tweens.Add(tweenedItem);
		}

		public static void TweenScale(GameObject obj, float duration, Vector3 target, Action<SB_TweenedItem> callback=null)
		{
			Log("Tween " + obj + ", dur: " + duration);
			SB_TweenedItem tweenedItem = (SB_TweenedItem)obj.AddComponent<SB_TweenedItem>();
			tweenedItem.ScaleTo(target, duration, callback);
			
			_tweens.Add(tweenedItem);
		}

		public static void TweenAlpha(GameObject obj, float duration, float target, Action<SB_TweenedItem> callback=null)
		{
			Log("Tween " + obj + ", dur: " + duration);
			SB_TweenedItem tweenedItem = (SB_TweenedItem)obj.AddComponent<SB_TweenedItem>();
			tweenedItem.AlphaTo(target, duration, callback);
			
			_tweens.Add(tweenedItem);
		}

		/** Pause all tweens */
		public static void PauseAllTweens()
		{
			foreach(SB_TweenedItem tweenedItem in _tweens)
			{
				tweenedItem.Paused = true;
			}
		}

		/** Resume all tweens */
		public static void ResumeAllTweens()
		{
			foreach(SB_TweenedItem tweenedItem in _tweens)
			{
				tweenedItem.Paused = false;
			}
		}

		public static void StopAllTweens()
		{
			while(_tweens.Count > 0)
			{
				SB_TweenedItem tweenedItem = (SB_TweenedItem)_tweens[0];
				tweenedItem.Kill();
				_tweens.RemoveAt(0);
			}
		}

		public static void RemoveAllTweensOf(GameObject obj)
		{
			SB_TweenedItem[] tweens = obj.transform.GetComponentsInChildren<SB_TweenedItem>();
			foreach (SB_TweenedItem tween in tweens) 
			{
				if(tween != null)
				{
					tween.Kill();
				}
			}
		}

		static void Log(string msg)
		{
			Debug.Log("[SB_Tween] " + msg);
		}
	}
}