﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Xml;
using ScaryBeasties.InAppPurchasing.Local;
using ScaryBeasties.InAppPurchasing.Shop;
using System.Collections.Generic;

namespace ScaryBeasties.InAppPurchasing.PackDownloader
{
	public class SB_PackDownloader : MonoBehaviour 
	{
		private WWW www;
		private bool _isLoading = false;
		private ArrayList _downloadQueue = new ArrayList();
		private ArrayList _errors = new ArrayList();
		private SB_QueuedPackDownload _currentQueuedDownload = null;
		
		private int _numFilesToDownload = 0;
		private int _fileCounter = 0;
		private float _overallPercentageLoaded = 0f;
		
		public virtual void DownloadPack (string packID, Action<SB_PackDownloaderResponse> callback = null)
		{
			// Clear any previous errors
			if (QueueLength == 0) 
			{
				_errors.Clear();
				_fileCounter = 0;
				_overallPercentageLoaded = 0f;
			}
			
			SB_QueuedPackDownload queued = new SB_QueuedPackDownload(packID, callback);
			_downloadQueue.Add(queued);
			_numFilesToDownload = QueueLength;
			
			if (QueueLength == 1) 
			{
				LoadNextQueuedPack();
			}
		}
		
		public int QueueLength
		{
			get {return _downloadQueue.Count;}
		}
		
		public SB_QueuedPackDownload CurrentDownload()
		{
			return _currentQueuedDownload;
		}

		private void LoadNextQueuedPack()
		{
			Debug.LogWarning ("_downloadQueue.Count: " + _downloadQueue.Count);
			if (QueueLength == 0)
			{
				DoCallback(SB_PackDownloaderResponse.DOWNLOAD_QUEUE_EMPTY, "", null);
				return;
			}
			
			_currentQueuedDownload = (SB_QueuedPackDownload)_downloadQueue[0];
			
			// Check if pack.xml file exists, in the pack folder 
			if (!SB_ShopUtils.PackXmlExists(_currentQueuedDownload.packId)) 
			{
				// Attempt to download the pack from the server
				StartCoroutine (DoDownloadPack(_currentQueuedDownload.packId));
			}
			else
			{
				// Pack has already been downloaded
				Debug.LogWarning ("FILE FOUND");
				SB_LocalPackManager.instance.LoadPackXML(_currentQueuedDownload.packId, OnPackAlreadyDownloaded);

				_downloadQueue.RemoveAt(0);
				LoadNextQueuedPack();
			}
		}
		
		private void DoCallback(string status, string packID, XmlDocument packXML=null)
		{
			if (_currentQueuedDownload != null && _currentQueuedDownload.callback != null) 
			{
				_currentQueuedDownload.callback(new SB_PackDownloaderResponse(status, packID, packXML, _errors));
			}
		}

		void OnPackAlreadyDownloaded (SB_LocalPackManagerResponse response)
		{
			XmlDocument packXML = SB_LocalPackManager.instance.GetPackXML(response.PackId);
			DoCallback(SB_PackDownloaderResponse.PACK_ALREADY_DOWNLOADED, response.PackId, packXML);
		}

		void OnPackSuccessfullyDownloaded (SB_LocalPackManagerResponse response)
		{
			XmlDocument packXML = SB_LocalPackManager.instance.GetPackXML(response.PackId);
			DoCallback(SB_PackDownloaderResponse.PACK_DOWNLOAD_SUCCESS, response.PackId, packXML);
		}

		private IEnumerator DoDownloadPack (string packID) 
		{
			_isLoading = true;
			if (Application.internetReachability == NetworkReachability.NotReachable) {
				yield break;
			} 
			else 
			{
				/*
				string fullPath = SB_ShopUtils.GetRemotePackDownloadUrl(packID);
				Debug.LogWarning("Downloading file: " + fullPath);
				www = new WWW (fullPath);
				*/
				
				string fullPath = SB_ShopUtils.GetRemotePackDownloadUrl(packID);
				Debug.LogWarning("Downloading file: " + fullPath);
				
				// Send POST vars
				WWWForm form = new WWWForm();
				form.AddField( "name", "value" );
				form.AddField("id", packID);
				form.AddField("platform", tk2dSystem.CurrentPlatform);
				form.AddField("locale", SB_Globals.LOCALE_STRING);
				
				Dictionary<string,string> headers = form.headers;
				byte[] rawData = form.data;
				
				/*
				// Add a custom header to the request.
				headers["id"] = packID;
				headers["platform"] = tk2dSystem.CurrentPlatform;
				headers["locale"] = SB_Globals.LOCALE_STRING;
				*/
					
				// Post a request to an URL with our custom headers
				www = new WWW(fullPath, rawData, headers);

				while (!this.www.isDone) 
				{
					if (this.www.progress >= 100 && !string.IsNullOrEmpty (this.www.error)) 
					{
						break;
					}
					yield return null;
				}
			
				this.HandleAssetResult (packID, this.www.error);
			}
		}
		
		public int LoadingProgress 
		{
			get 
			{
				if(this.www == null || this.www.isDone) { return -1; }
				{
					//Debug.Log(this.www.progress);
					
					float maxPcEachFile = (float)(100 / _numFilesToDownload);
					int fileProgress = Mathf.FloorToInt(this.www.progress * maxPcEachFile);
					
					float overallFileProgress = (float)(maxPcEachFile*_fileCounter) + fileProgress;
					//Debug.Log("_fileCounter: " + _fileCounter + ", maxPcEachFile " + maxPcEachFile + ", fileProgress: " + fileProgress + ", oveallFileProgress: " + oveallFileProgress);

					if(overallFileProgress > _overallPercentageLoaded)
					{
						_overallPercentageLoaded = overallFileProgress;
					}

					return Mathf.FloorToInt(_overallPercentageLoaded);
				}
			}
		}


		private void HandleAssetResult (string packID, string error)
		{
			Debug.LogError("----------------------------");
			Debug.LogError("HandleAssetResult! ");
			Debug.LogError("----------------------------");
			
			string status = "";
			
			_fileCounter++;
			
			_isLoading = false;
			if (!string.IsNullOrEmpty(error) || this.www.bytes.Length < 1024) 
			{
				// Problem downloading file
				Debug.LogError("Download error! " + error);
				
				_errors.Add(packID);
				DoCallback(SB_PackDownloaderResponse.PACK_DOWNLOAD_ERROR, packID, null);
				//return;
			}
			else
			{	
				// File successfully downloaded
				DirectoryInfo directory = new DirectoryInfo(SB_ShopUtils.GetLocalAssetDirectoryPath());
				if (!directory.Exists) {
					directory.Create();
				}
				
				string filePath = SB_ShopUtils.GetLocalAssetFilePath(packID) + ".zip"; 
				Debug.LogError(filePath);
				File.WriteAllBytes(filePath, this.www.bytes);
				
				Debug.Log("Zip file exists at: " + filePath);
				Debug.Log("Extracting file to: " + Application.persistentDataPath);
				Unzip (filePath, Application.persistentDataPath, true);
	
				#if UNITY_IOS
				iPhone.SetNoBackupFlag(filePath);
				#endif
	
				// Load the pack.xml file, from the unzipped folder
				if (SB_ShopUtils.PackXmlExists (packID)) 
				{
					// Load the pack xml
					SB_LocalPackManager.instance.LoadPackXML(packID, OnPackSuccessfullyDownloaded);
				}
				else
				{
					// Pack was unzipped, but no xml was found!
					DoCallback(SB_PackDownloaderResponse.PACK_XML_NOT_FOUND, packID);
				}
			}
			
			_downloadQueue.RemoveAt(0);
			LoadNextQueuedPack();
			
		}

		void Unzip(string filePath, string targetFolder, bool deleteZipOnFinish=false)
		{
			Debug.Log ("Unzipping " + filePath);
			ZipUtil.Unzip( filePath, targetFolder);
			
			if (deleteZipOnFinish) 
			{
				// delete file..
				File.Delete(filePath);
			}
		}
		
		protected virtual void OnDestroy()
		{
			if (www != null) 
			{
				www.Dispose();
				www = null;
			}

			if(_downloadQueue != null)
			{
				_downloadQueue.Clear();
				_downloadQueue = null;
			}
			
			if(_errors != null)
			{
				_errors.Clear();
				_errors = null;
			}
			
			if(_currentQueuedDownload != null)
			{
				_currentQueuedDownload.callback = null;
				_currentQueuedDownload = null;
			}
		}
	}
}
