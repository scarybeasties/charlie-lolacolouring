﻿using System;

namespace ScaryBeasties.InAppPurchasing.PackDownloader
{
	public class SB_QueuedPackDownload
	{
		public string packId;
		public Action<SB_PackDownloaderResponse> callback;
		
		public SB_QueuedPackDownload(string packId, Action<SB_PackDownloaderResponse> callback=null)
		{
			this.packId = packId;
			this.callback = callback;
		}
	}
}