﻿using UnityEngine;
using System.Collections;
using System.Xml;

namespace ScaryBeasties.InAppPurchasing.PackDownloader
{
	public class SB_PackDownloaderResponse
	{
		public static string PACK_DOWNLOAD_IN_PROGRESS = "packDownloadInProgress";
		public static string PACK_ALREADY_DOWNLOADED = "packAlreadyDownloaded";
		public static string PACK_XML_NOT_FOUND = "packXmlNotFound";
		public static string PACK_DOWNLOAD_ERROR = "packDownloadError";
		public static string PACK_DOWNLOAD_SUCCESS = "packDownloadSuccess";
		public static string DOWNLOAD_QUEUE_EMPTY = "downloadQueueEmpty";

		private string _status = "";
		private string _packId = "";
		private XmlDocument _packXML = null;
		private ArrayList _errors = null;
		
		public SB_PackDownloaderResponse (string status, string packId, XmlDocument packXML=null, ArrayList errors=null) 
		{
			_status = status;
			_packId = packId;
			_packXML = packXML;
			_errors = errors;
		}

		public string Status{ get{return _status;} }
		public string PackId{ get{return _packId;} }
		public XmlDocument PackXML{ get{return _packXML;} }
		public ArrayList Errors{ get{return _errors;} }
	}
}