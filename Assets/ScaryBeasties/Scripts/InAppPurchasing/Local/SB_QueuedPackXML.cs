﻿using System;

namespace ScaryBeasties.InAppPurchasing.Local
{
	public class SB_QueuedPackXML
	{
		public string packId;
		public Action<SB_LocalPackManagerResponse> callback;
		
		public SB_QueuedPackXML(string packId, Action<SB_LocalPackManagerResponse> callback=null)
		{
			this.packId = packId;
			this.callback = callback;
		}
	}
}