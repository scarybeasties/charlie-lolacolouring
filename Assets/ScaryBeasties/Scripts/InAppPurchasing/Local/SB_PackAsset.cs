﻿using System.Xml;
using ScaryBeasties.InAppPurchasing.Shop;
using UnityEngine;

namespace ScaryBeasties.InAppPurchasing.Local
{
    public class SB_PackAsset : Object
    {
        public int priority = 0;
        public string id;
        public string assetName;
        public bool flipX = true;
        public bool flipY = true;
        public bool scaleX = true;
        public bool scaleY = true;
        public bool draggable = true;
        public bool maintainAspectRatio = true;
        public bool rotate = true;
        public bool cloneable = true;

        public string description;
        public bool loop = false;
        public bool locked = false;

        // When this bool is set to true, the pack asset will only be available to ScaryBeasties, when SB_Globals.ALLOW_STORY_EDITING is true, not the end user
        public bool SBOnly = false;

        /**
		 * Returns the full asset name (including the path)
		*/
        public virtual string FileName
        {
            get
            {
                return "file://" + SB_ShopUtils.GetLocalAssetDirectoryPath() + "/" + assetName;
            }
        }

        /**
		 * SB_PackAsset
		 * 
		 * Stores the properties of pack's <asset> node:
		 * 
		 * 			<asset id="doctor0">doctors/doctor0.png</asset>
		 * 
		 * By default, all PackAsset objects 'flip' and 'scale' properties are set to true.
		 * These can be disabled from within the pack.xml for each pack:
		 * 
		 * 			<asset id="doctor0" flip="no" scale="no">doctors/doctor0.png</asset>
		 */
        public SB_PackAsset()
        {
        }

        public virtual void ParseXML(XmlNode xmlNode)
        {
            if (xmlNode.Attributes.GetNamedItem("id") != null)
            {
                id = xmlNode.Attributes.GetNamedItem("id").Value;
            }

            if (xmlNode.Attributes.GetNamedItem("priority") != null)
            {
                priority = int.Parse(xmlNode.Attributes.GetNamedItem("priority").Value);
            }

            if (xmlNode.Attributes.GetNamedItem("locked") != null)
            {
                string lockValue = xmlNode.Attributes.GetNamedItem("locked").Value.ToLower();
                locked = (lockValue == "no" || lockValue == "false") ? false : true;

            }

            assetName = xmlNode.InnerText;

            if (xmlNode.Attributes.GetNamedItem("flipX") != null)
            {
                string flipValue = xmlNode.Attributes.GetNamedItem("flipX").Value.ToLower();
                flipX = (flipValue == "no" || flipValue == "false") ? false : true;
            }

            if (xmlNode.Attributes.GetNamedItem("flipY") != null)
            {
                string flipValue = xmlNode.Attributes.GetNamedItem("flipY").Value.ToLower();
                flipY = (flipValue == "no" || flipValue == "false") ? false : true;
            }

            if (xmlNode.Attributes.GetNamedItem("scaleX") != null)
            {
                string scaleValue = xmlNode.Attributes.GetNamedItem("scaleX").Value.ToLower();
                scaleX = (scaleValue == "no" || scaleValue == "false") ? false : true;
            }

            if (xmlNode.Attributes.GetNamedItem("scaleY") != null)
            {
                string scaleValue = xmlNode.Attributes.GetNamedItem("scaleY").Value.ToLower();
                scaleY = (scaleValue == "no" || scaleValue == "false") ? false : true;
            }


            if (xmlNode.Attributes.GetNamedItem("maintainAspectRatio") != null)
            {
                string maintainAspectRatioValue = xmlNode.Attributes.GetNamedItem("maintainAspectRatio").Value.ToLower();
                maintainAspectRatio = (maintainAspectRatioValue == "no" || maintainAspectRatioValue == "false") ? false : true;
            }

            if (xmlNode.Attributes.GetNamedItem("rotate") != null)
            {
                string rotateValue = xmlNode.Attributes.GetNamedItem("rotate").Value.ToLower();
                rotate = (rotateValue == "no" || rotateValue == "false") ? false : true;
            }

            if (xmlNode.Attributes.GetNamedItem("description") != null)
            {
                description = xmlNode.Attributes.GetNamedItem("description").Value;
            }

            if (xmlNode.Attributes.GetNamedItem("loop") != null)
            {
                string loopValue = xmlNode.Attributes.GetNamedItem("loop").Value.ToLower();
                loop = (loopValue == "no" || loopValue == "false") ? false : true;
            }

            if (xmlNode.Attributes.GetNamedItem("draggable") != null)
            {
                string draggableValue = xmlNode.Attributes.GetNamedItem("draggable").Value.ToLower();
                draggable = (draggableValue == "no" || draggableValue == "false") ? false : true;
            }

            if (xmlNode.Attributes.GetNamedItem("cloneable") != null)
            {
                string cloneableValue = xmlNode.Attributes.GetNamedItem("cloneable").Value.ToLower();
                cloneable = (cloneableValue == "no" || cloneableValue == "false") ? false : true;
            }


            if (xmlNode.Attributes.GetNamedItem("SBOnly") != null)
            {
                string SBOnlyValue = xmlNode.Attributes.GetNamedItem("SBOnly").Value.ToLower();
                SBOnly = (SBOnlyValue == "no" || SBOnlyValue == "false") ? false : true;
            }
        }

        public virtual string ToString()
        {
            string str = "id: " + id +
                        ", priority: " + priority +
                        ", assetName: " + assetName +
                        ", flipX: " + flipX +
                        ", flipY: " + flipY +
                        ", scaleX: " + scaleX +
                        ", scaleY: " + scaleY +
                        ", maintainAspectRatio: " + maintainAspectRatio +
                        ", rotate: " + rotate +
                        ", description: " + description +
                        ", loop: " + loop +
                        ", draggable: " + draggable +
                        ", cloneable: " + cloneable +
                        ", SBOnly: " + SBOnly;

            return str;
        }
    }
}