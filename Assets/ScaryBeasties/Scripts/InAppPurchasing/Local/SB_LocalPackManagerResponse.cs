﻿
namespace ScaryBeasties.InAppPurchasing.Local
{
	public class SB_LocalPackManagerResponse
	{
		public const string PACK_XML_LOAD_SUCCESS = "SB_LocalPackManagerResponse_packXMLLoadSuccess";
		public const string PACK_XML_LOAD_FAILED = "SB_LocalPackManagerResponse_packXMLLoadFailed";
		
		public const string FILE_VALIDATION_NO_PACKS_FOUND = "SB_LocalPackManagerResponse_fileValidationNoPacksfound";
		public const string FILE_VALIDATION_MISSING_PACK_XML = "SB_LocalPackManagerResponse_fileValidationMissingPackXML";
		
		public const string FILE_VALIDATION_SUCCESS = "SB_LocalPackManagerResponse_fileValidationSuccess";
		public const string FILE_VALIDATION_FAILED = "SB_LocalPackManagerResponse_fileValidationFailed";
		
		private string _status = "";
		private string _packId = "";
		private object _data = null;
		
		public SB_LocalPackManagerResponse (string status, string packId, object data=null) 
		{
			_status = status;
			_packId = packId;
			_data = data;
		}
		
		public string Status{ get{return _status;} }
		public string PackId{ get{return _packId;} }
		public object Data{ get{return _data;} }
	}
}