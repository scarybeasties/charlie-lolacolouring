﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Data;
using System.Xml;
using System;
using ScaryBeasties.InAppPurchasing.Shop;
using ScaryBeasties.User;
using System.IO;

namespace ScaryBeasties.InAppPurchasing.Local
{
    public class SB_LocalPackManager : MonoBehaviour
    {
        private KeyValueObject _packXMLObjects;
        private ArrayList _xmlQueue = new ArrayList();
        private SB_QueuedPackXML _currentQueuedXML = null;

        private Action<SB_LocalPackManagerResponse> _fileValidationCallback = null;
        private KeyValueObject _fileValidationErrors;
        private int _numPacksToValidate;
        private int _packValidationCount;
        private string[] _validationIgnoreList;

        private Action<bool> _onLoadAllPackXMLsCallback = null;

        // If set to 'true' and all files are validated, no callback will be performed.
        private bool _onlyCallbackIfErrorFound = false;

        // Name of the prefab in the Resources folder.
        public const string resourceName = "Prefabs/SB_LocalPackManager_Prefab";

        private static SB_LocalPackManager _instance = null;
        public static SB_LocalPackManager instance
        {
            get
            {
                if (_instance == null)
                {
                    // Create a new instance of SB_AudioChannels_Prefab, if one doesn't already exist in the scene
                    var singletonsObjectPrefab = (SB_LocalPackManager)Resources.Load(resourceName, typeof(SB_LocalPackManager));

                    // instantiate it
                    _instance = (SB_LocalPackManager)Instantiate(singletonsObjectPrefab);

                }

                return _instance;
            }
        }

        /**
		 * IMPORTANT!
		 * Make sure the prefab does not get unloaded, otherwise we have problems accessing the pack XML objects
		 */
        void Awake()
        {
            DontDestroyOnLoad(transform.gameObject);
        }

        /**
		 * Constructor - Should never be called directly.
		 * Use 'SB_LocalPackManager.instance' to get the singleton
		 */
        public SB_LocalPackManager()
        {
            //_packs = new ArrayList ();
            _packXMLObjects = new KeyValueObject();
        }

        /**
		 * Return an ArrayList of all pack ID's which the user has downloaded.
		 * Search the Asset folder for any .xml files.
		 * (The only xml files which should be there are pack xml files!)
		 */
        public ArrayList GetAllAvailablePackIDs()
        {
            warn(" GetAllAvailablePackIDs()");
            ArrayList arr = new ArrayList();

            DirectoryInfo directory = null;
            FileInfo[] files = null;
            //directory = new DirectoryInfo(SB_ShopUtils.GetLocalAssetDirectoryPath());
            directory = new DirectoryInfo(SB_ShopUtils.GetLocalPackXMLsDirectoryPath());
            warn("Pack XML's directory: " + directory);
            if (directory.Exists)
            {
                files = directory.GetFiles("*.xml");
                foreach (FileInfo file in files)
                {
                    error(" found file: " + file.Name);

                    if (file.Extension.ToLower() == ".xml")
                    {
                        int endIndex = file.Name.IndexOf(".xml");

                        string packID = file.Name.Substring(0, endIndex);
                        warn("packID: " + packID);

                        arr.Add(packID);
                    }
                }
            }
            else
            {
                error(" directory does not exist!!!!! ");
            }

            return arr;
        }

        public void LoadAllPackXMLs(Action<bool> callback = null)
        {
            warn("..LoadAllPackXMLs");
            _onLoadAllPackXMLsCallback = callback;

            ArrayList packIDs = GetAllAvailablePackIDs();
            foreach (string packID in packIDs)
            {
                LoadPackXML(packID, CheckIfAllPackXMLsLoaded);
            }
        }

        public void LoadPackXML(string packId, Action<SB_LocalPackManagerResponse> callback = null)
        {
            warn("..LoadPackXML " + packId);
            AddPackXMLToQueue(packId, callback);

            if (_xmlQueue.Count == 1)
            {
                LoadNextQueuedPackXML();
            }
        }

        private void AddPackXMLToQueue(string packId, Action<SB_LocalPackManagerResponse> callback = null)
        {
            SB_QueuedPackXML queued = new SB_QueuedPackXML(packId, callback);
            _xmlQueue.Add(queued);
        }

        private void LoadNextQueuedPackXML()
        {
            warn("_xmlQueue.Count: " + _xmlQueue.Count);
            if (_xmlQueue.Count == 0)
                return;

            _currentQueuedXML = (SB_QueuedPackXML)_xmlQueue[0];

            string packXmlFilePath = SB_ShopUtils.GetPackXMLFilePath(_currentQueuedXML.packId);
            StartCoroutine(DoLoadPackXML(packXmlFilePath, _currentQueuedXML.packId));
        }

        IEnumerator DoLoadPackXML(string filePath, string packId)
        {
            bool success = false;
#if UNITY_ANDROID

            filePath = "file:///" + filePath;
            warn("DoLoadPackXML filePath: " + filePath);
            WWW www = new WWW(filePath);
            yield return www;


            if (string.IsNullOrEmpty(www.error))
            {
                success = true;
                warn(www.text);

                XmlDocument xml = new XmlDocument();
                xml.LoadXml(www.text);

                _packXMLObjects.SetKeyValue(packId, xml);
            }
            else
            {
                error(www.error);
            }
#else

            if(File.Exists(filePath))
            {
                yield return null;
                string text = File.ReadAllText(filePath);
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(text);

                _packXMLObjects.SetKeyValue(packId, xml);
                success = true;
            }
            else
            {
                error("File doesnt exist");
            }

#endif

            warn("---success: " + success);
            if (_currentQueuedXML.callback != null)
            {
                string status = SB_LocalPackManagerResponse.PACK_XML_LOAD_SUCCESS;
                if (!success)
                {
                    status = SB_LocalPackManagerResponse.PACK_XML_LOAD_FAILED;
                }

                _currentQueuedXML.callback(new SB_LocalPackManagerResponse(status, packId, GetPackXML(packId)));
            }

            _xmlQueue.RemoveAt(0);
            LoadNextQueuedPackXML();
        }

        public XmlDocument GetPackXML(string packId)
        {
            if (_packXMLObjects != null)
            {
                if (_packXMLObjects.ContainsKey(packId))
                {
                    //warn("FOUND PACK ID '"+packId+"' IN _packXMLObjects");
                    return (XmlDocument)_packXMLObjects.GetValue(packId);
                }
            }

            //warn("DID NOT FIND PACK ID '"+packId+"' IN _packXMLObjects!!!!!!!!!!!!!");
            return null;
        }


        /*
		public PackData GetPackData(string packId)
		{
			if(_packDataObjects != null)
			{
				if(_packDataObjects.ContainsKey(packId))
				{
					return (PackData)_packDataObjects.GetValue(packId);
				}
			}

			return null;
		}
		*/

        /**
		 * Delete pack files from local file system
		 */
        public void DeletePack(string packId)
        {
            string packLocation = SB_ShopUtils.GetLocalAssetFilePath(packId);
            System.IO.Directory.Delete(packLocation, true);
        }

        public void DeletePackXML(string packId)
        {
            string packXMLPath = SB_ShopUtils.GetPackXMLFilePath(packId);
            if (File.Exists(packXMLPath))
            {
                File.Delete(packXMLPath);
            }
        }


        /**
		 * Perform a check on all pack files and XML.
		 * 
		 * Check that the XML files exist, and do a check on 
		 * the asset files listed in the XML.
		 * 
		 * @callback 
		 * @onlyCallbackIfErrorFound - If set to 'true' and all files are validated, no callback will be performed.
		 */
        public void ValidateFiles(Action<SB_LocalPackManagerResponse> callback, bool onlyCallbackIfErrorFound = false, string[] validationIgnoreList = null)
        {
            warn("ValidateFiles");

            _fileValidationCallback = callback;
            _onlyCallbackIfErrorFound = onlyCallbackIfErrorFound;
            _validationIgnoreList = validationIgnoreList;

            warn("..getting user profile");
            SB_ProfileVO userProfile = SB_UserProfileManager.instance.CurrentProfile;

            warn("..checking user profile, installed IAP pack count");
            // Are any packs installed, and do they exist in the userprofile.xml?
            if (userProfile.installedIAPPacks.Count == 0)
            {
                warn("..user profile IAP pack count was zero");
                if (_fileValidationCallback != null)
                {
                    _fileValidationCallback(new SB_LocalPackManagerResponse(SB_LocalPackManagerResponse.FILE_VALIDATION_NO_PACKS_FOUND, ""));
                }

                // STOP HERE!
                return;
            }
            else
            {
                warn("..user profile IAP pack count was NOT zero!!!");
            }

            /*
			try
			{
			*/
            // Store any pack id's, whenever a file is not found
            _fileValidationErrors = new KeyValueObject();
            _numPacksToValidate = 0;
            _packValidationCount = 0;

            warn("..checking user's installed IAP pack ids");
            // Check that each pack xml file exists on the filesystem
            foreach (string packId in userProfile.installedIAPPacks.Keys)
            {
                string val = (string)userProfile.installedIAPPacks.GetValue(packId);
                bool packXmlFound = SB_ShopUtils.PackXmlExists(packId);
                warn("\t\t\t packId: " + packId + ", val: " + val + ", packXmlFound: " + packXmlFound);

                if (!packXmlFound)
                {
                    // Store the pack id
                    _fileValidationErrors.SetKeyValue(packId, false);
                }
                else
                {
                    // Queue up the pack xml file - do not load it yet!!
                    _numPacksToValidate++;
                    AddPackXMLToQueue(packId, ValidatePackXMLAssets);
                }
            }
            /*
			}
			catch(InvalidCastException e)
			{
				error ("\t\t\t 2-------------  " + e);
			}
			*/

            if (_xmlQueue.Count > 0)
            {
                // Load the xml file and then validate each asset
                LoadNextQueuedPackXML();
            }
            else
            {
                // There were no pack xml files to load
                _fileValidationCallback(new SB_LocalPackManagerResponse(SB_LocalPackManagerResponse.FILE_VALIDATION_MISSING_PACK_XML, "", _fileValidationErrors));
            }
        }

        void CheckIfAllPackXMLsLoaded(SB_LocalPackManagerResponse response)
        {
            warn("CheckIfAllPackXMLsLoaded _packXMLObjects.Count: " + _packXMLObjects.Count);

            if (_packXMLObjects.Count == GetAllAvailablePackIDs().Count)
            {
                if (_onLoadAllPackXMLsCallback != null)
                {
                    _onLoadAllPackXMLsCallback(true);
                }
            }
        }

        void ValidatePackXMLAssets(SB_LocalPackManagerResponse response)
        {
            _packValidationCount++;
            warn("\t\t\t ValidatePackXMLAssets: " + response.PackId + ", " + _packValidationCount + "/" + _numPacksToValidate);

            // Get all XML nodes in the document called <asset>
            XmlDocument xml = (XmlDocument)response.Data;
            XmlNodeList assetList = xml.SelectNodes("//asset");

            warn("list.Count: " + assetList.Count);
            foreach (XmlNode asset in assetList)
            {
                string assetFilePath = asset.InnerText;
                if (asset.SelectSingleNode("image") != null)
                {
                    // Bodge.. DrWho bios have an <image> subnode, which contains the actual asset file path
                    assetFilePath = asset.SelectSingleNode("image").InnerText;
                }

                // Only check <asset> nodes which have an InnerText (asset file path) set
                if (assetFilePath.Length > 0)
                {
                    bool doValidateFile = true;
                    if (_validationIgnoreList != null)
                    {
                        foreach (string ignoreFile in _validationIgnoreList)
                        {
                            if (assetFilePath.Contains(ignoreFile))
                            {
                                doValidateFile = false;
                            }
                        }
                    }

                    if (doValidateFile)
                    {
                        string assetPath = SB_ShopUtils.GetLocalAssetDirectoryPath() + "/" + assetFilePath;
                        bool assetFileExists = File.Exists(assetPath);
                        warn("\tasset: " + assetFilePath + ", exists: " + assetFileExists);

                        if (!assetFileExists)
                        {
                            // Store the pack id, of the failed asset
                            _fileValidationErrors.SetKeyValue(response.PackId, false);
                        }
                    }
                }
            }

            // Have we finished validating all packs?
            if (_packValidationCount == _numPacksToValidate)
            {
                warn("------------------------------------------");
                warn("FINISHED VALIDATING PACKS count: " + _packValidationCount + "/" + _numPacksToValidate);
                warn("_fileValidationErrors.Count " + _fileValidationErrors.Count);
                foreach (string key in _fileValidationErrors.Keys)
                {
                    bool val = (bool)_fileValidationErrors.GetValue(key);
                    warn("\t\t\t key: " + key + ", val: " + val);
                }
                warn("------------------------------------------");


                if (_fileValidationCallback != null)
                {
                    if (_fileValidationErrors.Count == 0)
                    {
                        warn("Validation success!");
                        // SUCCESS!! All files found!
                        if (!_onlyCallbackIfErrorFound)
                        {
                            _fileValidationCallback(new SB_LocalPackManagerResponse(SB_LocalPackManagerResponse.FILE_VALIDATION_SUCCESS, ""));
                        }
                    }
                    else
                    {
                        warn("Validation failed!");
                        // FAIL!! There are files missing
                        _fileValidationCallback(new SB_LocalPackManagerResponse(SB_LocalPackManagerResponse.FILE_VALIDATION_FAILED, "", _fileValidationErrors));
                    }
                }
            }
        }

        string GetTimeStamp()
        {
            DateTime now = DateTime.Now;

            string hrs = FormatTimeStamp(now.TimeOfDay.Hours.ToString());
            string mins = FormatTimeStamp(now.TimeOfDay.Minutes.ToString());
            string secs = FormatTimeStamp(now.TimeOfDay.Seconds.ToString());
            string ms = FormatTimeStamp(now.TimeOfDay.Milliseconds.ToString());

            return hrs + ":" + mins + ":" + secs + ":" + ms;
        }

        string FormatTimeStamp(string str)
        {
            if (str.Length == 1)
            {
                str = "0" + str;
            }

            return str;
        }

        protected void log(object msg)
        {
            Debug.Log(GetTimeStamp() + "[" + this.GetType() + "] " + msg);
        }

        protected void warn(object msg)
        {
            Debug.LogWarning(GetTimeStamp() + "[" + this.GetType() + "] " + msg);
        }

        protected void error(object msg)
        {
            Debug.LogError(GetTimeStamp() + "[" + this.GetType() + "] " + msg);
        }
    }
}