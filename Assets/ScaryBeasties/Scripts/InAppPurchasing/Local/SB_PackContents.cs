﻿
using System.Xml;
using System.Collections;
using UnityEngine;
using ScaryBeasties.InAppPurchasing.Shop;

//------------------------------------
// Not sure if this class is required anymore, seeing as packs are no longer in their own separate folders
//------------------------------------

namespace ScaryBeasties.InAppPurchasing.Local
{
	public class SB_PackContents : Object
	{
		protected XmlDocument _xml = null;
		protected string _packId;

		// Overidden by subclasses
		protected virtual void Init(XmlDocument xml)
		{
			_xml = xml;

			if (_xml.FirstChild.Attributes.GetNamedItem ("id") != null) 
			{
				_packId = _xml.FirstChild.Attributes.GetNamedItem ("id").Value;
			}
		}

		// Parse XML nodes
		protected virtual void SetupPackAssets(string assetPath, ArrayList arr)
		{
			foreach(XmlNode xmlNode in _xml.SelectNodes (assetPath))
			{
				//Debug.Log("Found background " + xmlNode.InnerText);
				SB_PackAsset packAsset = new SB_PackAsset();
				packAsset.ParseXML(xmlNode);
				
				arr.Add (packAsset);
			}
		}

		// Getters
		public string PackId			{	get	{return _packId;}	}
		public string PackFolder		{	get	{return SB_ShopUtils.GetLocalAssetDirectoryPath() + "/" + _packId + "_" + tk2dSystem.CurrentPlatform;}	} // TEMP! replace 1x with tk2dSystem.CurrentPlatform;

		public virtual string ToString()
		{
			string str = "PackId: " + PackId + ", PackFolder: " + PackFolder + "\n";
			return str;
		}
	}
}