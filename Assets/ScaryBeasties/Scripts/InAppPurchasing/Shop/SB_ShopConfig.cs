﻿using System.Xml;
using UnityEngine;

namespace ScaryBeasties.InAppPurchasing.Shop
{
	/**
	 * Stores shop configuration data.
	 */
	public class SB_ShopConfig
	{
		public int NewItemsVersion = 0;
		public string ShopImagePath = "";

		public SB_ShopConfig(XmlNode xml)
		{
			if(xml == null) return;
			
			NewItemsVersion = int.Parse(xml.SelectSingleNode("shop/newItemsVersion").InnerText);
			ShopImagePath = xml.SelectSingleNode("shop/imagePath").InnerText;
				
			Debug.LogWarning("--------------------------");
			Debug.LogWarning("NewItemsVersion: " + NewItemsVersion);
			Debug.LogWarning("ShopImagePath: " + ShopImagePath);
			Debug.LogWarning("--------------------------");
		}
		
		override public string ToString()
		{
			string str = "[ShopConfig] ";
			str += ", NewItemsVersion: " + NewItemsVersion;
			str += ", ShopImagePath: " + ShopImagePath;
			
			return str;
		}
	}
}
