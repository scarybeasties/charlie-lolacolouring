﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml;

namespace ScaryBeasties.InAppPurchasing.Shop
{
	public class SB_PackItem {

		public string packID = "";
		public float packVersion = 0f;
		public string title = "";
		public string subtitle = "";
		public string image = "";
		public string imageType = "jpg"; // Set a default value, for now.
		
		//public int numItems = 0; // Number of child items
		
		public bool purchased = false;
		//public bool isFree = false;
		public bool downloaded = false;
		
		public XmlElement Xml = null;
		
		public SB_PackItem(XmlElement xml)
		{
			Xml = xml;
			
			packID = xml.GetAttribute("id");
			if(xml.HasAttribute("version")) packVersion = float.Parse(xml.GetAttribute("version"));

			title = xml.SelectSingleNode("title").InnerText;
			subtitle = xml.SelectSingleNode("subtitle").InnerText;
			//numItems = int.Parse(xml.SelectSingleNode("num_items").InnerText);
			
			// Get image name, and its file type
			XmlNode imageNode = xml.SelectSingleNode("image");
			image = imageNode.InnerText;
			if(imageNode.Attributes.GetNamedItem("type") != null) imageType = imageNode.Attributes.GetNamedItem("type").Value;
			
			image += "." + imageType;
		}

		public virtual string ToString()
		{
			string str = "[SB_PackItem] packID: " + packID;
			str += ", version: " + packVersion;
			str += ", title: " + title;
			str += ", subtitle: " + subtitle;
			str += ", image: " + image;
			str += ", purchased: " + purchased;
			//str += ", numItems: " + numItems;
			
			return str;
		}
	}
}
