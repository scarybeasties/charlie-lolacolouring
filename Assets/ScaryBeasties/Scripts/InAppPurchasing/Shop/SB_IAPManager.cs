﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Collections.Generic;
using OnePF;
using System;
using ScaryBeasties.InAppPurchasing.Local;

namespace ScaryBeasties.InAppPurchasing.Shop
{
	public class SB_IAPManager 
	{
		private ArrayList _iaps;

		private int _state = 0;
		public static int STATE_INIT = 0;
		public static int STATE_XML_LOADED = 1;
		public static int STATE_COMPLETE = 2;

		private SB_ShopConfig _shopConfig = null;

		public string _lastXMLLoadDate;
		private const int XML_CACHE_SECONDS = 60;

		private static SB_IAPManager _instance = null;
		public static SB_IAPManager instance
		{
			get 
			{
				if(_instance == null)
				{
					_instance = new SB_IAPManager();
					
					// Create an object so it won't be null
					_instance.ShopConfig = new SB_ShopConfig(null);
				}
			
				return _instance;
			}
		}

		// Returns the SB_ShopConfig object, containing currency unit and other settings
		public SB_ShopConfig ShopConfig
		{
			get	{	return _shopConfig;	}
			set	{	_shopConfig = value;}
		}


		/**
		 * Constructor - Should never be called directly.
		 * Use 'SB_IAPManager.instance' to get the singleton
		 */
		public SB_IAPManager()
		{
			ClearIAPList();
		}
		
		private void ClearIAPList()
		{
			_iaps = new ArrayList();
		}

		/*
		 * Check for XML being Loaded
		 */
		public bool LoadXML(){

			if (_lastXMLLoadDate == null)
				return true;

			DateTime now = DateTime.Now;
			DateTime last = DateTime.Parse(_lastXMLLoadDate);
			TimeSpan timeDif = now.Subtract (last);

			if (timeDif.Seconds > XML_CACHE_SECONDS) {
				return true;
			}

			return false;

		}

		/**
		 * Test if IAP list has been loaded
		 */
		public bool IAPListReady()
		{
			Debug.Log("[SB_IAPManager] IAPListReady() _state: " + _state + ", _iaps.Count: " + _iaps.Count);
			
			if (_state == STATE_COMPLETE) 
			{
				if(_iaps.Count>0)
				{
					for(int j=0;j<_iaps.Count;j++)
					{
						SB_IAPItem iap = (SB_IAPItem) _iaps[j];
						Debug.Log("[SB_IAPManager] iap: "+ iap.ToString());
						if(iap.availableInStore) return true;
					}
					
				}
				return true;
			}
			
			#if UNITY_EDITOR
				// If testing in the IDE (Editor), then always return 'true', 
				// otherwise we'll be waiting for a response from the store, which we won't receive..
				// (Store response only happens when testing on a mobile device)
				return true;
			#endif
			
			return false;
		}

		public bool IAPXMLListReady(){
			if (_state == STATE_XML_LOADED) {
				if(_iaps.Count>0){
					return true;
				}
			}
			return false;
		}

		/**
		 * Handle the IAP list
		 */
		//public void AddXMLList(string xmlString)
		public void AddXMLList(string shopDataXmlString, string shopCopyXmlString)
		{
			ClearIAPList();
		
			XmlDocument iapXML = new XmlDocument();
			iapXML.LoadXml(shopDataXmlString);
			
			XmlDocument copyXML = new XmlDocument();
			copyXML.LoadXml(shopCopyXmlString);
			
			_shopConfig = new SB_ShopConfig(copyXML.SelectSingleNode("shopCopy/config"));
			
			XmlNodeList iapXMLList = iapXML.SelectNodes("shopData/iapList/iap");
			XmlNodeList copyXMLList = copyXML.SelectNodes("shopCopy/iapList/iap");
			foreach(XmlElement iapXMLItem in iapXMLList)
			{
				// NOTE:
				// The iap copy (title, description, moreinfo, images etc..) comes from the shopcopy.xml file
				// We need to find a matching SKU for the iap item from the shopcopy.xml and shopdata.xml
				
				// SKU from the shopdata.xml
				string iapSku = iapXMLItem.Attributes.GetNamedItem("sku").Value;
				
				foreach(XmlElement copyXMLItem in copyXMLList)
				{
					// SKU from shopcopy.xml
					string tmpSku = copyXMLItem.Attributes.GetNamedItem("sku").Value;
					
					// Do SKU's match?
					if(iapSku == tmpSku)
					{
						SB_IAPItem iapItem = new SB_IAPItem(iapXMLItem, copyXMLItem);
						AddIAPItem(iapItem);
						//break;
					}
				}
			}
			
			_state = STATE_XML_LOADED;
			
			DateTime now = DateTime.Now;
			_lastXMLLoadDate = now.ToString ();
		}

		public void AddIAPItem(SB_IAPItem item)
		{
			_iaps.Add (item);
		}

		public void SetupStoreAvailableIAPs(List<SkuDetails> storeiaps, List<string> purchasediaps){

			UpdateIAPs (storeiaps, purchasediaps);

			_state = STATE_COMPLETE;

		}

		public bool UpdateIAPs(List<SkuDetails> storeiaps, List<string> purchasediaps)
		{
			bool purchaseUpdate = false;
			ArrayList previousPurchases = new ArrayList ();

			for (int j=0; j<_iaps.Count; j++) {
				SB_IAPItem iap = (SB_IAPItem)_iaps [j];

				if(iap.purchased){
					previousPurchases.Add(iap.sku);
				}

				iap.purchased=false;
				iap.availableInStore=false;
			}

			Debug.Log("------------------------ UpdateIAPs() ------------------------");
			Debug.Log("storeiaps.Count: " + storeiaps.Count);
			Debug.Log("_iaps.Count: " + _iaps.Count);
			for(int i =0;i<storeiaps.Count;i++)
			{
				SkuDetails tsku = (SkuDetails) storeiaps[i];
				
				for(int j=0;j<_iaps.Count;j++)
				{
					SB_IAPItem iap = (SB_IAPItem) _iaps[j];
					Debug.Log("iap.sku: " + iap.sku + ", tsku.Sku: " + tsku.Sku);
					//if(iap.sku==tsku.Sku)
					if(iap.matchingSku(tsku.Sku))
					{
						iap.availableInStore=true;
					}
				}
			}
			
			for(int i =0;i<purchasediaps.Count;i++)
			{
				string tsku = (string) purchasediaps[i];
				
				for(int j=0;j<_iaps.Count;j++)
				{
					SB_IAPItem iap = (SB_IAPItem) _iaps[j];
					if(iap.sku==tsku)
					{
						SetPurchased(iap);
						if(!previousPurchases.Contains(tsku))
						{
							purchaseUpdate=true;
						}
					}
				}
			}

			return purchaseUpdate;
		}

		public bool RestoreIAPPurchase(string sku)
		{
			return PurchaseIAP(sku);
		}

		public bool PurchaseIAP(string sku)
		{	
			bool found = false;
			
			for(int j=0;j<_iaps.Count;j++)
			{
				SB_IAPItem iap = (SB_IAPItem) _iaps[j];
				if(iap.matchingSku(sku))
				{
					if(!iap.purchased)
					{	
						iap.availableInStore=true;
						SetPurchased(iap);
						found = true;
					}
				}
			}
			
			return found;
		}

		private void SetPurchased(SB_IAPItem iap){
			iap.purchased=true;
			ArrayList packfiles = SB_LocalPackManager.instance.GetAllAvailablePackIDs();

			for(int i=0;i<iap.packIDs.Count;i++){

				SB_PackItem pack = SB_PackManager.instance.GetPackItemWithID((string) iap.packIDs[i]);
				if(pack!=null){
					pack.purchased=true;

					SB_IAPItem titem = SB_IAPManager.instance.GetIAPItemWithSku(pack.packID);
					if(titem!=null){
						if(packfiles.Contains(pack.packID)){
							titem.toDownload=false;
						}
						else{
							titem.toDownload=true;
						}
					}

				}

				RemoveBundlesContainingPackID((string) iap.packIDs[i]);
							

			}



			SB_PackManager.instance.CheckDownloadedPacks ();
		}

		public void RemoveBundlesContainingPackID(string packID){

			for(int j=0;j<_iaps.Count;j++)
			{
				SB_IAPItem iap2 = (SB_IAPItem) _iaps[j];
				for(int k=0;k<iap2.packIDs.Count;k++){
					string id2 = (string) iap2.packIDs[k];
					if(id2==packID){
						//iap2.availableInStore = false;
						iap2.purchased=true;
					}
				}
			}

		}


		public SB_IAPItem GetIAPItemAtIndex(int i)
		{
			return (SB_IAPItem) _iaps [i];
		}

		public SB_IAPItem GetIAPItemWithSku(string sku)
		{
			for (int j=0; j<_iaps.Count; j++)
			{
				SB_IAPItem iap = (SB_IAPItem)_iaps [j];
				if (iap.matchingSku(sku)) 
				{
					return iap;
				}
			}
			return null;
		}

		public int IAPItemCount()
		{
			return _iaps.Count;
		}

	}

}
