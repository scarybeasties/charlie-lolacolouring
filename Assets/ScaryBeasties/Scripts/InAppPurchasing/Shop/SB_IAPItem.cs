using UnityEngine;
using System.Collections;
using System.Xml;
using ScaryBeasties.Utils;

namespace ScaryBeasties.InAppPurchasing.Shop
{
	public class SB_IAPItem : Object
	{
		public const int TYPE_BUY_NOW = 0;
		public const int TYPE_COMING_SOON = 1;
		public const int TYPE_NEW = 2;
		public const int TYPE_FREE = 3;
	
		public string sku = "";
		public string storeID;
		
		public string title = "";
		public string subtitle = "";
		public string infoTitle = "";
		public string infoDescription = "";
		public string infoColourHex = "#000000";
		public string cost = "";
		
		public int type = 0;
		public bool enabled = true;
		public bool availableInStore = false;
		public bool purchased = false;

		public bool toDownload = false;
		//public bool isFree = false;

		public string image = ""; // Image shown in shop carousel
		public string detailImage = ""; // Image shown in the more info page, before user buys the item
		public string imageType = "jpg"; // Set a default value, for now.

		public ArrayList packIDs = new ArrayList();
		//public ArrayList packs = new ArrayList();
		
		// Constructor
		public SB_IAPItem(XmlElement dataXml, XmlElement copyXml)
		{
			if (dataXml == null || copyXml == null)return;

			sku = dataXml.Attributes.GetNamedItem("sku").Value;
			storeID = dataXml.Attributes.GetNamedItem("storeID").Value;
			
			XmlNodeList packIDsList = dataXml.SelectNodes("packs/pack");
			foreach(XmlElement packIDItem in packIDsList)
			{
				packIDs.Add (packIDItem.InnerText);	
			}
			
			if(dataXml.Attributes.GetNamedItem("type") != null) type = int.Parse(dataXml.Attributes.GetNamedItem("type").Value);			
			if(dataXml.Attributes.GetNamedItem("enabled") != null && dataXml.Attributes.GetNamedItem("enabled").Value.ToLower() == "false") enabled = false;
			
			title = copyXml.SelectSingleNode("copy/title").InnerText;
			subtitle = copyXml.SelectSingleNode("copy/subtitle").InnerText;
			infoTitle = copyXml.SelectSingleNode("copy/info_title").InnerText;
			infoDescription = copyXml.SelectSingleNode("copy/info_description").InnerText;
			infoColourHex = copyXml.SelectSingleNode("copy/info_colour").InnerText;
			
			// Get image name, and its file type
			XmlNode imageNode = copyXml.SelectSingleNode("image");
			image = imageNode.InnerText;
			if(imageNode.Attributes.GetNamedItem("type") != null) imageType = imageNode.Attributes.GetNamedItem("type").Value;
			
			detailImage = image + "_detail." + imageType;
			image += "." + imageType;
		}
		
		public bool matchingSku(string tsku)
		{
			if (tsku == sku || tsku == storeID) 
				return true;
			
			return false;
		}
		
		public bool ContainsPack(string packID)
		{
			for (int i =0; i<packIDs.Count; i++) 
			{
				string pckID = (string)packIDs[i];
				if(pckID == packID)
				{
					return true;
				}
			}
			return false;
			
		}
		
		public bool ContainsPackFromIAP(SB_IAPItem item)
		{
			for (int i=0; i<packIDs.Count; i++) 
			{	
				string pckID = (string)packIDs[i];
				if(item.ContainsPack(pckID))
				{
					return true;
				}
			}
			
			return false;
		}
		
		/*
		public void AddPack(SB_PackItem item)
		{
			packs.Add (item);
		}

		public bool ContainsPack(string packID)
		{
			for (int i =0; i<packs.Count; i++) 
			{
				SB_PackItem pack = (SB_PackItem) packs[i];
				if(pack.packID==packID)
				{
					return true;
				}
			}
			return false;

		}

		public bool ContainsPackFromIAP(SB_IAPItem item)
		{
			for (int i=0; i<packs.Count; i++) 
			{	
				SB_PackItem pack = (SB_PackItem) packs[i];
				for(int j=0;j<item.packs.Count;j++)
				{
					SB_PackItem pack2 = (SB_PackItem) item.packs[j];

					if(pack.packID==pack2.packID)
					{
						return true;
					}
				}
			}

			return false;
		}
		*/
		
		// GETTERS
		public bool isNew {get{return type == TYPE_NEW;}}
		public bool isFree {get{return type == TYPE_FREE;}}
		public bool isComingSoon {get{return type == TYPE_COMING_SOON;}}

		public string typeStr 
		{
			get
			{
				string configXMLItem = "";
				switch(type)
				{
					case TYPE_BUY_NOW: 		configXMLItem = "item_type_buynow"; 		break;
					case TYPE_COMING_SOON: 	configXMLItem = "item_type_comingsoon"; 	break;
					case TYPE_NEW: 			configXMLItem = "item_type_new"; 			break;
					case TYPE_FREE: 		configXMLItem = "item_type_free"; 			break;
				}

				if(toDownload) configXMLItem = "item_purchased";

				return SB_Utils.GetNodeValueFromConfigXML ("config/copy/shop/text", configXMLItem);
			}
		}
		
		public string ToString()
		{
			string str = "[SB_IAPItem]";
					str += ", sku: " + sku;
					str += ", isNew: " + isNew;
					str += ", isFree: " + isFree;
					str += ", isComingSoon: " + isComingSoon;
					str += ", enabled: " + enabled;
					str += ", availableInStore: " + availableInStore;
					str += ", purchased: " + purchased;
					str += ", image: " + image;
					str += ", packIDs: " + packIDs.Count;
					str += ", displayTitle: " + title;
					str += ", displayDescription: " + subtitle;
					str += ", infoTitle: " + infoTitle;
					str += ", infoDescription: " + infoDescription;
					str += ", infoColourHex: " + infoColourHex;
					str += ", cost: " + cost;
			
			return str;
		}
	}
}
