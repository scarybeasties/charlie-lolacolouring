﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Collections.Generic;
using OnePF;
using System;
using ScaryBeasties.InAppPurchasing.Local;

namespace ScaryBeasties.InAppPurchasing.Shop
{
	public class SB_PackManager {
		
		private ArrayList _packs;

		private static SB_PackManager _instance = null;
		public static SB_PackManager instance
		{
			get 
			{
				if(_instance == null)
				{
					_instance = new SB_PackManager();
				}
				
				return _instance;
			}
		}
		
		
		/**
		 * Constructor - Should never be called directly.
		 * Use 'SB_PackItem.instance' to get the singleton
		 */
		public SB_PackManager()
		{
			ClearPacksList();
		}
		
		private void ClearPacksList()
		{
			_packs = new ArrayList();
		}
		
		/**
		 * Handle the Pack list
		 */
		//public void AddXMLList(string xmlString)
		public void AddXMLList(string shopDataXmlString, string shopCopyXmlString)
		{
			ClearPacksList();
			
			//XmlDocument packXML = new XmlDocument();
			//packXML.LoadXml(shopDataXmlString);
			
			XmlDocument copyXML = new XmlDocument();
			copyXML.LoadXml(shopCopyXmlString);
			
			//XmlNodeList packXMLList = packXML.SelectNodes("shopData/packList/pack");
			XmlNodeList packXMLList = copyXML.SelectNodes("shopCopy/packList/pack");
			
			foreach(XmlElement packXMLItem in packXMLList)
			{
				SB_PackItem packItem = new SB_PackItem(packXMLItem);
				Debug.LogWarning("NEW packItem: " + packItem.ToString());
				
				if(GetPackItemWithID(packItem.packID) == null)
				{
					Debug.LogWarning("...adding packItem");
					AddPackItem(packItem);
				}
			}
			
			DateTime now = DateTime.Now;
			
			/*
			XmlDocument packXML = new XmlDocument();
			packXML.LoadXml(xmlString);
			XmlNodeList packXMLList = packXML.SelectNodes("shopData/packList/pack");
			
			foreach(XmlElement packXMLItem in packXMLList)
			{
				SB_PackItem packItem = new SB_PackItem(packXMLItem);

				if(GetPackItemWithID(packItem.packID) == null)
				{
					AddPackItem(packItem);
				}
			}

			DateTime now = DateTime.Now;
			*/
		}

		public void CheckDownloadedPacks(){

			ArrayList packfiles = SB_LocalPackManager.instance.GetAllAvailablePackIDs();

			Debug.LogError("CHECKING DEFAULT PACK");
			for(int i=0;i<_packs.Count;i++){
				SB_PackItem pack = (SB_PackItem) _packs[i];
				if(pack.packID=="default_pack"){
					pack.downloaded=true;
					pack.purchased=true;
					Debug.LogError("ADDED DEFAULT PACK");
				}
				else if(packfiles.Contains(pack.packID)){
					pack.downloaded=true;
					pack.purchased=true;

					SB_IAPManager.instance.RemoveBundlesContainingPackID(pack.packID);

					SB_IAPItem titem = SB_IAPManager.instance.GetIAPItemWithSku(pack.packID);
					if(titem!=null){
						titem.toDownload=false;
					}
				}
			}

		}

		public void AddPackItem(SB_PackItem item)
		{
			SB_PackItem tmp = GetPackItemWithID (item.packID);
			if(tmp==null) _packs.Add (item);
		}
		
		public SB_PackItem GetPackItemAtIndex(int i)
		{
			if(_packs != null)
			{
				if(i >= 0 && i < _packs.Count)
				{
					return (SB_PackItem) _packs [i];
				}
			}
			
			return null;	
		}
		
		public SB_PackItem GetPackItemWithID(string id){
			
			for (int j=0; j<_packs.Count; j++) {
				SB_PackItem pack = (SB_PackItem)_packs [j];
				if (pack.packID==id) {
					return pack;
				}
			}
			return null;
		}
		
		
		public int PackItemCount(){
			return _packs.Count;
		}
		
	}
	
}
