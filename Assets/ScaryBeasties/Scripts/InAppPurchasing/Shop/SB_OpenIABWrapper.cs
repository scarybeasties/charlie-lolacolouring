﻿using UnityEngine;
using System.Collections;
using OnePF;
using System.Collections.Generic;
using ScaryBeasties.User;

namespace ScaryBeasties.InAppPurchasing.Shop
{
	//public class SB_OpenIABWrapper : MonoBehaviour 
	public class SB_OpenIABWrapper
	{
		public const string BILLING_SUPPORTED = "ShopBillingSupported";
		public const string BILLING_NOT_SUPPORTED = "ShopBillingNotSupported";
		public const string QUERY_INVENTORY_SUCCESS = "QueryInventorySuccess";
		public const string QUERY_INVENTORY_FAILED = "QueryInventoryFailed";
		public const string PURCHASE_SUCCESS = "PurchaseSuccess";
		public const string PURCHASE_FAILED = "PurchaseFailed";
		public const string CONSUMABLE_PURCHASE_SUCCESS = "ConsumablePurchaseSuccess";
		public const string CONSUMABLE_PURCHASE_FAILED = "ConsumablePurchaseFailed";
		public const string TRANSACTION_RESTORED = "TransactionRestored";
		public const string RESTORE_SUCCESS = "RestoreSuccess";
		public const string RESTORE_FAILED = "RestoreFailed";
	
		public delegate void SB_OpenIABWrapperEventDelegate(SB_OpenIABWrapper shopItemHandler,string eventType,object data=null);
		public event SB_OpenIABWrapperEventDelegate OnSB_OpenIABWrapperEvent;
		
		private bool _isInitialized = false;
		private bool _restoredTransactions = false;
		private Inventory _inventory = null;
		
		// We only need to initialise OpenIAB once
		public static bool OPEN_IAB_INIT_DONE = false;
		
		/**
		 * Returns a string array of the shop IAP sku's.
		 * Note: These are NOT the same as the store SKU's!
		 *		 Returns the sku's defined in the shopdata.xml file
		 */
		public string[] GetIAPSkus()
		{
			int numIAPs = SB_IAPManager.instance.IAPItemCount();
			string[] skus = new string[numIAPs];
			
			for(int i=0; i<numIAPs; i++)
			{	
				SB_IAPItem iapItem = SB_IAPManager.instance.GetIAPItemAtIndex(i);
				skus.SetValue(iapItem.sku, i);
			}
			
			return skus;
		}
		
		public string[] GetIAPStoreIDs()
		{
			int numIAPs = SB_IAPManager.instance.IAPItemCount();
			string[] storeIDs = new string[numIAPs];
			
			for(int i=0; i<numIAPs; i++)
			{	
				SB_IAPItem iapItem = SB_IAPManager.instance.GetIAPItemAtIndex(i);
				storeIDs.SetValue(iapItem.storeID, i);
			}
			
			return storeIDs;
		}
		
		/**
		 * Returns the shop inventory
		 */
		public Inventory Inventory
		{
			get { return _inventory; }
		}
		
		public bool RestoredTransactions
		{
			get { return _restoredTransactions; }
		}
		
		/*
		 * Restore transaction handling - IOS only!!!
		 */
		public void RestoreTransactions()
		{
			#if UNITY_IOS
			_restoredTransactions = false;
			OpenIAB.restoreTransactions();
			#endif
		}
		
		public void QueryInventory()
		{
			Debug.LogError("[SB_OpenIABWrapper] QueryInventory ----------------------------------------");
			
			#if UNITY_IOS
			OpenIAB.queryInventory();
			#else
			// For Android, we need to call 'queryInventory()' and pass it a string array of SKU's.
			// This will return the products which are available in the store, which match the mapped SKU's.
			// No need to do this for iOS.
			OpenIAB.queryInventory(GetIAPSkus());
			#endif
		}
		
		public void Init()
		{
			InitStoreEvents();
			
			/*
			if(OPEN_IAB_INIT_DONE)
			{
				Debug.LogError("------------------------------------------------------");
				Debug.LogError("[SB_OpenIABWrapper] Init already done!! Returning..");
				Debug.LogError("------------------------------------------------------");
				
				if(_isInitialized)
				{
					QueryInventory();
				}
				return;
				
			}
			*/
			
			
			InitStoreSKUs();
			
			/*
			if(!OPEN_IAB_INIT_DONE)
			{
				InitStoreSKUs();
			}
			else
			{
				Debug.LogError("[SB_OpenIABWrapper] SKUs have already been mapped");
			}
			*/
			
			var options = new Options();
			options.checkInventoryTimeoutMs = Options.INVENTORY_CHECK_TIMEOUT_MS * 2;
			options.discoveryTimeoutMs = Options.DISCOVER_TIMEOUT_MS * 2;
			options.checkInventory = false;
			options.verifyMode = OptionsVerifyMode.VERIFY_SKIP;
			
			if(SB_Globals.PLATFORM_ID == SB_BuildPlatforms.GooglePlay)
			{
				options.prefferedStoreNames = new string[] { OpenIAB_Android.STORE_GOOGLE };
				options.availableStoreNames = new string[] { OpenIAB_Android.STORE_GOOGLE };
				options.storeKeys = new Dictionary<string, string> { {OpenIAB_Android.STORE_GOOGLE, SB_Globals.SHOP_GOOGLE_KEY} };
			}
			else if(SB_Globals.PLATFORM_ID == SB_BuildPlatforms.Amazon)
			{
				options.prefferedStoreNames = new string[] { OpenIAB_Android.STORE_AMAZON };
				options.availableStoreNames = new string[] { OpenIAB_Android.STORE_AMAZON };
				
				// NOTE: Amazon doesn't need a store key
			}
			else if(SB_Globals.PLATFORM_ID == SB_BuildPlatforms.Samsung)
			{
				// GT - 27.08.2015
				// Not sure what to do here... 
				// Maybe use the same code as GooglePlay?
			}
			
			//options.storeSearchStrategy = SearchStrategy.INSTALLER;
			options.storeSearchStrategy = SearchStrategy.BEST_FIT;
			//options.storeSearchStrategy = SearchStrategy.INSTALLER_THEN_BEST_FIT;
			
			OpenIAB.init(options);
			Debug.LogError("SHOP INIT");
			
			OPEN_IAB_INIT_DONE = true;
		}
		
		public void InitStoreEvents()
		{
			// Listen to all events for illustration purposes
			OpenIABEventManager.billingSupportedEvent += billingSupportedEvent;
			OpenIABEventManager.billingNotSupportedEvent += billingNotSupportedEvent;
			OpenIABEventManager.queryInventorySucceededEvent += queryInventorySucceededEvent;
			OpenIABEventManager.queryInventoryFailedEvent += queryInventoryFailedEvent;
			OpenIABEventManager.purchaseSucceededEvent += purchaseSucceededEvent;
			OpenIABEventManager.purchaseFailedEvent += purchaseFailedEvent;
			OpenIABEventManager.consumePurchaseSucceededEvent += consumePurchaseSucceededEvent;
			OpenIABEventManager.consumePurchaseFailedEvent += consumePurchaseFailedEvent;
			OpenIABEventManager.transactionRestoredEvent += OnTransactionRestored;
			OpenIABEventManager.restoreFailedEvent += OnRestoreFailed;
			OpenIABEventManager.restoreSucceededEvent += OnRestoreSucceeded;
		}
		
		public void DisableStoreEvents()
		{
			// Remove all event handlers
			OpenIABEventManager.billingSupportedEvent -= billingSupportedEvent;
			OpenIABEventManager.billingNotSupportedEvent -= billingNotSupportedEvent;
			OpenIABEventManager.queryInventorySucceededEvent -= queryInventorySucceededEvent;
			OpenIABEventManager.queryInventoryFailedEvent -= queryInventoryFailedEvent;
			OpenIABEventManager.purchaseSucceededEvent -= purchaseSucceededEvent;
			OpenIABEventManager.purchaseFailedEvent -= purchaseFailedEvent;
			OpenIABEventManager.consumePurchaseSucceededEvent -= consumePurchaseSucceededEvent;
			OpenIABEventManager.consumePurchaseFailedEvent -= consumePurchaseFailedEvent;
			OpenIABEventManager.transactionRestoredEvent -= OnTransactionRestored;
			OpenIABEventManager.restoreFailedEvent -= OnRestoreFailed;
			OpenIABEventManager.restoreSucceededEvent -= OnRestoreSucceeded;
		}
		
		private void InitStoreSKUs()
		{
			for(int i=0; i<SB_IAPManager.instance.IAPItemCount(); i++)
			{	
				SB_IAPItem iapItem = SB_IAPManager.instance.GetIAPItemAtIndex(i);
				
				string storeName = "";
				switch(SB_Globals.PLATFORM_ID)
				{
				case SB_BuildPlatforms.iOS:
					storeName = OpenIAB_iOS.STORE;
					break;
				case SB_BuildPlatforms.GooglePlay:
					storeName = OpenIAB_Android.STORE_GOOGLE;
					break;
				case SB_BuildPlatforms.Amazon:
					storeName = OpenIAB_Android.STORE_AMAZON;
					break;
				case SB_BuildPlatforms.Samsung:
					storeName = OpenIAB_Android.STORE_SAMSUNG;
					break;
				}
				
				if(storeName != "")
				{
					if(!iapItem.isFree)
					{
						OpenIAB.mapSku(iapItem.sku, storeName, iapItem.storeID);
					}
					
					Debug.LogWarning("IAP item ["+i+"] ::: iapItem.sku: "+iapItem.sku+", MAPPING STORE "+storeName+", SKU "+iapItem.storeID);
				}
				else
				{
					Debug.LogError("** No SKU's have been set! Unexpected platform id: "+SB_Globals.PLATFORM_ID);
				}
			}
		}
		
		/*
		 * Initial checks if Billing is available
		 */
		private void billingSupportedEvent()
		{
			_isInitialized = true;
			Debug.LogWarning("billingSupportedEvent");
			if(OnSB_OpenIABWrapperEvent != null)
				OnSB_OpenIABWrapperEvent(this, BILLING_SUPPORTED);
			QueryInventory();
			

		}
		
		private void billingNotSupportedEvent(string error)
		{
			Debug.LogError("billingNotSupportedEvent: "+error);
			if(OnSB_OpenIABWrapperEvent != null)
				OnSB_OpenIABWrapperEvent(this, BILLING_NOT_SUPPORTED, error);
		}
		
		
		
		/*
		 * Store inventory handling
		 */
		private void queryInventorySucceededEvent(Inventory inventory)
		{
			
			OpenIABEventManager.queryInventorySucceededEvent -= queryInventorySucceededEvent;

			Debug.LogWarning("--------------------------------------------------------------------------------");
			Debug.LogWarning("[SB_OpenIABWrapper] queryInventorySucceededEvent: "+inventory);
			if(inventory != null)
			{
				_inventory = inventory;
				Debug.LogWarning("currentInventory: "+inventory.ToString());
				
				SB_IAPManager.instance.SetupStoreAvailableIAPs(_inventory.GetAllAvailableSkus(), _inventory.GetAllOwnedSkus());
				SB_PackManager.instance.CheckDownloadedPacks();
				// Add all owned sku's (pack id's) to the userprofile.xml key value object.
				// Once this is done, the Shop can do a check to validate the files on the filesystem
				SB_ProfileVO userProfile = SB_UserProfileManager.instance.CurrentProfile;
				foreach(string packId in _inventory.GetAllOwnedSkus())
				{
					Debug.LogWarning("*** USER HAS ALREADY PURCHASED PACK "+packId);
					userProfile.installedIAPPacks.SetKeyValue(packId, "true");
				}
				SB_UserProfileManager.instance.SaveProfileData(userProfile);
				
			}
			else
			{
				Debug.LogError("inventory was null");
			}
			
			if(OnSB_OpenIABWrapperEvent != null)
				OnSB_OpenIABWrapperEvent(this, QUERY_INVENTORY_SUCCESS, inventory);
		}

		private void queryInventoryFailedEvent(string error)
		{
			Debug.LogError("queryInventoryFailedEvent: "+error);
			if(OnSB_OpenIABWrapperEvent != null)
				OnSB_OpenIABWrapperEvent(this, QUERY_INVENTORY_FAILED);
		}
		
		private void purchaseSucceededEvent(Purchase purchase)
		{
			Debug.LogWarning("purchaseSucceededEvent: "+purchase);
			
			// Add the purchased sku (pack id) to the userprofile.xml key value object.
			SB_UserProfileManager.instance.CurrentProfile.installedIAPPacks.SetKeyValue(purchase.Sku, "true");
			SB_UserProfileManager.instance.SaveProfileData(SB_UserProfileManager.instance.CurrentProfile);
			
			if(OnSB_OpenIABWrapperEvent != null)
				OnSB_OpenIABWrapperEvent(this, PURCHASE_SUCCESS, purchase);
		}
		
		private void purchaseFailedEvent(int errorCode, string errorMessage)
		{
			Debug.LogError("purchaseFailedEvent code: "+errorCode+", msg: "+errorMessage);
			if(OnSB_OpenIABWrapperEvent != null)
				OnSB_OpenIABWrapperEvent(this, PURCHASE_FAILED);
		}
		
		private void consumePurchaseSucceededEvent(Purchase purchase)
		{
			Debug.LogWarning("consumePurchaseSucceededEvent: "+purchase);
			if(OnSB_OpenIABWrapperEvent != null)
				OnSB_OpenIABWrapperEvent(this, CONSUMABLE_PURCHASE_SUCCESS, purchase);
		}
		
		private void consumePurchaseFailedEvent(string error)
		{
			Debug.LogError("consumePurchaseFailedEvent: "+error);
			if(OnSB_OpenIABWrapperEvent != null)
				OnSB_OpenIABWrapperEvent(this, CONSUMABLE_PURCHASE_FAILED);	
		}
		
		private void OnTransactionRestored(string sku)
		{
			Debug.LogWarning("restoringTransaction: "+sku);
			if(SB_IAPManager.instance.RestoreIAPPurchase(sku))
			{
				_restoredTransactions = true;
				Debug.LogError("restoredTransaction: "+sku);
			}
			
			if(OnSB_OpenIABWrapperEvent != null)
				OnSB_OpenIABWrapperEvent(this, TRANSACTION_RESTORED, sku);
		}
		
		private void OnRestoreFailed(string error)
		{
			Debug.LogError("restoreFailedEvent: "+error);
			if(OnSB_OpenIABWrapperEvent != null)
				OnSB_OpenIABWrapperEvent(this, RESTORE_FAILED, error);
		}
		
		private void OnRestoreSucceeded()
		{
			Debug.LogWarning("RestoreSucceededEvent");
			if(OnSB_OpenIABWrapperEvent != null)
				OnSB_OpenIABWrapperEvent(this, RESTORE_SUCCESS);			
		}
		
		public void Dispose()
		{
			// This does nothing on iOS, only Android..
			//OpenIAB.unbindService();
			
			DisableStoreEvents();
			OnSB_OpenIABWrapperEvent = null;
			//_inventory = null;
		}
		
		void OnDestroy()
		{
			Dispose();
		}
	}
}