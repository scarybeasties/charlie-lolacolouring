using UnityEngine;
using System.Collections;
using System.IO;
using ScaryBeasties;
using ScaryBeasties.SB2D;

namespace ScaryBeasties.InAppPurchasing.Shop
{
	/**
	 * Shop utility methods.
	 */
	public class SB_ShopUtils : MonoBehaviour 
	{
		public static bool PackXmlExists(string packID)
		{
			return File.Exists(GetPackXMLFilePath(packID));
		}
		
		public static string GetLocalShopDirectoryPath () 
		{
			string path = Application.persistentDataPath + "/" + SB_Globals.SHOP_IMAGES_LOCAL_DIR;
			//Debug.Log ("GetLocalShopDirectoryPath() " + path);
			return path;
		}
		
		public static string GetLocalAssetDirectoryPath () 
		{
			string path = Application.persistentDataPath + "/" + SB_Globals.SHOP_ASSET_LOCAL_DIR;
			//Debug.Log ("GetLocalAssetDirectoryPath() " + path);
			return path;
		}
		
		public static string GetLocalAssetFilePath (string packID) 
		{
			string path = GetLocalAssetDirectoryPath () + "/" + packID + "_" + tk2dSystem.CurrentPlatform;
			//Debug.Log ("GetLocalAssetFilePath() " + path);
			return path;
		}
		
		public static string GetLocalPackXMLsDirectoryPath () 
		{
			string path = GetLocalAssetDirectoryPath() + "/xml/" + SB_Globals.LOCALE_STRING;
			return path;
		}
		
		public static string GetPackXMLFilePath (string packID) 
		{
			//return (GetLocalAssetFilePath(packID) + "/pack.xml");
			//string path = (GetLocalAssetDirectoryPath() + "/" + packID + ".xml");
			
			string path = (GetLocalPackXMLsDirectoryPath() + "/" + packID + ".xml");
			
			//Debug.Log ("GetPackXMLFilePath() " + path);
			return path;
		}
		
		// Returns the url to the shopcopy.xml file
		public static string GetRemoteShopCopyUrl () 
		{
			return SB_Globals.SHOP_ASSETS_PATH + SB_Globals.LOCALE_STRING + "/" + SB_Globals.SHOP_COPY_XML_FILE;
		}
		
		// Returns the url to the shopdata.xml file
		public static string GetRemoteShopDataUrl () 
		{
			string shopDataFile = "";
			
			switch(SB_Globals.PLATFORM_ID)
			{
				case SB_BuildPlatforms.iOS: 		shopDataFile = SB_Globals.SHOP_DATA_XML_FILE_IOS;		break;
				case SB_BuildPlatforms.GooglePlay: 	shopDataFile = SB_Globals.SHOP_DATA_XML_FILE_GOOGLE;	break;
				case SB_BuildPlatforms.Amazon: 		shopDataFile = SB_Globals.SHOP_DATA_XML_FILE_AMAZON;	break;
				case SB_BuildPlatforms.Samsung: 	shopDataFile = SB_Globals.SHOP_DATA_XML_FILE_SAMSUNG;	break;
			}
		
			return SB_Globals.SHOP_ASSETS_PATH + SB_Globals.LOCALE_STRING + "/" + shopDataFile;
		}
		
		// Returns the full url to the PHP page, which begins downloading of the zip.
		// The PHP page requires 3 vars to be sent to it as POST vars:
		//
		//		id - Name of the pack folder and zip file.  
		//		platform - 1x or 2x
		//		locale - 'en', 'fr', 'de' etc...  The locale folder on the server
		//
		// NOTE: These vars are POST'ed by the SB_PackDownloader class
		public static string GetRemotePackDownloadUrl(string packID) 
		{
			return SB_Globals.SHOP_ASSETS_PATH + "download.php";
		}
		
		public static void SaveSpriteToDisk(SB2DSprite spr, string localPath="")
		{
			//Debug.LogError("SaveSpriteToDisk AssetName: " + spr.AssetName + ", AssetFileName: " + spr.AssetFileName + ", localPath: " + localPath);
			
			if(!Directory.Exists(SB_ShopUtils.GetLocalShopDirectoryPath() + localPath))
			{
				Directory.CreateDirectory(SB_ShopUtils.GetLocalShopDirectoryPath() + localPath);
			}
			
			string filename = spr.GetFileName();
			//Debug.LogError("filename: " + filename);
			
			byte[] bytes;
			if(filename.IndexOf(".jpg") > -1)
			{
				bytes = spr.Texture.EncodeToJPG();
			}
			else
			{
				bytes = spr.Texture.EncodeToPNG();
			}
			
			string fullFileName = SB_ShopUtils.GetLocalShopDirectoryPath() + localPath + "/" + filename;
			//Debug.LogError("Local filename: " + filename);
			if(!File.Exists(fullFileName))
			{
				//Debug.LogError("File doesn't exist Local filename: " + filename);
				File.WriteAllBytes(fullFileName, bytes);
			}
		}
		
		public static void DeleteLocalShopFiles()
		{
			//Debug.Log("-------> About to delete shop images: " + SB_ShopUtils.GetLocalShopDirectoryPath());
			
			if(Directory.Exists(SB_ShopUtils.GetLocalShopDirectoryPath()))
			{
				/*
				Debug.Log("-------> ...deleting folder");
				Directory.Delete(SB_ShopUtils.GetLocalShopDirectoryPath(), true);
				Debug.Log("-------> ...done");
				*/
				
				// TODO: only delete the shop images!
			}
		}
	}
}