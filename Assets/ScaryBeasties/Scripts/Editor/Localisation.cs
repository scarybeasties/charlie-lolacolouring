﻿
#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

public class ScriptBatch : MonoBehaviour 
{
	/** Source folder for localised assets */
	public static string LOCALISED_SOURCE = "Assets/Localised";

	/** Target destination for localised assets.  MUST be within the Resources folder! **/
	public static string LOCALISED_TARGET = "Assets/Resources/Localised";
	public static string LOCALISED_WEBVIEWS_TARGET = "Assets/StreamingAssets/WebViews";
	
	[MenuItem("Set Localisation/DE (German)")]
	public static void BuildDE ()	{CopyFiles("DE");}

	[MenuItem("Set Localisation/DK (Danish)")]
	public static void BuildDK ()	{CopyFiles("DK");}

	[MenuItem("Set Localisation/EN (English)")]
	public static void BuildEN ()	{CopyFiles("EN");}

	[MenuItem("Set Localisation/FR (French)")]
	public static void BuildFR ()	{CopyFiles("FR");}

	[MenuItem("Set Localisation/IT (Italian)")]
	public static void BuildIT ()	{CopyFiles("IT");}

	[MenuItem("Set Localisation/NO (Norwegian)")]
	public static void BuildNO ()	{CopyFiles("NO");}
	
	[MenuItem("Set Localisation/NL (Dutch)")]
	public static void BuildNL ()	{CopyFiles("NL");}

	[MenuItem("Set Localisation/PL (Polish)")]
	public static void BuildPL ()	{CopyFiles("PL");}
	
	[MenuItem("Set Localisation/SE (Swedish)")]
	public static void BuildSE ()	{CopyFiles("SE");}

	
	public static void CopyFiles(string locale="")
	{
		if (locale == "") 
		{
			locale = "EN";
		}

		Debug.Log("Deleting files from: " + LOCALISED_TARGET);
		// Delete any existing localised files
		FileUtil.DeleteFileOrDirectory (LOCALISED_TARGET);

		Debug.Log("Copying files from:" + LOCALISED_SOURCE + "/" + locale + " ---> to: " + LOCALISED_TARGET);
		// Copy the new localised files
		FileUtil.CopyFileOrDirectory (LOCALISED_SOURCE + "/" + locale, LOCALISED_TARGET);

		// Copy the WebViews folder to StreamingAssets
		FileUtil.DeleteFileOrDirectory (LOCALISED_WEBVIEWS_TARGET);
		FileUtil.CopyFileOrDirectory (LOCALISED_SOURCE + "/" + locale + "/WebViews", LOCALISED_WEBVIEWS_TARGET);

		// Refresh the Unity asset database
		AssetDatabase.Refresh();
	}
}

#endif