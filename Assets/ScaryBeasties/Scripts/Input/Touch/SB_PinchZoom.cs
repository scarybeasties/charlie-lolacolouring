﻿/**
 * Pinch zoom behaviour.
 * 
 * Drag this script onto a game object, to allow it to be resized
 * by the user, via a pinch zoom.
 */


using UnityEngine;
using System.Collections;
using ScaryBeasties.Utils;
using System;
using ScaryBeasties.SB2D;

public class SB_PinchZoom : MonoBehaviour
{
	public int MINIMUM_ROTATION_DELTA = 8;
	public Vector2 MinimumScale = new Vector2(0.1f, 0.1f);
	public Vector2 MaximumScale = new Vector2(5f, 5f);
	
	// The Transform which will be pinch-zoomed
	public Transform Target = null;
	
	private bool _multiTouchStarted = false;
	private Vector2 _touch0StartPos;
	private Vector2 _touch1StartPos;
	private float _touchStartDistance;
	private Vector2 _targetStartScale;
	private Quaternion _targetStartRotation;
	
	private float _touchStartAngle;
	private float _touchStartAngleOffset;
	private bool _isRotating = false;
	
	public bool CanScaleX = true;
	public bool CanScaleY = true;
	public bool CanRotate = true;
	public bool MaintainAspectRatio = false;
	
	// Getters
	public bool MultiTouchStarted 	{get {return _multiTouchStarted;}}
	public bool IsRotating 			{get {return _isRotating;}}
	
	// Delegate
	public Action<SB_PinchZoom> OnMultiTouchStarted;
	public Action<SB_PinchZoom> OnMultiTouchEnded;


	private int _numTouches = 0;
	
	public void Reset()
	{
		_multiTouchStarted = false;
		_isRotating = false;
	}
	
	void Update()
	{
		// If there are two touches on the device...
		if (Input.touchCount == 2)
		{
			if(Target == null) return;
			
			// Store both touches.
			Touch touch0 = Input.GetTouch(0);
			Touch touch1 = Input.GetTouch(1);

			//if((!InsideBounds(touch0))&&(!InsideBounds(touch1))&&(!_multiTouchStarted)) return;

			if(!_multiTouchStarted)
			{	
				_multiTouchStarted = true;
				
				_touch0StartPos = touch0.position;
				_touch1StartPos = touch1.position;
				_touchStartDistance = SB_Utils.GetDistance(_touch0StartPos, _touch1StartPos);
				
				_targetStartScale = Target.localScale;
				_targetStartRotation = Target.rotation;
				_touchStartAngle = SB_Utils.GetAngle(_touch0StartPos, _touch1StartPos) - _targetStartRotation.eulerAngles.z;
				
				_touchStartAngleOffset = SB_Utils.GetAngle(_touch0StartPos, _touch1StartPos) - _touchStartAngle;
				
				// Perform callback
				if(OnMultiTouchStarted != null) OnMultiTouchStarted(this);
			}
			
			Vector2 touch0Pos = touch0.position;
			Vector2 touch1Pos = touch1.position;
			float touchCurrentDistance = SB_Utils.GetDistance(touch0Pos, touch1Pos);
			
			float touchDistanceDelta = touchCurrentDistance - _touchStartDistance;
			float touchDistanceDeltaPercentage = touchCurrentDistance/_touchStartDistance;
			Debug.LogError("_touchStartDistance: " + _touchStartDistance + ", touchCurrentDistance: " + touchCurrentDistance + ", touchDistanceDelta: " + touchDistanceDelta + ", touchDistanceDeltaPercentage: " + touchDistanceDeltaPercentage);
			
			Vector2 newScale = _targetStartScale;
			newScale.x = touchDistanceDeltaPercentage * _targetStartScale.x;
			newScale.y = touchDistanceDeltaPercentage * _targetStartScale.y;
			
			if(!CanScaleX)
			{
				newScale.x = Target.localScale.x;
			}
			
			if(!CanScaleY)
			{
				newScale.y = Target.localScale.y;
			}
			
			if(MaintainAspectRatio)
			{
				float ratio = Math.Abs(newScale.x);
				newScale.x = ratio;
				newScale.y = ratio;
			}
			
			// Preserve the target item's current scale, if it's negative
			float scaleXMultiplier = 1f;
			float scaleYMultiplier = 1f;
			if(Target.localScale.x < 0 && newScale.x > 0) newScale.x *= -1f;
			if(Target.localScale.y < 0 && newScale.y > 0) newScale.y *= -1f;
			
			Vector2 oldScale = Target.localScale;
			Vector2 absScale = new Vector2(Mathf.Abs(newScale.x),Mathf.Abs(newScale.y));
			if((absScale.x > MaximumScale.x || absScale.y > MaximumScale.y) || (absScale.x < MinimumScale.x || absScale.y < MinimumScale.y))
			{
				newScale = oldScale;
			}
			
			Target.localScale = newScale;
			
			float touchCurrentAngle = SB_Utils.GetAngle(touch0Pos, touch1Pos);
			float touchAngleDelta = touchCurrentAngle - _touchStartAngle;
			Quaternion newRotation = Quaternion.Euler(0, 0, _targetStartRotation.z + touchAngleDelta);
			//Debug.LogWarning("_touchStartAngleOffset: " + _touchStartAngleOffset + ", _touchStartAngle: " + _touchStartAngle + ", touchCurrentAngle: " + touchCurrentAngle + ", touchAngleDelta: " + touchAngleDelta + ", _targetStartRotation.z: " + _targetStartRotation.z + ", newRotation: " + newRotation + ", MINIMUM_ROTATION_DELTA: " + MINIMUM_ROTATION_DELTA);
			
			if(CanRotate && (UserExceededMiniumRotation(_touchStartAngleOffset - touchAngleDelta) || _isRotating))
			{	
				_isRotating = true;
				Target.rotation = newRotation;
			}
			
			//Debug.LogWarning("Target newScale: " + newScale + ", Target.localScale: " + Target.localScale + " Target.rotation.z: " + Target.rotation.z);
		}
		else
		{
			//if(OnMultiTouchEnded != null) OnMultiTouchEnded(this);
			//Reset();
		}

		if ((_numTouches > 1) && (Input.touchCount <= 1)) {
			Debug.LogError("ROTATION MULTITOUCH STOPPED");
			if(OnMultiTouchEnded != null) OnMultiTouchEnded(this);
			Reset();
		}
		_numTouches = Input.touchCount;
	}


	bool InsideBounds(Touch pos)
	{
		if(Target == null) return false;
		
		Vector2 newpos = Camera.main.ScreenToWorldPoint (pos.position);
		SB2DCanvasSprite spr = Target.transform.gameObject.GetComponent<SB2DCanvasSprite>();
		if (spr != null) {
			return spr.PixelHitTestPoint(newpos, 1.0f);
		}
		return false;
	}


	
	bool UserExceededMiniumRotation(float angleDelta)
	{
		if(Mathf.Abs(angleDelta) > MINIMUM_ROTATION_DELTA)
		{
			return true;
		}
		return false;
	}
	
	void OnDestroy()
	{
		Target = null;
		OnMultiTouchStarted = null;
		OnMultiTouchEnded = null;
	}
}
