/**
 * Base Scene
 * -------------
 * 
 * Chris Thodesen
 * Scary Beasties Limited
 * September 2014
 * 
 * Description
 * ------------
 * Camera image class, saves the image to disk. Images are saved as 
 * custom<avatarId>.png
 * so each profile has a unique filename to store the images.
 * 
 * Extends SB_BasePopup
 * 
 */

using UnityEngine;
using System.Collections;
using System.IO;
using ScaryBeasties.Utils;

namespace ScaryBeasties.Cam
{
    public class SB_CameraImage : MonoBehaviour
    {
        public string filename = "";
        public string fileExtension = ".png";

        private Texture2D _texture;

        void Start()
        {
            //Debug.Log("[SB_CameraImage] Start()");

            if (filename != "" && _texture != null)
            {
                //Debug.Log("[SB_CameraImage] Start() about to call LoadImage");
                StartCoroutine(DoLoadImage());
            }
        }

        public void SetupImageLoader(int imageWidth, int imageHeight)
        {
            _texture = new Texture2D(imageWidth, imageHeight);
        }

        public void SetupImageLoader(int imageSize)
        {
            _texture = new Texture2D(imageSize, imageSize);
        }

        public void LoadImage(string imageFilename)
        {
            filename = imageFilename;
            StartCoroutine(DoLoadImage());
        }

        // GT - why is this code in OnGUI ??? running continuously.. (Chris code!)
        /*
		void OnGUI()
		{
			if(filename == "" || renderer.material.mainTexture != null)
			{
				return;
			}

			//Debug.Log("[SB_CameraImage] renderer.material.mainTexture is null, about to call LoadImage");
			StartCoroutine(DoLoadImage());
		}
		*/


        public bool FileExists(string filename)
        {
            string fName = Application.persistentDataPath + "/" + filename + fileExtension;
            return File.Exists(fName);
        }

        IEnumerator DoLoadImage()
        {
            string fileToLoad = Application.persistentDataPath + "/" + filename + fileExtension;
#if UNITY_ANDROID
            Debug.Log("[SB_CameraImage] Attempting to load from: " + fileToLoad);
            if (File.Exists(fileToLoad))
            {
                fileToLoad = "file:///" + fileToLoad;
                Debug.Log("fileToLoad: " + fileToLoad);

                WWW www = new WWW(fileToLoad);
                yield return www;
                GetComponent<Renderer>().material.mainTexture = www.texture;
            }
            else
            {
                Debug.LogError("[SB_CameraImage] File does not exist " + fileToLoad);
            }
#else
            if (File.Exists(fileToLoad))
            {
                Texture2D tex = new Texture2D(2, 2);
                tex.LoadImage(File.ReadAllBytes(fileToLoad), true);
                tex.wrapMode = TextureWrapMode.Clamp;
                yield return null;
                GetComponent<Renderer>().material.mainTexture = tex;
            }
            else
            {
                yield return null;
            }
#endif
        }

        public Texture2D GetTexture()
        {
            if (_texture == null)
            {
                Debug.LogError("[SB_CameraImage] Texture does not exist, has it been loaded using LoadImage() ?");
            }
            return _texture;
        }

        void OnDestroy()
        {
            DestroyImmediate(_texture);
            _texture = null;
        }
    }
}