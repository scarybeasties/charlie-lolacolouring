using UnityEngine;
using System.Collections;
using ScaryBeasties.Utils;
using ScaryBeasties.User;


namespace ScaryBeasties.Cam
{
	public class SB_Camera : MonoBehaviour
	{
		#if USES_CAMERA
		public static bool CAMERA_ALLOWED = true;

		public delegate void CameraImageSaved(SB_Camera cam, Texture2D texture, string path);
		public event CameraImageSaved OnCameraImageSaved;

		private MeshRenderer[] _camMeshRenderers;

		private SB_CameraImage _cameraImage;
		private Texture2D _imageTexture;

		private WebCamTexture _cameraTexture;
		private Quaternion _cameraRotation;
		private string _frontCameraName;

		private int _cameraWidth;
		private int _cameraHeight;

		private	int _textureWidth;
		private int _textureHeight;

		private int _rotation;
		private bool _flippedHorizontally = false;
		private bool _flippedVertically = false;

		private bool _paused = false;

		private int _previousWidth;
		private float _elapsed = 0.0f;
		private bool _traced = false;
		private float _firstElapsed = 0.0f;

		public bool available = true;

		ScreenOrientation _currentOrientation;
		float _scaleSwitch=1;

		public static bool CameraExists() 
		{
			if(!CAMERA_ALLOWED) return false;

			bool result = false;
			WebCamDevice[] devices = WebCamTexture.devices;
			
			Debug.LogWarning("devices.Length: " + devices.Length);
			
			int i;
			string fcname="";
			for ( i = 0; i < devices.Length; i++ ) 
			{
				if( devices[i].isFrontFacing ) 
				{
					result = true;
					fcname = devices[i].name;
					Debug.LogWarning("devices["+i+"].name: " + devices[i].name);
				}

				#if UNITY_EDITOR
				// We can assume that if this app is being tested in the editor, there won't be any rear facing cameras!
				result = true;
				#endif
			}
			/*
			if(result){
				WebCamTexture cameraTexture = new WebCamTexture(fcname);
				if(!cameraTexture.isReadable){
					
					result = false;
				}
				cameraTexture=null;
			}*/

			return result;
		}

		void Start() 
		{

			_currentOrientation = Screen.orientation;

			
			if(_currentOrientation == ScreenOrientation.LandscapeLeft){
				_scaleSwitch=1;
			}
			else{
				_scaleSwitch=-1;
			}

			if(!CAMERA_ALLOWED) 
			{
				available = false;
				return;
			}

			_camMeshRenderers = GetComponents<MeshRenderer>();

			WebCamDevice[] devices = WebCamTexture.devices;

			int i;

			for ( i = 0; i < devices.Length; i++ ) 
			{
				if( devices[i].isFrontFacing ) 
				{
					_frontCameraName = devices[i].name;
				}
			}

			_cameraTexture = new WebCamTexture(_frontCameraName);

			if(_cameraTexture==null) available=false;
			//else if(!_cameraTexture.isReadable) available =false;
			else if(_cameraTexture.width<=0) available=false;
			else if (_cameraTexture.videoRotationAngle==null) available =false;
			//available=false;
			if(available)
			{
				foreach( MeshRenderer r in _camMeshRenderers )
				{
					r.material.mainTexture = _cameraTexture;
				}
				renderer.material.mainTexture = _cameraTexture;

				_cameraWidth = _cameraTexture.width;
				_cameraHeight = _cameraTexture.height;

				print ("_cameraWidth: " + _cameraWidth + ", _cameraHeight: " + _cameraHeight);

				_cameraTexture.Play();

				if( _cameraTexture.videoRotationAngle != 0 ) {
					int z = _cameraTexture.videoRotationAngle;
					while( z > 0 ) {
						RotateAntiClockwise();
						z -= 90;
					}
				}

				_previousWidth = _cameraTexture.width;
				

				StartCoroutine("CameraSetupRoutine");
			}
		}

		public void SetupCameraSize( int width, int height ) 
		{
			_textureWidth = width;
			_textureHeight = height;
		}

		public IEnumerator CameraSetupRoutine() {


				
			while( _previousWidth == _cameraTexture.width ) {
				// Debug.Log ("[WebCamText] previous width = " + _previousWidth + " actual: " + webcamTexture.width );
				yield return null;
			}
			
			_cameraWidth = _cameraTexture.width;
			_cameraHeight = _cameraTexture.height;
			
			Vector2 camScale = renderer.material.mainTextureScale;
			camScale.y = 1f;
			camScale.x = (float)_cameraHeight / (float)_cameraWidth;
			renderer.material.mainTextureScale = camScale;

			// Debug.Log ("[WebCamTest] webCamScale.x = " + webCamScale.x + " wcH " + _webcamHeight + " wcH: " + _webcamWidth + " wctW: " + webcamTexture.width + " wctH: " + webcamTexture.height + " prev: " + webcamTexture.width );
			
			Vector2 offset = renderer.material.mainTextureOffset;
			offset.x = ( (float)_cameraHeight / (float)_cameraWidth ) / 2f;
			renderer.material.mainTextureOffset = offset;

			
			Debug.Log ("camScale: " + camScale + ", _cameraWidth: " + _cameraWidth + ", _cameraHeight: " + _cameraHeight + ", offset: " + offset);
			
		}

		public WebCamTexture GetCameraTexture()
		{
			return _cameraTexture;
		}

		public float GetCameraAngle() {
			
			if (_cameraTexture != null )
				return _cameraTexture.videoRotationAngle;
			return 0f;
		}

		private Texture2D ScaleTexture(Texture2D source,int targetWidth,int targetHeight) 
		{
			Texture2D result = new Texture2D( targetWidth, targetHeight, source.format, true );
			Color[] rpixels = result.GetPixels(0);

			float incX = ( (float)1 / source.width ) * ( (float)source.width / targetWidth);
			float incY = ( (float)1 / source.height) * ( (float)source.height / targetHeight);

			for( int px=0; px < rpixels.Length; px++ ) 
			{
				rpixels[px] = source.GetPixelBilinear(incX * ((float)px % targetWidth),
				                                      incY * ((float)Mathf.Floor( px / targetWidth )));
			}

			result.SetPixels( rpixels, 0 );
			result.Apply();

			return result;
		}
		
		private Texture2D RotateTexture(Texture2D source,float angle) 
		{
			
			Color[] c = source.GetPixels();
			int size = c.Length;
			Color[] tempColors = new Color[size];
			
			if ( angle == 90 ) {
				for (var x = 0; x < size; x++) {
					for (var y = 0; y < size; y++) {
						tempColors[x + y*size] = c[y + x*size];
					}
				}
			}
			
			source.SetPixels(tempColors);
			return source;
			
		}

		public void PresaveCameraImage() 
		{
			_imageTexture = new Texture2D(_cameraTexture.width,_cameraTexture.height); 
			_imageTexture.SetPixels( _cameraTexture.GetPixels() );
			_imageTexture.Apply();
			_paused = true;
			
		}

		public void SaveCameraImage() 
		{
			if( !_paused ) 
			{
				_imageTexture = new Texture2D( _cameraTexture.width, _cameraTexture.height ); 
				_imageTexture.SetPixels( _cameraTexture.GetPixels() ); // cannot be saved whilst webcam is paused/stopped
				_imageTexture.Apply();
			}

			_imageTexture = ScaleTexture( _imageTexture, _textureWidth, _textureHeight);
			print ("RESCALED: _imageTexture.width: " + _imageTexture.width + ", _imageTexture.height: " + _imageTexture.height);

			gameObject.AddComponent<SB_CameraImage>();
			_cameraImage = GetComponent<SB_CameraImage>();

			// This only happens when testing the scene standalone, in the IDE!
			//if(SB_Globals.SELECTED_PROFILE_VO == null) return;
			if(SB_UserProfileManager.instance.CurrentProfile == null) return;

			byte[] bytes = _imageTexture.EncodeToPNG();
			ScreenshotManager.ImageFinishedSaving += ImageAssetSaved;
			//StartCoroutine(ScreenshotManager.SaveExisting(bytes, "custom"+SB_Globals.SELECTED_PROFILE_VO.profileId, true));
			string filename = "custom"+SB_UserProfileManager.instance.CurrentProfile.profileId;
			StartCoroutine(ScreenshotManager.SaveExisting(bytes, filename, true));
		}


		void ImageAssetSaved(string path)
		{
			Debug.Log ("ImageAssetSaved() Finished saving to " + path);
			ScreenshotManager.ImageFinishedSaving -= ImageAssetSaved;

			// Call delegate
			if(OnCameraImageSaved != null) OnCameraImageSaved(this, _imageTexture, path);
		}

		public bool CamTextureActive() {
			return _cameraTexture.isPlaying;
		}
		
		public void PauseCam() {
			PresaveCameraImage();
			_cameraTexture.Pause();
		}
		
		public void StopCam() {
			PresaveCameraImage();
			_cameraTexture.Stop();
		}
		
		public void PlayCam() {
			_cameraTexture.Play();
			_paused = false;
		}
		
		public void RotateClockwise() {
			_rotation += 90;
			if ( _rotation > 270 ) 
				_rotation = 0;
			
			Vector3 angle = transform.eulerAngles;
			angle.z = _rotation;
			transform.eulerAngles = angle;
		}
		
		public void RotateAntiClockwise() {
			_rotation -= 90;
			if ( _rotation < 0 ) 
				_rotation = 270;
			
			Vector3 angle = transform.eulerAngles;
			angle.z = _rotation;
			transform.eulerAngles = angle;
		}
		
		public void FlipVertically() {
			Vector3 camScale = transform.localScale;
			camScale.y = -camScale.y;
			transform.localScale = camScale;
		}
		
		public void FlipHorizontally() {
			Vector3 camScale = transform.localScale;
			camScale.x = -camScale.x;
			transform.localScale = camScale;
		}

		void Update()
		{
			//if(_currentOrientation != Screen.orientation)
			//{
				_currentOrientation = Screen.orientation;
				//Debug.LogWarning("_currentOrientation: " + _currentOrientation);

				tk2dSprite camPic = transform.GetComponent<tk2dSprite>();
				Vector3 camScale = camPic.scale;

				if(_currentOrientation == ScreenOrientation.LandscapeLeft)
				{
					camScale.x = -1;

					#if UNITY_IOS
				camScale.x = -1*_scaleSwitch;
				camScale.y = -1*_scaleSwitch;
					#else
					//camScale.y = 1;
				camScale.y = 1*_scaleSwitch;
					#endif
				}
				else if(_currentOrientation == ScreenOrientation.LandscapeRight)
				{
					
					camScale.x = 1;

					#if UNITY_IOS
				camScale.x = 1*_scaleSwitch;
				camScale.y = 1*_scaleSwitch;
					#else
					//camScale.y = -1;
				camScale.y = -1*_scaleSwitch;
					#endif

				}

				camPic.scale = camScale;
				//Debug.LogWarning("CAM SCALE "+camScale);
			//}
		}


		void OnDestroy() 
		{
			print ("destroy camera texture");

			OnCameraImageSaved = null;

			_cameraTexture.Stop();
			Destroy(_cameraTexture);
			_cameraTexture = null;

			_camMeshRenderers = null;

			Destroy(_cameraImage);
			_cameraImage = null;
			
			Destroy(_imageTexture);
			_imageTexture = null;
		}
		#else

		public static bool CameraExists() 
		{
			return false;
		}

		#endif
	}
}