using UnityEngine;
using System.Collections;
using ScaryBeasties.Utils;

namespace ScaryBeasties.Input
{
	public class SB_KeyboardInput : MonoBehaviour 
	{
		public delegate void KeyboardDoneDelegate(TouchScreenKeyboard keyboard);
		public event KeyboardDoneDelegate OnKeyboardDone;

		public string defaultText = "";
		public int maximumCharacters = 30;
		public bool multiline = false;

		private SB_TextField _inputField;
		protected TouchScreenKeyboard _keyboard;

		private string lastText ="";

		private string regex = "[^\\u0020-\\u007E\\u00A0-\\u00BE\\u2E80-\\uA4CF\\uF900-\\uFAFF\\uFE30-\\uFE4F\\uFF00-\\uFFEF\\u0080-\\u009F\\u2000-\\u201f\r\n]";

		// Use this for initialization
		void Start()
		{
			TouchScreenKeyboard.hideInput = true;
			_inputField = GetComponent<SB_TextField>();

			// We can't actually set the _inputField text at this point..
			// Need to wait for a cycle or two before trying..
			StartCoroutine(init());


		}

		/**
		 * Wait for a short while, before keyboard is shown.
		 */
		private IEnumerator init()
		{
			yield return new WaitForSeconds(0.1f);
			_inputField.text = defaultText;

			// Horrible hack...
			// Restart the coroutine, to keep setting the text field
			if(_inputField.text != defaultText)
			{
				StartCoroutine(init());
			}
			else
			{
				//Debug.Log ("Showing keyboard in 1 sec");
				yield return new WaitForSeconds(1f);
				//Debug.Log ("..about to show keyboard!");
				ShowKeyboard(defaultText);
			}
		}

		void OnGUI()
		{
			if(_keyboard != null && TouchScreenKeyboard.visible)
			{
				TouchScreenKeyboard.hideInput = true;

				// Call delegate
				if(_keyboard.done && OnKeyboardDone != null)
				{
					OnKeyboardDone(_keyboard);
				}
			}
		}

		// Update the text field, with the user entered text.
		void Update() 
		{
			if(_keyboard != null && TouchScreenKeyboard.visible)
			{
				/*string newText = _keyboard.text;
				if(newText.IndexOf(defaultText) > -1 && newText.Length > defaultText.Length-1)
				{
					_keyboard.text = _keyboard.text.Substring(defaultText.Length);
					newText = _keyboard.text;
				}

				// Enforce character limit!
				if(newText.Length > maximumCharacters)
				{
					newText = newText.Substring(0, maximumCharacters);
					_keyboard.text = newText;
				}

				// This is the only way I have found to remove new line & carriage return....
				if(!multiline)
				{
					newText = newText.Replace("\n", "").Replace("\r", "");
				}

				if(newText.Length == 0) newText = defaultText;
				_inputField.text = newText;*/
				if(lastText!=_keyboard.text)
				{
					if(_keyboard.text.IndexOf(defaultText) > -1 && _keyboard.text.Length > defaultText.Length-1)
					{
						_keyboard.text = _keyboard.text.Substring(defaultText.Length);
					}

					if(_keyboard.text.Length>maximumCharacters){
						_keyboard.text= _keyboard.text.Substring(0, maximumCharacters);
					}
					if(!multiline)
					{
						_keyboard.text= _keyboard.text.Replace("\n", "").Replace("\r", "").Replace(" ", "");
					}

					//_keyboard.text = System.Text.RegularExpressions.Regex.Replace(_keyboard.text,regex,"");

					_inputField.text = _keyboard.text;
					lastText=_inputField.text;
				}
			}
		}

		public string text
		{
			get
			{
				return _inputField.text;
			}
		}

		public void ShowKeyboard(string txt="", TouchScreenKeyboardType keyboardType=TouchScreenKeyboardType.ASCIICapable)
		{
			// If keyboard is already showing, then stop here..
			if(TouchScreenKeyboard.visible) return;

			_inputField.text = txt;

			// Show the keyboard
			_keyboard = TouchScreenKeyboard.Open(_inputField.text, keyboardType, true, false, false, false, txt);

			// Hide the default text input area
			TouchScreenKeyboard.hideInput = true;
		}

		public void HideKeyboard()
		{
			if(_keyboard != null)
			{
				_keyboard.active = false;
			}
		}

		public bool KeyboardIsVisible
		{
			get
			{
				return TouchScreenKeyboard.visible;
			}
		}

		public bool KeyboardIsDone
		{
			get
			{
				if(_keyboard == null) return false;

				return _keyboard.done;
			}
		}

		void OnDestroy()
		{
			if(_keyboard != null)
			{
				HideKeyboard();
				_keyboard = null;
			}

			if(_inputField != null)
			{
				Destroy(_inputField);
				_inputField = null;
			}

			OnKeyboardDone = null;
		}
	}
}
