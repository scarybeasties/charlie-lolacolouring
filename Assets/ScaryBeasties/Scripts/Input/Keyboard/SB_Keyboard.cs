﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Controls;
using ScaryBeasties.Base;
using ScaryBeasties.Sound;
using ScaryBeasties.Utils;
using System.Xml;
using ScaryBeasties.Data;
using ScaryBeasties.Cam;
using System;

namespace ScaryBeasties.Input
{
	public class SB_Keyboard : SB_BaseView 
	{
		public const int KEY_SPACEBAR = 0;
		public const int KEY_ENTER = 1;
		public const int KEY_DELETE = 2;
		public const int KEY_LETTER = 3;

		// Keyboard modes
		public const int LETTERS = 0;
		public const int NUMBERS = 1;
		public const int PUNCTUATION = 2;

		// Make sure alternate keys are above everything else!
		const int ALTERNATE_KEY_SORTING_ORDER = 200;
		
		// Store all the generated keys (..but not the uppercase/numeric/punctuation btns, spacebar and enter btn)
		protected ArrayList _letters = new ArrayList();

		// Stores the alternate values for each letter, which are presented when the user holds down a key
		protected KeyValueObject _keyAlternates;

		// An array of alternate letters, for the key which is currently pressed
		private string[] _currentLetterAlternates = null;

		private SB_Button _currentButtonPressed = null;
		private SB_Button _currentAlternateButton = null;

		private GameObject _initialKey, _alternateInitialKey;
		private float _startX, _startY;
		private float _screenWidth;

		private SB_KeyboardButtonSpacingConfig _keySpacing;
		private SB_KeyboardButtonSpacingConfig _alternateKeySpacing;

		private Transform _alternateLettersContainer;
		private Transform _lettersContainer;
		private Transform _numbersContainer;
		private Transform _punctuationContainer;
		private Transform _currentKeysContainer;

		protected SB_Button _deleteBtn;
		protected SB_Button _uppercaseBtn;
		protected SB_Button _punctuationBtn;
		protected SB_Button _numbersBtn;
		protected SB_Button _numbersBtn2;
		protected SB_Button _lettersBtn;
		protected SB_Button _enterBtn;
		protected SB_Button _spaceBtn;

		protected bool _mouseDown = false;

		protected float MAX_KEY_PRESSED_TIME = 0.4f;
		protected float _keyPressedTime = 0f;

		protected string inputString = "";

		private bool uppercase = false;

		public SB_KeyboardStyleConfig config;
		public SB_KeyboardButtonConfig buttons;
		public Action<int> OnKeyPressed;

		// Use this for initialization
		protected virtual void Start () 
		{
			_initialKey = transform.Find("input").gameObject;
			_startX = _initialKey.transform.position.x;
			_startY = _initialKey.transform.position.y;

			_alternateInitialKey = transform.Find("alternateInput").gameObject;

			_screenWidth = ZoomPoint((float)tk2dCamera.Instance.ScreenExtents.size.x);

			_alternateLettersContainer = transform.Find("AlternateLetters");
			_lettersContainer = transform.Find("Letters");
			_numbersContainer = transform.Find("Numbers");
			_punctuationContainer = transform.Find("Punctuation");

			_deleteBtn = transform.Find("DeleteBtn").GetComponent<SB_Button>();
			_uppercaseBtn = transform.Find("UppercaseBtn").GetComponent<SB_Button>();
			_punctuationBtn = transform.Find("PunctuationBtn").GetComponent<SB_Button>();
			_numbersBtn = transform.Find("NumbersBtn").GetComponent<SB_Button>();
			_numbersBtn2 = transform.Find("NumbersBtn2").GetComponent<SB_Button>();
			_lettersBtn = transform.Find("LettersBtn").GetComponent<SB_Button>();
			_enterBtn = transform.Find("EnterBtn").GetComponent<SB_Button>();
			_spaceBtn = transform.Find("SpaceBtn").GetComponent<SB_Button>();

			_buttons.Add(_deleteBtn);
			_buttons.Add(_uppercaseBtn);
			_buttons.Add(_punctuationBtn);
			_buttons.Add(_numbersBtn);
			_buttons.Add(_numbersBtn2);
			_buttons.Add(_lettersBtn);
			_buttons.Add(_enterBtn);
			_buttons.Add(_spaceBtn);

			_keyAlternates = new KeyValueObject();
			_alternateLettersContainer.gameObject.SetActive(false);

			SetupKeys("letters", _lettersContainer);
			SetupKeys("numbers", _numbersContainer);
			SetupKeys("punctuation", _punctuationContainer);

			_initialKey.SetActive(false);
			_alternateInitialKey.SetActive(false);
			Mode(LETTERS);

			// Store the key spacing values, for lookup later
			_keySpacing = config.keySpacing;
			_alternateKeySpacing = config.alternateKeySpacing;

			if(config.forceUppercase)
			{
				uppercase = true;
				SetUpper();
			}

			base.Start();
		}

		protected virtual void SetupKeys (string keyType, Transform container)
		{
			GameObject newKey;
			
			XmlNodeList rowsNodeList = SB_Globals.CONFIG_XML.GetNodeList("config/keyboard/" + keyType + "/row");
			//warn("lettersNodeList: " + lettersNodeList + ", " + rowsNodeList.Count);
			for(int i=0; i<rowsNodeList.Count; i++)
			{
				int row = i;
				int col = 0;
				
				XmlNodeList rowKeysList = rowsNodeList[i].ChildNodes;
				float offsetX = float.Parse(SB_Globals.CONFIG_XML.GetNodeAttribute(rowsNodeList[i], "offsetX"));

				//warn("rowsNodeList["+i+"].ParentNode.Name: " + rowsNodeList[i].Name);
				foreach(XmlElement node in rowKeysList)
				{
					//warn ("row ["+i+"] " + node.InnerText);

					string btnLabel = WWW.UnEscapeURL(node.InnerText);
					string alternates = WWW.UnEscapeURL(SB_Globals.CONFIG_XML.GetNodeAttribute(node, "alternates"));
					//warn ("alternates: " + alternates);

					if(alternates.Length > 0)
					{
						char[] splitchar = { ',' };
						string[] alternatesArr = alternates.Split(splitchar);
						//warn (node.InnerText + " ----- alternatesArr.Length: " + alternatesArr.Length);

						// Add this letter's alternate key values to the KV object, so we can do a lookup later
						if(!_keyAlternates.ContainsKey(btnLabel))
						{
							_keyAlternates.SetKeyValue(btnLabel, alternatesArr);
						}
					}

					float spacingX = config.keySpacing.x * col;
					float spacingY = config.keySpacing.y * row;

					newKey = (GameObject)Instantiate( _initialKey );
					newKey.transform.SetParent(container);
					newKey.transform.position = new Vector3(_startX + offsetX + spacingX, _startY-spacingY, 10.0f);
					
					SB_Button btn = newKey.GetComponent<SB_Button>();
					btn.label = btnLabel;
					
					CustomizeButton(btn);
					
					_letters.Add (btn);
					
					col++;
					_buttons.Add(btn);
				}
				
			}
		}

		// Create alternate key buttons
		protected virtual void SetupAlternateKeys (string[] alternates, Vector3 pos)
		{
			GameObject newKey;
			float offsetX = 0;
			float offsetY = 15;
			int col = 0;

			float startX = pos.x;
			float startY = pos.y + offsetY;
			int gapX = _alternateKeySpacing.x;
			
			//----------------------------------------------
			// Check if the alternates is going to hang off the screen.
			// Adjust the startX value accordingly, to ensure everything is on screen.
			float alternatesWidth = gapX * alternates.Length;
			float alternatesMaxX = startX + alternatesWidth;
			float screenMaxX = _screenWidth * 0.5f;
			if(alternatesMaxX > screenMaxX)
			{
				startX += (screenMaxX - alternatesMaxX);
			}
			//----------------------------------------------

			for(int i=0; i<alternates.Length; i++)
			{
				//warn ("alternates["+i+"] :  " + alternates[i]);

				string btnLabel = alternates[i];
				if(uppercase)
				{
					btnLabel = btnLabel.ToUpper();
				}

				newKey = (GameObject)Instantiate( _alternateInitialKey );

				tk2dSprite sprite = newKey.transform.Find("Sprite").GetComponent<tk2dSprite>();
				sprite.SortingOrder = ALTERNATE_KEY_SORTING_ORDER;
				newKey.GetComponent<Canvas>().sortingOrder = sprite.SortingOrder + 1;
				newKey.transform.SetParent(_alternateLettersContainer);
				newKey.transform.position = new Vector3(startX + offsetX + (gapX*col), startY + offsetY, 10.0f);
				newKey.SetActive(true);
				
				SB_Button btn = newKey.GetComponent<SB_Button>();
				btn.label = btnLabel;
				btn.OnOver += HandleOnAlternateKey;
				btn.OnOut += HandleOnAlternateKey;
				
				TintButon(btn, config.alternateDefaultTint);

				col++;
			}

			// Disable the main input keys, while the alternative keys are showing
			KeysEnabled(false);
		}

		// Enable/disable the box colliders, on the current keyboard keys
		void KeysEnabled(bool val)
		{
			BoxCollider2D[] currentKeyColliders = _currentKeysContainer.GetComponentsInChildren<BoxCollider2D>();
			foreach(BoxCollider2D collider in currentKeyColliders)
			{
				collider.enabled = val;
			}
		}

		void HandleOnAlternateKey (SB_Button btn, string eventType)
		{
			//warn ("HandleOnAlternateKey " + eventType + ", Selected: "  + btn.Selected);

			if(eventType == SB_Button.MOUSE_OVER)
			{
				_currentAlternateButton = btn;
				TintButon(btn, config.alternateRolloverTint);
			}
			else if(eventType == SB_Button.MOUSE_OUT)
			{
				_currentAlternateButton = null;
				TintButon(btn, config.alternateDefaultTint);
			}
		}

		// Switch the keyboard mode (letters/numbers/punctuation..)
		public void Mode(int modeType)
		{
			_lettersContainer.gameObject.SetActive(false);
			_numbersContainer.gameObject.SetActive(false);
			_punctuationContainer.gameObject.SetActive(false);

			_uppercaseBtn.gameObject.SetActive(false);
			_punctuationBtn.gameObject.SetActive(false);
			_lettersBtn.gameObject.SetActive(false);
			_numbersBtn.gameObject.SetActive(false);
			_numbersBtn2.gameObject.SetActive(false);

			switch(modeType)
			{
				case LETTERS:		
							_currentKeysContainer = _lettersContainer;			
							if(buttons.uppercase) _uppercaseBtn.gameObject.SetActive(true);
							if(buttons.numbers) _numbersBtn2.gameObject.SetActive(true);
							break;

				case NUMBERS:		
							_currentKeysContainer = _numbersContainer;
							if(buttons.punctuation) _punctuationBtn.gameObject.SetActive(true);
							_lettersBtn.gameObject.SetActive(true);
							break;

				case PUNCTUATION:
							_currentKeysContainer = _punctuationContainer;	
							if(buttons.numbers) _numbersBtn.gameObject.SetActive(true);
							_lettersBtn.gameObject.SetActive(true);
							break;
			}
			
			_uppercaseBtn.SetIdle();
			_punctuationBtn.SetIdle();
			_lettersBtn.SetIdle();
			_numbersBtn.SetIdle();
			_numbersBtn2.SetIdle();

			_currentKeysContainer.gameObject.SetActive(true);

			_spaceBtn.gameObject.SetActive(buttons.spacebar);
			_enterBtn.gameObject.SetActive(buttons.enter);
		}

		protected virtual void CustomizeButton (SB_Button btn)
		{
			// Overriden by subclass
		}

		// Give the button Sprite a colour tint
		protected virtual void TintButon (SB_Button btn, Color tint)
		{
			tk2dSprite btnColour = btn.gameObject.transform.Find("Sprite").GetComponent <tk2dSprite>();
			btnColour.color = tint;
		}

		private void SetUpper()
		{
			for(int i=0;i<_letters.Count;i++)
			{
				SB_Button btn = (SB_Button) _letters[i];
				btn.label = btn.label.ToUpper();
			}
		}
		
		private void SetLower()
		{
			if(config.forceUppercase) return;

			for(int i=0;i<_letters.Count;i++)
			{
				SB_Button btn = (SB_Button) _letters[i];
				btn.label = btn.label.ToLower();
			}
		}

		void ClearAlternateLetters ()
		{
			if(_currentAlternateButton != null)
			{
				LetterChosen(_currentAlternateButton.label);
				_currentAlternateButton = null;
			}

			int numChildren = _alternateLettersContainer.childCount;
			for (int i = numChildren - 1; i >= 0; i--)
			{
				GameObject child = _alternateLettersContainer.GetChild(i).gameObject;

				SB_Button btn = child.GetComponent<SB_Button>();
				btn.OnOver -= HandleOnAlternateKey;
				btn.OnOut -= HandleOnAlternateKey;

				GameObject.Destroy(child);
			}

			_alternateLettersContainer.gameObject.SetActive(false);
			KeysEnabled(true);
		}		

		// Handle keyboard key events
		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{	
			base.OnBtnHandler(btn, eventType);

			if(eventType == SB_Button.MOUSE_DOWN)
			{
				_currentButtonPressed = btn;
				_currentLetterAlternates = null;

				// Stop here, if the button pressed is not in the _letters arrayList
				if(!_letters.Contains(btn)) return;

				string letter = btn.label.ToLower();

				if(_keyAlternates.ContainsKey(letter))
				{
					string[] alternates = (string[]) _keyAlternates.GetValue(letter);
					_currentLetterAlternates = alternates;
				}
			}
			else if(eventType == SB_Button.MOUSE_UP)
			{
				//warn (btn + ", btn name: " + btn.name);

				if(btn == _deleteBtn)
				{
					if(inputString.Length>0)
					{
						inputString=inputString.Substring(0,inputString.Length-1);
					}
					if(OnKeyPressed != null) OnKeyPressed(KEY_DELETE);
				}
				else if(btn == _uppercaseBtn)
				{
					// Stop here, if uppercase is forced
					if(config.forceUppercase) return;

					uppercase = !uppercase;

					if(uppercase) SetUpper();
					else SetLower();
				}
				else if(btn == _spaceBtn)
				{
					// Stop here, if spacebar is not ticked
					if(!buttons.spacebar) return;

					inputString += " ";
					if(OnKeyPressed != null) OnKeyPressed(KEY_SPACEBAR);

				}
				else if(btn == _enterBtn)
				{
					if(OnKeyPressed != null) OnKeyPressed(KEY_ENTER);

					// Stop here, if multiline is not ticked
					if(!config.multiline) return;

					inputString += "\n";
				}
				else if(btn == _punctuationBtn)
				{
					Mode (PUNCTUATION);
				}
				else if(btn == _numbersBtn || btn == _numbersBtn2)
				{
					Mode (NUMBERS);
				}
				else if(btn == _lettersBtn)
				{
					Mode (LETTERS);
				}
				else
				{
					// User must have pressed a letter key
					LetterChosen(btn.label);
					//if(OnKeyPressed != null) OnKeyPressed(KEY_LETTER);
				}
			}
			
		}

		// Called whenever user presses a letter key
		void LetterChosen(string s)
		{	
			if((inputString.Length == 0)|| (uppercase))
			{
				s = s.ToUpper();
			}
			
			if(inputString.Length < config.maxLength)
			{
				inputString += s;
			}
			
			if(OnKeyPressed != null) OnKeyPressed(KEY_LETTER);
		}

		// Hide the keyboard
		public void HideKeyboard()
		{
			/*
			for(int i=0;i<_letters.Count;i++){
				SB_Button btn2 = (SB_Button) _letters[i];
				btn2.gameObject.SetActive(false);
			}
			*/

			this.transform.gameObject.SetActive(false);
		}
		
		public string GetString(){
			return inputString;
		}
		
		public void SetString(string val){
			inputString=val;
		}
		
		// Update is called once per frame
		void Update () 
		{
			if (UnityEngine.Input.GetMouseButtonDown (0))
			{
				_mouseDown = true;
			}
			if (UnityEngine.Input.GetMouseButtonUp (0)) 
			{
				_mouseDown = false;
				ClearAlternateLetters();
			}

			if(_mouseDown)
			{
				_keyPressedTime += Time.deltaTime;
				if(_keyPressedTime >= MAX_KEY_PRESSED_TIME)
				{
					if(_currentLetterAlternates != null && !_alternateLettersContainer.gameObject.activeSelf)
					{
						// Position the alternate letters above the pressed key
						SetupAlternateKeys(_currentLetterAlternates, _currentButtonPressed.gameObject.transform.position);
						_alternateLettersContainer.gameObject.SetActive(true);
					}
				}
			}
			else
			{
				_keyPressedTime = 0f;
			}
		}
		
		float ZoomPoint(float pt)
		{
			return pt*(SB_GameCamera.defaultZoom/(float)tk2dCamera.Instance.ZoomFactor);
		}
		
		void OnDestroy()
		{
			if(_letters != null)
			{
				for(int i=0;i<_letters.Count;i++)
				{
					SB_Button btn = (SB_Button) _letters[i];
					if(btn!=null)
					{
						Destroy(btn.gameObject);
						_letters[i] = null;
					}
				}
				_letters = null;
			}

			if(_currentLetterAlternates != null)
			{
				_currentLetterAlternates = null;
			}

			_currentButtonPressed = null;
			_currentAlternateButton = null;
			
			_initialKey = null;
			_alternateInitialKey = null;
			
			_alternateLettersContainer = null;
			_lettersContainer = null;
			_numbersContainer = null;
			_punctuationContainer = null;
			_currentKeysContainer = null;
			
			_deleteBtn = null;
			_uppercaseBtn = null;
			_punctuationBtn = null;
			_numbersBtn = null;
			_numbersBtn2 = null;
			_lettersBtn = null;
			_enterBtn = null;
			_spaceBtn = null;

			OnKeyPressed = null;

			base.OnDestroy();
		}
	}
}
