﻿using UnityEngine;

namespace ScaryBeasties.Input
{
	[System.Serializable]
	public class SB_KeyboardButtonConfig
	{
		public bool spacebar = true;
		public bool enter = true;
		public bool punctuation = true;
		public bool numbers = true;
		public bool uppercase = true;
	}

	[System.Serializable]
	public class SB_KeyboardStyleConfig
	{
		public int maxLength = 32;
		public bool forceUppercase = false;
		public bool multiline = true;

		public SB_KeyboardButtonSpacingConfig keySpacing;
		public SB_KeyboardButtonSpacingConfig alternateKeySpacing;

		// Colours for the alternate letters
		public Color alternateDefaultTint = new Color(255f/255f, 255f/255f, 255f/255f, 1);
		public Color alternateRolloverTint = new Color(30f/255f, 180f/255f, 219f/255f, 1);
	}

	[System.Serializable]
	public class SB_KeyboardButtonSpacingConfig
	{
		public int x = 0;
		public int y = 0;
	}

	// GT - This KeyboardLang enum is not currently used, but lists the langauges which are most likely to be in the apps
	public enum SB_KeyboardLang 
	{
		English,
		French,
		German,
		Spanish,
		Italian,
		Dutch,
		Norwegian,
		Swedish,
		Finnish,
		Polish
	}
}