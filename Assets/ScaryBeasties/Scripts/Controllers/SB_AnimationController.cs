﻿/**
 * Animation Controller
 * --------------------
 * 
 * George Tsappis
 * ScaryBeasties
 * July 2014
 * 
 * Description
 * --------------------
 * 
 * A class to handle the playing of animated clips, through an AnimationController state machine.
 * This class checks that the animation clip exists, before attempting to play it.
 * 
 * When creating a new instance of SB_AnimationController, 2 arguments need to be passed to the constructor:
 * 		@Animator animator The AnimatorController property from an 'Animator' control object.
 * 		@List<string> animationStates A list of strings, of the available animation states (clips).
 * 
 * Example:
 * 		_animationStates = new List<string>(new string[] {"Idle", "Hover", "Press", "Release"})
 * 		_animationController = new SB_AnimationController (GetComponent<Animator>(), _animationStates); 
 * 
 * The animation states need to be passed in as a List of strings, so that they can be checked whenever 
 * there's an attempt to play a clip.
 * If the clip does not exist, a warning/error will be output to the console.
 * 
 */ 

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ScaryBeasties.Controllers
{
	public class SB_AnimationController :Object
	{
		private Animator _animator;
		protected List<string> _animationStates;

		/**
		 * Constructor
		 * @Animator animator The AnimatorController property from an 'Animator' control object.
		 * @List<string> animationStates A list of strings, of the available animation states (clips).
		 */
		public SB_AnimationController(Animator animator, List<string> animationStates)
		{
			_animator = animator;
			_animationStates = animationStates;
		}

		/**
		 * Getter/Setter property for animationStates
		 */
		public List<string> animationStates
		{
			get 
			{
				return _animationStates;
			}

			set
			{
				_animationStates = value;
			}
		}

		public Animator animator
		{
			get{return _animator;}
			set{_animator = value;}
		}
		
		/**
		 * Plays an animation in the Animator
		 * @string state Name of the state (clip) to play
		 * @int layerIndex The layer in the state machine (By default, animations are on layer zero)
		 */
		public virtual void PlayAnimation(string stateName, int layerIndex=-1)
		{
			// By default, animations are on layer zero
			if(layerIndex == -1) layerIndex = 0;

			_animator.Play(stateName);

			/*
			if(CanPlayAnimation(stateName))
			{
				_animator.Play(stateName);
			}
			else
			{
				AnimatorStateError(stateName);
			}
			*/
		}
		
		/**
		 * Outputs a warning to the console.
		 * Either there was a problem with the Animator (null, or no controller found),
		 * or the requested state name does not exist.
		 * @string state Name of the state (clip) which was not found
		 */ 
		public void AnimatorStateError(string stateName)
		{
			warn("PlayAnimation() - Cannot play animation '" + stateName +"'. AnimatorExists: " + AnimatorExists() + ", AnimatorStateExists('" +stateName+"'): " + AnimatorStateExists(stateName));
		}
		
		/**
		 * Check if there's a valid Animator component 
		 */
		public bool AnimatorExists()
		{
			if(_animator != null && _animator.runtimeAnimatorController != null)
			{
				return true;
			}
			
			return false;
		}

		/**
		 * Make sure the 'state' exists, before trying to play it
		 * @string state Name of the state (clip) to check
		 */
		public bool AnimatorStateExists (string stateName)
		{
			if(_animationStates == null || _animationStates.Count == 0)
			{
				return false;
			}
			
			// Iterate through the _animationStates list, 
			// and examine all the available states
			foreach(string name in _animationStates)
			{
				if(name == stateName)
				{
					// Found the animation state!
					return true;
				}
			}
			
			return false;
		}

		/**
		 * Check if the Animator and stateName exist
		 * @string state Name of the state (clip) to play
		 */
		public bool CanPlayAnimation(string stateName)
		{
			if(AnimatorExists() && AnimatorStateExists(stateName))
			{
				return true;
			}

			return false;
		}

		/**
		 * Pause/Unpause the animation
		 */
		public void Pause(bool val)
		{
			if(_animator != null)
			{
				if(val) _animator.speed = 0;
				else _animator.speed = 1;
			}
		}

		protected void log(object msg) 
		{
			Debug.Log("[" + this.GetType() + "] " + msg);
		}
		
		protected void warn(object msg) 
		{
			Debug.LogWarning("[" + this.GetType() + "] " + msg);
		}
		
		protected void error(object msg) 
		{
			Debug.LogError("[" + this.GetType() + "] " + msg);
		}
		
		protected void exception(System.Exception e) 
		{
			Debug.LogException(e);
		}

		protected virtual void OnDestroy()
		{
			_animator = null;
			_animationStates = null;
		}
	}
}