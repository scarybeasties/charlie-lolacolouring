﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;
/*
using System.Reflection;
using UnityEditorInternal;
*/

namespace ScaryBeasties.Utils
{
    [RequireComponent(typeof(DynamicText))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class SB_TextField : MonoBehaviour
    {
        private DynamicText _textField;
        private MeshFilter _meshFilter;
        private string _txt = "";

        private BoxCollider2D _boxCollider2D;
        private Vector3 _meshSize;
        private float _ratio;
        private int _maxWidthPx;
        private int _maxHeightPx;

        public string textSortingLayerName = "Default";
        public int textSortingLayerId = 0;
        public int textSortingOrder = 1;

        private Vector3 _defaultScale;

        public string defaultText = "";

        /*
		void Awake()
		{
			_textField = GetComponent<DynamicText>();
			
			if(defaultText.Length > 0)
			{
				text = defaultText;
			}
		}
		*/

        // Use this for initialization
        void Start()
        {
            _textField = GetComponent<DynamicText>();
            _meshFilter = GetComponent<MeshFilter>();
            _boxCollider2D = GetComponent<BoxCollider2D>();

            _defaultScale = _textField.transform.localScale;

            /*
			Mesh sharedMesh = _meshFilter.sharedMesh;
			Bounds bounds = sharedMesh.bounds;
			_meshSize = bounds.size;
			*/

            _ratio = (_boxCollider2D.size.x * transform.localScale.x) / (_boxCollider2D.size.y * transform.localScale.y);
            _maxWidthPx = (int)(_boxCollider2D.size.x * transform.localScale.x);
            _maxHeightPx = (int)(_boxCollider2D.size.y * transform.localScale.y);

            // Disable collider, so it won't potentially interfere with buttons or other UI
            _boxCollider2D.enabled = false;
            //UpdateTextField();

            _textField = GetComponent<DynamicText>();

            if (defaultText.Length > 0)
            {
                text = defaultText;
            }

            ApplySortingOrder();
        }

        /**
		 * GT - 1.10.2014
		 * Horrible hack!!
		 * 
		 * This fixes a bug with the DynamicText field, when using 
		 * the 'VarelaRound-Regular' font.
		 * 
		 * When this font is applied, the text can sometimes appear as 
		 * gibberish, looking as though the text mesh has wrong...
		 * Only seems to happen with this font.
		 * 
		 * An error is thrown in the IDE, but can't be try/catched, because it 
		 * originates from the DynamicText class, which is sealed in a DLL file...
		 * 
		 * Might have to change this, as OnGUI can cause slowdown on some devices.
		 *
		 * TODO: Test on low powered devices, to see if OnGUI in text fields causes a problem
		 */
        void OnGUI()
        {
            if (_textField != null)
            {
                _textField.enabled = !_textField.enabled;
            }
        }


        public string text
        {
            set
            {
                string txt = value.Replace("\\n", System.Environment.NewLine);
                txt = txt.Replace("%26", "&");

                _txt = txt;
                defaultText = txt;

                UpdateTextField();
            }

            get
            {
                return _txt;
            }
        }

        void UpdateTextField()
        {
            //_textField = GetComponent<DynamicText>();

            if (_textField != null)
            {
                _textField.SetText(_txt);
                ResizeTextField();
            }
        }

        void ApplySortingOrder()
        {
            _textField.GetComponent<Renderer>().sortingLayerName = textSortingLayerName;
            _textField.GetComponent<Renderer>().sortingOrder = textSortingOrder;
        }

        void ResizeTextField()
        {
            if (_boxCollider2D == null) return;

            _textField.transform.localScale = _defaultScale;

            _meshFilter = GetComponent<MeshFilter>();
            _boxCollider2D = GetComponent<BoxCollider2D>();

            _ratio = (_boxCollider2D.size.x * transform.localScale.x) / (_boxCollider2D.size.y * transform.localScale.y);
            _maxWidthPx = (int)(_boxCollider2D.size.x * transform.localScale.x);
            _maxHeightPx = (int)(_boxCollider2D.size.y * transform.localScale.y);

            Mesh sharedMesh = _meshFilter.sharedMesh;
            Bounds bounds = sharedMesh.bounds;
            _meshSize = bounds.size;

            ApplySortingOrder();

            //Debug.LogWarning("[SB_TextField] Setting to textfield renderer.sortingLayerID to " + _textField.renderer.sortingLayerID + ", sortingLayerName: '" + _textField.renderer.sortingLayerName + "'");

            float scaleAmount = 1;
            Vector3 newScale;

            //print ("ratio: " + _ratio);

            if (_meshSize.x > (_meshSize.y / _ratio))
            {
                scaleAmount = _maxWidthPx / _meshSize.x;
            }
            else if (_meshSize.y > (_meshSize.x / _ratio))
            {
                scaleAmount = _maxHeightPx / _meshSize.y;
            }

            // Only resize the textfield if it's less than the original scale amount
            if (scaleAmount < transform.localScale.x || scaleAmount < transform.localScale.y)
            {

                newScale = new Vector3(scaleAmount, scaleAmount);
                _textField.transform.localScale = newScale;


                //bounds.size = _meshSize;
                //sharedMesh.bounds = bounds;
                //_meshFilter.sharedMesh = sharedMesh;

            }

            // TODO: Reposition the text, so that it fits within the BoxCollider bounds
            // The following code does not work properly when the scale of the game object is not 1...

            //Vector3 newPos = transform.position;
            //newPos.x += _boxCollider2D.center.x;
            //newPos.y += _boxCollider2D.center.y + (float)(_boxCollider2D.size.y * 0.5);
            //newPos.y += (_boxCollider2D.center.y + (float)(_boxCollider2D.size.y * 0.5));
            //transform.position = newPos;
        }

        void OnDestroy()
        {
            if (_textField != null)
            {
                Destroy(_textField);
                _textField = null;
            }

            _meshFilter = null;
            _boxCollider2D = null;
        }
    }
}