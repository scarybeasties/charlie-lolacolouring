using UnityEngine;
using System.Collections;

public class DebugLog : MonoBehaviour {

    private string _log = "";
    private Vector2 _scrollPosition;
    private bool _showText;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        DebugLog[] logs = FindObjectsOfType<DebugLog>();
        if(logs.Length > 1)
        {
            Destroy(gameObject);
            //Debug.Log("found an extra debuglog, destroying");
            return;
        }
 
        Application.RegisterLogCallback(HandleLog);
    }

    void HandleLog(string logString, string stackTrace, LogType type) {
        if(type == LogType.Log)
        {
            _log += logString + "\n\n";
        }
        else 
        {
            _log += logString + "\n" + stackTrace + "\n";
        }
    }

	void OnGUI()
    {
        if(GUI.Button(new Rect(Screen.width-100,Screen.height-100,100,100),"Logs"))
        {
            _showText = !_showText;
        }

        if (_showText)
        {

            _scrollPosition = GUILayout.BeginScrollView(_scrollPosition, GUILayout.Width(Screen.width), GUILayout.Height(Screen.height-100));
            GUILayout.TextArea(_log);
            if (GUILayout.Button("Clear"))
                _log = "";
        
            GUILayout.EndScrollView();
        
        }
    }
}