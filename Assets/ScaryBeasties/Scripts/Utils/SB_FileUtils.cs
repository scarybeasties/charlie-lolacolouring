﻿using UnityEngine;
using System.Collections;
using System.IO;

public class SB_FileUtils
{
	public static void RecursesiveFolderCopy(string path, string dest)
	{
		DirectoryInfo dirInfo = new DirectoryInfo(path);
		FileInfo[] files = dirInfo.GetFiles();
		
		if(!Directory.Exists(dest))
		{
			Directory.CreateDirectory(dest);
		}
		
		foreach(FileInfo file in files)
		{
			Debug.LogWarning ("\t\t\tfile: " + file.FullName);
			File.Copy(file.FullName, dest + "/" + file.Name);
		}
		
		DirectoryInfo[] dirs = dirInfo.GetDirectories();
		foreach(DirectoryInfo dir in dirs)
		{
			string destDir = dest + "/" + dir.Name;
			if(!Directory.Exists(destDir))
			{
				Directory.CreateDirectory(destDir);
			}
			RecursesiveFolderCopy(dir.FullName, destDir);
		}
	}
}
