using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class BuildEnvSettings : MonoBehaviour
{
    public const string BUILD_NUMBER = "_EDITOR_";
    public const string SPLIT_APK = "true";

    void Awake()
    {
        if (Debug.isDebugBuild)
        {
            Text text = GetComponent<Text>();
            text.text = Application.isEditor ? "####" : "#" + BUILD_NUMBER;
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }
}

