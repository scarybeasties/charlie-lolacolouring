﻿using UnityEngine;
using System.Collections;
using System;

namespace ScaryBeasties.Utils
{
	public class ScreenGrab 
	{
		public static event Action<Texture2D> OnPixelsCopied;

		public static IEnumerator CopyPixelsFromScreen(Action<Texture2D> callbackHandler, Rect portion, int destinationX=0, int destinationY=0, TextureFormat textureFormat=TextureFormat.RGB24)
		{
			yield return new WaitForEndOfFrame();

			Texture2D tex = new Texture2D((int)portion.width, (int)portion.height, textureFormat, false);
			tex.ReadPixels(portion, destinationX, destinationY,false);
			tex.Apply();

			callbackHandler(tex);
		}

		/**
		 * 
		 */
		public static IEnumerator CopyPixelsIntoMesh(Rect portion, GameObject destinationMesh=null, int destinationX=0, int destinationY=0, TextureFormat textureFormat=TextureFormat.RGB24, bool callback=false)
		{
			yield return new WaitForEndOfFrame();

			Texture2D tex = new Texture2D((int)portion.width, (int)portion.height, textureFormat, false);
			tex.ReadPixels(portion, destinationX, destinationY,false);
			tex.Apply();

			if(destinationMesh != null)
			{
				if(destinationMesh.GetComponent<Renderer>() == null)
				{
					throw(new NullReferenceException("ScreenGrab.CopyPixelsIntoMesh() :: Error - No renderer found on destinationMesh"));
				}
				else
				{
					destinationMesh.GetComponent<Renderer>().sharedMaterial.mainTexture = tex;
				}
			}

			// Call the delegate
			if(OnPixelsCopied != null) OnPixelsCopied(tex);

			if(callback) OnPixelsCopied(tex);
		}


		/**
		 * Returns a Rectangle, giving the x, y, width and height
		 * of a GameObject with a Renderer component.
		 * 
		 * If a Renderer is not attached to the GameObject, a NullReferenceException is thrown.
		 */
		public static Rect GetWorldGameObjectRect(GameObject g, tk2dCamera cam=null)
		{
			if(g.GetComponent<Renderer>() == null)
			{
				throw(new NullReferenceException("ScreenGrab.GetWorldGameObjectRect() :: Error - No renderer found on GameObject"));
			}

			Bounds bounds = g.GetComponent<Renderer>().bounds;
			Vector3 extents = bounds.extents;

			if(cam == null)
			{
				cam = tk2dCamera.Instance;
			}

			Vector3 wtsp = cam.GetComponent<Camera>().WorldToScreenPoint(g.transform.position);
			float scalerW = (float)(Screen.width) / (float)(cam.nativeResolutionWidth);
			float scalerH = (float)(Screen.height) / (float)(cam.nativeResolutionHeight);

			float w = (float)((extents.x*2) * cam.ZoomFactor) * scalerW;
			float h = (float)((extents.y*2) * cam.ZoomFactor) * scalerH;
			float x = (float)(wtsp.x - (w * 0.5));
			float y = (float)(wtsp.y - (h * 0.5));
			
			return new Rect(x, y, w, h);
		}
	}
}
