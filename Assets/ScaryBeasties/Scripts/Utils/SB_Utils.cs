﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Collections.Generic;
using System;
using System.Reflection;
using ScaryBeasties.Cam;
using System.Text;

namespace ScaryBeasties.Utils
{
	public class SB_Utils
	{
		/**
		 * Formats an XML document, with indentation and newlines
		 */
		public static string BeautifyXML (XmlDocument doc)
		{
			// NOTE UTF8 encoding adds a Byte Order Mark (BOM) which can break the XML when loading it..

			StringBuilder sb = new StringBuilder ();
			XmlWriterSettings settings = new XmlWriterSettings
			{
				Indent = true,
				IndentChars = "\t",
				NewLineChars = "\r\n",
				NewLineHandling = NewLineHandling.Replace,
				Encoding = Encoding.UTF8
			};
			using (XmlWriter writer = XmlWriter.Create(sb, settings)) {
				doc.Save (writer);
			}
			return sb.ToString ();
		}
		
		/**
		 * Check the config xml for a node, whose 'id' attribute matches 'attributeId'.
		 * If config.xml has not been loaded (and is null), then an empty string is returned.
		 */
		public static string GetNodeValueFromConfigXML (string nodePath, string attributeId, string attributeName="id")
		{
			//string txtNotFound = "Could not find xml:\n" + nodePath + ", @" + attributeId;
			string txtNotFound = "x";
			if (SB_Globals.CONFIG_XML == null)
				return txtNotFound;
			
			XmlNode result = SB_Globals.CONFIG_XML.GetNode (nodePath + "[@" + attributeName + "='" + attributeId + "']");
			
			if (result != null) {
				return result.InnerText;
			}
			
			return txtNotFound;
		}



		/**
		 * Returns a List of GameObjects, of a given tag type.
		 * 
		 * @GameObject parentObject The game object to iterate through.
		 * @string tagType The tag name of the object you want to find.
		 * @bool recursive Default is 'false'.  If set to 'true', then all children within the game object are recursed, and examined.
		 */
		public static List<GameObject> GetChildrenWithTagType (GameObject parentObject, string tagType, bool recursive=false)
		{
			List<GameObject> objects = new List<GameObject> ();
			foreach (Transform child in parentObject.transform) {
				if (child.tag == tagType) {
					//Debug.Log("\t\t** Found item of tag type " + tagType + ", name: " + child.name);
					objects.Add (child.gameObject);
				}

				if (child.childCount > 0 && recursive) {
					objects.AddRange (GetChildrenWithTagType (child.gameObject, tagType, true));
				}

			}

			return objects;
		}


		/** Easing Formulas */
		public static float EaseInOut (float curTime, float startVal, float changeVal, float duration)
		{

			curTime /= duration / 2.0f;
			if (curTime < 1)
				return changeVal / 2.0f * curTime * curTime + startVal;
			curTime -= 1.0f;
			return -changeVal / 2.0f * (curTime * (curTime - 2.0f) - 1.0f) + startVal;
		
		}

		/** Returns the distance between 2 vectors */
		public static float GetDistance (Vector2 vec1, Vector2 vec2)
		{
			float dx = vec1.x - vec2.x;
			float dy = vec1.y - vec2.y;

			return Mathf.Sqrt ((dx * dx) + (dy * dy));
		}

		public static float GetAngle (Vector2 start, Vector2 end)
		{
			float dx = end.x - start.x;
			float dy = end.y - start.y;

			return (float)(Math.Atan2 (dy, dx) * 180 / Math.PI);
		}

		public static float GetDegrees (Vector2 start, Vector2 end)
		{
			float dx = end.x - start.x;
			float dy = end.y - start.y;
			
			return (float)(Math.Atan2 (dy, dx));
		}

		public static Vector3 GetXY (float speed, float angle)
		{
			float x = (float)(speed * Math.Cos (angle * (Math.PI / 180)));
			float y = (float)(speed * Math.Sin (angle * (Math.PI / 180)));

			return new Vector3 (x, y, 0);
		}

		public static Vector2 Resize (Vector2 originalSize, Vector2 newSize)
		{
			Vector2 newScale = new Vector2 (1.0f, 1.0f);
			
			float originalWidth = originalSize.x;
			float originalHeight = originalSize.y;

			float newWidth = newSize.x;
			float newHeight = newSize.y;

			float ratio = newHeight / newWidth;
			if (originalWidth > (originalHeight / ratio)) {
				newScale.x = newScale.y = newWidth / originalWidth;
			} else {
				newScale.x = newScale.y = newHeight / originalHeight;
			}

			return newScale;
		}

		/**
		 * Returns a Vector2, resizing to fit the maximum new width or height, whichever is greater.
		 */
		public static Vector2 ResizeToFit (Vector2 originalSize, Vector2 newSize)
		{
			Vector2 newScale = new Vector2 (1.0f, 1.0f);
			
			float originalWidth = originalSize.x;
			float originalHeight = originalSize.y;
			
			float newWidth = newSize.x;
			float newHeight = newSize.y;
			/*
			float ratio1 = newHeight/newWidth;
			float ratio2 = newWidth/newHeight;

			float ratio = ratio1;
			if (ratio2 > ratio1) ratio = ratio2;

			float ratio = newHeight/newWidth;
			//if(originalWidth > (originalHeight/ratio)) 
			if(originalHeight > (originalWidth/ratio)) 
			{
				newScale.x = newScale.y = newWidth/originalWidth;
			}
			else 
			{
				newScale.x = newScale.y = newHeight/originalHeight;
			}
			*/

			float ratio1 = newWidth / originalWidth; // 6
			float ratio2 = newHeight / originalHeight; // 2
			float ratio = ratio1;
			if (ratio2 > ratio1)
				ratio = ratio2;
			newScale.x = newScale.y = ratio;

			return newScale;
		}

		public static byte[] StringToByteArray (string str)
		{
			byte[] bytes = new byte[str.Length * sizeof(char)];
			System.Buffer.BlockCopy (str.ToCharArray (), 0, bytes, 0, bytes.Length);
			return bytes;
		}
		
		public static string ByteArrayToString (byte[] bytes)
		{
			char[] chars = new char[bytes.Length / sizeof(char)];
			System.Buffer.BlockCopy (bytes, 0, chars, 0, bytes.Length);
			return new string (chars);
		}


		public static string EscapeString (string str, string charToFind="+", string charToReplace="%20")
		{
			return WWW.EscapeURL (str).Replace (charToFind, charToReplace);
		}
		
		public static string SanitizeString (string str)
		{
			// Replace \n with newline, and %26 with an ampersand
			string newString = str.Replace ("\\n", System.Environment.NewLine);
			//newString = str.Replace("\\t", "  ");
			newString = newString.Replace ("%26", "&");
			
			return newString;
		}
		
		
		public static float ZoomPoint (float pt)
		{
			return pt * (SB_GameCamera.defaultZoom / (float)tk2dCamera.Instance.ZoomFactor);
		}

		// Create a quad mesh
		public static Mesh CreateMesh ()
		{
			Mesh mesh = new Mesh ();
			
			Vector3[] vertices = new Vector3[]
			{
				new Vector3 (1, 1, 0),
				new Vector3 (1, -1, 0),
				new Vector3 (-1, 1, 0),
				new Vector3 (-1, -1, 0),
			};
			
			Vector2[] uv = new Vector2[]
			{
				new Vector2 (1, 1),
				new Vector2 (1, 0),
				new Vector2 (0, 1),
				new Vector2 (0, 0),
			};
			
			int[] triangles = new int[]
			{
				0, 1, 2,
				2, 1, 3,
			};
			
			mesh.vertices = vertices;
			mesh.uv = uv;
			mesh.triangles = triangles;
			mesh.RecalculateNormals ();
			
			return mesh;
		}

		public static string ColourToHex (Color32 color)
		{
			string hex = color.r.ToString ("X2") + color.g.ToString ("X2") + color.b.ToString ("X2");
			return hex;
		}
		
		public static Color32 HexToColour (string hex)
		{
			if (hex.Length == 0) {
				return new Color (0, 0, 0);
			}
			
			int startOffset = 0;
			if (hex.Substring (0, 1) == "#") {
				startOffset = 1;
			}
			
			byte r = byte.Parse (hex.Substring (startOffset + 0, 2), System.Globalization.NumberStyles.HexNumber);
			byte g = byte.Parse (hex.Substring (startOffset + 2, 2), System.Globalization.NumberStyles.HexNumber);
			byte b = byte.Parse (hex.Substring (startOffset + 4, 2), System.Globalization.NumberStyles.HexNumber);
			
			return new Color32 (r, g, b, 255);
		}

		public static string ParseAmazonStoreLink (string url)
		{
			string newUrl = url;
		
			// Check if the device is NOT and amazon (kindle) device
			if (SystemInfo.deviceModel.ToLower ().IndexOf ("amazon") == -1) {
				// Check if the url is a native amazon link
				string nativeStr = "amzn://apps/android?";
				int idx = newUrl.ToLower ().IndexOf (nativeStr);
				//Debug.LogWarning("idx: " + idx);
				
				// Use a browser link, for non amazon devices
				if (idx > -1) {
					string appId = url.Substring (nativeStr.Length);
					//Debug.LogWarning("appId: " + appId);
					newUrl = "http://www.amazon.com/gp/mas/dl/android?" + appId;					
				}
			}
			
			// Default - Must be an amazon device, use the original url
			return newUrl;
		}
	}
}