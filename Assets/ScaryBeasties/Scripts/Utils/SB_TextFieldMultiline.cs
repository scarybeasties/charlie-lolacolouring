﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SB_TextFieldMultiline : MonoBehaviour 
{
	private Text _text;
	private Vector2 _originalSizeDelta;
	private Vector2 _singleLineSizeDelta;
	private float _counter = 0f;
	private float _maxCounter = 0.8f;
	
	public float scale = 1.0f;

	// Use this for initialization
	void Start() 
	{
		_text = transform.GetComponent<Text>();
		
		_originalSizeDelta = _text.rectTransform.sizeDelta;
		_singleLineSizeDelta = _originalSizeDelta;
		_singleLineSizeDelta.y = _text.resizeTextMaxSize*scale;
	}
	
	void OnGUI()
	{
		_counter += Time.deltaTime;
		if(_counter > _maxCounter && _text.text.Length > 0)
		{
			_counter = 0f;
			if(_text.text.Contains(" "))
			{
				_text.rectTransform.sizeDelta = _originalSizeDelta;
			}
			else
			{
				_text.rectTransform.sizeDelta = _singleLineSizeDelta;
			}
			
			//Debug.LogError("_text.rectTransform.sizeDelta.y: " + _text.rectTransform.sizeDelta.y + ", _originalSizeDelta: " + _originalSizeDelta + ", _singleLineSizeDelta: " + _singleLineSizeDelta);
		}
	}
}
