﻿using UnityEngine;
using System.Collections;
using System.IO;
using ScaryBeasties.User;

namespace ScaryBeasties.Utils
{
    public class SB_TextureLoader : MonoBehaviour
    {


        public static void SaveTextureRange(string fname, Texture2D[] textures)
        {
            //Debug.LogWarning("SAVING");
            for (int i = 0; i < textures.Length; i++)
            {
                SB_TextureLoader.SaveTexture(fname + "_" + (i + 1), textures[i]);
            }
        }

        public static void SaveTexture(string fname, Texture2D tex)
        {
            byte[] bytes = tex.EncodeToPNG();
            File.WriteAllBytes(SB_UserProfileManager.instance.CurrentProfileFolder + fname + ".png", bytes);
            //Debug.LogWarning("SAVING "+SB_UserProfileManager.instance.CurrentProfileFolder + fname+".png");
        }

        public static void SaveTextureToFullPath(string fname, Texture2D tex)
        {
            byte[] bytes = tex.EncodeToPNG();
            File.WriteAllBytes(fname, bytes);
            //Debug.LogWarning("SAVING "+SB_UserProfileManager.instance.CurrentProfileFolder + fname+".png");
        }


        public static void SaveScaledTextureRange(string fname, Texture2D[] textures, float scl)
        {
            //Debug.LogWarning("SAVING");
            for (int i = 0; i < textures.Length; i++)
            {
                SB_TextureLoader.SaveScaledTexture(fname + "_" + (i + 1), textures[i], scl);
            }
        }

        public static void SaveScaledTextureToFullPath(string fname, Texture2D tex, float scl)
        {

            Color32[] otx = tex.GetPixels32();
            Texture2D tmptx = new Texture2D((int)tex.width, (int)tex.height);
            tmptx.SetPixels32(otx);
            TextureScale.Bilinear(tmptx, (int)(tex.width * scl), (int)(tex.height * scl));
            byte[] bytes = tmptx.EncodeToPNG();
            File.WriteAllBytes(fname, bytes);
            tmptx = null;
            otx = null;
        }

        public static void SaveScaledTexture(string fname, Texture2D tex, float scl)
        {

            Color32[] otx = tex.GetPixels32();
            Texture2D tmptx = new Texture2D((int)tex.width, (int)tex.height);
            tmptx.SetPixels32(otx);
            //int ox = (int) tex.width;
            //int oy = (int) tex.height;
            //Texture2D otex = tex.get
            TextureScale.Bilinear(tmptx, (int)(tex.width * scl), (int)(tex.height * scl));
            byte[] bytes = tmptx.EncodeToPNG();
            File.WriteAllBytes(SB_UserProfileManager.instance.CurrentProfileFolder + fname + ".png", bytes);
            //Debug.LogWarning("SAVING "+SB_UserProfileManager.instance.CurrentProfileFolder + fname+".png");

            tmptx = null;
            otx = null;
        }

        /*
		public static void LoadTextureRange(string fname, Texture2D[] textures){
			for(int i=0;i<textures.Length;i++){
				StartCoroutine(SB_TextureLoader.LoadTexture(fname+"_"+(i+1), textures[i]));
			}
		}*/

        public static IEnumerator LoadTexture(string fname, Texture2D tex)
        {
            string fileToLoad = fname + ".png";//SB_UserProfileManager.instance.CurrentProfileFolder + fname+".png";
#if UNITY_ANDROID
            Debug.LogError("[SB_TextureLoader] LoadTexture() Attempting to load from: " + fileToLoad);
            if (File.Exists(fileToLoad))
            {
                fileToLoad = "file:///" + fileToLoad;
                WWW www = new WWW(fileToLoad);
                //Debug.LogError("LOADING "+path);
                yield return www;
                if (string.IsNullOrEmpty(www.error))
                {
                    byte[] bytes = www.bytes;
                    //Debug.LogWarning("LOADING BYTES "+bytes.Length);
                    //tex = www.texture;
                    tex.LoadImage(bytes);
                }

                www.Dispose();
                www = null;
            }
            else
            {
                yield return null;
            }
#else

            if (File.Exists(fileToLoad))
            {
                tex.LoadImage(File.ReadAllBytes(fileToLoad), true);
                tex.wrapMode = TextureWrapMode.Clamp;
                yield return null;
            }
            else
            {
                yield return null;
            }
#endif
        }



        public static IEnumerator LoadTextureSized(string fname, Texture2D tex)
        {
            string fileToLoad = SB_UserProfileManager.instance.CurrentProfileFolder + fname + ".png";
            Debug.Log("[SB_TextureLoader] LoadTexture() Attempting to load from: " + fileToLoad);
#if UNITY_ANDROID
            if (File.Exists(fileToLoad))
            {
                fileToLoad = "file:///" + fileToLoad;
                WWW www = new WWW(fileToLoad);
                //Debug.LogWarning("LOADING "+SB_UserProfileManager.instance.CurrentProfileFolder fname+".png");
                yield return www;
                if (string.IsNullOrEmpty(www.error))
                {
                    byte[] bytes = www.bytes;
                    //Debug.LogWarning("LOADING BYTES "+bytes.Length);
                    //tex = www.texture;

                    tex.LoadImage(bytes);
                    //tex.Resize(www.texture.width,www.texture.height);
                    //tex.Apply();


                    //tex.Resize(www.texture.width,www.texture.height);
                }

                www.Dispose();
                www = null;
            }
            else
            {
                yield return null;
            }
#else

            if (File.Exists(fileToLoad))
            {
                tex.LoadImage(File.ReadAllBytes(fileToLoad), true);
                tex.wrapMode = TextureWrapMode.Clamp;
                yield return null;
            }
            else
            {
                yield return null;
            }
#endif
        }

        public static IEnumerator LoadIntoTexture(string fname, Texture2D tex)
        {
            string fileToLoad = SB_UserProfileManager.instance.CurrentProfileFolder + fname + ".png";
#if UNITY_ANDROID
            Debug.Log("[SB_TextureLoader] LoadTexture() Attempting to load from: " + fileToLoad);
            if (File.Exists(fileToLoad))
            {
                fileToLoad = "file:///" + fileToLoad;
                WWW www = new WWW(fileToLoad);
                yield return www;
                www.LoadImageIntoTexture(tex);

                www.Dispose();
                www = null;
            }
            else
            {
                yield return null;
            }
#else

            if (File.Exists(fileToLoad))
            {
                tex.LoadImage(File.ReadAllBytes(fileToLoad), true);
                tex.wrapMode = TextureWrapMode.Clamp;
                yield return null;
            }
            else
            {
                yield return null;
            }
#endif
        }

    }
}
