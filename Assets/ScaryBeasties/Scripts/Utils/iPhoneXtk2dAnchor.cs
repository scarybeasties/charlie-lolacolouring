﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class iPhoneXtk2dAnchor : MonoBehaviour
{

    private tk2dCameraAnchor _anchor;
    public Vector2 Offset;

    private void Awake()
    {
        _anchor = GetComponent<tk2dCameraAnchor>();

        tk2dCamera c = _anchor.AnchorCamera.GetComponent<tk2dCamera>();
        float ratio = (float)Screen.width / (float)Screen.height;
        Debug.Log(ratio);
        if (ratio > 2)
        {
            _anchor.AnchorOffsetPixels = Offset;
        }
    }
}
