﻿using UnityEngine;
using System.Collections;

namespace ScaryBeasties.Utils 
{
	public class OBBDownloader : MonoBehaviour 
	{
#if UNITY_EDITOR || UNITY_IOS
		// This function is called for IOS
		public bool OkToProceed()
		{
			return true;
		}
#elif UNITY_ANDROID
		
		private bool _isGooglePlayBuild = false;
		private bool _splitApplicationBinary = false;
		private bool _obbPresent = false;
		private bool _isDownloadingOBB = false;
		
		private float _counter = 0f;
		const float _counterMax = 3f;
		/*
		private int _numTries = 0;
		const int _maxTries = 3;
		*/
		string expPath;

		void Start()
		{
			// Check if this is a Google Play build, and that the base 64 key has been set

			if(SB_Globals.PLATFORM_ID == SB_BuildPlatforms.GooglePlay) 
			{
				_isGooglePlayBuild = true;
			}

			if(GooglePlayDownloaderSettings.base64_PublicKey.Length > 0 && GooglePlayDownloaderSettings.splitApplicationBinary)
			{
				_splitApplicationBinary = true;
			}

			Debug.LogWarning("[OBBDownloader] ---- _isGooglePlayBuild: " + _isGooglePlayBuild + ", _splitApplicationBinary: " + _splitApplicationBinary);
		}

		void Update() 
		{
			if( _obbPresent || !_splitApplicationBinary)
			{
				return;
			}
			
			// Check that this is a Google Play build, and not an Amazon build
			if( Application.platform == RuntimePlatform.Android && _isGooglePlayBuild ) 
			{
				// Make sure we're using a counter between checking for the OBB..
				// We don't want to be doing it on every frame update!
				_counter -= Time.deltaTime;
				if(_counter < 0)
				{
					_counter = _counterMax;
					LookForOBBFile();
					/*
					_numTries++;
					if(_numTries < _maxTries)
					{
						LookForOBBFile();
					}
					else
					{
						Log("Failed to find OBB file after " + _numTries + " tries");
					}
					*/
				}
			}
		}
		
		private void LookForOBBFile()
		{
			Log("LookForOBBFile() _isDownloadingOBB: " + _isDownloadingOBB);
			
			if(!GooglePlayDownloader.RunningOnAndroid()) 
			{
				Debug.LogError("Android platform not detected");
				return;
			}
			
			expPath = GooglePlayDownloader.GetExpansionFilePath();
			
			if(expPath == null)
			{
				Debug.LogError("[OBBDownloader] ---- External storage is not availible");
				return;
			} 
			else 
			{
				string mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);
				Debug.LogError("[OBBDownloader] ---- mainPath: " + mainPath);
				
				if(mainPath == null) 
				{
					Debug.LogError("[OBBDownloader] ---- mainPath is null, calling GooglePlayDownloader.FetchOBB()");
						
					// if the mainPath is null, there is currently no obb file.
					// so we need to download it.
					GooglePlayDownloader.FetchOBB();
					_isDownloadingOBB = true;
				}
				else
				{
					Debug.LogError("[OBBDownloader] ---- _obbPresent is present");
					_obbPresent = true;
					_isDownloadingOBB = false;
				}
			}
		}
		
		void Log(string msg)
		{
			Debug.LogWarning("[OBBDownloader] ---- " + msg);
		}
		
		// This function is called for Android
		public bool OkToProceed()
		{
			// Google Play build, check if split binary is ready
			if(_isGooglePlayBuild && _splitApplicationBinary)
			{
				return _obbPresent;
			}
			
			// Amazon builds, always ok to proceed (there's no split binary)
			return true;
		}
		
		public virtual void OnDestroy() {
			
		}
#endif
	}
}