﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Base;
using ScaryBeasties.Games.Common.Sprites;

namespace ScaryBeasties.Games.Common.Engines.Placement
{
	public class PlacementTarget : AnimatedSprite 
	{
		public const string MOUSE_DOWN = "MouseDown";

		// Setup delegates
		public delegate void MouseDownDelegate(PlacementTarget target, string eventType);
		public event MouseDownDelegate OnPress;

		protected string _value = "";
		protected bool _isActive = true;

		// This is used to identify the slot, and the value is set by the game on init
		public int position = 0;

		override protected void Start()
		{
			base.Start ();

			if(_value == "")
			{
				Value = _tk2dSprite.CurrentSprite.name;
				print ("PlacementTarget SPRITE NAME >>>> Value " + Value);
			}
		}

		/** 
		 * Set/Get the value of this placement target. 
		 * String val is returned, but can be casted to Int/Float etc, if required
		 */
		public string Value
		{
			get {return _value;}
			set	{_value = value;}
		}
		
		/**
		 * Getter/Setter property, so enable/disable the placement target.
		 */
		public bool isActive
		{
			get { return _isActive; }
			set { _isActive = value; }
		}

		protected virtual void OnMouseDown() 
		{
			log ("OnMouseDown " + this.name + ", isActive: " + isActive);
			if(!isActive) return;

			// Call the delgate
			if(OnPress != null) OnPress(this, MOUSE_DOWN);
		}
		
		override protected void OnDestroy()
		{
			base.OnDestroy();
			
			OnPress = null;
		}
	}
}
