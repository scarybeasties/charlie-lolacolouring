/**
 * Carousel Item class
 * -------------
 * 
 * Chris Thodesen
 * Scary Beasties Limited.
 * September 2014
 * 
 * Description
 * ------------
 * A carousel item class, necessary for items added to a vertical or 
 * horizontal scrollbar.
 * 
 * Sprites for the Locked/Unlocked/Selected states can be dragged on to 
 * the button instance, in the inspector panel.
 * 
 * Audio can be assigned to for the selected event, by entering a string
 * value on the button instance, in the inspector panel.
 * The value must match a path to audio inside the Assets/Resources folder.
**/
using UnityEngine;
using System.Collections;
using ScaryBeasties.Games.Common.Sprites;

namespace ScaryBeasties.Games.Common.Engines.Sticker 
{
	public class SB_Sticker : AnimatedSprite 
	{

		public const string STATE_DRAGGING = "stateDragging";
		public const string STATE_PLACING = "statePlacing";

		public const int BACKGROUND_STICKER = 0;
		public const int NORMAL_STICKER = 1;
		public const int FLOATING_STICKER = 2;

		public string sprite_name = "Donkey";
		public string sprite_idle = "/Idle";
		public string sprite_animate = "/Animate";
		
		public delegate void OnStickerPlacedEvent(SB_Sticker stickerReference);
		public event OnStickerPlacedEvent OnStickerPlaced;

		public delegate void OnStickerRemovedEvent(SB_Sticker stickerReference);
		public event OnStickerRemovedEvent OnStickerRemoved;

		public delegate void OnStickerMovedEvent(SB_Sticker stickerReference);
		public event OnStickerMovedEvent OnStickerMoved;

		public delegate void OnStickerSelectedEvent(SB_Sticker stickerReference);
		public event OnStickerSelectedEvent OnStickerSelected;
		
		private BoxCollider2D _collider;

		public float upper_boundary {
			get {
				return _upperBoundary;
			}
			set {
				_upperBoundary = value;
			}
		}

		public float lower_boundary {
			get {
				return _lowerBoundary;
			}
			set {
				_lowerBoundary = value;
			}
		}

		public float scale_factor {
			get {
				return _stickerScaleFactor;
			}
			set {
				_stickerScaleFactor = value;
			}
		}

		private bool _usesGravity = true;
		private float _gravity = 160f;
		private float _scale = 1.0f;
		private Vector3 _velocity = new Vector3();
		private Vector3 _position;

		private float _animationTimer = 10f;
		private float _animationMaxWait = 30f;
		private float _animationMinWait = 10f;

		private bool _startDraggingOnLoad = false;
		private bool _startFunctionCalled = false;

		private float _halfWidth;
		private float _halfHeight;
		
		private float _unityToTK2DScaleX;
		private float _unityToTK2DScaleY;

		private int _stickerType = 1;

		/* SCALING VARIABLES */
		private float _upperBoundary = 0.6f; // the percentage of the screen where scaling should stop.
		private float _lowerBoundary = 0.25f; // the percentage of the screen where scaling should start.

		private float _scalingSpace; // the space in units that the stickers scale in
		private float _lowerBoundarySize; // the size in units of the lower non-scaling region
		private float _upperBoundarySize; // the size in units of the lower non-scaling region

		private float _minimumStickerScale = 0.5f; // the smallest the sticker should get.
		private float _stickerScaleFactor = 0.5f; // the amount the stickers should scale.

		private float _stickerScalingRatio; // how much the stickers scale should change each unit it is moved.
		/* END OF SCALING VARIABLES */

		public void SetParentTransform( GameObject parent ) {
			transform.parent = parent.transform;
		}

		override protected void Start() {
			base.Start ();
			_collider = GetComponent<BoxCollider2D>();
			_position = this.transform.position;

			PlaySticker();

			if( _startDraggingOnLoad )
				SetState(STATE_DRAGGING);

			_startFunctionCalled = true;

			_unityToTK2DScaleX = Screen.width / tk2dCamera.Instance.ScreenExtents.size.x;
			_unityToTK2DScaleY = Screen.height / tk2dCamera.Instance.ScreenExtents.size.y;
			_halfWidth = tk2dCamera.Instance.ScreenExtents.size.x / 2f;
			_halfHeight = tk2dCamera.Instance.ScreenExtents.size.y / 2f;

			_spriteAnimator.AnimationCompleted = OnAnimationComplete;

			UpdateStickerSettings();
			UpdateStickerCollider();
		}

		public void SetStickerType(int item_type)
		{
			if( item_type >= 0 && item_type <= 2)
				_stickerType = item_type;
			else
			{
				warn("item type not valid, setting to normal value");
				_stickerType = NORMAL_STICKER;
			}

			UpdateStickerSettings();
		}

		public void SetRegion(float lower_boundary,float upper_boundary) 
		{
			if( lower_boundary < 0.0f)
				lower_boundary = 0.0f;

			if( upper_boundary > 1.0f)
				upper_boundary = 1.0f;

			if( upper_boundary < lower_boundary )
				upper_boundary = lower_boundary;

			if( lower_boundary > upper_boundary )
				lower_boundary = upper_boundary;

			_lowerBoundary = lower_boundary;
			_upperBoundary = upper_boundary;

			UpdateStickerSettings();
		}

		public void SetScale(float min_scale,float max_scale)
		{
			if( min_scale < 0.0f)
				min_scale = 0.0f;
			
			if( max_scale > 1.0f)
				max_scale = 1.0f;
			
			if( max_scale < min_scale )
				max_scale = min_scale;

			if( min_scale > max_scale )
				min_scale = max_scale;

			_minimumStickerScale = min_scale;
			_stickerScaleFactor = (float)max_scale - (float)min_scale;

			UpdateStickerSettings();
		}

		void UpdateStickerSettings()
		{
			_lowerBoundarySize = Screen.height * _lowerBoundary;
			_upperBoundarySize = Screen.height - (Screen.height * _upperBoundary);
			_scalingSpace = 1f - (_lowerBoundary + ( 1f - _upperBoundary ));
			
			_stickerScalingRatio = _stickerScaleFactor / _scalingSpace;
		}

		public void PlaySticker() {
			string result = _spriteAnimator == null ? "doesn't exist":"exists";
			_spriteAnimator.Play( sprite_name + sprite_idle );
			UpdateStickerCollider();
		}

		public void SetSticker( string sticker_name ) {
			sprite_name = sticker_name;
		}

		override protected void Update () {
			base.Update();
			// your code here..
			switch( _state ) {
			case STATE_DRAGGING:
				UpdateDragging();
				break;
			case STATE_PLACING:
				UpdatePlacing();
				break;
			case STATE_IDLE:
				UpdateIdle();
				break;
			}
		}

		// set the sticker to start dragging even if a mouse down
		// event hasn't been detected. Use this for when the sticker
		// is initially dragged off of the carousel.
		public void StartDraggingOnLoad() {
			if( !_startFunctionCalled )
				_startDraggingOnLoad = true;
			else 
				SetState(STATE_DRAGGING);
		}

		public void ReleaseSticker() {
			if( CheckRemove() ) {
				if( OnStickerRemoved != null ) 
				{
					OnStickerRemoved( this );
				}
			} else {
				SetState(STATE_PLACING);
				PositionSticker();
				UpdateStickerCollider();
			}
		}

		bool CheckRemove() 
		{
			Vector3 position = transform.position;
			if( position.y + _halfHeight < tk2dCamera.Instance.ScreenExtents.size.y * _lowerBoundary )
			{
				return true;
			}
			return false;
		}

		public void MoveSticker() {
			PositionSticker();
			ScaleSticker();
			UpdateStickerCollider();
			if( OnStickerMoved != null )
				OnStickerMoved( this );
		}

		void OnMouseDown() {
			SetState(STATE_DRAGGING);
			if( OnStickerSelected != null)
				OnStickerSelected( this );
		}

		void OnMouseUp() {
			ReleaseSticker();
		}

		void OnMouseDrag() {
			MoveSticker();
		}

		void UpdateIdle() {
			_animationTimer -= Time.deltaTime;
			if( _animationTimer <= 0f ) {
				_animationTimer = Random.Range(_animationMinWait,_animationMaxWait);
				_spriteAnimator.Play( sprite_name + sprite_animate );
			}
		}

		void OnAnimationComplete(tk2dSpriteAnimator sprite, tk2dSpriteAnimationClip clip) {
			_spriteAnimator.Play ( sprite_name + sprite_idle );
		}

		void UpdatePlacing() 
		{
			if(CheckAirborne()) 
			{
				UpdateGravity();
			} 
			else 
			{
				SetState(STATE_IDLE);
				if( OnStickerPlaced != null )
					OnStickerPlaced(this);
			}
			UpdateStickerCollider();
		}

		bool CheckAirborne() {
			if( _stickerType != FLOATING_STICKER ) {
				Vector3 position = transform.position;

				if( position.y + _halfHeight > tk2dCamera.Instance.ScreenExtents.size.y * _upperBoundary ) 
					return true;
				else return false;
			}
			return false;
		}

		void UpdateGravity() {
			Vector3 position = transform.position;

			_velocity.y -= _gravity * Time.deltaTime;

			position.y += _velocity.y * Time.deltaTime;
			if( position.y + _halfHeight <= tk2dCamera.Instance.ScreenExtents.size.y * _upperBoundary )
			{
				_velocity.y = 0f;
			}

			transform.position = position;
		}

		void UpdateDragging() 
		{
			Vector3 position = transform.position;
			float mouseX = (UnityEngine.Input.mousePosition.x / _unityToTK2DScaleX) - _halfWidth;
			float mouseY = (UnityEngine.Input.mousePosition.y / _unityToTK2DScaleY) - _halfHeight;
			position.x = mouseX;
			position.y = mouseY;
			transform.position = position;
		}

		void PositionSticker() 
		{
			Vector3 position = transform.position;
			float zpos;
			if( _stickerType == BACKGROUND_STICKER ) 
			{
				zpos = 1f;
			} else {
				zpos = (1f / tk2dCamera.Instance.ScreenExtents.height) * transform.position.y;
			}
			position.z = zpos;
			transform.position = position;
		}

		void UpdateStickerCollider()
		{
			if(_tk2dSprite != null)
			{
				// Resize the collider, to fit the sprite
				Bounds bounds = _tk2dSprite.GetUntrimmedBounds();

				if( _collider == null )
					return;

				if( bounds == null )
					return;

				_collider.size = new Vector2(bounds.size.x, bounds.size.y);
				_collider.offset = new Vector2(bounds.center.x, bounds.center.y );
			}
		}

		void ScaleSticker() {
			float maximumScale = _minimumStickerScale + _stickerScaleFactor;
			
			if( _stickerType == FLOATING_STICKER ) 
			{
				_spriteAnimator.Sprite.scale = new Vector3( maximumScale, maximumScale, 1f);
				return;
			}
			
			Vector3 position = transform.position;
			Vector3 scale = _spriteAnimator.Sprite.scale;
			
			float yPcnt = (1f / Screen.height) * UnityEngine.Input.mousePosition.y; // a value between 1 and 0, 1 is top of screen 0 is bottom.
			float regionPcnt = (yPcnt - _lowerBoundary) * _stickerScalingRatio;

			float scaleAmount = _stickerScaleFactor - ((yPcnt - _lowerBoundary) * _stickerScalingRatio);
			float actualScale = _minimumStickerScale + scaleAmount;
			
			if( yPcnt <= _lowerBoundary ) {
				scale.z = maximumScale; // full size
			} else if ( yPcnt >= _upperBoundary ) {
				scale.z = _minimumStickerScale; // minimum size
			} else {
				scale.z = actualScale; // scale
			}
			
			_spriteAnimator.Sprite.scale = new Vector3(scale.z,scale.z,1f);
		}

		override protected void OnDestroy() {
			base.OnDestroy();
			OnStickerPlaced = null;
			OnStickerRemoved = null;
			OnStickerMoved = null;
			OnStickerSelected = null;
		}
	}
}