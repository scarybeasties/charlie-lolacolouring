﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ScaryBeasties.Base;

/**
 * Each game's bespoke Help popup extends this class
 */
using ScaryBeasties.Sound;


namespace ScaryBeasties.Games.Common
{
	public class Help : SB_BasePopup
	{
		protected const string STATE_TUTORIAL = "state_tutorial";
		protected List<string> _tutorialScreens;
		protected int _currentTutorial = -1;

		public bool skipIntro = false;

		override protected void Start()
		{
			base.Start();
		}

		override protected void Init()
		{
			base.Init ();
			
			if(skipIntro)
			{
				IntroComplete();
			}
		}

		override protected void Update()
		{
			base.Update();
			switch( _state ) {
				case STATE_TUTORIAL:
					UpdateTutorial();
					break;
			}
		}

		protected override void UpdateReady ()
		{
			base.UpdateReady ();
			PlayNextTutorial();
		}

		protected virtual void PlayNextTutorial() 
		{
			_currentTutorial++;
			
			if( _currentTutorial >= _tutorialScreens.Count ) {
				_currentTutorial = 0;
			}

			//print ("current tutorial "+_currentTutorial+_tutorialScreens.Count);

			SetState(STATE_TUTORIAL);
			SwitchTutorialSection();
		}

		protected virtual void UpdateTutorial() {
			// override this
			//print ("help update tutorial");
		}

		public virtual void SwitchTutorialSection() {
			// override this
		}

		override public void PlayOutro(string stateName="Outro")
		{
			SB_SoundManager.instance.ClearVOQueue();

			base.PlayOutro(stateName);
		}

		override protected void OnDestroy()
		{
			_tutorialScreens = null;

			base.OnDestroy();
		}
	}
}