﻿using UnityEngine;
using System.Collections;

/**
 * A Value Object, to store game data.
 * Games can extend this class, if required, so that
 * bespoke data can be stored.
 */
using ScaryBeasties.Data;


namespace ScaryBeasties.Games.Common.VO
{
	public class GameDataVO
	{
		private int _id; // GameId
		private string _name; // Game name
		private bool _hasBeenPlayed = false;
		private int _skillLevel = 0; // What skill level has the user reached, in this game?
									 // This value only ever increments, and is used to determine 
									 // if a locked skilllevel needs unlocking
		private int _sessionSecsPlayed = 0;
		private int _totalSecsPlayed = 0;
		private int _numberOfPlays = 0;
		private bool _unlockRequired = false;
		private KeyValueObject _userContent = null;

		// TODO: add highscore array, 

		public GameDataVO(int id, string name, bool hasBeenPlayed=false, int skillLevel=0, int sessionSecsPlayed=0, int totalSecsPlayed=0, int numberOfPlays=0, KeyValueObject userContent=null)
		{
			_id = id;
			_name = name;
			_hasBeenPlayed = hasBeenPlayed;
			_skillLevel = skillLevel;
			_sessionSecsPlayed = sessionSecsPlayed;
			_totalSecsPlayed = totalSecsPlayed;
			_numberOfPlays = numberOfPlays;
			_userContent = userContent;

			if(_userContent == null)
			{
				_userContent = new KeyValueObject();
			}
		}

		/** 
		 * Reset the properties of this VO.
		 * Used whenever a profile is deleted.
		 */
		public void Reset()
		{
			// Reset everything, except for the game ID and Name
			_hasBeenPlayed = false;
			_skillLevel = 0;
			_sessionSecsPlayed = 0;
			_totalSecsPlayed = 0;
			_numberOfPlays = 0;
			_userContent = new KeyValueObject();
		}

		// Getters/Setters
		public int id						{ get{return _id;} }
		public string name					{ get {return _name;}}
		public bool hasBeenPlayed			{ get {return _hasBeenPlayed;}		set {_hasBeenPlayed = value;}}
		public int skillLevel				{ get {return _skillLevel;} 		set {_skillLevel = value;}}
		public int sessionSecsPlayed		{ get {return _sessionSecsPlayed;} 	set {_sessionSecsPlayed = value;}}
		public int totalSecsPlayed			{ get {return _totalSecsPlayed;} 	set {_totalSecsPlayed = value;}}
		public int numberOfPlays			{ get {return _numberOfPlays;} 		set {_numberOfPlays = value;}}
		public bool unlockRequired			{ get {return _unlockRequired;} 	set {_unlockRequired = value;}}
		public KeyValueObject userContent	{ get {return _userContent;} 		set {_userContent = value;}}

		public string ToString()
		{
			string str = 
						"id: " + id + 
						", name: " + name + 
						", hasBeenPlayed: " + hasBeenPlayed + 
						", skillLevel: " + skillLevel + 
						", sessionSecsPlayed: " + sessionSecsPlayed +
						", totalSecsPlayed: " + totalSecsPlayed +
						", numberOfPlays: " + numberOfPlays +
						", unlockRequired: " + unlockRequired +
						", userContent: " + _userContent.ToString();

			return str;
		}

		public string ToXML()
		{
			string str = 
				"\n\t\t<game id='" + id + "'" +
					 " name='" + name + "'" +
					 " hasBeenPlayed='" + hasBeenPlayed + "'" +
					 " skillLevel='" + skillLevel + "'" +
					 " sessionSecsPlayed='" + sessionSecsPlayed + "'" +
					 " totalSecsPlayed='" + totalSecsPlayed + "'" +
					 " numberOfPlays='" + numberOfPlays + "'" +
					 " unlockRequired='" + unlockRequired + "'" +
					 ">";

			str += "\n\t\t\t<userContent>" + _userContent.ToXML() + "\n\t\t\t</userContent>";

			str += "\n\t\t</game>";
			
			return str;
		}
	}
}
