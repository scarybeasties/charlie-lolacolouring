﻿using UnityEngine;
using System.Collections;

/**
 * A Value Object, to store game startup data.
 * Each game will check the properties, and act accordingly.
 */
namespace ScaryBeasties.Games.Common.VO
{
	public class GameStartupConfigVO
	{
		public bool hasIntro = true;
		public bool hasDifficultySelect = true;
		
		override public string ToString()
		{
			string str = "hasIntro: " + hasIntro + ", " + 
						 "hasDifficultySelect: " + hasDifficultySelect;

			return str;
		}
	}
}
