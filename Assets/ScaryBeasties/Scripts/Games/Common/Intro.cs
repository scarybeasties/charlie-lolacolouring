﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Base;
using System.Collections.Generic;
using ScaryBeasties.Controls;

/**
 * Not sure if this class is being used????????
 */
namespace ScaryBeasties.Games.Common
{
	public class Intro : SB_BaseView
	{
		//public const string PLAY_BTN = "play_btn";

		override protected void Start()
		{
			// Define the animation states (clips) which exist in this scene
			_animationStates = new List<string>(new string[] {"Intro", "Outro"});

			base.Start();
		}

		/** 
		 * There's no 'play' button on the intro screen, so once the intro has finished, we can call 
		 * PlayOutro immidiately.
		 */
		override protected void UpdateIntroComplete()
		{
			PlayOutro();
		}

		/*
		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			base.OnBtnHandler(btn, eventType);
			
			if(eventType == SB_Button.MOUSE_UP)
			{
				switch(btn.name)
				{
					case PLAY_BTN:
						PlayOutro();
						break;
						
					default:
						warn ("Unexpected button name: " + btn.name);
						break;
				}
			}
		}
		*/
	}
}
