﻿using UnityEngine;
using System.Collections;

public class PhotoHolder : MonoBehaviour 
{
	public const string OPEN = "Open";
	public const string CLOSED = "Closed";

	// Setup delegates
	public delegate void ChangeDelegate(PhotoHolder obj, string eventType);
	public event ChangeDelegate OnChanged;

	void OnEnable()
	{
		if(OnChanged != null)
		{
			OnChanged(this, OPEN);
		}
	}

	void OnMouseUp() 
	{
		this.gameObject.SetActive(false);

		if(OnChanged != null)
		{
			OnChanged(this, CLOSED);
		}
	}

	void OnDestroy()
	{
		OnChanged = null;
	}
}
