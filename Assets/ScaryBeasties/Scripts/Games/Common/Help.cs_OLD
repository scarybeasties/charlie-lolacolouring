﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ScaryBeasties.Base;

/**
 * Each game's bespoke Help popup extends this class
 */
using ScaryBeasties.Sound;
using ScaryBeasties.Controls;


namespace ScaryBeasties.Games.Common
{
	public class Help : SB_BasePopup
	{
		protected const string STATE_TUTORIAL = "state_tutorial";
		protected List<string> _tutorialScreens;
		protected int _currentTutorial = -1;
		
		protected int _currentScreen = -1;
		public int maxScreens = 1;

		public bool skipIntro = false;

		protected SB_Button _nextBtn, _prevBtn;

		override protected void Start()
		{
			base.Start();

			_nextBtn = transform.Find ("panel/next_btn").gameObject.GetComponent<SB_Button> ();
			_prevBtn = transform.Find ("panel/prev_btn").gameObject.GetComponent<SB_Button> ();
			SetupPages ();
			SetPage (0);
		}
		
		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			base.OnBtnHandler(btn, eventType);
			
			if(eventType == SB_Button.MOUSE_UP)
			{
				if(btn==_nextBtn){
					SetPage(_currentScreen+1);
				}
				if(btn==_prevBtn){
					SetPage(_currentScreen-1);
				}
			}
		}

		protected virtual void SetupPages(){

		}

		protected virtual void SetPage(int pnum){

			_currentScreen = pnum;

			for(int i=0;i<maxScreens;i++){
				GameObject tpage = transform.Find("panel/Page"+(i+1)).gameObject;
				if(i==_currentScreen){
					tpage.SetActive(true);
				}
				else{
					tpage.SetActive(false);
				}
			}

			if (_currentScreen == 0) {
				_prevBtn.gameObject.SetActive (false);
			} else {
				_prevBtn.gameObject.SetActive (true);
			}

			if (_currentScreen < (maxScreens-1)) {
				_nextBtn.gameObject.SetActive (true);
			} else {
				_nextBtn.gameObject.SetActive (false);
			}

		}
		
		override protected void Init()
		{
			base.Init ();
			
			if(skipIntro)
			{
				IntroComplete();
			}
		}

		override protected void Update()
		{
			base.Update();
			switch( _state ) {
				case STATE_TUTORIAL:
					UpdateTutorial();
					break;
			}
		}

		protected override void UpdateReady ()
		{
			base.UpdateReady ();

		}



		protected virtual void UpdateTutorial() {
			// override this
			//print ("help update tutorial");
		}

		public virtual void SwitchTutorialSection() {
			// override this
		}

		override public void PlayOutro(string stateName="Outro")
		{
			SB_SoundManager.instance.ClearVOQueue();

			base.PlayOutro(stateName);
		}

		override protected void OnDestroy()
		{
			_nextBtn = null;
			_prevBtn = null;
			
			base.OnDestroy();
		}
	}
}