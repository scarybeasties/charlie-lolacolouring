﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Base;

namespace ScaryBeasties.Games.Common.Sprites
{
	public class AnimatedSprite : SB_BaseView 
	{	
		// Store reference to the TK2D Sprite component
		protected tk2dSprite _tk2dSprite;
		protected tk2dSpriteAnimator _spriteAnimator;

		override protected void Start()
		{
			//_debugEnabled = false;

			base.Start ();
			_tk2dSprite = GetComponent<tk2dSprite>();
			_spriteAnimator = GetComponent<tk2dSpriteAnimator>();
			if(_spriteAnimator != null) _spriteAnimator.AnimationCompleted = OnAnimationCompletedDelegate;

			SetState(STATE_IDLE);
		}

		public void FlipX(int val=1)
		{
			if(val > 1 || val <-1 || val==0) val = 1;
			Vector3 newScale = transform.localScale;
			newScale.x = val;

			transform.localScale = newScale;
		}

		public void FlipY(int val=1)
		{
			if(val > 1 || val <-1 || val==0) val = 1;
			Vector3 newScale = transform.localScale;
			newScale.y = val;
			
			transform.localScale = newScale;
		}

		/** 
		 * A method to swap the sprite on the _tk2dSprite object 
		 */
		public virtual void ShowSprite(string name)
		{
			// Do nothing if the sprite is null
			if(name == null || _tk2dSprite == null) return;
			
			// Switch the TK2D sprite
			_tk2dSprite.SetSprite(name);
		}

		public float GetWidth() 
		{
			if (_tk2dSprite != null) 
			{
				return _tk2dSprite.GetUntrimmedBounds().size.x;
			}
			return 0.0f;
		}
		
		public float GetHeight() 
		{
			if(_tk2dSprite != null) 
			{
				return _tk2dSprite.GetUntrimmedBounds().size.y;
			}
			return 0.0f;
		}

		public tk2dSprite GetSprite()
		{
			return _tk2dSprite;
		}

		public virtual void PlayAnimation(string name)
		{
			// Do nothing if the anim is null
			if(name == null || _spriteAnimator == null) return;

			// Switch the TK2D animation clip
			_spriteAnimator.Play(name);
			//_spriteAnimator.AnimationCompleted = OnAnimationCompletedDelegate;
			
		}

		/** Called when the crurent 2d Toolkit animation clip has finished playing */
		protected virtual void OnAnimationCompletedDelegate (tk2dSpriteAnimator animator, tk2dSpriteAnimationClip clip)
		{
			//log("OnAnimationCompletedDelegate clip: " + clip.name);
			SetState(STATE_IDLE);
		}

		override public void Pause(bool val)
		{
			base.Pause(val);

			if(_spriteAnimator != null)
			{
				_spriteAnimator.enabled = !val;
			}
		}

		override protected void OnDestroy()
		{
			base.OnDestroy();

			_tk2dSprite=null;

			if(_spriteAnimator != null)
			{
				_spriteAnimator.AnimationCompleted = null;
				_spriteAnimator=null;
			}
		

		}
	}
}