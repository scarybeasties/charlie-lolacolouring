﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Base;
using ScaryBeasties.Controls;
using ScaryBeasties.Interfaces;
using System.Collections.Generic;

namespace ScaryBeasties.Games.Common
{
	public class DifficultySelect : SB_BaseView
	{
		public const string LEVEL_1_BTN = "level1_btn";
		public const string LEVEL_2_BTN = "level2_btn";
		public const string LEVEL_3_BTN = "level3_btn";
		public const string LEVEL_4_BTN = "level4_btn";
		public const string LEVEL_5_BTN = "level5_btn";
		public const string LEVEL_6_BTN = "level6_btn";
		public const string LEVEL_7_BTN = "level7_btn";
		public const string LEVEL_8_BTN = "level8_btn";
		public const string LEVEL_9_BTN = "level9_btn";
		public const string LEVEL_10_BTN = "level10_btn";

		protected int _currentSkillLevel = 0;
		protected bool _unlockingRequired = false;
		protected DifficultySelectPadlock _padlock = null;

		override protected void Start()
		{
			// Define the animation states (clips) which exist in this scene
			_animationStates = new List<string>(new string[] {"Intro", "Outro"});

			base.Start();
			SetupLocks();
		}

		/**
		 * This is called before the intro starts,
		 * and hides any unecessary locks, so they won't 
		 * appear when the Difficulty Select intro anim plays
		 */
		protected void SetupLocks()
		{
			int n=1;
			foreach(SB_Button button in _buttons)
			{
				if(button.name == "level" + n + "_btn")
				{
					SB_Button btn = button.GetComponent<SB_Button>();
					if(btn.transform.Find("lock")){
					if(n <= _currentSkillLevel+1)
					{
						if(_unlockingRequired && n == _currentSkillLevel+1)
						{
							// do nothing
						}
						else
						{
							btn.transform.Find("lock").gameObject.SetActive(false);
						}
					}
					}
					n++;
				}
			}
		}

		/** 
		 * Lock any buttons which are above the current skill level
		 * NOTE: 
		 * 		locking happens after IntroComplete
		 */
		public void LockButtons(int currentSkillLevel, bool unlockingRequired)
		{
			log("LockButtons() currentSkillLevel: " + currentSkillLevel + ", unlockingRequired: " + unlockingRequired);
			_currentSkillLevel = currentSkillLevel;
			_unlockingRequired = unlockingRequired;
		}

		override public void IntroComplete()
		{
			base.IntroComplete();
			ApplyLocks();
		}

		protected void ApplyLocks()
		{
			int n=1;
			foreach(SB_Button button in _buttons)
			{
				if(button.name == "level" + n + "_btn")
				{
					SB_Button btn = button.GetComponent<SB_Button>();
					if(btn.transform.Find("lock")){
					if(n <= _currentSkillLevel+1)
					{
						// Unlocked buttons
						btn.isActive = true;
						if(_unlockingRequired && n == _currentSkillLevel+1)
						{
							if(_padlock == null)
							{
								_padlock = btn.transform.Find("lock").gameObject.GetComponent<DifficultySelectPadlock>();
								_padlock.OnStateChanged += OnPadlockStateChanged;

								// Don't call _padlock.Unlock() yet.  
								// We need to ensure its Start() function has been called, and the _animationController is ready.
								// We therefore need to listen out for its 'STATE_INTRO_COMPLETE', via its OnStateChanged delegate.
							}
						}
						else
						{
							btn.transform.Find("lock").gameObject.SetActive(false);
						}
					}
					else
					{
						// Locked buttons
						btn.isActive = false;
					}
					}
					n++;
				}
			}
		}

		void OnPadlockStateChanged (SB_BaseView view, string state)
		{
			if(state == SB_BaseView.STATE_INTRO_COMPLETE)
			{
				_padlock.Unlock();
			}
			else if(state == SB_BaseView.STATE_OUTRO_COMPLETE)
			{
				_padlock.OnStateChanged -= OnPadlockStateChanged;
				_padlock.gameObject.SetActive(false);
			}
		}

		override public void ButtonsEnabled(bool val)
		{
			base.ButtonsEnabled(val);
			ApplyLocks();
		}
			
		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			base.OnBtnHandler(btn, eventType);
			
			if(eventType == SB_Button.MOUSE_UP)
			{
				switch(btn.name)
				{
					case LEVEL_1_BTN:
					case LEVEL_2_BTN:
					case LEVEL_3_BTN:
					case LEVEL_4_BTN:
					case LEVEL_5_BTN:
					case LEVEL_6_BTN:
					case LEVEL_7_BTN:
					case LEVEL_8_BTN:
					case LEVEL_9_BTN:
					case LEVEL_10_BTN:
										PlayOutro();
										break;

					default:
							warn ("Unexpected button name: " + btn.name);
							break;
				}
			}
		}

		
		override protected void OnDestroy()
		{
			if(_padlock != null)
			{
				_padlock.OnStateChanged -= OnPadlockStateChanged;
				_padlock = null;
			}

			base.OnDestroy();
		}
	}
}