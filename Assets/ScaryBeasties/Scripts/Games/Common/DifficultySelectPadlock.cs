﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Base;
using ScaryBeasties.Controls;
using ScaryBeasties.Interfaces;
using System.Collections.Generic;

namespace ScaryBeasties.Games.Common
{
	public class DifficultySelectPadlock : SB_BaseView
	{
		private tk2dSpriteAnimator _padlockAnim = null;

		override protected void Start()
		{
			// Define the animation states (clips) which exist in this scene
			_animationStates = new List<string>(new string[] {"Idle", "Unlock"});
			_padlockAnim = transform.Find("padlock").gameObject.GetComponent<tk2dSpriteAnimator>();

			base.Start();
		}

		// Play the dopesheet 'Unlock' clip
		public void Unlock()
		{
			_animationController.PlayAnimation("Unlock");
		}

		// Called from the dopesheet, to begin playing the tk2d animation of the padlock
		public void PlayUnlockAnim()
		{
			_padlockAnim.Play("unlock");
		}

		// Called from the dopesheet, when the animation has finished
		public void UnlockAnimComplete()
		{
			OutroComplete();
		}

		override protected void OnDestroy()
		{
			_padlockAnim = null;
			base.OnDestroy();
		}
	}
}