﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Base;

namespace ScaryBeasties.Games.Common.UI
{
	public class SB_Timer : SB_BaseView 
	{
		public const string STATE_TIMER_IDLE = "stateTimerIdle";
		public const string STATE_TIMER_STARTED = "stateTimerStarted";
		public const string STATE_TIMER_TICKING = "stateTimerTicking";
		public const string STATE_TIMER_FINISHED = "stateTimerFinished";
		
		// Private
		protected float _duration = 0;
		protected float _startTime = 0;
		protected float _elapsed = 0;

		// Setup delegates
		public delegate void OnClockTickDelegate(SB_BaseView view, string state, float elapsed, float duration);
		public delegate void OnClockFinishedDelegate(SB_BaseView view, string state);
		public event OnClockTickDelegate OnClockTick;
		public event OnClockFinishedDelegate OnClockFinished;

		public bool PauseTimer = false;

		public float Duration
		{
			get {return _duration;}
		}
		
		public float Elapsed
		{
			get {return _elapsed;}
		}

		public virtual void StartTimer(float duration=0)
		{
			_duration = duration;
			_startTime = Time.time;
			_elapsed = 0;

			SetState(STATE_TIMER_STARTED);
		}

		public virtual void StopTimer()
		{
			SetState(STATE_IDLE);
		}

		override protected void Update () 
		{
			base.Update();

			if(PauseTimer) return;
			
			switch(_state)
			{
				case STATE_TIMER_IDLE: break;
				case STATE_TIMER_STARTED: SetState(STATE_TIMER_TICKING); break;
				case STATE_TIMER_TICKING: UpdateTick(); break;
				case STATE_TIMER_FINISHED: UpdateFinished(); break;
			}
		}

		/** Clock is ticking */
		protected virtual void UpdateTick ()
		{
			_elapsed += Time.deltaTime;

			// Call delegate
			if(OnClockTick != null)
			{
				OnClockTick(this, _state, _elapsed, _duration);
			}

			if(_elapsed >= _duration)
			{
				SetState(STATE_TIMER_FINISHED);
			}
		}

		/** Clock has finished */
		protected virtual void UpdateFinished()
		{
			SetState(STATE_TIMER_IDLE);
			Reset();
			
			// Call delegate
			if(OnClockFinished != null)
			{
				OnClockFinished(this, _state);
			}
		}
		
		protected virtual void Reset()
		{
			_duration = 0;
			_startTime = 0;
			_elapsed = 0;
		}

		override protected void OnDestroy()
		{
			base.OnDestroy();

			OnClockTick = null;
			OnClockFinished = null;
		}
	}
}