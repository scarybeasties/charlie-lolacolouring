﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Base;

namespace ScaryBeasties.Games.Common.UI
{
	public class CircularClock : SB_Timer 
	{
		// Store references to the clock pieces
		GameObject _hand, _halfLeft, _halfRight, _backLeft, _backRight;
		float _rotationSpeed = 0;
		bool _clockPassedHalfway = false;

		// Use this for initialization
		override protected void Start () 
		{
			base.Start();

			_hand = transform.Find("hand").gameObject;
			_halfLeft = transform.Find("half_left").gameObject;
			_halfRight = transform.Find("half_right").gameObject;
			_backLeft = transform.Find("back_left").gameObject;
			_backRight = transform.Find("back_right").gameObject;

			// Hide the back right half, until the clock has started
			// This is to stop any part of it showing above the 'half_left' sprite.
			// (This was noticeable on 1x size, where a 1px strip of the 'back_right' was being shown above the 'half_left')
			_backLeft.SetActive(false);
			_backRight.SetActive(false);
		}

		override public void StartTimer(float duration=0)
		{
			_rotationSpeed = 360 / duration;
			_clockPassedHalfway = false;
			_backLeft.SetActive(true);
			_backRight.SetActive(true);

			base.StartTimer(duration);
		}

		/** Clock is ticking */
		override protected void UpdateTick ()
		{
			base.UpdateTick();

			float rotationAngle = _elapsed/_duration*360;
			float rotation = _rotationSpeed * Time.deltaTime;

			// Rotate the hand
			_hand.transform.Rotate(0,0, -rotation);

			// Rotate the right half
			_halfRight.transform.Rotate(0,0, -rotation);
			if(rotationAngle > 180)
			{
				_halfRight.SetActive(false);
				_clockPassedHalfway = true;
			}
			
			// Rotate the left half, once we've gone past half way
			if(_clockPassedHalfway)
			{
				_halfLeft.transform.Rotate(0,0, -rotation);
				if(_halfLeft.transform.rotation.z <= -1)
				{
					_halfLeft.SetActive(false);
					_clockPassedHalfway = true;
				}
			}
			
			if(rotationAngle >= 360)
			{
				_halfLeft.gameObject.SetActive(false);
				_halfRight.gameObject.SetActive(false);
			}
		}

		/** Clock has finished */
		override protected void UpdateFinished()
		{
			_hand.transform.rotation = new Quaternion(0,0,0, _hand.transform.rotation.w);
			base.UpdateFinished();
		}

		override protected void OnDestroy()
		{
			base.OnDestroy();
			
			_hand = null; 
			_halfLeft = null;
			_halfRight = null; 
			_backLeft = null;
			_backRight = null;
		}
	}
}
