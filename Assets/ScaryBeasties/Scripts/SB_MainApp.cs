﻿using UnityEngine;
using System.Collections;
using System.Xml;
using ScaryBeasties.Base;
using ScaryBeasties.Data;
using ScaryBeasties.Sound;
using ScaryBeasties.Stickers;
using System.Collections.Generic;
using ScaryBeasties.User;

namespace ScaryBeasties
{
	public class SB_MainApp : SB_BaseScene 
	{
		public string platform = "1x";
		public TextAsset configXmlFile;
		public bool devMode = false;
		public bool showDevModeIndicator = true;
		
		override protected void Start () 
		{
			Application.targetFrameRate = 60;
		
			//SB_Analytics.StartSession();

			SB_Analytics.instance.Init();
			SB_Analytics.instance.Setup(SB_Globals.ANALYTICS_APP_ID);


			base.Start();

			Screen.autorotateToLandscapeLeft = true;
			Screen.autorotateToLandscapeRight = true;
			Screen.autorotateToPortrait = false;
			Screen.autorotateToPortraitUpsideDown = false;
			Screen.orientation = ScreenOrientation.AutoRotation;

			Screen.sleepTimeout = 120;

			#if UNITY_EDITOR
			// Use the platform value set in the inspector
			tk2dSystem.CurrentPlatform = platform;
			#else
			// Detect the system capabilites, and set value accordingly
			tk2dSystem.CurrentPlatform = GetPlatform();
			#endif

			SB_Globals.DEVELOPMENT_MODE = devMode;
			SB_Globals.DEVELOPMENT_MODE_INDICATOR = showDevModeIndicator;
			if(SB_Globals.DEVELOPMENT_MODE)
			{
				SB_Globals.SHOP_ASSETS_PATH = SB_Globals.SHOP_ASSETS_PATH_DEV;
			}

			// Parse the config xml file
			SB_Globals.CONFIG_XML = new SB_XML_Parser(configXmlFile.text);
			//SB_Globals.LOCALE_STRING = GetLocaleString();
			//SB_Globals.LOCALE_ID = GetLocaleId();

			// TODO: Check if user has chosen a Language from language select screen, and load the localised config.xml file, and set locale
			//		-- (language select screen does not exist yet, 14.08.2015)
			//		-- Maybe set the language value in PlayerPrefs???

			error ("SB_Globals.LOCALE_STRING: " + SB_Globals.LOCALE_STRING + ", " + SB_Globals.LOCALE_ID);

			#if USES_CAMERA
			// Do a camera permission check
			SBNativeUtils.CameraAllowed();
			#endif

			LoadFirstScene();
		}
		
		/**
		 * Show the 'Dr Who' logo, while the next scene loads.
		 * By this point, we'll have determined whether we're running in 1x or 2x mode
		 */
		void ShowLogo()
		{
			transform.Find("Client_Logo").gameObject.SetActive(true);
		}
		
		private void LoadFirstScene()
		{
			ShowLogo();
			_loader.loadLevel(SB_Globals.SCENE_SPLASH);
		}

		/**
		 * Called when store XML has either loaded, or failed to load
		 */
		protected virtual void HandleOnXMLLoaded (bool success, string errors)
		{
			LoadFirstScene();
		}
		
		/**
		 * Returns a string '1x' or '2x', which
		 * 2D Toolkit uses to know whether to show 
		 * the low or high res graphics
		 */
		private string GetPlatform()
		{
			// Check if there's enough VRAM in the device
			if(SystemInfo.graphicsMemorySize > 120)
			{
				#if UNITY_ANDROID 
				if((Screen.orientation == ScreenOrientation.Landscape && Screen.width > 1024) ||
				   (Screen.orientation == ScreenOrientation.Portrait && Screen.height > 1024))
				{
					log ("** HIGH RES ANDROID");
					return "2x";
				}
				
				#elif UNITY_IOS
				/*********************************************
				// Check for specific devices (OLD CODE!!!!!)
				if(iPhone.generation == iPhoneGeneration.iPadMini1Gen ||
				   iPhone.generation == iPhoneGeneration.iPadUnknown ||
				   iPhone.generation == iPhoneGeneration.iPhoneUnknown ||
				   iPhone.generation == iPhoneGeneration.iPhone4 ||
				   iPhone.generation == iPhoneGeneration.iPhone4S ||
				   iPhone.generation == iPhoneGeneration.iPad3Gen)
				{
					return "2x";
				}
				************************************************/
				
				if(iPhone.generation == iPhoneGeneration.iPadUnknown ||
				   iPhone.generation == iPhoneGeneration.iPhoneUnknown ||
				   iPhone.generation >= iPhoneGeneration.iPhone4)
				{
					return "2x";
				}
				// if the devices aren't in the list, check for screen resolution
				else if((Screen.orientation == ScreenOrientation.Landscape && Screen.width > 1024) ||
				        (Screen.orientation == ScreenOrientation.Portrait && Screen.height > 1024))
				{
					// if the devices aren't in the list, check for screen resolution
					log ("** HIGH RES APPLE (other)");
					return "2x";
				}
				#endif
			}

			log ("** LOW RES");
			return "1x";
		}

		/*
		// Returns lowercase locale value, from config.xml (en, fr, de etc...)
		string GetLocaleString ()
		{
			XmlNode configNode = SB_Globals.CONFIG_XML.GetNode("config");
			string locale = SB_Globals.CONFIG_XML.GetNodeAttribute(configNode, "locale");

			locale = locale.ToLower();

			// Bodge fix, incase anyone uses 'uk' instead of 'en' as a locale value
			if (locale == "uk") locale = "en";

			return locale;
		}

		// Returns an integer representation of the locale string
		int GetLocaleId ()
		{
			int localeId = 0;

			switch (GetLocaleString ())
			{
				case "en": localeId = SB_Globals.LOCALE_ID_EN; break;
				case "de": localeId = SB_Globals.LOCALE_ID_DE; break;
				case "dk": localeId = SB_Globals.LOCALE_ID_DK; break;
				case "fr": localeId = SB_Globals.LOCALE_ID_FR; break;
				case "it": localeId = SB_Globals.LOCALE_ID_IT; break;
				case "nl": localeId = SB_Globals.LOCALE_ID_NL; break;
				case "no": localeId = SB_Globals.LOCALE_ID_NO; break;
				case "pl": localeId = SB_Globals.LOCALE_ID_NL; break;
				case "se": localeId = SB_Globals.LOCALE_ID_SE; break;
				default:
					localeId = 0;
					break;
			}

			return localeId;
		}
		*/
	}
}