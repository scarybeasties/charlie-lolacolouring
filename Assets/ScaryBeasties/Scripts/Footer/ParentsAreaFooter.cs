﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Utils;
using ScaryBeasties.Base;
using UnityEngine.UI;

namespace ScaryBeasties.Footer
{
	public class ParentsAreaFooter : SB_BaseView 
	{
		Text footerTextField;
		//SB_TextField footerTextField;

		override protected void Init()
		{
			//footerTextField = transform.Find("legals").gameObject.GetComponent<SB_TextField>();
			//footerTextField.text = SB_Utils.GetNodeValueFromConfigXML ("config/copy/parents/text", "legals");
			footerTextField = transform.Find ("text/legals").GetComponent<Text>();
			string legalString = SB_Utils.GetNodeValueFromConfigXML ("config/copy/parents/text", "legals");
			//legalString
				footerTextField.text = legalString.Replace("\\n", "\n");
		}
	}
}