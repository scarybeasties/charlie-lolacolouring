﻿using UnityEngine;
using System.Collections;
using System;

namespace ScaryBeasties.Network
{
	public class SB_ConnectionChecker
	{
		const string TEST_URL = "http://www.scarybeasties.com/app_content/connection.html";
		
		/*
		public static void CheckInternetConnection(MonoBehaviour monoBehaviour, string url, Action<bool> action)
		{
			monoBehaviour.StartCoroutine(DoCheckInternetConnection(url, action));
		}
		*/
		
		public static void CheckInternetConnection(MonoBehaviour monoBehaviour, Action<bool> action)
		{
			monoBehaviour.StartCoroutine(DoCheckInternetConnection(TEST_URL, action));
		}
				
		static IEnumerator DoCheckInternetConnection(string url, Action<bool> action)
		{
			Debug.Log("[SB_ConnectionChecker] CheckInternetConnection - Checking " + url);
			WWW www = new WWW(url);
			yield return www;
			
			if (www.error != null) 
			{
				Debug.LogError("[SB_ConnectionChecker] Error @ CheckInternetConnection - could not connect to " + url);
				action (false);
			}
			else 
			{
				action (true);
			}
		}
	}
}
