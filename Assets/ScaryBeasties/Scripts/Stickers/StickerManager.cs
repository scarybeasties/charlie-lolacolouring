﻿using UnityEngine;
using System.Collections;
using System.Xml;
using ScaryBeasties.User;

/**
 * Singleton class, to manage user sticker collections
 */
namespace ScaryBeasties.Stickers
{
	public class StickerManager
	{
		private StickerDataVO[] _allStickers;
		private BackgroundDataVO[] _allBackgrounds;

		private static StickerManager _instance = null;
		public static StickerManager instance
		{
			get 
			{
				if(_instance == null)
				{
					_instance = new StickerManager();
				}
				
				return _instance;
			}
		}

		public void Reset()
		{
			_instance = new StickerManager();
		}

		/**
		 * Constructor - Should never be called directly.
		 * Use 'StickerManager.instance' to get the singleton
		 */
		public StickerManager()
		{
			XmlNodeList stickers = SB_Globals.CONFIG_XML.GetNodeList("config/stickers/sticker");
			_allStickers = new StickerDataVO[stickers.Count];

			int n=0;
			// Parse the sticker XML nodes
			foreach(XmlElement sticker in stickers)
			{
				int id = int.Parse(sticker.GetAttribute("id"));
				string name = sticker.InnerText;

				bool collected = false;
				if(sticker.GetAttribute("collected") == "true") collected = true;

				bool enabled = false;
				if(sticker.GetAttribute("enabled") == "true") enabled = true;

				int type = int.Parse(sticker.GetAttribute("type"));
				string anim = sticker.GetAttribute("anim");
				StickerDataVO stickerData = new StickerDataVO(id, name, anim, collected, enabled, type);

				_allStickers[n] = stickerData;
				n++;
			}

			//-----------------------------------------------------------

			XmlNodeList backgrounds = SB_Globals.CONFIG_XML.GetNodeList("config/backgrounds/background");
			_allBackgrounds = new BackgroundDataVO[backgrounds.Count];
			
			n=0;
			// Parse the background XML nodes
			foreach(XmlElement background in backgrounds)
			{
				int id = int.Parse(background.GetAttribute("id"));
				string name = background.InnerText;

				bool enabled = false;
				if(background.GetAttribute("enabled") == "true") enabled = true;

				string regions = background.GetAttribute("regions");
				string scale = background.GetAttribute("scale");
				
				string[] regionsArr = regions.Split(new char[] { ',' });
				string[] scaleArr = scale.Split(new char[] { ',' });

				float regionTop = float.Parse(regionsArr[1]);
				float regionBottom = float.Parse(regionsArr[0]);
				float scaleMin = float.Parse(scaleArr[0]);
				float scaleMax = float.Parse(scaleArr[1]);

				BackgroundDataVO backgroundData = new BackgroundDataVO(id, name, enabled, regionTop, regionBottom, scaleMax, scaleMin);
				
				_allBackgrounds[n] = backgroundData;
				n++;
			}
		}

		/**
		 * User has collected a sticker
		 */
		public void CollectSticker(int id)
		{
			StickerDataVO sticker = GetSticker(id);
			sticker.enabled = true;
			sticker.collected = true;

			string collectedIds = "";
			ArrayList unlockedStickers = GetUnlockedStickers();
			for(int n=0; n<unlockedStickers.Count; n++)
			{
				sticker = (StickerDataVO)unlockedStickers[n];
				collectedIds += sticker.id;

				if(n+1 < unlockedStickers.Count)
				{
					collectedIds += ",";
				}
			}

			if(SB_UserProfileManager.instance.CurrentProfile != null)
			{
				SB_UserProfileManager.instance.CurrentProfile.collectedStickers = collectedIds;
				SB_UserProfileManager.instance.SaveProfileData(SB_UserProfileManager.instance.CurrentProfile);
			}
		}

		/**
		 * Returns the StickerDataVO for a given sticker ID.
		 * If sticker is not found, a null object is returned.
		 */
		public StickerDataVO GetSticker(int id)
		{
			foreach(StickerDataVO sticker in _allStickers)
			{
				if(sticker.id == id)
				{
					return sticker;
				}
			}

			return null;
		}

		public BackgroundDataVO GetBackground(int id)
		{
			foreach(BackgroundDataVO background in _allBackgrounds)
			{
				if(background.id == id)
				{
					return background;
				}
			}
			
			return null;
		}

		public BackgroundDataVO GetBackgroundAt(int id)
		{
			int i=0;
			foreach(BackgroundDataVO background in _allBackgrounds)
			{

				if(background.enabled)
				{
					if(i==id) return background;
					i++;
				}
			}
			return null;
		}

		/**
		 * Updates sticker data in the _allStickers array.
		 * Replaces the existing StickerDataVO with new data.
		 * This is called whenever a user collects a sticker.
		 */
		public void UpdateSticker(StickerDataVO updatedStickerData)
		{
			for(int n=0; n<_allStickers.Length; n++)
			{
				StickerDataVO sticker = _allStickers[n];
				if(sticker.id == updatedStickerData.id)
				{
					_allStickers[n] = updatedStickerData;
					return;
				}
			}
		}

		public void UpdateBackground(BackgroundDataVO updatedBackgroundData)
		{
			for(int n=0; n<_allBackgrounds.Length; n++)
			{
				BackgroundDataVO sticker = _allBackgrounds[n];
				if(sticker.id == updatedBackgroundData.id)
				{
					_allBackgrounds[n] = updatedBackgroundData;
					return;
				}
			}
		}

		public int GetNumPacks()
		{
			return SB_Globals.CONFIG_XML.GetNodeList ("config/packs/pack").Count;
		}

		/**
		 * Return a comma seperated string of unlocked pack id's
		 */
		public string GetUnlockedPacks()
		{
			XmlNodeList packs = SB_Globals.CONFIG_XML.GetNodeList("config/packs/pack");
			string unlockedPacks = "";

			foreach (XmlElement pack in packs) 
			{
				int id = int.Parse (pack.GetAttribute ("id"));
				if(SB_UserProfileManager.instance.PackIsUnlocked(id))
				{
					if(unlockedPacks.Length > 0) unlockedPacks += ",";
					unlockedPacks += id.ToString();
				}
			}

			return unlockedPacks;
		}

		public void UnlockPack(int packId)
		{
			XmlNodeList packs = SB_Globals.CONFIG_XML.GetNodeList("config/packs/pack");

			foreach(XmlElement pack in packs)
			{
				int id = int.Parse(pack.GetAttribute("id"));

				if(id == packId)
				{
					int n=0;

					string stickers = pack.GetAttribute("stickers");
					string[] stickersArr = stickers.Split(new char[] { ',' });

					Debug.LogWarning("Unlocking stickers: " + stickers);
					for(n=0; n<stickersArr.Length; n++)
					{
						int stickerId = int.Parse(stickersArr[n]);
						Debug.LogWarning("** Enabling sticker: " + stickerId);

						StickerDataVO stickerData = GetSticker(stickerId);
						stickerData.enabled = true;
						
						UpdateSticker(stickerData);
					}
					/*
					string backgrounds = pack.GetAttribute("backgrounds");
					string[] backgroundsArr = backgrounds.Split(new char[] { ',' });

					Debug.LogWarning("Unlocking backgrounds: " + backgrounds);
					for(n=0; n<backgroundsArr.Length; n++)
					{
						int backgroundId = int.Parse(backgroundsArr[n]);
						Debug.LogWarning("** Enabling background: " + backgroundId);

						BackgroundDataVO backgroundData = GetBackground(backgroundId);
						backgroundData.enabled = true;
						
						UpdateBackground(backgroundData);
					}*/

					return;
				}
			}
		}

		/**
		 * Returns an array of all the StickerDataVO objects
		 * 
		 * The array is ordered, so that the first items are the ones 
		 * which the user has collected, followed by the 
		 * uncollected items 
		 */
		public StickerDataVO[] GetAllStickers()
		{
			ArrayList unlockedStickers = GetUnlockedStickers();
			ArrayList lockedStickers = GetLockedStickers(true);

			StickerDataVO[] allStickersOrdered = new StickerDataVO[unlockedStickers.Count + lockedStickers.Count];

			int n, s = 0;
			for(n=0; n<unlockedStickers.Count; n++)
			{
				allStickersOrdered[s] = (StickerDataVO)unlockedStickers[n];
				s++;
			}

			for(n=0; n<lockedStickers.Count; n++)
			{
				allStickersOrdered[s] = (StickerDataVO)lockedStickers[n];
				s++;
			}

			return allStickersOrdered;
			//return _allStickers;
		}

		public BackgroundDataVO[] GetAllBackgrounds()
		{
			ArrayList enabledBackgrounds = GetBackgrounds(true);
			ArrayList disabledBackgrounds = GetBackgrounds(false);
			
			BackgroundDataVO[] allBackgroundsOrdered = new BackgroundDataVO[enabledBackgrounds.Count + disabledBackgrounds.Count];
			
			int n, s = 0;
			for(n=0; n<enabledBackgrounds.Count; n++)
			{
				allBackgroundsOrdered[s] = (BackgroundDataVO)enabledBackgrounds[n];
				s++;
			}

			for(n=0; n<disabledBackgrounds.Count; n++)
			{
				allBackgroundsOrdered[s] = (BackgroundDataVO)disabledBackgrounds[n];
				s++;
			}
			
			return allBackgroundsOrdered;
		}

		/**
		 * Returns an ArrayList of StickerDataVO objects, that the user has collected
		 */
		public ArrayList GetUnlockedStickers()
		{
			ArrayList stickers = new ArrayList();
			foreach(StickerDataVO sticker in _allStickers)
			{
				if((sticker.collected)&&(sticker.enabled))
				{
					stickers.Add(sticker);
				}
			}

			return stickers;
		}

		/**
		 * Returns an ArrayList of StickerDataVO objects, that the user has *not* collected
		 * @param includeDisabledStickers If set to 'true', stickers which are not enabled will also be included in the ArrayList
		 */
		public ArrayList GetLockedStickers(bool includeDisabledStickers=false)
		{
			ArrayList stickers = new ArrayList();
			foreach(StickerDataVO sticker in _allStickers)
			{
				if(!sticker.collected)
				{
					if(sticker.enabled)
					{
						// Only add 'enabled' stickers
						stickers.Add(sticker);
					}
					else if(includeDisabledStickers && !sticker.enabled)
					{
						stickers.Add(sticker);
					}
				}
			}
			
			return stickers;
		}

		/**
		 * Returns an ArrayList of StickerDataVO objects, that are enabled within the app
		 */
		public ArrayList GetEnabledStickers()
		{
			ArrayList stickers = new ArrayList();
			foreach(StickerDataVO sticker in _allStickers)
			{
				if(sticker.enabled)
				{
					stickers.Add(sticker);
				}
			}
			
			return stickers;
		}

		/**
		 * Returns an ArrayList of StickerDataVO objects, which have not yet been activated
		 * (Promo code required to unlock these)
		 */
		public ArrayList GetDisbledStickers()
		{
			ArrayList stickers = new ArrayList();
			foreach(StickerDataVO sticker in _allStickers)
			{
				if(!sticker.enabled)
				{
					stickers.Add(sticker);
				}
			}
			
			return stickers;
		}






		public ArrayList GetBackgrounds(bool enabled=true)
		{
			ArrayList backgrounds = new ArrayList();
			foreach(BackgroundDataVO background in _allBackgrounds)
			{
				if(background.enabled == enabled)
				{
					backgrounds.Add(background);
				}
			}
			
			return backgrounds;
		}
	}
}