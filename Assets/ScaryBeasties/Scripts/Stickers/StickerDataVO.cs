﻿using UnityEngine;
using System.Collections;

/**
 * A Value Object, to store sticker data.
 */
namespace ScaryBeasties.Stickers
{
	public class StickerDataVO
	{
		private int _id;

		/* Sticker name*/
		private string _name;
		private string _anim;

		/* Has user collected this sticker? */
		private bool _collected = false;

		/* Is this sticker enabled within the app? */
		private bool _enabled = false;

		/* Sticker placement type.  Is it a 'normal' sticker, or will it be fixed to the background/foreground? */
		private int _type = NORMAL_STICKER;

		/* Define the sticker types */
		public const int BACKGROUND_STICKER = 0;
		public const int NORMAL_STICKER = 1;
		public const int FLOATING_STICKER = 2;


		/* Define the top & bottom regions where this sticker can be dragged on the screen */
		//private float _regionTop = 1.0f;
		//private float _regionBottom = 0f;

		/* Define the amount of scaling to be applied to the sticker, when moved between the regions */
		//private float _scaleMax = 1.0f;
		//private float _scaleMin = 0.5f;

		/** Constructor */
		//public StickerDataVO(int id, string name, bool collected, bool enabled, float regionTop=1.0f, float regionBottom=0.0f, float scaleMax=1.0f, float scaleMin=0.5f)
		public StickerDataVO(int id, string name, string anim, bool collected, bool enabled, int type=NORMAL_STICKER)
		{
			_id = id;
			_name = name;
			_anim = anim;
			_collected = collected;
			_enabled = enabled;
			_type = type;
			/*
			_regionTop = regionTop;
			_regionBottom = regionBottom;
			_scaleMax = scaleMax;
			_scaleMin = scaleMin;
			*/
		}
		
		// Getters/Setters
		public int id {get{return _id;}}
		public string name {get{return _name;}}
		public string anim {get{return _anim;}}
		public bool collected {get {return _collected;} set{ _collected = value;}}
		public bool enabled {get{return _enabled;} set{ _enabled = value;}}
		public int type {get{return _type;}}

		/*
		public float regionTop {get{return _regionTop;}}
		public float regionBottom {get{return _regionBottom;}}
		public float scaleMax {get{return _scaleMax;}}
		public float scaleMin {get{return _scaleMin;}}
		*/

		override public string ToString()
		{
			string str = "id: " + id + ", " +
						"name: " + name + ", " + 
						"anim: " + anim + ", " + 
						"collected: " + collected + ", " + 
						"enabled: " + enabled + ", " + 
						"type: " + type;
						/*
						"regionTop: " + regionTop + ", " + 
						"regionBottom: " + regionBottom + ", " + 
						"scaleMax: " + scaleMax + ", " + 
						"scaleMin: " + scaleMin;
						*/
			
			return str;
		}
	}
}