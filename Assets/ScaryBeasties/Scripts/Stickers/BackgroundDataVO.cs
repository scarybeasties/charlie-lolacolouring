﻿using UnityEngine;
using System.Collections;
	
/**
 * A Value Object, to store sticker data.
 */
namespace ScaryBeasties.Stickers
{
	public class BackgroundDataVO
	{
		private int _id;
		
		/* Sticker name*/
		private string _name;

		/* Is this sticker enabled within the app? */
		private bool _enabled = false;
		
		/* Define the top & bottom regions where this sticker can be dragged on the screen */
		private float _regionTop = 1.0f;
		private float _regionBottom = 0f;
		
		/* Define the amount of scaling to be applied to the sticker, when moved between the regions */
		private float _scaleMax = 1.0f;
		private float _scaleMin = 0.5f;
		
		/** Constructor */
		public BackgroundDataVO(int id, string name, bool enabled, float regionTop=1.0f, float regionBottom=0.0f, float scaleMax=1.0f, float scaleMin=0.5f)
		{
			_id = id;
			_name = name;
			_enabled = enabled;
			_regionTop = regionTop;
			_regionBottom = regionBottom;
			_scaleMax = scaleMax;
			_scaleMin = scaleMin;
		}
		
		// Getters/Setters
		public int id {get{return _id;}}
		public string name {get{return _name;}}
		public bool enabled {get{return _enabled;} set{ _enabled = value;}}
		public float regionTop {get{return _regionTop;}}
		public float regionBottom {get{return _regionBottom;}}
		public float scaleMax {get{return _scaleMax;}}
		public float scaleMin {get{return _scaleMin;}}
		
		override public string ToString()
		{
			string str = "id: " + id + ", " +
						"name: " + name + ", " + 
						"enabled: " + enabled + ", " + 
						"regionTop: " + regionTop + ", " + 
						"regionBottom: " + regionBottom + ", " + 
						"scaleMax: " + scaleMax + ", " + 
						"scaleMin: " + scaleMin;
			
			return str;
		}
	}
}