using UnityEngine;
using System.Collections;
using ScaryBeasties.Utils;
using System;
using ScaryBeasties.Appstore;

namespace ScaryBeasties.WebView
{
    public class SB_WebViewHandler : MonoBehaviour
    {
        private SBWebView _webView;
        public bool isBanner;

        public delegate void ExitLinkDelegate(string url);
        public event ExitLinkDelegate OnExitLinkDelegate;

        const string WEBVEW_EXPIRY_KEY = "WebviewExpiryDate";

        // Use this for initialization
        void Start()
        {
            _webView = GetComponent<SBWebView>();

            //_webView.Hide();
            _webView.Setup();

            CacheClearCheck();

            /**
			 * NOTE: If user if offline, then we'll need to load a local
			 * webpage into the WebView.
			 * It needs to exist in the 'Assets/StreamingAssets' folder.
			 * 
			 * The method for loading the local page varies for Mac and Android.
			 */
            //For Mac Editor and iOS: _webView.url = Application.streamingAssetsPath + "/yourWebPage.html";
            //For Android: _webView.url = "file:///android_asset/yourWebPage.html";

            string url = SB_Utils.GetNodeValueFromConfigXML("config/links/link", "parents_promo_webview");
            string url_offline = SB_Utils.GetNodeValueFromConfigXML("config/links/link", "parents_promo_webview_offline");

            string targetURL = "";
            // Load local page, incase user is offline 
#if UNITY_ANDROID
            targetURL = url_offline;
#elif UNITY_IOS
            targetURL = Application.streamingAssetsPath + "/" + url_offline;
#endif

            // Is the device connected to the internet?
            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                // Append platform id (ios, android) and locale to the webview url
                url += "&pl=" + (int)SB_Globals.PLATFORM_ID + "&loc=" + SB_Globals.LOCALE_ID;

                targetURL = url;
            }

            Debug.LogWarning("[SB_WebViewHandler] url: " + targetURL);
            if (isBanner)
            {
                float scaleFactor = transform.parent.Find("Canvas").GetComponent<Canvas>().scaleFactor;
                _webView.ScaleFactor = scaleFactor;
                _webView.LoadBanner(targetURL);
            }
            else
            {
                _webView.LoadPage(targetURL);
            }

            AddDelegates();
        }

        public SBWebView WebView
        {
            get
            {
                return _webView;
            }
        }

        private void AddDelegates()
        {
            _webView.OnReceivedMessage += OnReceivedMessage;
            _webView.OnLoadComplete += OnLoadComplete;
            _webView.OnWebViewShouldClose += OnWebViewShouldClose;
            //_webView.OnEvalJavaScriptFinished += OnEvalJavaScriptFinished;
        }

        private void RemoveDelegates()
        {
            if (_webView != null)
            {
                _webView.OnReceivedMessage -= OnReceivedMessage;
                _webView.OnLoadComplete -= OnLoadComplete;
                _webView.OnWebViewShouldClose -= OnWebViewShouldClose;
                //_webView.OnEvalJavaScriptFinished -= OnEvalJavaScriptFinished;
            }
        }

        void CacheClearCheck()
        {
            // Try and get expiry date from PlayerPrefs
            string expiryStr = PlayerPrefs.GetString(WEBVEW_EXPIRY_KEY);
            //Debug.LogError("CacheClearCheck() expiryStr: " + expiryStr);

            DateTime now = DateTime.Now;

            // Set a default value, with an expiry date of yesterday
            DateTime expiry = now.AddDays(-1);
            if (expiryStr.Length > 0)
            {
                // Parse a saved expiry date
                expiry = DateTime.Parse(expiryStr);
            }

            //Debug.LogError("NOW: " + now.ToString());
            //Debug.LogError("expiry: " + expiry.ToString());

            if (now > expiry)
            {
                Debug.LogError("CacheClearCheck() Expired!");

                // Set new expiry date of tomorrow
                expiry = now.AddDays(1);

                Debug.LogError("CacheClearCheck() Setting new expiry date: " + expiry.ToString());
                PlayerPrefs.SetString(WEBVEW_EXPIRY_KEY, expiry.ToString());
            }
            else
            {
                //Debug.LogError("ok.");
            }
        }

        /**
		 * Called when the webview has loaded its content
		 * 
		 * NOTE: this won't recognise a 404 page as an 'error', it will just 
		 * return a 'true' value for success, because the actual 404 page 
		 * has loaded (instead of the desired content..)
		 */
        void OnLoadComplete(SBWebView webView, bool success, string errorMessage)
        {
            Debug.Log("OnLoadComplete " + success);
            if (!success)
            {
                Debug.Log("Something wrong in webview loading: " + errorMessage);
                RemoveDelegates();
                //webView.Hide();
            }

            //StartCoroutine(WaitThenShow());
        }

        protected virtual IEnumerator WaitThenShow()
        {
            yield return new WaitForSeconds(1.0f);
            _webView.Show();
        }

        void OnReceivedMessage(SBWebView webView, SBWebViewMessage message)
        {
            Debug.Log(message.rawMessage);

            if (string.Equals(message.path, "loadcomplete"))
            {
                //--------------------------------------
                // GT 18.11.2014
                // NOTE: this is no longer required
                //--------------------------------------

                webView.Show();
                //WaitThenShow();
                //_webView.Hide();
            }
            else if (string.Equals(message.path, "exitlink"))
            {
                string url = message.args["url"] as string;
                url = UnityEngine.Networking.UnityWebRequest.UnEscapeURL(url);
                //url = "https://itunes.apple.com/app-bundle/id1178707269?mt=8";
                //Debug.Log ("Opening exit link url: " + WWW.UnEscapeURL (message.args ["url"]));

                url = url.Replace("https", "https://");

                // The 3 vars are only used for iOS
                string appID = "";
                string campaignToken = "";
                string affiliateToken = "";

#if UNITY_IOS

                if (message.args.ContainsKey("appID")) { appID = message.args["appID"] as string; }
                if (message.args.ContainsKey("campaignToken")) { campaignToken = message.args["campaignToken"] as string; }
                if (message.args.ContainsKey("affiliateToken")) { affiliateToken = message.args["affiliateToken"] as string; }

                Debug.Log("appID: " + appID);
                Debug.Log("campaignToken: " + campaignToken);
                Debug.Log("affiliateToken: " + affiliateToken);

#endif
                if (url.Length > 0)
                {
                    Debug.Log("Opening URL " + url);
                    LogSceneEvent("OpenLink_" + url);

                    // No delegate set, so just open the link
                    Application.OpenURL(url);
                }
                else if (appID.Length > 0 && campaignToken.Length > 0)
                {
                    // This will track an event with the name of the app we are opening:
                    // 'OpenLink_NativeAppstore_sarah-duck-day-at-the-park',
                    // 'OpenLink_NativeAppstore_charlie-lola-my-little-town',
                    // etc..
                    LogSceneEvent("OpenLink_NativeAppstore_" + campaignToken);

                    // iOS - open the link in native popup, without leaving the app
                    AppstoreGameDataVO appstoreGameDataVO = new AppstoreGameDataVO(int.Parse(appID), campaignToken, affiliateToken);
                    AppstoreHandler.instance.openAppInStore(appstoreGameDataVO);
                }
                else if (OnExitLinkDelegate != null)
                {
                    OnExitLinkDelegate(url);
                }
                else
                {

                }

                SB_Analytics.instance.PromoEvent(message.rawMessage, true);
            }
        }

        public virtual string GetSceneTrackingName()
        {
            return Application.loadedLevelName;
        }

        public virtual void LogSceneEvent(string eventName)
        {
            //SB_Analytics.logEvent(GetSceneTrackingName() + "_" + eventName);
        }

        /**
		 * Return a 'false' value, to prevent 
		 * android native back button from closing the webview
		 */
        void OnWebViewShouldClose(SBWebView webView)
        {
        }

        void OnDestroy()
        {
            RemoveDelegates();
            if (_webView != null)
            {
                _webView.Close();
                Destroy(_webView);
                _webView = null;
            }

            OnExitLinkDelegate = null;
        }
    }
}