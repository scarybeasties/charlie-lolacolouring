﻿using UnityEngine;
using System.Collections;

namespace ScaryBeasties.Interfaces
{
	public interface SB_IAnimatedObject
	{
		void PlayAnimation(string stateName, int layerIndex=-1);
	}
}