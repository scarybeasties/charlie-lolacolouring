﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Controls;


/***
 * 
 * 
 * GT - 25.09.2014
 * 
 * 	THIS CLASS IS NO LONGER USED!
 *  SHOULD BE DELETED, ALONG WITH THE PREFAB..
 * 
 * 
 * 
 * 
 */

namespace ScaryBeasties.Sound
{
	public class SB_SoundToggleControls : MonoBehaviour 
	{
		// Store references to the game objects
		protected GameObject _soundOnGameObject;
		protected GameObject _soundOffGameObject;

		// Store references to the scripts, attached to the buttons
		protected SB_Button _soundOnBtn;
		protected SB_Button _soundOffBtn;

		private bool _isActive = true;

		private Vector3 _onScreenPosition;
		private Vector3 _offScreenPosition;

		private bool _initDone = false;

		/** 
		 * Awake gets called before Start, so it's better to
		 * grab the game objects within this function..
		 */
		void Awake()
		{
			if(!_initDone)
			{
				_soundOnGameObject = GameObject.Find("btn_sound_on");
				_soundOffGameObject = GameObject.Find("btn_sound_off");

				_soundOnBtn = _soundOnGameObject.GetComponent<SB_Button>() as SB_Button;
				_soundOffBtn = _soundOffGameObject.GetComponent<SB_Button>() as SB_Button;

				_onScreenPosition = _soundOnGameObject.transform.position;
				_offScreenPosition = new Vector3(_onScreenPosition.x, _onScreenPosition.y + 1000, _onScreenPosition.z);

				// Add delegates
				_soundOnBtn.OnRelease += ToggleSound;
				_soundOffBtn.OnRelease += ToggleSound;

				UpdateButtons();
			}

			_initDone = true;
		}

		/**
		 * Called whenever one of the buttons has been pressed.
		 * This will toggle the muted state of the SoundManager
		 */
		void ToggleSound (SB_Button btn, string eventType)
		{
			SB_SoundManager.instance.muted = !SB_SoundManager.instance.muted;
			UpdateButtons();
		}

		public bool isActive
		{
			get
			{
				return _isActive;
			}

			set
			{
				_isActive = value;
				//print ("----------------- SB_SoundToggleControls _isActive: " + _isActive);

				if(_isActive)
				{
					UpdateButtons();
				}
				else
				{
					_soundOnBtn.isActive = false;
					//GameObject.Find("btn_sound_on").GetComponent<SB_Button>().isActive = false;
					_soundOffBtn.isActive = false;
					//GameObject.Find("btn_sound_off").GetComponent<SB_Button>().isActive = false;

					// Set both buttons to ignore raycast
					_soundOnGameObject.layer = 2;
					_soundOffGameObject.layer = 2;
				}
			}
		}

		/**
		 * Update the button states, so that one is visible and the other invisible.
		 */
		public void UpdateButtons()
		{
			if(!_isActive) return;

			_soundOnBtn.isActive = !SB_SoundManager.instance.muted;
			_soundOffBtn.isActive = SB_SoundManager.instance.muted;
			
			_soundOnGameObject.GetComponent<Renderer>().enabled = !SB_SoundManager.instance.muted;
			_soundOffGameObject.GetComponent<Renderer>().enabled = SB_SoundManager.instance.muted;

			// Set the layer of the inactive button to '2' (Ignore Raycast)
			// This will prevent it from receiving inputs.
			// If we didn't do this, then the uppermost button would obscure inputs to the one below.
			if(_soundOnGameObject.GetComponent<Renderer>().enabled)
			{
				_soundOnGameObject.layer = 0;
				_soundOffGameObject.layer = 2;

				_soundOnGameObject.transform.position = _onScreenPosition;
				_soundOffGameObject.transform.position = _offScreenPosition;
			}
			else
			{
				_soundOnGameObject.layer = 2;
				_soundOffGameObject.layer = 0;

				_soundOnGameObject.transform.position = _offScreenPosition;
				_soundOffGameObject.transform.position = _onScreenPosition;
			}

			print ("[SB_SoundToggleControls] UpdateButtons() muted: " + SB_SoundManager.instance.muted);
		}

		void OnDestroy()
		{
			_soundOnBtn.OnRelease -= ToggleSound;
			_soundOffBtn.OnRelease -= ToggleSound;

			_soundOnGameObject = null;
			_soundOffGameObject = null;

			_soundOnBtn = null;
			_soundOffBtn = null;

		}

	}
}