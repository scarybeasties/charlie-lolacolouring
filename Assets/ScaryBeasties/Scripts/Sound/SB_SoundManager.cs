﻿using UnityEngine;
using System.Collections;

/**
 * Sound Manager
 * -------------
 * 
 * George Tsappis
 * ScaryBeasties
 * July 2014
 * 
 * Description
 * ------------
 * Handles the playback of music and sound effects.
 * This script is attached to a Prefab, containing a 
 * number of AudioSource Game objects.
 * 
 * Note:
 * -----
 * Short sound effects should be imported uncompressed as WAV / AIFF files, 
 * with the Audio Format in the inspector set to 'Native'.
 * 
 * Larger sounds (such as music) can be imported as MP3's / OGG, although this means
 * they will need to bne decompressed at runtime, which entails a processing overhead.
 * 
 * See unity audio documentation:
 * 		http://docs.unity3d.com/Manual/AudioFiles.html
 * 
 */
using ScaryBeasties.Base;
using System.Runtime.InteropServices;

namespace ScaryBeasties.Sound
{
	public class SB_SoundManager : SB_Base 
	{
		//IOS MUTE OVERRIDE
		#if UNITY_IOS
		[DllImport ("__Internal")]
		private static extern void iOSAudio_setupAudioSession();
		#endif

		private const bool OVERRIDE_MUTE = true;
		private bool _muteOverride = false;
		// Name of the prefab in the Resources folder.
		public const string resourceName = "Prefabs/SB_AudioChannels_Prefab";

		private static SB_SoundManager _instance = null;
		public static SB_SoundManager instance
		{
			get 
			{
				if(_instance == null)
				{
					// Create a new instance of SB_AudioChannels_Prefab, if one doesn't already exist in the scene
					var singletonsObjectPrefab = (SB_SoundManager)Resources.Load(resourceName, typeof(SB_SoundManager));

					// instantiate it
					_instance = (SB_SoundManager)Instantiate(singletonsObjectPrefab);
					_instance.Init();
				}

				return _instance;
			}
		}

		private AudioSource _musicChan;
		private AudioSource _voiceChan;
		private ArrayList _soundFxChannels = new ArrayList();
		private bool _muted = false;

		private string _currentMusic="";

		private ArrayList _voQueue = new ArrayList();

		public delegate void MuteDelegate(bool val);
		public event MuteDelegate OnMuted;
		
		public System.Action<string> OnVoiceChanged;


		/** 
		 * Getter & setter, to mute/unmute sounds 
		 * without having to call 
		 * 		SB_SoundManager.instance.MuteAllSounds()
		 * and 
		 * 		SB_SoundManager.instance.UnmuteAllSounds()
		 */
		public bool muted
		{
			get { return _muted; }
			set { 
					if(value)	MuteAllSounds();
					else 		UnmuteAllSounds();
				}
		}

		void Awake()
		{
			DontDestroyOnLoad(transform.gameObject);
		}

		public void Start()
		{
			_debugEnabled = false;

			if(_instance == null)
			{
				_instance = this;
				Init ();
			}
		}

		/**
		 * Find all the AudioSource objects, within this Prefab.
		 * There should be object named Music, and 8 other objects
		 * to play sound effects on.
		 * These are named Chan0 -> Chan7
		 */
		public void Init()
		{
			GameObject[] audioChannels = GameObject.FindGameObjectsWithTag("Audio");
			for (int i = 0;i < audioChannels.Length;i++)  
			{ 
				AudioSource aSrc = audioChannels[i].GetComponent<AudioSource>();
				//Debug.Log("Audio chan: " + aSrc.name);
				
				switch(aSrc.name)
				{
					case "Music": 	_musicChan = aSrc; 	break;
					case "Voice": 	_voiceChan = aSrc; 	break;

					default:
						// Push all other Audio tag objects to the _soundFxChannels ArrayList
						_soundFxChannels.Add(aSrc);		break;
				}
				
				DontDestroyOnLoad(aSrc);
			}
			
			//log("_soundFxChannels.Count: " + _soundFxChannels.Count);
			// Check if user has a saved audio preference
			if(PlayerPrefs.HasKey("SoundMuted"))
			{
				int mutedPref = PlayerPrefs.GetInt("SoundMuted");
				if(mutedPref == 1)
				{
					MuteAllSounds();
				}
			}


		}


		//Overide mute on ios
		public void MuteOverride(){
			#if UNITY_IOS
			if (OVERRIDE_MUTE) {
				//if(!_muteOverride){
					if (Application.platform == RuntimePlatform.IPhonePlayer)
					{
						iOSAudio_setupAudioSession();
						//_muteOverride=true;
					}
				//}

			}
			#endif
		}


		/**
		 * Play music on music channel
		 * @string id The name of the sound to play.  Asset must be in the 'Assets/Resources' folder.
		 * @float volume Playback volume 
		 */
		public void PlayMusic(string id, float volume=1.0f, bool loop=true)
		{
			_musicChan.clip = Resources.Load(id) as AudioClip;
			_musicChan.volume = volume;
			_musicChan.Play();
			_musicChan.loop = loop;

			MuteOverride ();

			_currentMusic=id;
			log("_musicChan.clip.name: " + _musicChan.clip.name + ", _musicChan.audio.clip.name: " + _musicChan.GetComponent<AudioSource>().clip.name);
		}

		public string CurrentMusic
		{
			get { return _currentMusic; }
		}

		public void PlayNewMusic(string id, float volume=1.0f)
		{
			if(id!=_currentMusic)
			{
				PlayMusic (id, volume);
			}
		}

		// Returns the ID of the last Voice audio which was played
		private string _lastVOPlayed = "";
		public string LastVOPlayed {get{return _lastVOPlayed; }}

		/**
		 * Play a voice on the Voice channel
		 * @string id The name of the sound to play.  Asset must be in the 'Assets/Resources' folder.
		 * @float volume Playback volume 
		 */
		public AudioSource PlayVoice(string id, float volume=1.0f)
		{
			_lastVOPlayed = id;
			
			AudioClip aud = Resources.Load(id) as AudioClip;
			if(aud != null)
			{
				_voiceChan.clip = aud;
				_voiceChan.volume = volume;
				_voiceChan.loop=false;
				_voiceChan.Play();

				/*
				while(_voQueue.Count>0){
					_voQueue.RemoveAt(0);
				}*/
				
				if(OnVoiceChanged != null)
				{
					OnVoiceChanged(id);
				}
				
				log("_voiceChan.clip.name: " + _voiceChan.clip.name + ", _voiceChan.audio.clip.name: " + _voiceChan.GetComponent<AudioSource>().clip.name);
			}
			else
			{
				error ("ERROR @ PlayVoice() id: " + id);
			}
			
			return _voiceChan;			
		}

		public void PlayVoiceQueued(string id, float volume=1.0f)
		{
			if(_voiceChan.isPlaying){
				_voQueue.Add(id);
			}
			else{
				PlayVoice(id,volume);
			}
		}

		public void PlayVoiceIfNotPlaying(string id, float volume=1.0f)
		{
			if(!_voiceChan.isPlaying){
				PlayVoice(id,volume);
			}
		}

		public void ClearVOQueue(){
			if(_voiceChan!=null){
				_voiceChan.Stop();

			}
			_voQueue.Clear();
		}

		public bool VoicePlaying()
		{
			return _voiceChan.isPlaying;     
		}

		public bool VoicePlayingOrQueued()
		{
			//return _voiceChan.isPlaying;     
			if(_voiceChan.isPlaying) return true;
			if(_voQueue.Count>0) return true;
			return false;
		}

		/** Update is called once per frame */
		protected void Update () 
		{
			if(_voiceChan!=null){
			if(!_voiceChan.isPlaying){
				if(_voQueue.Count>0){
					string id = (string) _voQueue[0];
					PlayVoice (id);
					_voQueue.RemoveAt(0);
				}
			}
			}
		}


		/**
		 * Play a sound on a sound fx channel
		 * @string id The name of the sound to play.  Asset must be in the 'Assets/Resources' folder.
		 * @float volume Playback volume 
		 * @int chan Integer, the sound channel to play on
		 */
		public AudioSource PlaySound(string id, float volume=1.0f, int chan=-1)
		{
			AudioSource soundChannel;
			if(chan < 0)
			{
				soundChannel = GetNextAvailableSoundChannel();
			}
			else
			{
				soundChannel = GetSoundChannel(chan);
			}
			
			log("chan: " + chan + ", soundChannel.name: " + soundChannel.name + ", vol: " + volume + ", sound id: " + id);

			soundChannel.clip = Resources.Load(id) as AudioClip;
			soundChannel.volume = volume;
			soundChannel.Play();
			return soundChannel;
		}


		public float SoundChannelData(int size = 1024){

				float val = 0.0f;
			for (int i = 0;i < _soundFxChannels.Count;i++)  
			{
				AudioSource src = GetSoundChannel(i);
				if(src.isPlaying){
					int ts = src.timeSamples;



					float[] spectrum = new float[size];
					//src.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);
					src.GetOutputData (spectrum,0);

					/*float[] samples = new float[size];
					for(int j=0;j<size;j++){
						samples[j] = spectrum[j];
					}*/


					val = GetDBFromSamples(spectrum);
				}
			}
			return val;
		}

		public float GetDBFromSamples(float[] samples, float refValue = 0.1f){

			float total = 0.0f;
			for (int i=0; i<samples.Length; i++) {
				total+=samples[i]*samples[i];
			}
			float rmsValue = Mathf.Sqrt (total / (float)samples.Length);

			float dbValue = 20*Mathf.Log10(rmsValue/refValue); 

			return rmsValue;
		}

		public AudioSource PlaySoundClip(AudioClip clip, float volume=1.0f, int chan=-1)
		{
			AudioSource soundChannel;
			if(chan < 0)
			{
				soundChannel = GetNextAvailableSoundChannel();
			}
			else
			{
				soundChannel = GetSoundChannel(chan);
			}
			

			soundChannel.clip = clip;
			soundChannel.volume = volume;
			soundChannel.Play();
			return soundChannel;
		}


		/**
		 * Play a sound on a sound fx channel with a range of random values
		 * @string id The name of the sound to play.  Asset must be in the 'Assets/Resources' folder.
		 * @float volume Playback volume 
		 * @int chan Integer, the sound channel to play on
		 */
		public AudioSource PlaySoundInRange(string id, int max, float volume=1.0f, int chan=-1)
		{
			int rand = 1+ Random.Range (0, max);
			id=id+rand;
			return PlaySound(id,volume,chan);
		}

		public AudioSource PlaySoundInRangeDelayed(string id, int max, float delay=0.0f, float volume=1.0f, int chan=-1)
		{
			int rand = 1+ Random.Range (0, max);
			id=id+rand;
			return PlaySoundDelayed(id,delay,volume,chan);
		}

		/**
		 * Play a sound on a sound fx channel with a delayed start
		 * @string id The name of the sound to play.  Asset must be in the 'Assets/Resources' folder.
		 * @float volume Playback volume 
		 * @int chan Integer, the sound channel to play on
		 */
		public AudioSource PlaySoundDelayed(string id, float delay=0.0f, float volume=1.0f, int chan=-1)
		{
			AudioSource soundChannel;
			if(chan < 0)
			{
				soundChannel = GetNextAvailableSoundChannel();
			}
			else
			{
				soundChannel = GetSoundChannel(chan);
			}
			
			log("chan: " + chan + ", soundChannel.name: " + soundChannel.name + ", vol: " + volume + ", sound id: " + id);
			
			soundChannel.clip = Resources.Load(id) as AudioClip;
			soundChannel.volume = volume;
			soundChannel.PlayDelayed(delay);

			return soundChannel;
		}

		/** Returns a given AudioSource sound channel */
		public AudioSource GetSoundChannel(int channelId)
		{
			if(channelId < 0 || _soundFxChannels.Count == 0) return null;

			// If requested channel id is out of bounds, use channel 0
			if(channelId > _soundFxChannels.Count-1) channelId = 0;

			return _soundFxChannels[channelId] as AudioSource;
		}

		/** Pause or unpause all sound fx */
		public void PauseSoundFX(bool val, int exceptChan=-1)
		{
			for (int i = 0;i < _soundFxChannels.Count;i++)  
			{
				AudioSource chan = GetSoundChannel(i);
				if(chan.clip != null && i != exceptChan)
				{
					if(val)
					{
						chan.Pause();
					}
					else
					{
						chan.Play();
					}
				}
			}
		}

		/** Stops all sound channels, including music*/
		public void StopAllSounds()
		{
			_musicChan.Stop();
			_voiceChan.Stop();

			for (int i = 0;i < _soundFxChannels.Count;i++)  
			{
				GetSoundChannel(i).Stop ();
			}
		}

		/** Stops all fx*/
		public void StopAllFX(bool nullifyClip=false)
		{

			for (int i = 0;i < _soundFxChannels.Count;i++)  
			{
				if(GetSoundChannel(i)!=null){
				GetSoundChannel(i).Stop ();
				if(nullifyClip)
				{
					GetSoundChannel(i).clip = null;
				}
				}
			}
		}

		/** Stops all fx*/
		public void StopAllVO()
		{
			_voiceChan.Stop();
		}


		public void StopMusic()
		{
			_musicChan.Stop ();
			_currentMusic = "";
		}

		/** 
		 * Mute all sound channels, including music 
		 * 
		 * GT - 31/10/2014,
		 * 		Voice channel and sound fx are now never muted, 
		 * 		only the music
		 */
		public void MuteAllSounds()
		{
			PlayerPrefs.SetInt("SoundMuted", 1);
			
			_muted = true;
			MuteMusic();

			/*
			_voiceChan.mute = true;

			for (int i = 0;i < _soundFxChannels.Count;i++)  
			{ 
				AudioSource chan = _soundFxChannels[i] as AudioSource;
				chan.mute = true;
			}
			*/

			if(OnMuted != null)
			{
				OnMuted(_muted);
			}
		}
		
		/** Unmute all sound channels, including music */
		public void UnmuteAllSounds()
		{
			PlayerPrefs.SetInt("SoundMuted", 0);

			_muted = false;
			UnmuteMusic();
			/*
			_voiceChan.mute = false;
			
			for (int i = 0;i < _soundFxChannels.Count;i++)  
			{ 
				AudioSource chan = _soundFxChannels[i] as AudioSource;
				chan.mute = false;
			}
			*/

			if(OnMuted != null)
			{
				OnMuted(_muted);
			}
		}

		public AudioSource MusicChan()
		{
			return _musicChan;
		}

		public void MuteMusic()
		{
			_musicChan.mute = true;
		}

		public void UnmuteMusic()
		{
			_musicChan.mute = false;
		}
			
		public void MuteVoice()
		{
			_voiceChan.mute = true;
			
			for (int i = 0;i < _soundFxChannels.Count;i++)  
			{ 
				AudioSource chan = _soundFxChannels[i] as AudioSource;
				chan.mute = true;
			}
		}

		public void UnmuteVoice()
		{
			_voiceChan.mute = false;
			
			for (int i = 0;i < _soundFxChannels.Count;i++)  
			{ 
				AudioSource chan = _soundFxChannels[i] as AudioSource;
				chan.mute = false;
			}
		}

		/**
		 * Returns the next available AudioSource sound channel
		 */
		public AudioSource GetNextAvailableSoundChannel()
		{
			log ("GetNextAvailableSoundChannel _soundFxChannels.Count: " + _soundFxChannels.Count);

			for (int i = 0;i < _soundFxChannels.Count;i++)  
			{ 
				AudioSource chan = GetSoundChannel(i) as AudioSource;
				
				log ("chan '" + chan.name + "' isPlaying: " + chan.isPlaying);
				if(!chan.isPlaying)
				{
					log("Channel is available: " + chan.name);
					return chan;
				}
			}

			// Default: If all sound channels are in use, return chan zero
			return GetSoundChannel(0);
		}
	}
}