﻿using UnityEngine;
using System.Collections;
//using VoxelBusters.NativePlugins;

namespace ScaryBeasties.Common.Sharing
{
	public class SB_ShareComponent : MonoBehaviour 
	{
		// GT - Commented out, as this app does not use Sharing, 
		// and the VoxelBlasters files have been deleted
		/*
		public int ButtonSpacing = 60;
		
		public const string BTN_EMAIL = "share_email_btn";
		public const string BTN_FACEBOOK = "share_facebook_btn";
		public const string BTN_TWITTER = "share_twitter_btn";
		public const string BTN_WHATSAPP = "share_whatsapp_btn";
		//public const string BTN_MESSAGING = "messaging_btn";
		public const string BTN_INSTAGRAM = "share_instagram_btn";
		public const string BTN_OTHERS = "share_others_btn";
		
		private ArrayList _shareButtons = new ArrayList();
		
		void Awake()
		{
			SetupEmailSharing();
			SetupFacebookSharing();
			SetupTwitterSharing();
			SetupWhatsAppSharing();
			SetupInstagramSharing();
			//SetupMessagingSharing ();
			SetupOthersSharing();
			
			// Ensure button spacing is not too high!
			float screenWidth = tk2dCamera.Instance.nativeResolutionWidth;
			float spacing = screenWidth / _shareButtons.Count;
			Log("screenWidth: " + screenWidth + ", ButtonSpacing: " + ButtonSpacing + ", spacing: " + spacing);

			if(spacing < ButtonSpacing)
			{
				ButtonSpacing = Mathf.RoundToInt(spacing);
			}
			
			float btnsWidth = 0f;
			for(int n=0; n<_shareButtons.Count; n++)
			{
				Transform btn = (Transform)_shareButtons[n];
				
				btnsWidth = n * ButtonSpacing;
				Vector2 pos = btn.localPosition;
				pos.x = btnsWidth;
				btn.localPosition = pos;
			}
			
			Transform buttonHolder = transform.Find("Buttons");
			Vector2 buttonHolderPos = buttonHolder.localPosition;
			buttonHolderPos.x = -(btnsWidth * 0.5f);
			buttonHolder.localPosition = buttonHolderPos;
		}
		
		void SetupEmailSharing()
		{
			Transform btn = transform.Find("Buttons/" + BTN_EMAIL);
			AddButton(btn, NPBinding.Sharing.IsMailServiceAvailable());
		}
		
		void SetupFacebookSharing()
		{
			Transform btn = transform.Find("Buttons/" + BTN_FACEBOOK);
			AddButton(btn, NPBinding.Sharing.IsFBShareServiceAvailable());
		}

		void SetupTwitterSharing()
		{
			Transform btn = transform.Find("Buttons/" + BTN_TWITTER);
			AddButton(btn, NPBinding.Sharing.IsTwitterShareServiceAvailable());
		}
		
		void SetupWhatsAppSharing()
		{
			Transform btn = transform.Find("Buttons/" + BTN_WHATSAPP);
			AddButton(btn, NPBinding.Sharing.IsWhatsAppServiceAvailable());
		}

		
		void SetupInstagramSharing()
		{
			Transform btn = transform.Find("Buttons/" + BTN_INSTAGRAM);
			AddButton(btn, NPBinding.Sharing.IsInstagramServiceAvailable());
		}
		
		void SetupOthersSharing()
		{
			Transform btn = transform.Find("Buttons/" + BTN_OTHERS);
			AddButton(btn, true);
		}
		
		void AddButton(Transform btn, bool val)
		{
			if(val)
			{
				_shareButtons.Add(btn);
			}
			else
			{
				Destroy(btn.gameObject);
			}
		}
		
		#region Public Methods
		
		public void SendEmailWithImage(string subject, string body, string[] recipients, Texture2D texture)
		{
			// Create composer
			MailShareComposer	_composer	= new MailShareComposer();
			_composer.Subject				= subject;
			_composer.Body					= body;
			_composer.IsHTMLBody			= true;
			_composer.ToRecipients			= recipients;
			_composer.AttachImage(texture);
			
			// Show share view
			NPBinding.Sharing.ShowView(_composer, FinishedSharing);
		}

		public void ShareImageOnMessage(string message, Texture2D texture)
		{
			// Create composer
			MessageShareComposer _composer = new MessageShareComposer ();
			_composer.Body				= message;
		//	_composer.AttachImage(texture);
			
			// Show share view
			NPBinding.Sharing.ShowView(_composer, FinishedSharing);
		}
		
		public void ShareImageOnFB(string message, Texture2D texture)
		{
			// Create composer
			FBShareComposer _composer	= new FBShareComposer();
			_composer.Text				= message;
			_composer.AttachImage(texture);
			
			// Show share view
			NPBinding.Sharing.ShowView(_composer, FinishedSharing);
		}
		
		public void ShareImageOnTwitter(string message, Texture2D texture)
		{
			// Create composer
			TwitterShareComposer _composer	= new TwitterShareComposer();
			_composer.Text					= message;
			_composer.AttachImage(texture);
			
			// Show share view
			NPBinding.Sharing.ShowView(_composer, FinishedSharing);			
		}
		
		public void ShareImageOnWhatsApp(string message, Texture2D texture)
		{
			// Create composer
			WhatsAppShareComposer _composer	= new WhatsAppShareComposer();
			_composer.Text = message;
			_composer.AttachImage(texture);
			
			// Show share view
			NPBinding.Sharing.ShowView(_composer, FinishedSharing);	
		}
		
		public void ShareImageOnInstagram(string message, Texture2D texture)
		{
			InstagramShareComposer _composer	= new InstagramShareComposer();
			_composer.Text = message;
			_composer.AttachImage(texture);
			
			// Show share view
			NPBinding.Sharing.ShowView(_composer, FinishedSharing);	
		}
		
		public void ShareImageUsingShareSheet(string message, Texture2D texture)
		{
			eShareOptions[]	m_excludedOptions = new eShareOptions[] 
			{ 
				//eShareOptions.MESSAGE,
				eShareOptions.MAIL,
				eShareOptions.FB,
				eShareOptions.TWITTER,
				//eShareOptions.INSTAGRAM
			};
			
			// Create share sheet
			ShareSheet _shareSheet 	= new ShareSheet();	
			_shareSheet.Text		= message;
			_shareSheet.ExcludedShareOptions	= m_excludedOptions;
			_shareSheet.AttachImage(texture);
			
			// Show composer
			NPBinding.UI.SetPopoverPointAtLastTouchPosition();
			NPBinding.Sharing.ShowView(_shareSheet, FinishedSharing);
		}
		#endregion
		
		private void FinishedSharing (eShareResult _result)
		{
			Log("Finished sharing");
			Log("Share Result = " + _result);
		}
		
		private void Log(string msg)
		{
			Debug.Log("[" + this.GetType() + "] " + msg);
		}
		
		void OnDestroy()
		{
			if(_shareButtons != null)
			{
				_shareButtons.Clear();
				_shareButtons = null;
			}
		}
		*/
	}
}