using UnityEngine;
using System.Collections;

/**
 * Globals
 * -------
 * 
 * George Tsappis
 * ScaryBeasties
 * July 2014
 * 
 * Description
 * ------------
 * A class to define static variables, 
 * to be accessed throughout the application
 * 
 */
using ScaryBeasties.Data;
using ScaryBeasties.User;
using ScaryBeasties.Games.Common.VO;
using System.Collections.Generic;

namespace ScaryBeasties
{
	public class SB_Globals : MonoBehaviour
	{
		// Unique application Id.
		// This is used like a namespace when saving profile data (player name, character etc.)
		// The APP_ID is prepended to the variable name used to save the data.
		// This is to avoid potential conflicts, if the user has other apps installed, 
		// which have already saved data using the same variable names
		public const string APP_ID = "CharlieLolaColouring_";
		public const string PHOTO_ALBUM_NAME = "CharlieLolaColouring";

		public const string ANALYTICS_APP_ID = "23";


		public const string CONFIG_XML_PATH = "/Resources/Localised/Data/config.xml";

		// Set this to true from MainApp, for the app to load assets from the dev location
		public static bool DEVELOPMENT_MODE = false;
		public static bool DEVELOPMENT_MODE_INDICATOR = false;
		
		//Shop constants
		public const string SHOP_ASSETS_PATH_DEV = "";// Location of dev assets, for testing prior to launch
		public static string SHOP_ASSETS_PATH = "";		// Location of live assets
		
		public const string SHOP_COPY_XML_FILE = "shopcopy.xml";
		public const string SHOP_DATA_XML_FILE_IOS = "shopdata-ios.xml";
		public const string SHOP_DATA_XML_FILE_GOOGLE = "shopdata-google.xml";
		public const string SHOP_DATA_XML_FILE_AMAZON = "shopdata-amazon.xml";
		public const string SHOP_DATA_XML_FILE_SAMSUNG = "shopdata-samsung.xml";
		
		public const string SHOP_ASSET_LOCAL_DIR = "assets"; // Folder on local filesystem (a sub folder of the persitent data path folder)
		public const string SHOP_IMAGES_LOCAL_DIR = "assets"; //"shop";// Folder on local filesystem, stores product images
		public const string SHOP_GOOGLE_KEY = "";

		// Scene names
		public const string SCENE_SPLASH = "Splash";
		public const string SCENE_INTRO = "Intro";
		public const string SCENE_TITLE = "Title";
		public const string SCENE_PROFILE_SELECTION = "ProfileSelection";
		public const string SCENE_PROFILE_NAMING = "ProfileNaming";
		public const string SCENE_PROFILE_PICTURE = "ProfilePicture";
		public const string SCENE_MAIN_MENU = "TitleMenu";
		public const string SCENE_EMPTY_LOADER = "EmptyLoader";

		public const string SCENE_GAME_FINISHED = "GameFinished";
		public const string SCENE_PARENTS_AREA = "ParentsArea";
		public const string SCENE_MORE_INFO = "MoreInfo";
		public const string SCENE_HELP = "Help";
		public const string SCENE_REDEEM_CODE = "RedeemCode";
		public const string SCENE_WEBVIEW_GENERIC = "WebViewGeneric";

		public const string SCENE_SHOP = "ShopScene_Menu";
		//public const string SCENE_SHOP = "ShopScene";
		public const string SCENE_SHOP_PRODUCT_DETAIL = "ShopScene_ProductDetail";
		
		public const string SCENE_COLOURING_MENU_PACKS = "ColouringMenuPacks";
		public const string SCENE_COLOURING_MENU_DRAWINGS = "ColouringMenuDrawings";
		public const string SCENE_COLOURING_MENU_MY_CREATIONS = "ColouringMenuMyCreations";
		public const string SCENE_COLOURING_MENU_INSPIRATION = "ColouringMenuInspiration";
		public const string SCENE_COLOURING_GAME = "JuniorColouringGame"; //"ColouringGame";
		//public const string SCENE_JUNIOR_COLOURING_GAME = "JuniorColouringGame";
		
		public const string MAIN_BUTTON_SOUND = "Audio/SFX/UI/SndButton";
		public const string MAIN_BUTTON_SOUND2 = "Audio/SFX/UI/SndButton2";
		public const string MAIN_BUTTON_SOUND3 = "Audio/SFX/UI/SndPlace";
		public const float MAIN_BUTTON_VOLUME = 1f;
		public const int MAIN_BUTTON_SOUND_CHANNEL = 7;
		
		// Maximum number of drawings a user can create
		public const int MAX_NUMBER_USER_DRAWINGS = 100;
		
		// The order of the game scenes, used in SB_GameFinishedScene class to determine
		// what the 'next game' button should link to
		public static readonly string[] GAME_ORDER = {SCENE_COLOURING_GAME};
			
		// Same as GAME_ORDER, but listing the id's 
		public static int[] GAME_ORDER_IDS = new int[]{0};

		//public static string[] TUTORIAL_IDS = new string[]{"CreateNewStory","AddBackground","AddBackground2","AddBackground3","AddCharacter","AddCharacter2","AddCharacter3","AddCharacter4","AddCharacter5","AddCharacter6","AddMore","PressTick","CreateNewPage","PickLayout","PickCell","Complete"};
		public static string[,] TUTORIAL_IDS = new string[16, 2]{
																	{"CreateNewStory", "NEW_STORY_BTN"},
																	{"AddBackground", "asset_menu_btn"},
																	{"AddBackground2", "ToolBacks"},
																	{"AddBackground3", ""},
																	{"AddCharacter", "asset_menu_btn"},
																	{"AddCharacter2", "ToolChars"},
																	{"AddCharacter3", ""},
																	{"AddCharacter4", ""},
																	{"AddCharacter5", ""},
																	{"AddCharacter6", ""},
																	{"AddMore", ""},
																	{"PressTick", ""},
																	{"CreateNewPage", "NEW_PAGE_BTN"},
																	{"PickLayout", ""},
																	{"PickCell", ""},
																	{"Complete", ""}
																};

		// Unlocking codes for content packs.
		// The first string in the 2d array is the pack id, 
		// the second value is the actual code.
		public static string[,] PACK_CODES = new string[2, 2] {
																{ "0", "ZOO" }, 
																{ "1", "TOWN" }
															  };
		
		public const int DEFAULT_LAYER = 0;
		public const int IGNORE_RAYCAST_LAYER = 2;

		// How many user profiles are there in the app?
		public const int NUM_PROFILES = 3;

		/**
		 * NOTE: Sorting layers are not the same as regular layers.  
		 * Sorting layers can be applied to renderer objects, using either:
		 * 		renderer.sortingLayerName (string)
		 * 	or..
		 * 		renderer.sortingLayerID (int)
		 * 
		 * The depth is set by adjusting 
		 * 		renderer.sortingOrder
		 */
		public const int SORTING_LAYER_DEFAULT = 0;
		public const int SORTING_LAYER_UI = 1;
		public const int SORTING_LAYER_POPUP = 2;
		
		public static SB_XML_Parser CONFIG_XML = new SB_XML_Parser ();


		public static string nextSceneName = "";
		public static KeyValueObject nextSceneKVObject = null;

		public static bool TEST_MODE {
			get {
				if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor) {
					return true;
				}

				return false;
			}
		}

		// CT the list of all avatar images that can be selected for the users profile.
		public static string[] AVATARS_CAROUSEL = {
			"profile_1",
			"profile_2",
			"profile_3",
			"profile_4",
			"profile_5",
			"profile_6",
			"profile_7",
			"profile_8"};
		
		public static string[] AVATARS = {
			"profile_1",
			"profile_2",
			"profile_3",
			"profile_4",
			"profile_5",
			"profile_6",
			"profile_7",
			"profile_8"};

		// Platform id's which are passed to external webview pages, to
		// determine what content to show
		public static SB_BuildPlatforms PLATFORM_ID = SB_BuildPlatforms.iOS;

		// BUILD_OPTIONS is set from SB_BuildSettings class, and stores the options for the current build platform (ios, Google Play, Amazon, Samsung)
		public static SB_BuildOptions BUILD_OPTIONS;

		//public static string LOCALE_STRING = "en";
		public static int LOCALE_ID = 0; // <--- This value will need to be saved to the user's device, using 'PlayerPrefs', and then read by the mainapp on startup, to set this value in SB_Globals.

		public const int LOCALE_ID_EN = 0;
		public const int LOCALE_ID_DE = 1;
		public const int LOCALE_ID_DK = 2;
		public const int LOCALE_ID_FR = 3;
		public const int LOCALE_ID_IT = 4;
		public const int LOCALE_ID_NO = 5;
		public const int LOCALE_ID_NL = 6;
		public const int LOCALE_ID_PL = 7;
		public const int LOCALE_ID_SE = 8;
		
		public static string LOCALE_STRING {
			get {
				string loc = "en";
				switch (LOCALE_ID) {
				case LOCALE_ID_DE:
					loc = "de";
					break;
				case LOCALE_ID_DK:
					loc = "dk";
					break;
				case LOCALE_ID_FR:
					loc = "fr";
					break;
				case LOCALE_ID_IT:
					loc = "it";
					break;
				case LOCALE_ID_NO:
					loc = "no";
					break;
				case LOCALE_ID_NL:
					loc = "nl";
					break;
				case LOCALE_ID_PL:
					loc = "pl";
					break;
				case LOCALE_ID_SE:
					loc = "se";
					break;
				}
				
				return loc;
			}
		}
	}
}
