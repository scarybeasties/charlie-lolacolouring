﻿using System;
using System.Collections;
using System.Collections.Generic;
using ScaryBeasties;
using UnityEngine;



public class SB_Analytics:MonoBehaviour
{
    private string ANALYTIC_PATH = "http://scarybserver.com/clients/bbc/analytics/sbstats_stathandler.php";

    private string ANALYTIC_APP_ID = "";
    private string ANALYTIC_APP_PLATFORM = "0";

	private string PREFS_INSTALL_KEY = "Installed";
  
    private List<SB_AnalyticEvent> eventQueue = new List<SB_AnalyticEvent>();
    private int MAX_QUEUE_LENGTH = 10;


    //timed events
    SB_AnalyticTimedEventController _timedEvents = new SB_AnalyticTimedEventController();

    private bool _analyticsInitialised = false;
    private bool _analyticsEnabled = false;


    private string _currentScene = "";

    //session data
    private DateTime _sessionStartTime;
    private DateTime _sessionEndTime;
    private int _sessionLength = 0;
    private int SESSION_TIMEOUT = 600;   //timeout in seconds between restarting sessions
   

	private static SB_Analytics _instance = null;
	public static SB_Analytics instance
	{
		get 
		{
			if(_instance == null)
			{
				GameObject go = new GameObject("AnalyticContainer");
				DontDestroyOnLoad(go);
				_instance = go.AddComponent<SB_Analytics>();
			}
			
			return _instance;
		}
	}

	public void Init(){

	}

    public void Setup(String analyticsAppId)
    {
		ANALYTIC_APP_ID = analyticsAppId;
        ANALYTIC_APP_PLATFORM = GetPlatform();
        if (ANALYTIC_APP_ID != "")
        {
            InitAnalytics();
			//PlayerPrefs.DeleteAll();
            if (NewInstall())
            {
                DeviceData();
            }
        }
    }


	private bool NewInstall(){

		if(PlayerPrefs.GetInt(PREFS_INSTALL_KEY)==0){
			PlayerPrefs.SetInt(PREFS_INSTALL_KEY,1);
			PlayerPrefs.Save();
			return true;
		}
		return false;
	}

    public void InitAnalytics(){

        ClearEventQueue();
        _analyticsInitialised = false;
        _analyticsEnabled = false;

        InitSession();

        SB_AnalyticEvent analyticEvent = new SB_AnalyticEvent(SB_AnalyticEvent.VALUE_EVENT_TYPE_INIT);
        StartCoroutine(SendInitEvent(analyticEvent));

    }



    public void DeviceData()
    {
        SB_AnalyticEvent analyticEvent = new SB_AnalyticEvent(SB_AnalyticEvent.VALUE_EVENT_TYPE_DEVICE, ANALYTIC_APP_ID, ANALYTIC_APP_PLATFORM);
        analyticEvent.AddEventParameter(SB_AnalyticEvent.KEY_DEVICE, SystemInfo.deviceModel);
        analyticEvent.AddEventParameter(SB_AnalyticEvent.KEY_LANGUAGE, Application.systemLanguage.ToString());
        AddAnalyticEvent(analyticEvent);
    }

    private void SessionLength()
    {
        SB_AnalyticEvent analyticEvent = new SB_AnalyticEvent(SB_AnalyticEvent.VALUE_EVENT_TYPE_SESSION_LENGTH, ANALYTIC_APP_ID, ANALYTIC_APP_PLATFORM);
        analyticEvent.AddEventParameter(SB_AnalyticEvent.KEY_SESSION_TIME, "" + _sessionLength);
        AddAnalyticEvent(analyticEvent);
    }

    private void SessionEvent(string sessionevent, int sessionTime = -1)
    {
        SB_AnalyticEvent analyticEvent = new SB_AnalyticEvent(SB_AnalyticEvent.VALUE_EVENT_TYPE_SESSION_EVENT, ANALYTIC_APP_ID, ANALYTIC_APP_PLATFORM);
        analyticEvent.AddEventParameter(SB_AnalyticEvent.KEY_SESSION_EVENT, sessionevent);
        if(sessionTime>=0){
            analyticEvent.AddEventParameter(SB_AnalyticEvent.KEY_SESSION_TIME, "" + sessionTime);
        }
        AddAnalyticEvent(analyticEvent);
    }

    public void PromoEvent(string promourl, bool isBanner = false)
    {
		promourl = WWW.UnEscapeURL(promourl);
        if (promourl.Length > 0)
        {
			string[] spliturl = promourl.Split('?');
			if(spliturl.Length>1){
				promourl = spliturl[spliturl.Length-1];
			}

            string promoid = "";
            Dictionary<string, string> dict = new Dictionary<string, string>();

			if(promourl.Contains("&")){
            	foreach (string item in promourl.Split('&'))
            	{
                	string[] parts = item.Replace("?", "").Split('=');
                	if (parts.Length>1)
                	{
                    	dict.Add(parts[0], parts[1]);
                	}
            	}
			}
			else{
				string[] parts = promourl.Split('=');
				if (parts.Length>1)
				{
					dict.Add(parts[0], parts[1]);
				}
			}

            if(dict.ContainsKey("p")){
                promoid = dict["p"];
            }
            else if (dict.ContainsKey("id"))
            {
                promoid = dict["id"];
            }

            if (promoid != "")
            {
                string bannerflag = "0";
                if(isBanner){
                    bannerflag = "1";
                }
                SB_AnalyticEvent analyticEvent = new SB_AnalyticEvent(SB_AnalyticEvent.VALUE_EVENT_TYPE_PROMO, ANALYTIC_APP_ID, ANALYTIC_APP_PLATFORM);
                analyticEvent.AddEventParameter(SB_AnalyticEvent.KEY_PROMO_ID, promoid);
                analyticEvent.AddEventParameter(SB_AnalyticEvent.KEY_PROMO_BANNER_FLAG, bannerflag);
                AddAnalyticEvent(analyticEvent);
            }

        }

    }

    public void SceneView(string sceneid){

        _currentScene = sceneid;

        SB_AnalyticEvent analyticEvent = new SB_AnalyticEvent(SB_AnalyticEvent.VALUE_EVENT_TYPE_SCENE_VIEW,ANALYTIC_APP_ID,ANALYTIC_APP_PLATFORM);
        analyticEvent.AddEventParameter(SB_AnalyticEvent.KEY_SCENE_ID, sceneid);
        AddAnalyticEvent(analyticEvent);

    }

    public void SceneEvent(string sceneevent)
    {

        SB_AnalyticEvent analyticEvent = new SB_AnalyticEvent(SB_AnalyticEvent.VALUE_EVENT_TYPE_SCENE_EVENT, ANALYTIC_APP_ID, ANALYTIC_APP_PLATFORM);
        analyticEvent.AddEventParameter(SB_AnalyticEvent.KEY_SCENE_ID, _currentScene);
        analyticEvent.AddEventParameter(SB_AnalyticEvent.KEY_SCENE_EVENT, sceneevent);
        AddAnalyticEvent(analyticEvent);

    }

    public void SceneEvent(string sceneid, string sceneevent)
    {

        SB_AnalyticEvent analyticEvent = new SB_AnalyticEvent(SB_AnalyticEvent.VALUE_EVENT_TYPE_SCENE_EVENT, ANALYTIC_APP_ID, ANALYTIC_APP_PLATFORM);
        analyticEvent.AddEventParameter(SB_AnalyticEvent.KEY_SCENE_ID, sceneid);
        analyticEvent.AddEventParameter(SB_AnalyticEvent.KEY_SCENE_EVENT, sceneevent);
        AddAnalyticEvent(analyticEvent);

    }

    public void GameEvent(string gameid, string gameevent)
    {

        SB_AnalyticEvent analyticEvent = new SB_AnalyticEvent(SB_AnalyticEvent.VALUE_EVENT_TYPE_GAME_EVENT, ANALYTIC_APP_ID, ANALYTIC_APP_PLATFORM);
        analyticEvent.AddEventParameter(SB_AnalyticEvent.KEY_GAME_ID, gameid);
        analyticEvent.AddEventParameter(SB_AnalyticEvent.KEY_GAME_EVENT, gameevent);
        AddAnalyticEvent(analyticEvent);

    }

    public void StartTimedEvent(string eventid){
        _timedEvents.StartTimedEvent(ANALYTIC_APP_ID, ANALYTIC_APP_PLATFORM,eventid);
    }

    public void CancelTimedEvent(string eventid)
    {
        _timedEvents.CancelTimedEvent(eventid);
    }

    public void CompleteTimedEvent(string eventid){
        SB_AnalyticTimedEvent tevt = _timedEvents.CompleteTimedEvent(eventid);
        if(tevt!=null){
            AddAnalyticEvent(tevt);
        }
    }

    private void AddAnalyticEvent(SB_AnalyticEvent analyticEvent){


        if(_analyticsInitialised && _analyticsEnabled){
            StartCoroutine(SendAnalyticEvent(analyticEvent));
            //StartCoroutine(SendAnalyticEventTesting(analyticEvent));  ///TESTING
        }
        else if(!_analyticsInitialised){
            eventQueue.Add(analyticEvent);
            while(eventQueue.Count>MAX_QUEUE_LENGTH){
                eventQueue.RemoveAt(0);
            }
        }

    }


    private void SendEventQueue(){
        
        while(eventQueue.Count>0){
            SB_AnalyticEvent evt = eventQueue[0];
            StartCoroutine(SendAnalyticEvent(evt));
            eventQueue.RemoveAt(0);
            evt = null;
        }
    }

    private void ClearEventQueue()
    {
        while (eventQueue.Count > 0)
        {
            SB_AnalyticEvent evt = eventQueue[0];
            eventQueue.RemoveAt(0);
            evt = null;
        }
    }

    private IEnumerator SendInitEvent(SB_AnalyticEvent analyticEvent)
    {

        bool connectionAccepted = false;  

        WWWForm form = new WWWForm();

        foreach (KeyValuePair<string, string> entry in analyticEvent.eventData)
        {
            form.AddField(entry.Key, entry.Value);
        }

		WWW www = new WWW(ANALYTIC_PATH, form);
        yield return www;
        if (www.error != null)
        {
            Debug.LogError("ERROR " + www.error);
        }
        else
        {
            int retVal = 0;
            if (int.TryParse(www.text, out retVal))
            {
                if(retVal>0){
                    connectionAccepted = true;
                    SESSION_TIMEOUT = retVal;
                }
            }

        }

        _analyticsInitialised = true;
        if(connectionAccepted){
            _analyticsEnabled = true;
            SendEventQueue();
        }
        else{
            ClearEventQueue();
        }

    }

    public IEnumerator SendAnalyticEvent(SB_AnalyticEvent analyticEvent)
    {
        WWWForm form = new WWWForm();

        foreach (KeyValuePair<string, string> entry in analyticEvent.eventData)
        {
            form.AddField(entry.Key, entry.Value);
        }

        WWW www = new WWW(ANALYTIC_PATH, form);

        yield return www;

        www.Dispose();

    }

    private IEnumerator SendAnalyticEventTesting(SB_AnalyticEvent analyticEvent)
    {
        
        WWWForm form = new WWWForm();

        foreach (KeyValuePair<string, string> entry in analyticEvent.eventData)
        {
            Debug.LogError(entry);
            form.AddField(entry.Key, entry.Value);
        }

        WWW www = new WWW(ANALYTIC_PATH, form);

        yield return www;

        if (www.error != null)
        {
            Debug.LogError("ERROR " + www.error);
        }
        else
        {
            Debug.LogError("TEST RESULT "+www.text);

        }

        www.Dispose();

    }


    private void InitSession(){
        _sessionLength = 0;
        _sessionStartTime = DateTime.Now;
        _sessionEndTime = DateTime.Now;
        SessionEvent(SB_AnalyticEvent.SESSION_START);
        _timedEvents.ClearEventQueue();
    }

    private void PauseSession(){
        
        _sessionEndTime = DateTime.Now;
        int timeSinceLastStart = GetTimeDifference(_sessionStartTime, _sessionEndTime);
        _sessionLength += timeSinceLastStart;
        SessionEvent(SB_AnalyticEvent.SESSION_PAUSE,timeSinceLastStart);
        _timedEvents.PauseEvents();
    }

    private void RestartSession(){
        
        int pauseTime = GetTimeDifference(_sessionEndTime, DateTime.Now);
        if(pauseTime>SESSION_TIMEOUT){
            SessionLength();
            InitAnalytics();
        }
        else{
            _sessionStartTime = DateTime.Now;
            _sessionEndTime = DateTime.Now;
            _timedEvents.ResumeEvents();
        }
        SessionEvent(SB_AnalyticEvent.SESSION_RESUME);
    }

    private void CompleteSession(){
        _timedEvents.ClearEventQueue();
        SessionEvent(SB_AnalyticEvent.SESSION_END);
        _sessionEndTime = DateTime.Now;
        _sessionLength += GetTimeDifference(_sessionStartTime, _sessionEndTime);
        SessionLength();
    }

    private int GetTimeDifference(DateTime start, DateTime end){
        TimeSpan ts = end.Subtract(start);
        return (int) ts.TotalSeconds;
    }




    private string GetPlatform()
    {

		switch (SB_Globals.PLATFORM_ID)
        {
		case SB_BuildPlatforms.iOS:
                return "0";

		case SB_BuildPlatforms.GooglePlay:
                return "1";

		case SB_BuildPlatforms.Amazon:
                return "2";

        }
        return "0";
    }

    private void OnApplicationPause(bool pause)
    {
        if(pause){
            PauseSession();
        }
        else{
            RestartSession();
        }
    }

    private void OnApplicationQuit()
    {
        CompleteSession();
    }
}

