﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SB_AnalyticTimedEvent : SB_AnalyticEvent
{
    public const string VALUE_EVENT_TYPE_TIMED = "tev";

    public const string KEY_EVENT_LENGTH = "eln";
    public const string KEY_TIMED_EVENT_ID = "tid";

    private string _eventId = "";

    private DateTime _eventStartTime;
    private DateTime _eventEndTime;
    private int _eventLength = 0;

    public SB_AnalyticTimedEvent(string evtid, string appid = "", string platform = "") : base(VALUE_EVENT_TYPE_TIMED, appid, platform)
    {
        _eventId = evtid;

        eventData.Add(KEY_TIMED_EVENT_ID, _eventId);

        _eventLength = 0;
        _eventStartTime = DateTime.Now; 

    }

    public string eventId{
        get{
            return _eventId;
        }
    }

    public int eventLength
    {
        get
        {
            return _eventLength;
        }
    }

    public void PauseEvent()
    {

        _eventEndTime = DateTime.Now;
        int timeSinceLastStart = GetTimeDifference(_eventStartTime, _eventEndTime);
        _eventLength += timeSinceLastStart;

    }

    public void ResumeEvent()
    {
        _eventStartTime = DateTime.Now;
        _eventEndTime = DateTime.Now; 
    }


    public void CompleteEvent(){
        PauseEvent();
        if(_eventLength>=0){
            eventData.Add(KEY_EVENT_LENGTH, _eventLength.ToString());
        }
    }


    private int GetTimeDifference(DateTime start, DateTime end)
    {
        TimeSpan ts = end.Subtract(start);
        return (int)ts.TotalSeconds;
    }
}
