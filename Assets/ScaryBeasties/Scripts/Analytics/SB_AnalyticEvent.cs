﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SB_AnalyticEvent {

    public const string KEY_EVENT_TYPE = "evt";
    public const string VALUE_EVENT_TYPE_INIT = "init";
    public const string VALUE_EVENT_TYPE_BTN_EVENT = "btn";
    public const string VALUE_EVENT_TYPE_GAME_EVENT = "game";
    public const string VALUE_EVENT_TYPE_SCENE_EVENT = "sevt";
    public const string VALUE_EVENT_TYPE_SCENE_VIEW = "scn";
    public const string VALUE_EVENT_TYPE_SESSION_LENGTH = "sess";
    public const string VALUE_EVENT_TYPE_SESSION_EVENT = "ssvt";
    public const string VALUE_EVENT_TYPE_DEVICE = "dev";
    public const string VALUE_EVENT_TYPE_PROMO = "prm";

    public const string KEY_APP_ID = "aid";
    public const string KEY_PLATFORM = "plt";
    public const string KEY_SCENE_ID = "sid";
    public const string KEY_GAME_ID = "gid";
    public const string KEY_SCENE_EVENT = "sevt";
    public const string KEY_GAME_EVENT = "gevt";
    public const string KEY_SESSION_TIME = "stm";
    public const string KEY_SESSION_EVENT = "ssvt";
    public const string KEY_DEVICE = "dev";
    public const string KEY_LANGUAGE = "lan";
    public const string KEY_PROMO_ID = "pid";
    public const string KEY_PROMO_BANNER_FLAG = "pbf";

    public const string SESSION_START = "SessionStart";
    public const string SESSION_PAUSE = "SessionPause";
    public const string SESSION_RESUME = "SessionResume";
    public const string SESSION_END = "SessionEnd";


    public Dictionary<string, string> eventData = new Dictionary<string, string>();

    public SB_AnalyticEvent(string evtType, string appid = "", string platform = ""){
        eventData.Add(KEY_EVENT_TYPE, evtType);
        if(appid!=""){
            eventData.Add(KEY_APP_ID, appid);
        }

        if (platform != "")
        {
            eventData.Add(KEY_PLATFORM, platform);
        }
    }



    public void AddEventParameter(string key, string value){
        eventData.Add(key,value);
    }

}
