﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SB_AnalyticEventList {

    //APP SPECIFIC EVENTS

    


    public const string MAIN_GAME = "ColouringGame";
	public const string COLOURING_TIME = "ColouringTime";

	public const string COLOURING_SNAPSHOT = "Snapshot";

    //GENERAL EVENTS

	public const string PROFILE_SCREEN = "ProfileSelect";
	public const string PROFILE_CREATE = "ProfileCreate";
	public const string PROFILE_DELETE = "ProfileDelete";

    public const string PAUSE_SCREEN = "PauseScreen";

    public const string MORE_GAMES_BTN = "MoreGamesBtn";
    public const string TERMS_BTN = "TermsBtn";
    public const string PRIVACY_BTN = "PrivacyBtn";
    public const string CONTACT_BTN = "ContactBtn";
    public const string PERMISSIONS_BTN = "PermissionBtn";
    public const string RESET_BTN = "ResetBtn";
    public const string RATE_BTN = "RateBtn";
    public const string INFO_BTN = "InfoBtn";
    public const string HELP_BTN = "HelpBtn";
    public const string PARENTS_BTN = "ParentBtn";
    public const string EXIT_BTN = "ExitBtn";
	public const string REDEEM_BTN = "RedeemCodeBtn";
	public const string CREDITS_BTN = "CreditsBtn";

	public const string CODE_UNLOCK = "CodeUnlock";

    public const string GAME_START = "GameStart";
    public const string GAME_END = "GameEnd";
	public const string GAME_INTRO = "GameIntro";
	public const string GAME_REPLAY = "GameReplay";
	public const string GAME_NEXT = "NextGame";
	public const string GAME_DIFFICULTY = "Difficulty";
}
