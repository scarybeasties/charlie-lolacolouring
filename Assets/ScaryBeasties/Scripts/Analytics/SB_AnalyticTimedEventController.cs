﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SB_AnalyticTimedEventController {

    private List<SB_AnalyticTimedEvent> _eventQueue = new List<SB_AnalyticTimedEvent>();
    private int MAX_QUEUE_LENGTH = 10;


    public void StartTimedEvent(string appid, string platform, string eventid){

        CancelTimedEvent(eventid);

        SB_AnalyticTimedEvent tevt = new SB_AnalyticTimedEvent(eventid, appid, platform);
        _eventQueue.Add(tevt);

        while(_eventQueue.Count>MAX_QUEUE_LENGTH){
            SB_AnalyticTimedEvent revt = _eventQueue[0];
            _eventQueue.RemoveAt(0);
            revt = null;
        }

    }


    public void CancelTimedEvent(string eventid){
        int index = FindTimedEvent(eventid);
        if (index >= 0)
        {
            SB_AnalyticTimedEvent tevt = _eventQueue[index];
            _eventQueue.RemoveAt(index);
            tevt = null;
        }

    }

    public SB_AnalyticTimedEvent CompleteTimedEvent(string eventid){

        int index = FindTimedEvent(eventid);
        if(index>=0){
            SB_AnalyticTimedEvent tevt = _eventQueue[index];
            tevt.CompleteEvent();
            _eventQueue.RemoveAt(index);
            return tevt;
        }

        return null;

    }

    public void PauseEvents(){
        for (int i = 0; i < _eventQueue.Count;i++){
            _eventQueue[i].PauseEvent();
        }
    }

    public void ResumeEvents()
    {
        for (int i = 0; i < _eventQueue.Count; i++)
        {
            _eventQueue[i].ResumeEvent();
        }
    }

    public void ClearEventQueue(){
        while(_eventQueue.Count>0){
            SB_AnalyticTimedEvent tevt = _eventQueue[0];
            _eventQueue.RemoveAt(0);
            tevt = null;
        }
    }


    public int FindTimedEvent(string eventId)
    {
        for (int i = 0; i < _eventQueue.Count;i++){
            if(_eventQueue[i].eventId==eventId){
                return i;
            }
        }
        return -1;
    }
}
