﻿using UnityEngine;
using System.Collections;
using System;

namespace ScaryBeasties.Cam
{
	public class SB_GameCamera : MonoBehaviour 
	{
		tk2dCamera _camera;
		public static float RATIO_3x2 = (float)Math.Round(2f / 3f, 2); // 0.66
		public static float RATIO_4x3 = (float)Math.Round(3f / 4f, 2); // 0.75
		public static float RATIO_16x9 = (float)Math.Round(9f / 16f, 2); // 0.5625
        public static float RATIO_195x9 = (float)Math.Round(9f / 19.5f, 2); // 0.5625

        public static float defaultZoom = 1.0f;

		// Note: 
		// Aspect ratios, almost 16:9 (0.5625)
		// iPhone 5 - 71:40 (0.5633)
		// iPhone 6 - 667:375 (0.5622)
		
		//private static BlurEffect _cameraBlur;

		void Awake () 
		{
			_camera = this.gameObject.GetComponent<tk2dCamera>();

			defaultZoom = _camera.ZoomFactor;

			/**
			 * Adjust the camera zoom slightly, depending on the aspect ratio.
			 * This ensures that as much of the game fits on screen as is possible.
			 * 
			 * Ratios are rounded to 2 decimal places, becaus iphone5/6 use odd aspect ratios, 
			 * which are almost (..but not quite) 16:9
			 * 
			 * when rounded to 2 decimal places, the result for 16:9, 71:40, 667:375 is 0.56
			 */
			if (Ratio <= RATIO_16x9) 
			{
				_camera.ZoomFactor = 0.8f;
			}
            if (Ratio <= RATIO_195x9)
            {
                _camera.ZoomFactor = 0.64f;
            }

            //BlurEffect blur = transform.GetComponent<BlurEffect>();
            //if(blur != null)
            //{
            //	_cameraBlur = blur;
            //	ApplyBlur(false);
            //}
        }
		
		public static void ApplyBlur(bool val)
		{
			//if(_cameraBlur != null)
			//{
			//	_cameraBlur.enabled = val;
			//}
		}

		public static float Ratio
		{
			get
			{
				float ratio = (float)Screen.height / (float)Screen.width;
				ratio = (float)Math.Round(ratio, 2);
				return ratio;
			}
		}
		/*
		public static float ZoomFactor {
			get {
				return _camera.ZoomFactor;
			}
		}*/

		void Start()
		{
			// Horrible hack to fix occasional anchoring issues with tk2d cameras..
			// The camera needs to be deactivated then reactivated, to ensure items 
			// anchored to the tk2d camera area correctly positioned

			_camera.gameObject.SetActive(false);
			_camera.gameObject.SetActive(true);
		}
	}
}