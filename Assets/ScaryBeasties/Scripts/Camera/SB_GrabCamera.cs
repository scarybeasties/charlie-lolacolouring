﻿using UnityEngine;
using System.Collections;

namespace ScaryBeasties.Cam
{
	public class SB_GrabCamera : MonoBehaviour 
	{
		private Texture2D _debugTexture = null;
		private Rect _rect;
		private CameraClearFlags _defaultCameraClearFlags;
		private Color _defaultCameraColour;
		
		// Used during debugging, so we can see the rectangle portion which is being grabbed
		public bool showBoundsPreview = false;
		
		void Start()
		{
			_defaultCameraClearFlags = this.GetComponent<Camera>().clearFlags;
			_defaultCameraColour = this.GetComponent<Camera>().backgroundColor;
						
			//_camera = transform.GetComponent<Camera>();
			this.GetComponent<Camera>().gameObject.SetActive(false);
		}

		void ApplyTransparency()
		{
			this.GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;
			this.GetComponent<Camera>().backgroundColor = Color.clear;
		}

		void Reset()
		{
			this.GetComponent<Camera>().clearFlags = _defaultCameraClearFlags;
			this.GetComponent<Camera>().backgroundColor = _defaultCameraColour;
			
			if(!showBoundsPreview)
			{
				this.GetComponent<Camera>().gameObject.SetActive(false);
			}
		}

		public Texture2D TakeScreenGrab(Rect rect, bool transparent=true)
		{
			this.GetComponent<Camera>().gameObject.SetActive(true);
			
			_rect = rect;
			if(showBoundsPreview) _debugTexture = CreateDebugTexture();
			
			// Initialize and render 
			RenderTexture rt = new RenderTexture(Screen.width, Screen.height, 32); 
			rt.format = RenderTextureFormat.ARGB32;

			if(transparent) ApplyTransparency();

			this.GetComponent<Camera>().targetTexture = rt; 
			this.GetComponent<Camera>().Render(); 
			RenderTexture.active = rt;

			Texture2D sshot = new Texture2D((int)rect.width, (int)rect.height, TextureFormat.ARGB32, true);	
			Debug.Log("[SB_GrabCamera] rect: " + rect);
			sshot.ReadPixels(rect, 0, 0);
			sshot.Apply();
			
			// Clean up 
			this.GetComponent<Camera>().targetTexture = null; 
			RenderTexture.active = null;
			// added to avoid errors 
			//DestroyImmediate(rt);

			/*
			_debugTexture = sshot;
			_rect.x = 0;
			_rect.y = 0;
			*/

			Reset();

			return sshot;
		}

		Texture2D CreateDebugTexture()
		{
			Color c = new Color(1.0f, 0.0f, 0.0f, 0.5f);

			Texture2D tex = new Texture2D(1, 1);
			tex.SetPixel(0, 0, c);
			tex.Apply();

			return tex;
		}

		void OnGUI()
		{
			if(_debugTexture == null) return;

			GUI.skin.box.normal.background = _debugTexture;
			GUI.Box(_rect, GUIContent.none);
		}

		void OnDestroy()
		{
			if(_debugTexture != null)
			{
				Destroy(_debugTexture);
				_debugTexture = null;
			}
		}
	}
}