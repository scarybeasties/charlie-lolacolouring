﻿using UnityEngine;
using System.Xml;
using System.Collections;
using ScaryBeasties;
using System;
using ScaryBeasties.Utils;

public class VersionInfoVO
{
	private string _currentVersion = "";
	private string _latestVersion = "";
	private string _date;
	private string _message;
	
	public VersionInfoVO(string currentVersion, string latestVersion, string date, string message="")
	{
		_currentVersion = currentVersion;
		_latestVersion = latestVersion;
		_date = date;
		_message = SB_Utils.SanitizeString(message);
	}
	
	// Getters
	public string CurrentVersion { get{return _currentVersion;}}
	public string LatestVersion { get{return _latestVersion;}}
	public string Date { get{return _date;}}
	public string Message { get{return _message;}}
	
	public string ToString()
	{
		string str = "CurrentVersion: " + CurrentVersion + ", LatestVersion: " + LatestVersion + ", Date: " + Date + ", Message: " + Message;
		return str;
	}
}

//public class VersionChecker : MonoBehaviour
public class VersionChecker
{
	const int VERSION_CHECK_NUM_DAYS = 7;
	const string VERSION_CHECK_EXPIRY_KEY = "VersionCheckExpiryDate";
	const string VERSION_CHECK_LAST_VERSION_PROMPTED_KEY = "VersionCheckLastVersionPrompted";
	
	public const string UPDATE_REQUIRED = "updateRequired";
	public const string UPDATE_NOT_REQUIRED = "updateNotRequired";
	
	public delegate void EventDelegate(VersionChecker versionChecker, string eventType, VersionInfoVO versionInfo=null);
	public event EventDelegate OnEvent;
	
	public bool UpdateRequired = false;
	public BundleVersionVO CurrentBundleVersionVO = null;
	public BundleVersionVO LatestBundleVersionVO = null;

	public bool USES_AUTHENTICATION = false;
	public string username = "";
	public string password = "";
	
	private string _updateMessage = "";
	
	private static VersionChecker _instance = null;
	public static VersionChecker instance
	{
		get 
		{
			if(_instance == null)
			{
				_instance = new VersionChecker();
			}
			
			return _instance;
		}
	}

	public void ParseBuildXML (XmlElement appVersionBuildXML)
	{
		string id 		= appVersionBuildXML.Attributes.GetNamedItem("id").Value;
		string bundleid = appVersionBuildXML.Attributes.GetNamedItem("bundleid").Value;
		string version 	= appVersionBuildXML.Attributes.GetNamedItem("version").Value;
		string date 	= appVersionBuildXML.Attributes.GetNamedItem("date").Value;
		
		//_updateMessage = "..update the app!!";
		_updateMessage = appVersionBuildXML.SelectSingleNode("message").FirstChild.Value;
		
		Debug.LogWarning("ParseBuildXML() id: " + id + ", bundleid: " + bundleid + ", version: " + version + ", date: " + date + ", _updateMessage: " + _updateMessage);
		
		LatestBundleVersionVO = new BundleVersionVO(version, date);
	}
	
	/**
	 * Check the current build version against the latest version from the XML
	 */ 
	public void CheckIfBuildUpToDate()
	{
		CurrentBundleVersionVO = new BundleVersionVO(CurrentBundleVersion.version);
		Debug.LogWarning("CurrentBundleVersion.version: " + CurrentBundleVersionVO.ToString());
		Debug.Log("CurrentBundleVersion.version: " + CurrentBundleVersionVO.ToString());
		
		#if UNITY_EDITOR
		/*
		// For testing the 'update' popup in the IDE
		DoUpdateRequired();
		return;
		*/
		#endif
		
		if(OkToCheckForLatestVersion())
		{
			if(LatestBundleVersionVO!=null){
				if(LatestBundleVersionVO.IsGreaterThan(CurrentBundleVersionVO))
				{
					DoUpdateRequired();
				}
				else
				{
					DoUpdateNotRequired();
				}
			}
		}
	}
	
	void DoUpdateRequired()
	{
		Debug.Log("------ NEW VERSION AVAILABLE!!!");
		
		UpdateRequired = true;
		VersionInfoVO versionInfo = new VersionInfoVO(CurrentBundleVersionVO.version, LatestBundleVersionVO.version, LatestBundleVersionVO.date, _updateMessage);
		
		// Store the version number which is being shown to the user
		PlayerPrefs.SetString(VERSION_CHECK_LAST_VERSION_PROMPTED_KEY, LatestBundleVersionVO.version);
		
		// Call the delgate
		if(OnEvent != null) OnEvent(this, UPDATE_REQUIRED, versionInfo);
	}
	
	void DoUpdateNotRequired()
	{
		Debug.Log("------ APPLICATION IS UP TO DATE");
		
		// Call the delgate
		if(OnEvent != null) OnEvent(this, UPDATE_NOT_REQUIRED);
	}
	
	/**
	 * Returns a bool.
	 * Perform a version update check once every so often (7 days..?)
	 * Or sooner, if a newer version is available than the one they were last prompted for
	 */ 
	bool OkToCheckForLatestVersion()
	{
		#if UNITY_EDITOR
		/*
		ResetExpiryKey();
		return true;
		*/
		#endif
	
	
		string lastUpdateVersionNumberPrompted = PlayerPrefs.GetString(VERSION_CHECK_LAST_VERSION_PROMPTED_KEY, CurrentBundleVersionVO.version);
		BundleVersionVO luvp = new BundleVersionVO(lastUpdateVersionNumberPrompted);
		
		if(LatestBundleVersionVO != null && LatestBundleVersionVO.IsGreaterThan(luvp))
		{
			// The latest version of the app (on the server) is greater than the version the last prompted update.
			// Return 'true', and go no further in this function
			Debug.LogError("OkToCheckForLatestVersion() LatestBundleVersionVO > last version prompted!");
			return true;
		}
		else
		{
			// The latest version of the app (on the server) is NOT greater than the version the last prompted update.
			// Check if we've gone past the expiry date
			Debug.LogError("OkToCheckForLatestVersion() LatestBundleVersionVO < last version prompted");
		}
		
		// Try and get expiry date from PlayerPrefs
		string expiryStr = PlayerPrefs.GetString(VERSION_CHECK_EXPIRY_KEY);
		Debug.LogError("OkToCheckForLatestVersion() expiryStr: " + expiryStr);
		
		DateTime now = DateTime.Now;
		
		// Set a default value, with an expiry date of yesterday
		DateTime expiry = now.AddDays(-1);
		if(expiryStr.Length > 0)
		{
			// Parse a saved expiry date
			expiry = DateTime.Parse(expiryStr);
		}
		
		Debug.LogError("NOW: " + now.ToString());
		Debug.LogError("expiry: " + expiry.ToString());
		
		if(now > expiry)
		{
			Debug.LogError("OkToCheckForLatestVersion() Expired!");
			
			#if UNITY_EDITOR
			// do nothing
			ResetExpiryKey(); // <-- uncomment when testing version checker in the IDE
			#else
			//////// Set new expiry date, of tomorrow
			////////expiry = now.AddDays(1);
			
			// Set new expiry date, of 7 days
			//expiry = now.AddDays(7);
			#endif			
			
			// Set new expiry date, of 7 days
			expiry = now.AddDays(VERSION_CHECK_NUM_DAYS);
			
			Debug.LogError("OkToCheckForLatestVersion() Setting new expiry date: " + expiry.ToString());
			PlayerPrefs.SetString(VERSION_CHECK_EXPIRY_KEY, expiry.ToString());
			
			return true;
		}
		
		return false;
	}
	
	public void ResetExpiryKey()
	{
		PlayerPrefs.DeleteKey(VERSION_CHECK_EXPIRY_KEY);
	}
}
