﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using ScaryBeasties.Utils;

namespace ScaryBeasties.Controls
{
	public class SB_ToggleButton : MonoBehaviour {

		public Action<bool> OnToggleChange;

		private bool _toggleState = false;
		private bool _enabled = true;

		private SB_TextField _labelTextField = null;
		private Text _labelText = null;
		private string _labelString = "";
		// Use this for initialization
		void Start () {
			// Look for a 'label' game object
			Transform lblTransform = transform.Find("label");
			if(lblTransform != null)
			{
				if(lblTransform.GetComponent<SB_TextField>() != null)
				{
					_labelTextField = lblTransform.GetComponent<SB_TextField>();
					_labelTextField.defaultText = _labelString;
					_labelTextField.text = _labelString;
				}
				else if(lblTransform.GetComponent<Text>() != null)
				{
					_labelText = lblTransform.GetComponent<Text>();
					if(_labelString.Length > 0)
					{
						_labelText.text = _labelString;
					}
				}
			}
			else
			{
				// do nothing, this button does not have a label
				//error ("Label not found on button " + this.gameObject.name);
			}
		}


		public bool enabled
		{
			get {return _enabled;}
			set {_enabled = value;}
		}

		public bool toggle
		{
			get {return _toggleState;}
			set {

				if (_toggleState != value) {
					_toggleState = value;
					//if(OnToggleChange != null) OnToggleChange(_toggleState);
				}
				
				if(OnToggleChange != null) OnToggleChange(_toggleState);
				SetToggleState(_toggleState);
			}
		}
		
		public void SetToggleState(bool enabled)
		{
			transform.Find("Toggle/On/Sprite").gameObject.SetActive(enabled);
			transform.Find("Toggle/Off/Sprite").gameObject.SetActive(!enabled);
		}

		public string label
		{
			set
			{
				_labelString = SB_Utils.SanitizeString(value);
				
				if(_labelTextField != null)
				{
					_labelTextField.defaultText = _labelString;
					_labelTextField.text = _labelString;
				}
				else if(_labelText != null)
				{
					_labelText.text = _labelString;
				}
			}
			
			get
			{
				return _labelString;
			}
		}
	
		// Update is called once per frame
		void Update () {
			if (_enabled) {
				if (UnityEngine.Input.GetMouseButtonUp(0)){
					Vector3 mpos = Camera.main.ScreenToWorldPoint(UnityEngine.Input.mousePosition);
					BoxCollider2D oncol = transform.Find("Toggle/On").GetComponent<BoxCollider2D>();
					BoxCollider2D offcol = transform.Find("Toggle/Off").GetComponent<BoxCollider2D>();
					if(oncol.OverlapPoint(new Vector2(mpos.x,mpos.y))){
						toggle=true;
					}
					else if(offcol.OverlapPoint(new Vector2(mpos.x,mpos.y))){
						toggle=false;
					}
				}
			}
		}

		protected virtual void OnDestroy()
		{

		}

	}
}
