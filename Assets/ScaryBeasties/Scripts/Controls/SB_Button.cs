﻿/**
 * Button class
 * -------------
 * 
 * George Tsappis
 * ScaryBeasties
 * July 2014
 * 
 * Description
 * ------------
 * A basic button class, which is linked to the SB_Button_Prefab object.
 * All instances of this prefab use a tag of name 'Button'.
 * 
 * Sprites for the Up/Hover/Clicked/Inactive states can be dragged on to 
 * the button instance, in the inspector panel.
 * 
 * Audio can be assigned to the Up/Hover/Clicked states, by entering a string
 * value on the button instance, in the inspector panel.
 * The value must match a path to audio inside the Assets/Resources folder.
 * 
 * Example:
 * 			A 'rollover_sound' audio file exists at this location:
 * 			Assets/Resources/Audio/SFX/rollover_sound.mp3
 * 			
 * 			To play the sound on rollover of the button instance, the 
 * 			'Hover Sound Id' string value would need to be set to
 * 			'Audio/SFX/rollover_sound' 
 * 			
 * 			(No file extension required!)
 * 
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using ScaryBeasties.Interfaces;
using ScaryBeasties.Controllers;
using ScaryBeasties.Base;
using ScaryBeasties.Sound;
using ScaryBeasties.Utils;
using UnityEngine.UI;

namespace ScaryBeasties.Controls
{
	public class SB_Button : SB_Base, SB_IAnimatedObject
	{
		public const string MOUSE_DOWN = "MouseDown";
		public const string MOUSE_UP = "MouseUp";
		public const string MOUSE_OVER = "MouseOver";
		public const string MOUSE_OUT = "MouseOut";
		public const string DRAG_START = "DragStart";
		public const string DRAGGING = "Dragging";
		public const string DRAG_FINISHED = "DragFinished";
		public const string CHANGE = "Change";

		// Animation clip names to use
		public const string ANIM_IDLE = "Idle";
		public const string ANIM_HOVER = "Hover";
		public const string ANIM_PRESS = "Press";
		public const string ANIM_RELEASE = "Release";
		
		// Setup delegates
		public delegate void MouseDownDelegate (SB_Button btn,string eventType);

		public delegate void MouseUpDelegate (SB_Button btn,string eventType);

		public delegate void MouseOverDelegate (SB_Button btn,string eventType);

		public delegate void MouseOutDelegate (SB_Button btn,string eventType);

		public delegate void DragStartDelegate (SB_Button btn,string eventType);

		public delegate void DraggingDelegate (SB_Button btn,string eventType);

		public delegate void DragFinishedDelegate (SB_Button btn,string eventType);

		public delegate void ChangeDelegate (SB_Button btn,string eventType);

		public event MouseDownDelegate OnPress;
		public event MouseUpDelegate OnRelease;
		public event MouseOverDelegate OnOver;
		public event MouseOutDelegate OnOut;
		public event MouseOutDelegate OnDragStart;
		public event MouseOutDelegate OnDragging;
		public event MouseOutDelegate OnDragFinished;
		public event ChangeDelegate OnChanged;
		
		// Sprites for button states
		public Sprite up;
		public Sprite hover;
		public Sprite clicked;
		public Sprite inactive;
		
		public string upSoundId = "";
		public string hoverSoundId = "";
		public string clickedSoundId = "";
		public bool useMusicChannel = false;
		public int soundChannel = -1;
		public object data = null;

		public bool hasRollover = true;
		public bool isDraggable = true;
		
		// Private properties
		private bool _isPressed = false;

		private bool _isActive = true;
		private bool _previousActiveState = true;

		private bool _selected = false;
		private Vector3 _startDragVector;
		private Vector3 _currentDragVector;
		private float _distance = 0f;
		private float _distanceDragged = 0f;
		private bool _dragging = false;
		private SpriteRenderer _spriteRenderer;
		private SB_TextField _labelTextField = null;
		private Text _labelText = null;
		private string _labelString = "";

		protected SB_AnimationController _animationController = null;
		protected List<string> _animationStates = new List<string> (new string[] {
			ANIM_IDLE,
			ANIM_HOVER,
			ANIM_PRESS,
			ANIM_RELEASE
		});

		// Use this for initialization
		override protected void Start ()
		{
			base.Start ();

			if (this.tag == "Untagged") {
				this.tag = "Button";
			}

			// Look for a 'label' game object
			Transform lblTransform = transform.Find ("label");
			if (lblTransform != null) {
				if (lblTransform.GetComponent<SB_TextField> () != null) {
					_labelTextField = lblTransform.GetComponent<SB_TextField> ();
					_labelTextField.defaultText = _labelString;
					_labelTextField.text = _labelString;
				} else if (lblTransform.GetComponent<Text> () != null) {
					_labelText = lblTransform.GetComponent<Text> ();
					if (_labelString.Length > 0) {
						_labelText.text = _labelString;
					}
				}
			} else {
				// do nothing, this button does not have a label
				//error ("Label not found on button " + this.gameObject.name);
			}

			Animator a = GetComponent<Animator> ();
			if (a == null) {
				a = transform.gameObject.AddComponent<Animator> ();
			}
			_animationController = new SB_AnimationController (a, _animationStates);
			//StartCoroutine(WaitThenInit());
		}


		public SB_AnimationController animationController {
			get { return _animationController; }
		}

		public List<string> animationStates {
			get { return _animationStates; }
			set { _animationStates = value; }
		}

		public string label {
			set {
				_labelString = SB_Utils.SanitizeString (value);

				if (_labelTextField != null) {
					_labelTextField.defaultText = _labelString;
					_labelTextField.text = _labelString;
				} else if (_labelText != null) {
					_labelText.text = _labelString;
				}
			}

			get {
				return _labelString;
			}
		}

		public GameObject LabelGameObject {
			get {
				if (transform.Find ("label").gameObject == null) {
					return transform.Find ("label").gameObject;
				}

				return transform.Find ("label").gameObject;
			}
		}

		/**
		 * Getter/Setter property, so enable/disable the button.
		 * Note: 
		 * 		This property was going to be called 'enabled', but that is already
		 * 		defined in the 'Behaviour' class, and cannot be overriden.
		 * 
		 * 		Setting the 'enabled' property to false will prevent the 'Update' 
		 * 		method from being triggered.
		 */
		public bool isActive {
			get { return _isActive; }
			set {

				_previousActiveState = _isActive;

				_isActive = value; 
				if (!_isActive) {
					// TODO: rethink how the 'inactive' sprite gets shown
					//		 might have to add another property to the button!
					//ShowSprite(inactive);

					ShowSprite (up);

					// Set the layer of the inactive button to '2' (Ignore Raycast)
					// This will prevent it from receiving inputs.
					this.gameObject.layer = SB_Globals.IGNORE_RAYCAST_LAYER;
				} else {
					this.gameObject.layer = SB_Globals.DEFAULT_LAYER;
					ShowSprite (up);
				}
				
				OnChange ();
			}
		}

		public bool PreviousActiveState {
			get{ return _previousActiveState; }
		}

		/**
		 * Getter/Setter
		 * Changes the button's selected state
		 */
		public bool Selected {
			get { return _selected; }

			set {
				_selected = value;
				if (_selected) {
					ShowSprite (clicked);
				} else if (_isPressed) {
					ShowSprite (up);
					_isPressed = false;
					_animationController.PlayAnimation (ANIM_RELEASE);
				}
			}
		}

		public bool IsDragging			{ get { return _dragging; } }

		public float DistanceDragged	{ get { return _distanceDragged; } }

		public Vector3 StartDragVector	{ get { return _startDragVector; } }

		public Vector3 CurrentDragVector{ get { return _currentDragVector; } }

		void Awake ()
		{
			ShowSprite (up);
		}

		void OnEnable ()
		{
			if (_animationController != null) {
				_animationController.PlayAnimation (ANIM_IDLE);
			}
		}

		public void SetIdle ()
		{
			_animationController.PlayAnimation (ANIM_IDLE);
		}

		protected virtual void ShowSprite (Sprite spr)
		{
			// Do nothing if the sprite is null
			if (spr == null)
				return;
			
			//print ("ShowSprite " + spr.name);
			_spriteRenderer = gameObject.GetComponent<SpriteRenderer> ();
			_spriteRenderer.sprite = spr;
		}

		private void PlaySound (string soundId)
		{
			if (soundId == "")
				return;
			
			if (useMusicChannel) {
				SB_SoundManager.instance.PlayMusic (soundId, 1.0f);
			} else {
				SB_SoundManager.instance.PlaySound (soundId, 1.0f, soundChannel);
			}
		}

		public void StopDrag ()
		{
			_dragging = false;
		}

		private void OnMouseDrag ()
		{
			if (isDraggable) {
				if (_dragging) {
					_currentDragVector = GetCurrentDragPoint (_distance);
					_distanceDragged = SB_Utils.GetDistance (_startDragVector, _currentDragVector);
					//print ("DRAGGING!!! _distance: " + _distance + ", dist: " + dist + ", _currentDragVector: " + _currentDragVector);

					// Call the delgate
					if (OnDragging != null)
						OnDragging (this, DRAGGING);
				}
			}
		}

		Vector3 GetCurrentDragPoint (float distance)
		{
			Ray ray = Camera.main.ScreenPointToRay (UnityEngine.Input.mousePosition);
			return ray.GetPoint (distance);
		}

		public void SetMouseDown ()
		{
			OnMouseDown ();
		}

		public void SetMouseUp ()
		{
			OnMouseUp ();
		}
		//public OnMouseUp { set{OnMouseUp();} }
		
		private void OnMouseDown ()
		{
			if (!isActive)
				return;

			if (!_selected) {
				ShowSprite (clicked);
				_animationController.PlayAnimation (ANIM_PRESS);
				PlaySound (clickedSoundId);
				_isPressed = true;
			}
			if (isDraggable) {
				if (!_dragging) {
					_distance = Vector3.Distance (transform.position, Camera.main.transform.position);
					_startDragVector = GetCurrentDragPoint (_distance);
					_dragging = true;

					// Call the delgate
					if (OnDragStart != null)
						OnDragStart (this, DRAG_START);
				}
			}

			// Call the delgate
			if (OnPress != null)
				OnPress (this, MOUSE_DOWN);
		}

		private void OnMouseUp ()
		{
			if (!isActive || !_isPressed)
				return;

			if (!_selected) {
				ShowSprite (hover);
				_animationController.PlayAnimation (ANIM_RELEASE);
				PlaySound (upSoundId);
				
				_isPressed = false;
			}

			if (isDraggable) {
				if (_dragging) {
					_dragging = false;

					// Call the delgate
					if (OnDragFinished != null)
						OnDragFinished (this, DRAG_FINISHED);
				}
			}

			// Call the delgate
			if (OnRelease != null)
				OnRelease (this, MOUSE_UP);
		}

		private void OnMouseEnter ()
		{

			if (!isActive)
				return;
			if (!hasRollover)
				return;

			if (!_selected) {
				ShowSprite (hover);
				if (_animationController != null)
					_animationController.PlayAnimation (ANIM_HOVER);
				PlaySound (hoverSoundId);
			}

			// Call the delgate
			if (OnOver != null)
				OnOver (this, MOUSE_OVER);
		}

		private void OnMouseExit ()
		{
			if (!isActive)
				return;
			if (!hasRollover)
				return;

			if (!_selected) {
				ShowSprite (up);
				if (_isPressed) {
					_animationController.PlayAnimation (ANIM_IDLE);
					_isPressed = false;
				} else {
					_animationController.PlayAnimation (ANIM_IDLE);
				}
			}
			// Call the delgate
			if (OnOut != null)
				OnOut (this, MOUSE_OUT);
		}

		/** Called whenever the button state has changed (example: activated/deactivated) */
		private void OnChange ()
		{
			// Call the delgate
			if (OnChanged != null)
				OnChanged (this, CHANGE);
		}

		/**
		 * INTERFACE IMPLEMENTATION
		 */
		public void	PlayAnimation (string stateName, int layerIndex = -1)
		{
			_animationController.PlayAnimation (stateName, layerIndex);
		}

		protected virtual void OnDestroy ()
		{
			up = null;
			hover = null;
			clicked = null;
			inactive = null;

			OnPress = null;
			OnRelease = null;
			OnOver = null;
			OnOut = null;
			OnChanged = null;

			if (_spriteRenderer != null) {
				Destroy (_spriteRenderer);
				_spriteRenderer = null;
			}

			if (_animationController)
				Destroy (_animationController);
			_animationController = null;

			_animationStates.Clear ();
			_animationStates = null;
		}
		
	}
}