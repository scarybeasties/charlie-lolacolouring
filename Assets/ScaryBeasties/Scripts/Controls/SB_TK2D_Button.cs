﻿/**
 * TK2D Button class
 * -------------
 * 
 * George Tsappis
 * ScaryBeasties
 * July 2014
 * 
 * Description
 * ------------
 * Extends SB_Button, and allows Toolkit2D sprites to be used.
 * 
 * A basic button class, which is linked to the SB_TK2D_Button_Prefab object.
 *
 * SB_TK2D_Button_Prefab instances have a TK2D sprite object, which requires a 
 * TK2D sprite collection to be set in the inspector, so that the correct material atlas
 * can be used on this button.
 * 
 * SB_TK2D_Button_Prefab instances feature the same properties as the basic SB_Button, which can be 
 * set in the inspector.
 * See SB_Button class for more details.
 */

using UnityEngine;
using System.Collections;

namespace ScaryBeasties.Controls
{
	public class SB_TK2D_Button : SB_Button
	{
		// Store reference to the TK2D Sprite component
		protected tk2dSprite _tk2dSprite;

		override protected void Start()
		{
			base.Start ();
			_tk2dSprite = GetComponent<tk2dSprite>();
		}

		/** 
		 * Override the ShowSprite method, 
		 * so we can swap the sprite on the _tk2dSprite object 
		 */
		override protected void ShowSprite(Sprite spr)
		{
			// Do nothing if the sprite is null
			if(spr == null || _tk2dSprite == null) return;

			// Switch the TK2D sprite to the name of the Up/Hover/Clicked/Inactive sprite set in the inspector
			_tk2dSprite.SetSprite(spr.name);
		}
		
		override protected void OnDestroy()
		{
			base.OnDestroy();
			_tk2dSprite = null;
		}
	}
}