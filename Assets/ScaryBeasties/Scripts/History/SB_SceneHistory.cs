﻿/**
 * Scene History
 * -------------
 * 
 * George Tsappis
 * ScaryBeasties
 * July 2014
 * 
 * Description
 * ------------
 * A singleton, which stores scene names in an array.
 * This allows us to implement 'back' button 
 * functionality accross several scenes.
 * 
 */ 

using UnityEngine;
using System.Collections;

namespace ScaryBeasties.History
{
	public class SB_SceneHistory : Object 
	{
		private static ArrayList _history = new ArrayList();

		// Store the name of the scene which was previously shown
		private static string _previousScene = "";

		private static SB_SceneHistory _instance = null;
		public static SB_SceneHistory instance
		{
			get 
			{
				if(_instance == null)
				{
					_instance = new SB_SceneHistory();
					DontDestroyOnLoad(_instance);
				}

				return _instance;
			}
		}
		
		/**
		 * Returns the number of elements in the _history array
		 */
		public int Length
		{
			get 
			{
				return _history.Count;
			}
		}
		
		/**
		 * Getter/Setter - the name of the scene which was previously shown
		 */
		public string PreviousScene
		{
			get 
			{
				return _previousScene;
			}

			set
			{
				_previousScene = value;
			}
		}

		/**
		 * Empty out the _history array 
		 */
		public void Reset()
		{
			Debug.LogError("---****---***---****---***---****---***---****---***");
			Debug.LogError("RESET HISTORY");
			Debug.LogError("---****---***---****---***---****---***---****---***");
			_history.Clear();
		}

		/**
		 * Add a scene name to the history array.
		 * First check that the last element in the array isn't already the same
		 * as the new value we are tying to store.
		 */
		public bool AddScene(string sceneName)
		{
			if(Length == 0 || (Length > 0 && sceneName != GetCurrentItem()))
			{
				if(!ContainsScene(sceneName))
				{
					_history.Add(sceneName);
				}
				else
				{
					// If the sceneName is already in the history, we need to remove
					// all the other items until we are back at the correct index
					while(GetCurrentItem() != sceneName)
					{
						RemoveLastItem();
					}
				}
					
				ToString();

				return true;
			}

			return false;
		}

		public string RemoveLastItem()
		{
			if(Length > 1)
			{
				var itemIndex = _history.Count-1;
				string itemValue = (string)_history[itemIndex];
				_history.RemoveAt(itemIndex);

				return itemValue;
			}

			return "";
		}

		/**
		 * Is the scene name already in the history?
		 */
		public bool ContainsScene(string sceneName)
		{
			foreach(string scene in _history)
			{
				if(scene == sceneName) return true;
			}

			return false;
		}

		public string GetPreviousItem(bool pop=false)
		{
			if(Length > 1)
			{
				var itemIndex = _history.Count-2;
				string itemValue = (string)_history[itemIndex];
				
				if(pop)
				{
					// Remove the very last item, as well as the one before it
					_history.RemoveAt(_history.Count-1);
					_history.RemoveAt(_history.Count-1);
				}
				
				return itemValue;
			}
			
			return "";
		}

		/**
		 * Returns the last item from the _history array.
		 * If array length is zero, an empty string is returned.
		 * 
		 * @bool pop If value passed is true, the element will be popped from the array
		 */
		public string GetCurrentItem(bool pop=false)
		{
			if(Length > 0)
			{
				var lastIndex = _history.Count-1;
				string lastItemValue = (string)_history[lastIndex];

				if(pop)
				{
					_history.RemoveAt(lastIndex);
				}

				return lastItemValue;
			}

			return "";
		}

		public string ToString()
		{
			var str = "";
			foreach(string scene in _history)
			{
				str += " > " + scene;
			}
			Debug.LogWarning ("------------------------------------------");
			Debug.LogWarning ("HISTORY: " + str);
			Debug.LogWarning ("------------------------------------------");

			return str;
		}
	}
}
