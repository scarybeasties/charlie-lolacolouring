﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using MiniJSON;

#if UNITY_IOS
using UnityEditor.iOS.Xcode;

public class iosBuildPostProcessor
{

    private const string BACKGROUND_MODE = "[BACKGROUND_MODE]";
    private const string GAME_CENTER = "[GAME_CENTER]";
    private const string IN_APP_PURCHASE = "[IN_APP_PURCHASE]";
    private const string PUSH_NOTIFICATIONS = "[PUSH_NOTIFICATIONS]";
    private const string ICLOUD = "[ICLOUD]";

    [PostProcessBuildAttribute(100)]
    public static void ChangeXcodePlist(BuildTarget buildTarget, string pathToBuiltProject)
    {

        if (buildTarget == BuildTarget.iOS)
        {

            // Get plist
            string plistPath = pathToBuiltProject + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));

            // Get root
            PlistElementDict rootDict = plist.root;

            //overwriting some plist settings.
            rootDict.SetBoolean("Requires Fullscreen", true);
            rootDict.SetBoolean("UIRequiresFullScreen", true);
            rootDict.SetBoolean("ITSAppUsesNonExemptEncryption", false);
            rootDict.SetString("NSPhotoLibraryUsageDescription", "The photo library is used to save screenshots of your creations in the app");
            rootDict.SetString("NSPhotoLibraryAddUsageDescription", "The photo library is used to save screenshots of your creations in the app");
            rootDict.SetString("NSCameraUsageDescription", "The camera is used to take a picture of yourself for your avatar");
            rootDict.SetString("NSMicrophoneUsageDescription", "The microphone is used for game input");

            //removing UIApplicationExitsOnSuspend as it is no longer supported by apple. Apps should not set whether or not they suspend or close the app when the app is put into the background
            rootDict.values.Remove("UIApplicationExitsOnSuspend");

            //add in any needed info.plist settings here.
            //You can append whole new entries like so:
            PlistElementDict dict = rootDict.CreateDict("NSAppTransportSecurity");
            PlistElementDict NSExceptionDomains = dict.CreateDict("NSExceptionDomains");
            PlistElementDict scarybeasties = NSExceptionDomains.CreateDict("scarybeasties.com");
            scarybeasties.SetBoolean("NSIncludesSubdomains", true);
            scarybeasties.SetBoolean("NSTemporaryExceptionAllowsInsecureHTTPLoads", true);
            scarybeasties.SetString("NSTemporaryExceptionMinimumTLSVersion", "TLSv1.2");
            scarybeasties.SetBoolean("NSThirdPartyExceptionAllowsInsecureHTTPLoads", false);
            scarybeasties.SetBoolean("NSThirdPartyExceptionRequiresForwardSecrecy", true);
            scarybeasties.SetString("NSThirdPartyExceptionMinimumTLSVersion", "TLSv1.2");
            scarybeasties.SetBoolean("NSRequiresCertificateTransparency", false);

            PlistElementDict scarybserver = NSExceptionDomains.CreateDict("scarybserver.com");
            scarybserver.SetBoolean("NSIncludesSubdomains", true);
            scarybserver.SetBoolean("NSTemporaryExceptionAllowsInsecureHTTPLoads", true);
            scarybserver.SetString("NSTemporaryExceptionMinimumTLSVersion", "TLSv1.1");


            //Next we need to modify the project settings, define all the things that need to be enabled or disabled per project.
            string projPath = System.IO.Path.Combine(pathToBuiltProject, "Unity-iPhone.xcodeproj/project.pbxproj");
            Debug.Log("pathToBuiltProject:" + pathToBuiltProject);

            PBXProject proj = new PBXProject();

            string text = File.ReadAllText(projPath);


            string oldCapabilities = "attributes = {\n\t\t\t\tTargetAttributes = {\n\t\t\t\t\t5623C57217FDCB0800090B9E = {\n\t\t\t\t\t\tTestTargetID = 1D6058900D05DD3D006BFB54;\n\t\t\t\t\t};\n\t\t\t\t};\n\t\t\t};";
            string newCapabilities = "attributes = {\n\t\t\t\tTargetAttributes = {\n\t\t\t\t\t1D6058900D05DD3D006BFB54 = {\n\t\t\t\t\t\tDevelopmentTeam = 3MNX4H5699;\n\t\t\t\t\t\tSystemCapabilities = {\n\t\t\t\t\t\t\tcom.apple.BackgroundModes = {\n\t\t\t\t\t\t\t\tenabled = " + BACKGROUND_MODE + ";\n\t\t\t\t\t\t\t};\n\t\t\t\t\t\t\tcom.apple.GameCenter = {\n\t\t\t\t\t\t\t\tenabled = " + GAME_CENTER + ";\n\t\t\t\t\t\t\t};\n\t\t\t\t\t\t\tcom.apple.InAppPurchase = {\n\t\t\t\t\t\t\t\tenabled = " + IN_APP_PURCHASE + ";\n\t\t\t\t\t\t\t};\n\t\t\t\t\t\t\tcom.apple.Push = {\n\t\t\t\t\t\t\t\tenabled = " + PUSH_NOTIFICATIONS + ";\n\t\t\t\t\t\t\t};\n\t\t\t\t\t\t\tcom.apple.iCloud = {\n\t\t\t\t\t\t\t\tenabled = " + ICLOUD + ";\n\t\t\t\t\t\t\t};\n\t\t\t\t\t\t};\n\t\t\t\t\t};\n\t\t\t\t\t5623C57217FDCB0800090B9E = {\n\t\t\t\t\t\tTestTargetID = 1D6058900D05DD3D006BFB54;\n\t\t\t\t\t};\n\t\t\t\t};\n\t\t\t};";

            newCapabilities = newCapabilities.Replace(BACKGROUND_MODE, EnvVar("BACKGROUND_MODE"));
            newCapabilities = newCapabilities.Replace(GAME_CENTER, EnvVar("GAME_CENTER"));
            newCapabilities = newCapabilities.Replace(IN_APP_PURCHASE, EnvVar("IN_APP_PURCHASE"));
            newCapabilities = newCapabilities.Replace(PUSH_NOTIFICATIONS, EnvVar("PUSH_NOTIFICATIONS"));
            newCapabilities = newCapabilities.Replace(ICLOUD, EnvVar("ICLOUD"));

            text = text.Replace(oldCapabilities, newCapabilities);


            proj.ReadFromString(text);
            string target = proj.TargetGuidByName("Unity-iPhone");

            //Turning of bitcode
            proj.SetBuildProperty(target, "ENABLE_BITCODE", "NO");

            //Settings architecture to standard
            //            proj.SetBuildProperty(target, "ARCHS", "$(ARCHS_STANDARD)");

            // Set a custom link flag
            proj.AddBuildProperty(target, "OTHER_LDFLAGS", "-ObjC");

            proj.AddFrameworkToProject(target, "StoreKit.framework", false);

            Debug.Log("before adding webkit");
            proj.AddFrameworkToProject(target, "WebKit.framework", false);
            Debug.Log("after adding webkit");
            proj.AddFrameworkToProject(target, "CoreData.framework", false);
            proj.AddFrameworkToProject(target, "Security.framework", false);
            proj.AddFrameworkToProject(target, "AssetsLibrary.framework", false);

            proj.AddFrameworkToProject(target, "Photos.framework", false);
            proj.AddFrameworkToProject(target, "libz.tbd", false);

            proj.RemoveFrameworkFromProject(target, "AdSupport.framework");
            proj.RemoveFrameworkFromProject(target, "libAdIdAccess.a");


            File.WriteAllText(projPath, proj.WriteToString());
            File.WriteAllText(plistPath, plist.WriteToString());


            string srcFilePath1 = Application.dataPath + "/Components/Application/Icon/iOS.jpg";
            string dstFilePath1 = Application.dataPath.Replace("Assets", "") + "Build/iOS/Unity-iPhone/Images.xcassets/AppIcon.appiconset/Icon-1024.jpg";
            if (File.Exists(dstFilePath1))
                File.Delete(dstFilePath1);

            if (File.Exists(srcFilePath1))
            {
                File.Copy(srcFilePath1, dstFilePath1);

                Dictionary<string, object> contents = Json.Deserialize(File.ReadAllText(Application.dataPath.Replace("Assets", "") + "Build/iOS/Unity-iPhone/Images.xcassets/AppIcon.appiconset/Contents.json")) as Dictionary<string, object>;
                List<object> images = contents["images"] as List<object>;
                Dictionary<string, string> entry = new Dictionary<string, string>();
                entry["idiom"] = "ios-marketing";
                entry["size"] = "1024x1024";
                entry["scale"] = "1x";
                entry["filename"] = "Icon-1024.jpg";

                images.Add(entry);
                contents["images"] = images;
                File.WriteAllText(Application.dataPath.Replace("Assets", "") + "Build/iOS/Unity-iPhone/Images.xcassets/AppIcon.appiconset/Contents.json", Json.Serialize(contents));
            }

            string pathToNonIDFADeviceSettings = Path.Combine(Application.dataPath, "Editor/IDFA/DeviceSettings.mm");
            if (File.Exists(pathToNonIDFADeviceSettings))
            {
                string pathToReplaceDeviceSettings = Application.dataPath.Replace("Assets", "") + "Build/iOS/Classes/Unity/DeviceSettings.mm";

                File.WriteAllText(pathToReplaceDeviceSettings, File.ReadAllText(pathToNonIDFADeviceSettings));
            }
        }
    }



    public static string EnvVar(string key)
    {
        string s = System.Environment.GetEnvironmentVariable(key);
        if (s == null)
        {
            s = "0";
        }
        return s;
    }
}
#endif