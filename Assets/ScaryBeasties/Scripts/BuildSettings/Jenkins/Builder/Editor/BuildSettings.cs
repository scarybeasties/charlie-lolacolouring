﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine.UI;
using System.IO;
using ScaryBeasties.BuildSettings.Builder;

public class BuildSettings
{

    public static int BUILD_NUMBER;

    private static BuildSettingsData _settings;
    static string[] GetBuildScenes()
    {
        List<string> names = new List<string>();

        foreach (EditorBuildSettingsScene e in EditorBuildSettings.scenes)
        {
            if (e == null)
                continue;

            if (e.enabled)
                names.Add(e.path);
        }
        return names.ToArray();
    }

    //Used for build systems like Jenkins, to get variables passed in from CLI.
    public static string EnvVar(string key)
    {
        return System.Environment.GetEnvironmentVariable(key);
    }

    private static void SetAndroidBuildNumber()
    {
#if UNITY_4
		PlayerSettings.Android.bundleVersionCode = int.Parse(EnvVar("BUILD_NUMBER"));
		PlayerSettings.bundleVersion = PlayerSettings.shortBundleVersion;
#else
        PlayerSettings.Android.bundleVersionCode = int.Parse(EnvVar("BUILD_NUMBER"));
#endif
    }

    private static void SetIOSBuildNumber()
    {
#if UNITY_4
		PlayerSettings.Android.bundleVersionCode = int.Parse(EnvVar("BUILD_NUMBER"));
		PlayerSettings.bundleVersion = PlayerSettings.shortBundleVersion + "b" +PlayerSettings.Android.bundleVersionCode;
#else
        PlayerSettings.iOS.buildNumber = EnvVar("BUILD_NUMBER");
#endif
    }

    private static void SetBuildNumber()
    {
        GameObject dummy = new GameObject();
        dummy.AddComponent<Text>();
        BuildEnvSettings build_number_instance = dummy.AddComponent<BuildEnvSettings>();
        MonoScript script = MonoScript.FromMonoBehaviour(build_number_instance);
        string buildNumberClassPath = AssetDatabase.GetAssetPath(script);

        //rewrite it with the correct nuild number
        string source = System.IO.File.ReadAllText(buildNumberClassPath);
        source = source.Replace(BuildEnvSettings.BUILD_NUMBER, EnvVar("BUILD_NUMBER"));
        source = source.Replace(BuildEnvSettings.SPLIT_APK, EnvVar("SPLIT_APK"));
        System.IO.File.WriteAllText(buildNumberClassPath, source);
    }

    private static void SetBuildSettings()
    {
        _settings = Resources.Load<BuildSettingsData>("BuildSettings");
        string define = "";

        if (_settings != null)
        {
            if (_settings.UsesTracking)
            {
                define += "USES_TRACKING;";
            }
            if (_settings.UsesShop)
            {
                define += "USES_STORE;";
            }
            if (_settings.UsesCamera)
            {
                define += "USES_CAMERA;";
            }
            define += _settings.CustomDefines;

        }
        else
        {
            Debug.LogError("Resources/BuildSettings.asset failed to load. Create new BuildSettings > Settings scriptableObject");
        }

        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, define);
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, define);
    }

    static void CommandLineBuild()
    {
        string platform = EnvVar("PLATFORM");
        BuildOptions options = EnvVar("DEVELOPMENT") == "true" ? BuildOptions.Development : BuildOptions.None;
        if (platform == "iOS")
        {
            BuildSettings.BuildIOS(options);
        }
        else if (platform == "Google")
        {
            BuildSettings.BuildAndroid(SB_BuildPlatforms.GooglePlay, options);
        }
        else if (platform == "Amazon")
        {
            BuildSettings.BuildAndroid(SB_BuildPlatforms.Amazon, options);
        }
    }

    static void BuildAssetBundles(string platform)
    {
        if (System.IO.Directory.Exists(Path.Combine(Application.streamingAssetsPath, "AssetBundles/")))
            System.IO.Directory.Delete(Path.Combine(Application.streamingAssetsPath, "AssetBundles/"), true);

        string outputPath = Path.Combine(Application.streamingAssetsPath, "AssetBundles/" + platform);
        if (!System.IO.Directory.Exists(outputPath))
        {
            System.IO.Directory.CreateDirectory(outputPath);
        }

        var options = BuildAssetBundleOptions.ChunkBasedCompression;

        bool shouldCheckODR = EditorUserBuildSettings.activeBuildTarget == BuildTarget.iOS;
#if UNITY_TVOS
            shouldCheckODR |= EditorUserBuildSettings.activeBuildTarget == BuildTarget.tvOS;
#endif


        //@TODO: use append hash... (Make sure pipeline works correctly with it.)
        BuildPipeline.BuildAssetBundles(outputPath, options, EditorUserBuildSettings.activeBuildTarget);

    }

    static void BuildIOS(BuildOptions option)
    {
        CreateStreamingAssetsManifest();

        BuildSettings.SetIOSBuildNumber();
        BuildSettings.SetBuildNumber();

#if UNITY_5_6_OR_NEWER
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.iOS, EnvVar("BUNDLE_ID"));
#else
            PlayerSettings.bundleIdentifier = EnvVar("BUNDLE_ID");
#endif


        string[] scenes = GetBuildScenes();
        string path = "Build/iOS/";

        if (scenes == null || scenes.Length == 0 || path == null)
            return;

        Debug.Log("PATH:: " + path);

        if (!System.IO.Directory.Exists(path))
        {
            System.IO.Directory.CreateDirectory(path);
        }

        SetBuildSettings();
        string define = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS);
        define += ";SB_PLATFORM_IOS";

        if (EnvVar("DEMO_MODE") == "true")
        {
            define += ";DEMO_MODE";
        }

        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, define);

        BuildAssetBundles("iOS");

        BuildPipeline.BuildPlayer(scenes, path, BuildTarget.iOS, option);
    }

    [MenuItem("ScaryBeasties/Build/Build IOS Editor (Development)")]
    static void BuildIOSEditorDevelopment()
    {
        BuildIOs(false);
    }

    //[MenuItem("ScaryBeasties/SetNDKPath")]
    static void SetNDKPath()
    {
        string path = "/Users/Shared/Jenkins/Desktop/android-ndk-r13b";
        if (Directory.Exists(path))
        {
            EditorPrefs.SetString("AndroidNdkRoot", path);
        }
        else
        {
            Debug.LogError("NdkPath doesn't exist:" + path + ". This is needed for the build computer only. If this is a local build, set the NDK in Unity >Preferences >External Tools instead");
        }
    }

    [MenuItem("ScaryBeasties/Build/Build IOS Editor (Release)")]
    static void BuildIOSEditorRelease()
    {
        BuildIOs(true);
    }

    static private void BuildIOs(bool release)
    {
        CreateStreamingAssetsManifest();

        string[] scenes = GetBuildScenes();
        string path = "Build/iOS";

        Debug.Log("PATH:: " + path);

        if (!System.IO.Directory.Exists(path))
        {
            System.IO.Directory.CreateDirectory(path);
        }

        if (scenes == null || scenes.Length == 0 || path == null)
            return;

        SetBuildSettings();
        string define = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS);
        define += ";SB_PLATFORM_IOS";
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, define);

        BuildAssetBundles("iOS");

        if (release)
            BuildPipeline.BuildPlayer(scenes, path, BuildTarget.iOS, BuildOptions.None);
        else
            BuildPipeline.BuildPlayer(scenes, path, BuildTarget.iOS, BuildOptions.Development);
    }


    [MenuItem("ScaryBeasties/Build/Build Google Editor (Development)")]
    static void BuildAndroidEditorDev()
    {
        BuildAndroidEditor(false);
    }

    [MenuItem("ScaryBeasties/Build/Build Google Editor (Release)")]
    static void BuildAndroidEditorRelease()
    {
        BuildAndroidEditor(true);
    }


    static void BuildAndroidEditor(bool release)
    {
        string[] scenes = GetBuildScenes();
        string dir = "build/" + "Google" + "/";
        string path = dir + "AndroidBuild" + "-" + "###" + ".apk";

        if (scenes == null || scenes.Length == 0 || path == null)
            return;

        if (!System.IO.Directory.Exists(dir))
        {
            System.IO.Directory.CreateDirectory(dir);
        }
        SetBuildSettings();
        string define = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android);
        define += ";SB_PLATFORM_GOOGLE_PLAY";

        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, define);

        PlayerSettings.Android.keystoreName = _settings.AndroidKeystorePath;//ANDROID_KEYSTORE_PATH;
        PlayerSettings.Android.keystorePass = _settings.AndroidKeystorePassword;//ANDROID_KEYSTORE_PASS;
        PlayerSettings.Android.keyaliasName = _settings.AndroidKeyAlias;//ANDROID_KEYALIAS;
        PlayerSettings.Android.keyaliasPass = _settings.AndroidKeyAliasPassword;//ANDROID_KEYALIAS_PASS;

        BuildAssetBundles("Android");
        ConfigureAndroidBuildSystem();


        if (release)
            BuildPipeline.BuildPlayer(scenes, path, BuildTarget.Android, BuildOptions.None);
        else
            BuildPipeline.BuildPlayer(scenes, path, BuildTarget.Android, BuildOptions.Development);


    }

    static void ConfigureAndroidBuildSystem()
    {
        switch (_settings.androidBuildSystem)
        {
            case AndroidBuildSystemName.Gradle:
                EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;
                break;

            case AndroidBuildSystemName.Internal:
                EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Internal;
                break;

            default:
                Debug.Log("androidBuildSystem not configured in BuildSettings object");
                break;

        }
        Debug.Log("setting androidBuildSystem:" + EditorUserBuildSettings.androidBuildSystem);
    }

    [MenuItem("ScaryBeasties/Build/Build Amazon Editor (Development)")]
    static void BuildAndroidAmazonEditorDev()
    {
        BuildAndroidAmazonEditor(false);
    }

    [MenuItem("ScaryBeasties/Build/Build Amazon Editor (Release)")]
    static void BuildAndroidAmazonEditorRelease()
    {
        BuildAndroidAmazonEditor(true);
    }

    static void BuildAndroidAmazonEditor(bool release)
    {
        string[] scenes = GetBuildScenes();
        string dir = "build/" + "Amazon" + "/";
        string path = dir + "AndroidBuild" + "-" + "###" + ".apk";

        if (scenes == null || scenes.Length == 0 || path == null)
            return;

        if (!System.IO.Directory.Exists(dir))
        {
            System.IO.Directory.CreateDirectory(dir);
        }

        SetBuildSettings();
        string define = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android);
        define += ";SB_PLATFORM_AMAZON";

        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, define);


        PlayerSettings.Android.keystoreName = _settings.AndroidKeystorePath;//ANDROID_KEYSTORE_PATH;
        PlayerSettings.Android.keystorePass = _settings.AndroidKeystorePassword;//ANDROID_KEYSTORE_PASS;
        PlayerSettings.Android.keyaliasName = _settings.AndroidKeyAlias;//ANDROID_KEYALIAS;
        PlayerSettings.Android.keyaliasPass = _settings.AndroidKeyAliasPassword;//ANDROID_KEYALIAS_PASS;

        ConfigureAndroidBuildSystem();

        if(release)
            BuildPipeline.BuildPlayer(scenes, path, BuildTarget.Android, BuildOptions.None);
        else
            BuildPipeline.BuildPlayer(scenes, path, BuildTarget.Android, BuildOptions.Development);
        
    }

    static string StreamingAssetsManifest;

    static void CreateStreamingAssetsManifest()
    {
        StreamingAssetsManifest = "";

        var src = System.IO.Path.Combine(Application.streamingAssetsPath, "WebViews/ParentsAreaPromo/");
        if (Directory.Exists(src))
        {
            RecursiveAddFiles(src);

            src = System.IO.Path.Combine(Application.streamingAssetsPath, "WebViews/ParentsArea/");

            RecursiveAddFiles(src);

            System.IO.File.WriteAllText(Application.dataPath + "/Resources/Manifest.txt", StreamingAssetsManifest);
            AssetDatabase.ImportAsset(Application.dataPath + "/Resources/Manifest.txt");
            AssetDatabase.Refresh();
        }
    }

    static void RecursiveAddFiles(string sourceDirName)
    {
        DirectoryInfo dir = new DirectoryInfo(sourceDirName);

        if (!dir.Exists)
        {
            throw new DirectoryNotFoundException(
                "Source directory does not exist or could not be found: "
                + sourceDirName);
        }

        DirectoryInfo[] dirs = dir.GetDirectories();

        FileInfo[] files = dir.GetFiles();
        foreach (FileInfo file in files)
        {
            string t = Path.Combine(sourceDirName, file.Name) + "\n";
            t = t.Replace(Application.streamingAssetsPath, "");
            if (!t.Contains(".meta") && !t.Contains(".DS"))
            {
                StreamingAssetsManifest += t;
            }
        }

        foreach (DirectoryInfo subdir in dirs)
        {
            string temppath = Path.Combine(sourceDirName, subdir.Name);
            RecursiveAddFiles(temppath);
        }
    }

    static void BuildAndroid(SB_BuildPlatforms platform, BuildOptions option)
    {
        //Switch to android build target if not
        if (EditorUserBuildSettings.activeBuildTarget != BuildTarget.Android)
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);

        CreateStreamingAssetsManifest();

        BuildSettings.SetAndroidBuildNumber();
        BuildSettings.SetBuildNumber();
        EditorPrefs.SetString("AndroidSdkRoot", "/Users/Shared/Jenkins/Desktop/AndroidSDK");
        EditorPrefs.SetString("JdkPath", "/Library/Java/JavaVirtualMachines/jdk1.8.0_112.jdk/Contents/Home");
        SetNDKPath();

#if UNITY_5_6_OR_NEWER
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, EnvVar("BUNDLE_ID"));
#else
            PlayerSettings.bundleIdentifier = EnvVar("BUNDLE_ID");
#endif

        string[] scenes = GetBuildScenes();
        string dir = "build/" + EnvVar("PLATFORM") + "/";
        string path = dir + EnvVar("JOB_NAME") + "-" + EnvVar("BUILD_NUMBER") + ".apk";

        if (scenes == null || scenes.Length == 0 || path == null)
            return;

        if (!System.IO.Directory.Exists(dir))
        {
            System.IO.Directory.CreateDirectory(dir);
        }

        SetBuildSettings();
        ConfigureAndroidBuildSystem();

        string define = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android);
        switch (platform)
        {
            case SB_BuildPlatforms.GooglePlay:
                define += ";SB_PLATFORM_GOOGLE_PLAY";
                break;
            case SB_BuildPlatforms.Amazon:
                define += ";SB_PLATFORM_AMAZON";
                break;
            case SB_BuildPlatforms.Samsung:
                define += ";SB_PLATFORM_SAMSUNG";
                break;
            default:
                throw new System.ArgumentOutOfRangeException();
        }
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, define);

        PlayerSettings.Android.useAPKExpansionFiles = (EnvVar("SPLIT_APK") == "true");
        Debug.Log("Should this use expansion files? " + (EnvVar("SPLIT_APK")));

        PlayerSettings.Android.keystoreName = _settings.AndroidKeystorePath;//ANDROID_KEYSTORE_PATH;
        PlayerSettings.Android.keystorePass = _settings.AndroidKeystorePassword;//ANDROID_KEYSTORE_PASS;
        PlayerSettings.Android.keyaliasName = _settings.AndroidKeyAlias;//ANDROID_KEYALIAS;
        PlayerSettings.Android.keyaliasPass = _settings.AndroidKeyAliasPassword;//ANDROID_KEYALIAS_PASS;

        BuildAssetBundles("Android");
        BuildPipeline.BuildPlayer(scenes, path, BuildTarget.Android, option);
    }
}
