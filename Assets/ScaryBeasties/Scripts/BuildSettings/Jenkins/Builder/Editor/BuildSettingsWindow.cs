﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace ScaryBeasties.BuildSettings.Builder
{
    public class BuildSettingsWindow : EditorWindow
    {
        private static BuildSettingsWindow _window;
        private BuildSettingsData _settings;

        [MenuItem("ScaryBeasties/Build/Settings")]
        static void OpenBuildSettings()
        {
            _window = (BuildSettingsWindow)EditorWindow.GetWindow(typeof(BuildSettingsWindow));
            _window.minSize = _window.maxSize = new Vector2(300, 280);
            _window.titleContent = new GUIContent("Build Settings");
        }

        void OnGUI()
        {
            if (_settings == null)
            {
                _settings = Resources.Load<BuildSettingsData>("BuildSettings");
            }
            else
            {
                GUILayout.BeginVertical(GUI.skin.FindStyle("Box"));

                GUILayout.BeginHorizontal();
                GUILayout.Label("Uses Tracking");
                GUILayout.FlexibleSpace();
                _settings.UsesTracking = GUILayout.Toggle(_settings.UsesTracking, "");
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.Label("Uses Shop");
                GUILayout.FlexibleSpace();
                _settings.UsesShop = GUILayout.Toggle(_settings.UsesShop, "");
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.Label("Uses Camera (Legacy Apps)");
                GUILayout.FlexibleSpace();
                _settings.UsesCamera = GUILayout.Toggle(_settings.UsesCamera, "");
                GUILayout.EndHorizontal();


                GUILayout.Label("Custom Defines");
                _settings.CustomDefines = GUILayout.TextField(_settings.CustomDefines);


                GUILayout.BeginHorizontal();
                GUILayout.Label("Keystore Path");
                if (GUILayout.Button("Select Keystore path"))
                {
                    string path = Application.dataPath.Replace(System.IO.Path.DirectorySeparatorChar + "Assets", "") + System.IO.Path.DirectorySeparatorChar;
                    Debug.Log(path);
                    _settings.AndroidKeystorePath = EditorUtility.OpenFilePanel("Select Android Keystore", "", "").Replace(path, "");
                }
                GUILayout.EndHorizontal();

                GUILayout.Label("Keystore Password");
                _settings.AndroidKeystorePassword = GUILayout.TextField(_settings.AndroidKeystorePassword);

                GUILayout.Label("Key Alias");
                _settings.AndroidKeyAlias = GUILayout.TextField(_settings.AndroidKeyAlias);

                GUILayout.Label("Key Alias Password");
                _settings.AndroidKeyAliasPassword = GUILayout.TextField(_settings.AndroidKeyAliasPassword);


                GUILayout.Label("Android Build System");
                int selected = (int)_settings.androidBuildSystem;
                selected = EditorGUILayout.Popup("Label", selected, new string[] { AndroidBuildSystemName.Gradle.ToString(),  AndroidBuildSystemName.Internal.ToString()}); 
                _settings.androidBuildSystem = (AndroidBuildSystemName)selected;
                GUILayout.EndVertical();

                string define = "";
                if (_settings.UsesTracking)
                {
                    define += "USES_TRACKING;";
                }
                if (_settings.UsesShop)
                {
                    define += "USES_STORE;";
                }
                if (_settings.UsesCamera)
                {
                    define += "USES_CAMERA;";
                }
                define += _settings.CustomDefines;
                PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, define);
                PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, define);
            }
        }
    }
}