﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScaryBeasties.BuildSettings.Builder
{
    public enum AndroidBuildSystemName
    {
        Gradle,
        Internal
    }

    [CreateAssetMenu(menuName = "BuildSettings/Settings", order = 1)]
    public class BuildSettingsData : ScriptableObject
    {

        public bool UsesTracking = true;
        public bool UsesCamera;
        public bool UsesShop;
        public string CustomDefines;

        public string AndroidKeystorePath;
        public string AndroidKeystorePassword;
        public string AndroidKeyAlias;
        public string AndroidKeyAliasPassword;
        public AndroidBuildSystemName androidBuildSystem = AndroidBuildSystemName.Gradle;
    }
}
