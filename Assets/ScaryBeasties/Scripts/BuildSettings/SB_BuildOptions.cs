﻿
[System.Serializable]
public class SB_BuildOptions
{
	public string trackingId = "";

	public virtual string ToString()
	{
		return "trackingId: " + trackingId;
	}
}

[System.Serializable]
// Google play has an extra field
public class SB_BuildOptionsGoogle : SB_BuildOptions
{
	public string base64_PublicKey = "";
	public bool spltApplicationBinary = false;
	//public string base64_shopKey = "";
	//public string GCM_ProjectNumber = "";

	override public string ToString()
	{
		//return base.ToString() + "base64_PublicKey: " + base64_PublicKey + ", spltApplicationBinary: " + spltApplicationBinary + ", base64_shopKey: " + base64_shopKey;
		return base.ToString() + "base64_PublicKey: " + base64_PublicKey; // + ", GCM_ProjectNumber: " + GCM_ProjectNumber;
	}
}

[System.Serializable]
// Google play has an extra field
public class SB_BuildOptionsAmazon : SB_BuildOptions
{
	//public string base64_shopKey = "";
	
	override public string ToString()
	{
		return base.ToString ();
		//return base.ToString() + "base64_shopKey: " + base64_shopKey;
	}
}