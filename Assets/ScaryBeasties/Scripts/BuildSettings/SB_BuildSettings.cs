﻿using UnityEngine;
using System.Collections;
using ScaryBeasties;

public class SB_BuildSettings : MonoBehaviour
{
    public SB_BuildPlatforms platform = SB_BuildPlatforms.iOS;

    [SerializeField]
    public SB_BuildOptions iOS = new SB_BuildOptions();

    [SerializeField]
    public SB_BuildOptionsGoogle GooglePlay = new SB_BuildOptionsGoogle();

    [SerializeField]
    public SB_BuildOptionsAmazon Amazon = new SB_BuildOptionsAmazon();

    [SerializeField]
    public SB_BuildOptions Samsung = new SB_BuildOptions();

    void Awake()
    {

        SB_Globals.PLATFORM_ID = platform;


#if SB_PLATFORM_IOS
		SB_Globals.PLATFORM_ID = SB_BuildPlatforms.iOS;
		Debug.Log("<color=orange>** Platform = iOS</color>");
#elif SB_PLATFORM_GOOGLE_PLAY
		SB_Globals.PLATFORM_ID = SB_BuildPlatforms.GooglePlay;
		Debug.Log("<color=orange>** Platform = Google Play</color>");
#elif SB_PLATFORM_AMAZON
		SB_Globals.PLATFORM_ID = SB_BuildPlatforms.Amazon;
		Debug.Log("<color=orange>** Platform = Google Amazon</color>");
#elif SB_PLATFORM_SAMSUNG
		SB_Globals.PLATFORM_ID = SB_BuildPlatforms.Samsung;
		Debug.Log("<color=orange>** Platform = Google Samsung</color>");
#else
        Debug.Log("<color=orange>** Platform NOT SET!!!!!!!!!</color>");
#endif


        GooglePlayDownloaderSettings.base64_PublicKey = GooglePlay.base64_PublicKey;
        GooglePlayDownloaderSettings.splitApplicationBinary = GooglePlay.spltApplicationBinary;

        switch (SB_Globals.PLATFORM_ID)
        {
            case SB_BuildPlatforms.iOS: SB_Globals.BUILD_OPTIONS = iOS; break;
            case SB_BuildPlatforms.GooglePlay: SB_Globals.BUILD_OPTIONS = GooglePlay; break;
            case SB_BuildPlatforms.Amazon: SB_Globals.BUILD_OPTIONS = Amazon; break;
            case SB_BuildPlatforms.Samsung: SB_Globals.BUILD_OPTIONS = Samsung; break;
        }

        Debug.Log("<color=orange>SB_Globals.PLATFORM_ID: " + SB_Globals.PLATFORM_ID + ", int value: " + (int)SB_Globals.PLATFORM_ID + "</color>");
    }
}
