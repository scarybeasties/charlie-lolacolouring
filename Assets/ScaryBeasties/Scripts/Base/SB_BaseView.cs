/**
 * Base View
 * -------------
 * 
 * George Tsappis
 * ScaryBeasties
 * July 2014
 * 
 * Description
 * ------------
 * 
 * 
 */ 

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using ScaryBeasties.Controls;
using ScaryBeasties.Controllers;
using ScaryBeasties.Utils;
using ScaryBeasties.Sound;

namespace ScaryBeasties.Base
{
	public class SB_BaseView : SB_Base
	{
		public const string STATE_IDLE = "stateIdle";
		public const string STATE_READY = "stateReady";
		public const string STATE_PLAYING = "statePlaying";
		public const string STATE_PAUSED = "statePaused";
		public const string STATE_INTRO = "stateIntro";
		public const string STATE_INTRO_COMPLETE = "stateIntroComplete";
		public const string STATE_OUTRO = "stateOutro";
		public const string STATE_OUTRO_COMPLETE = "stateOutroComplete";
		
		protected string _stateBeforePause = "";
		protected string _state = STATE_READY;

		protected SB_AnimationController _animationController;
		protected List<string> _animationStates = new List<string>();

		protected ArrayList _buttons = new ArrayList();

		// Setup delegates
		public delegate void OnButtonDelegate(SB_BaseView view, SB_Button btn, string eventType);
		public delegate void OnStateChangedDelegate(SB_BaseView view, string state);
		public event OnButtonDelegate OnButton;
		public event OnStateChangedDelegate OnStateChanged;

		//public string textSortingLayerName = "Default";
		public int textSortingLayerId = 0;
		public int textSortingOrder = 1;
		
		// Use this for initialization
		protected virtual void Start () 
		{
			base.Start();

			//_animationController = new SB_AnimationController (GetComponent<Animator>(), _animationStates); 
			_animationController = new SB_AnimationController (transform.GetComponent<Animator>(), _animationStates); 

			FindButtons();
			//FindDynamicText();

			// Make all buttons are not active, until the View is in its Ready state
			ButtonsEnabled(false);
			AddDelegates();
			PlayIntro();

			StartCoroutine(WaitThenInit());

			// Horrible workaround, in order to help identify objects of type 'SB_BaseView'
			if(this.tag == "Untagged")
			{
				this.tag = "SB_BaseView";
			}
		}

		protected virtual IEnumerator WaitThenInit()
		{
			yield return new WaitForSeconds(0.01f);
			Init ();
		}

		/** Subclass defines the functionality */
		protected virtual void Init()
		{
		}

		/*
		protected virtual void SetDynamicText(DynamicText textObject, string str)
		{
			if(textObject != null)
			{
				textObject.renderer.sortingLayerID = textSortingLayerId;
				textObject.renderer.sortingOrder = textSortingOrder;
				textObject.SetText(str);
			}
		}
		*/
		protected virtual void AddDelegates()
		{
			// Add button delegates
			for (int i = 0;i < _buttons.Count;i++) 
			{
				SB_Button btn = (SB_Button)_buttons[i];
				btn.OnPress += OnBtnHandler;
				btn.OnRelease += OnBtnHandler;
				btn.OnOver += OnBtnHandler;
				btn.OnOut += OnBtnHandler;
				//btn.OnDragStart += OnBtnHandler;
				//btn.OnDragging += OnBtnHandler;
				//btn.OnDragFinished += OnBtnHandler;
				btn.OnChanged += OnBtnHandler;
			}
		}
		
		protected virtual void RemoveDelegates()
		{
			// Remove button delegates
			for (int i = 0;i < _buttons.Count;i++) 
			{
				SB_Button btn = (SB_Button)_buttons[i];
				btn.OnPress -= OnBtnHandler;
				btn.OnRelease -= OnBtnHandler;
				btn.OnOver -= OnBtnHandler;
				btn.OnOut -= OnBtnHandler;
				//btn.OnDragStart -= OnBtnHandler;
				//btn.OnDragging -= OnBtnHandler;
				//btn.OnDragFinished -= OnBtnHandler;
				btn.OnChanged -= OnBtnHandler;
			}
		}

		/**
		 * Store all objects tagged with 'Button' in this scene
		 */
		protected void FindButtons()
		{
			List<GameObject> buttons = SB_Utils.GetChildrenWithTagType(gameObject, "Button", true);
			foreach (GameObject button in buttons) 
			{
				SB_Button btnScript = button.GetComponent<SB_Button>();
				//log ("Found button: " + button.name + ", btnScript.name: " + btnScript.name);
				
				if(!_buttons.Contains(btnScript))
				{
					_buttons.Add(btnScript);
				}
			}
		}

		/**
		 * Find all objects tagged with 'DynamicText' in this scene.
		 */
		/*
		protected void FindDynamicText()
		{
			List<GameObject> dynamicText = SB_Utils.GetChildrenWithTagType(gameObject, "DynamicText", true);
			foreach (GameObject txtObject in dynamicText) 
			{
				DynamicText txt = txtObject.GetComponent<DynamicText>();
				//log ("Found txt: " + txt.name + ", txt.name: " + txt.name);

				// Call 'SetDynamicText', which applies the sortingLayerID and sortingOrder
				// to the renderer.
				// This will make sure they are above other objects.
				// (The layerId and sortingOrder can't be set on DynamicText objects, so this is a workaround...)
				SetDynamicText(txt, "null");
			}
		}
		*/
		/**
		 * Enable or Disable all the child buttons within this game object
		 */
		public virtual void ButtonsEnabled(bool val)
		{
			// Disable buttons while outro is playing
			if(_state == STATE_OUTRO || _state == STATE_OUTRO_COMPLETE)
			{
				return;
			}

			for (int i = 0;i < _buttons.Count;i++) 
			{
				SB_Button btn = (SB_Button)_buttons[i];
				
				if(btn != null)
				{
					//log ("ButtonsEnabled() btn " + btn.name + " : " + val);
					btn.isActive = val;
				}
			}

			/*
			for(int n=0; n<this.transform.childCount; n++)
			{
				GameObject gameObj = this.transform.GetChild(n).gameObject;
				SB_BaseView view = gameObj.GetComponent<SB_BaseView>();
				if(view != null) view.ButtonsEnabled(val);
			}
			*/

			// Create a list of 'SB_BaseView' children, within this gameobject, and pause them all.
			List<GameObject> allBaseViewObjects = SB_Utils.GetChildrenWithTagType(this.transform.gameObject, "SB_BaseView", true);
			foreach(GameObject gameObj in allBaseViewObjects)
			{
				SB_BaseView view = gameObj.GetComponent<SB_BaseView>();
				if(view != null) view.ButtonsEnabled(val);
			}
		}

		/**
		 * Handle Button events.
		 * This should be overriden by the sub class, 
		 * so each button can be identified and dealt with.
		 */
		protected virtual void OnBtnHandler(SB_Button btn, string eventType)
		{
			//log("OnBtnHandler " + btn.name);
			if(OnButton != null) OnButton(this, btn, eventType);
		}

		bool hasAnimationController()
		{
			Animator c = transform.GetComponent<Animator>();
			if(c == null) return false;

			return true;
		}

		/** Pause the view, and its animation */
		public virtual void Pause(bool val)
		{
			if(val)
			{
				// If state is already paused, do nothing
				if(_state == STATE_PAUSED) return;

				_stateBeforePause = _state;
				SetState(STATE_PAUSED);
				//if(_animationController!=null)
				if(hasAnimationController()) 
					_animationController.Pause(true);
			}
			else
			{
				SetState(_stateBeforePause);
				//_stateBeforePause = "";

				//if(hasAnimationController()) 
				//if(_animationController!=null)
				if(hasAnimationController())
					_animationController.Pause(false);

			}

			// Create a list of 'SB_BaseView' children, within this gameobject, and pause them all.
			List<GameObject> allBaseViewObjects = SB_Utils.GetChildrenWithTagType(this.transform.gameObject, "SB_BaseView", true);
			foreach(GameObject gameObj in allBaseViewObjects)
			{
				SB_BaseView view = gameObj.GetComponent<SB_BaseView>();
				if(view != null) view.Pause(val);
			}

			// Create a list of 'SB_BaseView' children, within this gameobject, and pause them all.

			Animator[] anims = FindObjectsOfType<Animator>();
			for(int i=0;i<anims.Length;i++)
			{

				Animator view = anims[i];
				if(view != null){
					if(val) view.speed = 0;
					else view.speed = 1;
				}
			}

			// If the view is paused (..possibly because a popup might be showing), 
			// then we should ignore any raycasts
			if(val)
			{
				this.gameObject.layer = SB_Globals.IGNORE_RAYCAST_LAYER;
			}
			else
			{
				this.gameObject.layer = _gameObjectLayer;
			}
		}

		/** Update is called once per frame */
		protected virtual void Update () 
		{
			switch(_state)
			{
				case STATE_READY: 				UpdateReady();				break;
				case STATE_IDLE: 				UpdateIdle();				break;
				case STATE_PLAYING:				UpdatePlaying();			break;
				case STATE_PAUSED: 				UpdatePaused();				break;
				case STATE_INTRO: 				UpdateIntro();				break;
				case STATE_INTRO_COMPLETE: 		UpdateIntroComplete();		break;
				case STATE_OUTRO: 				UpdateOutro();				break;
				case STATE_OUTRO_COMPLETE: 		UpdateOutroComplete();		break;

				//default:
				//	warn ("Unexpected _state: " + _state); break;
			}
		}

		protected virtual void UpdateReady()
		{
			//log ("UpdateReady");
		}

		protected virtual void UpdateIdle()
		{
		}

		protected virtual void UpdatePlaying()
		{
			//log ("UpdatePlaying");
		}

		protected virtual void UpdatePaused()
		{
			//log ("UpdatePaused");
		}
		
		/** Called while the scene Intro animation is playing */
		protected virtual void UpdateIntro()
		{
			//log ("UpdateIntro");
		}
		
		/** Called once the scene Intro animation has finished playing */
		protected virtual void UpdateIntroComplete()
		{
			//warn ("UpdateIntroComplete");
			SetState(STATE_READY);
		}
		
		/** Called while the scene Outro animation is playing */
		protected virtual void UpdateOutro()
		{
			//log ("UpdateOutro");
		}
		
		/** Called once the scene Outro animation has finished playing */
		protected virtual void UpdateOutroComplete()
		{
			//log ("UpdateOutroComplete");
		}

		/** Called by Intro animation, on its first frame */
		public virtual void IntroStarted()
		{
			SetState(STATE_INTRO);
		}

		/** Called by Intro animation, on its final frame */
		public virtual void IntroComplete()
		{
			// Intro finished, we can now enable the buttons
			//ButtonsEnabled(true, _buttonNames);
			ButtonsEnabled(true);
			SetState(STATE_INTRO_COMPLETE);
		}

		 // GT - not sure why we have this, as well as IntroComplete???
		 /*
		public virtual void IntroDone()
		{
			// Intro finished, we can now enable the buttons
			//ButtonsEnabled(true, _buttonNames);
			ButtonsEnabled(true);
			SetState(STATE_INTRO_COMPLETE);
		}
		*/

		/** Called by Outro animation, on its first frame */
		public virtual void OutroStarted()
		{
			// Disable the buttons while outro is playing
			//ButtonsEnabled(false, _buttonNames);
			ButtonsEnabled(false);
			SetState(STATE_OUTRO);
		}
		
		/** Called by Outro animation, on its final frame */
		public virtual void OutroComplete()
		{
			SetState(STATE_OUTRO_COMPLETE);
		}

		public virtual void SetState(string state)
		{
			// Do nothing, if the new state is the same as the current value
			if(_state == state) return;

			//log ("\t\t\t** Setting state to " + state);
			_state = state;
			
			// Call delegate
			if(OnStateChanged != null)
			{
				OnStateChanged(this, _state);
			}
		}

		protected virtual void OpenExternalLink(string url)
		{
			if(url.Length == 0) return;

			Application.OpenURL(url);
		}

		/********************/
		/** PUBLIC METHODS **/
		/********************/

		/**
		 * Getter, returns the current state
		 */
		public string State()
		{
			return _state;
		}

		/**
		 * Play the intro animation
		 */
		public virtual void PlayIntro(string stateName="Intro")
		{
			_state = STATE_INTRO;

			if(_animationController != null || _animationController.CanPlayAnimation(stateName))
			//if(_animationController != null)
			{
				_animationController.PlayAnimation(stateName);
			}
			else
			{
				// There's no animator, so call IntroComplete immidiately
				IntroComplete();
			}
		}

		/**
		 * Play the outro animation
		 */
		public virtual void PlayOutro(string stateName="Outro")
		{
			log ("PlayOutro stateName: " + stateName);

			_state = STATE_OUTRO;

			if(_animationController != null || _animationController.CanPlayAnimation(stateName))
			//if(_animationController != null)
			{
				_animationController.PlayAnimation(stateName);
			}
			else
			{
				warn ("Can't play animation '" + stateName + "', calling OutroComplete immidiately.");

				if(_animationController == null)
				{
					warn ("\t\t** _animationController is null");
				}
				else
				{
					warn ("\t\t** _animationController.CanPlayAnimation('" + stateName + "') : " + _animationController.CanPlayAnimation(stateName));
				}

				// There's no animator, so call OutroComplete immidiately
				OutroComplete();
			}
		}

		/**
		 * This is a horrible solution to a bug with Unity animation timeline events.
		 * The available class functions do not always appear in Unity's dropdown menu (on the dope sheet event actions),
		 * so this 'AnimationEvent' function serves as a backup...
		 */
		public virtual void AnimationEvent(string eventType)
		{
			switch(eventType)
			{
				case "IntroStarted": 	IntroStarted(); 	break;
				case "IntroComplete": 	IntroComplete(); 	break;
				case "OutroStarted": 	OutroStarted(); 	break;
				case "OutroComplete":	OutroComplete(); 	break;

				default:
					warn ("AnimationEvent() unexpected event type: " + eventType);
					break;
			}
		}
		
		protected virtual void PlayButtonPressSound()
		{
			SB_SoundManager.instance.PlaySound(SB_Globals.MAIN_BUTTON_SOUND, SB_Globals.MAIN_BUTTON_VOLUME, SB_Globals.MAIN_BUTTON_SOUND_CHANNEL);
		}

		protected virtual void OnDestroy()
		{
			//log ("OnDestroy");

			OnButton = null;
			OnStateChanged = null;
			RemoveDelegates();

			if(_buttons != null)
			{
				for (int i = 0;i < _buttons.Count;i++) 
				{
					SB_Button btn = (SB_Button)_buttons[i];
					if(btn != null)
					{
						Destroy(btn);
					}
				}
				
				_buttons.Clear();
				_buttons = null;
			}

			if(_animationController != null)
			{
				Destroy(_animationController);
				_animationController = null;
			}

			if(_animationStates != null)
			{
				_animationStates.Clear();
				_animationStates = null;
			}


		}
	}
}