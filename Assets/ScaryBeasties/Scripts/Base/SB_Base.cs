/**
 * A Base class, from which all others classes can extend.
 * Contains methods to output messages to the debugger.
 * 
 * George Tsappis,
 * ScaryBeasties, 
 * July 2014
 */

using UnityEngine;
using System.Collections;
using System;

namespace ScaryBeasties.Base
{
	public class SB_Base : MonoBehaviour 
	{
		/** Sub classes can set this to 'false', to disable message logging */
		protected bool _debugEnabled = true;
		protected bool _startupDone = false;

		/* Stores the layer int which the game object is on */
		protected int _gameObjectLayer;

		string GetTimeStamp()
		{
			DateTime now = DateTime.Now;
			
			string hrs = FormatTimeStamp(now.TimeOfDay.Hours.ToString());
			string mins = FormatTimeStamp(now.TimeOfDay.Minutes.ToString());
			string secs = FormatTimeStamp(now.TimeOfDay.Seconds.ToString());
			string ms = FormatTimeStamp(now.TimeOfDay.Milliseconds.ToString());
			
			return hrs + ":" + mins + ":" + secs + ":" + ms;
		}
		
		string FormatTimeStamp(string str)
		{
			if(str.Length == 1)
			{
				str = "0" + str;
			}
			
			return str;
		}

		protected void log(object msg) 
		{
			if(!_debugEnabled) return;
			Debug.Log(GetTimeStamp() + " [" + this.GetType() + "] " + msg);
		}

		protected void warn(object msg) 
		{
			if(!_debugEnabled) return;
			Debug.LogWarning(GetTimeStamp() + "[" + this.GetType() + "] " + msg);
		}

		protected void error(object msg) 
		{
			if(!_debugEnabled) return;
			Debug.LogError(GetTimeStamp() + "[" + this.GetType() + "] " + msg);
		}
		
		protected void exception(System.Exception e) 
		{
			if(!_debugEnabled) return;
			Debug.LogException(e);
		}
		
		/** 
		 * Awake gets called before Start, so we'll use it to call Start() and then
		 * set a boolean '_startupDone = true', to prevent it being called again, when the device awakes.
		 * 
		 * // TODO: check that this has not had any adverse effects on anything!!  Added: 19/09/2014
		 */
		protected virtual void Awake()
		{
			_gameObjectLayer = this.gameObject.layer;

			if(!_startupDone)
			{
				Start();
			}
		}

		protected virtual void Start () 
		{
			if(_startupDone) return;
			_startupDone = true;
		}
	}
}