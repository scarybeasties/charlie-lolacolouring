/**
 * Base Scene
 * -------------
 * 
 * George Tsappis
 * ScaryBeasties
 * July 2014
 * 
 * Description
 * ------------
 * A base class, from which all Scenes extend.
 * 
 * Extends SB_BaseView
 * 
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System;
using ScaryBeasties.Loaders;
using ScaryBeasties.Popups;
using ScaryBeasties.UI;
using ScaryBeasties.History;
using ScaryBeasties.Data;
using ScaryBeasties.Controls;
using System.IO;
using ScaryBeasties.User;
using ScaryBeasties.Cam;

namespace ScaryBeasties.Base
{
	public class SB_BaseScene : SB_BaseView
	{
		public const string STATE_LOADING_NEXT_SCENE = "stateLoadingNextScene";

		/** 
		 * A static object, which is populated by the SB_SceneLoader.
		 * Contains properties which the scene can examine on Start.
		 */
		public static KeyValueObject SCENE_CONFIG_OBJECT = null;

		/** Used when loading scenes */
		protected SB_SceneLoader _loader;
		protected string _nextSceneToLoad = "";
		protected KeyValueObject _nextSceneConfig = null;
		protected SB_PopupManager _popupManager;
		protected SB_UIControls _uiControls;

		// Has user paused the game?
		protected bool _userPausedGame = false;
		protected bool _addSceneToHistory = true;

		public const string EVENT_SCENE_STARTED = "SceneStarted";
		public const string EVENT_SCENE_EVENT = "SceneEvent";
		public const string EVENT_BUTTON_PRESSED = "ButtonPressed";

		// Use this for initialization
		override protected void Start ()
		{
			// CT call garbage collection.
			System.GC.Collect ();

			if (Application.loadedLevelName != "MainApp") {
				_uiControls = SB_UIControls.instance;
				_uiControls.HideButtons ();
				_uiControls.currentScene = this;
			}

			LogSceneStartedEvent ();

			// Define the animation states (clips) which exist in this scene
			_animationStates = new List<string> (new string [] { "Intro", "Outro" });

			// Create a loader, for new scenes
			_loader = new SB_SceneLoader ();

			// Create a popup manager
			_popupManager = new SB_PopupManager ();

			if (_addSceneToHistory) {
				AddSceneToHistory ();
			} else {
				SB_SceneHistory.instance.ToString ();
			}

			base.Start ();

			SB_GameCamera.ApplyBlur (false);
		}

		protected void AddSceneToHistory ()
		{
			// Add this scene name to the SceneHistory, so the back button will return the user here
			SB_SceneHistory.instance.AddScene (Application.loadedLevelName);
		}

		public SB_BasePopup CurrentPopup {
			get {
				if (_popupManager != null) return _popupManager.CurrentPopupScript;
				return null;
			}
		}

		/**
		 * This is used to pause coroutines!!!
		 * Every coroutine needs to have this line added:
		 * 
		 * 		yield return _sync();
		 * 
		 */
		public Coroutine _sync ()
		{
			return StartCoroutine (PauseRoutine ());
		}

		public IEnumerator PauseRoutine ()
		{
			while (_userPausedGame) {
				yield return new WaitForFixedUpdate ();
			}

			yield return new WaitForEndOfFrame ();
		}

		public virtual string GetSceneTrackingName ()
		{
			return Application.loadedLevelName;
		}

		protected Dictionary<string, string> GetSceneTrackingParams ()
		{
			Dictionary<string, string> trackingParams = new Dictionary<string, string> ()
			{
				{"SceneName", GetSceneTrackingName()}
			};

			return trackingParams;
		}

		private void LogSceneStartedEvent ()
		{
			SB_Analytics.instance.SceneView(GetSceneTrackingName());

			string ev = GetSceneTrackingName () + "_" + EVENT_SCENE_STARTED;
			//SB_Analytics.logEvent (ev, GetSceneTrackingParams (), true);
		}

		private void EndSceneStartedEvent ()
		{
			string ev = GetSceneTrackingName () + "_" + EVENT_SCENE_STARTED;
			//SB_Analytics.endTimedEvent (ev, GetSceneTrackingParams ());
		}

		public virtual void LogSceneEvent (string eventName, Dictionary<string, string> parameters = null, bool isTimed = false)
		{
			string ev = GetSceneTrackingName () + "_" + EVENT_SCENE_EVENT;
			if (parameters == null) {
				parameters = GetSceneTrackingParams ();
			}
			parameters.Add ("EventName", eventName);

			//SB_Analytics.logEvent (ev, parameters, isTimed);

		}

		public virtual void LogSceneEndTimedEvent (string eventName, Dictionary<string, string> parameters = null)
		{
			string ev = GetSceneTrackingName () + "_" + eventName;
			//SB_Analytics.endTimedEvent (ev, parameters);
		}

		public virtual void LogButtonEvent (string btnName, Dictionary<string, string> parameters = null, bool isTimed = false)
		{
			string ev = GetSceneTrackingName () + "_" + EVENT_BUTTON_PRESSED;

			if (parameters == null) {
				parameters = GetSceneTrackingParams ();
			}
			parameters.Add ("ButtonName", btnName);

			//SB_Analytics.logEvent (ev, parameters, isTimed);

		}

		public virtual void LogPackUnlockedEvent (int id)
		{
			//SB_Analytics.logEvent ("PackUnlocked_" + id.ToString ());
		}

		public void ShowPausePopup ()
		{
			_userPausedGame = true;
			ShowPopup (SB_PopupTypes.GAME_PAUSED_POPUP);
		}

		/**
		 * Show a popup.
		 * @int popupType The type of the popup to show (defined inside SB_PopupTypes class)
		 * @SB_PopupVO popupVO An optional Value Object, containing title and body text,
		 * 					which is applied when the popup has opened.
		 */
		public virtual void ShowPopup (int popupType, SB_PopupVO popupVO = null)
		{
			// Disable the game buttons, while the popup is being shown
			ButtonsEnabled (false);
			if (_uiControls != null) _uiControls.ButtonsEnabled (false);
			Pause (true);

			_popupManager.ShowPopup (popupType, popupVO);
		}

		/** Called once a popup has been closed */
		protected virtual void PopupClosed ()
		{
			if (!_popupManager.PopupIsShowing) {
				ButtonsEnabled (true);
				if (_uiControls != null) _uiControls.ButtonsEnabled (true);
				Pause (false);
			}
		}

		override protected void AddDelegates ()
		{
			base.AddDelegates ();

			if (_uiControls != null) _uiControls.OnUIControl += OnBtnHandler;

			_loader.OnLoadingProgress += HandleOnLoading;
			_loader.OnLoadingComplete += HandleOnLoading;

			_popupManager.OnPopupEvent += HandleOnPopupEvent;
		}

		override protected void RemoveDelegates ()
		{
			base.RemoveDelegates ();

			if (_uiControls != null) _uiControls.OnUIControl -= OnBtnHandler;

			if (_loader != null) {
				_loader.OnLoadingProgress -= HandleOnLoading;
				_loader.OnLoadingComplete -= HandleOnLoading;
			}

			if (_popupManager != null) {
				_popupManager.OnPopupEvent -= HandleOnPopupEvent;
				//warn ("POPUP DELEGATES REMOVED");
			}
			//warn ("DELEGATES REMOVED");
		}

		/** This method is triggered during loading of a scene */
		protected virtual void HandleOnLoading (SB_SceneLoader loader, string eventType)
		{
			//log ("HandleOnLoading loaded: " + loader.getProgressPercentage() + "%, eventType: " + eventType);
		}

		/** This method is triggered whenever a popup event occurs */
		protected virtual void HandleOnPopupEvent (SB_BasePopup popup, string eventType)
		{
			log ("HandleOnPopupEvent " + eventType);
			switch (eventType) {
			case SB_BasePopup.POPUP_CLOSED:
				PopupClosed ();
				break;
			}
		}

		/** Update is called once per frame */
		override protected void Update ()
		{
			base.Update ();

			switch (_state) {
			case STATE_LOADING_NEXT_SCENE: UpdateLoadingNextScene (); break;

				//default:
				//warn ("Unexpected _state: " + _state); break;
			}
		}

		/** Called during loading of a new scene */
		protected virtual void UpdateLoadingNextScene ()
		{
			//log ("UpdateLoadingNextScene loaded: " + _loader.getProgressPercentage());
		}

		override public void IntroComplete ()
		{
			base.IntroComplete ();
			if (_uiControls != null) _uiControls.ButtonsEnabled (true);
		}

		/** Called by Outro animation, on its final frame */
		override public void OutroComplete ()
		{
			base.OutroComplete ();
			StartLoadingNextScene ();
		}

		/********************/
		/** PUBLIC METHODS **/
		/********************/

		/**
		 * Play the outro animation, then load a scene.
		 * @nextSceneToLoad string Name of the scene to load
		 * @stateName string Name of the animation state to play
		 * @nextSceneConfig object An object that will be passed to the scene, when it's finished loading.  Can contain any initialisation properties (skip intro, etc..)
		 */
		public void PlayOutroAndLoad (string nextSceneToLoad, string stateName = "Outro", KeyValueObject nextSceneConfig = null)
		{
			log ("PlayOutroAndLoad " + nextSceneToLoad + ", stateName: " + stateName);
			LoadScene (nextSceneToLoad, nextSceneConfig);
			PlayOutro (stateName);
		}

		public virtual void LoadScene (string nextSceneToLoad, KeyValueObject nextSceneConfig = null, bool immediate = false)
		{
			_nextSceneToLoad = nextSceneToLoad;
			_nextSceneConfig = nextSceneConfig;

			if (immediate) {
				StartLoadingNextScene ();
			}
		}

		protected virtual void StartLoadingNextScene ()
		{
			if (_nextSceneToLoad != "") {
				_state = STATE_LOADING_NEXT_SCENE;
				//_loader.loadLevel(_nextSceneToLoad, _nextSceneConfig);

				StartCoroutine (WaitThenLoadLevel ());

				//_state = STATE_LOADING_NEXT_SCENE;
				//_asyncOperation = Application.LoadLevelAsync(_nextSceneToLoad);
				//UpdateLoadingNextScene();
			}
		}


		/**
		 * Don't load straight away.
		 * We need to delay the loading of the next scene, to give the 
		 * 'Loader' gameObject time to display its message.
		 * 
		 * If we don't add a slight pause before the loading, the 
		 * Loader's message can occasionally appear blank
		 */
		protected IEnumerator WaitThenLoadLevel ()
		{
			yield return new WaitForSeconds (0.2f);
			_loader.loadLevel (_nextSceneToLoad, _nextSceneConfig);
		}

		//used to capture an area of screen to a file
		protected void SaveScreenArea (Rect area, string filename, Vector2 finalsize = default (Vector2))
		{
			StartCoroutine (EnumSaveScreenArea (area, filename, finalsize));
		}

		private IEnumerator EnumSaveScreenArea (Rect area, string filename, Vector2 finalsize = default (Vector2))
		{
			yield return new WaitForEndOfFrame ();

			float pcwid = (area.width) / ((float)tk2dCamera.Instance.ScreenExtents.size.x);
			float pcx = ((((float)tk2dCamera.Instance.ScreenExtents.size.x) / 2.0f) + area.x) / ((float)tk2dCamera.Instance.ScreenExtents.size.x);//(1.0f-pcwid)/2;
			int acx = (int)(pcx * Screen.width);
			//acx+=Screen.width/2;

			int wid = (int)(pcwid * Screen.width);

			float pchgt = (area.height) / ((float)tk2dCamera.Instance.ScreenExtents.size.y);
			//float pcy = (1.0f-pchgt)/2;
			float pcy = ((((float)tk2dCamera.Instance.ScreenExtents.size.y) / 2.0f) + area.y) / ((float)tk2dCamera.Instance.ScreenExtents.size.y);//(1.0f-pcwid)/2;

			int acy = (int)(pcy * Screen.height);
			//acy+=Screen.height/2;

			int hgt = (int)(pchgt * Screen.height);

			Texture2D sshot = new Texture2D (wid, hgt);
			sshot.ReadPixels (new Rect (acx, acy, wid, hgt), 0, 0);
			sshot.Apply ();

			if (finalsize.x > 0) {
				TextureScale.Bilinear (sshot, (int)finalsize.x, (int)finalsize.y);
			}

			byte [] imgBytes;
			if (filename.EndsWith (".png")) {
				imgBytes = sshot.EncodeToPNG ();
			} else {
				imgBytes = sshot.EncodeToJPG ();
			}

			Destroy (sshot);

			File.WriteAllBytes (filename, imgBytes);
		}

		override protected void OnBtnHandler (SB_Button btn, string eventType)
		{
			base.OnBtnHandler (btn, eventType);

			if (eventType == SB_Button.MOUSE_UP) {
				LogButtonEvent (btn.name);
			}
		}

		override protected void OnDestroy ()
		{
			//warn ("REMOVING SCENE");
			base.OnDestroy ();

			EndSceneStartedEvent ();

			_nextSceneToLoad = "";

			if (_uiControls != null) {
				_uiControls = null;
			}

			if (_loader != null) {
				_loader.OnDestroy ();
				Destroy (_loader);
				_loader = null;
			}

			if (_popupManager != null) {
				_popupManager.OnDestroy ();
				Destroy (_popupManager);
				_popupManager = null;
				//warn ("REMOVED POPUP MANAGER IN SCENE");
			}

			//warn ("SCENE REMOVED");

		}
	}
}