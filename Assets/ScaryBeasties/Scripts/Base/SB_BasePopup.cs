using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

/**
 * Define available popups
 */
using ScaryBeasties.Controls;
using ScaryBeasties.Popups;
using ScaryBeasties.Utils;
using UnityEngine.UI;
using ScaryBeasties.Cam;

namespace ScaryBeasties.Base
{
	public class SB_PopupTypes
	{
		// Popup types
		public const int GATING_POPUP = 0;
		public const int GATING_POPUP_DELETE_PROFILE = 1;
		public const int CONFIRM_DELETE_PROFILE_POPUP = 2;
		public const int GAME_PAUSED_POPUP = 3;
		public const int GAME_HELP_POPUP = 4;
		public const int GAME_DIFFICULTY_SELECTION_POPUP = 5;
		public const int GAME_QUIT_POPUP = 6;
		public const int CAMERA_POPUP = 7;
		public const int ALERT_POPUP = 8;
		
		public const int CONTINUE_DRAWING = 9;
		public const int RESET_DRAWING_POPUP = 10;
	}

	/**
	 * Base class for popups
	 */
	public class SB_BasePopup : SB_BaseView 
	{
		public const string POPUP_STARTED 	= "popupStarted";
		public const string POPUP_OPEN 		= "popupOpen";
		public const string POPUP_CLOSED 	= "popupClosed";
		public const string OPTION_CLOSE 	= "optionClose";

		// Store the text mesh objects
		protected SB_TextField _titleText;
		protected SB_TextField _bodyText;

		protected Text _titleTextText;
		protected Text _bodyTextText;


		// Store the value object within the popup
		public SB_PopupVO vo;

		// Setup delegates
		public delegate void PopupStartedDelegate(SB_BasePopup popup, string eventType);
		public delegate void PopupOpenDelegate(SB_BasePopup popup, string eventType);
		public delegate void PopupClosedDelegate(SB_BasePopup popup, string eventType);
		public delegate void OptionChosenDelegate(SB_BasePopup popup, string eventType);
		public event PopupStartedDelegate OnPopupStarted;
		public event PopupOpenDelegate OnPopupOpen;
		public event PopupClosedDelegate OnPopupClosed;
		public event OptionChosenDelegate OnOptionChosen;

		override protected void Start () 
		{
			// Define the animation states (clips) which exist in this scene
			_animationStates = new List<string>(new string[] {"Intro", "Outro"});

			base.Start();

			// Call the delgate
			if(OnPopupStarted != null) OnPopupStarted(this, POPUP_STARTED);
			
			ApplyBlur(true);
		}

		public virtual string GetSceneTrackingName()
		{
			return Application.loadedLevelName;
		}
		
		public virtual void LogPopupEvent(string eventName)
		{
			//SB_Analytics.logEvent(GetSceneTrackingName() + "_" + eventName);
		}
		
		public virtual void LogButtonEvent(string btnName)
		{
			//SB_Analytics.logEvent(GetSceneTrackingName() + "_btn_" + btnName);
		}

		override protected void Init()
		{
			log("------------ Init");
			base.Init ();

			Transform titleTrans = transform.Find("panel/text/title");
			Transform bodyTrans = transform.Find("panel/text/body");

			if(titleTrans != null)
			{
				// Old style text (legacy code..)
				_titleText = titleTrans.GetComponent<SB_TextField>();
				if(vo != null && _titleText != null) _titleText.text = SB_Utils.SanitizeString(vo.title);

				// New unity text
				_titleTextText = titleTrans.GetComponent<Text>();
				if(vo != null && _titleTextText != null) _titleTextText.text = SB_Utils.SanitizeString(vo.title);
			}

			if(bodyTrans != null)
			{
				// Old style text (legacy code..)
				_bodyText = bodyTrans.GetComponent<SB_TextField>();
				if(vo != null && _bodyText != null) _bodyText.text = SB_Utils.SanitizeString(vo.body);

				// New unity text
				_bodyTextText = bodyTrans.GetComponent<Text>();
				if(vo != null && _bodyTextText != null) _bodyTextText.text = SB_Utils.SanitizeString(vo.body);
			}
		}

		/**
		 * A public function, to allow the scene which is showing 
		 * this popup to set the title and body text
		 */
		public void SetText(string title, string body)
		{
			title = SB_Utils.SanitizeString(title);
			body = SB_Utils.SanitizeString(body);
			
			log ("SetText title: " + title + ", body: " + body);
			if(_titleText != null) _titleText.text = title;
			else log ("_titleText is null!!");
			if(_titleTextText != null) _titleTextText.text = title;
			else log ("_titleTextText is null!!");

			if(_bodyText != null) _bodyText.text = body;
			if(_bodyTextText != null) _bodyTextText.text = body;
			//else log ("_bodyText is null!!");
		}

		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			base.OnBtnHandler(btn, eventType);

			if(eventType == SB_Button.MOUSE_UP)
			{
				switch(btn.name)
				{
					case "close_btn":
						Close();
						break;
				}
			}
		}

		public virtual void Close()
		{
			OptionChosen(OPTION_CLOSE);
			PlayOutro();
		}

		protected void OptionChosen(string optionName)
		{
			// Call the delgate
			if(OnOptionChosen != null) OnOptionChosen(this, optionName);
		}
		
		override public void IntroComplete()
		{
			PopupOpen();
			base.IntroComplete ();
		}

		override public void OutroComplete()
		{
			log ("OutroComplete");
			PopupClosed();
			base.OutroComplete ();
		}

		private void PopupOpen() 
		{
			// Call the delgate
			if(OnPopupOpen != null) OnPopupOpen(this, POPUP_OPEN);
		}

		private void PopupClosed() 
		{
			ApplyBlur(false);
			
			log ("PopupClosed");
			// Call the delgate
			if(OnPopupClosed != null) OnPopupClosed(this, POPUP_CLOSED);
		}
		
		public void ApplyBlur(bool val)
		{
			SB_GameCamera.ApplyBlur(val);
		}

		override protected void OnDestroy()
		{
			base.OnDestroy();
			
			_titleText = null;
			_bodyText = null;
			_titleTextText = null;
			_bodyTextText = null;

			OnPopupStarted = null;
			OnPopupOpen = null;
			OnPopupClosed = null;
			OnOptionChosen = null;

			//warn("BASE POPUP DESTROYED");
		}
	}
}
