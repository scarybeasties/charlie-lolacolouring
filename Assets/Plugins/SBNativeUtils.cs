﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class SBNativeUtils : MonoBehaviour 
{
	#if UNITY_IPHONE

	[DllImport("__Internal")]
	private static extern void SB_iOS_CameraAllowed();

	#endif

	public static void CameraAllowed()
	{
		Debug.LogWarning("CameraAllowed");

		#if UNITY_EDITOR
			Debug.LogWarning("UNITY_EDITOR");
		#elif UNITY_IPHONE
			SB_iOS_CameraAllowed();
		#elif UNITY_ANDROID	
			// Android code to go here...
		#endif
	}
}
