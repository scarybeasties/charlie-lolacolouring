#import "NativeAppstore.h"

@implementation NativeAppstore

- (id)init
{
    self = [super init];
    
    return self;
}

-(void) openAppInStore: (int) appID withCampaignToken: (NSString *) campaignToken andAffiliateToken: (NSString *) affiliateToken andProviderToken: (NSString *) providerToken
{
    if ([SKStoreProductViewController class])
    {
        NSDictionary *parameters;
        
		if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1)
		{
        	// Load resources for iOS 7.1 or earlier
			parameters = @{SKStoreProductParameterITunesItemIdentifier: [NSNumber numberWithInteger: appID]};
		}
		else if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_8_2)
		{
			// Load resources upto iOS 8.2
			// 'SKStoreProductParameterProviderToken' not supported in this version
			parameters = @{SKStoreProductParameterITunesItemIdentifier: [NSNumber numberWithInteger: appID],
						   SKStoreProductParameterCampaignToken: campaignToken,
						   SKStoreProductParameterAffiliateToken: affiliateToken};
		}
		else
		{
			// Load resources for iOS 8.3 or later
			parameters = @{SKStoreProductParameterITunesItemIdentifier: [NSNumber numberWithInteger: appID],
						   SKStoreProductParameterCampaignToken: campaignToken,
						   SKStoreProductParameterAffiliateToken: affiliateToken,
						   SKStoreProductParameterProviderToken: providerToken};
		}
									 
        SKStoreProductViewController *productViewController = [[SKStoreProductViewController alloc] init];
        [productViewController loadProductWithParameters:parameters completionBlock:nil];
        [productViewController setDelegate:self];
        [UnityGetGLViewController() presentViewController:productViewController animated:YES completion:nil];
    }
    
    else
    {
        																							 //https://itunes.apple.com/gb/app/gruffalo-games/id909269064?mt=8&at=11l32DB
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat: @"http://itunes.apple.com/app/id%d?mt=8&at=%s&ct=%s&pt=%s", appID, affiliateToken, campaignToken, providerToken]]];
    }
}

-(void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
	[viewController dismissViewControllerAnimated:YES completion:nil];
    
    UnitySendMessage("~AppstoreHandler", "appstoreClosed", "");
}

- (BOOL)prefersStatusBarHidden
{
	return YES;
}

@end

// Helper method to create C string copy
NSString* MakeNSString (const char* string) {
    if (string) {
        return [NSString stringWithUTF8String: string];
    } else {
        return [NSString stringWithUTF8String: ""];
    }
}


static NativeAppstore *nativeAppstorePlugin = nil;


extern "C"
{
    void _OpenAppInStore(int appID, const char *campaignToken, const char *affiliateToken, const char *providerToken)
    {
        NSLog(@"OpenAppInStore :: Open App %d %s %s %s", appID, campaignToken, affiliateToken, providerToken);
        
        if (nativeAppstorePlugin == nil)
            nativeAppstorePlugin = [[NativeAppstore alloc] init];
        
		[nativeAppstorePlugin openAppInStore: appID withCampaignToken: MakeNSString(campaignToken) andAffiliateToken: MakeNSString(affiliateToken) andProviderToken: MakeNSString(providerToken)];
    }
}