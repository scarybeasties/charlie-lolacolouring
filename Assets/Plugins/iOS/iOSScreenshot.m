//
//  iOSScreenshot.m
//  
//
//  Created by Ryan on 20/03/2013.
//
//

bool saveToGallery( const char * path )
{
	NSString *imagePath = [NSString stringWithUTF8String:path];
	
    NSLog(@"###### This is the file path being passed: %@", imagePath);
    
	if( ![[NSFileManager defaultManager] fileExistsAtPath:imagePath] ) {
        NSLog(@"###### Early exit - file doesn't exist");
        return false;
    }
    
	UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    
    /*NSError *error;
    if( [[NSFileManager defaultManager] fileExistsAtPath:imagePath] ) {
        if ([[NSFileManager defaultManager] isDeletableFileAtPath:imagePath]) {
            BOOL success = [[NSFileManager defaultManager] removeItemAtPath:imagePath error:&error];
            if (!success) {
                NSLog(@"Error removing file at path: %@", error.localizedDescription);
            }
        }
    }*/
	
	if( image ) {
        NSLog(@"###### Trying to write image");
		UIImageWriteToSavedPhotosAlbum( image, nil, NULL, NULL );
        /*
        NSError *error;
        if( [[NSFileManager defaultManager] fileExistsAtPath:imagePath] ) {
            if ([[NSFileManager defaultManager] isDeletableFileAtPath:imagePath]) {
                BOOL success = [[NSFileManager defaultManager] removeItemAtPath:imagePath error:&error];
                if (!success) {
                    NSLog(@"Error removing file at path: %@", error.localizedDescription);
                }
            }
        }*/
        return true;
    }
    
    return false;
}

bool saveToFile( const char * path )
{
    NSString *imagePath = [NSString stringWithUTF8String:path];
    
    NSLog(@"###### This is the file path being passed: %@", imagePath);
    
    if( ![[NSFileManager defaultManager] fileExistsAtPath:imagePath] ) {
        NSLog(@"###### Early exit - file doesn't exist");
        return false;
    }
    
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    
    if( image ) {
        NSLog(@"###### Trying to write image");
        //UIImageWriteToSavedPhotosAlbum( image, nil, NULL, NULL );
        return true;
    }
    
    return false;
}
