//
//  SB_iOS_Utils.m
//  
//  Utility class to access 
//  native iOS functions from Unity
//
//  Scarybeasties 
//  12/11/2014
//
//

#import <AVFoundation/AVFoundation.h>

// Check if user has granted permission for the camera to be used
void SB_iOS_CameraAllowed()
{
	//__block isAllowed = false;
	UnitySendMessage("PluginListener", "HandleIncoming", "SB_iOS_CameraAllowed called");
	
	// Determine camera access on iOS >= 7
	if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType:completionHandler:)]) 
	{
	    // Completion handler will be dispatched on a separate thread
	    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) 
	    {
		if (YES == granted) 
		{
			// User granted access to the camera, continue with app launch
			UnitySendMessage("PluginListener", "HandleIncoming", "camera_allowed");
		}
		else 
		{
			// Camera denied
			UnitySendMessage("PluginListener", "HandleIncoming", "camera_denied");
		}

	    }];
	}
	else 
	{
	    // iOS < 7 (camera access always OK) 
		UnitySendMessage("PluginListener", "HandleIncoming", "camera_allowed");
	}
}