﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ScaryBeasties.Base;
using ScaryBeasties.Sound;
using ScaryBeasties.Games.Common;

namespace ScaryBeasties.Games.DrawingBadge
{
	public class ColouringHelp : Help
	{	
		private GameObject _finger;
		private tk2dSprite _background;
		private tk2dSpriteAnimator _fingerAnim;
		
		private bool _tutorialComplete = false;
		private bool _voComplete = false;
		private List<string> _voList;
		private string _soundPath;
		
		// Use this for initialization
		override protected void Start ()
		{
			base.Start ();
			
			_animationController.animationStates = new List<string> (new string[] {
				"Intro",
				"Outro",
				"Tutorial0",
				"Tutorial1",
				"Tutorial2",
				"Tutorial3",
				"Tutorial4"
			});
			
			_tutorialScreens = new List<string> (new string[] {
				"Tutorial0",
				"Tutorial1",
				"Tutorial2",
				"Tutorial3",
				"Tutorial4"
			});
			
			_soundPath = "Localised/VO/Charlie/";
			_voList = new List<string> (new string[] { "", 
														"vo_colouring_things",
														"vo_animal_in_tap",
				"vo_colouring_finger",
				"vo_animal_in_rub"
			});
			
			_background = transform.Find ("panel/panel_bg").GetComponent<tk2dSprite> ();
			_fingerAnim = transform.Find ("panel/finger").GetComponent<tk2dSpriteAnimator> ();
			
			//_fingerAnim.AnimationCompleted += HandleFingerAnimComplete;
		}
		
		protected override void UpdateTutorial ()
		{
			base.UpdateTutorial ();
			if (!SB_SoundManager.instance.VoicePlaying ()) {
				_voComplete = true;
			}
			if (_voComplete && _tutorialComplete) {
				PlayNextTutorial ();
			}
		}
		
		public override void SwitchTutorialSection ()
		{
			base.SwitchTutorialSection ();
			
			
			PlayTutorialVO ();
			
			_voComplete = false;
			_tutorialComplete = false;
			_animationController.PlayAnimation (_tutorialScreens [_currentTutorial]);
			
		}
		
		
		public void ExtendedAnimationEvent (string eventType)
		{
			switch (eventType) {
			case "AnimationStarted":
					
				break;
			case "AnimationComplete":
				if (!_voComplete) {
					_animationController.PlayAnimation (_tutorialScreens [_currentTutorial]);
				} else { 
					_tutorialComplete = true; 
				}
				break;
			case "FingerLift":
				_fingerAnim.Play ("FingerLift");
				break;
			case "FingerPress":
				_fingerAnim.Play ("FingerDown");
				break;
			case "FingerTap":
				_fingerAnim.Play ("FingerTap");
				break;
			case "OutroComplete":
				OutroComplete ();
				break;
			}
		}
		
		protected void PlayTutorialVO ()
		{
			if (_voList [_currentTutorial] != "") {
				string path = _soundPath + _voList [_currentTutorial];
				log ("PlayTutorialVO path: " + path);
				SB_SoundManager.instance.PlayVoice (path);
			}
		}
		/*
		void HandleFingerAnimComplete(tk2dSpriteAnimator caller, tk2dSpriteAnimationClip currentClip ) {
			switch( currentClip.name )
			{
			case "FingerLift":
				_fingerAnim.Play("FingerIdle");
				break;
			case "FingerPress":
				_fingerAnim.Play("FingerDown");
				break;
			case "FingerTap":
				_fingerAnim.Play("FingerIdle");
				break;
			}
		}
		*/
		protected override void OnDestroy ()
		{
			base.OnDestroy ();
			
			//_fingerAnim.AnimationCompleted -= HandleFingerAnimComplete;
			//_fingerAnim.AnimationCompleted = null;
			
			_finger = null;
			_background = null;
			_fingerAnim = null;
			
			_voList.Clear ();
			_voList = null;
		}
	}
}