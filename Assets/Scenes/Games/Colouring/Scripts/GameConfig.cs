﻿using UnityEngine;
using System.Collections;

/** 
 * Game configuration constants for Feeding Ducks game.
 */
using ScaryBeasties.Data;


namespace ScaryBeasties.Games.Colouring
{
	public class GameConfig 
	{
		// Name of the Help Popup prefab to use.
		public const string HELP_POPUP_RESOURCE_NAME 			= "Games/Colouring/Colouring_GameHelp_Prefab";
		
		// Temporary KeyValueObject, used to store user creation if they've gone to another scene
		public static KeyValueObject USER_SCENE_CONFIG_OBJECT = null;
		
		// Store the number of times the current user has played this game
		public static int NUMBER_OF_PLAYS = 0;
		
		public const int GAME_ID = 2;

		public const string COLOURING_ID = "colourID";
		public const string COLOURING_PAGE = "colourPage";
		
		public static ArrayList DRAWING_MENU_ITEMS = new ArrayList();
		public static string DRAWING_MENU_TITLE = "";
	}
}
