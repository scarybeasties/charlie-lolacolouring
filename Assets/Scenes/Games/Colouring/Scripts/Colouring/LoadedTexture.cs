﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

namespace ScaryBeasties.Colouring
{
    public class LoadedTexture : MonoBehaviour
    {


        private Texture2D _texture;
        private Action<bool> _callback;
        private Sprite _sprite;

        public bool loaded = false;
        public float scaler = 1.0f;
        public TextureWrapMode TextureWrapMode = TextureWrapMode.Clamp;

        // Use this for initialization
        void Awake()
        {
            _sprite = GetComponent<SpriteRenderer>().sprite;
            GetComponent<SpriteRenderer>().enabled = false;
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void CreateEmptyTexture(int w, int h)
        {
            _texture = new Texture2D(w, h, TextureFormat.RGBA32, false);

            // Set all pixels to pure white
            /*
			for(int i=0; i<w; i++)
			{
				for(int j=0; j<h; j++)
				{
					_texture.SetPixel(i, j, Color.white);
				}
			}
			_texture.Apply();
			*/

            // Set all pixels to pure white
            int len = w * h;
            Color[] colours = new Color[len];
            for (int i = 0; i < len; i++)
            {
                colours[i] = Color.white;
            }

            _texture.wrapMode = TextureWrapMode;
            _texture.SetPixels(colours);
            _texture.Apply();

            CreateSprite();
        }

        public Texture2D GetTexture()
        {
            _texture.name = "LoadedTexture";
            if (gameObject != null && gameObject.name.Length > 0)
            {
                _texture.name = gameObject.name;
            }
            return _texture;
        }

        //Image Loading
        public void LoadImage(string filePath, Action<bool> callback, bool loadFromLocalFileSystem = true)
        {
            log("LoadImage " + filePath);
            //Debug.LogWarning ("[SB2DImageLoader] LoadImage: "  +filePath);
            _callback = callback;

            if (loadFromLocalFileSystem)
            {
                filePath = "file://" + filePath;
            }
            StartCoroutine(DoLoadImage(filePath, callback));
        }

        public void LoadBytes(byte[] bytes, Action<bool> callback)
        {
            log("LoadBytes ");
            _callback = callback;

            // IMPORTANT! TextureFormat.RGBA32 must be set, for the images to look good on screen.
            //				mipmap must be set to false, otherwise the image looks blurry
            _texture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            _texture.LoadImage(bytes);
            _texture.wrapMode = TextureWrapMode;
            DoCallback(true);
        }

        IEnumerator DoLoadImage(string filePath, Action<bool> callback)
        {

            log("DoLoadImage " + filePath);
            _callback = callback;
#if UNITY_ANDROID
            filePath = filePath.Replace("file://", "file:///");
            WWW www = new WWW(filePath);
            yield return www;

            if (www.isDone)
            {
                if (www.bytes.Length == 0)
                {
                    // error
                    Debug.LogError("ERROR LOADING FILE " + www.error);
                    DoCallback(false);
                }
                else
                {
                    // success
                    _callback = callback;
                    _texture = new Texture2D(www.texture.width, www.texture.height, TextureFormat.RGBA32, false);
                    www.LoadImageIntoTexture(_texture);

                    _texture.wrapMode = TextureWrapMode;

                    log("<color=green>TEXURE LOADED w: " + _texture.width + ", h: " + _texture.height + "</color>");
                    CreateSprite();
                }
            }

            www.Dispose();
            www = null;
#else
            filePath = filePath.Replace("file:///", "file://");
            filePath = filePath.Replace("file://", "");

            if(File.Exists(filePath))
            {
                _callback = callback;
                _texture = new Texture2D(2, 2);
                _texture.LoadImage(File.ReadAllBytes(filePath), false);
                _texture.wrapMode = TextureWrapMode;
                yield return null;
                CreateSprite();
            }
            else
            {
                DoCallback(false);
            }

#endif
        }

        void CreateSprite()
        {
            log("CreateSprite");
            log("\t\tw: " + _texture.width + ", h: " + _texture.height);

            Rect r = new Rect(0f, 0f, _texture.width, _texture.height);
            log("\t\tRect created");

            _sprite = Sprite.Create(_texture, r, new Vector2(0.5f, 0.5f), 1f);
            log("_sprite created");
            GetComponent<SpriteRenderer>().sprite = _sprite;
            log("setting SpriteRenderer _sprite");
            /*
			if(_sprite == null)
			{
				_sprite = Sprite.Create(_texture, r, new Vector2(0.5f, 0.5f),1f);
				log("_sprite created");
				GetComponent<SpriteRenderer>().sprite = _sprite;
				log("setting SpriteRenderer _sprite");
			}
			else
			{
				log("_sprite is not null - replacing the texture");
				
				Texture2D texture = _sprite.texture;
				texture.SetPixels(_texture.GetPixels());
				texture.Apply();
				
				log("_sprite texture has been set");
			}
			*/

            GetComponent<SpriteRenderer>().enabled = true;

            float _scaler = 1.0f;

            switch (tk2dSystem.CurrentPlatform)
            {
                case "2x":
                    _scaler = 0.5f;
                    break;

                case "4x":
                    _scaler = 0.25f;
                    break;

                default:
                    _scaler = 1f;
                    break;
            }

            scaler = _scaler;

            log("setting up transform");
            Vector3 rescaled = transform.localScale;
            rescaled.x = _scaler;
            rescaled.y = _scaler;
            transform.localScale = rescaled;
            loaded = true;

            log("..about to do callback");
            DoCallback(true);
        }

        void DoCallback(bool success)
        {
            log("DoCallback success: " + success);

            if (_callback != null)
            {
                _callback(success);
            }
        }

        string GetTimeStamp()
        {
            DateTime now = DateTime.Now;

            string hrs = FormatTimeStamp(now.TimeOfDay.Hours.ToString());
            string mins = FormatTimeStamp(now.TimeOfDay.Minutes.ToString());
            string secs = FormatTimeStamp(now.TimeOfDay.Seconds.ToString());
            string ms = FormatTimeStamp(now.TimeOfDay.Milliseconds.ToString());

            return hrs + ":" + mins + ":" + secs + ":" + ms;
        }

        string FormatTimeStamp(string str)
        {
            if (str.Length == 1)
            {
                str = "0" + str;
            }

            return str;
        }

        protected void log(object msg)
        {
            Debug.Log(GetTimeStamp() + " [" + this.GetType() + "] " + msg);
        }

        void OnDestroy()
        {
            _texture = null;
            _callback = null;
            _sprite = null;
        }
    }
}
