﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Base;
using ScaryBeasties.Controls;
using System.Collections.Generic;
using ScaryBeasties.Utils;
using ScaryBeasties.Colouring;
using ScaryBeasties.Sound;

namespace ScaryBeasties.Colouring
{
	public class ColouringBottomMenu : SB_BaseView
	{
		public const string BTN_UNDO = "ui_undo_btn";
		//public const string BTN_COLOUR_PICKER = "ui_eyedropper_btn";
		public const string BTN_SAVE = "ui_save_btn";
		public const string BTN_TICK = "ui_tick_btn";
		//public const string BTN_HELP = "ui_help_btn";
		public const string BTN_CAMERA = "ui_camera_btn";

		public const int STATE_GAME = 0;
		public const int STATE_END = 1;
		private int _mState = -1;
		
		public ColouringScene ColouringScene; // Store a reference to the container scene, so we can interact with it

		private bool _enabled = true;

		override protected void Start ()
		{
			base.Start ();
			SetState (STATE_GAME);
			PositionButtons ();
		}

		public void Enable (bool val)
		{
			_enabled = val;
		}
		
		public void SetState (int state)
		{
			_mState = state;
			
			Transform btnUndo = transform.Find ("buttons/" + BTN_UNDO);
			//Transform btnColourPicker = transform.Find ("buttons/" + BTN_COLOUR_PICKER);
			Transform btnSave = transform.Find ("buttons/" + BTN_SAVE);
			Transform btnTick = transform.Find ("buttons/" + BTN_TICK);
			//Transform btnHelp = transform.Find ("buttons/" + BTN_HELP);
			Transform btnCamera = transform.Find ("buttons/" + BTN_CAMERA);
			
			btnUndo.gameObject.SetActive (false);
			//btnColourPicker.gameObject.SetActive (false);
			btnSave.gameObject.SetActive (false);
			btnTick.gameObject.SetActive (false);
			//btnHelp.gameObject.SetActive (false);
			btnCamera.gameObject.SetActive (false);
			
			if (_mState == STATE_GAME) {
				btnUndo.gameObject.SetActive (true);
				//btnColourPicker.gameObject.SetActive (true);
				btnSave.gameObject.SetActive (true);
				//btnHelp.gameObject.SetActive (true);
			} else if (_mState == STATE_END) {
				btnTick.gameObject.SetActive (true);
				btnCamera.gameObject.SetActive (true);
			}
		}

		private void PositionButtons ()
		{

			Transform btnUndo = transform.Find ("buttons/" + BTN_UNDO);
			//Transform btnColourPicker = transform.Find ("buttons/" + BTN_COLOUR_PICKER);
			Transform btnSave = transform.Find ("buttons/" + BTN_SAVE);
			//Transform btnHelp = transform.Find ("buttons/" + BTN_HELP);

			float sx = btnUndo.position.x;
			float ex = btnSave.position.x;
			float gap = (ex - sx) / 3f;
			float ypos = btnUndo.position.y;
			//btnColourPicker.position = new Vector2 (sx + (1 * gap), ypos);
			//btnHelp.position = new Vector2 (sx + (2 * gap), ypos);
		}
		
		public void SetButtonState (string btnName, bool enabled)
		{
			for (int n=0; n<_buttons.Count; n++) {
				SB_Button btn = (SB_Button)_buttons [n];
				if (btnName == btn.name) {
					btn.isActive = enabled;
					
					// Apply an alpha on the button sprite, if it's not currently enabled
					tk2dSprite btnSprite = btn.transform.Find ("Sprite").GetComponent<tk2dSprite> ();
					Color btnColor = btnSprite.color;
					if (enabled)
						btnColor.a = 1.0f;
					else
						btnColor.a = 0.3f;
					btnSprite.color = btnColor;
					
					break;
				}
			}
		}
		
		public void SetButtonSelected (string btnName)
		{
			for (int n=0; n<_buttons.Count; n++) {
				SB_Button btn = (SB_Button)_buttons [n];
				if (btnName == btn.name) {
					btn.Selected = true;
					
					tk2dSprite btnSprite = btn.transform.Find ("Sprite").GetComponent<tk2dSprite> ();
					error ("--> btnSprite.CurrentSprite.name: " + btnSprite.CurrentSprite.name);
					
					btnSprite.SetSprite (btnSprite.CurrentSprite.name.Replace ("_off", "_on"));
					break;
				}
			}
		}
		
		public void DeselectAll ()
		{
			for (int n=0; n<_buttons.Count; n++) {
				SB_Button btn = (SB_Button)_buttons [n];
				btn.Selected = false;
					
				tk2dSprite btnSprite = btn.transform.Find ("Sprite").GetComponent<tk2dSprite> ();
				btnSprite.SetSprite (btnSprite.CurrentSprite.name.Replace ("_on", "_off"));
			}
		}
		
		override protected void OnBtnHandler (SB_Button btn, string eventType)
		{	
			if (!_enabled)
				return;

			base.OnBtnHandler (btn, eventType);
			
			if (ColouringScene == null) {
				// Stop here, if 'ColouringScene' property has not been set!
				error ("ERROR - ColouringScene is null!");
				return;
			}
			
			// Ignore buttons, if loader is showing
			if(ColouringScene.LoadingIconShowing()) return;

			if (eventType == SB_Button.MOUSE_UP) {
				warn ("--> MOUSE UP " + btn.name);
				PlayButtonPressSound();
				switch (btn.name) {
				case BTN_UNDO:
					DeselectAll ();
					ColouringScene.Undo ();
					break;
				/*
				case BTN_COLOUR_PICKER:
					SetButtonSelected (BTN_COLOUR_PICKER);
					ColouringScene.SetColourPickerState ();
					break;
				*/
				case BTN_SAVE:
					DeselectAll ();
					ColouringScene.ShowEnd ();
					break;
					
				case BTN_TICK:
					SB_Analytics.instance.CompleteTimedEvent(SB_AnalyticEventList.COLOURING_TIME);
					SB_Analytics.instance.GameEvent(SB_AnalyticEventList.MAIN_GAME,SB_AnalyticEventList.GAME_END);

					DeselectAll ();
					ColouringScene.LoadPreviousScene ();
					break;
				/*
				case BTN_HELP:
					DeselectAll ();
					ColouringScene.ShowHelp ();
					break;
				*/
				
				case BTN_CAMERA:
					SB_Analytics.instance.CompleteTimedEvent(SB_AnalyticEventList.COLOURING_TIME);
					SB_Analytics.instance.GameEvent(SB_AnalyticEventList.MAIN_GAME,SB_AnalyticEventList.GAME_END);
					SB_Analytics.instance.GameEvent(SB_AnalyticEventList.MAIN_GAME,SB_AnalyticEventList.COLOURING_SNAPSHOT);

					DeselectAll ();
					ColouringScene.StartSaveImage ();
					break;
				}
			}
		}
		
		public void PulsateButton(string btn)
		{
			Transform btnTrans = transform.Find ("buttons/" + btn);
			if(btn != null)
			{
				Animator btnAnim = btnTrans.GetComponent<Animator>();
				btnAnim.Play("Pulse");
			}
		}
		
		public void StopPulsateButtons()
		{
			for (int n=0; n<_buttons.Count; n++) 
			{
				SB_Button btn = (SB_Button)_buttons [n];
				Animator btnAnim = btn.transform.GetComponent<Animator>();
				btnAnim.Play("Idle");
			}
		}
		
		override protected void OnDestroy ()
		{
			//ColouringScene = null;
			base.OnDestroy ();
		}
	}
}