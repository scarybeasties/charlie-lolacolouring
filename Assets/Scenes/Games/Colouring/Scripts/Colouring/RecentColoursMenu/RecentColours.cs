﻿using System;
using System.Collections;
using System.Xml;
using UnityEngine;
using ScaryBeasties.UI;
using ScaryBeasties.UI.ScrollableMenu;
using ScaryBeasties.Utils;
using ScaryBeasties.Sound;

namespace ScaryBeasties.Games.Colouring
{
	public class RecentColours : MonoBehaviour 
	{
		public const int MAX_ITEMS = 5;
	
		protected SB_ScrollableMenu _menu;
		//protected ColouringMenuItem _selectedMenuItem = null;
		
		protected ArrayList _items = new ArrayList();
		protected SB_Bounds _bounds;
		
		public Action<RecentColoursMenuItem> OnReleaseCallback;
		
		void Start()
		{
			GetMenu();
			_menu.OnReleaseCallback = HandleOnMenuItemReleased;	
			
			_bounds = transform.Find("Bounds").GetComponent<SB_Bounds>();
			
			StartCoroutine(WaitThenInit());
		}
		
		SB_ScrollableMenu GetMenu()
		{
			if(_menu == null)
			{
				_menu = transform.Find("Menu").GetComponent<SB_ScrollableMenu>();
			}
			
			return _menu;
		}
		
		IEnumerator WaitThenInit()
		{
			yield return new WaitForEndOfFrame();
			
			// Add some default colours to the menu
			XmlNodeList palettesList = SB_Globals.CONFIG_XML.GetNodeList("config/palettes/default");
			foreach(XmlElement node in palettesList)
			{
				string colours = node.SelectSingleNode("colours").InnerText;
				string[] coloursArr = colours.Split(',');
				
				for(int i=0; i<coloursArr.Length; i++)
				{
					Color colour = SB_Utils.HexToColour(coloursArr[i]);
					
					Debug.Log("----- ["+i+"] Adding new colour: " + colour);
					Add(colour);
				}
			}
			
			yield return new WaitForEndOfFrame();
			StartCoroutine(WaitThenSelectDefaultColour());
		}
		
		public void Enable()
		{
			Debug.LogWarning("----ENABLE RECENT COLOURS MENU");
			GetMenu().Enable();
		}
		
		public void Disable()
		{
			Debug.LogWarning("----DISABLE RECENT COLOURS MENU");
			GetMenu().Disable();
		}
		
		void Update()
		{
			/*
			Log("--------------------------");
			Log("_menu.ContentHolderLeft: " + _menu.ContentHolderLeft + ", ContentHolderWidth: " + _menu.ContentHolderWidth + ", x: " + _menu.ContentHolderRectTransform.position.x);
			Log("_bounds.Left: " + _bounds.Left + ", _bounds.Right: " + _bounds.Right + ", ScrollRect.horizontalNormalizedPosition: " + _menu.ScrollRect.horizontalNormalizedPosition);
			Log("--------------------------");
			*/
		}
								
		public RecentColoursMenuItem Add(Color colour)
		{
			Vector2 lastContentPos = _menu.ContentHolderRectTransform.position;
		
			_menu.RemoveAllItems();
			
			if(_items.Contains(colour))
			{
				// Colour is already in array, so remove it and re-add it at the zero index
				_items.Remove(colour);
			}
			else if(_items.Count >= MAX_ITEMS)
			{
				_items.RemoveAt(MAX_ITEMS-1);
			}
			
			_items.Insert(0, colour);
			
			RecentColoursMenuItem btn = null;
			float btnHeight = 0f;
			float btnWidth = 0f;
			
			for(int n=0; n<_items.Count; n++)
			{
				Color clr = (Color)_items[n];
				RecentColoursMenuItemProperties props = new RecentColoursMenuItemProperties{colour = clr};
				
				GameObject btnTemplate = transform.Find("ScrollableMenuItem").gameObject;
				GameObject newBtn = (GameObject)Instantiate(btnTemplate);
				RecentColoursMenuItem newBtnItem = newBtn.transform.GetComponent<RecentColoursMenuItem>();
				
				newBtnItem.Init(props);
				_menu.AddItem(newBtnItem);
				
				if(btn == null)
				{
					btn = newBtnItem;
				}
				
				btnHeight = btn.Height;
				btnWidth = btn.Width;
			}
			
			if(_menu.ScrollRect.vertical) lastContentPos = new Vector2(lastContentPos.x, lastContentPos.y+btnHeight);
			if(_menu.ScrollRect.horizontal) lastContentPos = new Vector2(lastContentPos.x+btnWidth, lastContentPos.y);
			
			_menu.ContentHolderRectTransform.position = lastContentPos;
			
			//_menu.ScrollToPosition(SB_ScrollableMenu.MENU_POSITION_LEFT, SB_ScrollableMenu.MENU_POSITION_LEFT);
			//_menu.ScrollToNormalisedPosition(SB_ScrollableMenu.MENU_POSITION_LEFT);
			_menu.ScrollToNormalisedPosition(SB_ScrollableMenu.MENU_POSITION_TOP);
			
			return btn;
		}
		
		public void DeselectAll()
		{
			for(int n=0; n<_menu.Items.Count; n++)
			{
				RecentColoursMenuItem item = (RecentColoursMenuItem)_menu.Items[n];
				item.Selected = false;
			}
		}
		
		public RecentColoursMenuItem SelectItemAt(int n)
		{
			if(_menu != null && n < _menu.Items.Count)
			{
				RecentColoursMenuItem item = (RecentColoursMenuItem)_menu.Items[n];
				item.Selected = true;
			
				if(OnReleaseCallback != null) OnReleaseCallback(item);
				return item;
			}
			
			return null;
		}
		
		protected virtual RecentColoursMenuItem CreateMenuItem(RecentColoursMenuItemProperties props)
		{
			GameObject btnTemplate = transform.Find("ScrollableMenuItem").gameObject;
			GameObject newBtn = (GameObject)Instantiate(btnTemplate);
			RecentColoursMenuItem newBtnItem = newBtn.transform.GetComponent<RecentColoursMenuItem>();
			
			newBtnItem.Init(props);
			_menu.AddItem(newBtnItem);
			
			// Make sure we are at the top of the list
			_menu.SetContentsNormalisedPosition(SB_ScrollableMenu.MENU_POSITION_TOP);
			
			return newBtnItem;
		}
		
		protected virtual void HandleOnMenuItemReleased (SB_ScrollableMenuItem menuItem)
		{
			DeselectAll();
			
			RecentColoursMenuItem item = (RecentColoursMenuItem)menuItem;
			item.Selected = true;
			
			//Vector2 targetPos = (Vector2)_menu.ScrollRect.transform.InverseTransformPoint(_menu.ContentHolderRectTransform.position) - (Vector2)_menu.ScrollRect.transform.InverseTransformPoint(item.GetComponent<RectTransform>().position);
			Vector2 targetPos = (Vector2)item.GetComponent<RectTransform>().position;
			//_menu.HorizontalScrollTo(targetPos.x);
			
			Vector2 worldTargetPos = Camera.main.ScreenToWorldPoint(targetPos);
			
			float margin = 20f;
			float moveAmount = 0f;
			if(_menu.ScrollRect.horizontal)
			{
				moveAmount = _bounds.Width*0.3f;
				if(targetPos.x < _bounds.Left+margin)
				{
					float targ = _menu.ContentHolderRectTransform.anchoredPosition.x + moveAmount;
					_menu.HorizontalScrollTo(targ);
				}
				else if(targetPos.x > _bounds.Right-margin)
				{
					float targ = _menu.ContentHolderRectTransform.anchoredPosition.x - moveAmount;
					_menu.HorizontalScrollTo(targ);
				}
			}
			else if(_menu.ScrollRect.vertical)
			{
				moveAmount = _bounds.Height*0.3f;
				float targ = 0f;
				if(targetPos.y < _bounds.Bottom+margin)
				{
					targ = _menu.ContentHolderRectTransform.anchoredPosition.y + moveAmount;
					targ += item.Height;
					_menu.VerticalScrollTo(targ);
				}
				else if(targetPos.y > _bounds.Top-margin)
				{
					targ = _menu.ContentHolderRectTransform.anchoredPosition.y - moveAmount;
					targ -= item.Height;
					_menu.VerticalScrollTo(targ);
				}
			}
			
			SB_SoundManager.instance.PlaySound (SB_Globals.MAIN_BUTTON_SOUND2, 0.5f, SB_Globals.MAIN_BUTTON_SOUND_CHANNEL);
			if(OnReleaseCallback != null) OnReleaseCallback(item);
		}
		
		public Color GetColourAt(int n)
		{
			if(_items != null && n < _items.Count) 
			{
				return (Color)_items[n];
			}
			
			return Color.white;
		}
		
		protected void Log(string msg) 
		{
			Debug.LogWarning("[" + this.GetType() + "] " + msg);
		}
		
		public void FromXMLString(string str)
		{
			//Debug.Log("----- FromXMLString str: " + str);
			
			string[] coloursArr = str.Split(',');
			//Debug.Log("----- coloursArr.Length: " + coloursArr.Length);
			
			//for(int n=0; n<coloursArr.Length; n++)
			for(int n=coloursArr.Length-1; n>-1; n--)
			{
				Color colour = SB_Utils.HexToColour((string)coloursArr[n]);
				Debug.Log("----- ["+n+"] Adding new colour: " + colour);
				Add(colour);
			}
			
			StartCoroutine(WaitThenSelectDefaultColour());
		}
		
		IEnumerator WaitThenSelectDefaultColour()
		{
			yield return new WaitForEndOfFrame();
			SelectItemAt(0);
			/*
			for(int n=0; n<_menu.Items.Count; n++)
			{
				Log("---> ["+n+"] " + SelectItemAt(n).Colour);
			}
			*/
			StopCoroutine(WaitThenSelectDefaultColour());
		}
		
		public string ToXMLString()
		{
			string xml = "<colours>";
			
			if(_items != null &&_items.Count > 0) 
			{
				for(int n=0; n<_items.Count; n++)
				{
					Color clr = (Color)_items[n];
					
					if(n>0) xml += ",";
					
					xml += SB_Utils.ColourToHex(clr);
				}
			}
			
			xml += "</colours>";
			
			return xml;
		}
		
		protected virtual void OnDestroy()
		{
			_menu.OnReleaseCallback = null;	
			_menu = null;
			
			//_selectedMenuItem = null;
		}
	}
}