﻿using UnityEngine;

namespace ScaryBeasties.Games.Colouring
{
	public class RecentColoursMenuItemProperties
	{
		public string id = "";
		public Color colour;
		public bool selected = false;
		public System.Object data=null;
	}
}