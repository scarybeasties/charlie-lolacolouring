﻿using UnityEngine;
using UnityEngine.UI;
using ScaryBeasties.UI.ScrollableMenu;

namespace ScaryBeasties.Games.Colouring
{
	[RequireComponent(typeof(Image))]
	[RequireComponent(typeof(LayoutElement))]
	public class RecentColoursMenuItem : SB_ScrollableMenuItem
	{
		private RecentColoursMenuItemProperties _props = null;
		
		private Color32 _colour;
		
		private Image _onImg;
		private Image _offImg;
		
		private bool _ready = false;
		
		public RecentColoursMenuItemProperties Props { get { return _props; } }
		
		override protected void Start ()
		{
			base.Start ();
			
			_onImg = transform.Find ("On").GetComponent<Image> ();
			_offImg = transform.Find ("Off").GetComponent<Image> ();
			
			_ready = true;
			
			if (_props != null) {
				Init (_props);
			}
		}
		
		public void Init (RecentColoursMenuItemProperties props)
		{
			_props = props;
			
			// Stop here if this Object has not yet been fully constructed.
			// Once Start is called, this Init function will be called again
			if (!_ready)
				return;
			
			//if(_props.imgPath.Length > 0) LoadImage(_props.imgPath);
			
			id = _props.id;
			data = _props.data;
			
			SetColour (_props.colour);
			Selected = _props.selected;
			
			ToggleSelectedImg ();
		}
		
		public Color32 Colour {
			get { return _colour;}
		}
		
		public void SetColour (Color32 colour)
		{
			_colour = colour;
			
			_onImg.color = colour;
			_offImg.color = colour;
		}
		
		public bool Selected {
			set {
				_props.selected = value;
				ToggleSelectedImg ();
			}
			
			get {
				return _props.selected;
			}
		}
		
		private void ToggleSelectedImg ()
		{
			if (_onImg != null && _onImg.gameObject != null) {
				_onImg.gameObject.SetActive (_props.selected);
			}
			
			if (_offImg != null && _offImg.gameObject != null) {
				_offImg.gameObject.SetActive (!_props.selected);
			}
		}
		
		override protected void OnDestroy ()
		{
			_onImg = null;
			_offImg = null;
			
			base.OnDestroy ();
		}
	}
}