﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Colouring.Junior;
using ScaryBeasties.UI;
using System.Collections.Generic;
using ScaryBeasties.Popups;
using System.Xml;
using ScaryBeasties.Base;
using ScaryBeasties.Sound;
using ScaryBeasties.Data;

namespace ScaryBeasties.Colouring
{
	public class JuniorColouringScene : ColouringScene
	{
		private bool lowMem = false;
		private JuniorTools _juniorTools;
		private int _currentTool = -1;
		
		private bool _firstTimePatternSelected = true;

		override protected void Start ()
		{
			
			if (Screen.width < 980)
				lowMem = true;
			if (SystemInfo.graphicsMemorySize <= 128)
				lowMem = true;
			if (tk2dSystem.CurrentPlatform == "1x")
				lowMem = true;
			
			#if UNITY_EDITOR
			lowMem = false; // // TESTING!!
			#endif
			
			_juniorTools = transform.Find ("Game/JuniorTools").GetComponent<JuniorTools> ();
			_juniorTools.OnToolSelected = HandleOnToolSelected;
			_juniorTools.OnToolReleased = HandleOnToolReleased;
			
			base.Start ();
			_uiControls.ShowButtons (new List<string> (new string[] { SB_UIControls.BTN_MENU, SB_UIControls.BTN_PAUSE }));
			
			// Set a default selected tool 
			//_juniorTools.SelectedTool = ToolType.TOOL_FLOOD_FILL;
			_juniorTools.SelectedTool = ToolType.TOOL_PAINTBRUSH;

			if (lowMem) {
				_colourController.SetLowMem ();
			}

			log ("lowMem: " + lowMem);
			log ("_juniorTools.NumTools: " + _juniorTools.NumTools);
		}

		override protected void Init ()
		{
			SB_SoundManager.instance.PlayNewMusic ("Audio/Music/Games/DRAWING", 0.5f);
			PlayIntroVO ();
			base.Init ();
		}

		private void EnableControls (bool val)
		{
			_colourController.EnableColouring (val);
			_juniorTools.Paused = !val;
			_uiControls.ButtonsEnabled (val);
		}

		override protected void ShowLoader (bool val)
		{
			base.ShowLoader (val);
			EnableControls (!val);
		}

		private void HandleOnToolSelected (int tool)
		{
			log ("HandleOnToolSelected " + tool + ", lowMem: " + lowMem);
			
			Tool selectedTool = _juniorTools.GetToolByType ((EnumToolType)tool);
			LogButtonEvent ("ToolSelected_" + selectedTool.ToolOptions.ToolType);
			//log("---> " + selectedTool.ToolOptions.ToolType + ", name: " + System.Enum.GetName(typeof(EnumToolType), selectedTool.ToolOptions.ToolType));
			
			// Disable the _colourController, while the tool is selected
			_colourController.EnableColouring (false);
			//_colourController.SetToolType(tool, selectedTool.FlowRate, selectedTool.Alpha);
			_colourController.SetSelectedTool (selectedTool);
			
			Texture2D tex = selectedTool.HighResTexture;
			//if(lowMem) tex = selectedTool.LowResTexture;
			
			if (tex != null) {
				if (tool == ToolType.TOOL_ERASER) {
					_colourController.SetTexture (tex, true);
				} else {
					// Single texture
					_colourController.SetTexture (tex);
				}
			} else {
				if (tool == ToolType.TOOL_GLITTER) {
					// Array of textures
					GlitterTool glitterTool = (GlitterTool)_juniorTools.GetTool (tool);
					_colourController.SetTexture (glitterTool.Textures);
				} else if (tool == ToolType.TOOL_SPRAY_PAINT) {
					_colourController.SetNoTexture ();
				} else {
					Debug.LogWarning ("Texture is null for tool type " + tool);
				}
			}
			
			Debug.Log ("------------------_currentTool: " + _currentTool + ", tool: " + tool + ", _colourMenu.IsShowing: " + _colourMenu.IsShowing);
			/*
			if(tool != ToolType.TOOL_ERASER && !_colourMenu.IsShowing)
			{
				//_juniorTools.Paused = true;
				_colourMenu.Show();
				_colourMenu.HideOnPressOutsideBounds = false;
				//_colourMenu.ShowIdleOn();
			}
			*/
			
			if (tool == ToolType.TOOL_ERASER || tool == ToolType.TOOL_SMUDGE) {
				if (_colourMenu.IsShowing) {
					_colourMenu.Hide ();
				}
			} else if (tool == ToolType.TOOL_PATTERN) {
				if (!_colourMenu.IsShowing) {
					// Make sure the menu is on the correct pageset, for the chosen tool
					_colourMenu.SetCurrentPage (ColourMenu.PAGESET_PATTERNS);
					_colourMenu.Show ();
				}
				
				// If this is the first time the user has selected the Pattern tool, make sure 
				// that a default pattern texture is loaded (in case user closes the pattern menu 
				// without selecting anything)
				if (_firstTimePatternSelected) {
					HandlePatternChanged (1);
					_firstTimePatternSelected = false;
				}
			} else if (_currentTool == tool) {
				// Make sure the menu is on the correct pageset, for the chosen tool
				_colourMenu.SetCurrentPage (ColourMenu.PAGESET_SOLID_COLOURS);
				_colourMenu.ToggleMenu ();
			}
			
			ResetIdle ();
			
			
			/*
			// OLD CODE - Shows the colour picker every time a new tool is selected
			if(tool != ToolType.TOOL_ERASER)
			{
				if(_currentTool != tool)
				{
					if(!_colourMenu.IsShowing)
					{
						Debug.Log("About to show menu");
						_colourMenu.Show();
						//_colourMenu.HideOnPressOutsideBounds = false;
					}
				}
				else
				{
					Debug.Log("About to toggle menu");
					_colourMenu.ToggleMenu();
				}
			}
			*/
			
			_currentTool = tool;
		}

		override protected void ToggleColourMenu ()
		{
			_colourMenu.SetCurrentPage (ColourMenu.PAGESET_SOLID_COLOURS);
			_colourMenu.ToggleMenu ();
		}

		override protected void HandlePatternChanged (int patternId)
		{
			PatternTool patternTool = (PatternTool)_juniorTools.GetToolByType (EnumToolType.PATTERN);
			string id = "";
			
			// Apply pattern properties from config XML
			XmlNodeList patternsList = SB_Globals.CONFIG_XML.GetNodeList ("config/patterns/pattern");
			foreach (XmlElement node in patternsList) {
				id = node.GetAttribute ("id");
				float flowRate = float.Parse (node.GetAttribute ("flowRate"));
				string rotateOnDraw = node.GetAttribute ("rotateOnDraw").ToLower ();
				int rotateAfterPxMoved = int.Parse (node.GetAttribute ("rotateAfterPxMoved"));
				string interpolateRotation = node.GetAttribute ("interpolateRotation").ToLower ();
				int numSlices = int.Parse (node.GetAttribute ("numSlices"));
				
				string pId = "pattern" + patternId;
				if (id == pId) {
					Debug.Log ("<color=green>----XML id: " + id + ", flowRate: " + flowRate + ", rotateOnDraw: " + rotateOnDraw + ", rotateAfterPxMoved: " + rotateAfterPxMoved + ", interpolateRotation: " + interpolateRotation + ", numSlices: " + numSlices + "</color>");
					
					patternTool.ToolOptions.FlowRate = flowRate;
					patternTool.ToolOptions.RotateAfterPxMoved = rotateAfterPxMoved;
					
					patternTool.ToolOptions.RotateOnDraw = false;
					if (rotateOnDraw == "yes" || rotateOnDraw == "true") {
						patternTool.ToolOptions.RotateOnDraw = true;
					}
					
					patternTool.ToolOptions.InterpolateRotation = false;
					if (interpolateRotation == "yes" || interpolateRotation == "true") {
						patternTool.ToolOptions.InterpolateRotation = true;
					}

					patternTool.ToolOptions.NumSlices = numSlices;
				}
			}
			
			if (id.Length > 0) {
				Dictionary<string, string> trackingParams = GetSceneTrackingParams ();
				trackingParams.Add ("Pattern", id);
				LogSceneEvent ("PatternSelected", trackingParams);
			}
			
			patternTool.SetPattern (patternId);
			patternTool.OnPatternLoaded = HandleOnPatternLoaded;
		}

		void HandleOnPatternLoaded (PatternTool patternTool)
		{
			patternTool.OnPatternLoaded = null;
			Texture2D tex = patternTool.HighResTexture;
			if (lowMem)
				tex = patternTool.LowResTexture;
			
			log ("<color=green>HandleOnPatternLoaded() TEXURE LOADED w: " + tex.width + ", h: " + tex.height + " - lowMem: " + lowMem + "</color>");
			log ("<color=green>patternTool.HighResTexture w: " + patternTool.HighResTexture.width + ", h: " + patternTool.HighResTexture.height + "</color>");
			log ("<color=green>patternTool.LowResTexture w: " + patternTool.LowResTexture.width + ", h: " + patternTool.LowResTexture.height + "</color>");
			
			_colourController.SetSelectedTool (patternTool);
			_colourController.SetTexture (tex);
		}

		override protected void HandleMenuShowing (bool menuIsShowing)
		{
			base.HandleMenuShowing (menuIsShowing);
			EnableControls (!menuIsShowing);
		}

		private void HandleOnToolReleased ()
		{
			Debug.LogWarning ("HandleOnToolReleased");
			EnableControls (true);
		}

		override protected void SetColour (Color32 col)
		{
			base.SetColour (col);
			_juniorTools.SetToolColour (0, col);
		}

		override public void ShowPopup (int popupType, SB_PopupVO popupVO = null)
		{
			base.ShowPopup (popupType, popupVO);
			
			//_colourController.EnableColouring(false);
			//_juniorTools.Paused = true;
			EnableControls (false);
			
			if (_colourMenu.IsShowing) {
				_colourMenu.Disable ();
			}
		}

		/** Called once a popup has been closed */
		override protected void PopupClosed ()
		{
			base.PopupClosed ();
			
			if (!_popupManager.PopupIsShowing) {
				if (_colourMenu.IsShowing) {
					_colourMenu.Enable ();
				} else {
					//_colourController.EnableColouring(true);
					//_juniorTools.Paused = false;
					EnableControls (true);
				}
			}
		}

		#region AUDIO

		private void PlayIntroVO ()
		{
			SB_SoundManager.instance.PlayVoiceQueued ("Localised/VO/Charlie/vo_colouring_things");
			SB_SoundManager.instance.PlayVoiceQueued ("Localised/VO/Charlie/vo_colouring_finger");
		}
		
		/*
		protected void PlayTickVO()
		{
			SB_SoundManager.instance.PlayVoiceIfNotPlaying("Localised/VO/Narrator/Colouring/vo_drawing_in_tick_2");
			_colouringBottomMenu.PulsateButton(ColouringBottomMenu.BTN_SAVE);
		}
		*/
		
		string[] _drawingVOs = new string[5] {
			"Localised/VO/Lola/vo_colouring_glitter",		// <-- Keep Glitter at index 0 !!!
			"Localised/VO/Charlie/vo_animal_in_rub",			// <-- Keep Rubber at index 1 !!!
			"Localised/VO/Charlie/vo_profile_tapthetick",			// <-- Keep Tick at index 2 !!!
			"Localised/VO/Lola/vo_colouring_socolourful",
			"Localised/VO/Lola/vo_animal_in_think"
		};
		private int _drawingVOIdx = -1;

		protected virtual void PlayDrawingVO ()
		{
			// Pick an initial value, at random
			if (_drawingVOIdx == -1) {
				_drawingVOIdx = Random.Range (0, _drawingVOs.Length);
			}
			
			string vo = _drawingVOs [_drawingVOIdx];
			
			// Make sure we don't play the same sound twice in a row!
			// (This could potentially happen right after the intro 'vo_drawing_in_finger_1' voice has played)
			if (SB_SoundManager.instance.LastVOPlayed == vo) {
				_drawingVOIdx++;
				if (_drawingVOIdx >= _drawingVOs.Length) {
					_drawingVOIdx = 0;
				}
				vo = _drawingVOs [_drawingVOIdx];
			}
			
			log ("PlayEncouragementVO - " + vo);
			SB_SoundManager.instance.PlayVoice (vo);
			
			switch (_drawingVOIdx) {
			case 0:
				_juniorTools.PulsateButton (EnumToolType.GLITTER);
				break;
			case 1:
				_juniorTools.PulsateButton (EnumToolType.ERASER);
				break;
			case 2:
				_colouringBottomMenu.PulsateButton (ColouringBottomMenu.BTN_SAVE);
				break;
			}
			
			_drawingVOIdx++;
			if (_drawingVOIdx >= _drawingVOs.Length) {
				_drawingVOIdx = 0;
			}
		}

		override protected void PlayIdleVO ()
		{
			ResetIdle ();
			
			if (_colourMenu.IsShowing) {
				_colourMenu.PlayIdleVO ();
			} else {
				PlayDrawingVO ();
			}
		}

		string[] _encouragementVOs = new string[3] {
			"Localised/VO/Charlie/vo_colouring_greatpicture",
			"Localised/VO/Lola/vo_colouring_ooh1",
			"Localised/VO/Lola/vo_gen_lolahappy_2"
		};
		private int _encouragementVOIdx = -1;

		override protected void PlayEncouragementVO ()
		{
			// Pick an initial value, at random
			if (_encouragementVOIdx == -1) {
				_encouragementVOIdx = Random.Range (0, _encouragementVOs.Length);
			}
			
			string vo = _encouragementVOs [_encouragementVOIdx];
			log ("PlayEncouragementVO - " + vo);
			
			SB_SoundManager.instance.PlayVoiceIfNotPlaying (vo);
			
			_encouragementVOIdx++;
			if (_encouragementVOIdx >= _encouragementVOs.Length) {
				_encouragementVOIdx = 0;
			}
		}

		string[] _endVOs = new string[3] {
			"Localised/VO/Charlie/vo_colouring_back",					// <-- Keep BackToMenu at index 0 !!!
			"Localised/VO/Charlie/vo_profile_tapthetick",				// <-- Keep Tick at index 1 !!!
			"Localised/VO/Charlie/vo_colouring_photo"	// <-- Keep Camera at index 2 !!!
		};
		private int _endVOIdx = -1;
		
		
		string[] _endCongratsVOs = new string[4] {
			"Localised/VO/Lola/vo_colouring_amazing",
			"Localised/VO/Lola/vo_colouring_lovely",
			"Localised/VO/Lola/vo_colouring_sogood",
			"Localised/VO/Lola/vo_colouring_best"
		};
		private int _endCongratsIdx = -1;

		
		override protected void PlayEndVO ()
		{
			SB_SoundManager.instance.ClearVOQueue ();
			
			_endCongratsIdx = Random.Range (0, _endCongratsVOs.Length);
			
			string vo = _endCongratsVOs [_endCongratsIdx];
			
			SB_SoundManager.instance.OnVoiceChanged = HandleOnVoiceChanged;
			
			SB_SoundManager.instance.PlayVoice (vo);
			SB_SoundManager.instance.PlayVoiceQueued (_endVOs [1]);
			SB_SoundManager.instance.PlayVoiceQueued (_endVOs [2]);
			_endVOIdx = 0;
			_idleCount = 10f;
		}

		void HandleOnVoiceChanged (string clipName)
		{
			log ("HandleOnVoiceChanged() clipName: " + clipName);
			if (clipName == _endVOs [1]) {
				PulsateEndButton (1);
			} else if (clipName == _endVOs [2]) {
				PulsateEndButton (2);
			}
		}

		void PulsateEndButton (int n)
		{
			_colouringBottomMenu.StopPulsateButtons ();
			switch (n) {
			case 0:
				transform.Find ("End/ui_back_btn").GetComponent<Animator> ().Play ("Pulse");
				break;
			case 1:
				_colouringBottomMenu.PulsateButton (ColouringBottomMenu.BTN_TICK);
				break;
			case 2:
				_colouringBottomMenu.PulsateButton (ColouringBottomMenu.BTN_CAMERA);
				break;
			}
		}

		override protected void PlayEndIdleVO ()
		{
			ResetIdle ();
			
			// Pick an initial value, at random
			if (_endVOIdx == -1) {
				_endVOIdx = Random.Range (0, _endVOs.Length);
			}
			
			string vo = _endVOs [_endVOIdx];
			
			// Make sure we don't play the same sound twice in a row!
			if (SB_SoundManager.instance.LastVOPlayed == vo) {
				_endVOIdx++;
				if (_endVOIdx >= _endVOs.Length) {
					_endVOIdx = 0;
				}
				vo = _endVOs [_endVOIdx];
			}
			
			log ("PlayEndVO - " + vo);
			
			SB_SoundManager.instance.PlayVoice (vo);
			
			PulsateEndButton (_endVOIdx);
			
			_endVOIdx++;
			if (_endVOIdx >= _endVOs.Length) {
				_endVOIdx = 0;
			}
		}

		#endregion

		override public void LoadScene (string nextSceneToLoad, KeyValueObject nextSceneConfig = null, bool immediate = false)
		{
			SB_SoundManager.instance.ClearVOQueue ();
			base.LoadScene (nextSceneToLoad, nextSceneConfig, immediate);
		}

		override protected void OnDestroy ()
		{
			base.OnDestroy ();
			
			_juniorTools.OnToolSelected = null;
			_juniorTools = null;
		}
	}
}