﻿using UnityEngine;
using System.Collections;
using System;
using ScaryBeasties.UI;
using ScaryBeasties.Utils;
using ScaryBeasties.Colouring.Junior;
using ScaryBeasties.Sound;

namespace ScaryBeasties.Colouring
{

	public class ColouringController : MonoBehaviour
	{
		public const int STATE_DRAWING = 0;
		public const int STATE_COLOUR_PICK = 1;
		private int _state = STATE_DRAWING;
		
		private Tool _selectedTool;
		private ToolOptions _selectedToolOptions; // Stores the current tool's options (FlowRate, RotateOnDraw, NumSlices, etc...)
		
		public Action<Color32> OnColourPicked;
		public Action OnPictureLoaded;
	
		private bool _enabled = false;
		private Vector2 _mouseDownPos;
		private	Vector2 _prevMousePos;

		public SB_Bounds Bounds;
		public bool ZoomEnabled = true;
		public bool DragEnabled = true;

		//Colouring 
		private ColouringArea _colourArea;
		private Color32 _currentColor;

		//Dragging
		private GameObject _dragArea;
		private bool _dragging = false;
		private bool _movedBeyondMinimumDistance = false;
		private Vector2 _dragStartPos;
		private Vector2 _dragStartOffset;

		private const int MINIMUM_DRAG_DISTANCE = 20;

		//Zooming
		private GameObject _zoomArea;
		private bool _zooming = false;
		private Vector2 _zoomMidpoint;
		private float _zoomStartScale = 0.0f;
		private float _zoomStartDist = 0.0f;
		private float _scale = 1.0f;
		private float _minScale = 0.0f;
		private float _maxScale = 12.0f;

		//Tapping
		private const float TAP_SPEED = 0.2f;
		private const float TAP_DIST = 10.0f;
		private float _startTapTime = 0.0f;

		//Bounds
		private Vector2 _picSize;

		public bool Paused = false;
		private ArrayList _colourHistory2 = new ArrayList ();
		private const int MAX_HISTORY = 50;
		private const float MAX_HISTORY_MEGABYTES = 2.5f; // 2.5MB should allow for a maximum of 10 images, at 512x512 size.
		// If the byte array data for each image contains few colours & variations, each image will be less than it's max of 262144 bytes.
		// This means we can potentially store up to the 'MAX_HISTORY' value, if each image has few bytes.
		
		private bool _resetDrawing = false;
		
		private Texture2D _drawTexture;
		private int _txmin, _tymin;
		private bool _erasing = false;
		private bool _drawing = false;
		private bool _lowMem = false;
		//private float _drawSpeed = 0.05f;
		//private float _drawCount=0.0f;
		//private int _lx, _ly;
		//private ArrayList _drawPoints = new ArrayList();
//		private ArrayList _drawPointsQueue = new ArrayList();
		
		private Vector2 _dragAreaPos;
		private Vector2 _zoomAreaScale;
		private float _zoomAreaScaleX, _zoomAreaScaleY;
		
//		private float _glitterCount=0.0f;
//		private float _glitterRate = 0.01f; //0.02f; //0.05f;
//		private float _sprayPaintRate = 0.005f; //0.02f;
		
		
		public bool IsDrawing {
			get{ return _drawing;}
		}	

		public void SetLowMem ()
		{
			_lowMem = true;
			//_drawSpeed /= 3.0f;
		}
		
		// Use this for initialization
		void Start ()
		{
			//_lx=-100000;
			
			_colourArea = transform.Find ("ZoomArea/DragableArea/ColouringArea").gameObject.transform.GetComponent<ColouringArea> ();
			_dragArea = transform.Find ("ZoomArea/DragableArea").gameObject;
			_zoomArea = transform.Find ("ZoomArea").gameObject;
			
			SetColourAreaToolOptions ();
			
			// 'Bounds' property should be linked on the inspector.
			// Do this as a fallback!
			if (Bounds == null) {
				Transform bnds = transform.Find ("ColouringBounds");
				if (bnds != null) {
					Bounds = bnds.GetComponent<SB_Bounds> ();
				}
			}
			
			AddListeners ();
		}
		
		public ColouringArea ColouringArea {
			get { return _colourArea; }
		}
		
		public ArrayList ColourHistory {
			get { return _colourHistory2; }
		}
		
		public void SetState (int state)
		{
			_state = state;
		}
		
		public void SetSelectedTool (Tool tool)
		{
			Debug.Log ("<color=orange>SetSelectedTool " + tool.ToolOptions.ToolType + "</color>");
			
			// Nullify the previous tool's texture refernces
			if (_selectedTool != null) {
				_selectedTool.Nullify ();
			}
		
			_selectedTool = tool;
			if (_colourArea != null) {
				_colourArea.SetDrawingTextureColours (_colourArea.GetDrawingTexture.GetPixels ());
				_selectedTool.InitTool (_colourArea.GetDrawingTexture, _lowMem);
			}
			
			_selectedToolOptions = tool.ToolOptions;
			
			if (_selectedToolOptions.FlowRate < 1f)
				_selectedToolOptions.FlowRate = 1f;
			
			if (_selectedToolOptions.Alpha > byte.MaxValue)
				_selectedToolOptions.Alpha = byte.MaxValue;
			if (_selectedToolOptions.Alpha <= byte.MinValue)
				_selectedToolOptions.Alpha = byte.MinValue + (byte)1; // Not sure why anyone would ever set the value to zero, but might as well put in a check
			
			if (_selectedToolOptions.RotateAfterPxMoved < 1)
				_selectedToolOptions.RotateAfterPxMoved = 1;

			//if(_selectedToolOptions.NumSlices <= 1) _selectedToolOptions.NumSlices = 0;
			
			Debug.Log ("[ColouringController] SetSelectedTool <color=red> tool.ToolOptions: " + tool.ToolOptions.ToString () + "</color>");
			Debug.Log ("[ColouringController] SetSelectedTool <color=green> _selectedToolOptions: " + _selectedToolOptions.ToString () + "</color>");

			SetColourAreaToolOptions ();
		}
		
		void SetColourAreaToolOptions ()
		{
			if (_selectedToolOptions == null)
				Debug.LogError ("_selectedToolOptions is NULL");
			if (_colourArea == null)
				Debug.LogError ("_colourArea is NULL");
			
			if (_colourArea != null && _selectedToolOptions != null) {
				//_colourArea.SetToolOptions(_selectedToolOptions);
				
				if (_selectedTool != null && _colourArea.GetDrawingTexture != null) {
					_selectedTool.InitTool (_colourArea.GetDrawingTexture, _lowMem);
				}
			}
		}
		
		public void SetTexture (Texture2D tex, bool erase=false)
		{
			Debug.LogWarning ("(1) SetTexture() - name: " + tex.name + ", tex.width: " + tex.width + ", tx.height: " + tex.height);
			
			_drawTexture = tex;
			SetBrush (_currentColor, _drawTexture);
			
			_erasing = erase;
		}
		
		// Array of textures (Glitter)
		public void SetTexture (Texture2D[] tex)
		{
			Debug.LogWarning ("(2) SetTexture() (Multiple)");
			//_colourArea.SetGlitter(tex);
			//_colourArea.ColourGlitter(_currentColor);
			
			// This is temporary..
			//GlitterTool gTool = (GlitterTool)(_selectedTool);
			//gTool.SetGlitter(tex);
			//gTool.ColourGlitter(_currentColor);

			if (tex [0] == null) {
				_txmin = 0;
				_tymin = 0;
				return;
			}
			
			_txmin = 0 - (tex [0].width / 2);
			_tymin = 0 - (tex [0].height / 2);
		}

		public void SetNoTexture ()
		{
			_txmin = 0;
			_tymin = 0;
		}
		
		public void SetBrush (Color32 col, Texture2D tx)
		{
			if (tx == null) {
				_txmin = 0;
				_tymin = 0;
				return;
			}
			
			_txmin = 0 - (tx.width / 2);
			_tymin = 0 - (tx.height / 2);
		}
		
		public float GetScaleMultiplier ()
		{
			return 1f / _zoomArea.transform.localScale.x;
		}

		public void LoadImage (string folder, string colouringImage, bool resetDrawing=false)
		{
			Debug.LogError ("[ColouringController] -----> LoadImage folder: " + folder + ", image: " + colouringImage + ", _resetDrawing: " + _resetDrawing);
			_resetDrawing = resetDrawing;
			
			Debug.Log ("<color=orange>..about to set _colourArea</color>");
			_colourArea = transform.Find ("ZoomArea/DragableArea/ColouringArea").gameObject.transform.GetComponent<ColouringArea> ();
			_colourArea.LoadImage (folder, colouringImage);
			SetColourAreaToolOptions ();
		}
		
		public void SaveImage (string imageId, string creationId)
		{
			_colourArea.SaveImage (imageId, creationId);
		}

		private void AddListeners ()
		{
			_colourArea.OnLoaded = PictureLoaded;
		}
		
		private void RemoveListeners ()
		{
			_colourArea.OnLoaded = null;
		}
		
		private void PictureLoaded ()
		{
			_picSize = new Vector2 (_colourArea.GetWidth (), _colourArea.GetHeight ());
			SetInitialScale ();
			_enabled = true;
			
			_dragAreaPos = new Vector2 (_dragArea.transform.position.x, _dragArea.transform.position.y);
			_zoomAreaScale = new Vector2 (_zoomArea.transform.localScale.x, _zoomArea.transform.localScale.y);
			_zoomAreaScaleX = 1 / _zoomAreaScale.x;
			_zoomAreaScaleY = 1 / _zoomAreaScale.y;
			
			Transform frameTrans = _zoomArea.transform.Find ("Frame");
			float scaler = 1f;
			if (tk2dSystem.CurrentPlatform == "1x")
				scaler = 2f;
			Vector2 newScale = frameTrans.localScale;
			newScale.x *= scaler;
			newScale.y *= scaler;
			frameTrans.localScale = newScale;
			frameTrans.gameObject.SetActive (true);	
			
			if (_resetDrawing) {
				ResetDrawing ();
				_resetDrawing = false;
			}

			AddDrawingPixelsToHistory ();
			SetColourAreaToolOptions ();

			if (OnPictureLoaded != null)
				OnPictureLoaded ();
		}
		
		public void ResetDrawing ()
		{
			_colourHistory2.Clear ();
			_colourArea.ResetDrawing ();
			
			// Make sure that the current tool has its drawing texture reference reset too.
			if (_selectedTool != null && _colourArea != null) {
				_selectedTool.InitTool (_colourArea.GetDrawingTexture, _lowMem);
			}
		}
		
		public void EnableColouring (bool val)
		{
			_enabled = val;
		}

		public void SetColouringColour (Color32 col)
		{
			Debug.Log ("SetColouringColour " + col);
			_currentColor = col;
		}

		private void SetInitialScale ()
		{
			float scx = Bounds.Width / _colourArea.GetWidth ();
			float scy = Bounds.Height / _colourArea.GetHeight ();
			
			bool scaleToFitAvailableHeight = true; //false;//true; // <-- Set to 'false', to scale to the width
			
			if (scx < scy) {
				if (scaleToFitAvailableHeight)
					_scale = scx;
				else
					_scale = scy;
			} else {
				if (scaleToFitAvailableHeight)
					_scale = scy;
				else
					_scale = scx;
			}

			ScalePicture ();

		}

		private void ScalePicture ()
		{
			_zoomArea.gameObject.transform.localScale = new Vector2 (_scale, _scale);
			_minScale = _scale;
		}
		
		private void StartZoom ()
		{

			if (UnityEngine.Input.touchCount == 2) {
				float dx = UnityEngine.Input.GetTouch (0).position.x - UnityEngine.Input.GetTouch (1).position.x;
				float dy = UnityEngine.Input.GetTouch (0).position.y - UnityEngine.Input.GetTouch (1).position.y;
				_zoomStartDist = Mathf.Sqrt ((dx * dx) + (dy * dy));
				_zoomStartScale = _zoomArea.transform.localScale.x;


				_zoomMidpoint = new Vector2 (((UnityEngine.Input.GetTouch (0).position.x + UnityEngine.Input.GetTouch (1).position.x) / 2), ((UnityEngine.Input.GetTouch (0).position.y + UnityEngine.Input.GetTouch (1).position.y) / 2));
				_zoomMidpoint = Camera.main.ScreenToWorldPoint (_zoomMidpoint);

				Vector2 oldPos = _zoomArea.transform.position;
				_zoomArea.transform.position = new Vector2 (0 + _zoomMidpoint.x, 0 + _zoomMidpoint.y);
				Vector2 posDiff = new Vector2 (_zoomArea.transform.position.x - oldPos.x, _zoomArea.transform.position.y - oldPos.y);
				_dragArea.transform.position = new Vector2 (_dragArea.transform.position.x - posDiff.x, _dragArea.transform.position.y - posDiff.y);


				_zooming = true;
			}
		}

		private void StopZoom ()
		{

			Vector2 oldPos = _zoomArea.transform.position;
			_zoomArea.transform.position = new Vector2 (0, 0);
			Vector2 posDiff = new Vector2 (_zoomArea.transform.position.x - oldPos.x, _zoomArea.transform.position.y - oldPos.y);
			_dragArea.transform.position = new Vector2 (_dragArea.transform.position.x - posDiff.x, _dragArea.transform.position.y - posDiff.y);

			_zooming = false;
		}
		
		private void StartDrag ()
		{
			if (!DragEnabled)
				return;

			Vector2 mpos = Camera.main.ScreenToWorldPoint (UnityEngine.Input.GetTouch (0).position);
			
			_dragStartPos = new Vector2 (mpos.x, mpos.y);
			_dragStartOffset = new Vector2 (_dragArea.transform.position.x, _dragArea.transform.position.y);
			_dragging = true;

			_startTapTime = Time.time;

		}

		private void StopDrag ()
		{

			_dragging = false;
			_movedBeyondMinimumDistance = false;
		}

		private void StopAllTouch ()
		{
			StopZoom ();
		}

		private void AddDrawingPixelsToHistory ()
		{
			if (_colourHistory2.Count > MAX_HISTORY) {
				_colourHistory2.RemoveAt (0);
			}
		
			byte[] bytes = _colourArea.GetDrawingBytes;
			_colourHistory2.Add (bytes);
			Debug.Log ("AddDrawingPixelsToHistory() - _colourHistory2 " + _colourHistory2.Count + ", bytes: " + bytes.Length);
			
			//Debug.LogWarning("---- HISTORY BYTES TOTAL: " + GetHistorySizeMB() + " MB");
			
			while (GetHistorySizeMB() > MAX_HISTORY_MEGABYTES) {
				Debug.LogWarning ("HISTORY SIZE EXCEEDS LIMIT - REMOVING ITEM");
				_colourHistory2.RemoveAt (0);
			}
		}
		
		private float GetHistorySizeMB ()
		{
			int bytesTotal = 0;
			for (int n=0; n<_colourHistory2.Count; n++) {
				byte[] b = (byte[])_colourHistory2 [n];
				bytesTotal += b.Length;
			}
			float megaBytesTotal = (bytesTotal / 1024f) / 1024f;
			
			return megaBytesTotal;
		}
		
		void Update ()
		{

		
			if (UnityEngine.Input.GetMouseButtonUp (0)) {
				// Don't add FloodFill tool to history yet - this is done after the flood fill is applied
				if (_drawing && _selectedToolOptions.ToolType != EnumToolType.FLOOD_FILL) {
					AddDrawingPixelsToHistory ();
				} else if (_enabled && _selectedToolOptions.ToolType == EnumToolType.FLOOD_FILL) {
					CheckIfPictureTapped (Camera.main.ScreenToWorldPoint (UnityEngine.Input.mousePosition));
				}
			
				//_drawPoints.Clear();
				_drawing = false;
				//_lx=-100000;
				
				_selectedTool.StopDrawing ();
			}
			
			if (Paused)
				return;
			
			if (_enabled) {
				if (_drawing) {
					//#if UNITY_EDITOR
					Vector2 mpos = Camera.main.ScreenToWorldPoint (UnityEngine.Input.mousePosition);
					/*
					#else
					Vector2 mpos = Camera.main.ScreenToWorldPoint(UnityEngine.Input.GetTouch(0).position);
					#endif
					*/
					mpos = new Vector2 (mpos.x - _dragAreaPos.x, mpos.y - _dragAreaPos.y);
					mpos = new Vector2 (mpos.x * _zoomAreaScaleX, mpos.y * _zoomAreaScaleY);
					//Vector2 mposs = new Vector2(mpos.x, mpos.y);
					//Debug.LogWarning("mousepos: " + mousepos + ", mpos: " + mpos + ", mposs: " + mposs);
					
					Vector2 mousepos = _colourArea.GetTrueMousePos (mpos);
					//Debug.LogWarning("-- trueMousePos: " + trueMousePos + ", _txmin: " + _txmin + ", _tymin: " + _tymin);
					
					//Vector2 mousepos = trueMousePos;
					
					mousepos.x = (int)mousepos.x + _txmin;
					mousepos.y = (int)mousepos.y + _tymin;
					
					_selectedTool.Draw (mousepos);
				} else {
					//#if UNITY_EDITOR
					if (UnityEngine.Input.GetMouseButtonDown (0)) {
						Vector2 mpos2 = Camera.main.ScreenToWorldPoint (UnityEngine.Input.mousePosition);
						_mouseDownPos = mpos2;
						if (Bounds.Contains (mpos2)) {
							_drawing = true;
						}
					}
					/*#else
					if(UnityEngine.Input.touchCount>0){
						Vector2 mpos2 = Camera.main.ScreenToWorldPoint(UnityEngine.Input.GetTouch(0).position);
						_mouseDownPos = mpos2;
						if(Bounds.Contains(mpos2))
						{
							_drawing=true;
						}
					}
					#endif*/
				}
			}
		}
		
		
		private void CheckIfPictureTapped (Vector2 mpos)
		{
			if (_selectedToolOptions.ToolType != EnumToolType.FLOOD_FILL) {
				return;
			}
			
			float time = Time.time - _startTapTime;
			if (Bounds != null) {
				if (Bounds.Contains (mpos)) {
					float dist = SB_Utils.GetDistance (mpos, _dragStartPos);
					Debug.LogWarning ("Mouse in bounds - time: " + time + ", TAP_SPEED: " + TAP_SPEED + ", dist: " + dist + " TAP_DIST: " + TAP_DIST);
					
					bool doTapPicture = false;
					
					if (DragEnabled && ((time < TAP_SPEED) && (dist < TAP_DIST))) {
						doTapPicture = true;
					} else if (!DragEnabled) {
						doTapPicture = true;
					}
					
					if (doTapPicture) {
						TapPicture (new Vector2 (mpos.x, mpos.y));
					}
				} else {
					Debug.LogWarning ("Mouse not in bounds");
				}
			} else {
				Debug.LogWarning ("Bounds is null");
			}
		}

		private void TapPicture (Vector2 mpos)
		{
			mpos = new Vector2 (mpos.x - _dragArea.transform.position.x, mpos.y - _dragArea.transform.position.y);
			mpos = new Vector2 (mpos.x / _zoomArea.transform.localScale.x, mpos.y / _zoomArea.transform.localScale.x);
			Vector2 tappos = new Vector2 (mpos.x, mpos.y); 
			
			Debug.Log ("------> TapPicture " + tappos);		
			_colourArea.Colour (_currentColor, tappos);
			
			if (_selectedToolOptions.ToolType == EnumToolType.FLOOD_FILL) {
				AddDrawingPixelsToHistory ();
			}
		}
		
		public void Undo ()
		{
			Debug.Log ("--- UNDO _colourHistory2.Count: " + _colourHistory2.Count);
			if (_colourHistory2.Count > 1) {
				Debug.Log ("1a) About to undo... _colourHistory2.Count: " + _colourHistory2.Count);
				
				int lastItemIndex = _colourHistory2.Count - 1;
				//if(lastItemIndex <= 0) lastItemIndex = 0;
				
				int prevHistoryIdx = lastItemIndex - 1;
				if (prevHistoryIdx >= 0) {
					byte[] bytes = (byte[])_colourHistory2 [prevHistoryIdx];
					Debug.Log ("bytes.Length: " + bytes.Length);
					_colourArea.SetBytes (bytes);
				}
				
				_colourHistory2.RemoveAt (lastItemIndex);
				
				Debug.Log ("1b) _colourHistory2.Count: " + _colourHistory2.Count);
			} else if (_colourHistory2.Count == 1) {
				byte[] bytes = (byte[])_colourHistory2 [0];
				Debug.Log ("bytes.Length: " + bytes.Length);
				_colourArea.SetBytes (bytes);
			} else {
				Debug.Log ("2) NOT UNDOING! _colourHistory2.Count: " + _colourHistory2.Count);
			}
			
			if (_selectedTool != null) {
				_selectedTool.SetDrawingTextureColours ();
			}
		}

		private void CheckPictureBounds ()
		{
			Vector2 picpos = _colourArea.gameObject.transform.position;
			float psx = (_colourArea.gameObject.transform.position.x - ((_picSize.x / 2.0f) * _zoomArea.transform.localScale.x));
			float pex = (_colourArea.gameObject.transform.position.x + ((_picSize.x / 2.0f) * _zoomArea.transform.localScale.x));

			float psy = (_colourArea.gameObject.transform.position.y - ((_picSize.y / 2.0f) * _zoomArea.transform.localScale.x));
			float pey = (_colourArea.gameObject.transform.position.y + ((_picSize.y / 2.0f) * _zoomArea.transform.localScale.x));

			float xdif = 0.0f;
			float ydif = 0.0f;
			
			if (psx > Bounds.Left) {
				xdif = psx - Bounds.Left;
			} else if (pex < Bounds.Right) {
				xdif = (Bounds.Right - pex) * -1;
			}
			
			if (psy > Bounds.Top) {
				ydif = psy - Bounds.Top;
			} else if (pey < Bounds.Bottom) {
				ydif = (Bounds.Bottom - pey) * -1;
			}
			
			float posX = _dragArea.transform.position.x - xdif;
			float posY = _dragArea.transform.position.y - ydif;

			Debug.LogWarning ("CheckPictureBounds--> posX: " + posX + ", posX: " + posX + ", Bounds: " + Bounds.Area ());
			_dragArea.transform.position = new Vector2 (posX, posY);
		}
		
		/*private bool MouseMoved()
		{
			// Make sure user has moved 
			Vector2 currentMousePos = Camera.main.ScreenToWorldPoint(UnityEngine.Input.mousePosition);
			if(currentMousePos.x != _prevMousePos.x && currentMousePos.y != _prevMousePos.y)
			{
				_prevMousePos = currentMousePos;
				return true;
			}
			
			return false;
		}*/

		public void DoTestFill ()
		{
			_colourArea.Test ();
		}
		
		void OnDestroy ()
		{
			RemoveListeners ();

			OnColourPicked = null;
			OnPictureLoaded = null;
			
			if (_colourHistory2 != null) {
				_colourHistory2.Clear ();
				_colourHistory2 = null;
			}
		}

	}
}
