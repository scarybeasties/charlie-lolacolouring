﻿using UnityEngine;
using System.Collections;
using System;
using ScaryBeasties.Utils;
using ScaryBeasties.Sound;

namespace ScaryBeasties.Colouring.Junior
{
	public class JuniorTools : MonoBehaviour
	{
		private ArrayList _tools = new ArrayList ();
		private int _selectedTool = -1;
		
		private int _pulseNum = -1;
		private float _pulseCount = 0.0f;
		private float _pulseGap = 0.2f;
		
		private Transform _toolsHolder;
		
		public Action<int> OnToolSelected;
		public Action OnToolReleased;
		public bool Paused = false;

		void Awake ()
		{
			_toolsHolder = transform.Find ("Tools");
			
			for (int i = 0; i < _toolsHolder.childCount; i++) {
				Tool t = GetTool (i);
				if (t != null) {
					if (t.GetComponent<Tool> () != null) {
						_tools.Add (t);
						//t.Hide();
					}
				}
			}
			
			_pulseNum = 0;
			StartCoroutine (WaitThenPositionTools ()); 
		}

		void Start ()
		{
			//StartCoroutine(WaitThenPositionTools()); 
		}

		public IEnumerator WaitThenPositionTools ()
		{
			//yield return new WaitForSeconds(0.01f); 
			yield return new WaitForEndOfFrame (); 
			PositionTools ();
		}
		
		// Getter
		public int NumTools {
			get {
				if (_tools != null)
					return _tools.Count;
				
				return 0;
			}
		}

		public int SelectedTool {
			get { return _selectedTool; }
			set { DoSelect (value); }
		}

		public void Update ()
		{
			if (Paused)
				return;
			
			//UpdateToolPulseCounter();
			
			if (UnityEngine.Input.GetMouseButtonDown (0)) {
				Vector3 mousepos = Camera.main.ScreenToWorldPoint (UnityEngine.Input.mousePosition);
				SelectTool (mousepos);
			} else if (UnityEngine.Input.GetMouseButtonUp (0)) {
				ReleaseTool ();
			}
		}
		/*
		public void UpdateToolPulseCounter()
		{
			if (_pulseNum >= 0)
			{
				_pulseCount -= Time.deltaTime;
				if (_pulseCount <= 0)
				{
					if (_pulseNum != _selectedTool)
					{
						Tool t = GetTool(_pulseNum);
						if(t != null)
						{
							t.Pulsate();
						}
					}
					_pulseNum++;
					
					if (_pulseNum >= _tools.Count)
					{
						_pulseNum = -1;
						_pulseCount = 0.0f;
					}
					else
					{
						_pulseCount = _pulseGap;
					}
				}
				
			}
		}
		*/

		/**
		 * The top and bottom tool have tk2d anchors, so they can adapt to different screen sizes.
		 * The tools in between need to be repositioned, accordingly
		*/
		private void PositionTools ()
		{
			// Right hand side tools
			Vector2 top = transform.Find ("Anchors/TR").position;
			Vector2 bottom = transform.Find ("Anchors/BR").position;
			
			float dist = bottom.y - top.y;
			float gap = dist / 5f;
			
			float xPos = top.x;
			GetToolByType (EnumToolType.SMUDGE).transform.position = new Vector2 (xPos, top.y + gap);
			GetToolByType (EnumToolType.PAINTBRUSH).transform.position = new Vector2 (xPos, top.y + gap * 2);
			GetToolByType (EnumToolType.PENCIL).transform.position = new Vector2 (xPos, top.y + gap * 3);
			/*GetTool (ToolType.TOOL_SMUDGE).transform.position = new Vector2 (xPos, top.y + gap);
			GetTool (ToolType.TOOL_PAINTBRUSH).transform.position = new Vector2 (xPos, top.y + gap * 2);
			GetTool (ToolType.TOOL_PENCIL).transform.position = new Vector2 (xPos, top.y + gap * 3);*/
			transform.Find ("ColourMenuBtn").position = new Vector2 (xPos, top.y + gap * 4);
			
			// Left hand side tools
			top = transform.Find ("Anchors/TL").position;
			bottom = transform.Find ("Anchors/BL").position;
			
			dist = bottom.y - top.y;
			gap = dist / 5f;
			xPos = top.x;
			/*GetTool (ToolType.TOOL_ERASER).transform.position = new Vector2 (xPos, top.y + gap);
			GetTool (ToolType.TOOL_SPRAY_PAINT).transform.position = new Vector2 (xPos, top.y + gap * 2);
			GetTool (ToolType.TOOL_PATTERN).transform.position = new Vector2 (xPos, top.y + gap * 3);
			GetTool (ToolType.TOOL_GLITTER).transform.position = new Vector2 (xPos, top.y + gap * 4);*/
			GetToolByType (EnumToolType.ERASER).transform.position = new Vector2 (xPos, top.y + gap);
			GetToolByType (EnumToolType.SPRAY_PAINT).transform.position = new Vector2 (xPos, top.y + gap * 2);
			GetToolByType (EnumToolType.PATTERN).transform.position = new Vector2 (xPos, top.y + gap * 3);
			GetToolByType (EnumToolType.GLITTER).transform.position = new Vector2 (xPos, top.y + gap * 4);
			/*
			for(int i=0; i<transform.childCount; i++)
			{
				Tool t = GetTool(i);
				t.Show();
			}
			*/
		}

		private void SelectTool (Vector3 mpos)
		{
			int toolNum = IsTouchingTool (mpos);
			if (toolNum > -1) {
				PlayButtonPressSound ();
				DoSelect (toolNum);
			}
		}

		private void ReleaseTool ()
		{
			if (OnToolReleased != null) {
				OnToolReleased ();
			}
		}

		public int IsTouchingTool (Vector3 mpos)
		{
			for (int i = 0; i < _tools.Count; i++) {	
				Tool t = (Tool)_tools [i];
				if (t != null) {			
					BoxCollider2D collider = t.GetComponent<BoxCollider2D> ();
					mpos.z = collider.transform.position.z;
					
					//if(collider.bounds.Contains(mpos))
					if (collider.OverlapPoint (mpos)) {
						return (int)t.ToolOptions.ToolType;
					}
				}
			}
		
			return -1;
		}

		void DoSelect (int i)
		{
			if (i != _selectedTool) {
				Log ("DoSelect " + i);
				
				if (_selectedTool >= 0) {
					GetToolByType ((EnumToolType)_selectedTool).Deselect ();
				}
				
				_selectedTool = i;
				GetToolByType ((EnumToolType)_selectedTool).Select ();
			}
			
			if (OnToolSelected != null) {
				OnToolSelected (_selectedTool);
			}
		}

		public Tool GetTool (int i)
		{
			//Transform toolTrans = transform.Find("Tool" + (i+1));
			Transform toolTrans = _toolsHolder.GetChild (i);
			
			if (toolTrans != null) {
				Tool t = toolTrans.GetComponent<Tool> ();
				if (t != null) {
					return t;
				}
			}
			
			return null;
		}

		public Tool GetToolByType (EnumToolType type)
		{
			for (int n = 0; n < NumTools; n++) {
				Tool t = GetTool (n);
				if (t.ToolOptions.ToolType == type) {
					return t;
				}
			}
			
			return null;
		}

		public void SetToolColour (int toolnum, Color colr)
		{
			Log ("SetToolColour [" + toolnum + "] " + colr);
			//GetTool(toolnum).SetColour(colr);
			
			for (int n = 0; n < NumTools; n++) {
				Tool t = GetTool (n);
				t.SetColour (colr);
			}
		}

		public void PulsateButton (EnumToolType toolType)
		{
			Tool t = GetToolByType (toolType);
			if (t != null) {
				t.Pulsate ();
			}
		}

		#region AUDIO

		void PlayButtonPressSound ()
		{
			SB_SoundManager.instance.PlaySound (SB_Globals.MAIN_BUTTON_SOUND2, SB_Globals.MAIN_BUTTON_VOLUME, SB_Globals.MAIN_BUTTON_SOUND_CHANNEL);
		}

		#endregion

		void Log (string msg)
		{			
			Debug.Log ("[" + this.GetType () + "] " + msg);
		}

		void OnDestroy ()
		{
			if (_tools != null) {
				_tools.Clear ();
				_tools = null;
			}
			
			OnToolSelected = null;
			OnToolReleased = null;
		}
	}
}