﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Sound;
using System;

namespace ScaryBeasties.Colouring.Junior
{
	public class Tool : MonoBehaviour
	{
		// The drawing textures for this tool
		public Texture2D HighResTexture;
		public Texture2D LowResTexture;
		
		// The texture which the tool will apply itself to
		protected Texture2D _drawing;
		
		// An array, storing the drawing texture's pixel colours
		protected Color32[] _drawingTextureColours;
		
		protected Color32[,] _originalTex; // TODO: change this from a 2d array, to 1d 
		
		protected int _drawingWidth, _drawingHeight;
		
		protected Color32 _toolColour = new Color32 (byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
		
		protected float _toolAudioCounter = 0f;
		protected float _toolAudioCounterMax = 0.05f;
		protected Vector2 _prevMousePos;
		
		protected const int TOOLS_SFX_CHANNEL = 6;
		protected AudioSource _toolsSFXChannel;
		
		protected Animator _animator;
		
		
		protected bool _initialDraw = false;
		protected Vector2 _lastDrawPoint;
		
		
		const float DRAW_REFRESH_HI_MEM = 0.025f;
		const float DRAW_REFRESH_LOW_MEM = 0.06f;
		const float DRAW_SPEED_HI_MEM = 0.05f;
		const float DRAW_SPEED_LOW_MEM = 0.1f; 
		
		protected float _drawingRefreshCounter = 0f;
		protected float _drawingRefreshCounterMax = DRAW_REFRESH_HI_MEM;
		
		protected float _flowRate = 1f;	// <-- NOTE: This value gets overridden by _toolOptions.FlowRate
		protected float _drawSpeed = DRAW_SPEED_HI_MEM;
		protected float _drawCount = 0.0f;
		protected int _lx, _ly;
		protected ArrayList _drawPoints = new ArrayList ();
		
		[SerializeField]
		protected ToolOptions
			_toolOptions = new ToolOptions ();
		
		// Get/Set ToolOptions
		public ToolOptions ToolOptions {
			get{ return _toolOptions;}
			set {
				_toolOptions = value;
			}
		}
		
		protected virtual void Start ()
		{
			_toolsSFXChannel = SB_SoundManager.instance.GetSoundChannel (TOOLS_SFX_CHANNEL);
		}
		
		public void Hide ()
		{
			transform.Find ("Tool").gameObject.SetActive (false);
		}
		
		public void Show ()
		{
			transform.Find ("Tool").gameObject.SetActive (true);
		}
		
		private Animator GetAnimator ()
		{
			if (_animator == null) {
				_animator = transform.GetComponent<Animator> ();
			}
			
			return _animator;
		}
		
		/**
		 * Set the tool to its 'selected' state
		 */
		public void Select ()
		{
			GetAnimator ().Play ("ToolsOn");
			transform.Find ("Tool/Selected").gameObject.SetActive (true);
		}
		
		/**
		 * Set the tool to its 'deselected' state
		 */
		public void Deselect ()
		{
			GetAnimator ().Play ("ToolsOff");
			transform.Find ("Tool/Selected").gameObject.SetActive (false);
		}
		
		/**
		 * Make the tool pulsate
		 */
		public void Pulsate ()
		{
			GetAnimator ().Play ("ToolPulse");
		}
		
		/**
		 * Set the tool colour
		 */
		public void SetColour (Color32 colr)
		{
			_toolColour = colr;
			
			if (transform.Find ("Tool/White") != null) {
				GameObject toolColourGameObject = transform.Find ("Tool/White").gameObject;
				
				if (toolColourGameObject != null) {
					tk2dSprite sprite = toolColourGameObject.GetComponent<tk2dSprite> ();
					sprite.color = _toolColour;
				}
			}
			
			ApplyColourToToolTexture ();
		}
		
		/**
		 * Apply the current tool colour to the pixels of the tool's texture
		 */
		protected virtual void ApplyColourToToolTexture ()
		{
			// Defined by subclass
		}
		
		/**
		 * Store a reference to the actual drawing, which we will be drawing to.
		 * This needs to be called once, before any calls to Draw()
		 */
		public virtual void InitTool (Texture2D drawing, bool lowMem)
		{
			_drawing = drawing;
			SetDrawingTextureColours ();
			
			_drawingWidth = _drawing.width;
			_drawingHeight = _drawing.height;
			
			_drawingRefreshCounterMax = DRAW_REFRESH_HI_MEM;
			_drawSpeed = DRAW_SPEED_HI_MEM;
			
			log ("InitTool - lowMem: " + lowMem, 4);

			_flowRate = _toolOptions.FlowRate;

			if (lowMem) {
				_drawSpeed = DRAW_SPEED_LOW_MEM;
				_drawingRefreshCounterMax = DRAW_REFRESH_LOW_MEM;
				_flowRate = _toolOptions.FlowRate;
				
				if (_toolOptions.ToolType == EnumToolType.PATTERN) {
					// Do nothing! (..for now)
				} else {
					_flowRate *= 1.25f;	
				}
			}
				
			
			int w = _drawingWidth;
			int h = _drawingHeight;
			_originalTex = new Color32[w, h];
			for (int y = 0; y < h; ++y) {
				for (int x = 0; x < w; ++x) {
					_originalTex [x, y] = _drawing.GetPixel (x, y);
				}
			}
		}
		
		public void SetDrawingTextureColours ()
		{
			_drawingTextureColours = _drawing.GetPixels32 ();
		}
		
		/*
		protected void InterpolateDrawPoints(Vector2 mousepos)
		{
			if(_lx!=-100000) 
			{
				float tx = (float) _lx;
				float ty = (float) _ly;
				float dx = (float) mousepos.x - tx;
				float dy = (float) mousepos.y - ty;
				float dist = Mathf.Sqrt((dx*dx)+(dy*dy));
				
				if(dist>=_flowRate)
				{
					while(dist>=_flowRate) 
					{
						float mx = (float) (dx/dist);
						float my = (float) (dy/dist);
						
						mx *= _flowRate;
						my *= _flowRate;
						
						tx = tx+mx;
						ty = ty+my;
						
						Vector2 v = new Vector2(tx,ty);
						_drawPoints.Add(v);
						//Debug.LogWarning("LINE "+tx+" "+ty+" "+mx+" "+my+" "+dist);
						
						dx = mousepos.x - tx;
						dy = mousepos.y - ty;
						dist = Mathf.Sqrt((dx*dx)+(dy*dy));
					}
					
					_lx = (int) tx;
					_ly = (int) ty;
				}
				else if(_drawPoints.Count == 0)
				{
					_drawPoints.Add(new Vector2(_lx,_ly));
				}
			}
			else
			{
				_lx = (int) mousepos.x;
				_ly = (int) mousepos.y;
				
				StartDrawing(new Vector2(mousepos.x,mousepos.y));
			}
		}
		
		protected virtual void ProcessDrawPoints()
		{
		}
		*/
		
		public virtual void Draw (Vector2 mousepos)
		{
			// Overridden by subclass
			//UpdateAudioCounter();
			

			_toolAudioCounter -= Time.deltaTime;
			if (_toolAudioCounter <= 0 && MouseMoved ()) {
				_toolAudioCounter = _toolAudioCounterMax;
				PlayToolSound ();
			}
			
			if (_lx != -100000) {
				float tx = (float)_lx;
				float ty = (float)_ly;
				float dx = (float)mousepos.x - tx;
				float dy = (float)mousepos.y - ty;
				float dist = Mathf.Sqrt ((dx * dx) + (dy * dy));
				
				//float distMult = 1/dist;
				
				if (dist >= _flowRate) {
					//System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch(); 
					//sw.Start();  
					while (dist>=_flowRate) {
						//float mx = dx * distMult;
						//float my = dy * distMult;
						float mx = (float)(dx / dist);
						float my = (float)(dy / dist);
						
						mx *= _flowRate;
						my *= _flowRate;
						
						tx = tx + mx;
						ty = ty + my;
						
						Vector2 v = new Vector2 (tx, ty);						
						_drawPoints.Add (v);
						//Debug.LogWarning("LINE "+tx+" "+ty+" "+mx+" "+my+" "+dist);
						
						dx = mousepos.x - tx;
						dy = mousepos.y - ty;
						dist = Mathf.Sqrt ((dx * dx) + (dy * dy));
					}
					
					//sw.Stop();  
					//Debug.Log("----------------------- DRAW while loop: "+ sw.ElapsedTicks); 
					
					_lx = (int)tx;
					_ly = (int)ty;
				} else if (_drawPoints.Count == 0) {
					_drawPoints.Add (new Vector2 (_lx, _ly));
				}
			} else {
				_lx = (int)mousepos.x;
				_ly = (int)mousepos.y;
				
				//StartDrawing(new Vector2(mousepos.x,mousepos.y));

				_initialDraw = true;
				
				int mx = (int)mousepos.x;
				int my = (int)mousepos.y;
				_lastDrawPoint = new Vector2 (mx, my);
			}
		}

		public virtual void StartDrawing (Vector2 m)
		{
			_initialDraw = true;
			
			int mx = (int)m.x;
			int my = (int)m.y;
			_lastDrawPoint = new Vector2 (mx, my);
		}
		
		public virtual void StopDrawing ()
		{
			_drawPoints.Clear ();
			_lx = -100000;
		}
		
		/**
		 * Nullify texture references and Color arrays
		 */
		public virtual void Nullify ()
		{
			_originalTex = null;
			_drawing = null;
			_drawingTextureColours = null;
		}
		
		protected virtual void UpdateAudioCounter ()
		{
			_toolAudioCounter -= Time.deltaTime;
			if (_toolAudioCounter <= 0 && MouseMoved ()) {
				_toolAudioCounter = _toolAudioCounterMax;
				PlayToolSound ();
			}
		}
		
		protected virtual void PlayToolSound ()
		{
			// Overridden by subclass
		}
		
		// Checks if user has moved the mouse/finger
		protected bool MouseMoved ()
		{
			Vector2 currentMousePos = Camera.main.ScreenToWorldPoint (UnityEngine.Input.mousePosition);
			if (currentMousePos.x != _prevMousePos.x && currentMousePos.y != _prevMousePos.y) {
				_prevMousePos = currentMousePos;
				return true;
			}
			
			return false;
		}
		
		#region DEBUGGING
		string GetTimeStamp ()
		{
			DateTime now = DateTime.Now;
			
			string hrs = FormatTimeStamp (now.TimeOfDay.Hours.ToString ());
			string mins = FormatTimeStamp (now.TimeOfDay.Minutes.ToString ());
			string secs = FormatTimeStamp (now.TimeOfDay.Seconds.ToString ());
			string ms = FormatTimeStamp (now.TimeOfDay.Milliseconds.ToString ());
			
			return hrs + ":" + mins + ":" + secs + ":" + ms;
		}
		
		string FormatTimeStamp (string str)
		{
			if (str.Length == 1) {
				str = "0" + str;
			}
			
			return str;
		}
		
		protected void log (object msg, int clr=0)
		{
			string logClr = "grey";
			if (clr == 1)
				logClr = "white";
			if (clr == 2)
				logClr = "green";
			else if (clr == 3)
				logClr = "blue";
			else if (clr == 4)
				logClr = "red";
			else if (clr == 5)
				logClr = "purple";
			else if (clr == 6)
				logClr = "cyan";
			
			Debug.Log (GetTimeStamp () + " [" + this.GetType () + "] " + "<color=" + logClr + ">" + msg + "</color>");
		}
		#endregion
		
		protected virtual void OnDestroy ()
		{
			if (HighResTexture != null) {
				HighResTexture = null;
			}
			
			if (LowResTexture != null) {
				LowResTexture = null;
			}
		}
	}
}