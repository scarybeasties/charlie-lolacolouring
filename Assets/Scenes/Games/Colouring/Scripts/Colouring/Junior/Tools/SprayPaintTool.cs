﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Sound;

namespace ScaryBeasties.Colouring.Junior
{
	/**
	 * SprayPaintTool extends GlitterTool
	 *
	 * The SprayPaint effect is similar to the Glitter, in that
	 * several textures are defined for the tool, and one is picked
	 * at random, to be drawn onto the picture.
	 *
	 * 
	 */
	
	public class SprayPaintTool : Tool
	{

		public float DrawRate = 0.01f;
		public int Radius = 20;
		
		private int _flowNum = 80;

		void Start ()
		{
			base.Start ();
			
			//DrawRate = 0.005f;
			//Radius = 5;
		}

		override public void Draw (Vector2 mpos)
		{
			//base.Draw(mpos);
			
			_drawCount -= Time.deltaTime;
			if (_drawCount <= 0.0f) {
				_drawCount = DrawRate;
				
				for (int i=0; i<_flowNum; i++) {
					float angle = Random.Range (0, 360.0f);// * Math.PI * 2;
					float radius = Random.Range (0, Radius);
					int mx = (int)(mpos.x + radius * Mathf.Cos (angle)); 
					int my = (int)(mpos.y + radius * Mathf.Sin (angle)); 

					//float colMult = Random.Range(0.4f,0.8f);


					
					if ((mx >= 0) && (my >= 0) && (mx < _drawingWidth) && (my < _drawingHeight)) {
						int colIdx = mx + (my) * _drawingWidth;
						Color32 tc = _drawingTextureColours [colIdx];
									
						if (//(tc != null) &&
						    (tc.a > byte.MinValue)) {
										
							//Color32 tc2 = new Color32((_toolColour.r+colMult),(_toolColour.g+colMult),(_toolColour.b+colMult),tc2.a);
							Color32 tc2 = new Color32 ((_toolColour.r), (_toolColour.g), (_toolColour.b), byte.MaxValue);
							_drawingTextureColours [colIdx] = tc2;
						}
					}
				}
				
				_drawing.SetPixels32 (_drawingTextureColours);
				_drawing.Apply ();
			}		
		}
		
		override protected void PlayToolSound ()
		{
			if (!_toolsSFXChannel.isPlaying) {
				SB_SoundManager.instance.PlaySound ("Audio/SFX/Games/Colouring/SndSprayPaint5", 0.25f, TOOLS_SFX_CHANNEL);
			}
		}
	}
}