﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Sound;

namespace ScaryBeasties.Colouring.Junior
{
	public class PaintBrushTool : Tool
	{
		protected Color32[,] _origBrushTex;
		protected Color32[,] _brushTex;
		
		protected int _brushW, _brushH;
		
		override protected void Start ()
		{
			base.Start ();
			//ApplyColourToToolTexture();
			
			_brushW = HighResTexture.width;
			_brushH = HighResTexture.height;
			
			_lx = -100000;
		}
		
		override protected void ApplyColourToToolTexture ()
		{
			// TEMP
			Texture2D brushTex = HighResTexture;
			
			Color32[,] drawText = new Color32[_brushW, _brushH];
			
			for (int y = 0; y < _brushH; ++y) {
				for (int x = 0; x < _brushW; ++x) {
					Color32 tmp = brushTex.GetPixel (x, y); // <-- Get from colour array?
					int ta = byte.MaxValue;
					Color32 tc;
					
					if (_toolOptions.ToolType == EnumToolType.PATTERN) {
						// pattern texture (do not apply a colour tint)
						tc = new Color32 (tmp.r, tmp.g, tmp.b, tmp.a);
					} else {
						ta = tmp.b / (byte)(255/(float)_toolOptions.Alpha);
						tc = new Color32 ((byte)_toolColour.r, (byte)_toolColour.g, (byte)_toolColour.b, (byte)ta);
					}
					
					drawText [x, y] = tc;
				}
			}
			
			_origBrushTex = drawText;
			_brushTex = drawText;
		}
		
		override public void Draw (Vector2 mousepos)
		{
			//base.Draw(mousepos);

			if (_lx != -100000) {
				float tx = (float)_lx;
				float ty = (float)_ly;
				float dx = (float)mousepos.x - tx;
				float dy = (float)mousepos.y - ty;
				float dist = Mathf.Sqrt ((dx * dx) + (dy * dy));
				
				if (dist >= _flowRate) {

					while (dist>=_flowRate) {


						tx += (dx / dist) * _flowRate;
						ty += (dy / dist) * _flowRate;
						
						Vector2 v = new Vector2 (tx, ty);						
						_drawPoints.Add (v);

						dx = mousepos.x - tx;
						dy = mousepos.y - ty;
						dist = Mathf.Sqrt ((dx * dx) + (dy * dy));
					}

					
					_lx = (int)tx;
					_ly = (int)ty;
				} else if (_drawPoints.Count == 0) {
					_drawPoints.Add (new Vector2 (_lx, _ly));
				}
			} else {
				_lx = (int)mousepos.x;
				_ly = (int)mousepos.y;
				
				_initialDraw = true;
				
				int mx = (int)mousepos.x;
				int my = (int)mousepos.y;
				_lastDrawPoint = new Vector2 (mx, my);
			}

			
			_drawCount -= Time.deltaTime;
			if (_drawCount <= 0.0f) {
				_drawCount = _drawSpeed;
				
				while (_drawPoints.Count>0) {
					Vector2 m = (Vector2)_drawPoints [0];
					int mx = (int)m.x;
					int my = (int)m.y;
					
					for (int x = 0; x<_brushW; x++) {
						for (int y =0; y<_brushH; y++) {
							int nextX = mx + x;
							int nextY = my + y;
							
							if ((nextX >= 0) && (nextY >= 0) && (nextX < _drawingWidth) && (nextY < _drawingHeight)) {
								Color32 brushColr = _brushTex [x, y];
								
								if (brushColr.a > 0) {
									int colIdx = nextX + (nextY * _drawingWidth);
									Color32 tc = _drawingTextureColours [colIdx];
									
									Color32 tc2 = brushColr;
//									if(_toolOptions.Alpha < 100f)
//									{
//										tc2.a = _toolOptions.Alpha;
//									}


									Color32 tcol;
									
									if (tc2.a < byte.MaxValue) {
										int balpha = (int)brushColr.a;
										if (balpha > _toolOptions.Alpha) {
											balpha = _toolOptions.Alpha;
//											_colouringArea.alphas[colIdx] = _toolOptions.Alpha;
										}
										float alpha = (float)balpha / 255.0f;
										float omin = 1.0f - alpha;
										tcol = new Color32 ((byte)((omin * (float)tc.r) + (alpha * (float)tc2.r)), (byte)((omin * (float)tc.g) + (alpha * (float)tc2.g)), (byte)((omin * (float)tc.b) + (alpha * (float)tc2.b)), (byte)255);
										_drawingTextureColours [colIdx] = tcol;

										//_drawing.SetPixel(nextX,nextY,tcol);
										
									} else {
										_drawingTextureColours [colIdx] = tc2;
										//_drawing.SetPixel(nextX,nextY,tc2);
										
									}
								}
							}
						}
					}
					_drawPoints.RemoveAt (0);
				}

				_drawing.SetPixels32 (_drawingTextureColours);
				_drawing.Apply (false);
			}
			
			

		}
		
		//float _drawingRefreshCounter = 0f;
		//float _drawingRefreshCounterMax = 0.05f;
		
		/*
		override public void Draw(Vector2 mousepos)
		{
			//log ("Draw _drawSpeed: " + _drawSpeed + ", _flowRate: " + _flowRate);
			
			UpdateAudioCounter();
			InterpolateDrawPoints(mousepos);
			
			_drawCount-=Time.deltaTime;
			if(_drawCount<=0.0f)
			{
				_drawCount=_drawSpeed;
				ProcessDrawPoints();
			}
		}
		
		override protected void ProcessDrawPoints()
		{
			while(_drawPoints.Count>0)
			{
				Vector2 m = (Vector2) _drawPoints[0];
				int mx = (int) m.x;
				int my = (int) m.y;
				
				for(int x = 0; x<_brushW; x++)
				{
					for(int y =0;y<_brushH; y++)
					{
						int nextX = mx+x;
						int nextY = my+y;
						
						if((nextX>=0)&&(nextY>=0)&&(nextX<_drawingWidth)&&(nextY<_drawingHeight))
						{
							Color brushColr = (Color)_brushTex[x,y];
							
							if(brushColr.a>0)
							{
								int colIdx = nextX + (nextY*_drawingWidth);
								Color tc = _drawingTextureColours[colIdx];
								
								Color tc2 = brushColr;
								if(_toolOptions.Alpha < 1f)
								{
									tc2.a = _toolOptions.Alpha;
								}
								
								Color tcol;
								
								if(tc2.a<1.0f)
								{
									byte omin = 1.0f-tc2.a;
									tcol = new Color(((omin*tc.r) + (tc2.a*tc2.r)),((omin*tc.g) + (tc2.a*tc2.g)),((omin*tc.b) + (tc2.a*tc2.b)),1.0f);
									_drawingTextureColours[colIdx] = tcol;
									
								}
								else
								{
									_drawingTextureColours[colIdx] = tc2;
									
								}
							}
						}
					}
				}
				_drawPoints.RemoveAt(0);
			}
			
			_drawing.SetPixels(_drawingTextureColours);
			_drawing.Apply();
		}
		*/
		
		override protected void PlayToolSound ()
		{
			if (!_toolsSFXChannel.isPlaying) {
				int rnd = Mathf.RoundToInt (Random.Range (1, 3));
				SB_SoundManager.instance.PlaySound ("Audio/SFX/Games/Colouring/SndBrush" + rnd, 10f, TOOLS_SFX_CHANNEL);
			}
		}
	}
}