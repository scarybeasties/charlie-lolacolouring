﻿using UnityEngine;
using System.Collections;
using System;
using ScaryBeasties.Sound;
using ScaryBeasties.Utils;

namespace ScaryBeasties.Colouring.Junior
{
	public class PatternTool : Tool
	{
		private float _patternSndCounter = 0f;
		private float _patternSndCounterMax = 0.125f;

		private float _maxAngleChange = 4.0f;
		
		protected int _brushW, _brushH;

		//private bool _useSlicedBrushes = false;
		
		private Vector2 _lastRotatedVec;
		private Vector2 _lastSlicePoint;
		private float _brushAngle, _targetAngle = 0;
		
		private ArrayList _originalBrushSlices = new ArrayList ();
		// An array of sliced brush texture (used when drawing patterns)
		private ArrayList _brushSlices = new ArrayList ();
		// An array of sliced brush texture (used when drawing patterns)
		private float[] _sliceAngles;
		private int _brushSliceIdx = 0;
		private int _brushSliceWidth = 0;
		private int _brushSliceHeight = 0;
		//private int NUM_BRUSH_SLICES = 9;
		const float BRUSH_TEXTURE_SAFE_ZONE = 0.7f;
		
		private Color32[,] _origBrushTex;
		//private Color32[,] _brushTex;
		
		private Texture2D _brushTexture;
		
		private tk2dSprite _patternSprite;
		private LoadedTexture _loadedTexture;
		
		public Action<PatternTool> OnPatternLoaded;


		protected override void Start ()
		{
			_toolOptions.ToolType = EnumToolType.PATTERN;
			base.Start ();
		}

		public override void InitTool (Texture2D drawing, bool lowMem)
		{
			base.InitTool (drawing, lowMem);

			if (lowMem) {
				_maxAngleChange = 4.0f;
			} else {
				_maxAngleChange = 4.0f;
			}
		}

		public void SetPattern (int patternId)
		{
			_patternSprite = transform.Find ("Tool/Pattern").GetComponent<tk2dSprite> ();
			_patternSprite.SetSprite ("roller/pattern" + patternId);
			
			_loadedTexture = transform.Find ("Tool/Texture").GetComponent<LoadedTexture> ();
			//_loadedTexture.TextureWrapMode = TextureWrapMode.Repeat;
			
			string path = Application.persistentDataPath + "/assets/patterns/pattern" + patternId + ".png";
			Debug.LogWarning ("PATTERNS PATH: " + path);
			
			_loadedTexture.LoadImage (path, OnTextureLoaded);
		}

		void OnTextureLoaded (bool success)
		{
			Debug.LogWarning ("OnPatternTexLoaded - " + success);
			
			HighResTexture = _loadedTexture.GetTexture ();
			LowResTexture = _loadedTexture.GetTexture ();
			
			if (OnPatternLoaded != null) {
				OnPatternLoaded (this);
			}
			
			ApplyColourToToolTexture ();
		}

		override protected void ApplyColourToToolTexture ()
		{
			if (_loadedTexture == null)
				return;
		
			// TEMP
			Texture2D brushTex = _loadedTexture.GetTexture ();
			_brushTexture = brushTex;
			
			Color32[,] drawText = new Color32[brushTex.width, brushTex.height];
			
			for (int y = 0; y < brushTex.height; ++y) {
				for (int x = 0; x < brushTex.width; ++x) {
					Color32 tmp = brushTex.GetPixel (x, y);
					float ta = byte.MaxValue;
					Color32 tc;
					
					// pattern texture (do not apply a colour tint)
					tc = new Color32 (tmp.r, tmp.g, tmp.b, tmp.a); 
					drawText [x, y] = tc;
				}
			}
			
			_origBrushTex = drawText;
			//	_brushTex = drawText;
			
			_brushW = brushTex.width;
			_brushH = brushTex.height;
			
			
			CreateBrushSlices ();
		}

		private void CreateBrushSlices ()
		{
			_originalBrushSlices = new ArrayList ();
			_brushSlices = new ArrayList ();
			//_useSlicedBrushes = false;
			
			int NUM_BRUSH_SLICES = ToolOptions.NumSlices;

			_sliceAngles = new float[NUM_BRUSH_SLICES];
			
			log ("CreateBrushSlices <color=green>NUM_BRUSH_SLICES: " + NUM_BRUSH_SLICES + "</color>");

			if (_toolOptions.ToolType == EnumToolType.PATTERN && NUM_BRUSH_SLICES == 1) {
				_originalBrushSlices.Add (_origBrushTex);
				_brushSlices.Add (_origBrushTex);
			}
			if (_toolOptions.ToolType == EnumToolType.PATTERN && NUM_BRUSH_SLICES > 1) {
				//_brushSliceWidth = (int)Mathf.Ceil(_brushW/NUM_BRUSH_SLICES);
				//_brushSliceHeight = _brushH;
				//_useSlicedBrushes = true;

				int margin = (int)Mathf.Ceil (((1 - BRUSH_TEXTURE_SAFE_ZONE) * 0.5f) * _brushW);
				
				int tmpTexutreW = _brushW - (margin * 2);
				
				_brushSliceWidth = (int)Mathf.Ceil ((_brushW - 2 * margin) * BRUSH_TEXTURE_SAFE_ZONE / NUM_BRUSH_SLICES);
				int halfBrushWidth = (int)Mathf.Ceil (_brushSliceWidth * 0.5f);
				_brushSliceHeight = _brushH;
				
				
				log ("<color=green>NUM_BRUSH_SLICES: " + NUM_BRUSH_SLICES + ", _brushSliceWidth: " + _brushSliceWidth + ", _brushSliceHeight: " + _brushSliceHeight + ", margin: " + margin + ", tmpTexutreW: " + tmpTexutreW + "</color>");
				
				string msg = "";
				
				for (int n = 0; n < NUM_BRUSH_SLICES; n++) {
					_sliceAngles [n] = 0.0f;
					//int startX = n * _brushSliceWidth;
					//int blockW = _brushSliceWidth;
					
					int startX = margin + (n * _brushSliceWidth);
					int startY = 0; //margin;
					int blockW = _brushSliceWidth;
					
					if (startX + blockW > _brushW) {
						blockW = _brushW - startX;
					}
					
					msg += "SLICE [" + n + "] startX: " + startX + ", blockW: " + blockW + ", _brushSliceHeight: " + _brushSliceHeight + ", TOTAL: " + (blockW * _brushSliceHeight) + "\n";
					
					// OLD CODE
					//Color32[] slicedPixels = _brushTexture.GetPixels(startX, 0, blockW, _brushSliceHeight);
					
					// --- NEW CODE
					/**/
					int w = _brushSliceWidth;
					int h = _brushSliceHeight;
					//Color32[] slicedPixels = new Color32[w*h];
					Color32[,] slicedPixels = new Color32[h, h];
					
					//log ("1) slicedPixels.Length: " + slicedPixels.Length);
					
					//int idx=0;
					for (int y = 0; y < h; ++y) {
						for (int x = 0; x < h; ++x) {
							slicedPixels [x, y] = Color.clear;
							//slicedPixels[idx] = Color.red;
							//idx++;
						}
					}
					
					// Position the slice in the middle
					int centerX = Mathf.RoundToInt (h * 0.5f);
					int sliceXOffset = centerX - Mathf.RoundToInt (blockW * 0.5f);
					
					//int maxX = blockW + halfBrushWidth;
					int maxX = blockW * 2;
					//int maxX = blockW;
					int brushTexMaxX = _origBrushTex.GetLength (0);
					int brushTexMaxY = _origBrushTex.GetLength (1);
					
					log ("--SLICE [" + n + "] _brushSliceHeight: " + _brushSliceHeight + ", blockW: " + blockW + ", centerX: " + centerX + ", sliceXOffset: " + sliceXOffset + ", brushTexMaxX: " + brushTexMaxX + ", startX: " + startX + ", maxX: " + maxX);
					for (int y = 0; y < _brushSliceHeight; ++y) {
						for (int x = 0; x < maxX; ++x) {
							int idxX = startX + x;
							int idxY = startY + y;
							Color32 clr = Color.green;
							if (idxX < brushTexMaxX && idxY < brushTexMaxY) {
								clr = _origBrushTex [startX + x, startY + y];
							}
							slicedPixels [(sliceXOffset + x), y] = clr;
						}
					}
					
					_originalBrushSlices.Add (slicedPixels);
					_brushSlices.Add (slicedPixels);
					
					//log ("SLICE ["+n+"] slicedPixels.Length: " + slicedPixels.Length + ", idx: " + idx);
				}
				
				#if UNITY_EDITOR
				//OutputBrushSlices();
				#endif
			}
		}


		
		override public void StartDrawing (Vector2 m)
		{

			base.StartDrawing (m);
			
			// Wait for user to start moving, before running the draw code loop
			_initialDraw = false;
			_lastSlicePoint = m;
		}

		override public void Draw (Vector2 mousepos)
		{
			if (_lx != -100000) {
				float tx = (float)_lx;
				float ty = (float)_ly;
				float dx = (float)mousepos.x - tx;
				float dy = (float)mousepos.y - ty;
				float dist = Mathf.Sqrt ((dx * dx) + (dy * dy));
				
				if (dist >= _flowRate) {
					
					while (dist >= _flowRate) {
						
						
						tx += (dx / dist) * _flowRate;
						ty += (dy / dist) * _flowRate;
						
						Vector2 v = new Vector2 (tx, ty);						
						_drawPoints.Add (v);
						
						dx = mousepos.x - tx;
						dy = mousepos.y - ty;
						dist = Mathf.Sqrt ((dx * dx) + (dy * dy));
					}
					
					
					_lx = (int)tx;
					_ly = (int)ty;
				} else if (_drawPoints.Count == 0) {
					_drawPoints.Add (new Vector2 (_lx, _ly));
				}
			} else {
				_lx = (int)mousepos.x;
				_ly = (int)mousepos.y;

				if (_toolOptions.InterpolateRotation) {
					_initialDraw = false;
				} else {
					_initialDraw = true;
				}
				
				int mx = (int)mousepos.x;
				int my = (int)mousepos.y;
				_lastDrawPoint = new Vector2 (mx, my);
			}
			
			_drawCount -= Time.deltaTime;
			if (_drawCount <= 0.0f) {
				_drawCount = _drawSpeed;
				
				if (_drawPoints.Count == 1 && !MouseMoved ()) {
					return;
				}
					
				int w = _brushW;
				int h = _brushH;
					
				while (_drawPoints.Count > 0) {
					Vector2 m = (Vector2)_drawPoints [0];
					int mx = (int)m.x;
					int my = (int)m.y;

					_brushSliceIdx = (_brushSliceIdx + 1) % _brushSlices.Count;

					float dx = mx - _lastDrawPoint.x;
					float dy = my - _lastDrawPoint.y;
					float distanceMoved = Mathf.Sqrt ((dx * dx) + (dy * dy));

					if (distanceMoved > _toolOptions.RotateAfterPxMoved) {
						_targetAngle = (float)(Math.Atan2 (dy, dx) * 180 / Math.PI);
						float difference = _targetAngle - _brushAngle;
						while (difference < -180)
							difference += 360;
						while (difference > 180)
							difference -= 360;

						if (!_initialDraw) {

							if (distanceMoved > 10.0f) {
								_initialDraw = true;
								RotateBrush (_targetAngle, true); // <-- Pass 'true', to force rotation of all brush slices, for the initial draw
								_lastDrawPoint = new Vector2 (mx, my);
							}


						} else if (_toolOptions.InterpolateRotation) {
							if (difference > _maxAngleChange) {
								_targetAngle = _brushAngle + _maxAngleChange;
								RotateBrush (_targetAngle);
								_drawPoints.Insert (0, new Vector2 (mx, my));
										
								mx = (int)_lastDrawPoint.x;
								my = (int)_lastDrawPoint.y;
							} else if (difference < 0 - _maxAngleChange) {
								_targetAngle = _brushAngle - _maxAngleChange;
								RotateBrush (_targetAngle);
								_drawPoints.Insert (0, new Vector2 (mx, my));
								mx = (int)_lastDrawPoint.x;
								my = (int)_lastDrawPoint.y;
							} else {
								RotateBrush (_targetAngle);
								_lastDrawPoint = new Vector2 (mx, my);
							}
						} else {
							RotateBrush (_targetAngle);
							_lastDrawPoint = new Vector2 (mx, my);
						}
					}
					if (_initialDraw) {
						Color32[,] brushtex = (Color32[,])_brushSlices [_brushSliceIdx];
						for (int x = 0; x < w; x++) {
							for (int y = 0; y < h; y++) {
								int nextX = mx + x;
								int nextY = my + y;
									
								if ((nextX >= 0) && (nextY >= 0) && (nextX < _drawingWidth) && (nextY < _drawingHeight)) {
										
									Color32 tc2 = brushtex [x, y];
										
									if (tc2.a > 0) {
										int colIdx = nextX + nextY * _drawingWidth;
										Color32 tc = _drawingTextureColours [colIdx];
											

										if (tc.a > byte.MinValue) {
											if (tc2.a < byte.MaxValue) {
												int balpha = (int)tc2.a;
												if (balpha > _toolOptions.Alpha) {
													balpha = _toolOptions.Alpha;
													//_colouringArea.alphas[colIdx] = _toolOptions.Alpha;
												}
												float alpha = (float)balpha / 255.0f;
												float omin = 1.0f - alpha;
												Color32 tcol = new Color32 ((byte)((omin * (float)tc.r) + (alpha * (float)tc2.r)), (byte)((omin * (float)tc.g) + (alpha * (float)tc2.g)), (byte)((omin * (float)tc.b) + (alpha * (float)tc2.b)), 255);
												_drawingTextureColours [colIdx] = tcol;
												
												//_drawing.SetPixel(nextX,nextY,tcol);
		
											} else {
												_drawingTextureColours [nextX + (nextY * _drawingWidth)] = tc2;
											}
										}
									}
								}
							}
						}
					}
				
					_drawPoints.RemoveAt (0);

				}
					
				_drawing.SetPixels32 (_drawingTextureColours);
				_drawing.Apply (false);
			}		
		}


		

		public void RotateBrush (float angle, bool forceRotateAllSlices = false)
		{

			Color32[,] tex;
			_brushAngle = angle;
			if (_sliceAngles [_brushSliceIdx] == angle) {
				return;
			}
			tex = (Color32[,])_originalBrushSlices [_brushSliceIdx];
			_sliceAngles [_brushSliceIdx] = angle;

	
			float radangle = Mathf.Deg2Rad * angle;

			int x = 0;
			int y = 0;
			float sn = Mathf.Sin (radangle);
			float cs = Mathf.Cos (radangle);
			
			int wid = tex.GetLength (0);
			int hgt = tex.GetLength (1);
			Color32[,] texRot = new Color32[wid, hgt];
			
			
			int xc = wid / 2;
			int yc = hgt / 2;
			
			for (int j = 0; j < hgt; j++) {
				for (int i = 0; i < wid; i++) {
					x = (int)(cs * (i - xc) + sn * (j - yc) + xc);
					y = (int)(-sn * (i - xc) + cs * (j - yc) + yc);
					
					if ((x > -1) && (x < wid) && (y > -1) && (y < hgt)) { 
						
						texRot [i, j] = tex [x, y];
						
					}
				}
			}

			_brushSlices [_brushSliceIdx] = texRot;

		}


		
		override protected void UpdateAudioCounter ()
		{
			_toolAudioCounter -= Time.deltaTime;
			if (_toolAudioCounter <= 0 && MouseMoved ()) {
				_toolAudioCounter = _patternSndCounterMax;
				PlayToolSound ();
			}
		}

		override protected void PlayToolSound ()
		{
			/*
			_patternSndCounter -= Time.deltaTime;
			if(_patternSndCounter <= 0f)
			{
				_patternSndCounter = _patternSndCounterMax;
				SB_SoundManager.instance.PlaySound("Audio/SFX/Games/Colouring/SndPaintRoller5" , 1f);
			}
			*/
			
			SB_SoundManager.instance.PlaySound ("Audio/SFX/Games/Colouring/SndPaintRoller5", 1f);
		}

		override protected void OnDestroy ()
		{
			_patternSprite = null;
			_loadedTexture = null;
			OnPatternLoaded = null;
			
			base.OnDestroy ();
		}
	}
}