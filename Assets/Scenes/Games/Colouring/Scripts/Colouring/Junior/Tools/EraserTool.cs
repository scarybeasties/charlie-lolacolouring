﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Sound;

namespace ScaryBeasties.Colouring.Junior
{
	public class EraserTool : Tool
	{
		protected Color32[,] _origBrushTex;
		protected Color32[,] _brushTex;
		
		protected int _brushW, _brushH;

		override protected void Start ()
		{
			base.Start ();
			//ApplyColourToToolTexture();
			_toolOptions.ToolType = EnumToolType.ERASER;
			_brushW = HighResTexture.width;
			_brushH = HighResTexture.height;
			
			_lx = -100000;
		}

		override protected void ApplyColourToToolTexture ()
		{
			// TEMP
			Texture2D brushTex = HighResTexture;
			
			Color32[,] drawText = new Color32[_brushW, _brushH];
			
			for (int y = 0; y < _brushH; ++y) {
				for (int x = 0; x < _brushW; ++x) {
					Color32 tmp = brushTex.GetPixel (x, y); // <-- Get from colour array?
					float ta = 1.0f;
					Color32 tc;
					
					if (_toolOptions.ToolType == EnumToolType.PATTERN) {
						// pattern texture (do not apply a colour tint)
						tc = new Color32 (tmp.r, tmp.g, tmp.b, tmp.a);
					} else {
						ta = tmp.b * 0.4f;
						tc = new Color (_toolColour.r, _toolColour.g, _toolColour.b, ta);
					}
					
					drawText [x, y] = tc;
				}
			}
			
			_origBrushTex = drawText;
			_brushTex = drawText;
		}

		override public void Draw (Vector2 mousepos)
		{
			base.Draw (mousepos);
			
			_drawCount -= Time.deltaTime;
			if (_drawCount <= 0.0f) {
				_drawCount = _drawSpeed;
				
				while (_drawPoints.Count > 0) {
					Vector2 m = (Vector2)_drawPoints [0];
					int mx = (int)m.x;
					int my = (int)m.y;
					
					for (int x = 0; x < _brushW; x++) {
						for (int y = 0; y < _brushH; y++) {
							int nextX = mx + x;
							int nextY = my + y;
							
							if ((nextX >= 0) && (nextY >= 0) && (nextX < _drawingWidth) && (nextY < _drawingHeight)) {
								Color32 brushColr = (Color32)_brushTex [x, y];
								
								if (brushColr.a > 0) {
									int colIdx = nextX + (nextY * _drawingWidth);
									Color32 tc = _drawingTextureColours [colIdx];



									int balpha = (int)brushColr.a;
									if (balpha > _toolOptions.Alpha) {
										balpha = _toolOptions.Alpha;
										//_colouringArea.alphas[colIdx] = _toolOptions.Alpha;
									}
									float alpha = (float)balpha / 255.0f;
									float omin = 1.0f - alpha;




									Color32 tc2 = brushColr;
									if (_toolOptions.Alpha < byte.MaxValue) {
										tc2.a = _toolOptions.Alpha;
									}
									
									Color32 tcol;
									
									//byte omin = (byte)(byte.MaxValue - tc2.a);
									Color32 tc3 = new Color32 (byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
									
									if (tc2.a < 1.0f) {
										tcol = new Color (((omin * tc.r) + (tc2.a * tc3.r)), ((omin * tc.g) + (tc2.a * tc3.g)), ((omin * tc.b) + (tc2.a * tc3.b)), 1.0f);
										_drawingTextureColours [colIdx] = tcol;
									} else {
										_drawingTextureColours [colIdx] = tc3;
									}
								}
							}
						}
					}
					_drawPoints.RemoveAt (0);
				}
				
				_drawing.SetPixels32 (_drawingTextureColours);
				_drawing.Apply ();
			}
		}

		override protected void PlayToolSound ()
		{
			if (!_toolsSFXChannel.isPlaying) {
				SB_SoundManager.instance.PlaySound ("Audio/SFX/Games/Colouring/SndErase", 10f, TOOLS_SFX_CHANNEL);
			}
		}
	}
}