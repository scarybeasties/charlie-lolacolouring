﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Sound;
using System.Collections.Generic;

namespace ScaryBeasties.Colouring.Junior
{
	/**
	 * SprayPaintTool extends GlitterTool
	 *
	 * The SprayPaint effect is similar to the Glitter, in that
	 * several textures are defined for the tool, and one is picked
	 * at random, to be drawn onto the picture.
	 *
	 * 
	 */
	
	public class SmudgeTool : Tool
	{
		
		public float DrawRate = 0.025f;
		public int Radius = 1;
		public float BlendDecay = 0.95f;
		public int BlendStrength = 10;

		private int _flowNum = 20;
		protected int _brushW, _brushH;
		protected Color32[,] _brushTex;
		private Color32 _startColor, _finishColor;
		
		void Start ()
		{
			base.Start ();
			_toolOptions.ToolType = EnumToolType.SMUDGE;
			_brushW = 30;
			_brushH = 30;
			_drawSpeed = DrawRate; 
			Texture2D brushTex = HighResTexture;
			
			Color32[,] drawText = new Color32[_brushW, _brushH];
			
			for (int y = 0; y < _brushH; ++y) {
				for (int x = 0; x < _brushW; ++x) {
					Color32 tmp = brushTex.GetPixel (x, y); // <-- Get from colour array?
					int ta = tmp.b * _toolOptions.Alpha;
					Color32 tc = new Color32 ((byte)1f, (byte)1f, (byte)1f, (byte)ta);
					
					
					drawText [x, y] = tc;
				}
			}

			_brushTex = drawText;
			//DrawRate = 0.005f;
			//Radius = 5;
		}


		override public void Draw (Vector2 mousepos)
		{
			//base.Draw(mousepos);
			if (_lx != -100000) {
				float tx = (float)_lx;
				float ty = (float)_ly;
				float dx = (float)mousepos.x - tx;
				float dy = (float)mousepos.y - ty;
				float dist = Mathf.Sqrt ((dx * dx) + (dy * dy));
				if (dist >= _flowRate) {
					
					while (dist>=_flowRate) {
						
						
						tx += (dx / dist) * _flowRate;
						ty += (dy / dist) * _flowRate;
						
						Vector2 v = new Vector2 (tx, ty);						
						_drawPoints.Add (v);
						
						dx = mousepos.x - tx;
						dy = mousepos.y - ty;
						dist = Mathf.Sqrt ((dx * dx) + (dy * dy));
					}
					
					
					_lx = (int)tx;
					_ly = (int)ty;
				} else if (_drawPoints.Count == 0) {
					_drawPoints.Add (new Vector2 (_lx, _ly));
				}
			} else {
				_lx = (int)mousepos.x;
				_ly = (int)mousepos.y;
				
				_initialDraw = true;

				int mx = (int)mousepos.x;
				int my = (int)mousepos.y;

				int startIdx = mx + (my * _drawingWidth);
				_startColor = _drawingTextureColours [startIdx];

				_lastDrawPoint = new Vector2 (mx, my);
			}

			
			_drawCount -= Time.deltaTime;
			if (_drawCount <= 0.0f) {
				_drawCount = _drawSpeed;

				if (_drawPoints.Count > 1) {
					for (int i = 0; i < _drawPoints.Count-1; i++) {
						BlendPoints ((Vector2)_drawPoints [i], (Vector2)_drawPoints [i + 1]);
					}
					_drawPoints.Clear ();
					_drawing.SetPixels32 (_drawingTextureColours);
					_drawing.Apply (false);
				}
			}
		}

		private bool PixelInBounds (int indexX, int indexY)
		{
			return ((indexX >= 0) && (indexY >= 0) && (indexX < _drawingWidth) && (indexY < _drawingHeight));
		}

		private void BlendPoints (Vector2 m1, Vector2 m2)
		{
			int mx = (int)m1.x;
			int my = (int)m1.y;
			int m2x = (int)m2.x;
			int m2y = (int)m2.y;

			int blendDirX = mx > m2x ? 1 : -1;
			int blendDirY = my > m2y ? 1 : -1;
			Color32 blendCol = Color.white;
			for (int x = 0; x < _brushW; x+= BlendStrength) {
				for (int y = 0; y < _brushH; y+=BlendStrength) {
					int lastX = mx + ((x - (_brushW / 2)) * blendDirX);
					int lastY = my + ((y - (_brushH / 2)) * blendDirY);
					int nextX = m2x + ((x - (_brushW / 2)) * blendDirX);
					int nextY = m2y + ((y - (_brushH / 2)) * blendDirY);
					if (PixelInBounds (nextX, nextY) && PixelInBounds (lastX, lastY)) {
						Color32 brushCol = _brushTex [x, y];
						if (brushCol.a > 0) {
							int lastIdx = (lastX) + (lastY * _drawingWidth);
							int colIdx = nextX + (nextY * _drawingWidth);
							Color32 tc = _drawingTextureColours [colIdx];
							Color32 lc = _drawingTextureColours [lastIdx];
							if (!Color32.Equals (tc, lc)) {
								Color32 tc2 = BlendColours (tc, lc, 0.5f);
								Color32 tcol;
								if (tc2.a < byte.MaxValue) {
									int balpha = (int)brushCol.a;
									if (balpha > _toolOptions.Alpha) {
										balpha = _toolOptions.Alpha;
										//_colouringArea.alphas[colIdx] = _toolOptions.Alpha;
									}
									float alpha = (float)balpha / 255.0f;
									float omin = 1.0f - alpha;
									tcol = new Color32 ((byte)((omin * (float)tc.r) + (alpha * (float)tc2.r)), (byte)((omin * (float)tc.g) + (alpha * (float)tc2.g)), (byte)((omin * (float)tc.b) + (alpha * (float)tc2.b)), 255);
									for (int i = 0; i < BlendStrength; i++) {
										for (int j = 0; j < BlendStrength; j++) {
											int idx = (nextX + (i * blendDirX)) + ((nextY + (j * blendDirY)) * _drawingWidth);
											if (idx >= 0 && idx < _drawingTextureColours.Length) {
												_drawingTextureColours [idx] = tcol;
											}
										}
									}
								} else {
									for (int i = 0; i < BlendStrength; i++) {
										for (int j = 0; j < BlendStrength; j++) {
											int idx = (nextX + (i * blendDirX)) + ((nextY + (j * blendDirY)) * _drawingWidth);
											if (idx >= 0 && idx < _drawingTextureColours.Length) {
												_drawingTextureColours [idx] = tc2;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		private Color32 BlendColours (Color32 c1, Color32 c2, float lerp)
		{
			return Color32.Lerp (c1, c2, lerp);
		}

		private Color32 BlendAllColours (params Color32[] pColours)
		{
			float colCount = pColours.Length;
			Color32 result = new Color32 ((byte)0f, (byte)0f, (byte)0f, (byte)1f);
			for (int i = 0; i < pColours.Length; i++) {			
				result.r += pColours [i].r;
				result.g += pColours [i].g;
				result.b += pColours [i].b;
				result.a += pColours [i].a;
			}
			result = new Color32 ((byte)((float)result.r / colCount), (byte)((float)result.g / colCount), (byte)((float)result.b / colCount), (byte)((float)result.a / colCount));
			return result;
		}
		
		
		
		override protected void PlayToolSound ()
		{
			if (!_toolsSFXChannel.isPlaying) {
				SB_SoundManager.instance.PlaySound ("Audio/SFX/Games/Colouring/SndSprayPaint5", 0.25f, TOOLS_SFX_CHANNEL);
			}
		}

		#region test functions

		private void BlendLine ()
		{
			Vector2 finishVec = (Vector2)_drawPoints [_drawPoints.Count - 1];
			int finishIdx = ((int)finishVec.x) + (((int)finishVec.y) * _drawingWidth);
			_finishColor = _drawingTextureColours [finishIdx];
			float blendFactor = 1;
			for (int idx = 0; idx < _drawPoints.Count - 1; idx++) {
				Vector2 m1 = (Vector2)_drawPoints [idx];
				Vector2 m2 = (Vector2)_drawPoints [idx + 1];
				int mx = (int)m1.x;
				int my = (int)m1.y;
				int m2x = (int)m2.x;
				int m2y = (int)m2.y;
				
				Color32 blendCol = Color.clear;
				for (int x = 0; x < _brushW; x++) {
					for (int y = 0; y < _brushH; y++) {
						int lastX = mx + x;
						int lastY = my + y;
						int nextX = m2x + x;
						int nextY = m2y + y;
						if ((nextX >= 0) && (nextY >= 0) && (nextX < _drawingWidth) && (nextY < _drawingHeight)) {
							int lastIdx = (lastX) + (lastY * _drawingWidth);
							int colIdx = nextX + (nextY * _drawingWidth);
							Color32 tc = _drawingTextureColours [colIdx];
							Color32 tc2 = BlendColours (_startColor, _finishColor, blendFactor);
							//Color32 lc = _drawingTextureColours [lastIdx];
							if (!Color32.Equals (tc, tc2)) {
								//Debug.Log (string.Format ("SMUDGIN AT {0},{1}", nextX, nextY));
								Color32 tcol;
								if (tc2.a < byte.MaxValue) {
									int balpha = (int)_brushTex [x, y].a;
									if (balpha > 0) {
										float alpha = (float)balpha / 255.0f;
										float omin = 1.0f - alpha;
										tcol = new Color32 ((byte)((omin * (float)tc.r) + (alpha * (float)tc2.r)), (byte)((omin * (float)tc.g) + (alpha * (float)tc2.g)), (byte)((omin * (float)tc.b) + (alpha * (float)tc2.b)), 255);
										for (int i = 0; i < BlendStrength; i++) {
											for (int j = 0; j < BlendStrength; j++) {
												int pIdx = (nextX + i) + ((nextY + j) * _drawingWidth);
												_drawingTextureColours [pIdx] = tcol;
											}
										}
										//_drawing.SetPixel (nextX, nextY, tcol);
									} else {
										for (int i = 0; i < BlendStrength; i++) {
											for (int j = 0; j < BlendStrength; j++) {
												int pIdx = (nextX + i) + ((nextY + j) * _drawingWidth);
												_drawingTextureColours [pIdx] = tc2;
												//_drawing.SetPixel (nextX, nextY, tc2);
											}
										}
									}
								}
							}
							
						}
					}
				}
				blendFactor *= BlendDecay;
			}
			
		}

		#endregion
	}
}