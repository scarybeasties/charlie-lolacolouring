﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Sound;

namespace ScaryBeasties.Colouring.Junior
{
	public class PencilTool : PaintBrushTool
	{




		override protected void PlayToolSound()
		{
			if(!_toolsSFXChannel.isPlaying)
			{
				int rnd = Mathf.RoundToInt(UnityEngine.Random.Range(1, 3));
				SB_SoundManager.instance.PlaySound("Audio/SFX/Games/Colouring/SndPencil" + rnd, 10f, TOOLS_SFX_CHANNEL);
			}
		}
	}
}