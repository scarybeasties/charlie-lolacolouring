﻿using UnityEngine;
using System.Collections;

namespace ScaryBeasties.Colouring.Junior
{
	[System.Serializable]
	public class ToolOptions
	{
		public EnumToolType ToolType;
		public byte Alpha = byte.MaxValue;
		public float FlowRate = 1f;
		public bool RotateOnDraw = false;
		public int RotateAfterPxMoved = 30;
		public bool InterpolateRotation = false;
		public int NumSlices = 0;
		
		public ToolOptions ()
		{
		
		}

		public string ToString ()
		{
			string str = "Alpha: " + Alpha;
			str += ", FlowRate: " + FlowRate;
			str += ", RotateOnDraw: " + RotateOnDraw;
			str += ", RotateAfterPxMoved: " + RotateAfterPxMoved;
			str += ", InterpolateRotation: " + InterpolateRotation;
			str += ", NumSlices: " + NumSlices;

			return str;
		}
	}
}