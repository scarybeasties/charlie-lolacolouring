﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Sound;

namespace ScaryBeasties.Colouring.Junior
{
	public class GlitterTool : Tool
	{
		public Texture2D[] Textures;
		
		//private float _drawCount = 0.0f;
		public float DrawRate = 0.01f;
		public int Radius = 90;
		
		private Color32[][,] _glitterTex;
		private Color32[][,] _origGlitterTex;
		private int _twid, _thgt;

		private int _flowNum = 6;
		
		override protected void Start ()
		{
			base.Start ();
		
			_glitterTex = new Color32[Textures.Length][,];
			_origGlitterTex = new Color32[Textures.Length][,];
			
			for (int i=0; i<Textures.Length; i++) {
				Texture2D tex = Textures [i];
				
				_glitterTex [i] = new Color32[tex.width, tex.height];
				_origGlitterTex [i] = new Color32[tex.width, tex.height];
				for (int y = 0; y < tex.height; ++y) {
					for (int x = 0; x < tex.width; ++x) {
						Color32 tmp = tex.GetPixel (x, y);
						
						_glitterTex [i] [x, y] = tmp;
						_origGlitterTex [i] [x, y] = tmp;
						
					}
				}
			}
		}
		
		override protected void ApplyColourToToolTexture ()
		{
			for (int i=0; i<_glitterTex.Length; i++) {			
				int twid = _glitterTex [i].GetLength (0);
				int thgt = _glitterTex [i].GetLength (1);
				for (int x = 0; x<twid; x++) {
					for (int y =0; y<thgt; y++) {
						Color32 tmp = _origGlitterTex [i] [x, y];
						_glitterTex [i] [x, y] = new Color32 (_toolColour.r, _toolColour.g, _toolColour.b, tmp.a);
					}
				}
			}
		}
		
		override public void Draw (Vector2 mpos)
		{
			//base.Draw(mpos);
			
			_drawCount -= Time.deltaTime;
			if (_drawCount <= 0.0f) {
				_drawCount = DrawRate;

				for (int i=0; i<_flowNum; i++) {
					int tg = (int)UnityEngine.Random.Range (0, _glitterTex.GetLength (0));
				
					float angle = Random.Range (0, 360.0f);// * Math.PI * 2;
					float radius = Random.Range (0, Radius);
					int mx = (int)(mpos.x + radius * Mathf.Cos (angle)); 
					int my = (int)(mpos.y + radius * Mathf.Sin (angle)); 
				
					float colMult = Random.Range (0, 0.6f);
				
				
					_twid = _glitterTex [tg].GetLength (0);
					_thgt = _glitterTex [tg].GetLength (1);
				
					for (int x = 0; x<_twid; x++) {
						for (int y =0; y<_thgt; y++) {
							if ((mx + x >= 0) && (my + y >= 0) && (mx + x < _drawingWidth) && (my + y < _drawingHeight)) {
								if (_glitterTex [tg] [x, y].a > 0f) {
									//Color32 tc = texture.GetPixel (mx+x, my+y);
									//Color32 tc = _drawing.GetPixel (mx+x, my+y); // <--- no need to use GetPixel, just lookup in _drawingTextureColours array
								
									int colIdx = mx + x + (my + y) * _drawingWidth;
									Color32 tc = _drawingTextureColours [colIdx];
								
									if (//tc != null &&
										(tc.a > 0)) {

										Color32 tc2 = _glitterTex [tg] [x, y];
										//Debug.LogError("BEFORE "+tc2.r+" "+colMult);
										tc2 = new Color32 ((byte)(tc2.r + colMult), (byte)(tc2.g + colMult), (byte)(tc2.b + colMult), tc2.a);
/*										Color32 tcol;
										if (tc2.a < byte.MaxValue) {
											int balpha = tc2.a;
											if (balpha > _toolOptions.Alpha) {
												balpha = _toolOptions.Alpha;
												//_colouringArea.alphas[colIdx] = _toolOptions.Alpha;
											}
											float alpha = (float)balpha / 255.0f;
											float omin = 1.0f - alpha;
											tcol = new Color32 ((byte)((omin * (float)tc.r) + (alpha * (float)tc2.r)), (byte)((omin * (float)tc.g) + (alpha * (float)tc2.g)), (byte)((omin * (float)tc.b) + (alpha * (float)tc2.b)), 255);
											_drawingTextureColours [colIdx] = tcol;
											
											//_drawing.SetPixel(nextX,nextY,tcol);
											
										} else {
											_drawingTextureColours [colIdx] = tc2;
											//_drawing.SetPixel(nextX,nextY,tc2);
											
										}*/
										if (tc.a > byte.MinValue) {
											float alpha = (float)tc2.a / 255.0f;
											float omin = 1.0f - alpha;
											Color32 tcol = new Color32 ((byte)((omin * (float)tc.r) + (alpha * (float)tc2.r)), (byte)((omin * (float)tc.g) + (alpha * (float)tc2.g)), (byte)((omin * (float)tc.b) + (alpha * (float)tc2.b)), 255);
											_drawingTextureColours [colIdx] = tcol;
												
											//_drawing.SetPixel(nextX,nextY,tcol);
												
										}
									}
									//	_drawingTextureColours [colIdx] = tc2;
								}
							}
						}
					}
				}

				
				_drawing.SetPixels32 (_drawingTextureColours);
				_drawing.Apply ();
			}
		}		

		
		override protected void UpdateAudioCounter ()
		{
			PlayToolSound ();
		}
		
		override protected void PlayToolSound ()
		{
			if (!_toolsSFXChannel.isPlaying) {
				SB_SoundManager.instance.PlaySound ("Audio/SFX/Games/Colouring/SndGlitter", 1f, TOOLS_SFX_CHANNEL);
			}
		}
		
		override protected void OnDestroy ()
		{
			base.OnDestroy ();
			
			foreach (Texture2D tex in Textures) {
				if (tex != null) {
					//Destroy(tex);
				}
			}
			Textures = null;
			
			_glitterTex = null;
			_origGlitterTex = null;
		}
	}
}