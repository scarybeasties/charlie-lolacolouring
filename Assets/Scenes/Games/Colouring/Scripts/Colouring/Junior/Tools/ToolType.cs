﻿using UnityEngine;
using System.Collections;

namespace ScaryBeasties.Colouring.Junior
{
	public class ToolType
	{
		public const int TOOL_GLITTER = 0;
		public const int TOOL_PENCIL = 1;
		public const int TOOL_PAINTBRUSH = 2;
		public const int TOOL_CRAYON = 3;
		public const int TOOL_FELTPEN = 4;
		public const int TOOL_CHALK = 5;
		public const int TOOL_SMUDGE = 6;
		public const int TOOL_SPRAY_PAINT = 7;
		public const int TOOL_PATTERN = 8;
		public const int TOOL_ERASER = 9;
		public const int TOOL_FLOOD_FILL = 10;
	}
	
	public enum EnumToolType
	{
		GLITTER = ToolType.TOOL_GLITTER,
		PENCIL = ToolType.TOOL_PENCIL,
		PAINTBRUSH = ToolType.TOOL_PAINTBRUSH,
		CRAYON = ToolType.TOOL_CRAYON,
		FELTPEN = ToolType.TOOL_FELTPEN,
		CHALK = ToolType.TOOL_CHALK,
		SMUDGE = ToolType.TOOL_SMUDGE,
		SPRAY_PAINT = ToolType.TOOL_SPRAY_PAINT,
		PATTERN = ToolType.TOOL_PATTERN,
		ERASER = ToolType.TOOL_ERASER,
		FLOOD_FILL = ToolType.TOOL_FLOOD_FILL
	}
	;
}