﻿using UnityEngine;
using System.Collections;
using System;

namespace ScaryBeasties.Colouring.Junior
{
	public class JuniorColours : MonoBehaviour
	{
		private Animator _animator;
		public Color32[] Colours;
		private ArrayList _colourHits = new ArrayList ();
		
		private int _numColours = 0;
		private int _selectedColour = 0;
		private int _colourChoice = -1;
		
		public Action<Color32> OnColourSelected;
		public bool Paused = false;
		
		void Awake ()
		{
			GetAnimator ();
			
			_numColours = transform.Find ("Pickers").childCount;
			for (int i =0; i<_numColours; i++) {
				_colourHits.Add (transform.Find ("Pickers").GetChild (i).gameObject);
			}
			
			Colours = new Color32[_numColours];
			
			// TODO: Have this colour list passed in, and not hardcoded!!
			Colours [0] = new Color32 ((byte)157, (byte)114, (byte)70, (byte)255);
			Colours [1] = new Color32 ((byte)255, (byte)209, (byte)65, (byte)255);
			Colours [2] = new Color32 ((byte)88, (byte)204, (byte)236, (byte)255);
			Colours [3] = new Color32 ((byte)17, (byte)17, (byte)17, (byte)255);
			Colours [4] = new Color32 ((byte)255, (byte)173, (byte)218, (byte)255);
			Colours [5] = new Color32 ((byte)228, (byte)0, (byte)0, (byte)255);
			Colours [6] = new Color32 ((byte)58, (byte)78, (byte)132, (byte)255);
			Colours [7] = new Color32 ((byte)164, (byte)226, (byte)90, (byte)255);
			Colours [8] = new Color32 ((byte)199, (byte)96, (byte)117, (byte)255);
		}
		
		Animator GetAnimator ()
		{
			if (_animator == null) {
				_animator = transform.GetComponent<Animator> ();
			}
			
			return _animator;
		}
		
		// Getter
		public int NumColours {
			get {
				return _numColours;
			}
		}
		
		public int SelectedColour {
			get { return _selectedColour; }
		}
		
		public void Update ()
		{
			if (Paused)
				return;
			
			if (UnityEngine.Input.GetMouseButtonDown (0)) {
				Vector3 mousepos = Camera.main.ScreenToWorldPoint (UnityEngine.Input.mousePosition);
				//SelectColour(mousepos);
				_colourChoice = GetColour (mousepos);
			}
			
			if (UnityEngine.Input.GetMouseButtonUp (0)) {
				Vector3 mousepos = Camera.main.ScreenToWorldPoint (UnityEngine.Input.mousePosition);
				//SelectColour(mousepos);
				
				int tcol = GetColour (mousepos);
				if (tcol >= 0) {
					if (tcol == _colourChoice) {
						SetColour (tcol);
					}
				}
				
			}
		}
		
		private int GetColour (Vector3 mpos)
		{		
			bool colourset = false;
			for (int i=0; i<_colourHits.Count; i++) {	
				BoxCollider2D collider = ((GameObject)_colourHits [i]).GetComponent<BoxCollider2D> ();
				mpos.z = collider.transform.position.z;
				
				if (collider.bounds.Contains (mpos)) {
					return i;
				}
			}
			
			return -1;
		}
		
		private void SetColour (int num)
		{		
			/*
			_selectedColour=num;
			_selectedColours[_selectedTool] = num;
			*/
			
			//Hide();
			_colourChoice = -1;
			
			Log ("Colour selected [" + num + "]");
			if (OnColourSelected != null) {
				OnColourSelected (Colours [num]);
			}
		}
		
		public void SetupColourPicker (int tool)
		{
			for (int i=0; i<_colourHits.Count; i++) {			
				GameObject tobj = (GameObject)_colourHits [i];
				tk2dBaseSprite colspr = tobj.transform.Find ("Colour").gameObject.GetComponent<tk2dBaseSprite> ();
				tk2dBaseSprite whitespr = tobj.transform.Find ("White").gameObject.GetComponent<tk2dBaseSprite> ();
				tobj.transform.Find ("Colour").gameObject.SetActive (true);
				
				GameObject colourmid = transform.Find ("Box/Mid").gameObject;
				GameObject colourtop = transform.Find ("Box/Top").gameObject;
				GameObject colourbottom = transform.Find ("Box/Bottom").gameObject;
				
				if (tool == ToolType.TOOL_PENCIL) {
					colspr.SetSprite ("pencil");
					whitespr.SetSprite ("pencil_white");
					
					colourmid.transform.localScale = new Vector2 (1.0f, 0.7f);
					colourtop.transform.position = new Vector2 (0.0f, 470.0f);
					colourbottom.transform.position = new Vector2 (0.0f, 530.0f);
				} else if (tool == ToolType.TOOL_PAINTBRUSH) {
					colspr.SetSprite ("brush");
					whitespr.SetSprite ("brush_white");
					
					colourmid.transform.localScale = new Vector2 (1.0f, 1.0f);
					colourtop.transform.position = new Vector2 (0.0f, 500.0f);
					colourbottom.transform.position = new Vector2 (0.0f, 500.0f);
				} else if (tool == ToolType.TOOL_CRAYON) {
					colspr.SetSprite ("crayon");
					whitespr.SetSprite ("crayon_white");
					colourmid.transform.localScale = new Vector2 (1.0f, 0.5f);
					colourtop.transform.position = new Vector2 (0.0f, 450.0f);
					colourbottom.transform.position = new Vector2 (0.0f, 550.0f);
				} else if (tool == ToolType.TOOL_CHALK) {
					colspr.SetSprite ("chalk");
					whitespr.SetSprite ("chalk");
					tobj.transform.Find ("Colour").gameObject.SetActive (false);
					colourmid.transform.localScale = new Vector2 (1.0f, 0.5f);
					colourtop.transform.position = new Vector2 (0.0f, 450.0f);
					colourbottom.transform.position = new Vector2 (0.0f, 550.0f);
				} else if (tool == ToolType.TOOL_FELTPEN) {
					colspr.SetSprite ("felt");
					whitespr.SetSprite ("felt_white");
					colourmid.transform.localScale = new Vector2 (1.0f, 0.7f);
					colourtop.transform.position = new Vector2 (0.0f, 470.0f);
					colourbottom.transform.position = new Vector2 (0.0f, 530.0f);
				} else if (tool == ToolType.TOOL_GLITTER) {
					colspr.SetSprite ("glitter_stars");
					whitespr.SetSprite ("glitter_stars");
					tobj.transform.Find ("Colour").gameObject.SetActive (false);
					colourmid.transform.localScale = new Vector2 (1.0f, 0.5f);
					colourtop.transform.position = new Vector2 (0.0f, 450.0f);
					colourbottom.transform.position = new Vector2 (0.0f, 550.0f);
				}
				
				whitespr.color = Colours [i];
			}
			
			/*
			// GT 17.06.2015
			for(int i=0;i<_drawings.Count;i++){
				DrawingArea ta = (DrawingArea) _drawings[i];
				ta.SetTool(_selectedTool);
			}
			*/
		}
		
		public void Show ()
		{
			if (GetAnimator () != null) {
				GetAnimator ().Play ("ColoursOn");
			}
		}
		
		public void Hide ()
		{
			if (GetAnimator () != null) {
				GetAnimator ().Play ("ColoursOff");
			}
		}
		
		// Returns true, if the animator is in 'ColoursIdle' state
		public bool IsIdle ()
		{
			if (GetAnimator () != null) {
				return GetAnimator ().GetCurrentAnimatorStateInfo (0).IsName ("ColoursIdle");
			}
			
			return false;
		}
		
		void Log (string msg)
		{			
			Debug.Log ("[" + this.GetType () + "] " + msg);
		}
		
		void OnDestroy ()
		{
			_animator = null;
			
			if (_colourHits != null) {
				_colourHits.Clear ();
				_colourHits = null;
			}
			
			OnColourSelected = null;
		}
	}
}