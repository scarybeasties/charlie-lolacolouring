﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Scenes;
using ScaryBeasties;
using ScaryBeasties.Utils;
using ScaryBeasties.Controls;
using ScaryBeasties.Data;
using ScaryBeasties.Sound;
using ScaryBeasties.Popups;
using ScaryBeasties.Base;
using ScaryBeasties.Games.Colouring;


using System.Collections.Generic;
using ScaryBeasties.UI;
using System.Xml;
using System.IO;
using ScaryBeasties.History;
using UnityEngine.UI;
using ScaryBeasties.Common.Sharing;
using ScaryBeasties.Games.Colouring.Menus;
using ScaryBeasties.Network;

namespace ScaryBeasties.Colouring
{
	public class ColouringScene : SB_BaseScene
	{
		public const int STATE_GAME = 0;
		public const int STATE_END = 1;
		public const int STATE_SAVE_IMAGE = 2;
		public const int STATE_LOAD = 10;

		private int _gState = STATE_GAME;

		private const int AFTER_LOAD_MENU = 0;
		private const int AFTER_LOAD_SHARE = 1;
		private int _afterLoad = 0;
		private float _loadCount = 0;
		private const float LOAD_PAUSE = 0.2f;
		
		protected ColouringController _colourController;
		protected ColourMenu _colourMenu;
		protected ColouringBottomMenu _colouringBottomMenu;

		private LoadedTexture _endPicture;
		private GameObject _endPictureGameObj;
		//private SB_ShareComponent _endShareComponent;

		private RecentColours _recentColours;
		private Color32 _currentColor;
		
		private ColouringGameData _colouringGameData;
		
		private bool _isNew = false;
		private bool _resetDrawing = false;
		private bool _createDuplicateDrawing = false;
		private bool _isEditingInspirationImage = false;
		private int _itemMenuId = 0;
		
		protected bool _pictureLoaded = false;
		
		private XmlDocument _drawingDataXML;
		
		private string _sharingSubject = "";
		private string _sharingEmailBody = "";
		
		Transform _loadingIcon;
		
		override protected void Awake ()
		{
			base.Awake ();
			//_endShareComponent = transform.Find ("End/Share").GetComponent<SB_ShareComponent> ();
			
			bool sharingEnabled = true;
			if (PlayerPrefs.HasKey ("SharingEnabled")) {
				int sharingPref = PlayerPrefs.GetInt ("SharingEnabled");
				sharingEnabled = (sharingPref == 0) ? false : true;
			}
			
			// Ensure user is online and the 'sharing enabled' parents setting is set to true
			if (Application.internetReachability == NetworkReachability.NotReachable) {
				sharingEnabled = false;
			}
			
			if (!sharingEnabled) {
				DisableSharingOptions ();
			}
			
			
			
			
			// GT - 21.07.2016 - No point doing a net connection test here, for this app, as this app does not have any social media sharing
			/*
			// Check for connectivity
			SB_ConnectionChecker.CheckInternetConnection(this, (isConnected)=>
			{
				Debug.Log("<color=orange>isConnected: " + isConnected + "</color>");
				if(!isConnected)
				{
					sharingEnabled = false;
					DisableSharingOptions();
				}
				
				Debug.Log("<color=orange>sharingEnabled: " + sharingEnabled + "</color>");				
			});
			*/
		}
		
		void DisableSharingOptions ()
		{
			// If sharing is disabled, then reset the anchor position to zero
			Transform endPictureHolder = transform.Find ("End/PictureHolder");
			tk2dCameraAnchor endPictureAnchor = endPictureHolder.GetComponent<tk2dCameraAnchor> ();
			endPictureAnchor.AnchorOffsetPixels = Vector2.zero;
		}
		
		protected override void Start ()
		{
			Debug.LogError ("INITII");

			SB_Analytics.instance.StartTimedEvent(SB_AnalyticEventList.COLOURING_TIME);
			SB_Analytics.instance.GameEvent(SB_AnalyticEventList.MAIN_GAME,SB_AnalyticEventList.GAME_START);

			
			// Check if a user creation needs to be reloaded
			if (GameConfig.USER_SCENE_CONFIG_OBJECT != null) {
				SCENE_CONFIG_OBJECT = GameConfig.USER_SCENE_CONFIG_OBJECT;
				GameConfig.USER_SCENE_CONFIG_OBJECT = null;
			}
			
			if (SCENE_CONFIG_OBJECT != null) {
				if (SCENE_CONFIG_OBJECT.GetValue (GameConfig.COLOURING_PAGE) != null) {
					//_startPage =  (int) SCENE_CONFIG_OBJECT.GetValue(GameConfig.COLOURING_PAGE);
				}
			}
			
			base.Start ();
			//_uiControls.ShowButtons(new List<string>(new string[] { SB_UIControls.BTN_BACK, SB_UIControls.BTN_PAUSE}));
			//_uiControls.ShowButtons(new List<string>(new string[] { SB_UIControls.BTN_BACK}));
			_uiControls.ShowButtons (new List<string> (new string[] { })); // No UI Controls back button - use the one in this scene instead

			//_colourPicker = transform.Find("Game/ColourPicker").GetComponent<ColourPicker> ();
			_colourController = transform.Find ("Game/ColouringController").GetComponent<ColouringController> ();
			_colourMenu = transform.Find ("ColourMenu").GetComponent<ColourMenu> ();
			_recentColours = transform.Find ("RecentColours").GetComponent<RecentColours> ();
			_colouringBottomMenu = transform.Find ("ColouringBottomMenu").GetComponent<ColouringBottomMenu> ();
			_colouringBottomMenu.ColouringScene = this;
			
			_endPicture = transform.Find ("End/PictureHolder/Picture").GetComponent<LoadedTexture> ();
			
			_loadingIcon = transform.Find ("Loader");
			ShowLoader (true);
		}
		
		protected virtual void ShowLoader (bool val)
		{
			_loadingIcon.gameObject.SetActive (val);
			
			if (!val) {
				_recentColours.Enable ();
			} else {
				_recentColours.Disable ();
			}
		}
		
		override protected void Init ()
		{
			ShowGame ();
			AddListeners ();
			Debug.LogError ("INITII");
			
			if (SB_SceneHistory.instance.Length <= 1) {
				// This will only happen when running the scene standalone, in the IDE.
				// Set some values, to allow us to test the scene
				_colouringGameData = new ColouringGameData{
																CreationID = DrawingManager.instance.GenerateUniqueName(),
																ImageID = "default_01",
																ImageFileName = "default_pack/default_01.png"
															};
			}
			
			if (SCENE_CONFIG_OBJECT != null) {
				if (SCENE_CONFIG_OBJECT.GetValue ("colouringGameData") != null) {
					_colouringGameData = (ColouringGameData)SCENE_CONFIG_OBJECT.GetValue ("colouringGameData");
				}
				
				if (SCENE_CONFIG_OBJECT.GetValue ("resetDrawing") != null) {
					// Can't case to bool (no idea why), so need to check the lowercase string value..
					if (SCENE_CONFIG_OBJECT.GetValue ("resetDrawing").ToString ().ToLower () == "true") {
						_resetDrawing = true;
					}
				}
				
				if (SCENE_CONFIG_OBJECT.GetValue ("createDuplicateDrawing") != null) {
					if (SCENE_CONFIG_OBJECT.GetValue ("createDuplicateDrawing").ToString ().ToLower () == "true") {
						_createDuplicateDrawing = true;
					}
				}
				
				if (SCENE_CONFIG_OBJECT.GetValue ("isEditingInspirationImage") != null) {
					if (SCENE_CONFIG_OBJECT.GetValue ("isEditingInspirationImage").ToString ().ToLower () == "true") {
						_isEditingInspirationImage = true;
					}
				}
				
				
				if (SCENE_CONFIG_OBJECT.GetValue (ColouringMenuBase.MENU_ITEMS_CENTER_ON_ITEM_KEY) != null) {
					_itemMenuId = (int)SCENE_CONFIG_OBJECT.GetValue (ColouringMenuBase.MENU_ITEMS_CENTER_ON_ITEM_KEY);
				}
			}
			
			if (_colouringGameData.CreationID == "") {
				_colouringGameData.CreationID = DrawingManager.instance.GenerateUniqueName ();
				warn ("Generating a new _creationId: " + _colouringGameData.CreationID);
				_isNew = true;
			}
			
			if (_createDuplicateDrawing) {
				string currentCreationID = _colouringGameData.CreationID;
				string currentCreationFolder = _colouringGameData.CreationFolder;
				
				string newCreationID = DrawingManager.instance.GenerateUniqueName ();
				string newCreationFolder = currentCreationFolder.Replace (currentCreationID, newCreationID);
				
				//_colouringGameData.CreationID = newCreationID;
				//_colouringGameData.CreationFolder.Replace(currentCreationID, newCreationID);
				
				warn ("CREATING DUPLICATE IMAGE");
				warn ("\tOLD currentCreationID " + currentCreationID + ", folder: " + currentCreationFolder);
				warn ("\tNEW newCreationID " + newCreationID + ", folder: " + newCreationFolder);
				//File.Copy(currentCreationFolder, newCreationFolder);
				
				SB_FileUtils.RecursesiveFolderCopy (currentCreationFolder, newCreationFolder);
				
				_colouringGameData.CreationID = newCreationID;
				_colouringGameData.CreationFolder = newCreationFolder;
			}
			
			
			log ("<color=orange>--> _colouringGameData: " + _colouringGameData.ToString () + "</color>");
			warn ("--> _resetDrawing: " + _resetDrawing);
			warn ("--> _createDuplicateDrawing: " + _createDuplicateDrawing);
			warn ("--> _isEditingInspirationImage: " + _isEditingInspirationImage);
			
			if (_colouringGameData.ImageFileName.Length > 0) {
				_colourController.LoadImage (_colouringGameData.CreationFolder, _colouringGameData.ImageFileName, _resetDrawing);
			} else {
				error ("UNEXPECTED COLOURING IMAGE FILENAME: " + _colouringGameData.ImageFileName);
			}
			
			FileInfo drawingXMLFile = DrawingManager.instance.GetDrawingXML (_colouringGameData.CreationFolder);
			
			if (drawingXMLFile != null) {
				if (_resetDrawing) {
					// Delete XML, if it already exists
					//DeleteDrawing(_creationId);
				} else {
				
					// Load drawing XML
					// THE XML FILE IS NOT SAVED FOR JUNIOR COLOURING!!
					//StartCoroutine (LoadDrawingDataXML (drawingXMLFile.FullName));
				}
			} else {
				warn ("Drawing XML file does not exist");
			}
			/*
			Text title = transform.Find ("End/title/label").GetComponent<Text> ();
			string titleTxt = SB_Utils.GetNodeValueFromConfigXML ("config/copy/menus/my_creations/text", "title");
			title.text = SB_Utils.SanitizeString (titleTxt);
			
			Text shareTitle = transform.Find ("End/Share/Title/label").GetComponent<Text> ();
			string shareTitleTxt = SB_Utils.GetNodeValueFromConfigXML ("config/copy/menus/my_creations/text", "share_title");
			shareTitle.text = SB_Utils.SanitizeString (shareTitleTxt);
			
			_sharingSubject = SB_Utils.GetNodeValueFromConfigXML ("config/copy/sharing/text", "subject");
			_sharingEmailBody = SB_Utils.GetNodeValueFromConfigXML ("config/copy/sharing/text", "email_body");
			*/
			//---------------------------------------------------------
			
			Dictionary<string, string> trackingParams = new Dictionary<string, string> ();
			
			if (_isNew)
				trackingParams.Add ("IsNew", _isNew.ToString ());
			if (_resetDrawing)
				trackingParams.Add ("IsReset", _resetDrawing.ToString ());
			if (_createDuplicateDrawing)
				trackingParams.Add ("IsDuplicate", _createDuplicateDrawing.ToString ());
			
			if (_isEditingInspirationImage) {
				trackingParams.Add ("IsEditingInspirationImage", _isEditingInspirationImage.ToString ());
				
				// Append '_inspiration' to the ImageID value, so we can see that the user is editing a pre-made image
				trackingParams.Add ("ImageID", _colouringGameData.ImageID + "_inspiration");
			} else {
				trackingParams.Add ("ImageID", _colouringGameData.ImageID);
			}
			
			LogSceneEvent ("ColouringInit", trackingParams);
		}
		
		private void AddListeners ()
		{
			_colourController.OnColourPicked = HandleColourChanged;
			_colourController.OnPictureLoaded = HandlePictureLoaded;
			//_colourController.OnPictureSaved = HandlePictureSaved;
			//_colourController.ColourHistory.OnChanged = HandleOnHistoryChanged;
			
			//_colourPicker.OnColourChange = ColourChanged;
			_colourMenu.OnColourChange = HandleColourChanged;
			_colourMenu.OnPatternChange = HandlePatternChanged;
			_colourMenu.OnMenuShowing = HandleMenuShowing;
			
			_recentColours.OnReleaseCallback = HandleOnRecentColoursReleased;
		}
		
		private void RemoveListeners ()
		{
			_colourController.OnColourPicked = null;
			_colourController.OnPictureLoaded = null;
			//_colourController.OnPictureSaved = null;
			//_colourController.ColourHistory.OnChanged = null;
			
			//_colourPicker.OnColourChange = null;
			_colourMenu.OnColourChange = null;
			_colourMenu.OnPatternChange = null;
			_colourMenu.OnMenuShowing = null;
			
			_recentColours.OnReleaseCallback = null;
		}
		
		/**
		 * Handle presses to colour swipe menu
		 */
		protected virtual void HandleColourChanged (Color32 col)
		{
			SetColour (col);
			
			RecentColoursMenuItem menuItem = _recentColours.Add (col);
			menuItem.Selected = true;
		}
		
		protected virtual void HandlePatternChanged (int patternId)
		{
			warn ("--> HandlePatternChanged - id: " + patternId);
		}

		protected virtual void HandlePictureLoaded ()
		{
			_pictureLoaded = true;
			ShowLoader (false);
		}
		
		/*
		private void HandlePictureSaved()
		{
			log("HandlePictureSaved()");
			ShowLoader(false);
			
			ShowEnd();
			_colouringBottomMenu.SetState(ColouringBottomMenu.STATE_END);
		}
		*/
		
		private void HandleOnHistoryChanged (ColourHistory colourHistory)
		{
			log ("HandleOnHistoryChanged count: " + colourHistory.History.Count);
			
			/*
			if(colourHistory.History.Count == 0)
			{
				_colouringBottomMenu.SetButtonState(ColouringBottomMenu.BTN_SAVE, false);
			}
			else
			{
				_colouringBottomMenu.SetButtonState(ColouringBottomMenu.BTN_SAVE, true);
			}
			*/
		}
		protected virtual void HandleMenuShowing (bool menuIsShowing)
		{
			log ("HandleMenuShowing - menuIsShowing: " + menuIsShowing);
			_colourController.Paused = menuIsShowing;
		}
		
		/**
		 * Handle presses to colour history carousel
		 */
		private void HandleOnRecentColoursReleased (RecentColoursMenuItem menuItem)
		{
			error ("--> HandleOnRecentColoursReleased menuItem.Colour: " + menuItem.Colour);
			SetColour (menuItem.Colour);
		}
		
		protected virtual void SetColour (Color32 col)
		{
			_currentColor = col;
			_colourController.SetColouringColour (col);
			_colouringBottomMenu.DeselectAll ();
			
			//RecentColoursMenuItem menuItem = _recentColours.Add(col);
			//menuItem.Selected = true;
		}


		
		override protected void Update ()
		{

			if (_gState == STATE_LOAD) {
				_loadCount -= Time.deltaTime;
				if (_loadCount <= 0) {
					LoadStarted ();	
				}
				//Debug.LogError("LOAD STARTED");
			}

			if (_userPausedGame || _popupManager.CurrentPopupObject != null) {
				//	_carousels.ForEach((c) => c.Paused = true);
			} else {	



				Vector3 mousepos = Camera.main.ScreenToWorldPoint (UnityEngine.Input.mousePosition);
				if (UnityEngine.Input.GetMouseButtonUp (0)) {
					//_colourArea.Colour(_currentColor,mousepos);
				}
				base.Update ();
				
				if (_gState == STATE_GAME && _colourController.IsDrawing) {
					//ResetIdle();
					UpdateEncouragementVO ();
				} else {
					UpdateIdle ();
				}
			}
			
			// Handle Android device 'back' button
			if (UnityEngine.Input.GetKeyUp (KeyCode.Escape) && SB_Globals.PLATFORM_ID != SB_BuildPlatforms.Amazon) {
				HandleBackButton ();
			}
		}
	
		/**
		* Handle events to popups
		*/
		override protected void HandleOnPopupEvent (SB_BasePopup popup, string eventType)
		{
			int currentPopupType = _popupManager.CurrentPopupType;
			switch (eventType) {
			case SB_GameQuitPopup.OPTION_OK:
				if (currentPopupType == SB_PopupTypes.GAME_QUIT_POPUP) {
					SB_Analytics.instance.SceneEvent(SB_AnalyticEventList.EXIT_BTN);

					QuitGame ();
				}
				break;
				
			case SB_ConfirmResetDrawingPopup.OPTION_OK:
				log ("-- CONFIRM RESET DRAWING");
				_colourController.ResetDrawing ();
					// User has closed the 'game paused' popup
				_userPausedGame = false;
				break;
				
			case SB_BasePopup.OPTION_CLOSE:
					// If the gating popup or game help was closed, then we need to return to the game paused popup
				if (currentPopupType == SB_PopupTypes.GATING_POPUP || 
					currentPopupType == SB_PopupTypes.GAME_HELP_POPUP || 
					currentPopupType == SB_PopupTypes.RESET_DRAWING_POPUP) {
					if (_userPausedGame) {
						ShowPopup (SB_PopupTypes.GAME_PAUSED_POPUP);
					}
				} else if (currentPopupType == SB_PopupTypes.GAME_PAUSED_POPUP) {
					// User has closed the 'game paused' popup
					_userPausedGame = false;
				}
					
				log ("A popup was closed.. _userPausedGame: " + _userPausedGame);
				break;
				
			case SB_GamePausedPopup.OPTION_PARENTS_AREA:
				SB_Analytics.instance.SceneEvent(SB_AnalyticEventList.PAUSE_SCREEN,SB_AnalyticEventList.PARENTS_BTN);

				ShowPopup (SB_PopupTypes.GATING_POPUP);
				break;
					
			case SB_GamePausedPopup.OPTION_HOW_TO_PLAY:
				SB_Analytics.instance.SceneEvent(SB_AnalyticEventList.HELP_BTN);

				SB_PopupVO helpPopupVO = new SB_PopupVO (SB_PopupTypes.GAME_HELP_POPUP, GameConfig.HELP_POPUP_RESOURCE_NAME, "", "");
				ShowPopup (SB_PopupTypes.GAME_HELP_POPUP, helpPopupVO);
				break;
					
				
			case SB_GamePausedPopup.OPTION_RESET_DRAWING:
				SB_Analytics.instance.SceneEvent(SB_AnalyticEventList.RESET_BTN);

				SB_PopupVO popupVO = new SB_PopupVO (SB_PopupTypes.RESET_DRAWING_POPUP, SB_PopupManager.PREFAB_RESET_DRAWING_POPUP, "", "", null);
				ShowPopup (SB_PopupTypes.RESET_DRAWING_POPUP, popupVO);
				break;
					
			case SB_GamePausedPopup.OPTION_MENU:
				SB_Analytics.instance.SceneEvent(SB_AnalyticEventList.EXIT_BTN);

					//PlayOutroAndLoad(SB_Globals.SCENE_MAIN_MENU); 
				QuitGame ();
				break;
					
			case SB_GatingPopup.GATING_SUCCESS:
				string nextScene = _popupManager.CurrentPopupScript.vo.data as string;
				LoadParentsArea ();
				break;
			/*
					// Parents area button was pressed, and gating succeeded
				case SB_GatingPopup.GATING_SUCCESS:
					// Save user content, incase user returns back here
					
					//GameConfig.USER_SCENE_CONFIG_OBJECT = GetKeyValueObject();
					break;
					*/
			case SB_BasePopup.POPUP_CLOSED:
				_recentColours.Enable ();
				_colourController.Paused = false;
				break;
	
			case SB_BasePopup.POPUP_STARTED:
	
				Debug.LogError ("POPUP STARTED");
				break;
			}
			
			base.HandleOnPopupEvent (popup, eventType);
		}

		public void ShowHelp ()
		{
			SB_PopupVO popupVO = new SB_PopupVO (SB_PopupTypes.GAME_HELP_POPUP, SB_PopupManager.PREFAB_HELP_POPUP, "", "");
			ShowPopup (SB_PopupTypes.GAME_HELP_POPUP, popupVO);
			
			_recentColours.Disable ();
			_colourController.Paused = true;
		}
		
		// Can be overriden by subclasses, incase they need to pass a KeyValue object to the next scene
		protected virtual void LoadParentsArea ()
		{
			LogSceneEvent ("Parents");
			SaveDrawing ();
			
			// Make sure the 'CreationFolder' has a value, so we can reload the image, when we return from the parents area
			if (_colouringGameData.CreationFolder == "") {
				_colouringGameData.CreationFolder = DrawingManager.instance.GetImageFolder (_colouringGameData.ImageID, _colouringGameData.CreationID);
			}
			
			KeyValueObject kv = new KeyValueObject ();
			kv.SetKeyValue ("colouringGameData", _colouringGameData);
			kv.SetKeyValue (ColouringMenuBase.MENU_ITEMS_CENTER_ON_ITEM_KEY, _itemMenuId);
			
			log ("<color=orange>--> WaitThenLoadParentsArea _colouringGameData: " + _colouringGameData.ToString () + "</color>");
			
			GameConfig.USER_SCENE_CONFIG_OBJECT = kv;
			//LoadScene(SB_Globals.SCENE_PARENTS_AREA, kv, true);
			PlayOutroAndLoad (SB_Globals.SCENE_PARENTS_AREA, "Outro", kv);
		}
		
		protected virtual void QuitGame ()
		{
			StartCoroutine (WaitThenSaveDrawing ());
		}
		
		protected virtual IEnumerator WaitThenSaveDrawing ()
		{
			yield return new WaitForSeconds (0.65f);
			SaveDrawing ();
			LoadPreviousScene ();
		}

		public void Enable (bool val)
		{
			_colouringBottomMenu.Enable (val);
			//transform.Find ("ui_back_btn").GetComponent<SB_Button> ().enabled = val;
		}
		
		public bool LoadingIconShowing ()
		{
			if (_loadingIcon == null)
				return false;
			
			return _loadingIcon.gameObject.activeSelf;
		}

		override protected void OnBtnHandler (SB_Button btn, string eventType)
		{
			// Ignore buttons, if loader is showing
			if (LoadingIconShowing ())
				return;
		
		
			base.OnBtnHandler (btn, eventType);
			
			if (_userPausedGame || _popupManager.CurrentPopupObject != null) {
				//	_carousels.ForEach((c) => c.Paused = true);
			} else {
				if (eventType == SB_Button.MOUSE_UP) {
					if (btn.name == "ColourMenuBtn") {
						// Don't play a sound for this button
					} else {
						PlayButtonPressSound ();
					}
					
					switch (btn.name) {						
					case SB_UIControls.BTN_MENU:
							//LogButtonEvent("menu");
						ShowPopup (SB_PopupTypes.GAME_QUIT_POPUP);
						break;
							
					// TEMPORARILY DISABLED!!!!
					case SB_UIControls.BTN_PAUSE:
							//LogButtonEvent("pause");
						_userPausedGame = true;
						ShowPopup (SB_PopupTypes.GAME_PAUSED_POPUP);
						break;
							
					case "TestFillBtn":
						_colourController.DoTestFill ();
	
						break;
					
					case "ColourMenuBtn":
						ToggleColourMenu ();
						break;
							
					case SB_UIControls.BTN_BACK:
						HandleBackButton ();
						break;
					/*
						case SB_ShareComponent.BTN_EMAIL:
							_endShareComponent.SendEmailWithImage (_sharingSubject, _sharingEmailBody, new string[]{}, _colourController.ColouringArea.GetSharingTexture);
							break;
						
						case SB_ShareComponent.BTN_FACEBOOK:
							_endShareComponent.ShareImageOnFB (_sharingSubject, _colourController.ColouringArea.GetSharingTexture);
							break;
							
						case SB_ShareComponent.BTN_TWITTER:
							LogButtonEvent("ShareBtn_twitter");
							_endShareComponent.ShareImageOnTwitter (_sharingSubject, _colourController.ColouringArea.GetSharingTexture);
							break;
							
						case SB_ShareComponent.BTN_WHATSAPP:
							_endShareComponent.ShareImageOnWhatsApp (_sharingSubject, _colourController.ColouringArea.GetSharingTexture);
							break;
							
						case SB_ShareComponent.BTN_INSTAGRAM:
							_endShareComponent.ShareImageOnInstagram (_sharingSubject, _colourController.ColouringArea.GetSharingTexture);
							break;
							
						case SB_ShareComponent.BTN_OTHERS:
							_endShareComponent.ShareImageUsingShareSheet (_sharingSubject, _colourController.ColouringArea.GetSharingTexture);
							break;
							*/
					}
				}
			}
		}
		
		protected virtual void ToggleColourMenu ()
		{
			_colourMenu.ToggleMenu ();
		}

		private void HandleBackButton ()
		{
			if (_gState == STATE_GAME) {
				/*if (_colourController.ColourHistory.History.Count == 0) {
									// Delete drawing XML file, if there's no history
									DeleteDrawing (_colouringGameData.CreationID);
								} else {
									SaveDrawing ();
								}
								LoadPreviousScene ();*/
				ShowLoading (STATE_GAME);				
			} else if (_gState == STATE_END) {
				SB_SoundManager.instance.ClearVOQueue ();
				ShowGame ();
			}
		}

		public void SaveImageToGallery ()
		{
			_userPausedGame = true;
			Enable (false);

			ScreenshotManager.ImageFinishedSaving += ScreenshotSaved;	
			
			string filename = "";
			if (_colouringGameData != null) {
				filename = SB_Globals.PHOTO_ALBUM_NAME + _colouringGameData.CreationID;
				Debug.Log ("*** About to save image to: " + filename);
				
				Texture2D sharingTexture = _colourController.ColouringArea.GetSharingTexture;
				if (sharingTexture != null) {
					byte[] bytes = sharingTexture.EncodeToPNG ();
					StartCoroutine (ScreenshotManager.SaveExisting (bytes, filename, true));
				} else {
					Debug.LogError ("*** sharingTexture is null!!! NOT saving image");
				}
			} else {
				Debug.LogError ("*** _colouringGameData is null!!! NOT saving image");
			}
		}

		void ScreenshotSaved (string path)
		{
			Debug.LogError ("screenshot finished saving to " + path);
			
			ScreenshotManager.ImageFinishedSaving -= ScreenshotSaved;
			LoadPreviousScene ();

		}
		
		// Called by the ColouringBottomMenu 'tick' button, and the back button in this class
		public void LoadPreviousScene ()
		{
			SaveDrawing ();
		
			KeyValueObject kv = new KeyValueObject ();
			kv.SetKeyValue (ColouringMenuBase.MENU_ITEMS_CENTER_ON_ITEM_KEY, _itemMenuId);
			log ("LoadPreviousScene ----> _itemMenuId: " + _itemMenuId);
		
			string prevScene = SB_SceneHistory.instance.GetPreviousItem (true);
			PlayOutroAndLoad (prevScene, "Outro", kv);
		}
		
		// Show the coloring game, with colouring buttons enabled
		public void ShowGame ()
		{
			_gState = STATE_GAME;
			
			transform.Find ("Game").gameObject.SetActive (true);
			transform.Find ("End").gameObject.SetActive (false);
			
			//transform.Find ("ColourMenuBtn").gameObject.SetActive (true);
			_recentColours.gameObject.SetActive (true);
			
			//_uiControls.GetButton(SB_UIControls.BTN_PARENTS).SetActive(false);
			_colouringBottomMenu.SetState (ColouringBottomMenu.STATE_GAME);
		}
		
		// Show the coloured pic, with 'share' buttons 
		public void ShowEnd ()
		{
			ShowLoading (STATE_END);
		}

		public void EndReadyToShow ()
		{
			_gState = STATE_END;
			
			if (_colourMenu.IsShowing) {
				_colourMenu.Hide ();
			}
			
			//PlayEndIdleVO();
			PlayEndVO ();
			
			SaveDrawing ();
			ShowLoader (false);
			
			transform.Find ("Game").gameObject.SetActive (false);
			transform.Find ("End").gameObject.SetActive (true);
			
			//transform.Find ("ColourMenuBtn").gameObject.SetActive (false);
			_recentColours.gameObject.SetActive (false);
			
			//_uiControls.ShowButtons(new List<string>(new string[] { SB_UIControls.BTN_PARENTS}));
			_colouringBottomMenu.SetState (ColouringBottomMenu.STATE_END);
			
			// ------- OLD CODE - load the preview iamge from disk
			//string previewImgPath = DrawingManager.instance.DrawingsRootFolder + "/" + _colouringGameData.ImageID + "/" + _colouringGameData.CreationID + "/" + _colouringGameData.ImageID + ".png";
			//_endPicture.LoadImage(previewImgPath, OnEndPreviewLoaded);
			// ------- END OLD CODE
			
			
			// ------- NEW CODE - Instantiate a copy of the creation, and put it in the end picture holder
			Transform endPicTrans = transform.Find ("End/PictureHolder/Picture");
			if (_endPictureGameObj != null) {
				Destroy (_endPictureGameObj);
			}
			
			_endPictureGameObj = (GameObject)Instantiate (_colourController.ColouringArea.gameObject);
			_endPictureGameObj.transform.SetParent (endPicTrans);
			_endPictureGameObj.transform.localScale = Vector2.one;
			_endPictureGameObj.transform.localPosition = Vector2.zero;
			
			SpriteRenderer[] renderers = _endPictureGameObj.GetComponentsInChildren<SpriteRenderer> ();
			foreach (SpriteRenderer r in renderers) {
				r.enabled = true;
			}
			
			float _scaler = 1f;
			if (tk2dSystem.CurrentPlatform == "1x")
				_scaler = 0.5f;
			Vector3 rescaled = endPicTrans.localScale;
			rescaled.x = _scaler;
			rescaled.y = _scaler;
			endPicTrans.localScale = rescaled;
			// ------- END NEW CODE
		}

		void OnEndPreviewLoaded (bool obj)
		{
			// Preview image is 512x512px, so we need to scale it to half size, to fit the area
			float _scaler = 0.5f;
			Vector3 rescaled = _endPicture.transform.localScale;
			rescaled.x = _scaler;
			rescaled.y = _scaler;
			
			_endPicture.transform.localScale = rescaled;
		}
		
		// Undo - Called from the ColouringBottomMenu class
		public void Undo ()
		{
			warn ("Undo");
			_colourController.Undo ();
		}
		
		// Set colour picker state - Called from the ColouringBottomMenu class
		public void SetColourPickerState ()
		{
			warn ("SetColourPickerState");
			_colourController.SetState (ColouringController.STATE_COLOUR_PICK);
		}

		public void ShowLoading (int afterload)
		{
			_loadCount = LOAD_PAUSE;
			Enable (false);
			ShowLoader (true);
			_afterLoad = afterload;
			_gState = STATE_LOAD;
		}

		public void LoadStarted ()
		{

			_gState = _afterLoad;
			if (_gState == STATE_GAME) {
				//if (_colourController.ColourHistory.History.Count == 0) {
				if (_colourController.ColourHistory.Count == 0) {
					// Delete drawing XML file, if there's no history
					DeleteDrawing (_colouringGameData.CreationID);
				} else {
					SaveDrawing ();
				}
				LoadPreviousScene ();
			} else if (_gState == STATE_END) {
				EndReadyToShow ();
			} else if (_gState == STATE_SAVE_IMAGE) {
				SaveImageToGallery ();
			}

			Enable (true);
		}

		public void StartSaveImage ()
		{
			PlayCameraSound ();
			ShowLoading (STATE_SAVE_IMAGE);
		}
		
		public void SaveDrawing ()
		{
			warn ("SaveDrawing --- ImageID: " + _colouringGameData.ImageID + ", CreationID: " + _colouringGameData.CreationID);
			
			ShowLoader (true);
			//WriteDrawingDataXML (); // XML FILE IS NOT SAVED FOR JUNIOR COLOURING
			_colourController.SaveImage (_colouringGameData.ImageID, _colouringGameData.CreationID);
		}
		/*
		IEnumerator LoadDrawingDataXML (string fileName)
		{
			string url = "file:///" + fileName;
			Debug.LogError ("----> LoadDrawingDataXML " + url);
			
			WWW www = new WWW (url);
			yield return www;
			
			// Check that the www.text actually has some data in it, and that no error has been returned
			if (www.error == null && www.text.Length > 16) {
				// Data loaded
				
				System.IO.StringReader stringReader = new System.IO.StringReader (www.text);
				stringReader.Read (); // skip BOM
				_drawingDataXML = new XmlDocument ();
				_drawingDataXML.LoadXml (stringReader.ReadToEnd ());
				
				//GameConfig.MonsterXML = _drawingDataXML;
				//SetupMonster();
				
				XmlNode drawingColoursNode = _drawingDataXML.SelectSingleNode ("drawing/colours").FirstChild;
				if (drawingColoursNode != null && !_resetDrawing) {
					string colours = drawingColoursNode.Value;
					_recentColours.FromXMLString (colours);
				}
				
				XmlNode drawingHistoryNode = _drawingDataXML.SelectSingleNode ("drawing/history").FirstChild;
				if (drawingHistoryNode != null && !_resetDrawing) {
					string history = drawingHistoryNode.Value;
					_colourController.ColourHistory.FromXMLString (history);
				}
				
				Debug.LogError ("----> SUCCESS: " + www.text);
			} else {
				// Failed to load data from URL
				Debug.LogError ("----> ERROR: " + www.error);
			}
			
			www.Dispose ();
			www = null;
			
			StopCoroutine (LoadDrawingDataXML (fileName));
		}
		*/
		private void DeleteDrawing (string drawingId)
		{
			DrawingManager.instance.DeleteDrawing (drawingId);
		}
		
		/*
		// XML FILE IS NOT SAVED FOR JUNIOR COLOURING
		private void WriteDrawingDataXML ()
		{
			string drawingDataXML = "<drawing>\n";
			drawingDataXML += "\t" + _recentColours.ToXMLString ();
			drawingDataXML += "\t" + _colourController.ColourHistory.ToXMLString ();
			drawingDataXML += "\n</drawing>";
				
			log ("-----------------------");
			log ("WriteDrawingDataXML()");
			log (drawingDataXML);
			log ("-----------------------");
				
			XmlDocument xml = new XmlDocument ();
			xml.LoadXml (drawingDataXML);
			string x = SB_Utils.BeautifyXML (xml);
				
			DrawingManager.instance.SaveDrawingXML (x, _colouringGameData.ImageID, _colouringGameData.CreationID);
		}
		*/
		
		#region IDLE
		protected float _idleCount = 10f;
		protected float _maxIdle = 10f;
		protected int _idleNum = 0;
		
		protected virtual void ResetIdle ()
		{
			_idleCount = _maxIdle;
		}
		
		private void UpdateIdle ()
		{
			//log("UpdateIdle _idleCount: " + _idleCount);
			
			_idleCount -= Time.deltaTime;
			if (_idleCount <= 0f) {
				_idleCount = _maxIdle;
				
				if (_gState == STATE_GAME) {
					PlayIdleVO ();
				} else {
					PlayEndIdleVO ();
				}
			}
		}
		
		private float _encouragementPrompt = 2.0f;
		
		public void UpdateEncouragementVO ()
		{
			//log("UpdateEncouragementVO _encouragementPrompt: " + _encouragementPrompt);
			
			_encouragementPrompt -= Time.deltaTime;
			if (_encouragementPrompt < 0.0f) {
				_encouragementPrompt = UnityEngine.Random.Range (15.0f, 20.0f);
				PlayEncouragementVO ();
			}
		}
		#endregion
		
		#region AUDIO
		protected virtual void PlayIdleVO ()
		{
			// Defined by subclass
		}
		
		protected virtual void PlayEndIdleVO ()
		{
			// Defined by subclass
		}
		
		protected virtual void PlayEndVO ()
		{
			// Defined by subclass
		}
		
		protected virtual void PlayEncouragementVO ()
		{
			// Defined by subclass
		}
		
		void PlayCameraSound ()
		{
			SB_SoundManager.instance.PlaySound ("Audio/SFX/UI/SndCamera");
		}
		#endregion
		
		override protected void OnDestroy ()
		{
			RemoveListeners ();
			
			/*
			Destroy (_colourPicker.gameObject);
			_colourPicker = null;
			*/
			_recentColours.OnReleaseCallback = null;
			_recentColours = null;
			Debug.LogError ("colouring scene disposed");
			base.OnDestroy ();
		}
	}
}