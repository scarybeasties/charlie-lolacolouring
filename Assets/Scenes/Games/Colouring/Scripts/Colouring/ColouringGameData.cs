﻿
/**
 * A data object for the colouring-in game
 */
namespace ScaryBeasties.Colouring
{
	public class ColouringGameData
	{
		// Unique timestamp identifier of the creation, used to name the folder containing the created images/data xml (e.g. '131058228175148427');
		public string CreationID = ""; 
		
		// Full path to the creation folder, on the local filesystem (e.g. 'C:\Users\George\AppData\LocalLow\ScaryBeasties\Colouring\user1\drawings\doctors_01\131058228175148427')
		public string CreationFolder = "";
		
		// Identifier of the colouring in image (Examples: 'doctors_01', 'patterns_06', 'enemies_09' etc..)
		// Should match the asset id value, from the pack xml 
		public string ImageID = "";
		
		// Image filename
		public string ImageFileName = "";
		
		public string ToString()
		{
			string str = "CreationID: " + CreationID;
			str += ", CreationFolder: " + CreationFolder;
			str += ", ImageID: " + ImageID;
			str += ", ImageFileName: " + ImageFileName;
			
			return str;
		}
	}
}