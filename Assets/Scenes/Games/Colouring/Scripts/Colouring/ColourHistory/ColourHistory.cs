﻿using UnityEngine;
using System.Collections;
using System;

namespace ScaryBeasties.Colouring
{
	public class ColourHistory 
	{
		// Callback, whenever history items are added/removed
		public Action<ColourHistory> OnChanged;
	
		// How many steps can be added to the colour history array?
		public const int MAX_HISTORY_COUNT = 256;
		private ArrayList _history = new ArrayList();
		
		public ArrayList History
		{
			get{return _history;}
		}
		
		public void Clear()
		{
			_history.Clear();
		}
		
		public void Add(ColourHistoryItem colHistoryItem)
		{
			if(_history.Count >= MAX_HISTORY_COUNT)
			{
				_history.RemoveAt(MAX_HISTORY_COUNT-1);
			}
			
			_history.Insert(0, colHistoryItem);
			Log("_history.Count: " + _history.Count + ", added: " + colHistoryItem.ToString());
			
			if(OnChanged != null)
			{
				OnChanged(this);
			}
		}
		
		public ColourHistoryItem Undo()
		{
			ColourHistoryItem item = null;
			
			if(_history.Count > 0)
			{
				item = (ColourHistoryItem)_history[0];
				_history.RemoveAt(0);
			}
			else
			{
				Log("COLOUR HISTORY IS EMPTY!");
			}
			
			if(OnChanged != null)
			{
				OnChanged(this);
			}
			
			return item;
		}
		
		public void FromXMLString(string str)
		{
			Log("----- FromXMLString str: " + str);
			
			string[] historyArr = str.Split('|');
			Log("----- historyArr.Length: " + historyArr.Length);
			Log("----- historyArr.Length: " + historyArr.Length);
			
			for(int n=0; n<historyArr.Length; n++)
			{
				string historyData = historyArr[n];
				Log("----- historyData["+n+"]: " + historyData);
				
				ColourHistoryItem historyItem = new ColourHistoryItem(historyData);
				Add(historyItem);
				
				Log("----- Adding new ColourHistoryItem.  Count: " + _history.Count);
			}
		}
		
		public string ToXMLString()
		{
			Log("--------> ToXMLString  Count: " + _history.Count);
			
			string xml = "<history>";
			
			if(_history != null &&_history.Count > 0) 
			{
				for(int n=0; n<_history.Count; n++)
				{
					ColourHistoryItem historyItem = (ColourHistoryItem)_history[n];
					if(n>0) xml += "|";
					xml += historyItem.ToXMLString();
				}
			}
			
			xml += "</history>";
			
			return xml;
		}
		
		private void Log(string msg)
		{
			Debug.Log("[" + this.GetType() + "] " + msg);
		}
		
		public void Dispose()
		{
			OnChanged = null;
			
			_history.Clear ();
			_history = null;
		}
	}
}