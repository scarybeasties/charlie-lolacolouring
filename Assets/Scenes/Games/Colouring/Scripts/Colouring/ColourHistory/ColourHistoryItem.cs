﻿using UnityEngine;
using ScaryBeasties.Utils;

namespace ScaryBeasties.Colouring
{
	public class ColourHistoryItem
	{
		public Vector2 Point;
		public Color CurrentColour;
		public Color PreviousColour;
		
		// Constructor
		public ColourHistoryItem(string data="")
		{
			// Parse any data passed in
			if(data.Length > 0)
			{
				string[] dataArr = data.Split('_');
				string pointStr = dataArr[0];
				
				// Split the string
				string[] points = pointStr.Split(',');
				
				// Create a vector 2 from the points array values
				Point = new Vector2(float.Parse(points[0]), float.Parse(points[1]));
				
				// Convert hex string to color objects
				CurrentColour = SB_Utils.HexToColour(dataArr[1]);
				PreviousColour = SB_Utils.HexToColour(dataArr[2]);
			}
		}
		
		override public string ToString()
		{
			return "Point: " + Point + ", CurrentColour: " + CurrentColour + ", PreviousColour: " + PreviousColour;
		}
		
		// Returns a string to be saved in the drawing XML file.
		// The properties are split with underscores.  Can't use commas, as the Vector2 Point has a comma in it,
		// which would break when doing a Split on the string.
		public string ToXMLString()
		{
			string str = Point + "_" + SB_Utils.ColourToHex(CurrentColour) + "_" + SB_Utils.ColourToHex(PreviousColour);
			
			// Remove surrounding brackets, and spaces
			str = str.Replace("(", "");
			str = str.Replace(")", "");
			str = str.Replace(" ", "");
			
			return str;
		}
	}
}