﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.UI.SwipeMenu;

namespace ScaryBeasties.Colouring
{
	public class ColourMenuItem : BaseColouringMenuItem
	{
		public Color32 Colour {
			get {
				return transform.Find ("Off").GetComponent<SpriteRenderer> ().color;
			}
			
			set {
				transform.Find ("Off").GetComponent<SpriteRenderer> ().color = value;
				transform.Find ("On").GetComponent<SpriteRenderer> ().color = value;
				//Debug.LogError("COLOUR " + value.r + ", " + value.g + ", " + value.b);
			}
		}
	}
}