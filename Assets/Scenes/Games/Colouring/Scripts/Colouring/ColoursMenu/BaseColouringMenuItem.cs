﻿
using ScaryBeasties.UI.SwipeMenu;

namespace ScaryBeasties.Colouring
{
	public class BaseColouringMenuItem : SwipeMenuItem
	{
		void Start()
		{
			Selected = false;
		}
		
		public bool Selected
		{
			get
			{
				return !transform.Find("Off").gameObject.activeSelf;
			}
			
			set
			{
				transform.Find("Off").gameObject.SetActive(!value);
				transform.Find("On").gameObject.SetActive(value);
			}
		}
		
		protected override void OnDestroy ()
		{
			base.OnDestroy ();
		}
	}
}