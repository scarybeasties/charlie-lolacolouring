﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.UI.SwipeMenu;

namespace ScaryBeasties.Colouring
{
	public class PatternMenuItem : BaseColouringMenuItem
	{
		private int _patternId = 0;
		
		public int PatternId 
		{
			get 
			{
				return _patternId;
			}
			
			set 
			{
				_patternId = value;
				
				tk2dSprite spr = transform.GetComponent<tk2dSprite>();
				spr.SetSprite("patterns/pattern" + _patternId);
				
			}
		}
	}
}
