﻿using System;
using System.Collections;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;
using ScaryBeasties.UI.SwipeMenu;
using ScaryBeasties.Utils;
using ScaryBeasties.Sound;

namespace ScaryBeasties.Colouring
{
	public class ColourMenu : MonoBehaviour
	{
		const string ANIM_OFF = "Off";
		const string ANIM_IDLE_OFF = "IdleOff";
		const string ANIM_ON = "On";
		const string ANIM_IDLE_ON = "IdleOn";
		
		public const int PAGESET_SOLID_COLOURS = 0;
		public const int PAGESET_PATTERNS = 1;
		
		private Animator _animator;
		private SwipeMenu _menu;
		private Transform _colourMenuItemTrans;
		private Transform _patternMenuItemTrans;
		private ArrayList _pageSetIds;
		
		private Text _menuTitle;
		private ArrayList _menuTitlesArr = new ArrayList ();
		
		private Color32 _selectedColour = new Color32 (byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
		// Set a default value for now..
		
		private bool _playedColourPrompt = false;
		// Has 'pick a colour' VO played?
		private bool _playedPatternPrompt = false;
		
		public bool IsShowing = false;
		public Action<Color32> OnColourChange;
		public Action<int> OnPatternChange;
		public Action<bool> OnMenuShowing;
		public bool HideOnPressOutsideBounds = true;

		void Awake ()
		{
			_animator = GetComponent<Animator> ();
			_menu = transform.Find ("SwipeMenu").GetComponent<SwipeMenu> ();
			
			_colourMenuItemTrans = transform.Find ("SwipeMenu/ColourMenuItem");
			_patternMenuItemTrans = transform.Find ("SwipeMenu/PatternMenuItem");
			
			_menuTitle = transform.Find ("SwipeMenu/title/label").GetComponent<Text> ();
			_menuTitle.text = "";
		}

		void Start ()
		{
			SetupMenu ();
			AddListeners ();
			
			_menu.Enable (false);
			_menu.Paused = true;
		}

		public void Show ()
		{
			warn ("Show()");
			
			AnimatorStateInfo animState = _animator.GetCurrentAnimatorStateInfo (0);
			
			if (!animState.IsName (ANIM_ON)) {
				_animator.Play (ANIM_ON);
				PlayPanelSound ();
				
				if (_menu.GetCurrentPageNumber () == PAGESET_SOLID_COLOURS) {
					PlayTapColourVO ();
				} else if (_menu.GetCurrentPageNumber () == PAGESET_PATTERNS) {
					PlayTapPatternVO ();
				}
			}
		}

		public void Hide ()
		{
			warn ("Hide()");
			
			AnimatorStateInfo animState = _animator.GetCurrentAnimatorStateInfo (0);
			
			if (!animState.IsName (ANIM_OFF)) {
				_animator.Play (ANIM_OFF);
				PlayPanelSound ();
			}
		}

		public void ShowIdleOn ()
		{
			_animator.Play (ANIM_IDLE_ON);
		}

		public void Enable ()
		{
			_menu.Enable (true);
			_menu.Paused = false;
		}

		public void Disable ()
		{
			_menu.Enable (false);
			_menu.Paused = true;
		}

		public void ToggleMenu ()
		{
			AnimatorStateInfo animState = _animator.GetCurrentAnimatorStateInfo (0);
			
			if (animState.IsName (ANIM_IDLE_OFF)) {
				Show ();
			} else if (animState.IsName (ANIM_IDLE_ON)) {
				Hide ();
			}
		}

		public void SetCurrentPage (int pageId)
		{
			_menu.ResetToPage (pageId);
		}

		private void SetupMenu ()
		{
			_pageSetIds = new ArrayList ();
			_menuTitlesArr = new ArrayList ();
			
			CreateColoursPageset ();
			CreatePatternsPageset ();
			
			HandlePageSetChanged (PAGESET_SOLID_COLOURS);
		}

		/**
		 * Create a menu pageset containing colour options, parsed from the config XML
		 */
		void CreateColoursPageset ()
		{
			int pageCount = 0;
			
			XmlNodeList palettesList = SB_Globals.CONFIG_XML.GetNodeList ("config/palettes/palette");
			foreach (XmlElement node in palettesList) {
				string name = node.SelectSingleNode ("name").InnerText;
				string colours = node.SelectSingleNode ("colours").InnerText;
				string[] coloursArr = colours.Split (',');
				
				Debug.LogWarning ("------------------ FOUND NODE! ID: " + node.GetAttribute ("id") + ", name: " + name + ", coloursArr.Length " + coloursArr.Length + ", colours: " + colours);
				
				_menuTitlesArr.Add (name);
				
				ArrayList pageset = new ArrayList ();
				for (int i = 0; i < coloursArr.Length; i++) {
					GameObject menuItem = (GameObject)Instantiate (_colourMenuItemTrans.gameObject);
					ColourMenuItem menuItemData = menuItem.GetComponent<ColourMenuItem> ();
					menuItemData.Colour = SB_Utils.HexToColour (coloursArr [i]);
					pageset.Add (menuItemData);
				}
				
				float gap = 55f;
				_menu.AddPageSet (pageset, new Vector2 (6, 5), new Vector2 (gap, gap), pageCount);
				_pageSetIds.Add (pageCount);
				pageCount++;
			}
		}

		/**
		 * Create a menu pageset containing pattern textures
		 */
		void CreatePatternsPageset ()
		{
			int pageCount = 0;
			ArrayList pageset = new ArrayList ();
			
			XmlNodeList patternsList = SB_Globals.CONFIG_XML.GetNodeList ("config/patterns/pattern");
			foreach (XmlElement node in patternsList) {
				string id = node.GetAttribute ("id");
				
				GameObject menuItem = (GameObject)Instantiate (_patternMenuItemTrans.gameObject);
				PatternMenuItem menuItemData = menuItem.GetComponent<PatternMenuItem> ();
				//menuItemData.PatternId = i;
				menuItemData.PatternId = int.Parse (id.Replace ("pattern", ""));
				pageset.Add (menuItemData);
			}
			
			float gap = 62f;
			_menu.AddPageSet (pageset, new Vector2 (5, 4), new Vector2 (gap, gap), pageCount);
			_pageSetIds.Add (pageCount);
			pageCount++;
		}

		void AddListeners ()
		{
			_menu.OnPageSetChanged = HandlePageSetChanged;
			_menu.OnHandleTap = HandleMenuTap;
			_menu.OnPressedOutsideBounds = HandlePressedOutsideBounds;
		}

		
		void RemoveListeners ()
		{
			_menu.OnPageSetChanged = null;
			_menu.OnHandleTap = null;
			_menu.OnPressedOutsideBounds = null;
		}

		/**
		 * Set all colour menu items to their non-selected state
		 */
		void ResetMenuItems ()
		{
			for (int n = 0; n < _menu.CurrentPage.NumItems (); n++) {
				BaseColouringMenuItem menuItem = (BaseColouringMenuItem)_menu.CurrentPage.GetMenuItemAt (n);
				menuItem.Selected = false;
			}
		}

		void HandleMenuTap (SwipeMenuItem item)
		{
			ResetMenuItems ();
			
			BaseColouringMenuItem menuItem = (BaseColouringMenuItem)item;
			menuItem.Selected = true;
			
			warn ("_menu.GetCurrentPageNumber(): " + _menu.GetCurrentPageNumber ());
			
			if (_menu.GetCurrentPageNumber () == PAGESET_SOLID_COLOURS) {
				ColourMenuItem colourMenuItem = (ColourMenuItem)menuItem;
				Debug.LogWarning ("------------------ HandleMenuTap - COLOUR: " + colourMenuItem.Colour);
				if (OnColourChange != null)
					OnColourChange (colourMenuItem.Colour);
			} else if (_menu.CurrentPage.pageSetID == PAGESET_SOLID_COLOURS) {
				PatternMenuItem patternMenuItem = (PatternMenuItem)menuItem;
				Debug.LogWarning ("------------------ HandleMenuTap - PATTERN ID: " + patternMenuItem.PatternId);
				if (OnPatternChange != null)
					OnPatternChange (patternMenuItem.PatternId);
			}
			
			
			Hide ();
		}

		void HandlePageSetChanged (int pageSet)
		{
			warn ("HandlePageSetChanged pageSet: " + pageSet);
			_menuTitle.text = (string)_menuTitlesArr [pageSet];
		}

		/**
		 * Hide the menu, if user pressed outside of the bounds
		 */
		void HandlePressedOutsideBounds (bool val)
		{
			if (val && HideOnPressOutsideBounds) {
				Hide ();
			}
		}

		protected void warn (object msg)
		{
			Debug.LogWarning ("[" + this.GetType () + "] " + msg);
		}

		protected void error (object msg)
		{
			Debug.LogError ("[" + this.GetType () + "] " + msg);
		}

		public void AnimEvent_MenuOn ()
		{
			IsShowing = true;
			
			_menu.Enable (true);
			_menu.Paused = false;
			_menu.ShowArrows ();
			
			error ("-------------------");
			error ("transform.position.y: " + transform.position.y);
			error ("_menu.transform.position.y: " + _menu.transform.position.y);
			
			float yOffset = transform.position.y - _menu.transform.position.y;
			error ("yOffset: " + yOffset);
			error ("-------------------");
			
			if (OnMenuShowing != null)
				OnMenuShowing (true);
		}

		public void AnimEvent_MenuOff ()
		{
			IsShowing = false;
			
			_menu.Enable (false);
			_menu.Paused = true;
			
			if (OnMenuShowing != null)
				OnMenuShowing (false);
		}

		#region AUDIO

		private void PlayPanelSound ()
		{
			SB_SoundManager.instance.PlaySound ("Audio/SFX/UI/SndPopup1", SB_Globals.MAIN_BUTTON_VOLUME, SB_Globals.MAIN_BUTTON_SOUND_CHANNEL);
		}

		public void PlayIdleVO ()
		{
			if (_menu.GetCurrentPageNumber () == PAGESET_SOLID_COLOURS) {
				PlayTapColourVO ();
			} else if (_menu.GetCurrentPageNumber () == PAGESET_PATTERNS) {
				PlayTapPatternVO ();
			}
		}

		public void PlayTapColourVO ()
		{
			/*
			if(_playedColourPrompt) return;
			
			SB_SoundManager.instance.PlayVoiceQueued("Localised/VO/Narrator/Colouring/vo_drawing_in_tap_1");
			_playedColourPrompt = true;
			*/
			
			int rnd = UnityEngine.Random.Range (0, 3);
			
			if (!_playedColourPrompt || rnd == 0) {
				SB_SoundManager.instance.PlayVoiceQueued ("Localised/VO/Charlie/vo_animal_in_tap");
			}
			
			_playedColourPrompt = true;
		}

		void PlayTapPatternVO ()
		{
			if (_playedPatternPrompt)
				return;
			
			// We don't currently have any VO for this, might have to use something generic
			//SB_SoundManager.instance.PlayVoiceQueued("Localised/VO/Narrator/Colouring/vo_drawing_in_tap_1");
			_playedPatternPrompt = true;
		}

		#endregion

		void OnDestroy ()
		{
			RemoveListeners ();
			
			_animator = null;
			_menu = null;
			_colourMenuItemTrans = null;
			_pageSetIds = null;
			
			
			OnColourChange = null;
			OnPatternChange = null;
			OnMenuShowing = null;
		}
	}
}
