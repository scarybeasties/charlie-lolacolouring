﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.SB2D;
using ScaryBeasties.InAppPurchasing.Shop;
using System.IO;
using System.Collections.Generic;
using System;
using ScaryBeasties.Utils;
using ScaryBeasties.Sound;
using ScaryBeasties.Colouring.Junior;

namespace ScaryBeasties.Colouring
{
    public class ColouringArea : MonoBehaviour
    {
        // Allows us to quickly change between fill types, to see which method is best on low spec devices
        const int FILL_TYPE_COLOUR_POINT = 0;
        const int FILL_TYPE_FLOOD = 1;
        const int FILL_TYPE = FILL_TYPE_FLOOD;

        //		const float BRUSH_TEXTURE_SAFE_ZONE = 0.7f;

        const int TOOLS_SFX_CHANNEL = 6;
        private AudioSource _toolsSFXChannel;

        public Action OnLoaded;
        //public Action OnSaved;

        //Paths
        private string _filename = "";
        private string _drawingPath = "";
        private string _linePath = "";
        private string _thumbPath = "";


        //Textures
        private LoadedTexture _drawing;
        private LoadedTexture _lines;


        //ColouringAreas
        //private float alphaTolerence = 0.5f; //0.25f;
        private float alphaTolerence = 0.8f;
        private bool[,] _marked;
        private bool[,] _linesMarked;
        private ArrayList _areas = new ArrayList();

        private bool _fillImageExists = false;

        private int _width, _height;

        private float _time;

        private Color[] _drawingTextureColours;
        private Color[] _lineArray;
        private byte[] _lineTest;

        private Texture2D _drawTex;
        //private Texture2D _mergedTexture;
        private Texture2D _sharingTexture;

        private byte[] _testTest;

        private float _halfTextureWidth = 0f;
        private float _halfTextureHeight = 0f;

        private Texture2D _brushTexture;
        private Color[,] _originalTex;  // <----- Is this required..? Original drawing pixels..?
        private Color[,] _origBrushTex;


        private float _drawingScaleX, _drawingScaleY;
        private float _transformScaleX, _transformScaleY;

        public struct Point
        {
            public short x;
            public short y;
            public Point(short aX, short aY)
            {
                x = aX;
                y = aY;
            }
            public Point(int aX, int aY) : this((short)aX, (short)aY)
            {
            }
        }

        // Use this for initialization
        void Start()
        {
            log("---------------Start");
            _time = Time.time;
            _toolsSFXChannel = SB_SoundManager.instance.GetSoundChannel(TOOLS_SFX_CHANNEL); // REMOVE THIS (moved to Tool class)
        }

        // Getters
        public float HalfTextureWidth { get { return _halfTextureWidth; } }
        public float HalfTextureHeight { get { return _halfTextureHeight; } }

        public Texture2D GetDrawingTexture
        {
            get { return _drawTex; }
        }

        public Texture2D GetSharingTexture
        {
            get { return _sharingTexture; }
        }
        /*
		public Color[] GetDrawingColourArray
		{
			get{ return _drawingTextureColours; }
		}
		*/
        public byte[] GetDrawingBytes
        {
            get { return _drawing.GetTexture().EncodeToPNG(); }
        }

        public void LoadImage(string folder, string colouringImage)
        {

            log("LoadImage() - folder: " + folder + ", colouringImage: " + colouringImage);
            _filename = colouringImage;
            _filename = _filename.Replace("file://", "file:///");

            //_drawingPath = SB_ShopUtils.GetLocalAssetDirectoryPath() + "/" + _filename.Replace(".png", "_fill.png");
            _linePath = SB_ShopUtils.GetLocalAssetDirectoryPath() + "/" + _filename.Replace(".png", "_lines.png");
            _thumbPath = SB_ShopUtils.GetLocalAssetDirectoryPath() + "/" + _filename;
            //_thumbPath = DrawingManager.instance.GetDrawingPreviewPath + "/" + _filename;

            string drawingID = DrawingManager.instance.GetDrawingID(colouringImage).Replace(".png", "");
            //_thumbPath = DrawingManager.instance.DrawingsRootFolder + "/" + drawingID + "/" + drawingID + ".png";
            //_drawingPath = DrawingManager.instance.DrawingsRootFolder + "/" + drawingID + "/" + drawingID + "_fill.png";
            _thumbPath = DrawingManager.instance.GetPreviewImage(drawingID, folder);
            _drawingPath = DrawingManager.instance.GetDrawingImage(drawingID, folder);

            log("---- drawingID " + drawingID);
            log("_thumbPath: " + _thumbPath);
            log("_drawingPath " + _drawingPath);

            _drawing = transform.Find("Drawing").gameObject.GetComponent<LoadedTexture>();
            _lines = transform.Find("Lines").gameObject.GetComponent<LoadedTexture>();

            if (File.Exists(_drawingPath))
            {
                _fillImageExists = true;
                log("--> _drawing.LoadImage: " + _drawingPath);
                _drawing.LoadImage(_drawingPath, OnDrawingImageLoaderCallback);
            }
            else
            {
                _fillImageExists = false;
                Debug.LogError("--- COULD NOT FIND '" + _drawingPath + "', creating a new texture..");
            }

            if (File.Exists(_linePath))
            {
                log("--> _lines.LoadImage: " + _linePath);
                _lines.LoadImage(_linePath, OnLineImageLoaderCallback);
            }

            Debug.LogError("--------LOAD IMAGE " + colouringImage);
            Debug.LogError("--------_drawingPath " + _drawingPath);
            Debug.LogError("--------_linePath " + _linePath);
        }

        public void SaveImage(string imageId, string creationId)
        {
            log("--------SAVE IMAGE " + _drawingPath);
            SaveMainImage(imageId, creationId);

            _sharingTexture = GetFlattenedTexture("jpg");
            SaveSharingImage(imageId, creationId);

            //_mergedTexture = GetFlattenedTexture();
            SavePreviewImage(imageId, creationId);
        }

        void SaveMainImage(string imageId, string creationId)
        {
            Texture2D drawingTexture = _drawing.GetTexture();
            drawingTexture.EncodeToPNG();
            DrawingManager.instance.SaveDrawingImage(drawingTexture.EncodeToPNG(), imageId, creationId);
        }

        /**
		 * Merge the lines and drawing textures, into one Texture2D object
		 */
        Texture2D GetFlattenedTexture(string encodeAsType = "png")
        {
            Texture2D mergedTexture = new Texture2D(_drawing.GetTexture().width, _drawing.GetTexture().height, TextureFormat.RGBA32, false);

            Color[] cols1 = _drawing.GetTexture().GetPixels();
            Color[] cols2 = _lines.GetTexture().GetPixels();
            log("--> About to merge preview texture..");
            for (var i = 0; i < cols1.Length; ++i)
            {
                Color mergedColor = cols2[i];


                if (cols2[i].a < 1.0f)
                {
                    float omin = 1.0f - cols2[i].a;
                    mergedColor = new Color(((omin * cols1[i].r) + (cols2[i].a * cols2[i].r)), ((omin * cols1[i].g) + (cols2[i].a * cols2[i].g)), ((omin * cols1[i].b) + (cols2[i].a * cols2[i].b)), 1.0f);


                }


                cols1[i] = mergedColor;
            }
            mergedTexture.SetPixels(cols1);
            mergedTexture.Apply();
            if (encodeAsType == "png")
            {
                mergedTexture.EncodeToPNG();
            }
            else
            {
                mergedTexture.EncodeToJPG();
            }

            log("--> merge complete");

            return mergedTexture;
        }

        /**
		 * Save a fullsize version of the merged image, so user can share it
		 */
        private void SaveSharingImage(string imageId, string creationId)
        {
            log("--> About to save the sharing image..");
            //DrawingManager.instance.SaveSharingImage(_sharingTexture.EncodeToPNG(), imageId, creationId);
            DrawingManager.instance.SaveSharingImage(_sharingTexture.EncodeToJPG(), imageId, creationId);
        }



        /**
		 * Save the merged lines & filled colour image, to make a preview for the carousels
		 */
        void SavePreviewImage(string imageId, string creationId)
        {
            log("SavePreviewImage()");

            //int previewSize = (int)(_drawing.GetTexture ().width * 0.5f);
            int previewSize = _drawing.GetTexture().width; // Carousel image size = 512px
                                                           //if(tk2dSystem.CurrentPlatform == "1x") previewSize = 256; //TODO: check that 1x devices still run ok, with larger preview images

            log("--> About to scale the preview texture..");
            //TextureScale.Bilinear(_mergedTexture, previewSize, previewSize);
            TextureScale.Bilinear(_sharingTexture, previewSize, previewSize);
            log("--> Scale done!");

            log("--> About to save the preview image..");
            //DrawingManager.instance.SavePreviewImage(_mergedTexture.EncodeToPNG(), imageId, creationId);			
            DrawingManager.instance.SavePreviewImage(_sharingTexture.EncodeToPNG(), imageId, creationId);
            log("--> Preview saved!");
        }

        // Handle the success/fail of the image loader
        private void OnDrawingImageLoaderCallback(bool success)
        {
            FinishLoading();
        }

        // Handle the success/fail of the image loader
        private void OnLineImageLoaderCallback(bool success)
        {
            if (!_fillImageExists)
            {
                // Create an empty 'fill' texture
                _drawing.CreateEmptyTexture(_lines.GetTexture().width, _lines.GetTexture().height);
            }

            FinishLoading();
        }

        public void SetBytes(byte[] bytes)
        {
            Texture2D drawingTexture = _drawing.GetTexture();
            drawingTexture.LoadImage(bytes);
            drawingTexture.Apply();

            _drawingTextureColours = drawingTexture.GetPixels();
        }

        void SetPixels(Color[] colours)
        {
            Texture2D drawingTexture = _drawing.GetTexture();
            drawingTexture.SetPixels(colours);
            drawingTexture.Apply();

            _drawingTextureColours = colours;
        }

        public void SetDrawingTextureColours(Color[] drawingTextureColours)
        {
            _drawingTextureColours = drawingTextureColours;
        }

        public void ResetDrawing()
        {
            log("--> ResetDrawing()");

            Texture2D drawingTexture = _drawing.GetTexture();
            int w = drawingTexture.width;
            int h = drawingTexture.height;

            // Set all pixels to pure white
            int len = w * h;
            Color[] colours = new Color[len];
            for (int i = 0; i < len; i++)
            {
                colours[i] = Color.white;
            }

            SetPixels(colours);
        }

        private void FinishLoading()
        {
            log("--> FinishLoading() _drawing.loaded: " + _drawing.loaded + ", _lines.loaded: " + _lines.loaded);

            if ((_drawing.loaded) && (_lines.loaded))
            {

                log("--> About to call SetupColouringAreas()");
                SetupColouringAreas();


                log("----------- _drawTex.width: " + _drawTex.width + ",  _drawTex.height: " + _drawTex.height, 4);
                log("----------- _width: " + _width + ",  _height: " + _height);


                int w = _drawTex.width;
                int h = _drawTex.height;
                _originalTex = new Color[w, h];
                for (int y = 0; y < h; ++y)
                {
                    for (int x = 0; x < w; ++x)
                    {
                        _originalTex[x, y] = _drawTex.GetPixel(x, y);
                    }
                }

                _drawingScaleX = 1 / _drawing.transform.localScale.x;
                _drawingScaleY = 1 / _drawing.transform.localScale.y;
                _transformScaleX = 1 / transform.localScale.x;
                _transformScaleY = 1 / transform.localScale.y;


                log("--> About to call OnLoaded()");
                if (OnLoaded != null)
                {
                    OnLoaded();
                }
            }
        }


        public float GetWidth()
        {
            return _drawing.GetTexture().width * _drawing.scaler;
        }

        public float GetHeight()
        {
            return _drawing.GetTexture().height * _drawing.scaler;
        }

        // Colour Areas
        public void Colour(Color col, Vector3 pos)
        {
            int width = _drawing.GetTexture().width;
            int height = _drawing.GetTexture().height;

            Vector2 mousepos = new Vector2(pos.x / _drawing.transform.localScale.x, pos.y / _drawing.transform.localScale.y);
            mousepos = new Vector2(mousepos.x / transform.localScale.x, mousepos.y / transform.localScale.y);
            mousepos.x += (width / 2);
            mousepos.y += (height / 2);
            mousepos.x = (int)mousepos.x;
            mousepos.y = (int)mousepos.y;

            if (mousepos.x > 0 && mousepos.x < width && mousepos.y > 0 && mousepos.y < height)
            {
                PlayFillSound();

                if (FILL_TYPE == FILL_TYPE_COLOUR_POINT)
                {
                    ColourPoint(col, mousepos);
                }
                else
                {
                    if (_lineTest[(int)mousepos.x + ((int)mousepos.y * _width)] == 0)
                    {
                        FloodFill((int)mousepos.x, (int)mousepos.y, col);
                    }
                }
            }
            else
            {
                Debug.Log("Pressed outside image area --> mousepos x: " + mousepos.x + ", y: " + mousepos.y);
            }
        }

        private void ColourPoint(Color col, Vector2 vec)
        {
            Debug.LogWarning("--> ColourPoint vec: " + vec);

            for (int i = 0; i < _areas.Count; i++)
            {
                ArrayList arr = (ArrayList)_areas[i];

                if (arr.Contains(vec))
                {
                    i = _areas.Count;

                    for (int j = 0; j < arr.Count; j++)
                    {
                        Vector2 vec2 = (Vector2)arr[j];
                        _drawing.GetTexture().SetPixel((int)vec2.x, (int)vec2.y, col);
                    }
                    _drawing.GetTexture().Apply();
                }
            }
        }

        public Vector2 GetTrueMousePos(Vector2 pos)
        {
            //Vector2 mousepos = new Vector2 (pos.x / _drawing.transform.localScale.x, pos.y / _drawing.transform.localScale.y);			
            //mousepos = new Vector2 (mousepos.x / transform.localScale.x, mousepos.y / transform.localScale.y);
            Vector2 mousepos = new Vector2(pos.x * _drawingScaleX, pos.y * _drawingScaleY);
            mousepos = new Vector2(mousepos.x * _transformScaleX, mousepos.y * _transformScaleY);

            mousepos.x += (_width * 0.5f);
            mousepos.y += (_height * 0.5f);
            mousepos.x = (int)mousepos.x;
            mousepos.y = (int)mousepos.y;

            return mousepos;
        }


        // Colouring Area Setup
        private void SetupColouringAreas()
        {
            log("--> SetupColouringAreas()");

            _width = _lines.GetTexture().width;
            _height = _lines.GetTexture().height;

            _halfTextureWidth = _width / 2;
            _halfTextureHeight = _height / 2;

            _drawTex = _drawing.GetTexture();
            _drawingTextureColours = _drawing.GetTexture().GetPixels();
            _lineArray = _lines.GetTexture().GetPixels();
            _lineTest = new byte[_lineArray.Length];

            log("--> iterating through texture, to mark lines");
            for (int i = 0; i < _width; i++)
            {
                for (int j = 0; j < _height; j++)
                {
                    Color col = _lineArray[i + j * _width];
                    if (col.a < alphaTolerence)
                    {
                        _lineTest[i + j * _width] = 0;
                    }
                    else
                    {
                        _lineTest[i + j * _width] = 1;
                    }
                }
            }

            //_lines.GetTexture ().filterMode = FilterMode.Point;

            //_lines.GetTexture ().mipMapBias = 2f;

            Debug.LogError("MIP MAPS " + _lines.GetTexture().mipmapCount + " " + _lines.GetTexture().mipMapBias);

        }

        private void StackTestPixels(int sx, int sy, ArrayList pixels)
        {

            Queue<Vector2> nodes = new Queue<Vector2>();
            nodes.Enqueue(new Vector2(sx, sy));

            while (nodes.Count > 0)
            {
                Vector2 current = nodes.Dequeue();
                for (int i = (int)current.x; i < _width; i++)
                {
                    if (_marked[i, (int)current.y])
                        break;

                    _marked[i, (int)current.y] = true;
                    pixels.Add(new Vector2(i, (int)current.y));

                    if ((int)current.y + 1 < _height)
                    {
                        if (!_marked[i, (int)current.y + 1])
                            nodes.Enqueue(new Vector2(i, (int)current.y + 1));
                    }
                    if ((int)current.y - 1 >= 0)
                    {
                        if (!_marked[i, (int)current.y - 1])
                            nodes.Enqueue(new Vector2(i, (int)current.y - 1));
                    }
                }
                for (int i = (int)current.x - 1; i >= 0; i--)
                {
                    if (_marked[i, (int)current.y])
                        break;

                    _marked[i, (int)current.y] = true;
                    pixels.Add(new Vector2(i, (int)current.y));

                    if ((int)current.y + 1 < _height)
                    {
                        if (!_marked[i, (int)current.y + 1])
                            nodes.Enqueue(new Vector2(i, (int)current.y + 1));
                    }
                    if ((int)current.y - 1 >= 0)
                    {
                        if (!_marked[i, (int)current.y - 1])
                            nodes.Enqueue(new Vector2(i, (int)current.y - 1));
                    }
                }
            }
        }

        private void FloodFill(int aX, int aY, Color aFillColor)
        {
            Debug.LogWarning("*** FloodFill");

            int w = _width;
            int h = _height;
            byte[] checkedPixels = new byte[_drawingTextureColours.Length];
            Color refCol = Color.black;
            Queue<Point> nodes = new Queue<Point>();
            nodes.Enqueue(new Point(aX, aY));

            while (nodes.Count > 0)
            {
                Point current = nodes.Dequeue();
                if (_lineTest[current.x + current.y * w] == 1)
                {
                    _drawingTextureColours[current.x + current.y * w] = aFillColor;
                }
                else
                {
                    for (int i = current.x; i < w; i++)
                    {
                        if (checkedPixels[i + current.y * w] > 0)
                            break;
                        _drawingTextureColours[i + current.y * w] = aFillColor;
                        checkedPixels[i + current.y * w] = 1;
                        if (_lineTest[i + current.y * w] == 1)
                            break;
                        if (current.y + 1 < h)
                        {
                            if (checkedPixels[i + current.y * w + w] == 0)
                                nodes.Enqueue(new Point(i, current.y + 1));
                        }
                        if (current.y - 1 >= 0)
                        {
                            if (checkedPixels[i + current.y * w - w] == 0)
                                nodes.Enqueue(new Point(i, current.y - 1));
                        }
                    }
                    for (int i = current.x - 1; i >= 0; i--)
                    {
                        if (checkedPixels[i + current.y * w] > 0)
                            break;
                        _drawingTextureColours[i + current.y * w] = aFillColor;
                        checkedPixels[i + current.y * w] = 1;
                        if (_lineTest[i + current.y * w] == 1)
                            break;
                        if (current.y + 1 < h)
                        {
                            if (checkedPixels[i + current.y * w + w] == 0)
                                nodes.Enqueue(new Point(i, current.y + 1));
                        }
                        if (current.y - 1 >= 0)
                        {
                            if (checkedPixels[i + current.y * w - w] == 0)
                                nodes.Enqueue(new Point(i, current.y - 1));
                        }
                    }
                }
            }
            //_drawTex.SetPixels (_drawingTextureColours);
            //_drawTex.Apply ();

            SetPixels(_drawingTextureColours);
        }

        public void Test()
        {
            ArrayList colours = new ArrayList();
            string coloursString = "d0cdcd,434343,000000,ebd7cc,ddb59f,84563e,c33b00,7c1d1e,11335a,3031c6,3a79ee,a14ae6,9090eb,52bae8,8fe0ef,668ba2,306b58,1f9061,2ecc97,c6d915,fdfd8e,fbd009,ffa800,fd8e8e,fd8efd,ff6015,d92626,e92e6f,901f66,008aff,6000ff,004eff,0003ff,c400ff,7a7aff,00f2ff,ffff3d,00ff00,80ff00,d2ff00,ff8000,ff0000,ff00ff,ff5dff,5100d8,0244da,0104ba,ac02df,6b6bd5,02c9d4,e2e238,19c719,6dd900,abc729,c26202,d00202,e504e5,da4dda,ff006d";
            string[] coloursArr = coloursString.Split(',');

            for (int n = 0; n < coloursArr.Length; n++)
            {
                colours.Add(SB_Utils.HexToColour(coloursArr[n]));
            }
            colours.Add(Color.red);
            colours.Add(Color.blue);
            colours.Add(Color.green);
            colours.Add(Color.cyan);
            colours.Add(Color.magenta);
            colours.Add(Color.yellow);

            int cidx = 0;
            _testTest = new byte[_drawingTextureColours.Length];
            int count = 5;
            for (int x = 0; x < _width; x++)
            {
                for (int y = 0; y < _height; y++)
                {
                    if (_lineTest[x + y * _width] == 0)
                    {
                        if (_testTest[x + y * _width] == 0)
                        {
                            TestFloodFill(x, y, (Color)colours[cidx]);
                            cidx = (cidx + 1) % colours.Count;
                        }
                    }
                }
            }
            _drawTex.Apply();
            //TestFloodFill(0,0, colours[cidx]);
        }

        private void TestFloodFill(int aX, int aY, Color aFillColor)
        {
            int w = _width;
            int h = _height;
            byte[] checkedPixels = new byte[_drawingTextureColours.Length];
            Color refCol = Color.black;
            Queue<Point> nodes = new Queue<Point>();
            nodes.Enqueue(new Point(aX, aY));

            while (nodes.Count > 0)
            {
                Point current = nodes.Dequeue();
                if (_lineTest[current.x + current.y * w] == 1)
                {
                    _drawingTextureColours[current.x + current.y * w] = aFillColor;
                }
                else
                {
                    for (int i = current.x; i < w; i++)
                    {
                        if (checkedPixels[i + current.y * w] > 0)
                            break;
                        _drawingTextureColours[i + current.y * w] = aFillColor;
                        checkedPixels[i + current.y * w] = 1;
                        _testTest[i + current.y * w] = 1;
                        if (_lineTest[i + current.y * w] == 1)
                            break;
                        if (current.y + 1 < h)
                        {
                            if (checkedPixels[i + current.y * w + w] == 0)
                                nodes.Enqueue(new Point(i, current.y + 1));
                        }
                        if (current.y - 1 >= 0)
                        {
                            if (checkedPixels[i + current.y * w - w] == 0)
                                nodes.Enqueue(new Point(i, current.y - 1));
                        }
                    }
                    for (int i = current.x - 1; i >= 0; i--)
                    {
                        if (checkedPixels[i + current.y * w] > 0)
                            break;
                        _drawingTextureColours[i + current.y * w] = aFillColor;
                        checkedPixels[i + current.y * w] = 1;
                        _testTest[i + current.y * w] = 1;
                        if (_lineTest[i + current.y * w] == 1)
                            break;
                        if (current.y + 1 < h)
                        {
                            if (checkedPixels[i + current.y * w + w] == 0)
                                nodes.Enqueue(new Point(i, current.y + 1));
                        }
                        if (current.y - 1 >= 0)
                        {
                            if (checkedPixels[i + current.y * w - w] == 0)
                                nodes.Enqueue(new Point(i, current.y - 1));
                        }
                    }
                }
            }
            _drawTex.SetPixels(_drawingTextureColours);

        }

        public Color GetColourAt(Vector2 point)
        {
            Vector2 mousepos = new Vector2(point.x / _drawing.transform.localScale.x, point.y / _drawing.transform.localScale.y);
            mousepos = new Vector2(mousepos.x / transform.localScale.x, mousepos.y / transform.localScale.y);
            mousepos.x += (_drawing.GetTexture().width / 2);
            mousepos.y += (_drawing.GetTexture().height / 2);
            mousepos.x = (int)mousepos.x;
            mousepos.y = (int)mousepos.y;

            if (_drawing != null)
            {
                Debug.LogWarning("DRAWING point: " + (int)mousepos.x + ", " + (int)mousepos.y);
                return _drawing.GetTexture().GetPixel((int)mousepos.x, (int)mousepos.y);
            }
            else
            {
                Debug.LogWarning("DRAWING IS NULL");
            }

            return Color.white;
        }

        #region AUDIO

        void PlayFillSound()
        {
            int rnd = Mathf.RoundToInt(UnityEngine.Random.Range(1, 4));
            SB_SoundManager.instance.PlaySound("Audio/SFX/Games/Colouring/SndFill" + rnd, 1f);
        }

        #endregion

        #region DEBUGGING
        string GetTimeStamp()
        {
            DateTime now = DateTime.Now;

            string hrs = FormatTimeStamp(now.TimeOfDay.Hours.ToString());
            string mins = FormatTimeStamp(now.TimeOfDay.Minutes.ToString());
            string secs = FormatTimeStamp(now.TimeOfDay.Seconds.ToString());
            string ms = FormatTimeStamp(now.TimeOfDay.Milliseconds.ToString());

            return hrs + ":" + mins + ":" + secs + ":" + ms;
        }

        string FormatTimeStamp(string str)
        {
            if (str.Length == 1)
            {
                str = "0" + str;
            }

            return str;
        }

        protected void log(object msg, int clr = 0)
        {
            string logClr = "grey";
            if (clr == 1) logClr = "white";
            if (clr == 2) logClr = "green";
            else if (clr == 3) logClr = "blue";
            else if (clr == 4) logClr = "red";
            else if (clr == 5) logClr = "purple";
            else if (clr == 6) logClr = "cyan";

            Debug.Log(GetTimeStamp() + " [" + this.GetType() + "] " + "<color=" + logClr + ">" + msg + "</color>");
        }
        #endregion

        void OnDestroy()
        {
            OnLoaded = null;

            _drawing = null;
            _lines = null;
            _drawTex = null;
        }
    }
}
