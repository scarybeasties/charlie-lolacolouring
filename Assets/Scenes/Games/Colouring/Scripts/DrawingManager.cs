﻿
using System;
using ScaryBeasties.User;
using System.IO;
using System.Collections;
using UnityEngine;
using ScaryBeasties.Utils;
using System.Text;

public class DrawingManager
{
	// Subfolder of story content, 
	public const string DRAWINGS_FOLDER = "drawings";
	public const string DRAWINGS_XML_FILENAME = "drawing.xml";
	
	public const string INSPRITATION_FOLDER = "inspiration";
	
	private static DrawingManager _instance = null;
	
	public static DrawingManager instance
	{
		get 
		{
			if(_instance == null)
			{
				_instance = new DrawingManager();
			}
			
			return _instance;
		}
	}

	// Returns a string, the location of SB created drawings within the current user profile folder
	public string InspirationsRootFolder { get { return SB_UserProfileManager.instance.CurrentProfileFolder + INSPRITATION_FOLDER; }}
	
	// Returns an array of strings, of the SB created drawing content folders
	public string[] InspirationsContentFolders 
	{ 
		get 
		{
			if(!Directory.Exists(InspirationsRootFolder))
			{
				Directory.CreateDirectory(InspirationsRootFolder);
			}
			
			return Directory.GetDirectories(InspirationsRootFolder);
		} 
	}
	
	// Returns a string, the location of user created drawings within the current user profile folder
	public string DrawingsRootFolder { get { return SB_UserProfileManager.instance.CurrentProfileFolder + DRAWINGS_FOLDER; }}
	
	// Returns an array of strings, of the drawing content folders
	public string[] DrawingsContentFolders 
	{ 
		get 
		{
			if(!Directory.Exists(DrawingsRootFolder))
			{
				Directory.CreateDirectory(DrawingsRootFolder);
			}
			
			return Directory.GetDirectories(DrawingsRootFolder);
		} 
	}
	
	/**
	 	Input local folder path:
	 	C:/Users/George/AppData/LocalLow/ScaryBeasties/Colouring/user1/drawings/patterns_01
	 
	 	Outputs drawingID: 
	 	patterns_01
	*/
	public string GetDrawingID(string folderPath)
	{
		string str = "";
		int DrawingIDStartIndex = folderPath.LastIndexOf("/");
		if(DrawingIDStartIndex > -1)
		{
			str = folderPath.Substring(DrawingIDStartIndex+1);
		}
		
		return str;
	}
	
	// Returns the total number of drawings the user has created
	public int GetNumberOfUserDrawings()
	{
		string[] drawingsContentFolders = DrawingManager.instance.DrawingsContentFolders;
		int numDrawings = 0;
		for (int n=0; n<drawingsContentFolders.Length; n++) 
		{
			string folder = drawingsContentFolders [n];
			folder = folder.Replace ("\\", "/");
			
			string drawingID = DrawingManager.instance.GetDrawingID (folder);
			DirectoryInfo[] dirs = DrawingManager.instance.GetDrawingDirectoriesByID (drawingID);
			if (dirs != null && dirs.Length > 0) 
			{
				numDrawings += dirs.Length;
			}
		}
		
		return numDrawings;
	}
	
	#region ScaryBeasties Inspiration drawings Getters
	public FileInfo[] GetInspriationFilesByID(string drawingID)
	{
		return GetDrawingFiles(InspirationsRootFolder + "/" + drawingID);
	}
	
	public DirectoryInfo[] GetInspriationDirectoriesByID(string drawingID)
	{
		return GetDrawingDirectories(InspirationsRootFolder + "/" + drawingID);
	}
	
	public FileInfo GetInspriationgXMLByID(string drawingID)
	{
		return GetDrawingXML(InspirationsRootFolder + "/" + drawingID);
	}
	
	public ArrayList GetInspriationPNGsByID(string drawingID)
	{
		return GetDrawingPNGs(InspirationsRootFolder + "/" + drawingID);
	}
	#endregion
	
	#region User drawing Getters
	public FileInfo[] GetDrawingFilesByID(string drawingID)
	{
		return GetDrawingFiles(DrawingsRootFolder + "/" + drawingID);
	}
	
	public DirectoryInfo[] GetDrawingDirectoriesByID(string drawingID)
	{
		return GetDrawingDirectories(DrawingsRootFolder + "/" + drawingID);
	}
	
	public FileInfo GetDrawingXMLByID(string drawingID)
	{
		return GetDrawingXML(DrawingsRootFolder + "/" + drawingID);
	}
	
	public ArrayList GetDrawingPNGsByID(string drawingID)
	{
		return GetDrawingPNGs(DrawingsRootFolder + "/" + drawingID);
	}
	#endregion
	
	// Return an array of FileInfo objects, for each file in the specified folderPath
	public FileInfo[] GetDrawingFiles(string folderPath)
	{
		FileInfo[] files = null;
		
		DirectoryInfo directory = new DirectoryInfo(folderPath);
		
		if(directory.Exists)
		{
			files = directory.GetFiles();
		}
		
		return files;
	}
	
	// Return an array of DirectoryInfo, sub directories for a given folderPath
	public DirectoryInfo[] GetDrawingDirectories(string folderPath)
	{
		DirectoryInfo[] dirs = null;
		
		DirectoryInfo directory = new DirectoryInfo(folderPath);
		
		if(directory.Exists)
		{
			//Debug.LogWarning("DIRECTORY FOUND " + folderPath);
			dirs = directory.GetDirectories();
		}
		else
		{
			//Debug.LogWarning("DIRECTORY DOES NOT EXIST " + folderPath);
		}
		
		return dirs;
	}
	
	// Returns a FileInfo object, for the 'story.xml' file in the specified folderPath
	public FileInfo GetDrawingXML(string folderPath)
	{
		Debug.LogWarning("GetDrawingXML - folderPath: " + folderPath);
		if(folderPath.Length == 0) return null;
		
		FileInfo[] files = GetDrawingFiles(folderPath);
		if(files == null || files.Length == 0) return null;
		
		foreach(FileInfo file in files)
		{
			if(file.Name.ToLower() == DRAWINGS_XML_FILENAME)
			{
				return file;
			}
		}
		
		return null;
	}
	
	// Returns an ArrayList of FileInfo objects, listing all the png's in the specified folderPath
	public ArrayList GetDrawingPNGs(string folderPath)
	{
		ArrayList pngs = new ArrayList();
		FileInfo[] files = GetDrawingFiles(folderPath);
		
		foreach(FileInfo file in files)
		{
			if(file.Extension.ToLower() == ".png")
			{
				pngs.Add(file);
			}
		}
		
		return pngs;
	}
	
	public void DeleteDrawing(string drawingID)
	{
		/*
		string path = DrawingsRootFolder + "/" + drawingID;
		
		DirectoryInfo dir = new DirectoryInfo(path);
		if(dir.Exists)
		{
			Debug.LogWarning("-----> About to delete folder: " + path);
			dir.Delete(true);
		}
		*/
	}
	
	public string SaveDrawingXML(string xml, string imageId, string creationId)
	{
		string path = DrawingsRootFolder + "/" + imageId + "/" + creationId;
		Debug.LogWarning("SaveDrawing path: " + path);
		
		DirectoryInfo dir = new DirectoryInfo(path);
		
		if( !dir.Exists )
		{
			Debug.Log("subdirectory doesnt exist, create subdirectory");
			dir.Create();
		}
		else
		{
			Debug.Log ("subdirectory already exists");
		}
		
		string filename = path + "/" + DRAWINGS_XML_FILENAME;
		Debug.LogWarning("Saving drawing XML to path: " + filename);
		
		File.WriteAllText(filename, xml, Encoding.UTF8);
		
		return filename;
	}
	
	public string SaveDrawingImage(byte[] imgBytes, string imageId, string creationId)
	{
		string path = DrawingsRootFolder + "/" + imageId + "/" + creationId;
		DirectoryInfo dir = new DirectoryInfo(path);
		
		if(!dir.Exists)
		{
			dir.Create();
		}
		string filename = path + "/" + imageId + "_fill.png";
		Debug.LogWarning("Saving drawing image to path: " + filename);
		File.WriteAllBytes(filename, imgBytes);
		return filename;
	}
	
	public string GetDrawingImage(string imageId, string folder)
	{
		
		Debug.LogWarning("GetDrawingImage() imageId: " + imageId + ", folder: " + folder);
		
		string path = folder + "/" + imageId + "_fill.png";
		path = path.Replace("\\", "/");
		
		Debug.LogWarning("GetDrawingImage() path: " + path);
		
		if(File.Exists(path))
		{
			Debug.LogWarning("GetDrawingImage() *** PATH EXISTS");
			return path;
		}
		
		return "";
	}
	
	public string SaveSharingImage(byte[] imgBytes, string imageId, string creationId)
	{
		string path = DrawingsRootFolder + "/" + imageId + "/" + creationId;
		DirectoryInfo dir = new DirectoryInfo(path);
		
		if(!dir.Exists)
		{
			dir.Create();
		}
		//string filename = path + "/" + imageId + "_sharing.png";
		string filename = path + "/" + imageId + "_sharing.jpg";
		Debug.LogWarning("Saving sharing image to path: " + filename);
		File.WriteAllBytes(filename, imgBytes);
		return filename;
	}
	
	public string GetSharingImage(string imageId, string folder)
	{
		//string path = folder + "/" + imageId + "_sharing.png";
		string path = folder + "/" + imageId + "_sharing.jpg";
		path = path.Replace("\\", "/");
		
		if(File.Exists(path))
		{
			return path;
		}
		
		return "";
	}

	public string GetSharingImagePath(string imageId, string creationId){
		string dir = DrawingsRootFolder + "/" + imageId + "/" + creationId;
		string path = dir + "/" + imageId + "_sharing.jpg";
		if(File.Exists(path))
		{
			return path;
		}
		
		return "";
	}
	
	public string SavePreviewImage(byte[] imgBytes, string imageId, string creationId)
	{
		string path = DrawingsRootFolder + "/" + imageId + "/" + creationId;
		DirectoryInfo dir = new DirectoryInfo(path);
		
		if(!dir.Exists)
		{
			dir.Create();
		}
		string filename = path + "/" + imageId + ".png";
		Debug.LogWarning("Saving drawing image to path: " + filename);
		File.WriteAllBytes(filename, imgBytes);
		return filename;
	}
	
	public string GetPreviewImage(string imageId, string folder)
	{
		string path = folder + "/" + imageId + ".png";
		path = path.Replace("\\", "/");
		
		if(File.Exists(path))
		{
			return path;
		}
		
		return "";
	}
	
	public string GetImageFolder(string imageId, string creationId)
	{
		string path = DrawingsRootFolder + "/" + imageId + "/" + creationId;
		return path;
	}
	
	/*
	public string[] GetCreationIds(string imageId)
	{
		//FileInfo 
	}
	*/
	
	// Return a string, a unique dated filename, to be used when creating story folders
	public string GenerateUniqueName()
	{
		DateTime now = DateTime.Now;
		
		// Use UTC time to genrerate filename
		//string str = prependWith + "_" + now.ToFileTimeUtc();
		string str = now.ToFileTimeUtc().ToString();
		
		return str;
	}
	
	/**
	 * Returns an ArrayList of valid story folders, and
	 * deletes any orphaned folders (ones which have no images and are still on the file system)
	 */
	public ArrayList SanitizeFolders()
	{
		ArrayList validFolders = new ArrayList();
		foreach(string stFolder in DrawingsContentFolders)
		{
			// Check that each folder has png's in it
			if(GetDrawingPNGs(stFolder).Count > 0)
			{
				validFolders.Add(stFolder);
			}
			else
			{
				Directory.Delete(stFolder, true);
			}
		}
		
		return validFolders;
	}
}
