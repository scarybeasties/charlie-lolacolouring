﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.InAppPurchasing.Shop;
using System.Xml;
using ScaryBeasties.Utils;

public class ColouringPackItem : SB_PackItem 
{
	public int numItems = 0; // Number of child items
	public Color colour;
	
	public ColouringPackItem(XmlElement xml) : base(xml)
	{
		numItems = int.Parse(xml.SelectSingleNode("num_items").InnerText);
		colour = SB_Utils.HexToColour(xml.SelectSingleNode("colour").InnerText);
	}
	
	override public string ToString()
	{
		string str = base.ToString();
		str += ", numItems: " + numItems;
		str += ", colour: " + colour.ToString();
		
		return str;
	}
}
