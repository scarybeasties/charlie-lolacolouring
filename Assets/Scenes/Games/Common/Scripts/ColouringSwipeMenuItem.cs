﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.UI.SwipeMenu;
using ScaryBeasties.InAppPurchasing.Shop;
using System.IO;
using ScaryBeasties.SB2D;
using ScaryBeasties;
using ScaryBeasties.Utils;
using ScaryBeasties.Games.Colouring;

public class ColouringSwipeMenuItem : SwipeMenuItem
{
    public ColouringAssetType assetType;
    public ColouringPackAsset assetData;

    public void SetItem(ColouringPackAsset packAsset)
    {

        assetType = packAsset.subtype;
        assetData = packAsset;
        id = packAsset.id;

        string imageFileName = SB_ShopUtils.GetLocalAssetDirectoryPath() + "/" + packAsset.assetName;
        string carouselImageFileName = ColouringPackManager.instance.GetCarouselImageFilename(imageFileName);


        if (File.Exists(carouselImageFileName))
        {
            SB2DScaledSprite spr = SB2DSpriteMaker.CreateScaledSprite(transform.Find("Content").gameObject);
            spr.LoadImage("file://" + carouselImageFileName);
            GameObject itemGameObj = spr.gameObject;
            itemGameObj.transform.parent = transform.Find("Content").gameObject.transform;
            SpriteRenderer trend = itemGameObj.GetComponent<SpriteRenderer>();
            trend.sortingOrder = 14;

        }
        else
        {
            Debug.LogError("NOT FOUND " + carouselImageFileName);
        }

        if (!packAsset.locked)
        {
            transform.Find("Lock").gameObject.SetActive(false);
        }
        else
        {
            transform.Find("Lock").gameObject.SetActive(true);
        }

    }

    protected void OnDestroy()
    {
        assetData = null;
        base.OnDestroy();
    }


}
