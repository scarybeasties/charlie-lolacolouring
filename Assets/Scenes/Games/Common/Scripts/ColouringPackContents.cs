﻿using System.Collections;
using System.Xml;
using ScaryBeasties.InAppPurchasing.Local;

/**
 * Class file to store the contents of each parsed pack XML file
*/
using ScaryBeasties.Games.Colouring;
using System.IO;
using UnityEngine;
using ScaryBeasties.Utils;


public class ColouringPackContents : SB_PackContents 
{
	private ArrayList _colouring = new ArrayList();
	protected string _packId;
	protected string _title;
	protected Color _colour;
	
	// Constructor
	public ColouringPackContents(XmlDocument xml)
	{
		Init(xml);
	}

	override protected void Init(XmlDocument xml)
	{
		base.Init(xml);

		_title = xml.SelectSingleNode("pack/config/title").InnerText;
		_colour = SB_Utils.HexToColour(xml.SelectSingleNode("pack/config/colour").InnerText);

		// Setup the pack assets, by passing the xml node path to the assets
		SetupPackAssets ("pack/colouring/asset", _colouring);
	}
	
	// Parse XML nodes
	override protected void SetupPackAssets(string assetPath, ArrayList arr)
	{
		foreach(XmlNode xmlNode in _xml.SelectNodes (assetPath))
		{
			//Debug.Log("Found background " + xmlNode.InnerText);

			ColouringPackAsset packAsset = new ColouringPackAsset();

			packAsset.ParseXML(xmlNode);
			if(arr == _colouring) 			packAsset.type = ColouringAssetType.Colouring;
			
			arr.Add (packAsset);
		}
	}

	// Getters
	public ArrayList Colouring		{	get	{return _colouring;}	}
	public string Title				{	get	{return _title;}	}
	public Color Colour				{	get	{return _colour;}	}
	
	override public string ToString()
	{
		string str = "";
		str += "_title: " + _title + "\n";
		str += "_colour: " + _colour + "\n";
		str += "Colouring: (" + Colouring.Count + ") \n";
		foreach(ColouringPackAsset packAsset in Colouring) str += packAsset.ToString() + "\n";
		
		return str;
	}
}
