﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ColouringPackVersionChecker {
	
	public const string PACK_VERSION_CHECK_KEY = "ColouringPackVersion";
	
	public static bool HasPackVersionChanged()
	{
		string lastPackVersion = PlayerPrefs.GetString(PACK_VERSION_CHECK_KEY,"1.0");

		Debug.Log("===================================== Last Pack Version: " + lastPackVersion + " CurentBundleVersion:: " + CurrentBundleVersion.version );

		if(lastPackVersion != CurrentBundleVersion.version)
		{
			PlayerPrefs.SetString(PACK_VERSION_CHECK_KEY,CurrentBundleVersion.version);
			return true;
		}
		
		return false;
	}
}
