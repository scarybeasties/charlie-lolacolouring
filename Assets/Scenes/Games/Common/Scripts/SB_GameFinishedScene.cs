﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Controls;
using ScaryBeasties.Base;
using ScaryBeasties.Utils;
using ScaryBeasties.Games.Common.UI;
using ScaryBeasties.Stickers;
using ScaryBeasties.UI;
using System.Collections.Generic;
using ScaryBeasties.User;
using ScaryBeasties.Sound;
using ScaryBeasties.History;

namespace ScaryBeasties.Scenes
{
	public class SB_GameFinishedScene : SB_GameScene  
	{
		public const string STATE_SCORE_COUNT = "stateScoreCount";

		protected bool _showScoreCount = true; // Some game end screens do not have a score count
		protected int _score = 0;
		protected int _stickerId = -1; // The id of the newly won sticker // TODO: Get the sticker id from the available stickers for this game

		protected int _scoreDisplaySecs = 2;
		protected float _scoreIncrementAmount = 1.0f; // This is a calculated value
		protected float _scoreCount = 0.0f;
		protected int _scoreInt = 0;

		private float updateInterval = 0.2f;
		private float accum = 0.0f; // FPS accumulated over the interval
		private int frames = 0; // Frames drawn over the interval
		private float timeleft; // Left time for current interval

		// Store reference to the sticker
		protected tk2dSprite _stickerSprite;
		protected GameObject _stickerHolder;

		protected StickerDataVO _stickerDataVO; // Stores the ValueObject for the collected sticker

		protected float _idleCounter=7.0f;
		protected float _idleMax=14.0f;

		override protected void Start()
		{
			_addSceneToHistory = false;
			_isGameEndScreen = true;

			base.Start ();

			if(SCENE_CONFIG_OBJECT != null)
			{
				if(SCENE_CONFIG_OBJECT.GetValue("showScoreCount") != null && (bool)SCENE_CONFIG_OBJECT.GetValue("showScoreCount") == false)
				{
					_showScoreCount = false;
				}

				if(SCENE_CONFIG_OBJECT.GetValue("score") != null)
				{
					_score = (int)SCENE_CONFIG_OBJECT.GetValue("score");
				}
			}

			_stickerId = GetNextSticker();

			_stickerHolder = transform.Find("Panel/StickerHolder").gameObject;
			if(_stickerHolder != null)
			{
				_stickerHolder.SetActive(false);

				// Only show the sticker if id is greater than -1
				if(_stickerId > -1)
				{
					tk2dSprite star = _stickerHolder.transform.Find("star").gameObject.GetComponent<tk2dSprite>();
					_stickerSprite = _stickerHolder.transform.Find("sticker").gameObject.GetComponent<tk2dSprite>();

					StickerManager sm = StickerManager.instance;
					_stickerDataVO = sm.GetSticker(_stickerId);

					if(_stickerDataVO.enabled)
					{
						warn("User has won sticker: " + _stickerDataVO.name);
						sm.CollectSticker(_stickerId);

						// Show the relevant sticker
						_stickerSprite.SetSprite(_stickerDataVO.name);

						// Scale the sticker, so it fits within the bounds of the holder
						Vector2 originalStickerSize = _stickerSprite.GetUntrimmedBounds().size;
						Vector2 newStickerSize = star.GetUntrimmedBounds().size;
								newStickerSize.x *= 0.7f;
								newStickerSize.y *= 0.7f;

						Vector2 resizedScale = SB_Utils.Resize(originalStickerSize, newStickerSize);
						if(resizedScale.x<=1.0f){
							_stickerSprite.transform.localScale = resizedScale;
						}


						//if(num==3) SB_SoundManager.instance.PlayVoiceQueued("Localised/VO/Narrator/vo_gen_youwon");

					}
				}
				else{
					//SB_SoundManager.instance.PlayVoiceQueued("Localised/VO/Narrator/vo_gen_donow");
				}
			}
			
			// Define the animation states (clips) which exist in this scene
			_animationStates = new List<string>(new string[] {"Intro", "Outro"});

			_uiControls.ShowButtons(new List<string>(new string[]  {SB_UIControls.BTN_MENU, 
																	SB_UIControls.BTN_PAUSE}));
		}

		protected void resetIdle(){
			_idleCounter=_idleMax;
			warn ("RESET DONE");
		}

		/**
		 * Returns the ID of the next sticker reward, which has yet to be collected by the current user.
		 * If user has collected all the stickers for this game, a -1 is returned, and the sticker animation
		 * is not shown.
		 */ 
		int GetNextSticker()
		{
			StickerManager sm = StickerManager.instance;
			char[] splitchar = { ',' };
			string[] collectedStickers = SB_UserProfileManager.instance.CurrentProfile.collectedStickers.Split(splitchar);

			for(int s=0; s<_stickerIDs.Length; s++)
			{



				int newSticker = _stickerIDs[s];

				StickerDataVO sdata = sm.GetSticker(newSticker);

				if(sdata.enabled){

					bool stickerAlreadyCollected = false;

					for(int n=0; n<collectedStickers.Length; n++)
					{
						int collectedStickerId = int.Parse(collectedStickers[n]);
						if(collectedStickerId == newSticker)
						{
							stickerAlreadyCollected = true;
							break;
						}
					}

					if(!stickerAlreadyCollected)
					{
						return newSticker;
					}
				}
			}


			return -1;
		}

		override public void IntroComplete()
		{
			base.IntroComplete();
			SetState(STATE_SCORE_COUNT);

			if(_stickerSprite != null && _stickerDataVO.enabled)
			{
				_stickerHolder.SetActive(true);
				_stickerHolder.GetComponent<Animator>().Play("Intro");

				int num = 1+(int)UnityEngine.Random.Range(0, 2);
				if(num==1) SB_SoundManager.instance.PlayVoiceQueued("Localised/VO/Narrator/vo_gen_sticker");
				if(num==2) SB_SoundManager.instance.PlayVoiceQueued("Localised/VO/Narrator/vo_gen_wonbuilder");
			}
		}

		override protected void Init()
		{
			if(_showScoreCount)
			{
				// Calculate increment amount
				_scoreIncrementAmount = (float)_score/(_scoreDisplaySecs/updateInterval);
				timeleft = updateInterval;  
			}
		}

		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{

			base.OnBtnHandler(btn, eventType);

			if(eventType == SB_Button.MOUSE_UP)
			{
				resetIdle();
				switch(btn.name)
				{
					case "menu_btn":
						LogButtonEvent("menu");
						PlayOutroAndLoad(SB_Globals.SCENE_MAIN_MENU);
						break;
						
					case "playagain_btn":
						// Handled by subclass
						LogButtonEvent("playagain");
						break;
					
					case "nextgame_btn":
						LogButtonEvent("nextgame");
						PlayOutroAndLoad(GetNextGame());
						break;
				}
			}
		}

		string GetNextGame ()
		{
			int startIndex = 0;
			int endIndex = Application.loadedLevelName.IndexOf("Game");
			if(endIndex < 0) endIndex = Application.loadedLevelName.Length-1;

			int len = SB_Globals.GAME_ORDER.Length-1;
			for(int n=0; n<len; n++)
			{
				string gameNameSubStr = Application.loadedLevelName.Substring(startIndex, endIndex);

				log ("SB_Globals.GAME_ORDER["+n+"]: " + SB_Globals.GAME_ORDER[n] + ", Application.loadedLevelName: " + Application.loadedLevelName + ", gameNameSubStr: " + gameNameSubStr);				
				if(SB_Globals.GAME_ORDER[n].IndexOf(gameNameSubStr) > -1)
				{
					return SB_Globals.GAME_ORDER[n+1];
				}
			}

			// This should never actually happen, but we need to return a default
			return Application.loadedLevelName;
		}

		protected virtual void PlayIdle(){
			SB_SoundManager.instance.PlayVoiceQueued("Localised/VO/Narrator/vo_gen_donow");
			resetIdle ();
		}

		override protected void Update () 
		{
			base.Update ();

			_idleCounter-=Time.deltaTime;

			if(_idleCounter<=0){
				PlayIdle();
			}
			
			switch(_state)
			{
				case STATE_SCORE_COUNT: 	UpdateScoreCount();	break;
			}
		}

		protected virtual void UpdateScoreCount () 
		{
			timeleft -= Time.deltaTime;
			accum += Time.timeScale/Time.deltaTime;
			++frames;

			if( timeleft <= 0.0 )
			{
				if(_scoreCount < _score)
				{
					_scoreCount += _scoreIncrementAmount;
					if(_scoreCount > _score)
					{
						_scoreCount = _score;
					}

					_scoreInt = (int)_scoreCount;
				}

				timeleft = updateInterval;
				accum = 0.0f;
				frames = 0;
			}
		}
		
		override protected void OnDestroy()
		{
			_stickerSprite = null;
			base.OnDestroy();
		}
	}
}
