﻿using ScaryBeasties.InAppPurchasing.Local;
using System.Xml;
using ScaryBeasties.InAppPurchasing.Shop;
using System;
using ScaryBeasties.User;
using ScaryBeasties.Games.Colouring;

public class ColouringPackAsset : SB_PackAsset
{
    public ColouringAssetType type;
    public ColouringAssetType subtype;

    public string colourHex = "";   // Used for Biography background colour

    // These values are used for assets which have text (Speech bubbles, biographies..)
    public string titleText = "";
    public string bodyText = "";

    public string BodyFontPrefab = "TextAreaPrefab";
    public int BodyFontSize = 40;
    public int BodyFontMinSize = 10;
    public int BodyFontMaxSize = 40;
    public string BodyFontColourHex = "#000000";
    public float BodyFontWidthPercent = 0.8f;
    public float BodyFontHeightPercent = 0.6f;
    public float XPos = 0f;
    public float YPos = 0f;


    public ColouringPackAsset()
    {

    }

    override public string FileName
    {
        get
        {
            /*
			if(type == DrWhoAssetType.MonsterMakerCreations)
			{
				// Monster maker creations are saved in User profile folder
				return "file:///" + SB_UserProfileManager.instance.CurrentProfileFolder + "/" + assetName;
			}
			*/
            return "file://" + SB_ShopUtils.GetLocalAssetDirectoryPath() + "/" + assetName;
        }
    }

    public virtual string AudioImageFileName
    {
        get
        {
            return "file://" + SB_ShopUtils.GetLocalAssetDirectoryPath() + "/sounds/c_audio.png";
        }
    }

    public override void ParseXML(XmlNode xmlNode)
    {
        base.ParseXML(xmlNode);

        if (xmlNode.Attributes.GetNamedItem("subtype") != null)
        {
            string stype = xmlNode.Attributes.GetNamedItem("subtype").Value;
            subtype = (ColouringAssetType)Enum.Parse(typeof(ColouringAssetType), stype);
        }

        if (xmlNode.Attributes.GetNamedItem("fontPrefab") != null)
        {
            BodyFontPrefab = xmlNode.Attributes.GetNamedItem("fontPrefab").Value;
        }

        if (xmlNode.Attributes.GetNamedItem("fontColour") != null)
        {
            BodyFontColourHex = xmlNode.Attributes.GetNamedItem("fontColour").Value.ToLower();
        }

        if (xmlNode.Attributes.GetNamedItem("fontSize") != null)
        {
            BodyFontSize = int.Parse(xmlNode.Attributes.GetNamedItem("fontSize").Value);
            BodyFontMaxSize = BodyFontSize;
        }

        if (xmlNode.Attributes.GetNamedItem("fontMinSize") != null)
        {
            BodyFontMinSize = int.Parse(xmlNode.Attributes.GetNamedItem("fontMinSize").Value);
        }

        if (xmlNode.Attributes.GetNamedItem("widthPercent") != null)
        {
            BodyFontWidthPercent = float.Parse(xmlNode.Attributes.GetNamedItem("widthPercent").Value);
        }

        if (xmlNode.Attributes.GetNamedItem("heightPercent") != null)
        {
            BodyFontHeightPercent = float.Parse(xmlNode.Attributes.GetNamedItem("heightPercent").Value);
        }

        if (xmlNode.Attributes.GetNamedItem("text") != null)
        {
            bodyText = xmlNode.Attributes.GetNamedItem("text").Value;
        }

        if (xmlNode.Attributes.GetNamedItem("x") != null)
        {
            XPos = float.Parse(xmlNode.Attributes.GetNamedItem("x").Value);
        }

        if (xmlNode.Attributes.GetNamedItem("y") != null)
        {
            YPos = float.Parse(xmlNode.Attributes.GetNamedItem("y").Value);
        }
    }

    override public string ToString()
    {
        return base.ToString() + ", type: " + type + ", subtype: " + subtype + ", colourHex: " + colourHex + ",\ntitleText: " + titleText + ",\nbodyText: " + bodyText + ", locked: " + locked;
    }
}