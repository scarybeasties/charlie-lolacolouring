﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.InAppPurchasing.Local;
using System.Collections.Generic;
using ScaryBeasties.Games.Colouring;
using System.Xml;
using System.Linq;
using System;
using ScaryBeasties.Data;
using System.IO;
using ScaryBeasties.InAppPurchasing.Shop;
using ScaryBeasties;

public class ColouringPackManagerQueuedCall
{
	public string function;
	public ColouringAssetType assetType;
	public MonoBehaviour monobehaviour;
	public Action<List<ColouringPackAsset>> callback;
	public string filterById = "";
}


public class ColouringPackManager 
{
	const string FUNC_GET_PACK_ASSET_LIST = "GetPackAssetList";

	private bool _XMLsLoaded = false;
	private ArrayList _onXMLLoadedCallbackQueue = new ArrayList();
	private bool _unzipping = false;
	
	private static ColouringPackManager _instance = null;
	public static ColouringPackManager instance
	{
		get 
		{
			if(_instance == null)
			{
				_instance = new ColouringPackManager();
			}
			
			return _instance;
		}
	}
	
	/**
	 * Constructor - Should never be called directly.
	 * Use 'ColouringPackManager.instance' to get the singleton
	 */
	public ColouringPackManager()
	{
		warn("***************************************************");
		warn("New ColouringPackManager instance created!!!!!");
		warn("..about to load all pack XML's");
		warn("***************************************************");
		SB_LocalPackManager.instance.LoadAllPackXMLs(OnAllPackXMLsLoaded);
	}
	
	public bool DefaultPackInstalled()
	{
		string assetDirPath = SB_ShopUtils.GetLocalAssetDirectoryPath();
		string defaultPackXML = assetDirPath + "/xml/" + SB_Globals.LOCALE_STRING + "/default_pack.xml";
		if(!Directory.Exists(assetDirPath) || !File.Exists(defaultPackXML))
		{
			return false;
		}

		if(_unzipping)
		{
			return false;
		}
		
		return true;
	}
	
	/**
	 * Check if default pack files are installed.
	 * A monobehaviour is required, to run a coroutine.
     */
	public bool CheckDefaultPackInstalled(MonoBehaviour monobehaviour)
	{
		if(!DefaultPackInstalled() || ColouringPackVersionChecker.HasPackVersionChanged())
		{	
			warn("***************************************************");
			//string defaultPackZip = Application.streamingAssetsPath + "/Packs/default_pack/default_pack_" + tk2dSystem.CurrentPlatform + ".zip";
			
			// NOTE: For this colouring app, we are using the 2x zip for all users, even the people who are using 1x devices!
			string defaultPackZip = Application.streamingAssetsPath + "/Packs/default_pack/default_pack_2x.zip";
			
			warn("defaultPackZip: " + defaultPackZip);
			warn("Application.streamingAssetsPath: " + Application.streamingAssetsPath);
			warn("Application.dataPath: " + Application.dataPath);
			warn("Application.persistentDataPath: " + Application.persistentDataPath);
			
			//RecursesiveFolderCopy(defaultPackFolder, Application.persistentDataPath);
			
			#if UNITY_EDITOR
				ZipUtil.Unzip(defaultPackZip, Application.persistentDataPath);
			
				SB_LocalPackManager.instance.LoadAllPackXMLs(OnAllPackXMLsLoaded);

				//monobehaviour.StartCoroutine(DoUnzipDefaultPack(defaultPackZip));
			#elif UNITY_ANDROID
				//defaultPackZip = "jar:file://" + Application.dataPath + "!/assets/Packs/default_pack/default_pack_" + tk2dSystem.CurrentPlatform + ".zip";
			defaultPackZip = "jar:file://" + Application.dataPath + "!/assets/Packs/default_pack/default_pack_2x.zip";
				monobehaviour.StartCoroutine(DoUnzipDefaultPack(defaultPackZip));
			#else
				//defaultPackZip = "file:" + Application.dataPath + "/Raw/Packs/default_pack/default_pack_" + tk2dSystem.CurrentPlatform + ".zip";
			defaultPackZip = "file:" + Application.dataPath + "/Raw/Packs/default_pack/default_pack_2x.zip";
				monobehaviour.StartCoroutine(DoUnzipDefaultPack(defaultPackZip));
				
				iPhone.SetNoBackupFlag(defaultPackZip);
			#endif
			
			warn("***************************************************");
			
			return false;
		}
		else
		{
			warn ("\t\tDefault assets found");
		}
		
		return true;
	}
	
	/**
	 * Returns the location of the default_pack.zip file, when it's
	 * been copied from StreamingAssets, over to Application.persistentDataPath
	 */
	string DefaultPackZipPath()
	{
		return Application.persistentDataPath + "/default_pack.zip";
	}

	IEnumerator DoUnzipDefaultPack (string filePath)
	{
		_unzipping = true;
		//error ("DoUnzipDefaultPack filePath: " + filePath);
		WWW www = new WWW (filePath);
		yield return www;
		
		error ("www.bytes.Length: " + www.bytes.Length);
		
		if (string.IsNullOrEmpty (www.error)) 
		{
			error ("ZIP FILE LOADED");
			
			// Copy the zip file to the device (persistent storage), before we can unzip it
			//string zipFileCopied = Application.persistentDataPath + "/default_pack.zip";
			File.WriteAllBytes(DefaultPackZipPath(), www.bytes);
		
			ZipUtil.Unzip(DefaultPackZipPath(), Application.persistentDataPath);
		}
		else
		{
			error ("ZIP FILE LOADED ERRRRRRRRRRRROR");
		}
		
//		if(!_XMLsLoaded)
		{
			
			warn("***************************************************");
			warn("DoUnzipDefaultPack()  ..about to load all pack XML's");
			warn("***************************************************");
			yield return new WaitForEndOfFrame();
			yield return new WaitForEndOfFrame();
			yield return new WaitForEndOfFrame();
			SB_LocalPackManager.instance.LoadAllPackXMLs(OnAllPackXMLsLoaded);
		}
		_unzipping = false;
	}
	
	/**
	 * Once pack xml files have been loaded, we can preload the carousel items,
	 * and then setup the carousel
	 */
	private void OnAllPackXMLsLoaded (bool success)
	{
		if(File.Exists(DefaultPackZipPath()))
		{
			File.Delete(DefaultPackZipPath());
		}
		
		if(success)
		{
			warn("***************************************************");
			warn("ALL PACK XML's LOADED!!");
			warn("***************************************************");
			_XMLsLoaded = true;
			ProcessQueuedCalls();
			
			
			string assetPath = Application.persistentDataPath + "/assets";
			string inspirationPath = Application.persistentDataPath + "/user1/inspiration";
			log ("<color=orange> IOS - setting no backup flag for path: " + assetPath + "</color>");
			log ("<color=orange> IOS - setting no backup flag for path: " + inspirationPath + "</color>");
			#if UNITY_IOS
			
			// Don't backup the assets
			iPhone.SetNoBackupFlag(assetPath);
			
			// Do not backup the inspiration folder either!
			iPhone.SetNoBackupFlag(inspirationPath);
			#endif
		}
		else
		{
			warn("***************************************************");
			error ("Failed to load one or more pack xml files!!!");
			warn("***************************************************");
		}
	}

	void ProcessQueuedCalls ()
	{
		foreach(ColouringPackManagerQueuedCall queuedCall in _onXMLLoadedCallbackQueue)
		{
			if(queuedCall.function == FUNC_GET_PACK_ASSET_LIST)
			{
				GetPackAssetList(queuedCall.assetType, queuedCall.callback, queuedCall.filterById);
			}
		}
		
		_onXMLLoadedCallbackQueue.Clear();
	}

	/**
	 * Returns a ColouringPackAsset, by doing a lookup of a a given 'assetType' using the 'file' name
	 */
	public void GetPackAsset(ColouringAssetType assetType, string file, Action<List<ColouringPackAsset>> callback)
	{
		GetPackAssetList(assetType, callback, file);
	}

	/**
	 * Returns a List of ColouringPackAsset objects, for a given asset type.
	 * This is done immidiately (not via a callback function)
	 */
	public List<ColouringPackAsset> GetPackAssetList(ColouringAssetType assetType, string assetFilterByID="")
	{
		//warn ("GetPackAssetList assetType: " + assetType + ", assetFilterByID: " + assetFilterByID + ", _XMLsLoaded: " + _XMLsLoaded);		
		return DoGetPackAssetList(assetType, assetFilterByID);
	}

	
	/**
	 * Returns a List of ColouringPackAsset objects, for a given asset type.
	 * Items in the list are ordered by their 'priority' value, which is set in each pack.xml
	 */
	public void GetPackAssetList(ColouringAssetType assetType, Action<List<ColouringPackAsset>> callback, string assetFilterByID="")
	{
		//warn ("GetPackAssetList assetType: " + assetType + ", assetFilterByID: " + assetFilterByID + ", _XMLsLoaded: " + _XMLsLoaded);
		
		// If XML hasn't loaded yet, add it to the callback queue
		if(!_XMLsLoaded)
		{
			ColouringPackManagerQueuedCall queuedCall = new ColouringPackManagerQueuedCall{	function = FUNC_GET_PACK_ASSET_LIST, 
																					assetType=assetType, 
																					callback=callback, 
																					filterById=assetFilterByID};
			_onXMLLoadedCallbackQueue.Add(queuedCall);
			
			SB_LocalPackManager.instance.LoadAllPackXMLs(OnAllPackXMLsLoaded);
			return;
		}
		
		if(callback != null) callback(DoGetPackAssetList(assetType, assetFilterByID));
	}
	
	List<ColouringPackAsset> DoGetPackAssetList(ColouringAssetType assetType, string assetFilterByID="")
	{
		List<ColouringPackAsset> packAssetList = new List<ColouringPackAsset>();
		
		KeyValueObject kvTemp = new KeyValueObject();
		
		// Get an array of all available pack ID's
		ArrayList allPackIDs = SB_LocalPackManager.instance.GetAllAvailablePackIDs();
		foreach(string packID in allPackIDs)
		{
			//log("** PACK ID: " + packID);
			XmlDocument packXML = SB_LocalPackManager.instance.GetPackXML(packID);
			ColouringPackContents packData =  new ColouringPackContents(packXML);
			
			//warn("** PACK ID: " + packID + ", packXML " + packXML.ToString());
			
			ArrayList arr = new ArrayList();
			switch(assetType)
			{
				case ColouringAssetType.Colouring:		arr = packData.Colouring; 		break;
			}
			
			//warn("-----------PackId: " + packData.PackId + ", packData.Monsters: " + packData.Monsters.Count);
			
			// We only want 1 instance of each PackAsset in the list, so add it to a temporary KeyValueObject, indexed by the asset's filename
			foreach(ColouringPackAsset packAsset in arr)
			{
				bool assetIdMatchesFilter = (assetFilterByID.Length > 0 && packAsset.id.IndexOf(assetFilterByID) > -1);
				bool assetNameMatchesFilter = (assetFilterByID.Length > 0 && packAsset.assetName.IndexOf(assetFilterByID) > -1);
				
				if(assetFilterByID.Length == 0 || (assetIdMatchesFilter || assetNameMatchesFilter))
				{
					//warn("FOUND A PACK ASSET WHICH MATCHES ID " + assetFilterByID + ", FILENAME: " + packAsset.FileName + ", assetName: " + packAsset.assetName);
					kvTemp.SetKeyValue(packAsset.FileName, packAsset);
				}
				else
				{
					//warn("...DOES NOT MATCH ID " + assetFilterByID + ", FILENAME: " + packAsset.FileName + ", assetName: " + packAsset.assetName);
				}
			}
		}
		
		// Itereate through the kvTemp object and add each PackAsset to the list
		foreach(string key in kvTemp.Keys)
		{
			packAssetList.Add((ColouringPackAsset)kvTemp.GetValue(key));
		}
		
		// Sort the list, by order of the ColouringPackAsset 'priority' property
		//packAssetList = packAssetList.OrderBy(a => a.priority).ToList();
		//packAssetList.Reverse();
		
		packAssetList = packAssetList.OrderByDescending(a => a.priority).ToList();
		return packAssetList;
	}
	
	public ColouringPackContents GetPackContents(string packID)
	{
		XmlDocument packXML = SB_LocalPackManager.instance.GetPackXML(packID);
		if(packXML != null)
		{
			ColouringPackContents packData =  new ColouringPackContents(packXML);
			return packData;
		}
		
		return null;
	}
	
	/**
	 * Returns a string, with the filename prefixed with a 'c_'
	 * (All carousel images should be prefixed with a 'c_' in their filename)
	 */
	public string GetCarouselImageFilename(string assetFilePath)
	{
		int index = assetFilePath.LastIndexOf("/")+1;
		if(index < 0) index = 0;
		return assetFilePath.Insert(index, "c_");
	}
	
	void log(string msg)
	{
		Debug.Log("[ColouringPackManager] " + msg);
	}
	
	void warn(string msg)
	{
		Debug.LogWarning("[ColouringPackManager] " + msg);
	}
	
	void error(string msg)
	{
		Debug.LogError("[ColouringPackManager] " + msg);
	}
}
