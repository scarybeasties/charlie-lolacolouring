using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ScaryBeasties.Controls;
using ScaryBeasties.Base;
using ScaryBeasties.History;
using ScaryBeasties.Popups;
using ScaryBeasties.UI;
using ScaryBeasties.Games.Common;
using ScaryBeasties.Games.Common.VO;
using System.Xml;
using System;
using ScaryBeasties.Data;
using ScaryBeasties.User;
using ScaryBeasties.Sound;
using ScaryBeasties.Utils;

namespace ScaryBeasties.Scenes
{
	public class SB_GameScene : SB_BaseScene 
	{
		public const string STATE_GAME_OVER = "stateGameOver";

		// Has user paused the game?
		//protected bool _userPausedGame = false;

		protected int _skillLevel = 0;
		protected int _gameScore = 0;

		// A Value Object to store properties for the Game Help popup. This is set by the each game's main class
		protected SB_PopupVO _gameHelpPopupVO = null;

		// Store the prefab GameObjects, while they are in use
		protected GameObject _difficultySelect = null;
		protected DifficultySelect _difficultySelectScript = null;
		protected GameObject _skipButtonGameObject;

		protected List<string> _uiButtons = new List<string>(new string[]  {SB_UIControls.BTN_MENU, SB_UIControls.BTN_PAUSE});

		// Store the config node XML for this game
		protected XmlNode _gameXML = null;
		protected int _gameId;
		protected string _gameName = "";
		protected bool _levelslocked = false;
		protected int[] _stickerIDs;

		// Key/Value object to store user generated content (paths to images)
		//protected KeyValueObject _userContent;
		protected GameDataVO _gameDataVO;
		protected float _gameTimePlayed;

		protected bool _isGameEndScreen = false;

		protected float _idleCount = 10f;
		protected float _maxIdle = 10f;
		protected int _voiceIdleCount = 0;
		protected int _maxVoiceIdleCount = 0;
		
		override protected void Start()
		{
			base.Start ();
			_uiControls.ShowButtons(_uiButtons);
			ParseGameXML();
			_gameDataVO = GetGameDataVO();

			// If there's a skip intro button in this scene then we need to set it active straight away
			_skipButtonGameObject = GameObject.Find("btn_skip");
			if(_skipButtonGameObject != null) 
			{
				SB_Button skipButton = _skipButtonGameObject.GetComponent<SB_Button>();
				//warn ("btn_skip found");
				skipButton.isActive = true;
				_buttons.Add(skipButton);

				//RemoveDelegates();
				//AddDelegates();
			}
			else warn ("btn_skip not found in this scene!");

			if(_gameDataVO == null) error("_gameDataVO is null!!!!");
			else _gameDataVO.hasBeenPlayed = true;



			warn ("_isGameEndScreen: " + _isGameEndScreen);
		}

		void OnApplicationPause(bool val)
		{
			error ("OnApplicationPause " + val);
			if(val)
			{
				if(State() != SB_BaseScene.STATE_PAUSED)
				{
					ShowPausePopup();
				}
			}
		}

		protected void ParseGameXML()
		{
			if(_gameXML == null) 
			{
				//throw(new NullReferenceException("SB_GameScene.ParseGameXML() :: Error - game xml is null!"));
				error("SB_GameScene.ParseGameXML() :: Error - game xml is null!");
				return;
			}

			_gameId 		= int.Parse(_gameXML.Attributes.GetNamedItem("id").Value);
			_gameName 		= _gameXML.Attributes.GetNamedItem("name").Value;
			_levelslocked	= bool.Parse(_gameXML.Attributes.GetNamedItem("levelslocked").Value);
			string stickers = _gameXML.Attributes.GetNamedItem("stickers").Value;

			char[] splitchar = { ',' };
			string[] stickerIdStrings = stickers.Split(splitchar);
			_stickerIDs = new int[stickerIdStrings.Length];

			if(stickers.Length > 0)
			{
				int n=0;
				foreach(string s in stickerIdStrings)
				{
					_stickerIDs[n] = int.Parse(s);
					warn ("STICKER "+_stickerIDs[n]);
					n++;
				}
			}

			warn("----------------------");
			warn("_gameId: " + _gameId);
			warn("_gameName: " + _gameName);
			warn("_levelslocked: " + _levelslocked);
			warn("_stickerIDs: " + stickers);
			warn("----------------------");
		}

		/**
		 * Find the GameDataVO for this game, from the current user profle 
		 */
		protected GameDataVO GetGameDataVO(int gameId=-1)
		{
			if(gameId == -1) gameId = _gameId;

			SB_ProfileVO currentProfile = SB_UserProfileManager.instance.CurrentProfile;
			foreach(GameDataVO gameDataVO in currentProfile.gameData)
			{
				if(gameDataVO.id == gameId)
				{
					return gameDataVO;
				}
			}

			return null;
		}

		protected virtual void InitGame()
		{
			warn ("InitGame");
			if(!_isGameEndScreen)
			{
				_gameTimePlayed = 0;
				_gameDataVO.sessionSecsPlayed = 0;
				_gameDataVO.numberOfPlays++;
			}
		}

		// Show difficulty selection
		protected virtual void ShowDifficultySelect(string resourceName, string animationClipName)
		{
			_difficultySelect = CreatePrefabGameObject(resourceName);
			_difficultySelectScript = _difficultySelect.GetComponent<DifficultySelect>();
			AddViewDelegates(_difficultySelectScript);
			
			if(_levelslocked)
			{
				_difficultySelectScript.LockButtons(_gameDataVO.skillLevel, _gameDataVO.unlockRequired);
				_gameDataVO.unlockRequired = false;
			}
			
			_animationController.PlayAnimation(animationClipName);
		}
		/*
		override protected void UpdateIntroComplete()
		{
			if(_levelslocked)
			{
				_difficultySelectScript.LockButtons(_gameDataVO.skillLevel, _gameDataVO.unlockRequired);
				_gameDataVO.unlockRequired = false;
			}

			base.IntroComplete();
		}
		*/

		/**
		 * Called when the user leaves the game, 
		 * to save any changes to the VO
		 */
		protected virtual void UpdateGameDataVO()
		{
			if(_isGameEndScreen) return;
				
			SB_ProfileVO currentProfile = SB_UserProfileManager.instance.CurrentProfile;
			int i=0;
			foreach(GameDataVO gameDataVO in currentProfile.gameData)
			{
				if(gameDataVO.id == _gameId)
				{
					//_gameDataVO.numberOfPlays++;
					_gameDataVO.totalSecsPlayed += (int)_gameTimePlayed;

					warn ("UPDATING GAME DATA VO " + _gameDataVO);
					currentProfile.gameData[i] = _gameDataVO;
					SB_UserProfileManager.instance.SaveProfileData(currentProfile);
					return;
				}

				i++;
			}
		}

		override public void ShowPopup(int popupType, SB_PopupVO popupVO=null)
		{
			base.ShowPopup(popupType, popupVO);
			if(_difficultySelectScript != null) _difficultySelectScript.ButtonsEnabled(false);
		}

		/** Called once a popup has been closed */
		override protected void PopupClosed()
		{
			base.PopupClosed();
			if(!_popupManager.PopupIsShowing)
			{
				if(_difficultySelectScript != null) _difficultySelectScript.ButtonsEnabled(true);
			}
		}

		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			base.OnBtnHandler(btn, eventType);

			if(eventType == SB_Button.MOUSE_UP)
			{
				//warn ("btn.name: " + btn.name);
				//SB_SoundManager.instance.PlaySound("Audio/SFX/Games/General/SndButton");// <--- sound does not exist
				switch(btn.name)
				{
					case SB_UIControls.BTN_MENU:
						LogButtonEvent("menu");
						ShowPopup(SB_PopupTypes.GAME_QUIT_POPUP);
						break;

					case SB_UIControls.BTN_PAUSE:
						LogButtonEvent("pause");
						_userPausedGame = true;
						ShowPopup(SB_PopupTypes.GAME_PAUSED_POPUP);
						break;

					case SB_UIControls.BTN_PARENTS:
						//LogButtonEvent("shop");
						// GT - disabled for alpha build (15.10.2015)
						//ShowPopup(SB_PopupTypes.GATING_POPUP);
						LoadParentsArea();
						break;

					// Skip intro button
					case "btn_skip":
						LogButtonEvent("skipintro");
						IntroComplete();
						break;

					// Back to menu button
					case "btn_back":
						LogButtonEvent("backpress");
						ReturnToMenu();
						break;

					// Next Game Button
					case "btn_next":
						LogButtonEvent("nextpress");
						MoveToNextGame();
						break;
				}
			}
		}

		override public void IntroComplete()
		{
			base.IntroComplete();

			if(_skipButtonGameObject != null)
			{
				_skipButtonGameObject.SetActive(false);
			}
		}


		protected virtual void ReturnToMenu(){
			LoadScene(SB_Globals.SCENE_MAIN_MENU,null,true);
		}

		protected virtual void MoveToNextGame(){
			LoadScene(GetNextGame (),null,true);
		}

		string GetNextGame ()
		{
			int startIndex = 0;
			int endIndex = Application.loadedLevelName.IndexOf("Game");
			if(endIndex < 0) endIndex = Application.loadedLevelName.Length-1;
			
			int len = SB_Globals.GAME_ORDER.Length-1;
			for(int n=0; n<len; n++)
			{
				string gameNameSubStr = Application.loadedLevelName.Substring(startIndex, endIndex);
				
				log ("SB_Globals.GAME_ORDER["+n+"]: " + SB_Globals.GAME_ORDER[n] + ", Application.loadedLevelName: " + Application.loadedLevelName + ", gameNameSubStr: " + gameNameSubStr);				
				if(SB_Globals.GAME_ORDER[n].IndexOf(gameNameSubStr) > -1)
				{
					return SB_Globals.GAME_ORDER[n+1];
				}
			}
			
			// This should never actually happen, but we need to return a default
			return Application.loadedLevelName;
		}

		/** Handle button events, to Prefabs which have been loaded into this scene */
		protected virtual void OnViewButton (SB_BaseView view, SB_Button btn, string eventType)
		{
			if(eventType == SB_Button.MOUSE_UP)
			{
				//log ("HandleDifficultySelectButtons " + btn.name);
				if(view == _difficultySelectScript)
				{
					switch(btn.name)
					{
						case DifficultySelect.LEVEL_1_BTN:	_skillLevel = 0;	break;
						case DifficultySelect.LEVEL_2_BTN:	_skillLevel = 1;	break;
						case DifficultySelect.LEVEL_3_BTN:	_skillLevel = 2;	break;
						case DifficultySelect.LEVEL_4_BTN:	_skillLevel = 3;	break;
						case DifficultySelect.LEVEL_5_BTN:	_skillLevel = 4;	break;
						case DifficultySelect.LEVEL_6_BTN:	_skillLevel = 5;	break;
						case DifficultySelect.LEVEL_7_BTN:	_skillLevel = 6;	break;
						case DifficultySelect.LEVEL_8_BTN:	_skillLevel = 7;	break;
						case DifficultySelect.LEVEL_9_BTN:	_skillLevel = 8;	break;
						case DifficultySelect.LEVEL_10_BTN:	_skillLevel = 9;	break;
					}
				}

				LogButtonEvent("skilllevel_" + _skillLevel);
			}
		}

		/** 
		 * This is triggered whenever the 'State' of a loaded prefab 
		 * within this game changes
		 */
		protected virtual void OnViewStateChanged(SB_BaseView view, string state)
		{
			switch(state)
			{
				case STATE_OUTRO_COMPLETE:
					// prefab within this game has finished its Outro animation, so it can now be destroyed
					RemoveViewDelegates(view);
					Destroy(view.gameObject);
					break;
			}
			
		}

		protected void AddViewDelegates(SB_BaseView view)
		{
			if(view != null)
			{
				view.OnButton += OnViewButton;
				view.OnStateChanged += OnViewStateChanged;
			}
		}

		protected void RemoveViewDelegates(SB_BaseView view)
		{
			if(view != null)
			{
				view.OnButton -= OnViewButton;
				view.OnStateChanged -= OnViewStateChanged;

				if(_difficultySelectScript != null && view == _difficultySelectScript) 
				{
					OnDifficultyViewDestroyed();
				}
			}
		}

		/** These methods can be overriden by the game sub class */
		protected virtual void OnDifficultyViewDestroyed() 	{_difficultySelect = null; _difficultySelectScript = null;}

		protected virtual void QuitGame()
		{
			LoadScene(SB_Globals.SCENE_MAIN_MENU, null, true);
		}

		/**
		 * Handle events to popups
		 */
		override protected void HandleOnPopupEvent (SB_BasePopup popup, string eventType)
		{
			base.HandleOnPopupEvent(popup, eventType);

			log ("GAME - OnPopupHandler - eventType: " + eventType);

			int currentPopupType = _popupManager.CurrentPopupType;
			switch(eventType)
			{
				case SB_GameQuitPopup.OPTION_OK:
					if (currentPopupType == SB_PopupTypes.GAME_QUIT_POPUP)
					{
						QuitGame();
					}
					break;

				case SB_BasePopup.OPTION_CLOSE:
					// If the gating popup or game help was closed, then we need to return to the game paused popup
					if(currentPopupType == SB_PopupTypes.GATING_POPUP || currentPopupType == SB_PopupTypes.GAME_HELP_POPUP)
					{
						if(_userPausedGame)
						{
							ShowPopup(SB_PopupTypes.GAME_PAUSED_POPUP);
						}
					}
					else if (currentPopupType == SB_PopupTypes.GAME_PAUSED_POPUP)
					{
						// User has closed the 'game paused' popup
						_userPausedGame = false;
					}
					
					log ("A popup was closed.. _userPausedGame: " + _userPausedGame);
					break;

				case SB_GamePausedPopup.OPTION_PARENTS_AREA:
					// OLD CODE (gating for parents area)
					//ShowPopup(SB_PopupTypes.GATING_POPUP);
					
					// NEW CODE (show shop gating)
					ShowShopGating();
					break;
					
				case SB_GamePausedPopup.OPTION_HOW_TO_PLAY:
					ShowPopup(SB_PopupTypes.GAME_HELP_POPUP, _gameHelpPopupVO);
					break;
					
				case SB_GamePausedPopup.OPTION_MENU:
					PlayOutroAndLoad(SB_Globals.SCENE_MAIN_MENU); 
					break;

					/*
				case SB_GatingPopup.GATING_SUCCESS:
					_popupManager.CurrentPopupScript.PlayOutro();
					//_popupManager.CurrentPopupScript.OutroComplete();
					LoadParentsArea();
					break;
					*/
					
				case SB_GatingPopup.GATING_SUCCESS:
					string nextScene = _popupManager.CurrentPopupScript.vo.data as string;
					if(nextScene == SB_Globals.SCENE_SHOP)
					{
						LoadShop();
					}
					else
					{
						LoadParentsArea();
					}
					break;
					
			}
		}

		// Can be overriden by subclasses, incase they need to pass a KeyValue object to the next scene
		protected virtual void LoadParentsArea()
		{
			LogSceneEvent("Parents");
			LoadScene(SB_Globals.SCENE_PARENTS_AREA, null, true);
		}
		
		protected virtual void LoadShop()
		{
			LogSceneEvent("Shop");
			LoadScene(SB_Globals.SCENE_SHOP, null, true);
		}
		
		protected virtual GameObject CreatePrefabGameObject(string prefabName)
		{
			// Create a new instance of the popup prefab
			var objectPrefab = (GameObject)Resources.Load(prefabName, typeof(GameObject));

			// instantiate it
			GameObject newGameObject = (GameObject)Instantiate(objectPrefab);
			
			return newGameObject;
		}

		/**
		 * Returns a Value Object, for this games help popup
		 */
		protected virtual SB_PopupVO GameHelpPopupVO
		{
			get
			{
				return _gameHelpPopupVO;
			}
		}

		override protected void Update () 
		{
			base.Update();

			if(!_isGameEndScreen)
			{
				_gameTimePlayed += Time.deltaTime;
				_gameDataVO.sessionSecsPlayed = (int)_gameTimePlayed;
			}
		}

		protected bool GameObjectIsActive(string name)
		{
			if(transform.Find(name) != null)
			{
				if(transform.Find(name).gameObject.activeSelf)
				{
					return true;
				}
			}
			
			return false;
		}

		protected void UpdateVoiceIdle()
		{
			_idleCount -= Time.deltaTime;
			if( _idleCount <= 0f )
			{
				_idleCount = _maxIdle;
				PlayIdleVO();
			}
		}

		protected virtual void PlayIdleVO(int startFrom=0)
		{
			_maxVoiceIdleCount = startFrom;

			bool backBtnActive = GameObjectIsActive("btn_back");
			bool nextBtnActive = GameObjectIsActive("btn_next");

			if(backBtnActive) 
			{
				_maxVoiceIdleCount++;
				transform.Find("btn_back").GetComponent<SB_Button>().PlayAnimation("Idle");
			}
			if(nextBtnActive)
			{
				_maxVoiceIdleCount++;
				transform.Find("btn_next").GetComponent<SB_Button>().PlayAnimation("Idle");
			}
			
			_voiceIdleCount++;
			if(_voiceIdleCount > _maxVoiceIdleCount)
			{
				_voiceIdleCount = 0;
			}
		}

		/**
		 * Sub classes should call this function when the game is finished
		 */
		protected virtual void GameOver()
		{
			log ("GAME OVER!!!!!!!");
			SetState(STATE_GAME_OVER);

			log("_levelslocked: " + _levelslocked + ", _gameDataVO.skillLevel: " + _gameDataVO.skillLevel + ", _skillLevel: " + _skillLevel);
			if(_levelslocked && (_skillLevel+1) > _gameDataVO.skillLevel)
			{
				// User has unlocked a new level!
				warn("*** User has unlocked a new level!");
				_gameDataVO.unlockRequired = true;
				_gameDataVO.skillLevel = _skillLevel+1;
			}
		}

		/*
		override public void PlayOutro(string stateName="Outro")
		{
			_uiControls.HideButtons();
			base.PlayOutro(stateName);
		}
		*/
		
		protected virtual void ShowShopGating ()
		{
			string title = SB_Utils.GetNodeValueFromConfigXML ("config/copy/gating_popup/parents/text", "title_shop");
			string body = SB_Utils.GetNodeValueFromConfigXML ("config/copy/gating_popup/parents/text", "body");
			
			SB_PopupVO vo = new SB_PopupVO(SB_PopupTypes.GATING_POPUP, SB_PopupManager.PREFAB_GATING_POPUP, title, body, SB_Globals.SCENE_SHOP);
			ShowPopup(SB_PopupTypes.GATING_POPUP, vo);
		}

		override protected void OnDestroy()
		{
			// Update the game data VO, and save it to the user profile
			UpdateGameDataVO();

			_gameHelpPopupVO = null;
			
			_skipButtonGameObject = null;
			_difficultySelectScript = null;

			if(_difficultySelect != null)
			{
				Destroy (_difficultySelect);
				_difficultySelect = null;
			}
			_uiButtons = null;

			_gameXML = null;
			_gameDataVO = null;
			
			base.OnDestroy();
		}
	}
}