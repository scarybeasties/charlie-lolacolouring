﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ScaryBeasties.Controls;
using ScaryBeasties.Base;
using ScaryBeasties.History;
using ScaryBeasties.Popups;
using ScaryBeasties.User;
using ScaryBeasties.UI;
using ScaryBeasties.Utils;
using ScaryBeasties.Data;
using ScaryBeasties.Cam;
using ScaryBeasties.Sound;

namespace ScaryBeasties.Scenes
{
	public class SB_ProfileSelectionScene : SB_BaseScene 
	{
		// Store the text mesh objects
		private SB_TextField _titleText;
		//private SB_UserProfileManager _userProfileManager = new SB_UserProfileManager();

		GameObject _profile1, _profile2, _profile3;


		float _idleCounter = 0.0f;
		float _idleMax = 10.0f;
		int _idleNum = 1;

		
		
		private void resetIdle()
		{
			_idleCounter = _idleMax;
		}
		
		
		public void PlayIdle()
		{
			int num = SB_UserProfileManager.instance.NumberOfActiveProfiles(3);

			_idleNum++;

			if( _idleNum > 2 ) 
				_idleNum = 1;


			
			resetIdle ();
		}


		override protected void Start()
		{
			//SB_SoundManager.instance.PlayNewMusic("Audio/Music/MusMenuMusic");

			// Force clear of history.
			// At this point user, can only go back to the title screen
			SB_SceneHistory.instance.Reset();
			SB_SceneHistory.instance.AddScene(SB_Globals.SCENE_TITLE);

			base.Start ();
			_uiControls.ShowButtons(new List<string>(new string[]  {SB_UIControls.BTN_BACK, 
																	SB_UIControls.BTN_SOUND,
																	SB_UIControls.BTN_PARENTS}));

			_debugEnabled = false;



			_profile1 = transform.Find("btn_profile1").gameObject;
			_profile2 = transform.Find("btn_profile2").gameObject;
			_profile3 = transform.Find("btn_profile3").gameObject;

			HideProfile(1);
			HideProfile(2);
			HideProfile(3);


			//SB_SoundManager.instance.PlayVoiceQueued("Localised/VO/Narrator/vo_profile_tapyourpicture");
		}
		
		override protected void Init()
		{
			// Get the text object, and set content from config.xml
			_titleText = GameObject.Find("text/title").GetComponent<SB_TextField>();
			_titleText.text = SB_Utils.GetNodeValueFromConfigXML ("config/copy/profile_selection/text", "title");


		}

		protected override void UpdateReady ()
		{
			_idleCounter -= Time.deltaTime;

			if( _idleCounter <= 0 )
			{
				PlayIdle();
			}

			base.UpdateReady ();
		}

		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			base.OnBtnHandler(btn, eventType);

			resetIdle ();

			if(eventType == SB_Button.MOUSE_UP)
			{
				//log ("Pressed: " + btn.name);

				switch(btn.name)
				{
					/*
					case SB_UIControls.BTN_BACK:
						PlayOutroAndLoad(SB_SceneHistory.instance.GetLastItem(true)); 
						break;
					*/

					case SB_UIControls.BTN_PARENTS:
						ShowPopup(SB_PopupTypes.GATING_POPUP);
						break;
					
					case "btn_profile1": 	ProfileSelected(1);		break;
					case "btn_profile2": 	ProfileSelected(2);		break;
					case "btn_profile3": 	ProfileSelected(3);		break;

					case "btn_profile1_delete":		ProfileDeleteSelected(1);	break;
					case "btn_profile2_delete":		ProfileDeleteSelected(2);	break;
					case "btn_profile3_delete":		ProfileDeleteSelected(3);	break;

					case "btn_toggle":
						_customPicsActive = !_customPicsActive;
						_profile1.transform.Find("custom_pic").gameObject.SetActive(_customPicsActive);
						_profile2.transform.Find("custom_pic").gameObject.SetActive(_customPicsActive);
						_profile3.transform.Find("custom_pic").gameObject.SetActive(_customPicsActive);
						break;
				}
			}
		}

		// TEMP!!!
		private bool _customPicsActive = true;

		/**
		 * User has selected a profile
		 */
		protected void ProfileSelected(int profileId)
		{
			SB_SoundManager.instance.ClearVOQueue();
			SB_SoundManager.instance.PlaySound("Audio/SFX/Games/General/SndButton");
			SB_UserProfileManager.instance.CurrentProfile = SB_UserProfileManager.instance.GetProfileVO(profileId);

			// If the selected profile has no name, then we need to setup the profile
			if(SB_UserProfileManager.instance.CurrentProfile.name == "")
			{
				PlayOutroAndLoad(SB_Globals.SCENE_PROFILE_NAMING);
			}
			// Selected profile has a name, so proceed to main menu
			else
			{
				PlayOutroAndLoad(SB_Globals.SCENE_MAIN_MENU);
			}

			//SB_SceneHistory.instance.AddScene(Application.loadedLevelName);


		}

		
		/**
		 * User has chosen to delete a profile.
		 * Store the chosen id, and show the gating popup.
		 */
		protected void ProfileDeleteSelected(int profileId)
		{
			SB_SoundManager.instance.ClearVOQueue();
			SB_SoundManager.instance.PlaySound("Audio/SFX/Games/General/SndButton");
			SB_UserProfileManager.instance.CurrentProfile = SB_UserProfileManager.instance.GetProfileVO(profileId);
			ShowPopup(SB_PopupTypes.GATING_POPUP_DELETE_PROFILE);
		}

		/**
		 * Handle events to popups
		 */	
		override protected void HandleOnPopupEvent (SB_BasePopup popup, string eventType)
		{
			base.HandleOnPopupEvent(popup, eventType);

			int currentPopupType = _popupManager.CurrentPopupType;
			switch(eventType)
			{
				case SB_GatingPopup.GATING_SUCCESS:

					if(currentPopupType == SB_PopupTypes.GATING_POPUP)
					{
						LoadScene(SB_Globals.SCENE_PARENTS_AREA, null, true);
					}

					else if(currentPopupType == SB_PopupTypes.GATING_POPUP_DELETE_PROFILE)
					{
						ShowPopup(SB_PopupTypes.CONFIRM_DELETE_PROFILE_POPUP);
					}

					break;

				case SB_ConfirmProfileDeletePopup.OPTION_OK:
					// Erase the profile data
					//log ("DELETE THE PROFILE!");
					SB_ProfileVO deletedProfileVO = SB_UserProfileManager.instance.DeleteProfileData(SB_UserProfileManager.instance.CurrentProfile);
					SetupProfile(deletedProfileVO);
					break;

					
				case SB_ConfirmProfileDeletePopup.OPTION_CANCEL:
				case SB_ConfirmProfileDeletePopup.OPTION_CLOSE:
					//log ("NOT DELETING PROFILE");
					SB_UserProfileManager.instance.CurrentProfile = null;
					break;
			}	
		}

		private GameObject GetProfile(int n)
		{
			switch(n)
			{
				case 1: return _profile1;
				case 2: return _profile2;
				case 3: return _profile3;
			}

			return null;
		}

		/**
		 * Hide a profile
		 */ 
		private void HideProfile(int n)
		{
			GetProfile(n).SetActive(false);
		}

		/**
		 * Setup the user profile name and image.
		 * @SB_ProfileVO profileVO A Profile Value Object, containing the profile data.
		 */
		protected void SetupProfile(SB_ProfileVO profileVO)
		{
			//GameObject profileGameObject = transform.Find("billboard_bg/btn_profile" + profileVO.profileId).gameObject;
			GameObject profileGameObject = GetProfile (profileVO.profileId);
			profileGameObject.SetActive (true);

			SB_TextField profileNameTextField = profileGameObject.transform.Find("name").GetComponent<SB_TextField>();

			profileNameTextField.text = profileVO.name;
			
			// If profile name is blank, we need to hide the 'delete' button
			if(profileNameTextField.text == "")
			{
				//log ("profile name is blank");
				GameObject deleteProfileButtonGameObject = profileGameObject.transform.Find("btn_profile" + profileVO.profileId + "_delete").gameObject;
				deleteProfileButtonGameObject.SetActive(false);
			}

			//log ("looking for 'pic' game object");
			tk2dSprite profilePicSprite = profileGameObject.transform.Find("pic").GetComponent<tk2dSprite>();			
			//log ("got 'pic' game object");
			//log ("profileVO.avatarId: " + profileVO.avatarId);

			if(profileVO.avatarId != "" && profileVO.avatarId != "custom")
			{
				//log ("profilePicSprite.SetSprite(" + profileVO.avatarId +")");
				//profilePicSprite.SetSprite("profile_icon_" + profileVO.avatarId);
				profilePicSprite.SetSprite(profileVO.avatarId);
			}
			else
			{
				profilePicSprite.SetSprite("profile_blank");
			}

			GameObject avatarBorder = profileGameObject.transform.Find ("picture_border").gameObject;
			GameObject customPic = profileGameObject.transform.Find ("custom_pic").gameObject;
			GameObject avatarPic = profileGameObject.transform.Find ("pic").gameObject;
			if( profileVO.avatarId != "custom" ) 
			{
				log ("--not a custom pic");
				avatarPic.SetActive(true);
				avatarBorder.SetActive(false);
				customPic.gameObject.SetActive(false);
				customPic.GetComponent<Renderer>().enabled = false;
			}
			else
			{
				log ("--using a custom pic");
				avatarPic.SetActive(false);
				avatarBorder.SetActive(true);
				customPic.gameObject.SetActive(true);

				MeshRenderer meshRndr = customPic.GetComponent<MeshRenderer>();
				meshRndr.GetComponent<Renderer>().material.mainTexture = profileVO.customProfilePic;
				meshRndr.GetComponent<Renderer>().sortingLayerID = SB_Globals.SORTING_LAYER_UI;
				meshRndr.GetComponent<Renderer>().sortingOrder = 4; // Make sure the mesh is below the picture frame, but above the paper
			}

			//log ("StartCoroutine WaitThenContinueSetupProfile");
			StartCoroutine (WaitThenContinueSetupProfile (profileVO));
		}

		/** 
		 * Hack.. 
		 * We need to wait for a short while, before we can set the name textfield,
		 * because the parent game object was inactive
		 */
		IEnumerator WaitThenContinueSetupProfile(SB_ProfileVO profileVO)
		{
			//log ("WaitThenContinueSetupProfile() 0.3f secs");
			string setname = profileVO.name;
			print("Set name to: "+setname);

			yield return new WaitForSeconds(0.3f);
			// TODO: find out why xcode crashes at this point. doesnt like yields?
			
			//log ("WaitThenContinueSetupProfile() resuming");
			GameObject profileGameObject = GetProfile (profileVO.profileId);
			SB_TextField profileNameTextField = profileGameObject.transform.Find("name").GetComponent<SB_TextField>();
			profileNameTextField.text = setname;//profileVO.name;
			print ( "Set profile [" + profileVO.profileId + "] name to: " + profileVO.name + " or: " + setname );
		}


		override public void OutroStarted() {
			base.OutroStarted();
		}

		public override void IntroStarted ()
		{
			base.IntroStarted ();
			/*
			SetupProfile(SB_UserProfileManager.instance.GetProfileVO(1));
			SetupProfile(SB_UserProfileManager.instance.GetProfileVO(2));
			SetupProfile(SB_UserProfileManager.instance.GetProfileVO(3));
			*/
		}

		/**
		 * This function is called from the scene's 
		 * 'Intro' dopesheet animation
		 */
		public void SetupUserProfile(int id)
		{
			SetupProfile(SB_UserProfileManager.instance.GetProfileVO(id));
			error ("SetupUserProfile id: " + id);
		}

		override protected void StartLoadingNextScene(){
			transform.Find("Loader").gameObject.SetActive(true);
			base.StartLoadingNextScene();
		}
		
		override protected void OnDestroy()
		{
			Destroy (transform.Find("Loader").gameObject);
			
			_profile1 = null;
			_profile2 = null;
			_profile3 = null;

			if(_titleText != null)
			{
				_titleText = null;
			}
			base.OnDestroy();
		}

	}
}