﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ScaryBeasties.Controls;
using ScaryBeasties.Base;
using ScaryBeasties.History;
using ScaryBeasties.Input;
using ScaryBeasties.Popups;
using ScaryBeasties.UI;
using ScaryBeasties.Utils;
using ScaryBeasties.User;
using ScaryBeasties.Sound;
using ScaryBeasties.Data;

namespace ScaryBeasties.Scenes
{
	public class SB_ProfileNamingScene : SB_BaseScene 
	{
		const int MAX_NAME_LENGTH = 9;

		private SB_TextField _titleText;
		private SB_TextField _profileNameText;
		private SB_Keyboard _keyboard;
		//private SB_KeyboardInput _profileNameKeyboardInput;

		private string _namePromptTxt;

		private bool _userInput = false; // has the user put anything in the text field?
		private string _inputText;
		private SB_Button _okButton;

		float _idleCounter=0.0f;
		float _idleMax=10.0f;
		int _idleNum=1;
		
		/*
		private void resetIdle(){
			_idleCounter=_idleMax;
		}
		

		public void PlayIdle(){
			
			_idleNum++;
			if(_idleNum>2) _idleNum=1;
			if(!_cameraActive) _idleNum=1;
			if(_idleNum==1){
				SB_SoundManager.instance.PlayVoiceIfNotPlaying("Localised/VO/Narrator/vo_profile_tapthetick");
			}
			else SB_SoundManager.instance.PlayVoiceIfNotPlaying("Localised/VO/Narrator/vo_profile_photoofyourself");
			
			resetIdle ();
			
		}*/

		override protected void Start()
		{
			base.Start ();
			_uiControls.ShowButtons(new List<string>(new string[]  {SB_UIControls.BTN_BACK, 
																	SB_UIControls.BTN_SOUND,
																	SB_UIControls.BTN_PARENTS}));

			_okButton = transform.Find("ok_btn").GetComponent<SB_Button>();
			_okButton.gameObject.SetActive(false);

			_keyboard = transform.Find("KeyboardContainer/Keyboard").GetComponent<SB_Keyboard>();
			_keyboard.config.maxLength = MAX_NAME_LENGTH;
		}

		override protected void Init()
		{
			base.Init ();

			// Get the game objects
			_titleText = GameObject.Find("text/title").GetComponent<SB_TextField>();
			_profileNameText = GameObject.Find("profile_name/text_input").GetComponent<SB_TextField>();
			//_profileNameKeyboardInput = GameObject.Find("profile_name/text_input").GetComponent<SB_KeyboardInput>();
			
			// Set the text, from config.xml
			_titleText.text = SB_Utils.GetNodeValueFromConfigXML ("config/copy/profile_naming/text", "title");
			_namePromptTxt = "";//SB_Utils.GetNodeValueFromConfigXML ("config/copy/profile_naming/text", "name_prompt");
			//_profileNameKeyboardInput.defaultText = _namePromptTxt;

			//SetDynamicText(_titleText, SB_Utils.GetNodeValueFromConfigXML ("config/copy/profile_naming/text", "title"));

			//if(SB_Globals.SELECTED_PROFILE_VO != null)
			if(SB_UserProfileManager.instance.CurrentProfile != null)
			{
				// Set the input field to be the name of the selected profile.
				// If no name is set, use the prompt from the XML
				//SetDynamicText(_profileNameText, SB_Globals.SELECTED_PROFILE_VO.name);
			}

			//SB_SoundManager.instance.PlayVoiceQueued("Localised/VO/Charlie/vo_profile_whatsyourname");
		}

		protected override void UpdateReady ()
		{
			base.UpdateReady ();

			/*if(SB_Globals.TEST_MODE)
			{
				// Use a dummy profle name, when testing locally (in the IDE)
				_profileNameText.text = "TEST_PROFILE";
			}*/

			_profileNameText.text=_keyboard.GetString ();

			//if (_profileNameText.text != _namePromptTxt || SB_Globals.TEST_MODE ) 
			if (_profileNameText.text.Length>0 ) 
			{
				_okButton.gameObject.SetActive(true);
			}
			else 
			{
				_okButton.gameObject.SetActive(false);
			}

			// Has user pressed 'done' button on their on screen keyboard?
			/*if(!SB_Globals.TEST_MODE && _profileNameKeyboardInput.KeyboardIsDone)
			{
				OKBtnPressed();
			}*/
		}

		/**
		 * Has user entered a valid name?
		 */
		private bool ProfileNameIsValid()
		{
			// CT 25-09-2014
			// TODO: remove this hack
			if( SB_Globals.TEST_MODE )
			{
				return true;
			}
			// end of hack

			//if(_profileNameText.text.Length > 0 && _profileNameText.text != _namePromptTxt)
			if(_profileNameText.text != _namePromptTxt)
			{
				return true;
			}

			return false;
		}

		void OKBtnPressed()
		{
			SB_ProfileVO currentProfile = SB_UserProfileManager.instance.CurrentProfile;
			if(ProfileNameIsValid())
			{
				_keyboard.HideKeyboard();
				//SB_Globals.SELECTED_PROFILE_VO.name = _profileNameText.text;
				currentProfile.name = _profileNameText.text;
				//warn ("SB_Globals.SELECTED_PROFILE_VO.name: " + SB_Globals.SELECTED_PROFILE_VO.name);
				warn ("currentProfile.name: " + currentProfile.name);

				SB_SoundManager.instance.ClearVOQueue();
				SB_SoundManager.instance.PlaySound("Audio/SFX/Games/General/SndButton");


				PlayOutroAndLoad(SB_Globals.SCENE_PROFILE_PICTURE);	
			}
		}

		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			base.OnBtnHandler(btn, eventType);

			if(eventType == SB_Button.MOUSE_UP)
			{
				log ("Pressed: " + btn.name);
				
				switch(btn.name)
				{
					/*
					case SB_UIControls.BTN_BACK:
						PlayOutroAndLoad(SB_SceneHistory.instance.GetLastItem(true)); 
						break;
					*/
					case SB_UIControls.BTN_PARENTS:
						ShowPopup(SB_PopupTypes.GATING_POPUP);
						break;
					/*
					case "text_input_btn":
						// Set default keyboard type, for iOS
						TouchScreenKeyboardType keyboardType = TouchScreenKeyboardType.ASCIICapable;
						
						#if UNITY_ANDROID
							//keyboardType = TouchScreenKeyboardType.NamePhonePad;
						#endif

						_profileNameKeyboardInput.ShowKeyboard(_profileNameText.text, keyboardType);
						break;*/

					case "ok_btn":
						OKBtnPressed();
						break;
				}
			}
		}

		/**
		 * Handle events to popups
		 */	
		override protected void HandleOnPopupEvent (SB_BasePopup popup, string eventType)
		{
			base.HandleOnPopupEvent(popup, eventType);
			
			int currentPopupType = _popupManager.CurrentPopupType;
			switch(eventType)
			{
				case SB_GatingPopup.GATING_SUCCESS:
					if(currentPopupType == SB_PopupTypes.GATING_POPUP)
					{
						LoadScene(SB_Globals.SCENE_PARENTS_AREA, null, true);
					}
					break;
			}	
		}

		/*
		override public void ShowPopup(int popupType, SB_PopupVO popupVO=null)
		{
			if(_profileNameKeyboardInput != null)
			{
				_profileNameKeyboardInput.HideKeyboard();
			}
			base.ShowPopup(popupType, popupVO);
		}

		override public void OutroStarted()
		{
			if(_profileNameKeyboardInput != null)
			{
				_profileNameKeyboardInput.HideKeyboard();
			}
			base.OutroStarted();
		}*/

		override protected void StartLoadingNextScene(){
			_keyboard.HideKeyboard();
			transform.Find("Loader").gameObject.SetActive(true);
			base.StartLoadingNextScene();
		}
		
		override protected void OnDestroy()
		{
			Destroy (transform.Find("Loader").gameObject);
			if(_titleText != null)
			{
				Destroy(_titleText);
				_titleText = null;
			}
			
			if(_profileNameText != null)
			{
				Destroy(_profileNameText);
				_profileNameText = null;
			}
			/*
			if(_profileNameKeyboardInput != null)
			{
				Destroy(_profileNameKeyboardInput);
				_profileNameKeyboardInput = null;
			}*/

			if(_keyboard != null)
			{
				Destroy(_keyboard);
				_keyboard = null;
			}
			
			base.OnDestroy();
		}
	}
}
