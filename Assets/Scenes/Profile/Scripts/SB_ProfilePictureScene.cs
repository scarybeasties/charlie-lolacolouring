using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ScaryBeasties.Cam;
using ScaryBeasties.Controls;
using ScaryBeasties.Base;
using ScaryBeasties.History;
using ScaryBeasties.Popups;
using ScaryBeasties.User;
using ScaryBeasties.UI;
using ScaryBeasties.Utils;
using ScaryBeasties.Data;
using ScaryBeasties;
using ScaryBeasties.Sound;

namespace ScaryBeasties.Scenes
{
	public class SB_ProfilePictureScene : SB_BaseScene 
	{
		public const string STATE_PIC_CHOSEN = "statePicChosen";



		private SB_TextField _titleText;
		private SB_TextField _titleWelcome;
		private tk2dSprite _profileAvatar, _profileAvatarBorder;
		private DynamicText _profileNameText;
		private SB_CameraImage _customAvatar;

		GameObject _profile;
		tk2dSpriteAnimator _paper;

		//private int _selectedPicId = 0;
		private string _selectedPicId = "profile_1"; // Use the Sarah avatar as a default profile pic
		//private SB_UserProfileManager _userProfileManager = new SB_UserProfileManager();


		private SB_BaseCarousel _carousel;
		private List<GameObject> _carouselItems = new List<GameObject>();

		private GameObject _carouselItemPrefab;
		private GameObject _okButton;

		private SB_Button _cameraButton;
		private bool _cameraActive=false;

		private bool _selectionMade = false;
		private bool _photoTaken = false;

		private float _screenWidth,_screenHeight;
		private float _carouselHeight,_carouselWidth,_carouselTop;
		private float _carouselSpeed =200.0f;

		private int _pageState=0;
		private const int PSTATE_CAROUSELON=0;
		private const int PSTATE_CAROUSELSCROLL=3;
		private const int PSTATE_READY=1;
		private const int PSTATE_CAROUSELOFF=2;


		float _idleCounter=0.0f;
		float _idleMax=10.0f;
		int _idleNum=1;


		private void resetIdle() {
			_idleCounter=_idleMax;
		}
		
		
		public void PlayIdle() {


			if(_popupManager.CurrentPopupType == SB_PopupTypes.CAMERA_POPUP) {
				//SB_SoundManager.instance.PlayVoiceIfNotPlaying("Localised/VO/Charlie/vo_profile_photoofyourself");
			} else {
				if( !_photoTaken ) {
					//SB_SoundManager.instance.PlayVoiceIfNotPlaying("Localised/VO/Charlie/vo_profile_tapthetick");
				} else {
					/*
					if( Random.Range(0,2) > 0 ) {
						SB_SoundManager.instance.PlayVoiceIfNotPlaying("Localised/VO/Charlie/vo_profile_changephoto");
					} else {
						SB_SoundManager.instance.PlayVoiceIfNotPlaying("Localised/VO/Charlie/vo_profile_tapthetick");
					}*/
				}
			}
			resetIdle ();
			
		}

		override protected void Start() {
			base.Start ();
			_uiControls.ShowButtons(new List<string>(new string[]  {SB_UIControls.BTN_BACK,
																	SB_UIControls.BTN_SOUND,
																	SB_UIControls.BTN_PARENTS}));

			_screenWidth = ZoomPoint((float)tk2dCamera.Instance.ScreenExtents.size.x);
			_screenHeight = (float)tk2dCamera.Instance.ScreenExtents.size.y;



			_profile = transform.Find("profile").gameObject;
			_profileAvatar 			= _profile.transform.Find("avatar").GetComponent<tk2dSprite>();
			_profileAvatarBorder 	= _profile.transform.Find("avatar_border").GetComponent<tk2dSprite>();
			_profileNameText 		= _profile.transform.Find("name").GetComponent<DynamicText>();
			_customAvatar			= _profile.transform.Find("custom_avatar").GetComponent<SB_CameraImage>();
			_customAvatar.SetupImageLoader( 100,160 );

			_customAvatar.GetComponent<Renderer>().enabled = false;

			_profileAvatarBorder.GetComponent<Renderer>().enabled = false;
			_profileAvatar.SetSprite(_selectedPicId);


			_carouselItemPrefab = transform.Find ("CarouselItem").gameObject;

			_okButton = transform.Find ("ok_btn").gameObject;
			_okButton.SetActive(false); // Hide, until the intro has finished!

			_cameraActive =  SB_Camera.CameraExists();
			LogSceneEvent("CameraExists_" + _cameraActive.ToString());

			SetupCarousel ();
			SetupCarouselItems();

			_carousel.SetupItems(_carouselItems);
				
			// This scene has an OutroWelcome animation, shown when the user has setup their new profile
			// This code *must* go here, after base.Start(), to ensure that the _animationStates List is set correctly
			_animationStates = new List<string>(new string[] {"Intro", "Outro", "OutroWelcome"});
			_animationController.animationStates = _animationStates;



			//SB_SoundManager.instance.PlayVoiceQueued("Localised/VO/Charlie/vo_profile_choosepicture");

		}

		private void SetupCarousel(){

			_carousel = transform.Find ("Carousel").gameObject.GetComponent<SB_BaseCarousel> ();
			if(_cameraActive){
				_carousel.carouselOffsets.left=130.0f;
			}
			else{
				_carousel.carouselOffsets.left=10.0f;
			}
			_carousel.SetupBounds();

			tk2dBaseSprite carouselback = transform.Find ("Carousel/CarouselBackground").gameObject.GetComponent<tk2dBaseSprite> ();
			_carouselWidth = (carouselback.GetBounds().size.x);///(SB_GameCamera.defaultZoom/(float)tk2dCamera.Instance.ZoomFactor);
			_carouselHeight = (carouselback.GetBounds().size.y)/(SB_GameCamera.defaultZoom/(float)tk2dCamera.Instance.ZoomFactor);
			float hgt = _screenHeight-_carouselHeight;

			float cx = 0+(_carouselWidth-_screenWidth)/2;
			float cy = 0+(_carouselHeight-_screenHeight)/2;
			//_carousel.transform.position=new Vector3(_carousel.transform.position.x,cy,_carousel.transform.position.z);
			transform.Find ("Carousel/CarouselBackground").transform.position=new Vector3(cx,carouselback.transform.position.y,carouselback.transform.position.z);
			
			GameObject cam = (GameObject) transform.Find ("CameraButton").gameObject;
			tk2dBaseSprite camback = transform.Find ("CameraButton/CameraBackground").gameObject.GetComponent<tk2dBaseSprite> ();
			float cwid=140.0f;//camback.GetBounds().size.x/(SB_GameCamera.defaultZoom/(float)tk2dCamera.Instance.ZoomFactor);
			float cx2 = 0+(cwid-_screenWidth)/2;
			float cy2 = 0+((camback.GetBounds().size.y)-_screenHeight)/2;
			//cam.transform.position = new Vector3(cx2,cy,cam.transform.position.z);

			_carouselTop=(0-(ZoomPoint(_screenHeight)/2))+_carouselHeight;

			//_carousel.PlaceAtPoint(SB_BaseCarousel.POSITION_RIGHT);

			_carousel.Disable ();
			_carousel.transform.position = new Vector3(_carousel.transform.position.x,(0-(_screenHeight/2)+(_carouselTop/2)),_carousel.transform.position.z);
			
			_cameraButton = transform.Find ("CameraButton").GetComponent <SB_Button>();
			_cameraButton.transform.position = new Vector3(cx2,(0-(_screenHeight/2)+(_carouselTop/2)),_cameraButton.transform.position.z);

			if(!_cameraActive){
				_cameraButton.gameObject.SetActive(false);
			}
			_carousel.SetupBounds();

		}

		float ZoomPoint(float pt){
			return pt*(SB_GameCamera.defaultZoom/(float)tk2dCamera.Instance.ZoomFactor);
		}

		override public void IntroComplete()
		{
			base.IntroComplete();
			_okButton.SetActive(true);
		}

		void SelectAvatar(int id)
		{


			SB_SoundManager.instance.PlaySound("Audio/SFX/Games/General/SndBoxAppear");
			_selectionMade = true;
			//SB_CarouselItem c = objectReference.GetComponent<SB_CarouselItem>();
			_profileAvatar.GetComponent<Renderer>().enabled = true;
			_customAvatar.GetComponent<Renderer>().enabled = false;
			_profileAvatar.SetSprite(SB_Globals.AVATARS[id]);
			_selectedPicId = SB_Globals.AVATARS[id];
			_profileAvatarBorder.GetComponent<Renderer>().enabled = false;
			warn ("_selectedPicId: " + _selectedPicId);
			_photoTaken = false;
		}

		void HandleCustomAvatarSelected()
		{
			_selectionMade = true;
			_profileAvatar.GetComponent<Renderer>().enabled = false;
			_customAvatar.GetComponent<Renderer>().enabled = true;
			_customAvatar.LoadImage("custom"+SB_UserProfileManager.instance.CurrentProfile.profileId);
			_profileAvatarBorder.GetComponent<Renderer>().enabled = true;
			_selectedPicId = "custom";
			_photoTaken = true;
		}

		override protected void Init()
		{
			// Get the game objects
			_titleText			= GameObject.Find("text/title").GetComponent<SB_TextField>();
			_titleWelcome		= GameObject.Find("text/welcome").GetComponent<SB_TextField>();
			//_profileAvatar 		= _profile.transform.Find("avatar").GetComponent<tk2dSprite>();
			//_profileNameText 	= _profile.transform.Find("name").GetComponent<DynamicText>();

			// Set the text, from config.xml
			_titleText.text 	= SB_Utils.GetNodeValueFromConfigXML("config/copy/profile_picture/text", "title");
			_titleWelcome.text 	= SB_Utils.GetNodeValueFromConfigXML("config/copy/profile_picture/text", "welcome");

			SetProfileName();
			_carousel.PlaceAtPoint(SB_BaseCarousel.POSITION_OFF_RIGHT);
		}

		void SetProfileName()
		{
			SB_ProfileVO currentProfile = SB_UserProfileManager.instance.CurrentProfile;
			if(_profileNameText != null && currentProfile != null)
			{
				_profileNameText.SetText(currentProfile.name);
			}
		}

		private void SetupCarouselItems()
		{
			tk2dBaseSprite tspr;
			SB_CarouselDataItem cdata;

			int i;
			for( i = 0; i < SB_Globals.AVATARS_CAROUSEL.Length; i++ ) 
			{
				GameObject carousel_item = (GameObject) Instantiate(_carouselItemPrefab,new Vector3(),Quaternion.identity);
				tspr = carousel_item.GetComponent<tk2dBaseSprite>();

				cdata = carousel_item.GetComponent<SB_CarouselDataItem>();
				cdata.objectId=i;
				cdata.objectRef=SB_Globals.AVATARS_CAROUSEL[i];
				tspr.SetSprite(SB_Globals.AVATARS_CAROUSEL[i]);
				_carouselItems.Add(carousel_item);
			}
		}




		/**
		 * Has user chosen a picture?
		 */
		private bool ProfilePicIsValid()
		{
			//log ("ProfilePicIsValid - _selectedPicId: " + _selectedPicId);
			//if(_selectedPicId > 0)
			if(_selectedPicId != "")
			{
				return true;
			}
			
			return false;
		}

		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			base.OnBtnHandler(btn, eventType);

			if(eventType == SB_Button.MOUSE_UP)
			{
				//log ("Pressed: " + btn.name);
				
				switch(btn.name)
				{
					/*
					case SB_UIControls.BTN_BACK:
						PlayOutroAndLoad(SB_SceneHistory.instance.GetLastItem(true)); 
						break;
					*/
					case SB_UIControls.BTN_PARENTS:
						ShowPopup(SB_PopupTypes.GATING_POPUP);
						break;
					
					case "ok_btn":
						//log ("ProfilePicIsValid(): " + ProfilePicIsValid());
						if(ProfilePicIsValid())
						{

							SB_SoundManager.instance.ClearVOQueue();
							SB_SoundManager.instance.PlaySound("Audio/SFX/Games/General/SndButton");
							SB_ProfileVO currentProfile = SB_UserProfileManager.instance.CurrentProfile;
							if(currentProfile != null)
							{
								LogSceneEvent("ProfilePic_" + _selectedPicId);

								currentProfile.avatarId = _selectedPicId;
								//_userProfileManager.SaveProfileData(currentProfile);
								SB_UserProfileManager.instance.SaveProfileData(currentProfile);
							}
							
							// Hide buttons.. we dont want the user clicking away at this point!
							SB_UIControls.instance.HideButtons();
							
							SetState (STATE_PIC_CHOSEN);
							_okButton.SetActive(false);
							_carousel.Disable();
							
							_animationController.PlayAnimation("OutroWelcome");
							
						}
						break;

					case "CameraButton":

					SB_SoundManager.instance.ClearVOQueue();
					SB_SoundManager.instance.PlaySound("Audio/SFX/Games/General/SndButton");
					_carousel.Disable();
						#if USES_CAMERA
						ShowPopup(SB_PopupTypes.CAMERA_POPUP);
						#endif
						break;
					default:
						// User must have chosen one of the icons
						//_selectedPicId = (int)btn.data;
						//_selectedPicId = (string)btn.data;

						//_profileAvatar.SetSprite("profile_icon_" + _selectedPicId);

						// TODO: get the name of the selected icon from the carousel, and set the avatar sprite
						//_profileAvatar.SetSprite("Avatar_Main_Duck");
						break;
				}
			}
		}

		override public void PlayOutro(string stateName="Outro")
		{
			base.PlayOutro(stateName);

			// Hide the OK button (again), incase user has left this scene using the 'back' button!
			_okButton.SetActive(false);

		}

		/**
		 * Handle events to popups
		 */	
		override protected void HandleOnPopupEvent (SB_BasePopup popup, string eventType)
		{
			base.HandleOnPopupEvent(popup, eventType);
			
			int currentPopupType = _popupManager.CurrentPopupType;
			switch(eventType)
			{
			case SB_CameraPopup.POPUP_CLOSED:
				_carousel.Enable();
				break;

				case SB_GatingPopup.GATING_SUCCESS:
					if(currentPopupType == SB_PopupTypes.GATING_POPUP)
					{
						LoadScene(SB_Globals.SCENE_PARENTS_AREA, null, true);
					}
					break;
					
				case SB_CameraPopup.PHOTO_SAVED:
					if(currentPopupType == SB_PopupTypes.CAMERA_POPUP)
					{
						HandleCustomAvatarSelected();

					}
				_carousel.Enable();
				break;
			}	
		}

		public void PlayEndVO(){

			//SB_SoundManager.instance.PlayVoiceQueued("Localised/VO/Lola/vo_gen_lolaocelebrate_1");

		}

		public void EndingComplete(){
			PlayOutroAndLoad(SB_Globals.SCENE_PROFILE_SELECTION, "Outro");
		}

		/** Called once user has selected a profile picture, and Sarah/Duck animation has finished */
		void OnWelcomeCelebrateComplete (tk2dSpriteAnimator arg1, tk2dSpriteAnimationClip arg2)
		{
			PlayOutroAndLoad(SB_Globals.SCENE_PROFILE_SELECTION, "Outro");
		}



		/** Update is called once per frame */
		override protected void Update () 
		{
			base.Update();
			if(_pageState==PSTATE_READY){
				//_icarousel.CarouselMove();
				if (UnityEngine.Input.GetMouseButtonUp (0)) {
					resetIdle();
					SB_CarouselDataItem item = _carousel.GetTapped();
					if(item!=null){
						SelectAvatar(item.objectId);
					}
				}

				_idleCounter-=Time.deltaTime;
				if(_idleCounter<=0){
					PlayIdle();
				}
			}
			else if(_pageState==PSTATE_CAROUSELOFF){
				if(_carousel.transform.position.y>(0-(_screenHeight/2)+(_carouselTop))){
					_carousel.transform.position = new Vector3(_carousel.transform.position.x,_carousel.transform.position.y-(_carouselSpeed*Time.deltaTime),_carousel.transform.position.z);
					if(_cameraActive)_cameraButton.transform.position = new Vector3(_cameraButton.transform.position.x,_cameraButton.transform.position.y-(_carouselSpeed*Time.deltaTime),_cameraButton.transform.position.z);
					
				}

			}
			else if(_pageState==PSTATE_CAROUSELON){
				if(_carousel.transform.position.y+(_carouselSpeed*Time.deltaTime)<(0-(_screenHeight/2)-(_carouselTop/2))){
					_carousel.transform.position = new Vector3(_carousel.transform.position.x,_carousel.transform.position.y+(_carouselSpeed*Time.deltaTime),_carousel.transform.position.z);
					if(_cameraActive)_cameraButton.transform.position = new Vector3(_cameraButton.transform.position.x,_cameraButton.transform.position.y+(_carouselSpeed*Time.deltaTime),_cameraButton.transform.position.z);
					
				}
				else{
					_carousel.transform.position = new Vector3(_carousel.transform.position.x,(0-(_screenHeight/2)-(_carouselTop/2)),_carousel.transform.position.z);
					if(_cameraActive)_cameraButton.transform.position = new Vector3(_cameraButton.transform.position.x,(0-(_screenHeight/2)-(_carouselTop/2)),_cameraButton.transform.position.z);

					_carousel.Enable();
					//_carousel.ScrollToPoint(SB_BaseCarousel.POSITION_LEFT,0.7f);
					_carousel.ScrollOn(SB_BaseCarousel.POSITION_LEFT,0.7f);
					_pageState=PSTATE_READY;
				}
				
			}
			

			
		}

		override protected void StartLoadingNextScene(){
			transform.Find("Loader").gameObject.SetActive(true);
			base.StartLoadingNextScene();
		}
		
		override protected void OnDestroy()
		{
			Destroy (transform.Find("Loader").gameObject);
			_profile = null;
			


			if(_titleText != null)
			{
				Destroy(_titleText);
				_titleText = null;
			}

			if(_titleWelcome != null)
			{
				Destroy(_titleWelcome);
				_titleWelcome = null;
			}
			
			if(_profileAvatar != null)
			{
				Destroy(_profileAvatar);
				_profileAvatar = null;
			}

			if(_profileAvatarBorder != null)
			{
				Destroy(_profileAvatarBorder);
				_profileAvatarBorder = null;
			}

			if(_profileNameText != null)
			{
				Destroy(_profileNameText);
				_profileNameText = null;
			}
			/*
			if(_userProfileManager != null)
			{
				_userProfileManager = null;
			}
			*/
			if(_carousel != null)
			{
				Destroy (_carousel);
				_carousel = null;
			}

			if(_cameraButton!=null){
				Destroy (_cameraButton);
				_cameraButton=null;
			}

			_carouselItems = null;
			_carouselItemPrefab = null;
			_okButton = null;

			base.OnDestroy();
		}
	}
}