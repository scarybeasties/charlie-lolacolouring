﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Utils;
using UnityEngine.UI;

public class SB_Loader : MonoBehaviour {

	private SB_TextField _text;
	private string _msg;

	void Start()
	{
		SetText();
	}

	void OnEnable()
	{
		SetText();
	}

	public void SetText(string msg="")
	{
		if (msg == "") {
			_msg = SB_Utils.GetNodeValueFromConfigXML ("config/copy/loading/text", "wait");
		} else {
			_msg = msg;
		}
		
		if(transform.Find("loadingtxt") && transform.Find("loadingtxt").GetComponent<SB_TextField>() != null)
		{
			_text = transform.Find("loadingtxt").GetComponent<SB_TextField>();
			_text.text = _msg;
		}
		// Check if we're using the new Canvas textfield
		else if(transform.Find("text") && transform.Find("text").GetComponent<Text>() != null)
		{
			Text canvasText = transform.Find("text").GetComponent<Text>();
			canvasText.text = _msg;
		}
	}

	public void AnimatedWait(int num){

		string txt = SB_Utils.GetNodeValueFromConfigXML ("config/copy/loading/text", "wait_nodots");
		num = num % 4;
		for(int i=0;i<4;i++){
			if(i<num){
				txt = txt+".";
			}
			else{
				txt = txt+" ";
			}

		}
		SetText (txt);
	}

	public void AnimatedPercentWait(float num){

		int pc = (int)(num * 100);
		string txt = SB_Utils.GetNodeValueFromConfigXML ("config/copy/loading/text", "loaded")+" "+pc+"%";

		SetText (txt);
	}

	void Update()
	{
		if(_text != null)
		{
			_text.text = _msg;
		}
	}

	protected void OnDestroy()
	{
		_text = null;
	}
	

}
