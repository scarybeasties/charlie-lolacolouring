﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Utils;
using UnityEngine.UI;
using System.Xml;
using ScaryBeasties.User;
using ScaryBeasties;

public class TutorialScene : MonoBehaviour 
{
	private Text txt;
	private int id = 0;
	private bool _showing = false;

	public static TutorialScene CreateTutorial(){
		string resourceName = "Prefabs/Tutorials/TutorialOverlay";
		GameObject prefab = (GameObject)Resources.Load(resourceName, typeof(GameObject));
		
		// Add the instantiated prefab to the holder gameobject
		return ((GameObject)Instantiate(prefab)).GetComponent<TutorialScene>();
	}

	public static bool IsCurrentTutorial(string tname)
	{
		//if (SB_UserProfileManager.instance.CurrentProfile.tutorialNum >= SB_Globals.TUTORIAL_IDS.Length)
		if (SB_UserProfileManager.instance.CurrentProfile.tutorialNum >= SB_Globals.TUTORIAL_IDS.GetLength(0))
			return false;
		
		//Debug.LogError("---> tutorialNum: " + SB_UserProfileManager.instance.CurrentProfile.tutorialNum + ", SB_Globals.TUTORIAL_IDS.Length: " + SB_Globals.TUTORIAL_IDS.Length + ", " + SB_Globals.TUTORIAL_IDS [SB_UserProfileManager.instance.CurrentProfile.tutorialNum, 0] + ", SB_Globals.TUTORIAL_IDS.GetLength(0): " + SB_Globals.TUTORIAL_IDS.GetLength(0));
			
		if (SB_Globals.TUTORIAL_IDS [SB_UserProfileManager.instance.CurrentProfile.tutorialNum, 0] == tname)
			return true;
		return false;
	}

	// Use this for initialization
	void Start () {

	}

	public bool IsShowing(){
		return _showing;
	}

	public void ShowTutorial(){
		ShowTutorial (SB_UserProfileManager.instance.CurrentProfile.tutorialNum);
	}

	public void ShowTutorial(Vector2 screenpos, Vector2 pointpos){
		ShowTutorial (SB_UserProfileManager.instance.CurrentProfile.tutorialNum,screenpos,pointpos);
	}

	public void ShowTutorial(int tnum){
		ShowTutorial (tnum, new Vector2 (0, 0), new Vector2 (0, 0));
	}

	public void ShowTutorial(int tnum, Vector2 screenpos, Vector2 pointpos){
		txt = transform.Find ("TutorialController/TutorialBox/Text").gameObject.GetComponent<Text> ();
		string sid = SB_Globals.TUTORIAL_IDS [tnum, 0];
		XmlNode result = ScaryBeasties.SB_Globals.CONFIG_XML.GetNode("config/tutorials/tutorial[@id='" + sid +"']");
	
		txt.text = result.InnerText;

		if ((screenpos.x == 0) && (screenpos.y == 0)) {
			string cposs = result.Attributes.GetNamedItem ("position").Value;
			string[] ctpos = cposs.Split (',');
			screenpos = new Vector2 (float.Parse (ctpos [0]), float.Parse (ctpos [1]));
		}
		transform.position = screenpos;


		if ((pointpos.x == 0) && (pointpos.y == 0)) {
			string pposs =  result.Attributes.GetNamedItem("point").Value;
			string[] ptpos = pposs.Split (',');
			pointpos = new Vector2 (float.Parse (ptpos [0]), float.Parse (ptpos [1]));
		} 

		if ((pointpos.x == 0) && (pointpos.y == 0)) {
			transform.Find ("TutorialController/TutorialBox/Background/PointerHolder").gameObject.SetActive(false);
		} else {
			transform.Find ("TutorialController/TutorialBox/Background/PointerHolder").gameObject.SetActive(true);
		}

		float angle = SB_Utils.GetAngle (pointpos, screenpos);
		transform.Find ("TutorialController/TutorialBox/Background/PointerHolder").gameObject.transform.rotation = Quaternion.Euler (new Vector3 (0, 0, angle));
		transform.Find ("TutorialController/TutorialBox/Background/PointerHolder/Pointer").gameObject.transform.position = pointpos;
		transform.Find ("TutorialController/TutorialBox/Background/PointerHolder/PointerShadow").gameObject.transform.position = new Vector2(pointpos.x+3,pointpos.y-3);

		Animator tanim = transform.Find ("TutorialController").gameObject.GetComponent<Animator> ();
		tanim.Play ("AnimOn");

		id = tnum;
		_showing = true;
		
	}

	public void CompleteTutorialStep()
	{
		/*
		if (id == SB_UserProfileManager.instance.CurrentProfile.tutorialNum) {
			SB_UserProfileManager.instance.CurrentProfile.tutorialNum=id+1;
			id++;
			SB_UserProfileManager.instance.SaveProfileData(SB_UserProfileManager.instance.CurrentProfile);
		}
		*/
		IncrementTutorial();
		Animator tanim = transform.Find ("TutorialController").gameObject.GetComponent<Animator> ();
		tanim.Play ("AnimOff");
		_showing = false;

	}

	public static int GetTutorialNum(string tname){

		//for (int i =0; i<SB_Globals.TUTORIAL_IDS.Length; i++)
		for (int i =0; i<SB_Globals.TUTORIAL_IDS.GetLength(0); i++)
		{
			if(SB_Globals.TUTORIAL_IDS[i, 0]==tname) return i;
		}
		return -1;
	}
	
	/**
	 * Return the button name which needs to be pressed, for the current tutorial step
	 */
	public string GetTutorialButton()
	{
		return (string)SB_Globals.TUTORIAL_IDS[id, 1];
	}

	public void IncrementTutorial(){
		
		if (id == SB_UserProfileManager.instance.CurrentProfile.tutorialNum) 
		{
			/*
			SB_UserProfileManager.instance.CurrentProfile.tutorialNum=id+1;
			id++;
			SB_UserProfileManager.instance.SaveProfileData(SB_UserProfileManager.instance.CurrentProfile);
			*/
			MoveToTutorialStep(id+1);
		}
		_showing = false;

	}

	public void MoveToTutorialStep(int step){
		
		SB_UserProfileManager.instance.CurrentProfile.tutorialNum=step;
		id=step;
		SB_UserProfileManager.instance.SaveProfileData(SB_UserProfileManager.instance.CurrentProfile);

		_showing = false;
		
		//SB_Analytics.logEvent("Tutorial_Step_" + id);
	}
	
	public void ResetTutorial()
	{
		MoveToTutorialStep(0);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnDestroy()
	{
		txt = null;
	}
}
