﻿using UnityEngine;
using System.Collections;

namespace ScaryBeasties.UI.SwipeMenu
{
	public class SwipeMenuPageSet : MonoBehaviour {
		
		public Vector2 gridMax,gridGaps;
		private ArrayList _pages = new ArrayList();
		public int pageSetID = 0;
		
		// Use this for initialization
		void Start () {
			
			gridMax = new Vector2 (4, 3);
			gridGaps = new Vector2 (0.0f, 0.0f);
			
		}
		
		public void AddPage(SwipeMenuPage item){
			_pages.Add (item);
		}
		
		public void RemovePage(SwipeMenuPage item){
			_pages.Remove (item);
		}
		
		public int NumPages(){
			return _pages.Count;
		}
		
		protected void OnDestroy()
		{
			_pages = null;
		}
	}
}

