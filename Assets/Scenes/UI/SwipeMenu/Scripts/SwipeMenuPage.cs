﻿using UnityEngine;
using System.Collections;

namespace ScaryBeasties.UI.SwipeMenu
{
	public class SwipeMenuPage : MonoBehaviour {
		
		
		public Vector2 gridMax,gridGaps;
		private ArrayList _menuItems = new ArrayList();
		public int pageSetID = 0;
		
		// Use this for initialization
		void Start () {

			//gridMax = new Vector2 (4, 3);
			//gridGaps = new Vector2 (0.0f, 0.0f);

		}

		public void AddMenuItem(SwipeMenuItem item){
			_menuItems.Add (item);
		}

		public void RemoveMenuItem(SwipeMenuItem item){
			_menuItems.Remove (item);
		}


		public int NumItems(){
			return _menuItems.Count;
		}

		public SwipeMenuItem GetMenuItemAt(int num){
			return (SwipeMenuItem) _menuItems[num];
		}

		public SwipeMenuItem GetMenuItemAtLocation(Vector3 pos){

			for (int i=0; i<_menuItems.Count; i++) {



				SwipeMenuItem tobj = (SwipeMenuItem) _menuItems[i];
				BoxCollider2D col = tobj.GetComponent<BoxCollider2D>();

				if(col.OverlapPoint(new Vector2(pos.x,pos.y))){
				
					return tobj;
				}
			}
			return null;
			
		}
		
		public void Enable(bool val){
			for (int i=0; i<_menuItems.Count; i++) {
				SwipeMenuItem tobj = (SwipeMenuItem)_menuItems [i];
				BoxCollider2D col = tobj.GetComponent<BoxCollider2D> ();
				col.enabled = val;
			}
		}

		protected void OnDestroy()
		{
			while(_menuItems.Count>0) {
				SwipeMenuItem titem = (SwipeMenuItem) _menuItems[0];
				Destroy (titem.gameObject);
				_menuItems.RemoveAt(0);
				
				titem = null;
			}
			
			_menuItems = null;
		}
	}
}

