﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Cam;
using UnityEngine.UI;
using System;
using ScaryBeasties.Sound;

namespace ScaryBeasties.UI.SwipeMenu
{
	public class SwipeMenu : MonoBehaviour
	{
		public const int BTN_SOUND_CHANNEL = 7;
		
		//ACTIONS
		public Action<SwipeMenuItem> OnHandleTap;
		public Action<int> OnPageSetChanged;
		public Action<bool> OnPressedOutsideBounds;

		public bool PageSwipeEnabled = true;
		public Color PipColour = new Color (0, 0, 0);

		private Vector2 _centre = new Vector2 (0.0f, 0.0f);
		private float _screenWidth=0;
		private float _screenHeight=0;
		private float _screenSwapExtent;
		private float _defaultZoom=0;
		private float _zoomFactor=0;

		private ArrayList _pages = new ArrayList ();
		private int _pageSets = 0;

		private int _currentPage = 0;

		private bool _enabled = true;
		private bool _mouseDown = false;

		//SNAPPING
		private bool _snapping = false;
		private float _snapStartX, _snapEndX, _snapStartTime;
		private const float MIN_SNAP_DIST = 5.0f;
		private const float SNAP_SPEED = 0.2f;
		private const float SWIPE_SENSITIVITY = 100.0f;
		//DRAGGING
		private Vector3 _initialDragPos, _currentDragPos, _lastDragPos;
		private ArrayList _dragPositions = new ArrayList ();
		private const int DRAGLIST_LENGTH = 30;
		//private float _xmove = 0.0f;
		private const float DECEL = 10.0f;
		//SELECTING
		private const float TAP_SENSITIVITY = 1.0f;		//time to register it as a tap
		private const float TAP_DIST = 10.0f; //1.0f;
		private float _startTapTime = 0.0f;
		private SwipeMenuItem _tapItem = null;
		private Vector2 _tapStartPos;
		private int _arrowDirection = 0;

		//PIPS
		private ArrayList _pips = new ArrayList ();
		private float _pipGap = 10.0f;

		public bool Paused = false;
		private float unpauseDelay = 0f;
		private const float MAX_UNPUASE_DELAY = 0.5f;


		//ARROWS
		private GameObject _nextArrow;
		private GameObject _prevArrow;
		private bool _arrowPress = false;

		//Initial Anim
		private bool _initialAnim = false;

		public Bounds bounds = new Bounds();
		private float _boundsMinX = 0f;
		private float _boundsMaxX = 0f;
		private float _boundsMinY = 0f;
		private float _boundsMaxY = 0f;

	
		// GETTERS
		public SwipeMenuPage CurrentPage {
			get {
				if (_pages != null && _currentPage > -1) {
					SwipeMenuPage currentPage = (SwipeMenuPage)_pages [_currentPage];
					return currentPage;
				}
				
				return null; 
			}
		}
	

		// Use this for initialization
		void Start ()
		{
			tk2dBaseSprite spr = transform.Find ("SwipeMenuPip/On").gameObject.GetComponent<tk2dBaseSprite> ();
			spr.color = PipColour;
			SetScreenSize ();
			SetExtents ();
			SetupArrows ();
			
			_boundsMinX = bounds.center.x-bounds.extents.x;
			_boundsMaxX = bounds.center.x+bounds.extents.x;
			_boundsMinY = bounds.center.y-bounds.extents.y;
			_boundsMaxY = bounds.center.y+bounds.extents.y;
		}


		public void SetScreenSize ()
		{

			_screenWidth = ZoomPoint ((float)tk2dCamera.Instance.ScreenExtents.size.x);
			_screenHeight = (float)tk2dCamera.Instance.ScreenExtents.size.y;
			_screenSwapExtent = 0.3f * _screenWidth;

		}

		private void SetExtents ()
		{
			Debug.LogWarning("SetExtents()");
			
			if (_screenWidth == 0) {
				_screenWidth = ZoomPoint ((float)tk2dCamera.Instance.ScreenExtents.size.x);
				_screenHeight = (float)tk2dCamera.Instance.ScreenExtents.size.y;
			}
			_screenSwapExtent = 0.3f * _screenWidth;
			_centre = new Vector2 (transform.position.x, transform.position.y);
		}

		float ZoomPoint (float pt)
		{
			Debug.LogError ("CAMERA ZOOMS " + _defaultZoom + " " + _zoomFactor);
			if ((_defaultZoom == 0) || (_zoomFactor == 0)) {
				_defaultZoom = SB_GameCamera.defaultZoom;
				_zoomFactor = (float)tk2dCamera.Instance.ZoomFactor;
			}
			if (_defaultZoom > 0) {
				return pt * (_defaultZoom / _zoomFactor);
			}
			return pt;
		}

		public void AddPageSet (ArrayList items, Vector2 grid, Vector2 gaps, int pageSetID = -1)
		{

			int maxPerPage = (int)grid.x * (int)grid.y;

			SwipeMenuPage cpage = null;
			float sx = (0 - ((grid.x * gaps.x) / 2.0f)) + (gaps.x / 2.0f);
			float sy = (0 + ((grid.y * gaps.y) / 2.0f)) - (gaps.y / 2.0f);


			int startPage = -1;
			if (pageSetID >= 0) {
				for (int i=0; i<_pages.Count; i++) {
					SwipeMenuPage tpage = (SwipeMenuPage)_pages [i];
					if (tpage.pageSetID > pageSetID) {
						startPage = i;
						break;
					}
				}
			}


			for (int i=0; i<items.Count; i++) {

				if (i % maxPerPage == 0) {
					cpage = ((GameObject)Instantiate (transform.Find ("SwipeMenuPage").gameObject)).GetComponent<SwipeMenuPage> ();
					if (startPage >= 0) {
						_pages.Insert (startPage, cpage);
						startPage++;
					} else {
						_pages.Add (cpage);
					}
					cpage.gridMax = new Vector2 (grid.x, grid.y);
					cpage.gridGaps = new Vector2 (gaps.x, gaps.y);

					if (pageSetID < 0)
						cpage.pageSetID = _pageSets;
					else
						cpage.pageSetID = pageSetID;
					cpage.gameObject.transform.SetParent (transform.Find ("Pages").gameObject.transform);
				}

				SwipeMenuItem mitem = (SwipeMenuItem)items [i];
				mitem.index = i;
				mitem.gameObject.transform.SetParent (cpage.transform);


				int xpos = (int)(i % grid.x);
				int ypos = (int)((i % maxPerPage) / grid.x);
				mitem.gameObject.transform.position = new Vector2 (sx + (xpos * gaps.x), sy - (ypos * gaps.y));

				cpage.AddMenuItem (mitem);


			}



			ResetToPage (0);
			SetupPips ();
			if (pageSetID < 0)
				_pageSets++;

			ShowArrows ();
		}

		public void DoInitialAnim(){

			if ((_currentPage==0)&&(_pages.Count>1)) {
				_initialAnim = true;

				/*SwipeMenuPage page = (SwipeMenuPage)_pages [_currentPage];
				page.gameObject.transform.position = new Vector2 (_centre.x + _screenWidth, _centre.y);
				SwipeMenuPage page2 = (SwipeMenuPage)_pages [_currentPage+1];
				page.gameObject.transform.position = new Vector2 (_centre.x + (2*_screenWidth), _centre.y);
*/
				SetPagePosition(0 + _screenWidth);

				_snapStartTime = Time.time;
				_snapStartX = _centre.x + _screenWidth;
				_snapEndX = 0 - (_screenWidth/2);

			}
			HideArrows ();
		}
		
		public void ClearMenu ()
		{
			while (_pages.Count>0) {
				SwipeMenuPage tpage = (SwipeMenuPage)_pages [0];
				_pages.Remove (tpage);
				Destroy (tpage.gameObject);

			}
		}

		private void DeletePageSet (int pageSetID)
		{

			for (int i=0; i<_pages.Count; i++) {
				SwipeMenuPage tpage = (SwipeMenuPage)_pages [i];
				if (tpage.pageSetID == pageSetID) {
					_pages.Remove (tpage);
					Destroy (tpage.gameObject);
					i--;
				}
			}	
		}


		public void DeleteItem (SwipeMenuItem mitem)
		{

			int pageSetID = -1;
			Vector2 gridMax = new Vector2 ();
			Vector2 gridGaps = new Vector2 ();

			ArrayList items = new ArrayList ();

			for (int i=0; i<_pages.Count; i++) {
				SwipeMenuPage cpage = (SwipeMenuPage)_pages [i];



				for (int j=0; j<cpage.NumItems(); j++) {

					SwipeMenuItem titem = cpage.GetMenuItemAt (j);
					if (titem == mitem) {
						pageSetID = cpage.pageSetID;
						gridMax = new Vector2 (cpage.gridMax.x, cpage.gridMax.y);
						gridGaps = new Vector2 (cpage.gridGaps.x, cpage.gridGaps.y);
						cpage.RemoveMenuItem (titem);
						Destroy (titem);
					}


				}
			}

			for (int i=0; i<_pages.Count; i++) {
				SwipeMenuPage cpage = (SwipeMenuPage)_pages [i];
				if (cpage.pageSetID == pageSetID) {
					for (int j=0; j<cpage.NumItems(); j++) {
						SwipeMenuItem titem = cpage.GetMenuItemAt (j);
						cpage.RemoveMenuItem (titem);
						//titem.transform.parent = transform;
						titem.transform.SetParent (transform);
						items.Add (titem);
						j--;
					}
				}
			}
			int currentPage = _currentPage;

			DeletePageSet (pageSetID);
			AddPageSet (items, gridMax, gridGaps, pageSetID);
			ResetToPage (currentPage);
			ShowArrows ();
		}


		//Arrows

		private bool UseArrows(){

			if (_nextArrow != null)
				return true;

			return false;

		}

		private void SetupArrows(){

			_nextArrow = transform.Find ("NextArrow").gameObject;
			_prevArrow = transform.Find ("PrevArrow").gameObject;

			if (UseArrows ()) {
				tk2dBaseSprite spr = _nextArrow.transform.Find ("Colour").gameObject.GetComponent<tk2dBaseSprite> ();
				//spr.color = PipColour;
				tk2dBaseSprite spr2 = _prevArrow.transform.Find ("Colour").gameObject.GetComponent<tk2dBaseSprite> ();
				//spr2.color = PipColour;
				HideArrows();
			}

		}

		public void ShowArrows(){

			if (UseArrows ()) {

				if(_currentPage>0){
					_prevArrow.SetActive(true);
				}
				else{
					_prevArrow.SetActive(false);
				}

				if(_currentPage<(_pages.Count-1)){
					_nextArrow.SetActive(true);
				}
				else{
					_nextArrow.SetActive(false);
				}
				_arrowPress=false;
			}

		}

		private void HideArrows(){
			if (UseArrows ()) {
				_prevArrow.SetActive(false);
				_nextArrow.SetActive(false);
			}
		}


		//Pips

		private void SetupPips ()
		{

			while (_pips.Count<_pages.Count) {
				GameObject pip = (GameObject)Instantiate (transform.Find ("SwipeMenuPip").gameObject);
				tk2dBaseSprite spr = pip.transform.Find ("On").gameObject.GetComponent<tk2dBaseSprite> ();
				spr.color = PipColour;
				_pips.Add (pip);
			}

			while (_pips.Count>_pages.Count) {
				GameObject tpipt = (GameObject)_pips [0];
				_pips.Remove (tpipt);
				Destroy (tpipt);
			}

			float sx = (0 - ((_pips.Count * _pipGap) / 2.0f)) + (_pipGap / 2.0f);
			for (int i=0; i<_pips.Count; i++) {
				GameObject tpip = (GameObject)_pips [i];

				tpip.transform.SetParent (transform.Find ("Pips").gameObject.transform);
				tpip.transform.position = new Vector2 (sx + (i * _pipGap), transform.Find ("Pips").gameObject.transform.position.y);
			}
			SetCurrentPip ();
		}

		private void SetCurrentPip ()
		{
			for (int i=0; i<_pips.Count; i++) {
				GameObject tpip = (GameObject)_pips [i];
				if (i == _currentPage) {
					tpip.transform.Find ("On").gameObject.SetActive (false);
					tpip.transform.Find ("Off").gameObject.SetActive (true);
				} else {
					tpip.transform.Find ("On").gameObject.SetActive (true);
					tpip.transform.Find ("Off").gameObject.SetActive (true);
				}
			}
		}

		private void SetCurrentPage (int pnum)
		{
			Debug.LogWarning("SetCurrentPage() pnum: " + pnum);
			
			int prevPageSetID = -1;
			if ((_currentPage >= 0) && (_currentPage < _pages.Count)) {
				SwipeMenuPage prevPage = (SwipeMenuPage)_pages [_currentPage];
				prevPageSetID = prevPage.pageSetID;
			}
			
			_currentPage = pnum;
			SetCurrentPip ();
			//ShowArrows ();
			
			SwipeMenuPage currentPage = (SwipeMenuPage)_pages [_currentPage];
			if (currentPage.pageSetID != prevPageSetID) {
				if (OnPageSetChanged != null) {
					OnPageSetChanged (currentPage.pageSetID);
				}
			}
		}

		public void ResetToPage (int pnum)
		{
			Debug.LogWarning("ResetToPage pnum: " + pnum);
			
			SetExtents ();

			for (int i=0; i<_pages.Count; i++) {

				SwipeMenuPage page = (SwipeMenuPage)_pages [i];
				if (i < pnum) {
					page.gameObject.transform.position = new Vector2 (_centre.x - _screenWidth, _centre.y);
				} else if (i > pnum) {

					page.gameObject.transform.position = new Vector2 (_centre.x + _screenWidth, _centre.y);
				} else {
					page.gameObject.transform.position = new Vector2 (_centre.x, _centre.y);
				}
			}

			SetCurrentPage (pnum);

		}

		private void StartTap (Vector3 mpos)
		{
			_arrowDirection = 0;
			_tapItem=null;
			_tapItem = ((SwipeMenuPage)_pages [_currentPage]).GetMenuItemAtLocation (mpos);
			_tapStartPos = new Vector2 (mpos.x, mpos.y);
			if (_tapItem != null) {
				if(_tapItem.enabled){
					_startTapTime = Time.time;
				}
				else{
					_tapItem=null;
				}
			} else {
				BoxCollider2D col1 = _nextArrow.GetComponent<BoxCollider2D>();
				if(col1.OverlapPoint(new Vector2(mpos.x,mpos.y))){
					_arrowDirection = 1;
					_startTapTime = Time.time;
				}

				BoxCollider2D col2 = _prevArrow.GetComponent<BoxCollider2D>();
				if(col2.OverlapPoint(new Vector2(mpos.x,mpos.y))){
					_arrowDirection = -1;
					_startTapTime = Time.time;
				}
			}

		}

		public void Enable (bool val)
		{
			_enabled = val;
			for (int i= 0; i<_pages.Count; i++) {
				SwipeMenuPage page = (SwipeMenuPage)_pages [i];
				page.Enable (val);
			}

			//if (_enabled) {
			//	ShowArrows ();
			//}
		}

		private void GetNearestPage ()
		{
			Debug.LogWarning("GetNearestPage");
			
			int tpage = -1;
			float tdist = _screenWidth;
			for (int i=0; i<_pages.Count; i++) {
				SwipeMenuPage tp = ((SwipeMenuPage)_pages [i]);
				float xdist = Mathf.Abs (tp.transform.position.x - _centre.x);
				if (xdist < tdist) {
					tpage = i;
					tdist = xdist;
				}
			}

			if (tpage >= 0) {
				SetCurrentPage (tpage);
			}

		}
		// Update is called once per frame
		//void Update () {


		//}

		void LateUpdate ()
		{
			// GT - when unpausing, wait for a short while before allowing this function to execute.
			// This is to fix an issue where closing a popup in a scene would cause the SwipeMenu to
			// immediately jump relative to the mouse position when the popup was closed.
			if (Paused) {
				unpauseDelay = MAX_UNPUASE_DELAY;
				return;
			} else if (unpauseDelay > 0f) {
				unpauseDelay -= Time.deltaTime;
			}
			
			if (unpauseDelay > 0) {
				_mouseDown = false;
				return;
			}
		
			if (_initialAnim) {

				float stime = Time.time - _snapStartTime;
				float pos = 0.0f;
				float enddist = _snapEndX - _snapStartX;
				SwipeMenuPage page = (SwipeMenuPage)_pages [_currentPage];
				SwipeMenuPage page2 = (SwipeMenuPage)_pages [_currentPage+1];

				float speed = 1.0f;


				stime /= speed / 2;
				if (stime < 1) {
					pos = (enddist / 2) * stime * stime + _snapStartX;
				} else {
					stime--;
					pos = -(enddist/2)*(stime*(stime-2)-1)+_snapStartX;

					if(stime>=1.0f){
						_initialAnim=false;
						_snapStartX = ((SwipeMenuPage)_pages [_currentPage]).gameObject.transform.position.x;
						_snapEndX = _centre.x;
						_snapStartTime = Time.time;
						_snapping = true;
					}
				}
				
				SetPagePosition (pos);
			}
			else if ((_enabled) && (_pages.Count > 0)) 
			{
				Vector3 mousepos = Camera.main.ScreenToWorldPoint (UnityEngine.Input.mousePosition);

				// End here, if user has not pressed within the 'bounds' area
				if(_mouseDown && !PressedInsideBounds(mousepos))
				{
					if(OnPressedOutsideBounds != null) OnPressedOutsideBounds(true);
					return;
				}

				_lastDragPos = _currentDragPos;
				_currentDragPos = mousepos;

				_dragPositions.Add (_currentDragPos);
				while (_dragPositions.Count>DRAGLIST_LENGTH) {
					_dragPositions.RemoveAt (0);
				}
				//if(!_snapping){
				if (UnityEngine.Input.GetMouseButtonDown (0)) {
					//_xmove=0;
					if(!_arrowPress)
					{
						/**/
						GetNearestPage ();
						_mouseDown = true;
						_snapping = false;
						_initialDragPos = mousepos;
						_currentDragPos = mousepos;
						_lastDragPos = _currentDragPos;
				
						_dragPositions.Clear ();

						StartTap (mousepos);
					}
				} else if (UnityEngine.Input.GetMouseButtonUp (0)) {
					_mouseDown = false;

					//if(_xmove==0) 
					//SnapBack ();
					if(!_arrowPress){
						bool tapped = false;
						float dx = mousepos.x - _tapStartPos.x;
						float dy = mousepos.y - _tapStartPos.y;
						float tapdist = Mathf.Sqrt((dx*dx)+(dy*dy));
						if (_tapItem != null) {
							if (Time.time - _startTapTime < TAP_SENSITIVITY) {
								if(tapdist<TAP_DIST){
									if (((SwipeMenuPage)_pages [_currentPage]).GetMenuItemAtLocation (mousepos) == _tapItem) {
										//SB_SoundManager.instance.PlaySound(SB_Globals.MAIN_BUTTON_SOUND, SB_Globals.MAIN_BUTTON_VOLUME, SB_Globals.MAIN_BUTTON_SOUND_CHANNEL);
										SB_SoundManager.instance.PlaySound(SB_Globals.MAIN_BUTTON_SOUND2, SB_Globals.MAIN_BUTTON_VOLUME, SB_Globals.MAIN_BUTTON_SOUND_CHANNEL);
										if (OnHandleTap != null)
											OnHandleTap (_tapItem);
										_tapItem = null;
										tapped=true;
									}
								}

							}
						}
						else if ((_arrowDirection!=0)) {
							//if (Time.time - _startTapTime < TAP_SENSITIVITY) {
								if(tapdist<TAP_DIST){
									ArrowMove();
									tapped=true;
								}
							//}
						}

						if(!tapped)
						{
							SnapBack();
						}
					}

					_tapItem = null;
				}

				//}

				if ((_mouseDown)&&(!_arrowPress) && PageSwipeEnabled) 
				{
					if(_pages.Count > 1)
					{
						//_lastDragPos = _currentDragPos;
						float xmove = _currentDragPos.x - _lastDragPos.x;
						//_currentDragPos=mousepos;
						MovePages (xmove);
						HideArrows();
						//_xmove=xmove;
					}
				}
				/*else if(Mathf.Abs (_xmove)>0){

					if(_xmove>0){
						_xmove -= (Time.deltaTime*DECEL);
						if(_xmove<0) _xmove = 0;
					}
					if(_xmove<0){
						_xmove += (Time.deltaTime*DECEL);
						if(_xmove>0) _xmove = 0;

					}
					if((_xmove==0)||(ChangePage())) SnapBack();
					else MovePages(_xmove);
				}*/
				else if (_snapping) {

					float stime = Time.time - _snapStartTime;
					float pos = 0.0f;
					float enddist = _snapEndX - _snapStartX;
					stime /= SNAP_SPEED / 2;
					if (stime < 1) {
						pos = (enddist / 2) * stime * stime + _snapStartX;
					} else {
						//	stime--;
						//	pos = -(enddist/2)*(stime*(stime-2)-1)+_snapStartX;

						stime--;
						pos = -(enddist/2)*(stime*(stime-2)-1)+_snapStartX;
						
						if(stime>=1.0f){
							SetPagePosition (_centre.x);
							_snapping = false;
							ShowArrows ();
						}

					}

					//Debug.LogError("POS "+pos+" END "+_snapEndX+" CENTER "+_centre.x);
					if (_snapping) {
						if (Mathf.Abs (pos - _snapEndX) < 1.0f) {
							SetPagePosition (_centre.x);
							_snapping = false;
							ShowArrows();

						} else {
							SetPagePosition (pos);
						}
					}

				}

			}
		}

		bool PressedInsideBounds(Vector3 mousepos)
		{
			_boundsMinX = transform.position.x + (bounds.center.x-bounds.extents.x);
			_boundsMaxX = transform.position.x + (bounds.center.x+bounds.extents.x);
			_boundsMinY = transform.position.y + (bounds.center.y-bounds.extents.y);
			_boundsMaxY = transform.position.y + (bounds.center.y+bounds.extents.y);
		
			if(mousepos.x > _boundsMinX && mousepos.x < _boundsMaxX)
			{
				if(mousepos.y > _boundsMinY && mousepos.y < _boundsMaxY)
				{
					//Debug.LogError("INSIDE BOUNDS!");
					return true;
				}
			}
			
			return false;
		}

		private void MovePages (float xmove)
		{
			float xpos = ((SwipeMenuPage)_pages [_currentPage]).gameObject.transform.position.x - _centre.x;
			//Debug.LogError (xpos);
			if ((xmove > 0) && (xpos > 0)) {//&&(xpos <= (_screenWidth / 8.0f))) {
				if (_currentPage == 0) {
					//float xpos = ((SwipeMenuPage)_pages [_currentPage]).gameObject.transform.position.x - _centre.x;
					if (xpos > (_screenWidth / 8.0f))
						xmove = 0;
					else {
						xmove = xmove * (Mathf.Abs ((_screenWidth / 8.0f) - xpos) / (_screenWidth / 8.0f));
					}
				}
			} else if ((xmove < 0) && (xpos < 0)) {//&&(xpos >= 0 - (_screenWidth / 8.0f))) {
				if (_currentPage == _pages.Count - 1) {

					//float xpos = ((SwipeMenuPage)_pages [_currentPage]).gameObject.transform.position.x - _centre.x;
					if (xpos < 0 - (_screenWidth / 8.0f))
						xmove = 0;
					else {
						xmove = xmove * (((_screenWidth / 8.0f) - Mathf.Abs (xpos)) / (_screenWidth / 8.0f));

						//xmove = xmove * (((_screenWidth / 8.0f)-xpos) / (_screenWidth / 8.0f));

						//xmove = xmove * (Mathf.Abs ((_screenWidth- (xpos))-(7*(_screenWidth / 8.0f))) / (_screenWidth / 8.0f));
					}
				}
			}

			
			SetPagePosition (((SwipeMenuPage)_pages [_currentPage]).gameObject.transform.position.x + xmove);

		}
			
		private void SetPagePosition (float xpos, float ypos=0)
		{
			Debug.LogWarning("SetPagePosition xpos: " + xpos + ", ypos: " + ypos);
			
			GameObject thisPage = ((SwipeMenuPage)_pages [_currentPage]).gameObject;

			float xmove = xpos - thisPage.transform.position.x;

			//thisPage.transform.position = new Vector2 (thisPage.transform.position.x + xmove, bounds.center.y);//thisPage.transform.position.y);
			thisPage.transform.localPosition = new Vector2 (thisPage.transform.position.x + xmove, ypos);//thisPage.transform.position.y);
			
			if (_currentPage > 0) {
				GameObject prevPage = ((SwipeMenuPage)_pages [_currentPage - 1]).gameObject;
				//prevPage.transform.position = new Vector2 (prevPage.transform.position.x + xmove, bounds.center.y);//prevPage.transform.position.y);
				prevPage.transform.localPosition = new Vector2 (prevPage.transform.position.x + xmove, ypos);//prevPage.transform.position.y);
			}
			if (_currentPage + 1 < _pages.Count) {
				GameObject nextPage = ((SwipeMenuPage)_pages [_currentPage + 1]).gameObject;
				//nextPage.transform.position = new Vector2 (nextPage.transform.position.x + xmove, bounds.center.y);//nextPage.transform.position.y);
				nextPage.transform.localPosition = new Vector2 (nextPage.transform.position.x + xmove, ypos);//nextPage.transform.position.y);
			}

		}

		private void SnapBack ()
		{
			Debug.LogWarning("SnapBack");
			
			//	_xmove = 0;
			GameObject thisPage = ((SwipeMenuPage)_pages [_currentPage]).gameObject;
			float xdist = thisPage.transform.position.x - _centre.x;

			Vector3 lastDragPos = new Vector3 (0, 0, 0);
			if (_dragPositions.Count > 0)
				lastDragPos = (Vector3)_dragPositions [0];
			float xmove = lastDragPos.x - _currentDragPos.x;

			//TEST FOR PAGE CHANGE

			bool swapPage = false;
			//Debug.LogError ("SENS " + xmove+" "+lastDragPos+" "+_currentDragPos);
			if (xmove > SWIPE_SENSITIVITY) {
				swapPage = true;
			}
			if (Mathf.Abs (xdist) >= MIN_SNAP_DIST) {
				if (xdist > _screenSwapExtent) {
					swapPage = true;

				} else if (xdist < (0 - (_screenSwapExtent))) {
					swapPage = true;
				}
			}

			//if (Mathf.Abs(xdist) < MIN_SNAP_DIST) {
			if (!swapPage) {
				SetPagePosition (_centre.x);
				//SetCurrentPage(_currentPage);
				ShowArrows();
			} else {

				//if(xdist>_screenSwapExtent){
				if (xdist > 0) {
					if (_currentPage > 0) {
						SetCurrentPage (_currentPage - 1);
						xdist = ((SwipeMenuPage)_pages [_currentPage]).gameObject.transform.position.x - _centre.x;
						if (_currentPage > 0) {

							((SwipeMenuPage)_pages [_currentPage - 1]).gameObject.transform.position = new Vector2 (((SwipeMenuPage)_pages [_currentPage]).gameObject.transform.position.x - _screenWidth, _centre.y);
						}
					}
				}
				//else if(xdist<(0-(_screenSwapExtent))){
				else if (xdist < 0) {
					if (_currentPage + 1 < _pages.Count) {
						SetCurrentPage (_currentPage + 1);
						xdist = ((SwipeMenuPage)_pages [_currentPage]).gameObject.transform.position.x - _centre.x;
						if (_currentPage + 1 < _pages.Count) {
							((SwipeMenuPage)_pages [_currentPage + 1]).gameObject.transform.position = new Vector2 (((SwipeMenuPage)_pages [_currentPage]).gameObject.transform.position.x + _screenWidth, _centre.y);

						}
					}
				}

				SB_SoundManager.instance.PlaySound ("Audio/SFX/UI/SndPopup1", 0.1f, BTN_SOUND_CHANNEL);

				_snapStartX = ((SwipeMenuPage)_pages [_currentPage]).gameObject.transform.position.x;
				_snapEndX = _centre.x;
				_snapStartTime = Time.time;
				_snapping = true;

			}


		}

		private void ArrowMove(){
			if (_arrowDirection < 0) {
				if (_currentPage > 0) {
					SetCurrentPage (_currentPage - 1);
					//xdist = ((SwipeMenuPage)_pages [_currentPage]).gameObject.transform.position.x - _centre.x;
					if (_currentPage > 0) {
						
						((SwipeMenuPage)_pages [_currentPage - 1]).gameObject.transform.position = new Vector2 (((SwipeMenuPage)_pages [_currentPage]).gameObject.transform.position.x - _screenWidth, _centre.y);
					}
				}
			}
			//else if(xdist<(0-(_screenSwapExtent))){
			else if (_arrowDirection > 0) {
				if (_currentPage + 1 < _pages.Count) {
					SetCurrentPage (_currentPage + 1);
					//xdist = ((SwipeMenuPage)_pages [_currentPage]).gameObject.transform.position.x - _centre.x;
					if (_currentPage + 1 < _pages.Count) {
						((SwipeMenuPage)_pages [_currentPage + 1]).gameObject.transform.position = new Vector2 (((SwipeMenuPage)_pages [_currentPage]).gameObject.transform.position.x + _screenWidth, _centre.y);
						
					}
				}
			}
			
			SB_SoundManager.instance.PlaySound ("Audio/SFX/UI/SndPopup1", 0.1f, BTN_SOUND_CHANNEL);
			
			_snapStartX = ((SwipeMenuPage)_pages [_currentPage]).gameObject.transform.position.x;
			_snapEndX = _centre.x;
			_snapStartTime = Time.time;
			_snapping = true;
			_arrowDirection = 0;
			_arrowPress = true;
		}

		private bool ChangePage ()
		{
			Debug.LogWarning("ChangePage");
			
			GameObject thisPage = ((SwipeMenuPage)_pages [_currentPage]).gameObject;
			float xdist = thisPage.transform.position.x - _centre.x;
			if (xdist > _screenSwapExtent) {
				if (_currentPage > 0) {
					return true;
				}
			} else if (xdist < (0 - (_screenSwapExtent))) {
				if (_currentPage + 1 < _pages.Count) {
					return true;
				}
			}
			return false;
		}

		public int GetCurrentPageNumber ()
		{
			return _currentPage;
		}

		protected void OnDestroy ()
		{
			_tapItem = null;
			while (_pages.Count>0) {
				SwipeMenuPage tpage = (SwipeMenuPage)_pages [0];
				Destroy (tpage.gameObject);
				_pages.RemoveAt (0);

				tpage = null;
			}
			
			_pages = null;
			_dragPositions = null;
			_pips = null;
			
			_nextArrow = null;
			_prevArrow = null;
			
			OnHandleTap = null;
			OnPageSetChanged = null;
		}
	}
}
