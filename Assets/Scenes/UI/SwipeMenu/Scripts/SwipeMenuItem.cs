﻿using UnityEngine;
using System.Collections;

namespace ScaryBeasties.UI.SwipeMenu
{
	public class SwipeMenuItem : MonoBehaviour {
		
		public string id = "";
		public int index = 0;
		public bool enabled = true;

		
		// Use this for initialization
		void Start () {
			
		}


		public void SetItemContent(GameObject tobj){

			tobj.transform.parent = gameObject.transform.Find ("Content").gameObject.transform;

		}
		
		public void SetItemColour(Color col){

			tk2dBaseSprite tspr = gameObject.transform.Find ("Colour").gameObject.GetComponent<tk2dBaseSprite> ();
			tspr.color = col;
		}
		
		// Update is called once per frame
		void Update () {
			
		}

		protected virtual void OnDestroy()
		{

		}
	}
}
