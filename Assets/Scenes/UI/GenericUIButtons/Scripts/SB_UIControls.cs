using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ScaryBeasties.Controls;
using ScaryBeasties.Base;
using ScaryBeasties.Sound;
using ScaryBeasties.History;
using ScaryBeasties.Utils;
using UnityEngine.UI;

namespace ScaryBeasties.UI
{
	public class SB_UIControls : SB_BaseView 
	{
		public const string BTN_SOUND = "SB_SoundToggleControls";
		public const string BTN_PARENTS = "ui_parents_area_btn";
		public const string BTN_PAUSE = "ui_pause_btn";
		public const string BTN_BACK = "ui_back_btn";
		public const string BTN_MENU = "ui_menu_btn";
		public const string BTN_SOUND_TOGGLE = "ui_btn_sound_toggle";

		// Game objects
		protected GameObject _parentsAreaBtnGameObject;
		protected GameObject _pauseBtnGameObject;
		protected GameObject _backBtnGameObject;
		protected GameObject _menuBtnGameObject;
		protected GameObject _soundToggleBtnGameObject;
		
		public delegate void UIControlDelegate(SB_Button btn, string eventType);
		public event UIControlDelegate OnUIControl;

		private SB_BaseScene _currentScene = null;

		private bool _initDone = false;
		private bool _btnsEnabled = false;
		private bool _mouseDown = false;

		private bool _backEnabled = true;

		// A string of comma separated names of buttons, which this UIControls class should ignore
		//public string ignore = "";

		// Name of the prefab in the Resources folder.
		public const string resourceName = "Prefabs/SB_UIControls_Prefab";
		
		private static SB_UIControls _instance = null;
		public static SB_UIControls instance
		{
			get 
			{
				if(_instance == null)
				{
					// Create a new instance of SB_AudioChannels_Prefab, if one doesn't already exist in the scene
					var singletonsObjectPrefab = (SB_UIControls)Resources.Load(resourceName, typeof(SB_UIControls));
					
					// instantiate it
					_instance = (SB_UIControls)Instantiate(singletonsObjectPrefab);
				}
				
				return _instance;
			}
		}

		public SB_BaseScene currentScene
		{
			get {return _currentScene;}
			set {_currentScene = value;}
		}

		/** 
		 * Awake gets called before Start, so it's better to
		 * grab the game objects within this function..
		 */
		void Awake() 
		{
			DontDestroyOnLoad(transform.gameObject);

			if(!_initDone)
			{
				_parentsAreaBtnGameObject 		= transform.Find(BTN_PARENTS).gameObject;
				_pauseBtnGameObject 			= transform.Find(BTN_PAUSE).gameObject;
				_backBtnGameObject 				= transform.Find(BTN_BACK).gameObject;
				_menuBtnGameObject 				= transform.Find(BTN_MENU).gameObject;
				_soundToggleBtnGameObject 		= transform.Find(BTN_SOUND_TOGGLE).gameObject;
				
				UpdateSoundButtons();

				GameObject devModeIndicator = transform.Find("DevModeIndicator").gameObject;
				devModeIndicator.SetActive(false);
				if(SB_Globals.DEVELOPMENT_MODE && SB_Globals.DEVELOPMENT_MODE_INDICATOR)
				{
					devModeIndicator.SetActive(true);

					Text devModeTxt = devModeIndicator.transform.Find("Text").GetComponent<Text>();		
					devModeTxt.text += " (" + tk2dSystem.CurrentPlatform + ")";
				}
			}
			
			_initDone = true;
		}

		public ArrayList GetAllButtons()
		{
			ArrayList arr = new ArrayList();

			arr.Add(_parentsAreaBtnGameObject);
			arr.Add(_pauseBtnGameObject);
			arr.Add(_backBtnGameObject);
			arr.Add(_menuBtnGameObject);
			arr.Add(_soundToggleBtnGameObject);

			return arr;
		}

		override protected void Start()
		{
			// Create a new instance, if the GameObject is not already in the game
			if(_instance == null)
			{
				_instance = this;
			}

			base.Start();
		}

		virtual public void Update() 
		{
			// Don't handle the hardware back button, if user is pressing the screen in game
			if(UnityEngine.Input.GetMouseButtonDown(0))
			{
				_mouseDown = true;
			}
			else if(UnityEngine.Input.GetMouseButtonUp(0))
			{
				_mouseDown = false;
			}

			if(_mouseDown) return;

			if(UnityEngine.Input.GetKeyUp( KeyCode.Escape ) )
			{
				warn ("ESCAPE RELEASED!! SB_Globals.PLATFORM_ID: " + SB_Globals.PLATFORM_ID + ", Application.platform: " + Application.platform.ToString());
			}

			if( UnityEngine.Input.GetKeyUp( KeyCode.Escape ) && SB_Globals.PLATFORM_ID != SB_BuildPlatforms.Amazon ) 
			{
				HandleBackButton();
			}
		}

		/**
		 * Returns true, if user is currently pressing any of the UI buttons
		 */
		public bool IsTouchingUIButton(bool forceScriptedInteraction=false)
		{
			if(!_btnsEnabled) return false;

				// Ignore any presses with a certain radius of the 'pause' button
				Vector3 _mousePos = Camera.main.ScreenToWorldPoint (UnityEngine.Input.mousePosition);

				ArrayList btns = GetAllButtons();
				foreach(GameObject btn in btns)
				{
					if(btn.activeSelf)
					{
						float dist = SB_Utils.GetDistance(_mousePos, btn.transform.position);
						
						if(dist < 25f)
						{
							if(forceScriptedInteraction)
							{
								if(UnityEngine.Input.GetMouseButtonDown(0))
								{
									btn.GetComponent<SB_Button>().SetMouseDown();
								}
								else if(UnityEngine.Input.GetMouseButtonUp(0))
								{
									btn.GetComponent<SB_Button>().SetMouseUp();
								}
							}

							//warn ("Is touching btn " + btn.name);
							return true;
						}
					}
				}
			
			return false;
		}


		//TODO: Put a flag in to stop this from firing multiple times. CT: 03/03/2015
		virtual protected void HandleBackButton() 
		{
			warn ("HandleBackButton() _btnsEnabled: " + _btnsEnabled + ", _currentScene: " + _currentScene.name);

			if( Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WindowsEditor ) 
			{
				// Do nothing, if the UI controls are disabled
				//if(!_btnsEnabled) return;

				if(_currentScene != null && _currentScene.CurrentPopup != null)
				{
					warn("*** Close popup ***");
					_currentScene.CurrentPopup.Close();
				}
				else if( Application.loadedLevelName == SB_Globals.SCENE_TITLE)
				{
					warn("*** Application.Quit() ***");
					//_currentScene.LoadScene(SB_Globals.SCENE_PROFILE_SELECTION,null,true);
					Application.Quit();
				}
				else if(_btnsEnabled)
				{
					if( _pauseBtnGameObject.activeSelf )
					{
						warn("*** Show pause popup ***");
						if(_currentScene.State() != SB_BaseScene.STATE_PAUSED)
						{
							_currentScene.ShowPausePopup();
						}
					}
					else if( _backBtnGameObject.activeSelf )
					{
						warn("*** Load previous scene ***");
						_currentScene.LoadScene(SB_SceneHistory.instance.GetPreviousItem(true),null,true);
					}
				}
			}
		}

		public void OverrideBackButton(bool val=true){
			_backEnabled = !val;
		}

		/** Called by Intro animation, on its final frame */
		override public void IntroComplete()
		{
			UpdateSoundButtons();
			base.IntroComplete();
		}

		override public void ButtonsEnabled(bool val)
		{
			_btnsEnabled = val;
			base.ButtonsEnabled(val);
		}

		/** Hide all buttons */
		public void HideButtons()
		{
			_soundToggleBtnGameObject.SetActive(false);
			_parentsAreaBtnGameObject.SetActive(false);
			_pauseBtnGameObject.SetActive(false);
			_backBtnGameObject.SetActive(false);
			_menuBtnGameObject.SetActive(false);
		}

		public void ShowButtons(List<string> buttonsList)
		{
			HideButtons();
			// Show the buttons which are in the list
			foreach(string name in buttonsList)
			{
				switch(name)
				{
					case BTN_SOUND: 	_soundToggleBtnGameObject.SetActive(true); break;
					case BTN_PARENTS:	_parentsAreaBtnGameObject.SetActive(true); break;
					case BTN_PAUSE: 	_pauseBtnGameObject.SetActive(true); break;
					case BTN_BACK: 		_backBtnGameObject.SetActive(true); break;
					case BTN_MENU: 		_menuBtnGameObject.SetActive(true); break;
				}
			}

			CollidersEnabled(true);
		}

		public GameObject GetButton(string name)
		{
			switch(name)
			{
				case BTN_SOUND: 	return _soundToggleBtnGameObject; break;
				case BTN_PARENTS:	return _parentsAreaBtnGameObject; break;
				case BTN_PAUSE: 	return _pauseBtnGameObject; break;
				case BTN_BACK: 		return _backBtnGameObject; break;
				case BTN_MENU: 		return _menuBtnGameObject; break;
			}

			return null;
		}

		/**
		 * Enables/disables the box colliders on the UI buttons
		 */ 
		public void CollidersEnabled(bool val)
		{
			_parentsAreaBtnGameObject.transform.GetComponent<BoxCollider2D>().enabled = val;
			_pauseBtnGameObject.transform.GetComponent<BoxCollider2D>().enabled = val;
			_backBtnGameObject.transform.GetComponent<BoxCollider2D>().enabled = val;
			_menuBtnGameObject.transform.GetComponent<BoxCollider2D>().enabled = val;
			_soundToggleBtnGameObject.transform.GetComponent<BoxCollider2D>().enabled = val;
		}

		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			if(eventType == SB_Button.MOUSE_UP)
			{
				//log(" **  OnBtnHandler " + btn.name);

				switch(btn.name)
				{
					case BTN_BACK:
							if(_backEnabled){
									if(_currentScene != null)
									{
										ButtonsEnabled(false);
										_currentScene.PlayOutroAndLoad(SB_SceneHistory.instance.GetPreviousItem(true));
									}
							}
									PlayButtonPressSound();
									break;

					case BTN_SOUND_TOGGLE:
									ToggleSound(btn, eventType);
									break;

				}
			}

			if(OnUIControl != null) OnUIControl(btn, eventType);
		}

		/**
		 * Called whenever one of the buttons has been pressed.
		 * This will toggle the muted state of the SoundManager
		 */
		void ToggleSound (SB_Button btn, string eventType)
		{
			SB_SoundManager.instance.muted = !SB_SoundManager.instance.muted;
			UpdateSoundButtons();
		}
		
		/**
		 * Update the button states, so that one is visible and the other invisible.
		 */
		public void UpdateSoundButtons()
		{
			// GT - really horrible hack....
			tk2dSprite soundButtonSprite = _soundToggleBtnGameObject.transform.Find("Sprite").gameObject.GetComponent<tk2dSprite>();
			if(SB_SoundManager.instance.muted)
			{
				soundButtonSprite.SetSprite("small_button_soundoff");
			}
			else
			{
				soundButtonSprite.SetSprite("small_button_sound");
			}


			print ("[SB_SoundToggleControls] UpdateButtons() muted: " + SB_SoundManager.instance.muted);
		}

		override protected void OnDestroy()
		{
			_parentsAreaBtnGameObject = null;
			_pauseBtnGameObject = null;
			_backBtnGameObject = null;
			_menuBtnGameObject = null;

			_currentScene = null;
			OnUIControl = null;

			base.OnDestroy();
		}
	}
}