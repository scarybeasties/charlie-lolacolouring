﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Base;
using ScaryBeasties.Controls;
using System.Collections.Generic;
using ScaryBeasties.Utils;
using ScaryBeasties.Loaders;
using ScaryBeasties;
using ScaryBeasties.Data;
using ScaryBeasties.Games.Colouring.Menus;

public class BottomMenu : SB_BaseView 
{
	public const string BTN_PACKS = "ui_packs_btn";
	public const string BTN_SHOP = "ui_shop_btn";
	public const string BTN_MY_CREATIONS = "ui_creations_btn";
	public const string BTN_INSPIRATION = "ui_inspiration_btn";
	
	public static string SELECTED_BTN = "";
	
	private SB_SceneLoader _loader;
	
	void Awake()
	{
		FindButtons();
		
		if(SELECTED_BTN != "")
		{
			SetButtonSelected(SELECTED_BTN);
		}
	}
	
	override protected void Init()
	{
		_loader = new SB_SceneLoader();
		
		IntroComplete();
		AddDelegates();
		ButtonsEnabled(true);
	}
	
	private void SetButtonSelected(string btnName)
	{
		for(int n=0; n<_buttons.Count; n++)
		{
			SB_Button btn = (SB_Button)_buttons[n];
			if(btnName == btn.name)
			{
				btn.Selected = true;
				
				//tk2dSprite btnSprite = btn.transform.Find("Sprite").GetComponent<tk2dSprite>();				
				//btnSprite.SetSprite(btnSprite.CurrentSprite.name.Replace("_off", "_on"));
				
				GameObject btnBg = btn.transform.Find("background").gameObject;
				btnBg.SetActive(true);
				break;
			}
		}
	}
	
	override protected void OnBtnHandler(SB_Button btn, string eventType)
	{
		base.OnBtnHandler(btn, eventType);
		
		if(eventType == SB_Button.MOUSE_UP)
		{
			switch(btn.name)
			{
				case BTN_SHOP:
				break;
				
				case BTN_PACKS:
				//StartCoroutine(WaitThenLoadScene(SB_Globals.SCENE_COLOURING_MENU_PACKS)); // <-- Load this scene if we have multiple packs to choose from
				
				// Hey Duggee Colouring app only has the default pack, so we can go straight to the 'drawings' menu, to list its contents
				KeyValueObject kv = new KeyValueObject();
				kv.SetKeyValue(ColouringMenuBase.MENU_ITEMS_PACK_ID_KEY, "default_pack");
				StartCoroutine(WaitThenLoadScene(SB_Globals.SCENE_COLOURING_MENU_DRAWINGS, kv));
				break;
				
				case BTN_MY_CREATIONS:
				StartCoroutine(WaitThenLoadScene(SB_Globals.SCENE_COLOURING_MENU_MY_CREATIONS));
				break;
				
			case BTN_INSPIRATION:
				StartCoroutine(WaitThenLoadScene(SB_Globals.SCENE_COLOURING_MENU_INSPIRATION));
				break;
			}
			
			ButtonsEnabled(false);
		}
	}
	
	IEnumerator WaitThenLoadScene(string sceneName, KeyValueObject kv=null)
	{
		yield return new WaitForSeconds(0.1f);
		_loader.loadLevel(sceneName, kv);
	}
	
	override protected void OnDestroy()
	{
		_loader = null;
		base.OnDestroy ();
	}
}
