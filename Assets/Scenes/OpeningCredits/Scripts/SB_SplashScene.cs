﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Base;
using System.Collections.Generic;
using ScaryBeasties.Controls;
using ScaryBeasties.Data;

namespace ScaryBeasties.Scenes
{
	public class SB_SplashScene : SB_BaseScene 
	{
		int _currentSlide = 0;

		// Use this for initialization
		override protected void Start () 
		{
			// Define the animation states (clips) which exist in this scene
			_animationStates = new List<string>(new string[] {"Intro", "Outro", "Slide1", "Slide2"});

			base.Start ();
			//_animationController.PlayAnimation("Intro");
			_animationController.PlayAnimation("Slide1");
		}

		override protected void Update() 
		{
			base.Update();

			if(UnityEngine.Input.GetMouseButtonUp(0))
			{
				ShowNextSlide();
			}
		}

		public void ShowNextSlide()
		{
			_currentSlide++;
			if(_currentSlide > 3)
			{
				// Set the next scene to load, once the splash animation has played through
				//_nextSceneToLoad = SB_Globals.SCENE_INTRO;

				//_nextSceneToLoad = SB_Globals.SCENE_MAIN_MENU;
				
				// There's no Outro animation in the Splash, 
				// so just directly call OutroComplete, to trigger the loading of the next scene.
				//OutroComplete();
				KeyValueObject kvObject = new KeyValueObject();
				kvObject.SetKeyValue("first", true);
				LoadScene(SB_Globals.SCENE_TITLE,kvObject,true);
			}
			else
			{
				_animationController.PlayAnimation("Slide" + _currentSlide);
			}
		}
	}
}