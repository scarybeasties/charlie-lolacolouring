﻿using UnityEngine;
using System.Collections;

namespace ScaryBeasties.Games.Colouring.Menus
{
	public class ColouringMenuItemProperties
	{
		public string creationId = "";
		public string creationFolder = "";
		public string id = "";
		public string title = "";
		public string imgPath = "";
		public Color colour;
		public System.Object data=null;
		
		public bool itemUnlocked=false;
		public bool isSBDrawing=false; // Is this an image created by Scary Beasties? If set to true, image data will be duplicated, so user can edit it themseleves
		
		public string ToString()
		{
			string str = "creationId: " + creationId;
			str += ", creationFolder: " + creationFolder;
			str += ", id: " + id;
			str += ", title: " + title;
			str += ", imgPath: " + imgPath;
			str += ", colour: " + colour;
			str += ", isSBDrawing: " + isSBDrawing;
			
			return str;
		}
	}
}