﻿
using ScaryBeasties.Scenes;
using System.Collections;
using ScaryBeasties.UI.ScrollableMenu;
using ScaryBeasties.Popups;
using UnityEngine;
using ScaryBeasties.Base;
using ScaryBeasties.UI;
using System.Collections.Generic;
using ScaryBeasties.Utils;
using UnityEngine.UI;
using ScaryBeasties.Data;
using ScaryBeasties.InAppPurchasing.Shop;
using System.IO;
using ScaryBeasties.Controls;
using ScaryBeasties.Colouring;
using ScaryBeasties.Sound;
using ScaryBeasties.Network;

namespace ScaryBeasties.Games.Colouring.Menus
{
	//public class ColouringMenuBase : SB_GameScene 
	public class ColouringMenuBase : SB_BaseScene
	{
		public const string MENU_ITEMS_TITLE_KEY = "MenuItemsTitle";
		public const string MENU_ITEMS_KEY = "MenuItems";
		public const string MENU_ITEMS_UNLOCKED_KEY = "MenuItemsUnlocked";
		public const string MENU_ITEMS_PACK_ID_KEY = "MenuItemsPackId";
		public const string MENU_ITEMS_CENTER_ON_ITEM_KEY = "MenuItemsCenterOnItem";
		
		protected SB_ScrollableMenu _menu;
		protected ColouringMenuItem _selectedMenuItem = null;
		
		protected ColouringPackAsset _selectedPackAsset;
		
		protected BottomMenu _bottomMenu;
		protected Text _messageText;
		
		protected bool _isEditingInspirationImage = false; // This is set to true, if user is editing a premade inspiration image
		
		float _idleCounter = 0.0f;
		float _idleMax = 12.0f;
		int _lastIdlePlayed = 0;
		
		override protected void Awake ()
		{
			base.Awake ();
			_bottomMenu = transform.Find ("BottomMenu").GetComponent<BottomMenu> ();
		}
		
		override protected void Start ()
		{
			base.Start ();
			
			_uiControls.ShowButtons (new List<string> (new string[] { SB_UIControls.BTN_BACK, SB_UIControls.BTN_SOUND, SB_UIControls.BTN_PARENTS}));
			_menu = transform.Find ("Canvas/Menu").GetComponent<SB_ScrollableMenu> ();
			//_menu.OnReleaseCallback = HandleOnMenuItemReleased;	
			//_menu.OnItemRemovedCallback = HandleOnMenuItemRemoved;	
			
			_messageText = transform.Find ("Canvas/Message").GetComponent<Text> ();
			ShowMessage (SB_Utils.GetNodeValueFromConfigXML ("config/copy/menus/text", "loading"));
			
			//SB_SoundManager.instance.PlaySound ("Audio/SFX/UI/SndPopup1", 1f, SB_Globals.MAIN_BUTTON_SOUND_CHANNEL);
		}
		
		override protected void Init ()
		{
			Text title = transform.Find ("title/label").GetComponent<Text> ();
			string titleTxt = SB_Utils.GetNodeValueFromConfigXML ("config/copy/colouring/text", "title");
			//title.text = SB_Utils.SanitizeString (titleTxt);
			title.text = ""; // No title, for Hey Duggee Colouring menu scenes
			
			//warn ("------------------------------");
			//warn ("_menu.Height: " + _menu.Height + ", Screen.height: " + Screen.height + ", SB_Utils.ZoomPoint(Screen.height): " + SB_Utils.ZoomPoint (Screen.height));
			//warn ("------------------------------");
		}
		
		protected virtual ColouringMenuItem CreateMenuItem (ColouringMenuItemProperties props, bool isShopImage=false)
		{
			GameObject btnTemplate = transform.Find ("Canvas/ScrollableMenuItem").gameObject;
			GameObject newBtn = (GameObject)Instantiate (btnTemplate);
			ColouringMenuItem newBtnItem = newBtn.transform.GetComponent<ColouringMenuItem> ();
			
			// Don't show the button yet - Wait until all menu items have been created!
			//newBtnItem.gameObject.SetActive(false);
			
			newBtnItem.Init (props);
			_menu.AddItem (newBtnItem);
			
			// Make sure we are at the top of the list
			_menu.SetContentsNormalisedPosition (SB_ScrollableMenu.MENU_POSITION_TOP);
			
			return newBtnItem;
		}
		
		/**
		 * Returns the full local or remote image path for the menu item.
		 * Check if the local image exists on disk.
		 * If not, return remote path, and load image using www
		 */
		protected string GetMenuItemImagePath (string imgPath)
		{
			string localImagePath = SB_ShopUtils.GetLocalShopDirectoryPath () + "/" + imgPath;
			string remoteImagePath = "";
			
			// Is the device connected to the internet?
			if (Application.internetReachability != NetworkReachability.NotReachable) 
			{
				remoteImagePath = SB_Globals.SHOP_ASSETS_PATH + SB_Globals.LOCALE_STRING + "/" + SB_IAPManager.instance.ShopConfig.ShopImagePath + "/" + tk2dSystem.CurrentPlatform + "/" + imgPath;
			}
			
			//warn ("---------> GetMenuItemImagePath: " + imgPath);
			//warn ("-> localImagePath: " + localImagePath);
			//warn ("-> remoteImagePath: " + remoteImagePath);
			
			if (File.Exists (localImagePath)) {
				return localImagePath;
			}
			
			return remoteImagePath;
		}
		
		protected virtual void DoMenuScroll ()
		{
			HideMessage ();
			
			// Scroll the list, from bottom to top
			_menu.ScrollToNormalisedPosition (SB_ScrollableMenu.MENU_POSITION_BOTTOM, SB_ScrollableMenu.MENU_POSITION_TOP);
		}
		
		protected IEnumerator WaitThenLoadMenuThumbs(int startThumbIndex=0, int numThumbsToLoad=3, float vertNormalisedPos=SB_ScrollableMenu.MENU_POSITION_TOP, float horizNormalisedPos=SB_ScrollableMenu.MENU_POSITION_LEFT)
		{
			yield return new WaitForSeconds (0.01f);
			HideMessage ();
			_menu.ScrollRect.verticalNormalizedPosition = vertNormalisedPos;
			_menu.ScrollRect.horizontalNormalizedPosition = horizNormalisedPos;
			
			yield return new WaitForSeconds (0.1f);
			
			_menu.OnItemThumbsLoaded = HandleOnItemThumbsLoaded;
			//_menu.LoadThumbs(0, numThumbsToLoad); // Load the first 3 thumbnails
			_menu.LoadThumbs(startThumbIndex, numThumbsToLoad);
			//_menu.LoadThumbs(); // Load all thumbs
			
			// NOTE: Wait until 'OnMenuReady' is called, before setting the other delegates

			StopCoroutine (WaitThenLoadMenuThumbs ());
		}

		protected virtual void HandleOnMenuStartDrag()
		{
		
		}
		
		protected virtual void HandleOnMenuItemReleased (SB_ScrollableMenuItem menuItem)
		{
			if (_popupManager.PopupIsShowing) {
				//warn ("HandleOnMenuItemReleased - POPUP IS ALREADY SHOWING!");
				return;
			}
			
			_selectedMenuItem = (ColouringMenuItem)menuItem;
			//warn ("HandleOnMenuItemReleased - menuItem " + menuItem.id);
			
			PlaySelectedItemVO();
			PlayButtonPressSound();
			
			if (_selectedMenuItem.Props.itemUnlocked) {
				if (menuItem.data != null) {
					_selectedPackAsset = (ColouringPackAsset)menuItem.data;
					
					if (_selectedMenuItem.Props.isSBDrawing) {
						// The user selected a pre-made ScaryBeasties drawing, from the Inspirations menu.
						// Load the colouring game, and create a duplicate image.
						
						// Create a new ID for this duplicated drawing
						_selectedMenuItem.Props.creationId = DrawingManager.instance.GenerateUniqueName ();
						
						if(MaxNumberOfCreationsReached())
						{
							ShowMaxCreationsAlertPopup();
						}
						else
						{
							LoadColouringGame ();
						}
					} 
					else 
					{
						FileInfo drawingXMLFile = DrawingManager.instance.GetDrawingXML (_selectedMenuItem.Props.creationFolder);
						if (drawingXMLFile != null) {
							// User selected an item which has already been worked on, show the 'continue' popup
							_menu.Disable ();
							
							// Show popup
							ShowContinueDrawingPopup();
						} 
						else 
						{
							if(MaxNumberOfCreationsReached())
							{
								ShowMaxCreationsAlertPopup();
							}
							else
							{
								// User chose an item which has not yet been worked on
								LoadColouringGame ();
							}
						}
					}
				}
			} else {
				error ("ITEM IS LOCKED!! Take user to the shop??");
			}
		}
		
		protected virtual void OnMenuReady()
		{
			//log("---- OnMenuReady");
			_menu.OnStartDragCallback = HandleOnMenuStartDrag;	
			_menu.OnReleaseCallback = HandleOnMenuItemReleased;	
			_menu.OnItemRemovedCallback = HandleOnMenuItemRemoved;
		}

		protected virtual void HandleOnItemThumbsLoaded()
		{
			//log("---- HandleOnItemThumbsLoaded");
		}
		
		protected virtual void ShowMaxCreationsAlertPopup()
		{
			string title = SB_Utils.GetNodeValueFromConfigXML("config/copy/max_drawings_alert_popup/text", "title");
			string body = SB_Utils.GetNodeValueFromConfigXML("config/copy/max_drawings_alert_popup/text", "body");
			title = SB_Utils.SanitizeString(title);
			body = SB_Utils.SanitizeString(body);
			
			SB_AlertPopupVO popupVO = new SB_AlertPopupVO(SB_PopupTypes.ALERT_POPUP, SB_PopupManager.PREFAB_ALERT_POPUP, title, body, false);
			ShowPopup(SB_PopupTypes.ALERT_POPUP, popupVO);
			
			_menu.Disable();
		}
		
		protected virtual void ShowContinueDrawingPopup()
		{	
			KeyValueObject data=null;
			
			// Disable the 'duplicate drawing' button, if user has reached max number of creations
			if(MaxNumberOfCreationsReached())
			{
				data = new KeyValueObject();
				ArrayList excludeOptions = new ArrayList();
				excludeOptions.Add(SB_ContinueDrawingPopup.OPTION_DUPLICATE_DRAWING);
				data.SetKeyValue("ExcludeOptions", excludeOptions);
			}
			
			SB_PopupVO popupVO = new SB_PopupVO (SB_PopupTypes.CONTINUE_DRAWING, SB_PopupManager.PREFAB_CONTINUE_DRAWING_POPUP, "", "", data);
			ShowPopup (SB_PopupTypes.CONTINUE_DRAWING, popupVO);
		}
		
		bool MaxNumberOfCreationsReached()
		{
			return DrawingManager.instance.GetNumberOfUserDrawings() >= SB_Globals.MAX_NUMBER_USER_DRAWINGS;
		}
		
		/**
		 * Callback from scrollmenu, whenever a menu item is removed
		 */
		protected virtual void HandleOnMenuItemRemoved (int numItems)
		{
			//warn ("HandleOnMenuItemRemoved() numItems: " + numItems);
		}
		
		protected virtual void LoadColouringGame (bool resetDrawing=false, bool createDuplicateDrawing=false)
		{
			KeyValueObject kv = new KeyValueObject ();
			
			//warn ("--------------------------------");
			//warn ("LoadColouringGame");
			//Debug.LogWarning ("	\t_selectedPackAsset.id: " + _selectedPackAsset.id);
			//Debug.LogWarning ("	\t_selectedPackAsset.assetName: " + _selectedPackAsset.assetName);
			//Debug.LogWarning ("	\t_selectedMenuItem.Props: " + _selectedMenuItem.Props.ToString ());
			
			ColouringGameData colouringGameData = new ColouringGameData{
																			CreationID =_selectedMenuItem.Props.creationId,
																			CreationFolder = _selectedMenuItem.Props.creationFolder,
																			ImageID = _selectedPackAsset.id,
																			ImageFileName = _selectedPackAsset.assetName
																		};
			
			kv.SetKeyValue ("colouringGameData", colouringGameData);
			kv.SetKeyValue ("resetDrawing", resetDrawing);
			kv.SetKeyValue ("createDuplicateDrawing", createDuplicateDrawing);
			kv.SetKeyValue ("isEditingInspirationImage", _isEditingInspirationImage);
			kv.SetKeyValue (MENU_ITEMS_CENTER_ON_ITEM_KEY, _selectedMenuItem.index);
			
			warn ("	\tkv: " + kv.ToString ());
			warn ("--------------------------------");
			
			LoadScene (SB_Globals.SCENE_COLOURING_GAME, kv, true);
		}
		
		override protected void HandleOnPopupEvent (SB_BasePopup popup, string eventType)
		{
			base.HandleOnPopupEvent (popup, eventType);
			
			Dictionary<string, string> trackingParams =	new Dictionary<string, string>();
			//log ("HandleOnPopupEvent " + eventType);
			switch (eventType) 
			{
				case SB_ContinueDrawingPopup.OPTION_CONTINUE_DRAWING: 
					trackingParams.Add("Option", "ContinueDrawing");
					LoadColouringGame ();
					break;
						
				case SB_ContinueDrawingPopup.OPTION_RESET_DRAWING: 
					trackingParams.Add("Option", "ResetDrawing");
					LoadColouringGame (true);
					break;
						
				case SB_ContinueDrawingPopup.OPTION_DUPLICATE_DRAWING: 
					trackingParams.Add("Option", "DuplicateDrawing");
					LoadColouringGame (false, true);
					break;
						
				case SB_ContinueDrawingPopup.OPTION_DELETE_DRAWING:
					trackingParams.Add("Option", "DeleteDrawing");
					DeleteSelectedDrawing ();
					break;
					
				case SB_GatingPopup.GATING_SUCCESS:
					LoadParentsArea();
					break;
			}
			
			if(trackingParams.ContainsKey("Option"))
			{
				LogSceneEvent("ContinueDrawingPopup", trackingParams);
			}
		}

		override protected void OnBtnHandler (SB_Button btn, string eventType)
		{
			base.OnBtnHandler (btn, eventType);			
			resetIdle ();
			
			if (eventType == SB_Button.MOUSE_UP) {
				//LogButtonEvent(btn.name);
				PlayButtonPressSound();
				if (btn.name == "ui_parents_area_btn") 
				{
					_menu.Disable();
					ShowPopup(SB_PopupTypes.GATING_POPUP);
				}
			}
		}
		
		protected virtual void LoadParentsArea()
		{
			LogSceneEvent("Parents");
			LoadScene(SB_Globals.SCENE_PARENTS_AREA, null, true);
		}
		
		/**
		 * Delete the drawing and XML from local filesystem.
		 * Re-create the menu.
		 */
		protected virtual void DeleteSelectedDrawing ()
		{
			DeleteDrawingFiles ();
			
			_menu.RemoveItem (_selectedMenuItem);
		}

		protected void DeleteDrawingFiles ()
		{
			string path = _selectedMenuItem.Props.creationFolder;
			log ("DeleteSelectedDrawing() folder: " + path);
			
			if (Directory.Exists (path)) {
				Directory.Delete (path, true);
			}
		}
		
		override protected void PopupClosed ()
		{
			base.PopupClosed ();
			_menu.Enable ();
		}
		
		protected void ShowMessage (string txt)
		{
			/*
			if (_messageText != null) {
				_messageText.text = txt;
				_messageText.gameObject.SetActive (true);
			}
			*/
			
			transform.Find ("Loader").gameObject.SetActive (true);
		}
		
		protected void HideMessage ()
		{
			/*
			if (_messageText != null) {
				_messageText.gameObject.SetActive (false);
			}
			*/
			
			Destroy(transform.Find ("Loader").gameObject);
			Destroy(_messageText.gameObject);
		}
		
		override protected void UpdateReady ()
		{
			_idleCounter -= Time.deltaTime;
			if (_idleCounter <= 0) {
				resetIdle ();
				PlayIdleVO ();
			}
			
			base.UpdateReady ();
		}
		
		private void resetIdle ()
		{
			_idleCounter = _idleMax;
		}
		
		#region AUDIO		
		protected virtual void PlayIdleVO()
		{
			if(_lastIdlePlayed == 0)  PlayTapPicture();
			else PlayChoosePicture();
		}
		
		protected void PlayChoosePicture()
		{
			_lastIdlePlayed = 0;
			SB_SoundManager.instance.PlayVoice("Localised/VO/Narrator/Menu/vo_profile_choosepicture_2");
		}
		
		protected void PlayTapPicture()
		{
			_lastIdlePlayed = 1;
			SB_SoundManager.instance.PlayVoice("Localised/VO/Narrator/Menu/vo_profile_tapyourpicture_1");
		}
		
		protected virtual void PlaySelectedItemVO()
		{
		
		}
		
		#endregion
		
		override protected void OnDestroy ()
		{
			_menu.OnStartDragCallback = null;	
			_menu.OnReleaseCallback = null;	
			_menu.OnItemRemovedCallback = null;	
			_menu.OnItemThumbsLoaded = null;	
			_menu = null;
			
			_selectedMenuItem = null;
			
			base.OnDestroy ();
		}
	}
}