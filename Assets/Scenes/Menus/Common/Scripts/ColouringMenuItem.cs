﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.UI.ScrollableMenu;
using ScaryBeasties.SB2D;
using System.IO;
using ScaryBeasties.InAppPurchasing.Shop;
using UnityEngine.UI;
using ScaryBeasties.Utils;

namespace ScaryBeasties.Games.Colouring.Menus
{
    [RequireComponent(typeof(Image))]
    [RequireComponent(typeof(LayoutElement))]
    public class ColouringMenuItem : SB_ScrollableMenuItem
    {
        private ColouringMenuItemProperties _props = null;

        private bool _loadingImageFromServer = true;
        private Color _colour;

        private Image _background;
        private Image _padlock;
        private Image _textBackground;
        private Image _maskedContent;
        private GameObject _shadow;
        private Text _titleText;

        private bool _ready = false;
        private bool _isLocked = false;

        private SB2DSprite _spr;

        public ColouringMenuItemProperties Props { get { return _props; } }

        override protected void Start()
        {
            base.Start();

            _background = transform.GetComponent<Image>();
            _padlock = transform.Find("Padlock").GetComponent<Image>();
            _textBackground = transform.Find("TextBackground").GetComponent<Image>();
            _titleText = transform.Find("Text/Title").GetComponent<Text>();
            _maskedContent = transform.Find("MaskedContentImage").GetComponent<Image>();
            _shadow = transform.Find("Shadow").gameObject;

            transform.GetComponent<Image>().enabled = false;
            _maskedContent.gameObject.SetActive(false);
            _shadow.SetActive(false);
            _ready = true;

            if (_props != null)
            {
                Init(_props);
            }
        }

        public void Init(ColouringMenuItemProperties props)
        {
            _props = props;

            // Stop here if this Object has not yet been fully constructed.
            // Once Start is called, this Init function will be called again
            if (!_ready) return;

            id = _props.id;
            data = _props.data;

            SetColour(_props.colour);
            SetText(_props.title);

            if (_props.title.Length > 0) SetTextVisible(true);
            else SetTextVisible(false);

            //if(_props.imgPath.Length > 0) LoadImage(_props.imgPath);
            imgPath = _props.imgPath;

            ShowPadlock(_isLocked);
        }

        public void SetColour(Color colour)
        {
            _colour = colour;

            _background.color = colour;
            _textBackground.color = colour;
        }

        public void SetText(string txt)
        {
            _titleText.text = txt;
        }

        public string GetText()
        {
            if (_titleText != null)
            {
                return _titleText.text;
            }

            return "";
        }

        public void SetTextVisible(bool val)
        {
            _titleText.gameObject.SetActive(val);
            _textBackground.gameObject.SetActive(val);
        }

        public void ShowPadlock(bool val)
        {
            _isLocked = val;
            if (_padlock != null)
            {
                _padlock.gameObject.SetActive(_isLocked);
            }
        }

        /**
		 * Load the menu item image
		 */
        override public void LoadImage()
        {
            _maskedContent.gameObject.SetActive(true);
            ImageIsLoaded = true;
            //_maskedContent.gameObject.SetActive(true);
            //_shadow.SetActive(true);
            string path = imgPath;
            if (_spr == null)
            {
                _spr = SB2DSpriteMaker.CreateScaledSprite(transform.Find("Content").gameObject, "SB2DSprite", OnSpriteLoaded);
            }
            transform.Find("Content").gameObject.SetActive(true);

            if (SB_IAPManager.instance.ShopConfig == null)
            {
                Debug.LogError("ERROR!! - SB_IAPManager.instance.ShopConfig is null!!!");
            }

            string url = "";
            //string localImage = SB_ShopUtils.GetLocalShopDirectoryPath() + "/" + path; // TODO: Check that shop images load correctly from server and from local disk(19.04.2016)
            string localImage = path;

            Debug.LogError("---> remote image url: " + url);
            Debug.LogError("---> localImage path: " + localImage);
            if (File.Exists(localImage))
            {
                // Load image from local filesystem
                _loadingImageFromServer = false;
                url = localImage;
                //Debug.LogWarning("---> LOCAL LOADING IMAGE FROM: " + url);
                _spr.LoadReadableImageFromDisk(url);
            }
            else
            {
                url = SB_Globals.SHOP_ASSETS_PATH + SB_Globals.LOCALE_STRING + "/" + SB_IAPManager.instance.ShopConfig.ShopImagePath + "/" + tk2dSystem.CurrentPlatform + "/" + path;
                //Debug.LogWarning("---> WWW LOADING IMAGE FROM: " + url);
                _spr.LoadReadableImage(url);
            }

            GameObject itemGameObj = _spr.gameObject;
            itemGameObj.transform.parent = transform.Find("Content").gameObject.transform;

            SpriteRenderer trend = itemGameObj.GetComponent<SpriteRenderer>();
            trend.sortingOrder = 0;
            //Debug.LogWarning ("SORTING ORDER " + trend.sortingOrder);
            transform.Find("Content").gameObject.transform.localScale = new Vector2(1.0f, 1.0f);

            transform.GetComponent<Image>().enabled = true;

        }

        private void OnSpriteLoaded(SB2DSprite spr)
        {
            if (_loadingImageFromServer)
            {
                Debug.LogWarning("SPRITE LOADING CALLBACK");

                string remotePath = SB_Globals.SHOP_ASSETS_PATH + SB_Globals.LOCALE_STRING + "/" + SB_IAPManager.instance.ShopConfig.ShopImagePath + "/" + tk2dSystem.CurrentPlatform;
                string saveToLocalPath = spr.AssetFileName.Substring(remotePath.Length);
                saveToLocalPath = saveToLocalPath.Replace("/" + spr.GetFileName(), ""); // Remove the filename from the localPath, so we just have the folder structure

                SB_ShopUtils.SaveSpriteToDisk(spr, saveToLocalPath);
            }


            //------------------------------------------------------
            // Horrible hack to get masking of SB2D sprites working..
            // - Get the 'sprite' object, from the SB2D Sprite Renderer
            // - Assign the sprite to a Unity UI Image object (.. because UI Image objects can be masked)
            // - Hide the original SB2D gameobject.
            //------------------------------------------------------

            Sprite sprite = spr.GetComponent<SpriteRenderer>().sprite;
            _maskedContent.sprite = sprite;
            transform.Find("Content").gameObject.SetActive(false);

            _maskedContent.gameObject.SetActive(true);
            _shadow.SetActive(true);
            // Call parent class method, to dispatch event to listeners
            ImageLoaded();
        }

        override public void UnloadImage()
        {
            Destroy(_maskedContent.material.mainTexture);
            _maskedContent.sprite = null;

            //transform.GetComponent<Image>().enabled = false;
            //_maskedContent.GetComponent<Canvas>().enabled = false;
            _maskedContent.gameObject.SetActive(false);
            _shadow.SetActive(false);
            //_spr.DestroyImageLoader();
            _spr.GetComponent<SpriteRenderer>().sprite = null;
            Destroy(_spr.GetComponent<Renderer>().material.mainTexture);
            Destroy(_spr.gameObject);
            _spr = null;

            ImageIsLoaded = false;
        }

        override protected void OnDestroy()
        {
            _background = null;
            _padlock = null;
            _textBackground = null;
            _titleText = null;
            _spr = null;

            base.OnDestroy();
        }
    }
}