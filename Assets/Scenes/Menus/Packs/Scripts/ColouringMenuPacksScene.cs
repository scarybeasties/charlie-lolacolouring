﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ScaryBeasties.InAppPurchasing.Shop;
using ScaryBeasties.UI.ScrollableMenu;
using ScaryBeasties.Data;
using ScaryBeasties.InAppPurchasing.Local;
using UnityEngine.UI;
using ScaryBeasties.Utils;
using ScaryBeasties.History;
using ScaryBeasties.Base;
using ScaryBeasties.Popups;
using ScaryBeasties.Cam;
using ScaryBeasties.Games.Colouring.Menus;

namespace ScaryBeasties.Games.Colouring
{
	public class ColouringMenuPacksScene : ColouringMenuBase
	{
		private ArrayList _packContentToLoad = new ArrayList ();
		private string _currentPackId = "";
		
		override protected void Awake ()
		{
			base.Awake ();
			BottomMenu.SELECTED_BTN = BottomMenu.BTN_PACKS;
		}
		
		override protected void Start ()
		{
			base.Start ();
			SetupMenu ();
		}
		
		override protected void Init ()
		{
			Text title = transform.Find ("title/label").GetComponent<Text> ();
			string titleTxt = SB_Utils.GetNodeValueFromConfigXML ("config/copy/menus/packs/text", "title");
			//title.text = SB_Utils.SanitizeString (titleTxt);
			title.text = ""; // No title, for Hey Duggee Colouring menu scenes
		}
		
		private void SetupMenu ()
		{
			string packID = "";
			string imgPath = "";
			
			// Add packs which user already has installed on their device
			ArrayList localPackIDs = SB_LocalPackManager.instance.GetAllAvailablePackIDs ();
			
			warn ("--------------------SetupMenu() - localPackIDs.Count " + localPackIDs.Count);
			
			
			for (int n=0; n<localPackIDs.Count; n++) {
				packID = (string)localPackIDs [n];
				warn ("LOCAL PACK ID: " + packID);
				_packContentToLoad.Add (packID); 
				
				imgPath = GetMenuItemImagePath (packID + "/" + packID + ".png");
				
				ColouringPackContents packContents = ColouringPackManager.instance.GetPackContents (packID);
				
				// 'packContents' will be null, if testing this scene directly in the IDE, so we'll set some dummy values for now:
				Color colr = Color.blue;
				string ttl = packID + " (test)";
				
				// Horrible code, to allow us to test this scene standalone..
				// If there's previous scenes in the SB_SceneHistory, then we are 
				// most likely running from MainApp - so use the 'packContents' object
				if (SB_SceneHistory.instance.Length > 1) {
					colr = packContents.Colour;
					ttl = packContents.Title;
				}
				
				ColouringMenuItemProperties props = new ColouringMenuItemProperties{
																						id=packID,
																						colour=colr,
																						title=ttl,
																						imgPath=imgPath,
																						itemUnlocked=true
																					};
				
				CreateMenuItem (props, true);
			}
			
			// Add all remaining packs
			warn ("SB_PackManager.instance.PackItemCount(): " + SB_PackManager.instance.PackItemCount ());
			for (int i=0; i<SB_PackManager.instance.PackItemCount(); i++) {
				//SB_PackItem packItem = SB_PackManager.instance.GetPackItemAtIndex(i);
				ColouringPackItem packItem = new ColouringPackItem (SB_PackManager.instance.GetPackItemAtIndex (i).Xml);
				warn ("ColouringPackItem [" + i + "] = " + packItem.ToString ());
				
				if (!localPackIDs.Contains (packItem.packID)) {
					packID = packItem.packID;
					imgPath = packID + "/" + packID + ".png";
					
					ColouringMenuItemProperties props = new ColouringMenuItemProperties{
						id=packID,
						colour=packItem.colour,
						title=packItem.title,
						imgPath=imgPath,
						data=packItem
					};
					
					CreateMenuItem (props, true);
				}
			}
			
			//DoMenuScroll();
			StartCoroutine (WaitThenLoadMenuThumbs ());
		}


		
		override protected void HandleOnMenuItemReleased (SB_ScrollableMenuItem menuItem)
		{
			// NOTE: - Do not call base.HandleOnMenuItemReleased()
			
			_selectedMenuItem = (ColouringMenuItem)menuItem;
			
			if (_popupManager.PopupIsShowing) {
				return;
			}
			
			PlayButtonPressSound();
			
			if (menuItem.data != null) {
				ColouringPackItem packItem = (ColouringPackItem)menuItem.data;
				
				ArrayList subMenuItems = new ArrayList ();
				for (int n=1; n<=packItem.numItems; n++) {
					string imgPath = packItem.image.Replace (".png", "_" + n + ".png");
					subMenuItems.Add (packItem.packID + "/" + imgPath);
				}
				
				LoadDrawingsSubMenu (subMenuItems);
			} else {
				// User has already purchased the item, and files exist locally.
				// Load the local pack xml file, before proceeding to the next scene
				ColouringPackManager.instance.GetPackAssetList (ColouringAssetType.Colouring, OnPackAssetListLoaded, menuItem.id);
			}
			
		}
		
		void OnPackAssetListLoaded (List<ColouringPackAsset> contentList)
		{
			warn ("OnPackAssetListLoaded() contentList.Count: " + contentList.Count);
			
			ArrayList subMenuItems = new ArrayList ();
			for (int i = 0; i < contentList.Count; i++) {
				ColouringPackAsset packAsset = contentList [i];
				warn ("packAsset.assetName: " + packAsset.assetName);
				
				//subMenuItems.Add(packAsset.assetName);
				subMenuItems.Add (packAsset);
			}
			
			LoadDrawingsSubMenu (subMenuItems);
		}
		
		void LoadDrawingsSubMenu (ArrayList subMenuItems)
		{
			KeyValueObject kv = new KeyValueObject ();
			kv.SetKeyValue (MENU_ITEMS_TITLE_KEY, _selectedMenuItem.GetText ());
			kv.SetKeyValue (MENU_ITEMS_KEY, subMenuItems);
			kv.SetKeyValue (MENU_ITEMS_UNLOCKED_KEY, _selectedMenuItem.Props.itemUnlocked);
			LoadScene (SB_Globals.SCENE_COLOURING_MENU_DRAWINGS, kv, true);
		}
		
		override protected void OnDestroy ()
		{
			_packContentToLoad.Clear ();
			_packContentToLoad = null;

			base.OnDestroy ();
		}
	}
}