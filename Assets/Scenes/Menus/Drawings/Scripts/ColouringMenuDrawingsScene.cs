﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Games.Colouring.Menus;
using ScaryBeasties.InAppPurchasing.Shop;
using ScaryBeasties.UI.ScrollableMenu;
using ScaryBeasties.Data;
using UnityEngine.UI;
using ScaryBeasties.Utils;
using System.IO;
using ScaryBeasties.Popups;
using ScaryBeasties.Base;
using System.Linq;
using System.Collections.Generic;
using ScaryBeasties.Sound;
using ScaryBeasties.Cam;
using YourFavouriteNamespace;


namespace ScaryBeasties.Games.Colouring
{
	public class ColouringMenuDrawingsScene : ColouringMenuBase
	{
		const int START_ON_MENU_ITEM = 2;
	
		const int STATE_IDLE = 0;
		const int STATE_SCROLL_ON = 1;
		private int _menuState = STATE_IDLE;
	
		ArrayList _menuItems;
		bool _menuItemsUnlocked = true;
		string _packId = "default_pack"; // Default value, for testing the scene standalone in the IDE
		
		private int _centerOnItemIndex = -1; // Which item should the scroll menu center on?
		private float _currentTime = 0f;	// Used during menu scroll tween
		private float _menuTargetXPos = 0f;
		private float _menuStartXPos = 0f;
		private float _menuScrollDistance = 0f;
		
		private float ITEM_NORMALISED_START_OFFSET = -0.01f;
		
		//private float ITEM_NORMALISED_GAP = 0.03525f; // This value depends on the number of items in the menu..  approx: (_menu.ContentHolder.transform.localScale.x / _menu.Items.Count )
		private float ITEM_NORMALISED_GAP = 0.026329375f; // This value depends on the number of items in the menu..  approx: (_menu.ContentHolder.transform.localScale.x / _menu.Items.Count )
		
		
		override protected void Awake ()
		{
			base.Awake ();
			BottomMenu.SELECTED_BTN = BottomMenu.BTN_PACKS;
		}
		
		// Use this for initialization
		void Start ()
		{
			base.Start ();
			
			_menu.ScrollLimit = 2800; //1400;
			_menuItems = GameConfig.DRAWING_MENU_ITEMS;
			
			if (SCENE_CONFIG_OBJECT != null) {
				if (SCENE_CONFIG_OBJECT != null) {
					if (SCENE_CONFIG_OBJECT.GetValue (MENU_ITEMS_TITLE_KEY) != null) {
						GameConfig.DRAWING_MENU_TITLE = (string)SCENE_CONFIG_OBJECT.GetValue (MENU_ITEMS_TITLE_KEY);
					}
				}
				
				if (SCENE_CONFIG_OBJECT.GetValue (MENU_ITEMS_KEY) != null) {
					_menuItems = (ArrayList)SCENE_CONFIG_OBJECT.GetValue (MENU_ITEMS_KEY);
					GameConfig.DRAWING_MENU_ITEMS = _menuItems;
				}
				
				if (SCENE_CONFIG_OBJECT.GetValue (MENU_ITEMS_UNLOCKED_KEY) != null) {
					_menuItemsUnlocked = (bool)SCENE_CONFIG_OBJECT.GetValue (MENU_ITEMS_UNLOCKED_KEY);
				}
				
				if (SCENE_CONFIG_OBJECT.GetValue (MENU_ITEMS_PACK_ID_KEY) != null) {
					_packId = (string)SCENE_CONFIG_OBJECT.GetValue (MENU_ITEMS_PACK_ID_KEY);
				}
				
				if (SCENE_CONFIG_OBJECT.GetValue (MENU_ITEMS_CENTER_ON_ITEM_KEY) != null) {
					_centerOnItemIndex = (int)SCENE_CONFIG_OBJECT.GetValue (MENU_ITEMS_CENTER_ON_ITEM_KEY);
				}
			}
			
			if (_menuItems.Count > 0) {
				SetupMenu (_menuItems);
			} else {
				// NOTE: This should only ever happen if testing the scene standalone.
				// We grab the 'default_pack' asset list, if _menuItems is empty
				ColouringPackManager.instance.GetPackAssetList (ColouringAssetType.Colouring, OnPackAssetListLoaded, _packId);
			}
		}
		
		override protected void Init ()
		{
			Text title = transform.Find ("title/label").GetComponent<Text> ();
			//title.text = SB_Utils.SanitizeString (GameConfig.DRAWING_MENU_TITLE);
			title.text = ""; // No title, for Hey Duggee Colouring menu scenes
			
			PlayChoosePicture ();
		}
		
		void OnPackAssetListLoaded (List<ColouringPackAsset> contentList)
		{
			//warn ("OnPackAssetListLoaded() contentList.Count: " + contentList.Count);
			
			ArrayList subMenuItems = new ArrayList ();
			for (int i = 0; i < contentList.Count; i++) {
				ColouringPackAsset packAsset = contentList [i];
				//warn ("packAsset.assetName: " + packAsset.assetName);
				
				//subMenuItems.Add(packAsset.assetName);
				subMenuItems.Add (packAsset);
			}
			
			_menuItems = subMenuItems;
			SetupMenu (_menuItems);
		}
		
		private void SetupMenu (ArrayList menuItems)
		{
			_menu.ScrollRect.enabled = false;
			
			//error ("SetupMenu menuItems.Count: " + menuItems.Count);
			
			string imgPath = "";
			
			for (int i = 0; i < menuItems.Count; i++) {
				ColouringPackAsset packAsset = (ColouringPackAsset)menuItems [i];
				
				imgPath = GetMenuItemImagePath (packAsset.assetName);
				
				string creationId = "";
				string creationFolder = "";
				
				// Check if a coloured preview image exists for this drawing
				DirectoryInfo[] dirs = DrawingManager.instance.GetDrawingDirectoriesByID (packAsset.id);
				
				if (dirs != null && dirs.Length > 0) {
					/*
					// Order the files by date
					FileInfo[] files = dirs[0].GetFiles().OrderBy(p => p.LastWriteTime).ToArray();
					creationId = files[0].Name;
					creationFolder = files[0].FullName;
					*/	
					
					creationId = dirs [0].Name;
					creationFolder = dirs [0].FullName;
					
					string userDrawingPreviewPath = DrawingManager.instance.GetPreviewImage (packAsset.id, creationFolder);
					if (userDrawingPreviewPath != "") {
						imgPath = userDrawingPreviewPath;
					}
				} else {
					//warn ("*** dirs is null! packAsset.id: " + packAsset.id);
				}
				//warn ("imgPath [" + i + "] = " + imgPath);
				
				ColouringMenuItemProperties props = new ColouringMenuItemProperties{
																					creationId=creationId,
																					creationFolder=creationFolder,
																					id=packAsset.id,
																					colour=Color.white,
																					imgPath=imgPath,
																					data=packAsset,
																					itemUnlocked=_menuItemsUnlocked
																					};
				//warn ("CREATING MENU ITEM - PROPS: " + props.ToString ());															
				
				ColouringMenuItem menuItem = CreateMenuItem (props, false);
				menuItem.index = i;
				menuItem.ShowPadlock (!_menuItemsUnlocked);
				
				//error ("-------------> SHOW PADLOCK: " + !_menuItemsUnlocked);
				//log ("<color=orange>----  menuItem.Width: " + menuItem.Width + "</color>");
			}
			
			
			// Start the menu off from the right hand side, and preload a few (..but not all) of the item images
			int numThumbsToLoad = 5; //8;
			if (numThumbsToLoad > menuItems.Count)
				numThumbsToLoad = menuItems.Count;
			

			
			float vNormPos = SB_ScrollableMenu.MENU_POSITION_TOP;
			//float hNormPos = SB_ScrollableMenu.MENU_POSITION_CENTER; // Start scrolling the menu from the center point
			//float hNormPos = GetItemNormalisedHorizPos(START_ON_MENU_ITEM);
			
			int startOnItem = START_ON_MENU_ITEM;
			if (_centerOnItemIndex == -1) {
				_centerOnItemIndex = 0;
			}
			if (_centerOnItemIndex > 0)
				startOnItem = _centerOnItemIndex - 1;
			float hNormPos = GetItemNormalisedHorizPos (startOnItem);
			//log ("<color=orange>----  startOnItem: " + startOnItem + ", hNormPos: " + hNormPos + "</color>");
			
			int startThumbIndex = startOnItem - 1;
			if (startThumbIndex < 0)
				startThumbIndex = 0;
			
			//log ("<color=orange>----  startThumbIndex: " + startThumbIndex + "</color>");
			//log ("<color=orange>---- menuItems.Count: " + _menuItems.Count + ", _menu.ContentHolderWidth: " + _menu.ContentHolderWidth + "</color>");
			
			StartCoroutine (WaitThenLoadMenuThumbs (startThumbIndex, numThumbsToLoad, vNormPos, hNormPos));
		}
		
		float GetItemNormalisedHorizPos (int itemIdx)
		{
			float normPos = ITEM_NORMALISED_START_OFFSET + (ITEM_NORMALISED_GAP * itemIdx);
			
			if (normPos < 0f)
				normPos = 0f;
			else if (normPos > 1f)
				normPos = 1f;
			
			return normPos;
		}
		
		override protected void HandleOnItemThumbsLoaded ()
		{
			//log ("<color=green>HandleOnItemThumbsLoaded ()</color>");
			base.HandleOnItemThumbsLoaded ();
			StartCoroutine (WaitThenScrollMenu ());
		}

		IEnumerator WaitThenScrollMenu ()
		{
			/*
			log("WaitThenScrollMenu () ...3");
			yield return new WaitForSeconds(1);
			
			log("WaitThenScrollMenu () ...2");
			yield return new WaitForSeconds(1);
			
			log("WaitThenScrollMenu () ...1");
			yield return new WaitForSeconds(1);
			*/
			
			yield return new WaitForSeconds (0.25f);
			
			//log ("WaitThenScrollMenu ()");
			
			//_menu.ScrollRect.horizontalNormalizedPosition = SB_ScrollableMenu.MENU_POSITION_CENTER;
			
			int startOnItem = START_ON_MENU_ITEM;
			if (_centerOnItemIndex == -1) {
				_centerOnItemIndex = 0;
			}
			if (_centerOnItemIndex > 0)
				startOnItem = _centerOnItemIndex - 1;
			_menu.ScrollRect.horizontalNormalizedPosition = GetItemNormalisedHorizPos (startOnItem);
			
			//log ("<color=green>---- _centerOnItemIndex: " + _centerOnItemIndex + ", startOnItem: " + startOnItem + "</color>");
						
			Canvas.ForceUpdateCanvases ();
			
			yield return new WaitForEndOfFrame ();
			
			_menuStartXPos = _menu.ContentHolderRectTransform.position.x;
			_menuTargetXPos = _menu.GetCenteredXPos (_centerOnItemIndex);
			//log ("_menuStartXPos: " + _menuStartXPos + ", _menuTargetXPos: " + _menuTargetXPos);
			
			yield return new WaitForEndOfFrame ();
			_menuState = STATE_SCROLL_ON;
			SB_SoundManager.instance.PlayVoice ("Localised/VO/Charlie/vo_colouring_choosepicture");
		}
		
		override protected void HandleOnMenuStartDrag ()
		{
			//_menuState = STATE_IDLE;
		}
		
		void Update ()
		{
			//log("_menu.ScrollRect.normalizedPosition: " + _menu.ScrollRect.normalizedPosition.x + ", _menuState: " + _menuState + ", _menu.ContentHolderRectTransform.position: " + _menu.ContentHolderRectTransform.position);
			//log("_menu.ScrollRect.normalizedPosition: " + _menu.ScrollRect.normalizedPosition.x);
			
			if (_menuState == STATE_SCROLL_ON) {
				_currentTime += Time.deltaTime;
				double step = System.Math.Min (_currentTime / 1.0f, 1.0);
				float easedStep = Easing.EaseOut ((double)step, EasingType.Quadratic);
				
				_menuScrollDistance = (_menuStartXPos - _menuTargetXPos);
				
				Vector2 newPos = _menu.ContentHolderRectTransform.position;
				newPos.x = _menuStartXPos - (_menuScrollDistance * easedStep);
				_menu.ContentHolderRectTransform.position = newPos;
				
				//log("easedStep: " + easedStep + ", _menuScrollDistance: " + _menuScrollDistance + ", newPos.x: " + newPos.x);
				
				if (_menu.ScrollRect.normalizedPosition.x > 1) {
					_menu.SetContentsNormalisedPosition (1);
					OnMenuReady ();
				} else if (_menu.ScrollRect.normalizedPosition.x < 0) {
					_menu.SetContentsNormalisedPosition (0);
					OnMenuReady ();
				}
				
				if (easedStep == 1) {
					OnMenuReady ();
				}
			}
		}
		
		override protected void OnMenuReady ()
		{
			base.OnMenuReady ();
			_menuState = STATE_IDLE;
			_menu.ScrollRect.enabled = true;
		}
		
		protected override void DeleteSelectedDrawing ()
		{
			DeleteDrawingFiles ();
			
			_menu.RemoveAllItems ();
			SetupMenu (_menuItems);
		}
		
		#region AUDIO
		/*
		override protected void PlayIdleVO()
		{
			//PlayAudioTap();
		}
		*/
		override protected void PlaySelectedItemVO ()
		{
			string vo = "";
			/*
			// TEMP - hardcoded...
			if(_selectedMenuItem.id == "default_01")
			{
				vo = "Localised/VO/Narrator/General/vo_gen_tag_2";
			}
			else if(_selectedMenuItem.id == "default_02")
			{
				vo = "Localised/VO/Narrator/General/vo_gen_roly_1";
			}
			else if(_selectedMenuItem.id == "default_03")
			{
				vo = "Localised/VO/Narrator/General/vo_gen_norrie_1";
			}
			else if(_selectedMenuItem.id == "default_04")
			{
				vo = "Localised/VO/Narrator/General/vo_gen_happy_1";
			}
			else if(_selectedMenuItem.id == "default_05")
			{
				vo = "Localised/VO/Narrator/General/vo_gen_betty_2";
			}
			else if(_selectedMenuItem.id == "default_07")
			{
				vo = "Localised/VO/Squirrels/VO_kids_duggee";
			}
			else
			{
				vo = "Localised/VO/Squirrels/VO_yay";
			}
			*/
			
			vo = "Localised/VO/Squirrels/VO_yay";
			
			if (vo != "") {
				SB_SoundManager.instance.ClearVOQueue ();
				SB_SoundManager.instance.PlayVoice (vo);
				
			}	
		}
		#endregion
		
		override protected void OnDestroy ()
		{
			//_menuItems.Clear ();
			_menuItems = null;

			base.OnDestroy ();
		}
	}
}
