﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using ScaryBeasties.Games.Colouring;
using ScaryBeasties.Utils;
using System.IO;
using ScaryBeasties.Games.Colouring;
using System.Collections.Generic;
using ScaryBeasties.UI.ScrollableMenu;

namespace ScaryBeasties.Games.Colouring.Menus
{
	public class ColouringMenuMyCreationsScene : ColouringMenuBase
	{
		private int _numCreations = 0;
		private int _packAssetCount = 0;
	
		override protected void Awake ()
		{
			base.Awake ();
			BottomMenu.SELECTED_BTN = BottomMenu.BTN_MY_CREATIONS;
		}
		
		override protected void Start ()
		{
			base.Start ();
			SetupMenu ();
		}
		
		override protected void Init ()
		{
			Text title = transform.Find ("title/label").GetComponent<Text> ();
			string titleTxt = SB_Utils.GetNodeValueFromConfigXML ("config/copy/menus/my_creations/text", "title");
			//title.text = SB_Utils.SanitizeString (titleTxt);
			title.text = ""; // No title, for Hey Duggee Colouring menu scenes
		}
		
		private void SetupMenu ()
		{
			warn ("-------------------SetupMenu()");
			string[] drawingsContentFolders = DrawingManager.instance.DrawingsContentFolders;
			_numCreations = 0;
			int numDrawings = DrawingManager.instance.GetNumberOfUserDrawings();
			
			for (int n=0; n<drawingsContentFolders.Length; n++) {
				string folder = drawingsContentFolders [n];
				folder = folder.Replace ("\\", "/");
				
				string drawingID = DrawingManager.instance.GetDrawingID (folder);
				warn ("\t\t\tdrawingID: " + drawingID);
				
				DirectoryInfo[] dirs = DrawingManager.instance.GetDrawingDirectoriesByID (drawingID);
				if (dirs != null && dirs.Length > 0) {
					_numCreations++;
					
					// Do not construct menu items yet.
					// First we need to retrieve the 'packAsset' object for each object
					ColouringPackManager.instance.GetPackAsset (ColouringAssetType.Colouring, drawingID, OnPackAssetLoaded);
				}
			}
			
			//warn ("_numCreations: " + _numCreations);	
			Debug.Log("<color=green>_numCreations:</color> " + _numCreations + ", numDrawings: " + numDrawings);
			if (_numCreations == 0) {
				ShowEmptyMenuMessage ();
			}
		}
		
		void OnPackAssetLoaded (List<ColouringPackAsset> packAssetList)
		{
			_packAssetCount++;
			
			ColouringPackAsset packAsset = packAssetList [0];
			//string imgPath = DrawingManager.instance.GetDrawingPreviewPath(packAsset.id);
			
			string imgPath = "";
			string creationId = "";
			string creationFolder = "";
			
			log ("OnPackAssetLoaded ---------> " + packAsset.ToString ());
			DirectoryInfo[] dirs = DrawingManager.instance.GetDrawingDirectoriesByID (packAsset.id);
			
			if (dirs != null && dirs.Length > 0) {
				for (int n=0; n<dirs.Length; n++) {
					creationId = dirs [n].Name;
					creationFolder = dirs [n].FullName;
					imgPath = DrawingManager.instance.GetPreviewImage (packAsset.id, creationFolder);
					
					ColouringMenuItemProperties props = new ColouringMenuItemProperties
					{
						creationId=creationId,
						creationFolder=creationFolder,
						id=packAsset.id,
						colour=Color.white,
						imgPath=imgPath,
						data=packAsset,
						itemUnlocked=true
					};
					
					CreateMenuItem (props);				
				}
			}
			
			if (_packAssetCount == _numCreations) {
				//DoMenuScroll();
				StartCoroutine (WaitThenLoadMenuThumbs ());
			}
		}
		
		override protected void HandleOnMenuItemRemoved (int numItems)
		{
			base.HandleOnMenuItemRemoved (numItems);
			
			if (numItems == 0) {
				ShowEmptyMenuMessage ();
			}
		}
		
		private void ShowEmptyMenuMessage ()
		{
			string msg = SB_Utils.SanitizeString (SB_Utils.GetNodeValueFromConfigXML ("config/copy/menus/my_creations/text", "empty"));
			ShowMessage (msg);
		}

		void OnDestroy ()
		{

			base.OnDestroy ();
		}
	}
}
