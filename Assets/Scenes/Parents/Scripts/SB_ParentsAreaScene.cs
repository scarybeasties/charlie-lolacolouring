﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ScaryBeasties.Controls;
using ScaryBeasties.Base;
using ScaryBeasties.History;
using ScaryBeasties.UI;
using ScaryBeasties.Utils;
using ScaryBeasties.WebView;
using ScaryBeasties.Data;
using UnityEngine.UI;
using ScaryBeasties.Sound;
using ScaryBeasties.Network;

namespace ScaryBeasties.Scenes
{
	public class SB_ParentsAreaScene : SB_BaseScene
	{
		// Store the text mesh objects
		private Text _titleText;
		protected SB_WebViewHandler _webViewHandler;

		private SB_Button _redeemCodeBtn;
		private SB_Button _moreGamesBtn;
		private SB_Button _moreInfoBtn;
		private SB_Button _helpBtn;
		private SB_Button _rateAppBtn;

		int[] BUTTON_POSITIONS_4 = new int[] { -121, -40, 40, 121}; 
		int[] BUTTON_POSITIONS_5 = new int[] { -162, -81, 0, 81, 162 }; 

		override protected void Start ()
		{
			//GameObject dummyWebView = transform.Find("DummyWebview").gameObject;
			GameObject webViewHolder = transform.Find ("WebViewHolder").gameObject;			
			//Destroy(dummyWebView);
			
			base.Start ();
			_uiControls.ShowButtons (new List<string> (new string[]  {SB_UIControls.BTN_BACK}));

			int rnd = 1+(int)Random.Range(0, 7);
			tk2dBaseSprite bug = transform.Find("logos/bugbies").GetComponent<tk2dBaseSprite>();
			bug.SetSprite("bugbies_0"+rnd);

			SetupButtons();

		}

		override protected void Init ()
		{
			// Get the text object, and set content from config.xml
			_titleText = transform.Find ("text/title").GetComponent<Text> ();
			_titleText.text = SB_Utils.GetNodeValueFromConfigXML ("config/copy/parents/text", "title");

			
			// Set the dynamic text
			_redeemCodeBtn.label 	= SB_Utils.GetNodeValueFromConfigXML ("config/buttons/button", "redeemcode");
			_moreGamesBtn.label 	= SB_Utils.GetNodeValueFromConfigXML ("config/buttons/button", "moregames");
			_moreInfoBtn.label 		= SB_Utils.GetNodeValueFromConfigXML ("config/buttons/button", "moreinfo");
			_helpBtn.label 			= SB_Utils.GetNodeValueFromConfigXML ("config/buttons/button", "help");
			_rateAppBtn.label 		= SB_Utils.GetNodeValueFromConfigXML ("config/buttons/button", "rateapp");


			// Find the UniWebView component
			GameObject webViewGameObject = GameObject.FindGameObjectWithTag("WebView");
			if(webViewGameObject != null)
			{
				_webViewHandler = webViewGameObject.GetComponent<SB_WebViewHandler>();
			}

			#if UNITY_IOS
			//SB_TextField debug = GameObject.Find ("debug").GetComponent<SB_TextField>();
			//debug.text = iPhone.generation.ToString() + ", " + tk2dSystem.CurrentPlatform;
			#endif
		}

		void SetupButtons ()
		{
			// Find the buttons
			_redeemCodeBtn 	= transform.Find("redeem_code_btn").gameObject.GetComponent<SB_Button>();
			_moreGamesBtn 	= transform.Find("more_games_btn").gameObject.GetComponent<SB_Button>();
			_moreInfoBtn 	= transform.Find("more_info_btn").gameObject.GetComponent<SB_Button>();
			_helpBtn 		= transform.Find("help_btn").gameObject.GetComponent<SB_Button>();
			_rateAppBtn 	= transform.Find("rate_app_btn").gameObject.GetComponent<SB_Button>();

			float yPos = _helpBtn.transform.position.y;

			// GT - 13.07.2015 - there's no redeem button for the Hey Duggee launch
			_redeemCodeBtn.gameObject.SetActive(false);
			
			// GT - 21.07.2016 - Hide all buttons, until net connection check is complete
			_moreGamesBtn.gameObject.SetActive(false);
			_moreInfoBtn.gameObject.SetActive(false);
			_helpBtn.gameObject.SetActive(false);
			_rateAppBtn.gameObject.SetActive(false);

			
			_moreGamesBtn.transform.position = new Vector2(BUTTON_POSITIONS_4[0], yPos);
			_moreInfoBtn.transform.position = new Vector2(BUTTON_POSITIONS_4[1], yPos);
			_helpBtn.transform.position = new Vector2(BUTTON_POSITIONS_4[2], yPos);
			_rateAppBtn.transform.position = new Vector2(BUTTON_POSITIONS_4[3], yPos);
			
			// Is the device connected to the internet?
			
			if(Application.internetReachability != NetworkReachability.NotReachable)
			{
				_moreGamesBtn.gameObject.SetActive(true);
				_moreInfoBtn.gameObject.SetActive(true);
				_helpBtn.gameObject.SetActive(true);
				_rateAppBtn.gameObject.SetActive(true);
			}
			else
			{
				// Not connected!
				// Hide some of the buttons
				
				_moreGamesBtn.gameObject.SetActive(false);
				_rateAppBtn.gameObject.SetActive(false);
				
				_moreInfoBtn.gameObject.SetActive(true);
				_helpBtn.gameObject.SetActive(true);
			}
			
			/*
			SB_ConnectionChecker.CheckInternetConnection(this, (isConnected)=>
			{
				Debug.Log("<color=orange>isConnected: " + isConnected + "</color>");
				if(isConnected)
				{
					_moreGamesBtn.gameObject.SetActive(true);
					_moreInfoBtn.gameObject.SetActive(true);
					_helpBtn.gameObject.SetActive(true);
					_rateAppBtn.gameObject.SetActive(true);
				}
				else
				{
					// Not connected!
					// Hide some of the buttons
					
					_moreGamesBtn.gameObject.SetActive(false);
					_rateAppBtn.gameObject.SetActive(false);
					
					_moreInfoBtn.gameObject.SetActive(true);
					_helpBtn.gameObject.SetActive(true);
				}
			});
			*/
		}

		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			base.OnBtnHandler(btn, eventType);

			if(eventType == SB_Button.MOUSE_UP)
			{
				// Local var, to store target url
				var url = "";
				KeyValueObject kvObject;

				switch(btn.name)
				{
					case "redeem_code_btn":
									//LogButtonEvent("redeem");
					SB_Analytics.instance.SceneEvent(SB_AnalyticEventList.REDEEM_BTN);

									PlayOutroAndLoad(SB_Globals.SCENE_REDEEM_CODE);
									break;

					case "more_info_btn":
									//LogButtonEvent("moreinfo");
					SB_Analytics.instance.SceneEvent(SB_AnalyticEventList.INFO_BTN);

									PlayOutroAndLoad(SB_Globals.SCENE_MORE_INFO);
									break;

					case "more_games_btn":
									//LogButtonEvent("moregames");
					SB_Analytics.instance.SceneEvent(SB_AnalyticEventList.MORE_GAMES_BTN);

									//url = SB_Utils.GetNodeValueFromConfigXML("config/links/link", "parents_more_games");
									kvObject = new KeyValueObject();
									kvObject.SetKeyValue("name", "Webview_MoreGames");
									kvObject.SetKeyValue("url", SB_Utils.GetNodeValueFromConfigXML("config/links/link", "parents_more_games"));
									kvObject.SetKeyValue("url_offline", SB_Utils.GetNodeValueFromConfigXML("config/links/link", "parents_more_games_offline"));
									
									LoadScene(SB_Globals.SCENE_WEBVIEW_GENERIC, kvObject, true);
									break;

					case "help_btn":
									//LogButtonEvent("help");
					SB_Analytics.instance.SceneEvent(SB_AnalyticEventList.HELP_BTN);

									PlayOutroAndLoad(SB_Globals.SCENE_HELP);
									break;

					case "rate_app_btn":
									//LogButtonEvent("rateapp");
					SB_Analytics.instance.SceneEvent(SB_AnalyticEventList.RATE_BTN);

									// ios
									url = SB_Utils.GetNodeValueFromConfigXML("config/links/link", "parents_rate_app_ios");
									
									#if UNITY_ANDROID
									if(SB_Globals.PLATFORM_ID == SB_BuildPlatforms.GooglePlay)
									{
										// android - google play
										url = SB_Utils.GetNodeValueFromConfigXML("config/links/link", "parents_rate_app_android");
									}
									else if(SB_Globals.PLATFORM_ID == SB_BuildPlatforms.Amazon)
									{
										// android - amazon market
										url = SB_Utils.GetNodeValueFromConfigXML("config/links/link", "parents_rate_app_android_amazon");
										//warn ("ORIGINAL AMAZON URL: " + url);
										url = SB_Utils.ParseAmazonStoreLink(url);
										//warn ("FIXED AMAZON URL: " + url);
									}
									else if(SB_Globals.PLATFORM_ID == SB_BuildPlatforms.Samsung)
									{
										// android - amazon market
										url = SB_Utils.GetNodeValueFromConfigXML("config/links/link", "parents_rate_app_android_samsung");
									}
									#endif
									break;
				}
				
				// Open target link
				if(url != "")
				{
					log ("url: " + url);
					OpenExternalLink(url);
				}
			}
		}

		override protected void OnDestroy()
		{
			if(_titleText != null)
			{
				Destroy(_titleText);
				_titleText = null;
			}
			
			if(_webViewHandler != null)
			{
				Destroy(_webViewHandler);
				_webViewHandler = null;
			}

			_redeemCodeBtn = null;
			_moreGamesBtn = null;
			_moreInfoBtn = null;
			_helpBtn = null;
			_rateAppBtn = null;

			base.OnDestroy();
		}
	}
}