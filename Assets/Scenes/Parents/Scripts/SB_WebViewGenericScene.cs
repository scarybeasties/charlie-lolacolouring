using UnityEngine;
using System.Collections;
using ScaryBeasties.Base;
using ScaryBeasties.WebView;
using ScaryBeasties.History;
using ScaryBeasties.UI;
using System.Collections.Generic;
using System;
using ScaryBeasties.Appstore;
using ScaryBeasties.Utils;
using System.Net;
using System.IO;

namespace ScaryBeasties.Scenes
{
    /**
	 * Generic scene containing a full screen WebView.
	 * When the WebView is closed/finished, the user is returned 
	 * to the previous scene.
	 */
    public class SB_WebViewGenericScene : SB_BaseScene
    {
        private SBWebView _webView;
        private string _url, _offlineUrl = "";
        private string _sceneName = "";

        const string WEBVEW_EXPIRY_KEY = "WebviewExpiryDate";

        protected virtual void Awake()
        {
#if UNITY_ANDROID
			if(transform.Find("bottom_black_bar") != null)
			{
				transform.Find("bottom_black_bar").gameObject.SetActive(true);
			}
#endif
        }

        override protected void Start()
        {
            if (SCENE_CONFIG_OBJECT != null)
            {
                if (SCENE_CONFIG_OBJECT.GetValue("name") != null)
                {
                    log("1) _sceneName: " + _sceneName);
                    _sceneName = (string)SCENE_CONFIG_OBJECT.GetValue("name");
                }
                else
                {
                    log("2) _sceneName: " + _sceneName);
                    _sceneName = Application.loadedLevelName;
                }
                log("3) _sceneName: " + _sceneName);

                if (SCENE_CONFIG_OBJECT.GetValue("url") != null)
                {
                    _url = (string)SCENE_CONFIG_OBJECT.GetValue("url");
                }

                if (SCENE_CONFIG_OBJECT.GetValue("url_offline") != null)
                {
                    _offlineUrl = (string)SCENE_CONFIG_OBJECT.GetValue("url_offline");
                }

                // Append platform id (ios, android) and locale to the webview url
                string append = "&pl=" + (int)SB_Globals.PLATFORM_ID + "&loc=" + SB_Globals.LOCALE_ID;
                _url += append;
            }

            base.Start();

            warn("url: " + _url);
            warn("url_offline: " + _offlineUrl);

            GameObject webview = GameObject.FindGameObjectWithTag("WebView");

            _webView = webview.GetComponent<SBWebView>();
            //_webView.SetTransparentBackground();
            //_webView.Hide();
            _webView.Setup();

            CacheClearCheck();

            // Load local page, incase user is offline 
            string targetURL = "";
#if UNITY_ANDROID
            targetURL = _offlineUrl;
#elif UNITY_IOS
            targetURL = Application.streamingAssetsPath + "/" + _offlineUrl;
#endif

            // Is the device connected to the internet?

            if (TestInternetConnection(_url))
            {
                targetURL = _url;
            }
            _webView.LoadPage(targetURL);
            AddWebViewDelegates();
        }

        private bool TestInternetConnection(string targetURL)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(targetURL);
            try
            {
                using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
                {
                    bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
                    if (isSuccess)
                    {
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
            return false;
        }

        /**
		 * We don't want to track 'WebViewGeneric', so override and use something meaningful instead
		 */
        override public string GetSceneTrackingName()
        {
            return _sceneName;
        }

        private void AddWebViewDelegates()
        {
            //warn("AddWebViewDelegates()");

            _webView.OnReceivedMessage += OnReceivedMessage;
            _webView.OnLoadComplete += OnLoadComplete;
            _webView.OnWebViewShouldClose += OnWebViewShouldClose;
            //_webView.OnEvalJavaScriptFinished += OnEvalJavaScriptFinished;
        }

        private void RemoveWebViewDelegates()
        {
            //warn("RemoveWebViewDelegates()");

            _webView.OnReceivedMessage -= OnReceivedMessage;
            _webView.OnLoadComplete -= OnLoadComplete;
            _webView.OnWebViewShouldClose -= OnWebViewShouldClose;
            //_webView.OnEvalJavaScriptFinished -= OnEvalJavaScriptFinished;
        }


        /**
		 * Called when the webview has loaded its content
		 * 
		 * NOTE: this won't recognise a 404 page as an 'error', it will just 
		 * return a 'true' value for success, because the actual 404 page 
		 * has loaded (instead of the desired content..)
		 */
        void OnLoadComplete(SBWebView webView, bool success, string errorMessage)
        {
            //warn("OnLoadComplete " + success);

            if (!success)
            {
                warn("Something wrong in webview loading: " + errorMessage);
                RemoveWebViewDelegates();
                webView.Close();
                return;
            }

            webView.Show();
        }

        void OnReceivedMessage(SBWebView webView, SBWebViewMessage message)
        {
            warn("OnReceivedMessage()");

            Debug.Log(message.rawMessage);

            if (string.Equals(message.path, "close"))
            {
                webView.Close();
                Destroy(webView);

                WebViewClosed();
            }
            else if (string.Equals(message.path, "exitlink"))
            {
                string url = message.args["url"] as string;

                // The 3 vars are only used for iOS
                string appID = "";
                string campaignToken = "";
                string affiliateToken = "";

#if UNITY_IOS

                if (message.args.ContainsKey("appID")) { appID = message.args["appID"] as string; }
                if (message.args.ContainsKey("campaignToken")) { campaignToken = message.args["campaignToken"] as string; }
                if (message.args.ContainsKey("affiliateToken")) { affiliateToken = message.args["affiliateToken"] as string; }

                Debug.Log("appID: " + appID);
                Debug.Log("campaignToken: " + campaignToken);
                Debug.Log("affiliateToken: " + affiliateToken);

#endif

                if (appID.Length > 0 && campaignToken.Length > 0)
                {
                    // This will track an event with the name of the app we are opening:
                    // 'OpenLink_NativeAppstore_sarah-duck-day-at-the-park',
                    // 'OpenLink_NativeAppstore_charlie-lola-my-little-town',
                    // etc..
                    LogSceneEvent("OpenLink_NativeAppstore_" + campaignToken);

                    // iOS - open the link in native popup, without leaving the app
                    AppstoreGameDataVO appstoreGameDataVO = new AppstoreGameDataVO(int.Parse(appID), campaignToken, affiliateToken);
                    AppstoreHandler.instance.openAppInStore(appstoreGameDataVO);
                }
                else
                {
                    if (SB_Globals.PLATFORM_ID == SB_BuildPlatforms.Amazon)
                    {
                        url = SB_Utils.ParseAmazonStoreLink(url);
                    }

                    LogSceneEvent("OpenLink_" + url);
                    url = UnityEngine.Networking.UnityWebRequest.UnEscapeURL(url);
                    warn("Opening exit link url: " + url);
                    Application.OpenURL(url);
                }
                SB_Analytics.instance.PromoEvent(message.rawMessage, false);
            }
        }

        /**
		 * Return a 'false' value, to prevent 
		 * android native back button from closing the webview
		 */
        void OnWebViewShouldClose(SBWebView webView)
        {
            WebViewClosed();
        }

        void WebViewClosed()
        {
            string prevScene = SB_SceneHistory.instance.GetPreviousItem(true);
            //warn("WebViewClosed() loading previous scene " + prevScene);

            // Load the previous scene
            LoadScene(prevScene, null, true);
        }

        void CacheClearCheck()
        {
            // Try and get expiry date from PlayerPrefs
            string expiryStr = PlayerPrefs.GetString(WEBVEW_EXPIRY_KEY);
            Debug.LogError("CacheClearCheck() expiryStr: " + expiryStr);

            DateTime now = DateTime.Now;

            // Set a default value, with an expiry date of yesterday
            DateTime expiry = now.AddDays(-1);
            if (expiryStr.Length > 0)
            {
                // Parse a saved expiry date
                expiry = DateTime.Parse(expiryStr);
            }

            //Debug.LogError("NOW: " + now.ToString());
            //Debug.LogError("expiry: " + expiry.ToString());

            if (now > expiry)
            {
                Debug.LogError("CacheClearCheck() Expired!");


                // Set new expiry date of tomorrow
                expiry = now.AddDays(1);

                Debug.LogError("CacheClearCheck() Setting new expiry date: " + expiry.ToString());
                PlayerPrefs.SetString(WEBVEW_EXPIRY_KEY, expiry.ToString());
            }
            else
            {
                //Debug.LogError("ok.");
            }
        }

        void OnDestroy()
        {
            //warn("OnDestroy()");
            RemoveWebViewDelegates();

            Destroy(_webView);
            _webView = null;
        }
    }
}
