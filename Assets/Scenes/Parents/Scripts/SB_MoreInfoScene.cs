﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ScaryBeasties.Controls;
using ScaryBeasties.Base;
using ScaryBeasties.History;
using ScaryBeasties.UI;
using ScaryBeasties.Utils;
using ScaryBeasties.Data;
using ScaryBeasties.Sound;
using UnityEngine.UI;

namespace ScaryBeasties.Scenes
{
	public class SB_MoreInfoScene : SB_BaseScene
	{	
		// Store the text mesh objects
		private Text _titleText;

		private SB_Button _termsBtn;
		private SB_Button _privacyBtn;
		private SB_Button _contactBtn;
		private SB_Button _disclaimerBtn;

		override protected void Start ()
		{
			base.Start ();
			_uiControls.ShowButtons (new List<string> (new string[]  {SB_UIControls.BTN_BACK}));

			//int rnd = 1+(int)Random.Range(0, 7);
			//tk2dBaseSprite bug = transform.Find("logos/bugbies").GetComponent<tk2dBaseSprite>();
			//bug.SetSprite("bugbies_0"+rnd);
		}

		override protected void Init ()
		{
			// Get the text object, and set content from config.xml
			_titleText = GameObject.Find ("text/title").GetComponent<Text> ();
			_titleText.text = SB_Utils.GetNodeValueFromConfigXML ("config/copy/more_info/text", "title");

			// Find the buttons
			_termsBtn = transform.Find ("terms_btn").gameObject.GetComponent<SB_Button> ();
			_privacyBtn = transform.Find ("privacy_btn").gameObject.GetComponent<SB_Button> ();
			_contactBtn = transform.Find ("contact_btn").gameObject.GetComponent<SB_Button> ();
			_disclaimerBtn = transform.Find ("disclaimer_btn").gameObject.GetComponent<SB_Button> ();
			
			// Set the dynamic text
			_termsBtn.label = SB_Utils.GetNodeValueFromConfigXML ("config/buttons/button", "terms");
			_privacyBtn.label = SB_Utils.GetNodeValueFromConfigXML ("config/buttons/button", "privacy");
			_contactBtn.label = SB_Utils.GetNodeValueFromConfigXML ("config/buttons/button", "contact");
			_disclaimerBtn.label = SB_Utils.GetNodeValueFromConfigXML ("config/buttons/button", "disclaimer");
		}

		override protected void OnBtnHandler (SB_Button btn, string eventType)
		{
			base.OnBtnHandler (btn, eventType);

			if (eventType == SB_Button.MOUSE_UP) {
				// Local var, to store target url
				var url = "";
				KeyValueObject kvObject;
				PlayButtonPressSound();
				
				switch (btn.name) {
				case "terms_btn":
					LogButtonEvent ("terms");
					SB_Analytics.instance.SceneEvent(SB_AnalyticEventList.TERMS_BTN);

									//url = SB_Utils.GetNodeValueFromConfigXML("config/links/link", "moreinfo_terms");
									
									// Open a fullscreen webview, pass it the url and an offline url (incase user is offline!)
					kvObject = new KeyValueObject ();
					kvObject.SetKeyValue ("name", "Webview_Terms");
					kvObject.SetKeyValue ("url", SB_Utils.GetNodeValueFromConfigXML ("config/links/link", "moreinfo_terms"));
					kvObject.SetKeyValue ("url_offline", SB_Utils.GetNodeValueFromConfigXML ("config/links/link", "moreinfo_terms_offline"));
									
					LoadScene (SB_Globals.SCENE_WEBVIEW_GENERIC, kvObject, true);
					break;

				case "privacy_btn":
					LogButtonEvent ("privacy");
					SB_Analytics.instance.SceneEvent(SB_AnalyticEventList.PRIVACY_BTN);

									//url = SB_Utils.GetNodeValueFromConfigXML("config/links/link", "moreinfo_privacy");

									// Open a fullscreen webview, pass it the url and an offline url (incase user is offline!)
					kvObject = new KeyValueObject ();
					kvObject.SetKeyValue ("name", "Webview_Privacy");
					kvObject.SetKeyValue ("url", SB_Utils.GetNodeValueFromConfigXML ("config/links/link", "moreinfo_privacy"));
					kvObject.SetKeyValue ("url_offline", SB_Utils.GetNodeValueFromConfigXML ("config/links/link", "moreinfo_privacy_offline"));
									
					LoadScene (SB_Globals.SCENE_WEBVIEW_GENERIC, kvObject, true);
					break;

				case "contact_btn":
					LogButtonEvent ("contact");
					SB_Analytics.instance.SceneEvent(SB_AnalyticEventList.CONTACT_BTN);

									//url = SB_Utils.GetNodeValueFromConfigXML("config/links/link", "moreinfo_contact_us");kvObject = new KeyValueObject();
									
					kvObject = new KeyValueObject ();
					kvObject.SetKeyValue ("name", "Webview_ContactUs");
					kvObject.SetKeyValue ("url", SB_Utils.GetNodeValueFromConfigXML ("config/links/link", "moreinfo_contact_us"));
					kvObject.SetKeyValue ("url_offline", SB_Utils.GetNodeValueFromConfigXML ("config/links/link", "moreinfo_contact_us_offline"));
									
					LoadScene (SB_Globals.SCENE_WEBVIEW_GENERIC, kvObject, true);

									//DoContactUs();
					break;
					
				case "disclaimer_btn":
					LogButtonEvent ("disclaimer");
					SB_Analytics.instance.SceneEvent(SB_AnalyticEventList.PERMISSIONS_BTN);

									// Open a fullscreen webview, pass it the url and an offline url (incase user is offline!)
					kvObject = new KeyValueObject ();
					kvObject.SetKeyValue ("name", "Webview_Disclaimer");
					kvObject.SetKeyValue ("url", SB_Utils.GetNodeValueFromConfigXML ("config/links/link", "moreinfo_disclaimer"));
					kvObject.SetKeyValue ("url_offline", SB_Utils.GetNodeValueFromConfigXML ("config/links/link", "moreinfo_disclaimer_offline"));
									
					LoadScene (SB_Globals.SCENE_WEBVIEW_GENERIC, kvObject, true);
					break;
				}
				
				// Open target link
				if (url != "") {
					OpenExternalLink (url);
				}
			}
		}

		protected void DoContactUs ()
		{
			string email = SB_Utils.GetNodeValueFromConfigXML ("config/links/link", "moreinfo_contact_us_email");
			string subject = SB_Utils.EscapeString (SB_Utils.GetNodeValueFromConfigXML ("config/copy/more_info/text", "contact_us_title"));
			string body = SB_Utils.EscapeString (SB_Utils.GetNodeValueFromConfigXML ("config/copy/more_info/text", "contact_us_body"));
		
			print ("DoContactUs() mailto:" + email + "?subject=" + subject + "&body=" + body);
			Application.OpenURL ("mailto:" + email + "?subject=" + subject + "&body=" + body);
		}

		override protected void OnDestroy ()
		{
			if (_titleText != null) {
				Destroy (_titleText);
				_titleText = null;
			}

			if (_termsBtn != null) {
				Destroy (_termsBtn);
				_termsBtn = null;
			}
			
			if (_privacyBtn != null) {
				Destroy (_privacyBtn);
				_privacyBtn = null;
			}
			
			if (_contactBtn != null) {
				Destroy (_contactBtn);
				_contactBtn = null;
			}
			
			if (_disclaimerBtn != null) {
				Destroy (_disclaimerBtn);
				_disclaimerBtn = null;
			}

			base.OnDestroy ();
		}
	}
}