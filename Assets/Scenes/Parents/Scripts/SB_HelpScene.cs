﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Base;
using ScaryBeasties.UI;
using System.Collections.Generic;
using ScaryBeasties.Controls;
using ScaryBeasties.Games.Common;
using ScaryBeasties.Utils;
using ScaryBeasties.Sound;
using UnityEngine.UI;
using ScaryBeasties.Cam;

namespace ScaryBeasties.Scenes
{
	public class SB_HelpScene : SB_BaseScene
	{
	
		public static string[,] HELP_PREFABS = new string[1, 2] {
			{ SB_Globals.GAME_ORDER_IDS[0].ToString(), ScaryBeasties.Games.Colouring.GameConfig.HELP_POPUP_RESOURCE_NAME }
																};

		
		private int _helpIndex = -1;
		private GameObject _currentHelp = null;
		private Text _titleText;

		override protected void Start ()
		{
			base.Start ();
			_uiControls.ShowButtons (new List<string> (new string[]  {SB_UIControls.BTN_BACK}));

			_titleText = transform.Find ("text/title").GetComponent<Text> ();

			int rnd = 1 + (int)Random.Range (0, 7);
			tk2dBaseSprite bug = transform.Find ("bugbies").GetComponent<tk2dBaseSprite> ();
			bug.SetSprite ("bugbies_0" + rnd);
		}

		
		override protected void Init ()
		{
			ShowNextHelp ();
		}

		void ShowNextHelp ()
		{
			_helpIndex++;
			if (_helpIndex >= HELP_PREFABS.GetLength (0))
				_helpIndex = 0;

			CreateHelpPrefab ();
		}

		void ShowPreviousHelp ()
		{
			_helpIndex--;
			if (_helpIndex < 0)
				_helpIndex = HELP_PREFABS.GetLength (0) - 1;
			
			CreateHelpPrefab ();
		}

		void CreateHelpPrefab ()
		{
			string gameId = HELP_PREFABS [_helpIndex, 0];
			string gamePrefabPath = HELP_PREFABS [_helpIndex, 1];

			log("CreateHelpPrefab gameId: " + gameId + ", gamePrefabPath: " + gamePrefabPath);
			_titleText.text = SB_Utils.GetNodeValueFromConfigXML ("config/copy/help/text", "title_" + gameId);

			GameObject helpHolder = transform.Find ("help_holder").gameObject;
			if (_currentHelp != null) {
				Destroy (_currentHelp);
				_currentHelp = null;
			}

			// Create a new instance of the prefab
			GameObject helpPrefab = (GameObject)Resources.Load (gamePrefabPath, typeof(GameObject));

			// instantiate it
			_currentHelp = (GameObject)Instantiate (helpPrefab, helpHolder.transform.position, Quaternion.identity);

			// Reparent it
			_currentHelp.transform.parent = helpHolder.transform;
			_currentHelp.transform.localScale = helpHolder.transform.localScale;

			// Hide unnecessary items in the help 
			_currentHelp.transform.Find ("bg_trans_60pc").gameObject.SetActive (false);
			_currentHelp.transform.Find ("panel/close_btn").gameObject.SetActive (false);
			Destroy(_currentHelp.transform.Find ("PopupCamera").gameObject);

			Help helpScript = _currentHelp.GetComponent<Help> ();
			helpScript.skipIntro = true;
			helpScript.ApplyBlur(false);
		
			StartCoroutine(WaitThenDisableBlur());
		}

		protected virtual IEnumerator WaitThenDisableBlur()
		{
			yield return new WaitForEndOfFrame();
			SB_GameCamera.ApplyBlur(false);
		}
		
		override protected void OnBtnHandler (SB_Button btn, string eventType)
		{
			base.OnBtnHandler (btn, eventType);
			
			if (eventType == SB_Button.MOUSE_UP) 
			{
				PlayButtonPressSound();
				
				switch (btn.name) 
				{
					case "prev_btn":
						ShowPreviousHelp ();
						break;
	
					case "next_btn":
						ShowNextHelp ();
						break;
				}
			}
		}
		
		override protected void OnDestroy ()
		{
			_currentHelp = null;
			_titleText = null;
			
			base.OnDestroy ();
		}
	}
}