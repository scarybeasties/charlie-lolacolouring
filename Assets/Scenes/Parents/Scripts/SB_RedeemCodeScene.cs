using UnityEngine;
using System.Collections;
using ScaryBeasties.Base;
using ScaryBeasties.UI;
using System.Collections.Generic;
using ScaryBeasties.Controls;
using ScaryBeasties.Utils;
using ScaryBeasties.Input;
using ScaryBeasties.User;
using ScaryBeasties.Stickers;

namespace ScaryBeasties.Scenes
{
	public class SB_RedeemCodeScene : SB_BaseScene
	{
		private GameObject _textInputGameObj, _statusMsgGameObj;

		private SB_TextField _titleText, _codeInputText, _statusMsgText;
		//private SB_KeyboardInput _keyboardInput;

		private string _codePromptTxt, _codeIncorrectTxt, _codeCorrectTxt, _codeUsedTxt;
		private string _lastKeyboardText = "";
		private string _lastTested = "";

		private SB_Button _okButton;

		//private SB_KeyboardInput _profileNameKeyboardInput;

		private SB_Keyboard _keyboard;

		override protected void Start ()
		{
			base.Start ();
			_uiControls.ShowButtons (new List<string> (new string[]  {SB_UIControls.BTN_BACK}));

			_okButton = transform.Find ("ok_btn").GetComponent<SB_Button> ();

			_keyboard = transform.Find ("Keyboard").GetComponent<SB_Keyboard> ();

			int rnd = 1 + (int)Random.Range (0, 7);
			tk2dBaseSprite bug = transform.Find ("logos/bugbies").GetComponent<tk2dBaseSprite> ();
			bug.SetSprite ("bugbies_0" + rnd);
		}

		override protected void Init ()
		{
			// Get the text object, and set content from config.xml
			_titleText = transform.Find ("text/title").GetComponent<SB_TextField> ();
			_titleText.text = SB_Utils.GetNodeValueFromConfigXML ("config/copy/redeem/text", "title");
			_codePromptTxt = SB_Utils.GetNodeValueFromConfigXML ("config/copy/redeem/text", "enter_code");
			_codeIncorrectTxt = SB_Utils.GetNodeValueFromConfigXML ("config/copy/redeem/text", "incorrect_code");
			_codeCorrectTxt = SB_Utils.GetNodeValueFromConfigXML ("config/copy/redeem/text", "correct_code");
			_codeUsedTxt = SB_Utils.GetNodeValueFromConfigXML ("config/copy/redeem/text", "already_used");
			
			// Get the game objects
			_titleText = GameObject.Find ("text/title").GetComponent<SB_TextField> ();

			//_profileNameKeyboardInput = GameObject.Find("input_code/text_input").GetComponent<SB_KeyboardInput>();

			_textInputGameObj = GameObject.Find ("input_code/text_input");
			_codeInputText = _textInputGameObj.GetComponent<SB_TextField> ();
			//_keyboardInput = _textInputGameObj.GetComponent<SB_KeyboardInput>();

			_statusMsgGameObj = GameObject.Find ("input_code/status_msg");
			_statusMsgText = _statusMsgGameObj.GetComponent<SB_TextField> ();
			_statusMsgGameObj.SetActive (false);

			// TEMP!
			//SB_UserProfileManager.instance.ResetPacks();
			//_codePromptTxt = "QUACK";
			//_codePromptTxt = "SCARF";
			//_codePromptTxt = "WORLD";
			//_codePromptTxt = "RESET";

			//_keyboardInput.defaultText = _codePromptTxt;
			//_keyboardInput.OnKeyboardDone += onKeyboardDoneHandler;

		}

		/*
		void OnGUI()
		{
			if (_lastKeyboardText.Length > 0 && _keyboardInput.text != _lastKeyboardText) 
			{
				HideStatusMessage();
				_lastKeyboardText = _keyboardInput.text;
			}
		}*/
		/*
		void onKeyboardDoneHandler (TouchScreenKeyboard keyboard)
		{
			OKBtnPressed();
		}*/
		
		override protected void OnBtnHandler (SB_Button btn, string eventType)
		{
			//warn ("OnBtnHandler");
			base.OnBtnHandler (btn, eventType);

			if (eventType == SB_Button.MOUSE_UP) {
				//warn ("btn.name: " + btn.name);
				switch (btn.name) {
				/*case "text_input_btn":
						HideStatusMessage();

						// Set default keyboard type, for iOS
						TouchScreenKeyboardType keyboardType = TouchScreenKeyboardType.ASCIICapable;
						
						#if UNITY_ANDROID
							//keyboardType = TouchScreenKeyboardType.NamePhonePad;
						#endif

						_keyboardInput.ShowKeyboard(_codePromptTxt, keyboardType);
						break;*/
						
				case "ok_btn":
					OKBtnPressed ();
					break;
				}
			}
		}

		protected override void UpdateReady ()
		{
			base.UpdateReady ();
			
			// Has user pressed 'done' button on their on screen keyboard?
			/*if(!SB_Globals.TEST_MODE && _profileNameKeyboardInput.KeyboardIsDone)
			{
				OKBtnPressed();
			}*/

			string ts = _keyboard.GetString ();
			if (ts != _codeInputText.text) {
				HideStatusMessage ();
				_codeInputText.text = ts;
			}
		}

		// validate code
		void OKBtnPressed ()
		{
			string codeInputUppercase = _codeInputText.text.ToUpper ();
			if (codeInputUppercase == _lastTested)
				return;
			_lastTested = codeInputUppercase;

			for (int n=0; n<SB_Globals.PACK_CODES.GetLength(0); n++) {
				string packId = SB_Globals.PACK_CODES [n, 0];
				string packCode = SB_Globals.PACK_CODES [n, 1].ToUpper ();

				bool packUnlocked = SB_UserProfileManager.instance.PackIsUnlocked (int.Parse (packId));

				//warn ("codeInputUppercase: " + codeInputUppercase + ", packId: " + packId + ", packCode: " + packCode);

				if (codeInputUppercase == "RESET") {
					SB_UserProfileManager.instance.ResetPacks ();
					ShowStatusMessage ("Packs reset");
					return;
				} else if (codeInputUppercase == packCode) {
					if (packUnlocked) {
						warn ("CORRECT! PACK ALREADY UNLOCKED!");
						ShowStatusMessage (_codeUsedTxt);
					} else {
						warn ("CORRECT! UNLOCKING PACK");
						SB_UserProfileManager.instance.UnlockPack (int.Parse (packId));
						LogPackUnlockedEvent (int.Parse (packId));
						
						ShowStatusMessage (_codeCorrectTxt);
						//_keyboardInput.HideKeyboard();
						_keyboard.HideKeyboard ();

						// Show the user what they've unlocked
						tk2dBaseSprite tspr = transform.Find ("unlocked_content/image").gameObject.GetComponent<tk2dBaseSprite> ();
						tspr.SetSprite ("unlock_image_" + packId);
						transform.Find ("unlocked_content").gameObject.SetActive (true);



					}
					return;
				}
			}

			warn ("INCORRECT");
			_keyboard.SetString ("");
			_codeInputText.text = "";
			ShowStatusMessage (_codeIncorrectTxt);
		}

		void ShowStatusMessage (string msg)
		{
			_textInputGameObj.SetActive (false);
			_statusMsgGameObj.SetActive (true);
			_statusMsgText.text = msg;
		}

		void HideStatusMessage ()
		{
			//warn ("HideStatusMessage");
			_textInputGameObj.SetActive (true);
			_statusMsgGameObj.SetActive (false);
			_statusMsgText.text = "";
		}

		override protected void OnDestroy ()
		{
			_titleText = null;
			_codeInputText = null;
			//_keyboardInput = null;
			_okButton = null;

			/*
			if(_profileNameKeyboardInput != null)
			{
				Destroy(_profileNameKeyboardInput);
				_profileNameKeyboardInput = null;
			}*/

			if (_keyboard != null) {
				Destroy (_keyboard);
				_keyboard = null;
			}
			
			base.OnDestroy ();
		}
	}
}
