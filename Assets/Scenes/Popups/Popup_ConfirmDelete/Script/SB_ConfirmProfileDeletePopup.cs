using UnityEngine;
using ScaryBeasties.Controls;
using ScaryBeasties.Base;
using ScaryBeasties.Utils;
using ScaryBeasties.Cam;
using ScaryBeasties.User;

namespace ScaryBeasties.Popups
{
	public class SB_ConfirmProfileDeletePopup : SB_BasePopup 
	{
		public const string OPTION_CANCEL = "optionCancel";
		public const string OPTION_OK = "optionOk";

		private MeshRenderer _customPic;
		private tk2dSprite _profilePic;
		private SB_TextField _profileNameText;
		
		override protected void Start()
		{
			_customPic			= GameObject.Find("panel/profile/custom_pic").GetComponent<MeshRenderer>();
			_profilePic 		= GameObject.Find("panel/profile/pic").GetComponent<tk2dSprite>();
			_profileNameText 	= GameObject.Find("panel/profile/name").GetComponent<SB_TextField>();

			base.Start();
		}

		override protected void Init()
		{
			base.Init ();
			SB_ProfileVO currentProfile = SB_UserProfileManager.instance.CurrentProfile;

			print ("init delete: "+currentProfile.avatarId);
			//if(SB_Globals.SELECTED_PROFILE_VO != null)
			if(currentProfile != null)
			{
				//if(SB_Globals.SELECTED_PROFILE_VO.avatarId == "custom") 
				if(currentProfile.avatarId == "custom") 
				{
					//_customPic.renderer.material.mainTexture = SB_Globals.SELECTED_PROFILE_VO.customProfilePic;
					_customPic.GetComponent<Renderer>().material.mainTexture = currentProfile.customProfilePic;
					_profilePic.gameObject.SetActive(false);
				}
				else if(currentProfile.avatarId.Length > 0)
				{
					// Use a chosen avatar image
					//_profilePic.SetSprite(SB_Globals.SELECTED_PROFILE_VO.avatarId);
					_profilePic.SetSprite(currentProfile.avatarId);
					_customPic.gameObject.SetActive(false);
				}

				//_profileNameText.text = SB_Globals.SELECTED_PROFILE_VO.name;
				_profileNameText.text = currentProfile.name;
			}
		}

		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			base.OnBtnHandler(btn, eventType);
			
			//log("OnBtnHandler:" + btn.name + ", eventType: " + eventType);
			if(eventType == SB_Button.MOUSE_UP)
			{
				//log("OnBtnHandler:" + btn.name);
				
				switch(btn.name)
				{
					case "cancel_btn": 
						OptionChosen(OPTION_CANCEL);
						PlayOutro();
						break;
						
					case "ok_btn": 
						OptionChosen(OPTION_OK); 
						PlayOutro();
						break;
				}
			}
		}
		
		override protected void OnDestroy()
		{
			if(_profileNameText != null)
			{
				Destroy(_profileNameText);
				_profileNameText = null;
			}
			
			if(_profilePic != null)
			{
				Destroy(_profilePic);
				_profilePic = null;
			}

			if(_customPic != null)
			{
				_customPic = null;
			}

			base.OnDestroy();
		}
	}
}