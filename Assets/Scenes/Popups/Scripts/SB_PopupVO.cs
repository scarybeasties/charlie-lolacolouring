﻿/**
 * Popup VO
 * 
 * A value object, used by popups, to store their 
 * title and body text, which is set when the Popup 
 * has started.
 * 
 * Text can't be set on the popup's Start() method, 
 * because null errors are thrown!
 * 
 */

namespace ScaryBeasties.Popups
{
	public class SB_PopupVO
	{
		protected int _popupType = -1;
		protected string _prefabType = "";
		protected string _title = "";
		protected string _body = "";
		protected object _data = null;
		
		public SB_PopupVO(int popupType, string prefabType, string title, string body, object data=null)
		{
			_popupType = popupType;
			_prefabType = prefabType;
			_title = title;
			_body = body;
			_data = data;
		}
		
		// Getters
		public int popupType{		get {return _popupType;}	}
		public string prefabType{	get {return _prefabType;}	}
		public string title{		get {return _title;}		}
		public string body{			get {return _body;}			}
		public object data{			get {return _data;}			}
		
		override public string ToString()
		{
			var str = "popupType: " + popupType.ToString() + ", " + 
					"prefabType: " + prefabType + ", " + 
					"title: " + title + ", " + 
					"body: " + body;

			return str;
		}
	}
}