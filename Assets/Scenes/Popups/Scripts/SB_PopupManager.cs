﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Base;
using ScaryBeasties.Utils;

namespace ScaryBeasties.Popups
{
	public class SB_PopupManager : ScriptableObject 
	{
		// Name of the 'GatingPopup' prefab in the Resources folder.
		public const string PREFAB_GATING_POPUP = "Prefabs/SB_GatingPopup_Prefab";
		// Name of the 'GamePausedPopup' prefab in the Resources folder.
		public const string PREFAB_GAME_PAUSED_POPUP = "Prefabs/SB_GamePausedPopup_Prefab";
		// Name of the 'ConfirmDeleteProfilePopup' prefab in the Resources folder.
		public const string PREFAB_CONFIRM_DELETE_PROFILE_POPUP = "Prefabs/SB_ConfirmDeleteProfilePopup_Prefab";
		// Name of the 'GameQuitPopup_Prefab' prefab in the Resources folder.
		public const string PREFAB_GAME_QUIT_POPUP = "Prefabs/SB_GameQuitPopup_Prefab";
		// Name of the 'CameraPopup_Prefab' prefab in the Resources folder.
		public const string PREFAB_CAMERA_POPUP = "Prefabs/SB_CameraPopup_Prefab";
		// Name of the 'AlertPopup_Prefab' prefab in the Resources folder.
		public const string PREFAB_ALERT_POPUP = "Prefabs/SB_AlertPopup_Prefab";
		// Name of the 'Help_Prefab' prefab in the Resources folder.
		public const string PREFAB_HELP_POPUP = "Prefabs/SB_HelpPopup_Prefab";
		
		// Name of the 'AlertPopup_Prefab' prefab in the Resources folder.
		public const string PREFAB_CONTINUE_DRAWING_POPUP = "Prefabs/SB_ContinueDrawingPopup_Prefab";
		
		// Name of the 'SB_ConfirmResetDrawingPopup_Prefab' prefab in the Resources folder.
		public const string PREFAB_RESET_DRAWING_POPUP = "Prefabs/SB_ConfirmResetDrawingPopup_Prefab";

		// Store the current popup game object, and its script
		protected GameObject _popupGameObject;
		protected SB_BasePopup _popupScript;
		protected SB_PopupVO _popupVO;

		// Store the next popup type, if one is to be shown once the current popup is closed
		private int _nextPopupType = -1;
		private SB_PopupVO _nextPopupVO;
		
		// Setup delegates
		public delegate void PopupEventDelegate(SB_BasePopup popup, string eventType);
		public event PopupEventDelegate OnPopupEvent;

		// Getters
		public GameObject CurrentPopupObject	{	get {return _popupGameObject;}	}
		public SB_BasePopup CurrentPopupScript 	{	get {return _popupScript;} }
		//public SB_PopupVO CurrentPopupVO 		{	get {return _popupVO;}	}
		public int CurrentPopupType 			
		{
			get 
			{
				if(_popupVO != null)
				{
					return _popupVO.popupType;
				}

				return -1;
			}
		}

		/** Returns true if a popup is currently showing, or if a popup is waiting to be shown */
		public bool PopupIsShowing 
		{
			get 
			{
				// If popup exists, or there's one which will be shown next, return true
				if(_popupGameObject != null || _nextPopupType > -1)
				{
					return true;
				}

				return false;
			}
		}

		/**
		 * Show a popup.
		 * @int popupType The type of the popup to show (defined inside SB_PopupTypes class)
		 * @SB_PopupVO popupVO An optional Value Object, containing title and body text,
		 * 					which is applied when the popup has opened.
		 */
		public void ShowPopup(int popupType, SB_PopupVO popupVO=null)
		{
			log ("ShowPopup " + popupType);
			if(popupVO != null)
			{
				log ("ShowPopup popupVO: " + popupVO.ToString());
			}
			else
			{
				warn ("popupVO is null!");
			}

			if(_popupGameObject == null)
			{
				_popupVO = popupVO;
				
				switch(popupType)
				{
					case SB_PopupTypes.GATING_POPUP:
						if(_popupVO == null)
						{
							_popupVO = new SB_PopupVO(popupType,
							                          PREFAB_GATING_POPUP,
							                          SB_Utils.GetNodeValueFromConfigXML ("config/copy/gating_popup/parents/text", "title"),
							                          SB_Utils.GetNodeValueFromConfigXML ("config/copy/gating_popup/parents/text", "body"));
						}
						break;
						
					case SB_PopupTypes.GATING_POPUP_DELETE_PROFILE:
						if(_popupVO == null)
						{
							_popupVO = new SB_PopupVO(popupType,
							                          PREFAB_GATING_POPUP,
							                          SB_Utils.GetNodeValueFromConfigXML ("config/copy/gating_popup/profile_delete/text", "title"), 
							                          SB_Utils.GetNodeValueFromConfigXML ("config/copy/gating_popup/profile_delete/text", "body"));
						}
						break;
						
					case SB_PopupTypes.CONFIRM_DELETE_PROFILE_POPUP:
						if(_popupVO == null)
						{
							_popupVO = new SB_PopupVO(popupType,
							                          PREFAB_CONFIRM_DELETE_PROFILE_POPUP,
							                          SB_Utils.GetNodeValueFromConfigXML ("config/copy/confirm_profile_delete_popup/text", "title"), 
							                          SB_Utils.GetNodeValueFromConfigXML ("config/copy/confirm_profile_delete_popup/text", "body"));						
						}
						break;
						
					case SB_PopupTypes.GAME_PAUSED_POPUP:
						if(_popupVO == null)
						{
							_popupVO = new SB_PopupVO(popupType,
							                          PREFAB_GAME_PAUSED_POPUP,
							                          SB_Utils.GetNodeValueFromConfigXML ("config/copy/game_paused_popup/text", "title"), 
							                          SB_Utils.GetNodeValueFromConfigXML ("config/copy/game_paused_popup/text", "body"));
						}
						break;
					
					case SB_PopupTypes.GAME_QUIT_POPUP:
						if(_popupVO == null)
						{
							_popupVO = new SB_PopupVO(popupType,
						                          	  PREFAB_GAME_QUIT_POPUP,
							                          SB_Utils.GetNodeValueFromConfigXML ("config/copy/game_quit_popup/text", "title"), 
						                        	  SB_Utils.GetNodeValueFromConfigXML ("config/copy/game_quit_popup/text", "body"));
						}
						break;

					case SB_PopupTypes.CAMERA_POPUP:
						if( _popupVO == null ) 
						{
							_popupVO = new SB_PopupVO(popupType,
							                          PREFAB_CAMERA_POPUP,
							                          "","");
						}
						break;

					case SB_PopupTypes.ALERT_POPUP:
						if( _popupVO == null ) 
						{
							_popupVO = new SB_PopupVO(popupType,
							                          PREFAB_ALERT_POPUP,
							                          "","");
						}
						break;

					

				}
				
				if(_popupVO == null)
				{
					error ("_popupVO is null!!");
					return;
				}

				log ("***********************************************");
				log ("   >>>>>>  " + _popupVO.ToString());
				log ("***********************************************");

				log ("_popupVO.prefabType: " + _popupVO.prefabType);

				// Create a new instance of the popup prefab
				var objectPrefab = (GameObject)Resources.Load(_popupVO.prefabType, typeof(GameObject));
				
				// instantiate it
				_popupGameObject = (GameObject)Instantiate(objectPrefab);
				
				// Get the script associated to the popup
				switch(popupType)
				{
					case SB_PopupTypes.GATING_POPUP:
					case SB_PopupTypes.GATING_POPUP_DELETE_PROFILE:
						_popupScript = _popupGameObject.GetComponent<SB_GatingPopup>();
						break;
						
					case SB_PopupTypes.GAME_PAUSED_POPUP:
						_popupScript = _popupGameObject.GetComponent<SB_GamePausedPopup>();
						break;
						
					case SB_PopupTypes.GAME_HELP_POPUP:
						_popupScript = _popupGameObject.GetComponent<SB_BasePopup>();
						break;
					
					case SB_PopupTypes.GAME_QUIT_POPUP:
						_popupScript = _popupGameObject.GetComponent<SB_GameQuitPopup>();
						break;

					case SB_PopupTypes.CONFIRM_DELETE_PROFILE_POPUP:
						_popupScript = _popupGameObject.GetComponent<SB_ConfirmProfileDeletePopup>();
						break;
						
					case SB_PopupTypes.CONTINUE_DRAWING:
						_popupScript = _popupGameObject.GetComponent<SB_ContinueDrawingPopup>();
						break;

					#if USES_CAMERA
					case SB_PopupTypes.CAMERA_POPUP:
						_popupScript = _popupGameObject.GetComponent<SB_CameraPopup>();
						break;
					#endif

					default:
						_popupScript = _popupGameObject.GetComponent<SB_BasePopup>();
						warn ("ShowPopup() Unexpected popupType: " + popupType);
						break;
				}

				_popupScript.vo = _popupVO;
				
				AddDelegates();
			}
			else
			{
				// Wait for current popup to close, before opening the next one
				_nextPopupType = popupType;
				_nextPopupVO = popupVO;
				
				//log ("---------- Not opening popup '" + popupType + "' yet, waiting for current popup to close. _nextPopupVO: " + _nextPopupVO.ToString());
			}
		}
		
		protected virtual void AddDelegates()
		{
			if(_popupScript != null)
			{
				_popupScript.OnPopupStarted += OnPopupHandler;
				_popupScript.OnPopupOpen += OnPopupHandler;
				_popupScript.OnPopupClosed += OnPopupHandler;
				_popupScript.OnOptionChosen += OnPopupHandler;
				
				if(_popupScript is SB_GatingPopup)
				{
					// Cast the popup to type SB_GatingPopup 
					SB_GatingPopup popupScript = (SB_GatingPopup)_popupScript;
					popupScript.OnGatingSuccess += OnPopupHandler;
					popupScript.OnGatingFailed += OnPopupHandler;
				}
			}
			else
			{
				error ("AddPopupDelegates() - _popupScript is null, cannot add delegates");
			}
		}
		
		protected virtual void RemoveDelegates()
		{
			if(_popupScript != null)
			{
				_popupScript.OnPopupStarted -= OnPopupHandler;
				_popupScript.OnPopupOpen -= OnPopupHandler;
				_popupScript.OnPopupClosed -= OnPopupHandler;
				_popupScript.OnOptionChosen -= OnPopupHandler;
				
				if(_popupScript is SB_GatingPopup)
				{
					// Cast the popup to type SB_GatingPopup 
					SB_GatingPopup popupScript = (SB_GatingPopup)_popupScript;
					popupScript.OnGatingSuccess -= OnPopupHandler;
					popupScript.OnGatingFailed -= OnPopupHandler;
				}
			}
		}
		
		/**
		 * Handle events to popups
		 */
		protected virtual void OnPopupHandler (SB_BasePopup popup, string eventType)
		{
			//error("OnPopupHandler - eventType: " + eventType);
			
			switch(eventType)
			{
				case SB_BasePopup.POPUP_STARTED:
					
					if(_popupVO != null)
					{
						_popupScript.SetText(_popupVO.title, _popupVO.body);
					}
					else
					{
						warn ("Not setting popup text, _popupVO is null!");
					}
					break;
				case SB_BasePopup.POPUP_OPEN: break;			
				case SB_BasePopup.POPUP_CLOSED: 
					Destroy(_popupGameObject);
					RemoveDelegates();
					_popupGameObject = null;
					_popupScript = null;
					_popupVO = null;
					
					// Do we need to open another popup?
					if(_nextPopupType > -1)
					{
						ShowPopup(_nextPopupType, _nextPopupVO);
						_nextPopupType = -1;
						_nextPopupVO = null;
					}
				System.GC.Collect();
					break;
					
				case SB_GatingPopup.GATING_SUCCESS:
					_popupScript.PlayOutro();
					//_popupScript.OutroComplete();
					break;
			}
		
			// Call the delgate
			if(OnPopupEvent != null) OnPopupEvent(popup, eventType);
		}

		protected void log(object msg) 
		{
			Debug.Log("[" + this.GetType() + "] " + msg);
		}
		
		protected void warn(object msg) 
		{
			Debug.LogWarning("[" + this.GetType() + "] " + msg);
		}
		
		protected void error(object msg) 
		{
			Debug.LogError("[" + this.GetType() + "] " + msg);
		}

		public void OnDestroy()
		{
			warn ("REMOVING THE POPUP HANDLER REMOVING THE POPUP HANDLER REMOVING THE POPUP HANDLER REMOVING THE POPUP HANDLER REMOVING THE POPUP HANDLER REMOVING THE POPUP HANDLER REMOVING THE POPUP HANDLER ");
			RemoveDelegates();

			if(_popupGameObject != null)
			{
				Destroy(_popupGameObject);
				_popupGameObject = null;
			}
			
			if(_popupScript != null)
			{
				Destroy(_popupScript);
				_popupScript = null;
			}
			
			_popupVO = null;
			_nextPopupVO = null;
			OnPopupEvent = null;
			warn ("POPUP MANAGER DESTROYED");
		}
	}
}