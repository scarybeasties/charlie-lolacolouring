﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Base;
using ScaryBeasties.Controls;
using ScaryBeasties.Utils;
using UnityEngine.UI;
using ScaryBeasties.Data;

namespace ScaryBeasties.Popups
{
	public class SB_ContinueDrawingPopup : SB_BasePopup 
	{
		public const string OPTION_CONTINUE_DRAWING = "optionContinueDrawing";
		public const string OPTION_RESET_DRAWING = "optionResetDrawing";
		public const string OPTION_DELETE_DRAWING = "optionDeleteDrawing";
		public const string OPTION_DUPLICATE_DRAWING = "optionDuplicateDrawing";
		
		private GameObject _options;
		private GameObject _confirm;
		
		private string _optionChosen = "";
		
		private SB_Button _continueBtn;
		private SB_Button _resetBtn;
		private SB_Button _deleteBtn;
		private SB_Button _duplicateBtn;
		
		override protected void Awake()
		{
			base.Awake();
			_options = transform.Find("panel/options").gameObject;
			_confirm = transform.Find("panel/confirm").gameObject;
			
			_continueBtn = transform.Find("panel/options/continue_btn").gameObject.GetComponent<SB_Button>();
			_resetBtn = transform.Find("panel/options/reset_btn").gameObject.GetComponent<SB_Button>();
			_deleteBtn = transform.Find("panel/options/delete_btn").gameObject.GetComponent<SB_Button>();
			_duplicateBtn = transform.Find("panel/options/duplicate_btn").gameObject.GetComponent<SB_Button>();
			
			ShowOptions();
		}
		
		override protected void Start () 
		{
			base.Start();			
			
			ApplyExlusions(); // IMPORTANT - keep this call in Start()
		}
		
		// Set button labels
		override protected void Init()
		{
			base.Init ();
			
			_continueBtn.label = SB_Utils.GetNodeValueFromConfigXML("config/copy/continue_drawing_popup/text", "btn_continue");
			_resetBtn.label = SB_Utils.GetNodeValueFromConfigXML("config/copy/continue_drawing_popup/text", "btn_reset");
			_deleteBtn.label = SB_Utils.GetNodeValueFromConfigXML("config/copy/continue_drawing_popup/text", "btn_delete");
			_duplicateBtn.label = SB_Utils.GetNodeValueFromConfigXML("config/copy/continue_drawing_popup/text", "btn_duplicate");
			
			SB_Button yesBtn = transform.Find("panel/confirm/yes_btn").gameObject.GetComponent<SB_Button>();
			SB_Button noBtn = transform.Find("panel/confirm/no_btn").gameObject.GetComponent<SB_Button>();
			
			yesBtn.label = SB_Utils.GetNodeValueFromConfigXML("config/copy/continue_drawing_popup/text", "btn_yes");
			noBtn.label = SB_Utils.GetNodeValueFromConfigXML("config/copy/continue_drawing_popup/text", "btn_no");
		}
		
		override public void Close()
		{
			base.Close();
		}
		
		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			base.OnBtnHandler(btn, eventType);
			
			if(eventType == SB_Button.MOUSE_UP)
			{
				warn("btn.name: " + btn.name);
				
				switch(btn.name)
				{
					case "continue_btn":
						OptionChosen(OPTION_CONTINUE_DRAWING);
						PlayOutro();
					 	break;
						
					case "reset_btn":
						_optionChosen = OPTION_RESET_DRAWING;
						ShowConfirm(btn.label);
					break;
					
					case "delete_btn":
						_optionChosen = OPTION_DELETE_DRAWING;
						ShowConfirm(btn.label);
						break;
						
					case "duplicate_btn":
						OptionChosen(OPTION_DUPLICATE_DRAWING);
						PlayOutro();
						break;
					
					case "no_btn":
						_optionChosen = "";
						ShowOptions();
						break;
						
					case "yes_btn":
						OptionChosen(_optionChosen);
						PlayOutro();
						break;
				}
			}
		}
		
		private void ShowOptions()
		{
			_options.SetActive(true);
			_confirm.SetActive(false);
		}
		
		/**
		 * Check the vo.data object, to see if any key value data has been passed to it.
		 * This should be an arraylist of buttons to exclude
		 */
		private void ApplyExlusions ()
		{
			if(vo != null && vo.data != null)
			{
				KeyValueObject kv = (KeyValueObject)vo.data;
				if(kv.ContainsKey("ExcludeOptions"))
				{
					ArrayList excludeOptions = (ArrayList)kv.GetValue("ExcludeOptions");
					for(int n=0; n<excludeOptions.Count; n++)
					{
						string exclude = (string)excludeOptions[n];
						switch(exclude)
						{
							case OPTION_CONTINUE_DRAWING:
								_continueBtn.gameObject.SetActive(false);
								break;
								
							case OPTION_RESET_DRAWING:
								_resetBtn.gameObject.SetActive(false);
								break;
								
							case OPTION_DELETE_DRAWING:
								_deleteBtn.gameObject.SetActive(false);
								break;
								
							case OPTION_DUPLICATE_DRAWING:
								_duplicateBtn.gameObject.SetActive(false);
								break;
						}
					}
				}
			}
		}
		
		private void ShowConfirm(string btnLabel)
		{
			Text confirmTitle = transform.Find("panel/confirm/title").GetComponent<Text>();
			confirmTitle.text = btnLabel + "\n" + SB_Utils.GetNodeValueFromConfigXML("config/copy/continue_drawing_popup/text", "title_confirm");
			
			_options.SetActive(false);
			_confirm.SetActive(true);
		}
		
		override protected void OnDestroy()
		{
			base.OnDestroy();
			
			_options = null;
			_confirm = null;
			
			_continueBtn = null;
			_resetBtn = null;
			_deleteBtn = null;
			_duplicateBtn = null;
		}
	}
}