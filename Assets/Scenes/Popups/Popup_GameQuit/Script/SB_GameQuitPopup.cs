using UnityEngine;
using System.Collections;
using ScaryBeasties.Controls;
using ScaryBeasties.Base;

namespace ScaryBeasties.Popups
{
	public class SB_GameQuitPopup : SB_BasePopup 
	{
		public const string OPTION_CANCEL = "optionCancel";
		public const string OPTION_OK = "optionOk";

		override protected void Init()
		{
			base.Init();
			SetText("Are you sure?" , "");
		}

		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			base.OnBtnHandler(btn, eventType);
			
			//log("OnBtnHandler:" + btn.name + ", eventType: " + eventType);
			if(eventType == SB_Button.MOUSE_UP)
			{
				//log("OnBtnHandler:" + btn.name);
				
				switch(btn.name)
				{
				case "cancel_btn": 
					OptionChosen(OPTION_CANCEL);
					PlayOutro();
					break;
					
				case "ok_btn": 
					OptionChosen(OPTION_OK); 
					PlayOutro();
					break;
				}
			}
		}
	}
}