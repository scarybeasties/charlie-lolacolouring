﻿/**
 * Base Scene
 * -------------
 * 
 * Chris Thodesen
 * Scary Beasties Limited
 * September 2014
 * 
 * Description
 * ------------
 * Camera popup, for taking images with the camera.
 * 
 * Extends SB_BasePopup
 * 
 */ 

using UnityEngine;
using System.Collections;
using ScaryBeasties.Controls;
using ScaryBeasties.Base;
using ScaryBeasties.Utils;
using ScaryBeasties.Cam;
using ScaryBeasties.User;
using ScaryBeasties.Sound;

namespace ScaryBeasties.Popups
{
	public class SB_CameraPopup : SB_BasePopup
	{
		public const string PHOTO_SAVED = "photoSaved"; 

		#if USES_CAMERA
		private SB_CameraImage cameraImage;
		private SB_Camera _camera;
		private bool _okPressed = false;

		GameObject _closeBtn, _okBtn;

		override protected void Start()
		{
			base.Start();

			_closeBtn = transform.Find("panel/close_btn").gameObject;
			_okBtn = transform.Find("panel/ok_btn").gameObject;

			SetupCamera();

			_okPressed = false;

		}

		override protected void Update () 
		{
			base.Update();
		}

		void SetupCamera() 
		{
			GameObject pic = transform.Find ("panel/profile/pic").gameObject;
			_camera = pic.GetComponent<SB_Camera>();
			//pic..
			//pic.renderer.material 
		}

		override protected void Init()
		{
			base.Init ();
			if(_camera.available)
			{
				_camera.SetupCameraSize( 200, 240 );
				//_camera.SetupCameraSize( 220, 280 );
				SB_SoundManager.instance.PlayVoiceQueued("Localised/VO/Charlie/vo_profile_movearound");
			}
			else
			{
				OptionChosen(POPUP_CLOSED);
				PlayOutro();
			}
		}

		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			base.OnBtnHandler(btn, eventType);
			
			if(eventType == SB_Button.MOUSE_UP)
			{
				switch(btn.name) {
				case "ok_btn":
					if( _okPressed ) 
						return;

					_okPressed = true;
					SB_SoundManager.instance.ClearVOQueue();
					SB_SoundManager.instance.PlaySound("Audio/SFX/Games/General/SndCamera");
					
					SavePhotoAndLeave();
					break;
				case "rotate_clockwise_btn":
					
					break;
				case "rotate_anticlockwise_btn":
					
					break;
				case "flip_horizontal_btn":
					
					break;
				case "flip_vertical_btn":
					
					break;
				case "back_btn":
						OptionChosen(POPUP_CLOSED);
						PlayOutro();
					break;
				}
			}
		}

		private void SavePhotoAndLeave() 
		{
			print ("SavePhotoAndLeave()");
			
			GameObject picInner = transform.Find("panel/profile/pic_inner").gameObject;
			Rect portion = ScreenGrab.GetWorldGameObjectRect(picInner);
			print ("portion: " + portion);

			// Hide buttons, while pixels are being copied from screen
			_closeBtn.SetActive(false);
			_okBtn.SetActive(false);

			// Copy pixels from src into destination
			ScreenGrab.OnPixelsCopied += HandleOnPixelsAppliedToTexture;
			StartCoroutine(ScreenGrab.CopyPixelsIntoMesh(portion, picInner));
		}

		void HandleOnPixelsAppliedToTexture(Texture2D texture)
		{
			_closeBtn.SetActive(true);
			_okBtn.SetActive(true);

			ScreenGrab.OnPixelsCopied -= HandleOnPixelsAppliedToTexture;

			byte[] bytes = texture.EncodeToPNG();
			print ("bytes.Length: " + bytes.Length);
			ScreenshotManager.ImageFinishedSaving += ImageAssetSaved;

			// This only happens when testing the scene standalone, in the IDE!
			//if(SB_Globals.SELECTED_PROFILE_VO == null) return;
			if(SB_UserProfileManager.instance.CurrentProfile == null) return;

			string filename = "custom"+SB_UserProfileManager.instance.CurrentProfile.profileId;

			//StartCoroutine(ScreenshotManager.SaveExisting(bytes, "custom"+SB_Globals.SELECTED_PROFILE_VO.profileId, true));
			StartCoroutine(ScreenshotManager.SaveExistingNoGallery(bytes, filename, true));
		}
	
		void ImageAssetSaved(string path)
		{
			Debug.Log ("ImageAssetSaved() Finished saving to " + path);
			ScreenshotManager.ImageFinishedSaving -= ImageAssetSaved;

			PlayOutro();
			Destroy(_camera.gameObject);
			
			OptionChosen(PHOTO_SAVED);
		}

		override protected void OnDestroy()
		{
			base.OnDestroy();

			Destroy(_camera);
			_camera = null;

			Destroy(cameraImage);
			cameraImage = null;
		}
		#endif
	}
}