﻿namespace ScaryBeasties.Popups
{
	public class SB_AlertPopupVO: SB_PopupVO
	{
		protected bool _showCancelBtn = true;
		
		public SB_AlertPopupVO(int popupType, string prefabType, string title, string body, bool showCancelBtn=true) : base (popupType, prefabType, title, body)
		{
			_showCancelBtn = showCancelBtn;
		}
		
		// Getters
		public bool ShowCancelBtn{	get {return _showCancelBtn;}	}
		
		override public string ToString()
		{
			var str = base.ToString();
			str += ", ShowCancelBtn: " + ShowCancelBtn;
			
			return str;
		}
	}
}