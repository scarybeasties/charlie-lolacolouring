﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Base;
using ScaryBeasties.Controls;
using ScaryBeasties.Popups;
using ScaryBeasties.Utils;

public class SB_AlertPopup : SB_BasePopup 
{
	public const string OPTION_OK = "optionOK";

	private SB_Button _okBtn;
	private SB_Button _cancelBtn;

	Vector3 _okPos;
	Vector3 _cancelPos;
	
	override protected void Start () 
	{
		// Find the buttons
		_okBtn = transform.Find("panel/ok_btn").gameObject.GetComponent<SB_Button> ();
		_cancelBtn = transform.Find("panel/cancel_btn").gameObject.GetComponent<SB_Button> ();

		_okPos = _okBtn.transform.localPosition;
		_cancelPos = _cancelBtn.transform.localPosition;
		
		base.Start();
		
		SB_AlertPopupVO alertVO = (SB_AlertPopupVO)vo;
		_okBtn.gameObject.SetActive(true);
		
		if(alertVO != null)
		{
			_cancelBtn.gameObject.SetActive(alertVO.ShowCancelBtn);
		}
		
		// Set both buttons to their original positions
		_okBtn.transform.localPosition = _okPos;
		_cancelBtn.transform.localPosition = _cancelPos;
		
		// If no cancel button, move the 'ok' button to the center
		if(alertVO != null && !alertVO.ShowCancelBtn)
		{
			tk2dCameraAnchor anchor = _okBtn.transform.GetComponent<tk2dCameraAnchor>();
			if(anchor != null)
			{
				anchor.enabled = false;
			}
			
			_okBtn.transform.localPosition = new Vector3(0, _okPos.y, _okPos.z);
		}
	}
	// Set button labels
	override protected void Init()
	{
		base.Init ();
		
		SB_Button yesBtn = _okBtn.gameObject.GetComponent<SB_Button>();
		SB_Button noBtn = _cancelBtn.gameObject.GetComponent<SB_Button>();
		
		yesBtn.label = SB_Utils.GetNodeValueFromConfigXML("config/copy/alert_popup/text", "btn_yes");
		noBtn.label = SB_Utils.GetNodeValueFromConfigXML("config/copy/alert_popup/text", "btn_no");
		
		// If there's only 1 button showing, then set its label to 'ok'
		SB_AlertPopupVO alertVO = (SB_AlertPopupVO)vo;
		if(alertVO != null && !alertVO.ShowCancelBtn)
		{
			yesBtn.label = SB_Utils.GetNodeValueFromConfigXML("config/copy/alert_popup/text", "btn_ok");
		}
	}
	
	override protected void OnBtnHandler(SB_Button btn, string eventType)
	{
		base.OnBtnHandler(btn, eventType);
		
		//log("OnBtnHandler:" + btn.name + ", eventType: " + eventType);
		if(eventType == SB_Button.MOUSE_UP)
		{
			//log("OnBtnHandler:" + btn.name);
			
			switch(btn.name)
			{
				case "ok_btn":
					//LogButtonEvent("ok");
					OptionChosen(OPTION_OK);
					PlayOutro();
					break;
					
				case "cancel_btn":
					OptionChosen(OPTION_CLOSE);
					PlayOutro();
					break;
			}
		}
	}
	
	override protected void OnDestroy()
	{
		_okBtn = null;
		_cancelBtn = null;

		base.OnDestroy();
	}
}
