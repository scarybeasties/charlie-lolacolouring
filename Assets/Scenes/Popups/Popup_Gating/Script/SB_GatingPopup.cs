/**
 * Base Scene
 * -------------
 * 
 * George Tsappis
 * ScaryBeasties
 * July 2014
 * 
 * Description
 * ------------
 * Gating popup, for Parents Area.
 * 
 * Extends SB_BasePopup
 * 
 */ 

using UnityEngine;
using System.Collections;
using ScaryBeasties.Controls;
using ScaryBeasties.Base;
using ScaryBeasties.Utils;

namespace ScaryBeasties.Popups
{
	public class SB_GatingPopup : SB_BasePopup 
	{
		public const string GATING_SUCCESS = "gatingSuccess";
		public const string GATING_FAILED = "gatingFailed";

		// Setup delegates
		public delegate void GatingSuccessDelegate(SB_BasePopup popup, string eventType);
		public delegate void GatingFailedDelegate(SB_BasePopup popup, string eventType);
		public event GatingSuccessDelegate OnGatingSuccess;
		public event GatingFailedDelegate OnGatingFailed;

		// The minimum and maximum values to use for the sums
		private const int MIN_NUMBER = 5;
		private const int MAX_NUMBER = 20;

		private int _num1 = 0;
		private int _num2 = 0;
		private int _sum = 0;

		private const int MAX_NUM_TRIES = 3;
		private int _numTries = 0;

		// Operator type, for the sum.
		// 0 = addition
		// 1 = subtraction
		private int _operation = 0;

		// Store the text mesh objects
		private SB_TextField _num1Text;
		private SB_TextField _num2Text;
		private SB_TextField _operatorText;
		private SB_TextField _equalsText;
		private SB_TextField _sumText;

		private SB_Button _input1;
		private SB_Button _input2;
		private SB_Button _input3;
		private SB_Button _input4;
		private SB_Button _input5;
		private SB_Button _input6;
		private SB_Button _input7;
		private SB_Button _input8;
		private SB_Button _input9;

		override protected void Start()
		{
			// Get the text mesh objects
			_num1Text 		= transform.Find("panel/question/num1").GetComponent<SB_TextField>();
			_num2Text 		= transform.Find("panel/question/num2").GetComponent<SB_TextField>();
			_operatorText 	= transform.Find("panel/question/operator").GetComponent<SB_TextField>();
			_equalsText 	= transform.Find("panel/question/equals").GetComponent<SB_TextField>();
			_sumText 		= transform.Find("panel/question/sum").GetComponent<SB_TextField>();

			base.Start();
		}

		
		override protected void Init()
		{
			base.Init ();
		}

		override public void IntroStarted()
		{
			base.IntroStarted();

			_input1	= transform.Find("panel/input_buttons/input_1").GetComponent<SB_Button>();
			_input2	= transform.Find("panel/input_buttons/input_2").GetComponent<SB_Button>();
			_input3	= transform.Find("panel/input_buttons/input_3").GetComponent<SB_Button>();
			_input4	= transform.Find("panel/input_buttons/input_4").GetComponent<SB_Button>();
			_input5	= transform.Find("panel/input_buttons/input_5").GetComponent<SB_Button>();
			_input6	= transform.Find("panel/input_buttons/input_6").GetComponent<SB_Button>();
			_input7	= transform.Find("panel/input_buttons/input_7").GetComponent<SB_Button>();
			_input8	= transform.Find("panel/input_buttons/input_8").GetComponent<SB_Button>();
			_input9	= transform.Find("panel/input_buttons/input_9").GetComponent<SB_Button>();

			// Get numbers from config.xml (some countries might need numbers localised!)
			_input1.label = SB_Utils.GetNodeValueFromConfigXML ("config/copy/gating_popup/numbers/text", "1");
			_input2.label = SB_Utils.GetNodeValueFromConfigXML ("config/copy/gating_popup/numbers/text", "2");
			_input3.label = SB_Utils.GetNodeValueFromConfigXML ("config/copy/gating_popup/numbers/text", "3");
			_input4.label = SB_Utils.GetNodeValueFromConfigXML ("config/copy/gating_popup/numbers/text", "4");
			_input5.label = SB_Utils.GetNodeValueFromConfigXML ("config/copy/gating_popup/numbers/text", "5");
			_input6.label = SB_Utils.GetNodeValueFromConfigXML ("config/copy/gating_popup/numbers/text", "6");
			_input7.label = SB_Utils.GetNodeValueFromConfigXML ("config/copy/gating_popup/numbers/text", "7");
			_input8.label = SB_Utils.GetNodeValueFromConfigXML ("config/copy/gating_popup/numbers/text", "8");
			_input9.label = SB_Utils.GetNodeValueFromConfigXML ("config/copy/gating_popup/numbers/text", "9");

			Vector3 labelPosition = new Vector3(0, 0, 1);
			_input1.LabelGameObject.transform.localPosition = labelPosition;
			_input2.LabelGameObject.transform.localPosition = labelPosition;
			_input3.LabelGameObject.transform.localPosition = labelPosition;
			_input4.LabelGameObject.transform.localPosition = labelPosition;
			_input5.LabelGameObject.transform.localPosition = labelPosition;
			_input6.LabelGameObject.transform.localPosition = labelPosition;
			_input7.LabelGameObject.transform.localPosition = labelPosition;
			_input8.LabelGameObject.transform.localPosition = labelPosition;
			_input9.LabelGameObject.transform.localPosition = labelPosition;


			CreateSum ();
		}
		
		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			base.OnBtnHandler(btn, eventType);
			
			if(eventType == SB_Button.MOUSE_UP)
			{
				switch(btn.name)
				{
					case "input_1": numberChosen("1"); break;
					case "input_2": numberChosen("2"); break;
					case "input_3": numberChosen("3"); break;
					case "input_4": numberChosen("4"); break;
					case "input_5": numberChosen("5"); break;
					case "input_6": numberChosen("6"); break;
					case "input_7": numberChosen("7"); break;
					case "input_8": numberChosen("8"); break;
					case "input_9": numberChosen("9"); break;

					//case "submit_btn":
					//				ValidateSum();
					//				break;
				}
			}
		}

		protected void numberChosen(string num)
		{
			// If user has already selected a number, and is waiting for
			// validation, ignore any other chosen number
			if(_num2Text.text != "?") return;

			_num2Text.text = num;
			StartCoroutine(WaitThenValidateSum());
		}

		protected IEnumerator WaitThenValidateSum()
		{
			yield return new WaitForSeconds(0.8f);
			ValidateSum();
		}


		private void CreateSum()
		{
			// Choose a random number, to determine what sum operator to use
			_operation = Random.Range(0, 2);

			if(_operation == 0)
			{
				// Addition
				_num1 = Random.Range(MIN_NUMBER, MAX_NUMBER);
				_num2 = Random.Range(1, 9);
				_sum = _num1 + _num2;

				print (_num1 + " + " + _num2 + " = " + _sum);

				_operatorText.text = "+";
			}
			else
			{
				// Subtraction
				_num1 = Random.Range(5, 20);
				_num2 = Random.Range(1, _num1-1);
				
				//  Make sure the 2nd number is not too large!
				if (_num2 > 9) {_num2 = 9;}
				_sum = _num1 - _num2;
				
				print (_num1 + " - " + _num2 + " = " + _sum);

				_operatorText.text = "-";
			}
			
			_num1Text.text = _num1.ToString();
			_num2Text.text = "?";
			_equalsText.text = "=";
			_sumText.text = _sum.ToString();

		}

		/**
		 * Check if user has submitted the correct answer
		 */
		private void ValidateSum ()
		{
			if(_num2Text.text == _num2.ToString())
			{
				//log("CORRECT!");

				// Call the delgate
				if(OnGatingSuccess != null) OnGatingSuccess(this, GATING_SUCCESS);
			}
			else
			{
				_numTries++;
				if(_numTries >= MAX_NUM_TRIES)
				{
					// User has exceeded maximum number of tries.  Force close the popup
					OptionChosen(OPTION_CLOSE);
					PlayOutro();
					return;
				}

				//log("INCORRECT!");
				//Handheld.Vibrate ();

				// Create a new sum
				CreateSum();

				// Call the delgate
				if(OnGatingFailed != null) OnGatingFailed(this, GATING_FAILED);
			}
		}
		
		override protected void OnDestroy()
		{
			base.OnDestroy();

			OnGatingSuccess = null;
			OnGatingFailed = null;
			
			_num1Text = null;
			_num2Text = null;
			_operatorText = null;
			_equalsText = null;
			_sumText = null;
			_input1 = null;
			_input2 = null;
			_input3 = null;
			_input4 = null;
			_input5 = null;
			_input6 = null;
			_input7 = null;
			_input8 = null;
			_input9 = null;
		}
	}
}