using UnityEngine;
using System.Collections;
using ScaryBeasties.Controls;
using ScaryBeasties.Base;
using ScaryBeasties.Utils;

namespace ScaryBeasties.Popups
{
	public class SB_ConfirmResetDrawingPopup : SB_BasePopup 
	{
		public const string OPTION_CANCEL = "resetDrawing_optionCancel";
		public const string OPTION_OK = "resetDrawing_OptionOk";

		override protected void Init()
		{
			base.Init();
			//SetText("Reset Drawing\nAre you sure...?" , "");
			
			string title = SB_Utils.GetNodeValueFromConfigXML ("config/copy/confirm_reset_drawing_popup/text", "title");
			string body = SB_Utils.GetNodeValueFromConfigXML ("config/copy/confirm_reset_drawing_popup/text", "body");
			SetText(title, body);
		}

		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			base.OnBtnHandler(btn, eventType);
			
			//log("OnBtnHandler:" + btn.name + ", eventType: " + eventType);
			if(eventType == SB_Button.MOUSE_UP)
			{
				//log("OnBtnHandler:" + btn.name);
				
				switch(btn.name)
				{
				case "cancel_btn":
					Close();
					break;
					
				case "ok_btn": 
					OptionChosen(OPTION_OK); 
					PlayOutro();
					break;
				}
			}
		}
	}
}