using UnityEngine;
using System.Collections;
using ScaryBeasties.Controls;
using ScaryBeasties.Base;
using ScaryBeasties.Sound;
using ScaryBeasties.Utils;
using ScaryBeasties.UI;

namespace ScaryBeasties.Popups
{
	public class SB_GamePausedPopup : SB_BasePopup 
	{
		public const string OPTION_MUSIC = "optionMusic";
		public const string OPTION_PARENTS_AREA = "optionParentsArea";
		public const string OPTION_HOW_TO_PLAY = "optionHowToPlay";
		public const string OPTION_MENU = "optionMenu";
		public const string OPTION_RESET_DRAWING = "optionResetDrawing";

		private tk2dSprite _musicBtnSprite;
		private SB_Button _musicBtn;
		private SB_Button _grownUpsBtn;
		private SB_Button _helpBtn;
		private SB_Button _menuBtn;
		private SB_Button _resetBtn;
		private SB_Button _continueBtn;

		override protected void Init()
		{
			base.Init ();

			// Any buttons in the popup which require anchoring must be applied via script.
			// This is because anchoring to the game camera in a popup prefab won't work (camera does not get applied)
			/*
			tk2dCameraAnchor closeButtonAnchor = transform.Find("close_btn").GetComponent<tk2dCameraAnchor>();
			tk2dCamera tk2dCamera = GameObject.Find("GameCamera/tk2dCamera").GetComponent<tk2dCamera>();
			closeButtonAnchor.AnchorCamera = tk2dCamera.camera;
			closeButtonAnchor.AnchorPoint = tk2dSprite.Anchor.UpperLeft;
			closeButtonAnchor.AnchorOffsetPixels = new Vector2(60f, -30f);
			closeButtonAnchor.AnchorToNativeBounds = true;
			*/

			_musicBtnSprite = transform.Find("panel/music_btn/Sprite").GetComponent<tk2dSprite>();
			UpdateMusicButton();

			// Find the buttons
			_musicBtn	 = transform.Find("panel/music_btn").gameObject.GetComponent<SB_Button>();
			_grownUpsBtn = transform.Find("panel/parentsarea_btn").gameObject.GetComponent<SB_Button>();
			_helpBtn 	= transform.Find("panel/howtoplay_btn").gameObject.GetComponent<SB_Button>();
			_menuBtn 	= transform.Find("panel/menu_btn").gameObject.GetComponent<SB_Button>();
			_resetBtn 	= transform.Find("panel/reset_btn").gameObject.GetComponent<SB_Button>();
			_continueBtn 	= transform.Find("panel/continue_btn").gameObject.GetComponent<SB_Button>();

			// Set the dynamic text
			_musicBtn.label = SB_Utils.GetNodeValueFromConfigXML ("config/buttons/button", "music");
			_grownUpsBtn.label = SB_Utils.GetNodeValueFromConfigXML ("config/buttons/button", "grownups");
			_helpBtn.label = SB_Utils.GetNodeValueFromConfigXML ("config/buttons/button", "help");
			_menuBtn.label = SB_Utils.GetNodeValueFromConfigXML ("config/buttons/button", "menu");
			_resetBtn.label = SB_Utils.GetNodeValueFromConfigXML ("config/buttons/button", "resetdrawing");
			_continueBtn.label = SB_Utils.GetNodeValueFromConfigXML ("config/buttons/button", "continue");
		}

		/**
		 * Toggle the sprite on the Music on/off button
		 */
		private void UpdateMusicButton()
		{
			print ("UpdateMusicButton SB_SoundManager.instance.muted: " + SB_SoundManager.instance.muted);

			if(!SB_SoundManager.instance.muted)
			{
				_musicBtnSprite.SetSprite("main_ui_btn_music_on");
			}
			else
			{
				_musicBtnSprite.SetSprite("main_ui_btn_music_off");
			}


		}

		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			base.OnBtnHandler(btn, eventType);

			//log("OnBtnHandler:" + btn.name + ", eventType: " + eventType);
			if(eventType == SB_Button.MOUSE_UP)
			{
				//log("OnBtnHandler:" + btn.name);
				
				switch(btn.name)
				{
					case "music_btn":
									SB_SoundManager.instance.muted = !SB_SoundManager.instance.muted; 
									UpdateMusicButton();
									OptionChosen(OPTION_MUSIC);

									SB_UIControls.instance.UpdateSoundButtons();
									break;

					case "parentsarea_btn":
									
									LogButtonEvent("pausepopup_shop");
									OptionChosen(OPTION_PARENTS_AREA); 
									PlayOutro();
									
									break;

					case "howtoplay_btn":	
						
									
									LogButtonEvent("pausepopup_howtoplay");
									OptionChosen(OPTION_HOW_TO_PLAY); 
									PlayOutro();
									
									break;

					case "menu_btn":
									LogButtonEvent("pausepopup_menu");
									OptionChosen(OPTION_MENU);
									PlayOutro();
									break;
					
					case "reset_btn":
									LogButtonEvent("pausepopup_resetdrawing");
									OptionChosen(OPTION_RESET_DRAWING);
									PlayOutro();
									break;

					case "continue_btn":
									OptionChosen(OPTION_CLOSE);
									PlayOutro();
									break;
				}
			}
		}

		override protected void OnDestroy()
		{
			_musicBtnSprite=null;
			_musicBtn=null;
			_grownUpsBtn=null;
			_helpBtn=null;
			_menuBtn=null;
			_resetBtn=null;
			_continueBtn=null;
			
			base.OnDestroy();
		}
	}
}