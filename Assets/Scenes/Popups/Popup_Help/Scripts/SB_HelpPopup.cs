﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Base;
using ScaryBeasties.Controls;
using ScaryBeasties.Utils;
using UnityEngine.UI;

namespace ScaryBeasties.Popups
{
	public class SB_HelpPopup : SB_BasePopup 
	{

		
		// Set button labels
		override protected void Init()
		{
			base.Init ();

			Text dragTxt = transform.Find ("panel/DragHelp/label").GetComponent<Text> ();
			dragTxt.text= SB_Utils.GetNodeValueFromConfigXML("config/copy/colouring/text", "drag_help");

			Text tapTxt = transform.Find ("panel/TapHelp/label").GetComponent<Text> ();
			tapTxt.text= SB_Utils.GetNodeValueFromConfigXML("config/copy/colouring/text", "tap_help");

			Text pinchTxt = transform.Find ("panel/PinchHelp/label").GetComponent<Text> ();
			pinchTxt.text= SB_Utils.GetNodeValueFromConfigXML("config/copy/colouring/text", "pinch_help");


		}
	}
}
