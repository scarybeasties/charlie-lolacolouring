﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShopInfoMessage : MonoBehaviour 
{
	private string _msg = "";
	
	void Start()
	{
		SetText(_msg);
	}
	
	void OnEnable()
	{
		SetText(_msg);
	}
	
	public void SetText(string msg="", int posY=12)
	{
		_msg = msg;
		
		Transform txtTrans = transform.Find("text");
		
		if(txtTrans && txtTrans.GetComponent<Text>() != null)
		{
			Text canvasText = txtTrans.GetComponent<Text>();
			canvasText.text = _msg;
			
			Vector3 txtPos = txtTrans.localPosition;
			txtPos.y = posY;
			txtTrans.localPosition = txtPos;
		}
	}
}
