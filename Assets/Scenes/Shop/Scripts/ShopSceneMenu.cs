﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ScaryBeasties.Controls;
using ScaryBeasties.Base;
using ScaryBeasties.History;
using ScaryBeasties.Popups;
using ScaryBeasties.UI;
using ScaryBeasties.Utils;
using ScaryBeasties.Sound;
using OnePF;
using ScaryBeasties.Cam;
using UnityEngine.UI;
using ScaryBeasties.SB2D;
using System;
using System.IO;
using ScaryBeasties.InAppPurchasing.Local;
using ScaryBeasties.InAppPurchasing.PackDownloader;
using ScaryBeasties.InAppPurchasing.Shop;
using ScaryBeasties.Data;
using ScaryBeasties.User;
using ScaryBeasties.UI.SwipeMenu;
using ScaryBeasties.DrWho.Shop;

namespace ScaryBeasties.Scenes.Shop
{
	public class ShopSceneMenu : ShopSceneBase
	{
		const string SHOP_LAST_SEEN_NEW_ITEMS_VERSION_KEY = "LastSeenNewItemsVersion";
		
		public const int PAGESET_STORE = 0;
		public const int PAGESET_MY_PURCHASES = 1;
		public const string KEY_MENU_DEEPLINK = "MenuDeeplinkToPage";
		
		const string SHOP_IMAGE_CACHE_EXPIRY_KEY = "ShopImagesExpiryDate";
		
		private SwipeMenu _menu;
		
		private bool _menuRefreshRequired = false;
		private Text _shopMessageText;

		private ArrayList _storeItemsPageSet;
		private ArrayList _myPurchasedItemsPageSet;

		private SB_Button _restorePurchasesBtn;

		private ShopItemDetails _details;

		private bool _detailsShowing = false;
		private SB_Button _detailBackBtn;
		private SB_Button _buyBtn;
		
		override protected void Start()
		{
			base.Start();

			Text title = transform.Find("title/label").GetComponent<Text>();
			string titleTxt = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/store/text", "title");
			//string titleTxt = SB_Utils.GetNodeValueFromConfigXML ("config/copy/shop/my_purchases/text", "title");
			title.text = SB_Utils.SanitizeString(titleTxt);
			
			_menu = transform.Find("SwipeMenu").gameObject.GetComponent<SwipeMenu>();
			_shopMessageText = transform.Find("Message").GetComponent<Text>();
			
			_restorePurchasesBtn = transform.Find("restore_purchases_btn").GetComponent<SB_Button>();
			_restorePurchasesBtn.label = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/buttons/button", "shop_iap_restore");
			
			#if UNITY_ANDROID
			// This button is not used for Android
			ShowRestorePurchcasesButton(false);
			#endif

			_details = transform.Find("ShopItemDetails").gameObject.GetComponent<ShopItemDetails>();
			_details.transform.position = new Vector2(0, -1000);

			_detailBackBtn = transform.Find("ShopItemDetails/shop_back_btn").GetComponent<SB_Button>();
			_buyBtn = transform.Find("ShopItemDetails/buy_btn").GetComponent<SB_Button>();
			//_detailBackBtn.gameObject.SetActive (false);
			//_buyBtn.gameObject.SetActive (false);
			//ShowRestorePurchcasesButton(false);
			
			// Delete any downloaded shop images, if 'now' is greater than our cache timeout value
			CheckCachedShopImages();

			// Do not load store xml yet. Wait until Init()
			// Might be best just to load the XML once, per session, instead of everytime the scene is loaded?
			
			// ---- GT - BODGE 
			// This code is only required because we are not choosing a user profile through the user profile select scene...
			if(SB_UserProfileManager.instance.CurrentProfile == null)
			{
				SB_UserProfileManager.instance.CurrentProfile = SB_UserProfileManager.instance.GetProfileVO(0);
			}
		}

		/**
		 * Pause the swipe menu, whenever a popup is showing (the app calls Pause() whenever a popup is shown)
		 */
		override public void Pause(bool val)
		{
			base.Pause(val);

			if(_menu != null)
			{
				_menu.Enable(!val);
			}
		}

		override protected void Update()
		{
			if(_menu != null)
			{
				if(_popupManager.CurrentPopupObject != null || _shopInfoMessageVisible)
				{
					_menu.Paused = true;
				}
				else if(_popupManager.CurrentPopupObject == null && !_shopInfoMessageVisible)
				{
					_menu.Paused = false;
				}
			}

			base.Update();
		}

		override protected void OnIAPListReady()
		{
			base.OnIAPListReady();
			StoreLoaded();
		}
		
		/*
		 * Store Initialisation
		 */
		private void StoreLoaded()
		{
			Debug.LogError("************** STORE LOADED **************");
			for(int i=0; i<SB_IAPManager.instance.IAPItemCount(); i++)
			{	
				SB_IAPItem iapItem = SB_IAPManager.instance.GetIAPItemAtIndex(i);
				//Debug.LogError("Item ["+i+"]" + iapItem.ToString());				
			}
			
			SetupShopMenu();
			SetupMyPurchasedPacksMenu();
			
			int menuPageset = PAGESET_STORE;
			
			// Check for Deeplink to menu page
			if(SCENE_CONFIG_OBJECT != null)
			{
				if(SCENE_CONFIG_OBJECT.GetValue(KEY_MENU_DEEPLINK) != null)
				{
					menuPageset = (int)SCENE_CONFIG_OBJECT.GetValue(KEY_MENU_DEEPLINK);
				}
			}
			_menu.ResetToPage(menuPageset);

			AddListeners();
			
			error("_menu.CurrentPage.pageSetID: "+_menu.CurrentPage.pageSetID);
			HandleMenuPageSetChanged(_menu.CurrentPage.pageSetID);
			
			ShowInfoMessage(false);
			
			// Validate files, but only handle errors - ignore if everything's ok.
			#if UNITY_ANDROID
			//ValidateFiles(true); // <- Might need to do this for Android users, incase they've deleted any files
			#endif
			
			if(SB_IAPManager.instance.ShopConfig != null)
			{
				PlayerPrefs.SetInt(SHOP_LAST_SEEN_NEW_ITEMS_VERSION_KEY, SB_IAPManager.instance.ShopConfig.NewItemsVersion);
			}
		}
		
		
		/**
		 * OK button was pressed on info popup.
		 * Do different things, depening on current shop state.
		 */
		/*
		void InfoPopupOKPressed()
		{
			if (_shopState == STATE_RESTORE_FAILED)
			{
				// Retry to restore purchases
				RestorePurchases();
			}
			else if(_failedPacks != null && _failedPacks.Count > 0)
			{
				// Try downloading failed packs
				//DownloadFailedPacks();
				_shopPackDownloader.DownloadFailedPacks(_failedPacks);
			}
			else
			{
				//CloseInfoPopup(_menuRefreshRequired);
				ShowInfoMessage(false);
			}
		}
		*/
		
		/**
		 * Item buying
		 */
		override protected void BuyItem(SB_IAPItem item)
		{
			base.BuyItem(item);
		}

		override protected void StartPurchase()
		{
			base.StartPurchase();
		}

		protected override void ItemAlreadyBought()
		{
			base.ItemAlreadyBought();
			//UpdateMenu ();
			HideDetails();	
			string msg = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/store/text", "msg_already_bought"); 
			ShowAlertPopup(msg, "", false);
			ShowInfoMessage(false);

			UpdateMenu();

		}

		override protected void PurchasedSuccess(string sku)
		{
			base.PurchasedSuccess(sku);
			SB_PackManager.instance.CheckDownloadedPacks();

			HideDetails();

			UpdateMenu();
		}
		
		override protected void PurchaseCancelled()
		{
			base.PurchaseCancelled();
		}

		override protected void RestorePurchases()
		{
			base.RestorePurchases();
		}
		
		override protected void RestoreSuccess()
		{
			base.RestoreSuccess();
		}
		
		void ShowRestorePurchcasesButton(bool val)
		{
			// This button is not used for Android
			#if UNITY_ANDROID
			val = false;
			#endif
			
			_restorePurchasesBtn.gameObject.SetActive(val);
		}
		
		/*
		 * Button handling
		 */
		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			if(eventType == SB_Button.MOUSE_UP)
			{
				warn("SB_Button.MOUSE_UP btn.name: "+btn.name);
				
				// TEST!!!
				//ImmersiveModeEnabler.HideNavBar();

				if(_detailsShowing)
				{
					if(btn == _detailBackBtn)
					{
						HideDetails();
					}
					else if(btn == _buyBtn)
					{
						if(_details.currentItem != null)
						{
							Dictionary<string, string> trackingParams =	new Dictionary<string, string>() 
							{
								{"SKU", _details.currentItem.sku}
							};
							LogButtonEvent("BuyItem", trackingParams);
							BuyItem(_details.currentItem);
						}
					}
				}
				else
				{
					switch(btn.name)
					{
					case "restore_purchases_btn":
						LogButtonEvent("RestorePurchases");
						RestorePurchases();
						break;
					}
				}
				
				switch(btn.name)
				{
				// A test button, so we can see what we've purchased in the output logs
				case "query_inventory_btn":
					_openIABWrapper.QueryInventory();
					break;
						
				case SB_UIControls.BTN_PARENTS:
					LogButtonEvent("Parents");
						
						// Users who are in the shop have already passed the gating popup, so go straight to the parents area.
					LoadScene(SB_Globals.SCENE_PARENTS_AREA, null, true);
						//ShowPopup(SB_PopupTypes.GATING_POPUP);
					break;
				}
			}
		}

		void HandleConfirmYes()
		{
			switch(_shopState)
			{
			case STATE_RESTORE_FAILED:
				RestorePurchases();
				break;
			}
		}
		
		/**
		 * Handle presses to SwipeMenu items
		 */
		private void MenuItemSelected(SwipeMenuItem item)
		{
			ShopMenuItem shopMenuItem = (ShopMenuItem)item;
			warn("MenuItemSelected() shopMenuItem:"+shopMenuItem.id);
			if(shopMenuItem.iapItemData != null)
			{
				// Shop IAP item was pressed (item not yet purchased)
				/*warn ("** iapItemData" + shopMenuItem.iapItemData.ToString());
				
				KeyValueObject kv = new KeyValueObject ();
				kv.SetKeyValue ("iapItem", shopMenuItem.iapItemData);
				
				LoadScene(SB_Globals.SCENE_SHOP_PRODUCT_DETAIL, kv, true);*/

				if(shopMenuItem.iapItemData.toDownload)
				{
					foreach(string packId in shopMenuItem.iapItemData.packIDs)
					{
						DownloadPack(packId);
					}
				}
				else
				{
					ShowDetails(shopMenuItem.iapItemData);
				}

			}
			else if(shopMenuItem.packItemData != null)
			{
				// Product item was pressed, which user has already purchased
				warn("** packItemData"+shopMenuItem.packItemData.ToString());
			}
		}

		public void ShowDetails(SB_IAPItem iapData)
		{
			Dictionary<string, string> trackingParams =	new Dictionary<string, string>() 
			{
				{"SKU", iapData.sku}
			};
			LogSceneEvent("ShowDetails", trackingParams);
			
			if(iapData.type == SB_IAPItem.TYPE_COMING_SOON)
				return;
			
			//_detailBackBtn.gameObject.SetActive (true);
			//_buyBtn.gameObject.SetActive (true);
			transform.Find("title/label").gameObject.SetActive(false);
			_menu.Enable(false);
			_menu.gameObject.SetActive(false);
			//_details.gameObject.SetActive (true);
			_details.transform.position = new Vector2(0, 0);
			_details.SetupProductDetails(iapData);
			_detailsShowing = true;

			ShowRestorePurchcasesButton(false);

		}

		public void HideDetails()
		{
			//_detailBackBtn.gameObject.SetActive (false);
			//_buyBtn.gameObject.SetActive (false);
			transform.Find("title/label").gameObject.SetActive(true);
			_menu.Enable(true);
			_menu.gameObject.SetActive(true);
			_details.transform.position = new Vector2(0, -1000);
			_detailsShowing = false;

			ShowRestorePurchcasesButton(true);
			_shopState = STATE_READY;
		}
		
		void HandleMenuPageSetChanged(int pageSet)
		{
			_shopMessageText.text = "";
			string titleTxt = "";
			if(pageSet == PAGESET_STORE)
			{
				LogSceneEvent("MenuPage_Store");
				
				titleTxt = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/store/text", "title");
				//ShowRestorePurchcasesButton(false);
				
				if(_storeItemsPageSet.Count == 0)
				{
					LogSceneEvent("MenuPage_Store_no_items_available");
					_shopMessageText.text = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/store/text", "no_items_available"); 
				}
			}
			else if(pageSet == PAGESET_MY_PURCHASES)
			{
				LogSceneEvent("MenuPage_MyPurchases");
			
				titleTxt = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/my_purchases/text", "title");
				//ShowRestorePurchcasesButton(true);
				
				if(_myPurchasedItemsPageSet.Count == 0)
				{
					LogSceneEvent("MenuPage_MyPurchases_no_items_available");
					_shopMessageText.text = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/my_purchases/text", "no_items_available"); 
				}
			}
			
			Text title = transform.Find("title/label").GetComponent<Text>();
			title.text = SB_Utils.SanitizeString(titleTxt);
		}

		void DownloadFailedPacks(KeyValueObject failedPacks)
		{
			if(failedPacks != null)
			{
				foreach(string packId in failedPacks.Keys)
				{
					// Delete the pack xml file before re-downloading the pack.
					SB_LocalPackManager.instance.DeletePackXML(packId);
					DownloadPack(packId);
				}
			}
		}
		
		/*
		void DeletePack (SB_PackItem pack)
		{
			Debug.LogError ("DELETE PACK " + pack.ToString());
			SB_LocalPackManager.instance.DeletePack(pack.packID);
			ShowCarousels();
		}
		
		
		void InstallPack (SB_PackItem pack)
		{
			Debug.LogError ("InstallPack " + pack.ToString());
			DownloadPack(pack.packID);
		}
		*/
		
		/**
		 * Handle events to popups
		 */
		override protected void HandleOnPopupEvent(SB_BasePopup popup, string eventType)
		{
			base.HandleOnPopupEvent(popup, eventType);
			
			error("HandleOnPopupEvent - SHOP STATE: "+_shopState);
			switch(eventType)
			{
			case SB_GatingPopup.GATING_SUCCESS:
					//LoadScene(SB_Globals.SCENE_PARENTS_AREA, null, true);
				break;
					
			case SB_AlertPopup.OPTION_OK: 
				if(_shopState == STATE_FILE_VALIDATION_FAILED && _failedPacks != null && _failedPacks.Count > 0)
				{
					// Try downloading failed packs
					DownloadFailedPacks(_failedPacks);
					//_shopPackDownloader.DownloadFailedPacks(_failedPacks);
				}
				break;
					
			case SB_AlertPopup.OPTION_CLOSE: 
				if(_shopState == STATE_FILE_VALIDATION_FAILED)
				{
					_shopState = STATE_READY;
				}
				break;
			}
		}
		
		override protected void PackDownloadSuccess()
		{
			base.PackDownloadSuccess();
			SB_PackManager.instance.CheckDownloadedPacks();
			UpdateMenu();
			HideDetails();
		}

		protected override void UpdateMenu()
		{

			base.UpdateMenu();

			_menu.ClearMenu();
		//	_menu.SetExtents ();
			SetupShopMenu();
		//	_menu.SetExtents ();
			SetupMyPurchasedPacksMenu();
		//	_menu.SetExtents ();
			_menu.ResetToPage (0);

			// If no items available (all bought..) then go to menu page 1
			/*if(_storeItemsPageSet.Count == 0)
			{
				_menu.ResetToPage(PAGESET_MY_PURCHASES);
			}
			else
			{
				_menu.ResetToPage(PAGESET_STORE);
			}*/
			
			HandleMenuPageSetChanged(_menu.CurrentPage.pageSetID);
		}
		
		private void SetupShopMenu()
		{
			warn("********* SetupShopMenu");
			warn("********* SB_IAPManager.instance.IAPItemCount() "+SB_IAPManager.instance.IAPItemCount());
			
			_storeItemsPageSet = new ArrayList();
			
			for(int i=0; i<SB_IAPManager.instance.IAPItemCount(); i++)
			{
				warn("********* Creating new item");
				
				// Only add unpurchased items to this pageset
				bool addItemToMenu = false;
				

				// For free items, we need to check the userprofile, to see if the item exists in the user's installedIAPPacks key value object 
				/*if(iapItem.isFree)
				{
					foreach(string packid in iapItem.packIDs)
					{
						if(SB_UserProfileManager.instance.CurrentProfile.installedIAPPacks.ContainsKey(packid))
						{
							warn("Free IAP '" + iapItem.sku + "' has already been downloaded");
							// User has already downloaded the 'free' iap item
							//iapItem.purchased = true;

							SB_IAPManager.instance.GetIAPItemAtIndex(i).purchased = true;
							
							//SB_PackItem packItem = SB_PackManager.instance.GetPackItemWithID(packid);
							//packItem.purchased = true;
							//packItem.isFree = true;
						}
					}
				}*/

				SB_IAPItem iapItem = SB_IAPManager.instance.GetIAPItemAtIndex(i);
				if(iapItem == null)
					error("*** iapItem is null!!");

				if((iapItem.enabled && (iapItem.availableInStore || iapItem.isFree) && !iapItem.purchased) || (iapItem.toDownload)||(iapItem.type==SB_IAPItem.TYPE_COMING_SOON))
				{
					//if(true) // Phil test
					error("*** creating menu item");

					Transform swipeMenuItemTrans = transform.Find("SwipeMenu/SwipeMenuItem");
					if(swipeMenuItemTrans == null)
						error("*** swipeMenuItemTrans is null!!!!!!!!!!!!");

					GameObject menuItem = (GameObject)Instantiate(swipeMenuItemTrans.gameObject);
					if(menuItem == null)
						error("*** menuItem is null!!!!!!!!!!!!");

					error("*** menuItem created");

					ShopMenuItem menuItemData = menuItem.GetComponent<ShopMenuItem>();
					error("*** menuItemData created");

					string price = "";
					string priceStr = "";

					if(iapItem == null)
					{
						error("*** iapItem is null!!!!!!!!!!!!");
					}

					if(iapItem.toDownload)
					{
						priceStr = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/buttons/button", "shop_iap_download");
					}
					else if(iapItem.isFree)
					{
						warn("ITEM IS FREEEEEEEE!");
						priceStr = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/text", "item_free_price");
					}
					else
					{
						#if !UNITY_EDITOR
						SkuDetails tsku = null;

						if(_openIABWrapper.Inventory == null)
						{
							error("*** _openIABWrapper.Inventory is null!!!!!!");
						}

						// IMPORTANT! Check that the inventory has details for the SKU, before going any further
						if(_openIABWrapper.Inventory.HasDetails(iapItem.sku))
						{
							tsku = _openIABWrapper.Inventory.GetSkuDetails(iapItem.sku);
							warn("tsku: " + tsku);
						}
						
						// A temporary price string value, only displayed if the SKU does not exist in the store inventory (retrieved from server)
						price = "99999";
						if(tsku != null)
						{
							price = tsku.Price;
						}
						#endif
						
						priceStr = price;
					}
					iapItem.cost = priceStr;
					error("*** about to call menuItemData.SetupIAPItem()");
					
					//addItemToMenu = true;
					menuItemData.SetupIAPItem(iapItem);
					if(iapItem.toDownload)
					{
						_storeItemsPageSet.Insert(0, menuItemData);
					}
					else
						_storeItemsPageSet.Add(menuItemData);
				}
				
				#if UNITY_EDITOR
				// Show all 'enabled' items, when testing in IDE
				/*addItemToMenu = true;
				iapItem.cost = "9999";
				
				// Ignore any packs marked as 'purchased' in the test data
				ArrayList testData = GetTestData();
				foreach(string packId in testData)
				{
					if(addItemToMenu && iapItem.ContainsPack(packId))
					{
						addItemToMenu = false;
					}
					
					
				}*/
				
				#endif
				
				
				

			}
			
			// TODO: Remove items from shop, which contain packs already purchased
			
			/*
			//if(!addItemToMenu)
			//{
				for (int j=0; j<SB_IAPManager.instance.IAPItemCount(); j++) 
				{
					SB_IAPItem iapItem2 = SB_IAPManager.instance.GetIAPItemAtIndex(j);
					

					error("***** LOOKING FOR " +iapItem2.sku + " IN PACK " + iapItem.sku + " : " + iapItem.ContainsPackFromIAP(iapItem2));
					if(iapItem.sku != iapItem2.sku && iapItem.ContainsPackFromIAP(iapItem2))
					{
						addItemToMenu = false;
						error ("\t\tNOT ADDING ITEM TO MENU " + iapItem.sku);
					}
			
				}
			//}
			*/
			
			if(_storeItemsPageSet.Count == 0)
			{
				// No items in the shop! (..maybe everything's been purchased) Show a message
				
				// Add an empty menu item to the pageset, so user can still swipe
			//	_menu.AddPageSet(CreateEmptyPageSet(), new Vector2(3, 1), new Vector2(155.0f, 220.0f), PAGESET_STORE);
			}
			else
			{
				_menu.AddPageSet(_storeItemsPageSet, new Vector2(3, 1), new Vector2(155.0f, 220.0f), PAGESET_STORE);
			}
		}
		
		
		
		/**
		 * Setup purchased packs page
		 */	
		private void SetupMyPurchasedPacksMenu()
		{
			_myPurchasedItemsPageSet = new ArrayList();
			
			for(int i=0; i<SB_PackManager.instance.PackItemCount(); i++)
			{
				// Only add purchased items to this pageset
				bool addItemToMenu = false;
				SB_PackItem pack = SB_PackManager.instance.GetPackItemAtIndex(i);
				
				//if((_openIABWrapper.Inventory != null && _openIABWrapper.Inventory.HasPurchase(pack.packID)))
				//{
				if(pack.downloaded)
				{
					addItemToMenu = true;
				}
				//}
				
				#if UNITY_EDITOR
				// Include any packs marked as 'purchased' in the test data
				/*ArrayList testData = GetTestData();
				foreach(string packId in testData)
				{
					error("............MY PACKS looking for packId " + packId + ", pack.packID: " + pack.packID);
					if(!addItemToMenu && (pack.packID == packId || pack.isFree))
					{
						addItemToMenu = true;
					}
				}*/
				#endif
				
				if(addItemToMenu)
				{
					GameObject menuItem = (GameObject)Instantiate(transform.Find("SwipeMenu/SwipeMenuItem").gameObject);
					ShopMenuItem menuItemData = menuItem.GetComponent<ShopMenuItem>();
					menuItemData.SetupPackItem(pack);
					menuItemData.enabled = false;
					_myPurchasedItemsPageSet.Add(menuItemData);
				}
				else
				{
					Debug.LogError("_openIABWrapper.Inventory is null!!!!  pack details: "+pack.ToString());
				}
			}
			
			
			
			
			
			
			if(_myPurchasedItemsPageSet.Count == 0)
			{
				// No items in the shop! (..maybe everything's been purchased) Show a message
				
				// Add an empty menu item to the pageset, so user can still swipe
				//_menu.AddPageSet(CreateEmptyPageSet(), new Vector2(3, 1), new Vector2(155.0f, 220.0f), PAGESET_MY_PURCHASES);
			}
			else
			{
				_menu.AddPageSet(_myPurchasedItemsPageSet, new Vector2(3, 1), new Vector2(155.0f, 220.0f), PAGESET_MY_PURCHASES);
			}
			
		}
		
		ArrayList CreateEmptyPageSet()
		{
			ArrayList arr = new ArrayList();
			
			// Add an empty menu item to the pageset, so user can still swipe
			GameObject menuItem = (GameObject)Instantiate(transform.Find("SwipeMenu/SwipeMenuItemEmpty").gameObject);
			ShopMenuItem menuItemData = menuItem.GetComponent<ShopMenuItem>();
			
			arr.Add(menuItemData);
			
			return arr;
		}
		
		private void AddListeners()
		{
			_menu.OnHandleTap = MenuItemSelected;
			_menu.OnPageSetChanged = HandleMenuPageSetChanged;
		}
		
		private void RemoveListeners()
		{
			_menu.OnHandleTap = null;
			_menu.OnPageSetChanged = null;
		}

		
		/**
		 * Delete any downloaded shop images, if 'now' is greater than our cache timeout value
		 */
		private void CheckCachedShopImages()
		{
			Debug.Log("----------------------------------------------");
			
			// Try and get expiry date from PlayerPrefs
			string expiryStr = PlayerPrefs.GetString(SHOP_IMAGE_CACHE_EXPIRY_KEY);
			Debug.Log("CheckCachedShopImages() expiryStr: "+expiryStr);
			
			DateTime now = DateTime.Now;
			
			// Set a default value, with an expiry date of yesterday
			DateTime expiry = now.AddDays(-1);
			if(expiryStr.Length > 0)
			{
				// Parse a saved expiry date
				expiry = DateTime.Parse(expiryStr);
			}
			
			Debug.LogError("NOW: "+now.ToString());
			Debug.LogError("expiry: "+expiry.ToString());
			
			if(now > expiry)
			{
				// Set new expiry date, of 2 days
				expiry = now.AddDays(2);
				
				Debug.Log("CheckCachedShopImages() Setting new expiry date: "+expiry.ToString());
				PlayerPrefs.SetString(SHOP_IMAGE_CACHE_EXPIRY_KEY, expiry.ToString());
				SB_ShopUtils.DeleteLocalShopFiles();
			}
			Debug.Log("----------------------------------------------");
		}
		
		/**
		 * Return a dummy ArrayList of pack item id's, to represent purchased packs
		 */
		ArrayList GetTestData()
		{
			ArrayList purchasedPacks = new ArrayList();
			
			
			purchasedPacks.Add("pack1");
			//purchasedPacks.Add("pack3");
			
			/*
			purchasedPacks.Add("pack3");
			purchasedPacks.Add("pack4");
			purchasedPacks.Add("pack2");
			purchasedPacks.Add("pack5");
			*/
			
			return purchasedPacks;
		}
		
		override protected void OnDestroy()
		{
			_menu = null;
			_shopMessageText = null;
			
			if(_storeItemsPageSet != null)
			{
				_storeItemsPageSet.Clear();
				_storeItemsPageSet = null;
			}
			
			if(_myPurchasedItemsPageSet != null)
			{
				_myPurchasedItemsPageSet.Clear();
				_myPurchasedItemsPageSet = null;
			}
			
			_restorePurchasesBtn = null;
			_details = null;
			_detailBackBtn = null;
			_buyBtn = null;
			
			Destroy(transform.Find("ShopInfoMessage").gameObject);
			base.OnDestroy();
		}
	}
}