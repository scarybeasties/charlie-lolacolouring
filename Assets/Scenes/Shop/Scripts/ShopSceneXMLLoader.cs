﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.InAppPurchasing.Shop;
using System;
using ScaryBeasties.Utils;
using System.Xml;

public class ShopSceneXMLLoader : MonoBehaviour 
{
	private string _shopCopyXMLStr = "";
	private string _shopDataXMLStr = "";
	private int _shopXMLLoadCount = 0;
	private string _shopXMLLoadErrors = "";
	
	public Action<bool, string> OnXMLLoaded;
	
	// Only load the XML once, per session
	public static bool XML_LOADED = false;
	
	const string SHOP_COPY_XML_NODE = "<shopCopy>";
	const string SHOP_DATA_XML_NODE = "<shopData>";
	
	/**
	 * Load store XML files
	 */
	public void LoadStoreXML(bool forceLoad=false)
	{
		if(XML_LOADED && !forceLoad)
		{
			// Stop here, if XML has already been loaded
			XMLLoaded(true);
			return;
		}
		
		StartCoroutine(LoadCopyXML());
	}
	
	/**
	 * Check if the shopcopy.xml and shopdata.xml files have both been loaded
	 */
	void CheckXMLLoadStatus ()
	{
		Debug.LogWarning ("CheckXMLLoadStatus _shopXMLLoadCount: " + _shopXMLLoadCount);
		Debug.LogWarning ("_shopXMLLoadErrors: " + _shopXMLLoadErrors);
		if(_shopXMLLoadCount == 2)
		{
			// Make sure no errors were encountered during XML loading!
			if(_shopXMLLoadErrors.Length == 0)
			{
				SB_IAPManager.instance.AddXMLList(_shopDataXMLStr, _shopCopyXMLStr);
				SB_PackManager.instance.AddXMLList(_shopDataXMLStr, _shopCopyXMLStr);
				//StoryManager.instance.InitFromXml(_shopCopyXMLStr);
				
				// Grab the application build data from the shop data xml 
				XmlDocument appVersionXML = new XmlDocument();
				appVersionXML.LoadXml(_shopDataXMLStr);
				XmlElement appVersionBuildXML = appVersionXML.SelectNodes("shopData/application/build")[0] as XmlElement;
				VersionChecker.instance.ParseBuildXML(appVersionBuildXML);
				
				XMLLoaded(true);
			}
			else
			{
				XMLLoaded(false, _shopXMLLoadErrors);
			}
		}
	}
	
	void XMLLoaded(bool status, string errors="")
	{
		if(OnXMLLoaded != null)
		{
			XML_LOADED = status;
			OnXMLLoaded(status, errors);
		}
	}

	/**
	* Check that the xmlString contains a node, of some sort.
	* This check is essential, as the server might return an html response, instead of a 404 error,
	* so the response needs to be parsed
	*/
	bool ContainsNode(string xmlStr, string str)
	{
		// Check that there's some data in the response (32 is an arbitrary value, could just as well be 1)
		if(xmlStr.Length < 32) return false;
		
		return xmlStr.IndexOf(str) > -1;
	}
	
	/**
	 * Load shopcopy.xml
	 */
	IEnumerator LoadCopyXML()
	{
		string url = SB_ShopUtils.GetRemoteShopCopyUrl() + "?rnd=" + UnityEngine.Random.Range(0, 9999); // GET UNCACHED XML FILE
		Debug.LogError("----> LoadCopyXML " + url);
		WWW www = new WWW(url);
		yield return www;
		
		// Check that the www.text actually has some data in it, and that no error has been returned
		if (www.error == null && ContainsNode(www.text, SHOP_COPY_XML_NODE))
		{
			// Data loaded
			_shopCopyXMLStr = www.text;
			Debug.LogError("_shopCopyXMLStr "+_shopCopyXMLStr);
		}
		else
		{
			// Failed to load data from URL
			Debug.LogError("----> ERROR: " + www.error);
			_shopXMLLoadErrors += SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/store/text", "msg_error_shopcopy_xml") + "\n";
		}
		
		_shopXMLLoadCount++;
		//CheckXMLLoadStatus();
		
		www.Dispose();
		www = null;
		
		StopCoroutine(LoadCopyXML());
		StartCoroutine(LoadDataXML());
	}
	
	/**
	 * Load shopdata.xml
	 */
	IEnumerator LoadDataXML()
	{
		string url = SB_ShopUtils.GetRemoteShopDataUrl() + "?rnd=" + UnityEngine.Random.Range(0, 9999); // GET UNCACHED XML FILE
		Debug.LogError("----> LoadDataXML " + url);
		
		WWW www = new WWW(url);
		yield return www;
		
		// Check that the www.text actually has some data in it, and that no error has been returned
		if (www.error == null && ContainsNode(www.text, SHOP_DATA_XML_NODE))
		{
			// Data loaded
			_shopDataXMLStr = www.text;
			Debug.LogError("_shopDataXMLStr "+_shopDataXMLStr);
		}
		else
		{
			// Failed to load data from URL
			Debug.LogError("----> ERROR: " + www.error);
			_shopXMLLoadErrors += SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/store/text", "msg_error_shopdata_xml");
		}
		
		_shopXMLLoadCount++;
		CheckXMLLoadStatus();
		
		www.Dispose();
		www = null;
		
		StopCoroutine(LoadDataXML());
	}
	
	void OnDestroy()
	{
		OnXMLLoaded = null;
	}
}
