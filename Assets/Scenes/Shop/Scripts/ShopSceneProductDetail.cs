﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Scenes;
using System.Collections.Generic;
using ScaryBeasties.UI;
using ScaryBeasties.Controls;
using UnityEngine.UI;
using ScaryBeasties.SB2D;
using ScaryBeasties.InAppPurchasing.Shop;
using System.IO;
using System.Globalization;
using ScaryBeasties.Data;
using ScaryBeasties.Utils;
using ScaryBeasties.Cam;
using ScaryBeasties.Base;
using ScaryBeasties.Tweening;
using ScaryBeasties.Popups;
using ScaryBeasties.Scenes.Shop;

namespace ScaryBeasties.DrWho.Shop
{
	public class ShopSceneProductDetail : ShopSceneBase 
	{
		private SB_IAPItem _iapItem;

		private Text _productTitle;
		private Text _productDescription;
		private Text _productPrice;
		private Text _cornerText;
		private GameObject _productImage;
		private RectTransform _productContentPanel;
		private ScrollRect _scrollRect;
		private SpriteRenderer _background;
		private SB_Button _buyBtn;

		override protected void Start()
		{
			base.Start();
				
			if(SCENE_CONFIG_OBJECT != null)
			{
				if(SCENE_CONFIG_OBJECT.GetValue("iapItem") != null)
				{
					_iapItem =  (SB_IAPItem)SCENE_CONFIG_OBJECT.GetValue("iapItem");
				}
			}

			//_backBtn = transform.Find ("back_btn").gameObject.GetComponent<SB_Button> ();
			_buyBtn = transform.Find("buy_btn").gameObject.GetComponent<SB_Button> ();
			_buyBtn.label = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/buttons/button", "shop_iap_buy");

			_productTitle = transform.Find("product/title/label").GetComponent<Text>();
			_productDescription = transform.Find("product/content/Canvas/Panel/label").GetComponent<Text>();
			_productImage = transform.Find("product/image/image").gameObject;
			_productPrice = _buyBtn.transform.Find("Price").GetComponent<Text>();
			_cornerText = transform.Find("corner_text/text").GetComponent<Text>();
			
			_productContentPanel = transform.Find("product/content/Canvas/Panel").GetComponent<RectTransform>();
			_scrollRect = _productContentPanel.GetComponent<ScrollRect>();
			_background = transform.Find("background").GetComponent<SpriteRenderer>();

			SetupProductDetails();
		}

		private void SetupProductDetails() 
		{
			// Use test data, when running this scene in IDE
			if(_iapItem == null)
			{
				_iapItem = GetTestData();
			}

			_productTitle.text = _iapItem.infoTitle; //SB_Utils.GetNodeValueFromConfigXML ("config/copy/shop/store/text", "title");
			_productDescription.text = _iapItem.infoDescription;
			if(_productDescription.preferredHeight < _productContentPanel.sizeDelta.y) 
			{
				_scrollRect.enabled = false;
			}
			else 
			{
				_productDescription.rectTransform.sizeDelta = new Vector2(_productDescription.rectTransform.sizeDelta.x, _productDescription.preferredHeight);
				_scrollRect.verticalNormalizedPosition = 1;
			}

			if(_iapItem.type == SB_IAPItem.TYPE_COMING_SOON)
			{
				// Item not yet available.  Hide the 'buy' button, show 'coming soon' message
				_buyBtn.gameObject.SetActive(false);
				_cornerText.text = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/text", "item_type_comingsoon");
			}
			else if(_iapItem.type == SB_IAPItem.TYPE_FREE)
			{
				// Item is free
				_buyBtn.label = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/buttons/button", "shop_iap_download");
				_productPrice.text = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/text", "item_type_free");
				_cornerText.gameObject.SetActive(false);
			}
			else
			{
				// Item is available to download, show its price
				_productPrice.text = _iapItem.cost;
				_cornerText.gameObject.SetActive(false);
			}

			_background.color = SB_Utils.HexToColour(_iapItem.infoColourHex);
			Color alphaTextBackgroundColour = _background.color * 0.9f;
			alphaTextBackgroundColour.a = 0.6f;
			transform.Find("product/content/contentBackground").GetComponent<SpriteRenderer>().color = alphaTextBackgroundColour;

			SB2DScaledSprite spr = SB2DSpriteMaker.CreateScaledSprite(_productImage, "Asset", this.OnSpriteLoaded);
			string url = _iapItem.detailImage;
			
			string localShopImage = SB_ShopUtils.GetLocalShopDirectoryPath() + "/" + _iapItem.detailImage;
			warn("_iapItem.detailImage: " + _iapItem.detailImage);
			warn("localShopImage: " + localShopImage);
			
			if(File.Exists(localShopImage))
			{
				// Load image from local filesystem
				url = localShopImage;
				
				warn("---> LOADING IMAGE FROM: " + url);
				spr.LoadImageFromDisk(url);
			}
			else if(SB_IAPManager.instance.ShopConfig != null)
			{
				// Load image from server
				url = SB_Globals.SHOP_ASSETS_PATH + SB_Globals.LOCALE_STRING + "/" + SB_IAPManager.instance.ShopConfig.ShopImagePath + "/" + _iapItem.detailImage;
				
				warn("---> LOADING IMAGE FROM: " + url);
				spr.LoadImage(url);
			}
		}

		private void OnSpriteLoaded(SB2DSprite spr) 
		{
			SB2DScaledSprite scaledSpr = (SB2DScaledSprite)spr;

			float newX = _productImage.transform.position.x - (scaledSpr.ScaledWidth / 2);
			float newY = _productImage.transform.position.y + (scaledSpr.ScaledHeight / 2);
			
			_productImage.transform.position = new Vector3(newX, newY, _productImage.transform.position.z);
			SpriteRenderer sprRenderer = spr.transform.GetComponent<SpriteRenderer>();

			Color col1 = sprRenderer.color;
			col1.a = 0.0f;
			sprRenderer.color = col1;

			SB_Tweener.TweenAlpha (spr.gameObject, 0.4f, 1.0f);
			
			/*
			warn ("LOADED SPRITE " + spr.AssetName + ", " + spr.AssetFileName);
			
			string localShopImage = SB_ShopUtils.GetLocalShopDirectoryPath() + "/" + spr.AssetName;
			
			if(!Directory.Exists(SB_ShopUtils.GetLocalShopDirectoryPath()))
			{
				Directory.CreateDirectory(SB_ShopUtils.GetLocalShopDirectoryPath());
			}
			string filename = SB_ShopUtils.GetLocalShopDirectoryPath() + "/" + spr.AssetName;
			byte[] jpg = spr.Texture.EncodeToJPG();
			File.WriteAllBytes(filename, jpg);
			*/
		}

		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			base.OnBtnHandler(btn, eventType);
			
			if(eventType == SB_Button.MOUSE_UP)
			{
				if(btn.name == "buy_btn")
				{
					LogButtonEvent("buy");
					error (_iapItem.ToString());

					/**/
					#if UNITY_EDITOR
					// Show confirmation popup
					string msg = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/store/text", "popup_confirm_purchase");
					msg += "\n" + _iapItem.cost;
					ShowAlertPopup(msg);
					#else
					BuyItem(_iapItem);
					#endif
				}
			}
			
		
		}
		
		
		override protected void HandleOnPopupEvent (SB_BasePopup popup, string eventType)
		{
			base.HandleOnPopupEvent (popup, eventType);

			log ("------HandleOnPopupEvent " + eventType);
			switch(eventType)
			{
				case SB_AlertPopup.OPTION_OK: 
					#if UNITY_EDITOR
					/// TEMPORARY CODE TO TEST PACK DOWNLOAD!!!!!!!!!!!!!!!!!!!!!
					if(_shopState != STATE_NOT_INTERNET_CONNECTED)
					{
						warn ("DOWNLOAD THE PACKS!!!!");
						foreach(string packId in _iapItem.packIDs)
						{
							DownloadPack(packId);
						}
					}
					#endif
					break;
			}
		}
		
		override protected void PackDownloadSuccess()
		{
			base.PackDownloadSuccess();

			_buyBtn.gameObject.SetActive(false);
			_cornerText.text = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/text", "item_purchased");
			_cornerText.gameObject.SetActive(true);
		}
		
		override protected void PackDownloadFailed(string errors)
		{
			base.PackDownloadFailed(errors);
		}

		/**
		 * Return a dummy SB_IAPItem, for use when testing in the IDE
		 */
		SB_IAPItem GetTestData()
		{
			SB_IAPManager.instance.ShopConfig = new SB_ShopConfig(null);
			SB_IAPManager.instance.ShopConfig.ShopImagePath = "images/shop";
			
			SB_IAPItem iap = new SB_IAPItem (null, null);

			//iap.type = SB_IAPItem.TYPE_COMING_SOON;
			//iap.type = SB_IAPItem.TYPE_FREE;
			//iap.type = SB_IAPItem.TYPE_BUY_NOW;
			iap.type = SB_IAPItem.TYPE_NEW;
			iap.sku = "pack2";
		
			iap.packIDs.Add("pack2");
			iap.packIDs.Add("pack3");
			iap.packIDs.Add("pack12");
			//iap.packIDs.Add("pack14");
			iap.packIDs.Add("pack4");

			iap.infoTitle = "Test title...";
			iap.infoDescription = "TEST - Description of pack 1.. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?";
			iap.detailImage = "pack2_detail.jpg";
			iap.cost = "£99.99";
			//iap.infoColourHex = "#000000";
			iap.infoColourHex = "#AA8C30";

			return iap;
		}

		override protected void OnDestroy()
		{
			_iapItem = null;
			_productTitle = null;
			_productDescription = null;
			_productPrice = null;
			_cornerText = null;
			
			_productImage = null;
			_productContentPanel = null;
			_scrollRect = null;
			_background = null;
			_buyBtn = null;

			SB_Tweener.StopAllTweens();

			base.OnDestroy();
		}
	}
}