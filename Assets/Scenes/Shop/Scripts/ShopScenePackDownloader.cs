﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.InAppPurchasing.PackDownloader;
using ScaryBeasties.Data;
using ScaryBeasties.InAppPurchasing.Local;
using System;
using ScaryBeasties.User;
using ScaryBeasties.InAppPurchasing.Shop;

namespace ScaryBeasties.Scenes.Shop
{
	public class ShopScenePackDownloader : SB_PackDownloader 
	{
		public Action<bool, string, string> OnDownloadComplete;
		
		public void DownloadPack (string packID)
		{
			base.DownloadPack(packID, OnPackDownloadHandler);
		}
		
		public void DownloadFailedPacks(KeyValueObject failedPacks)
		{
			Debug.LogWarning("DownloadFailedPacks");
			
			// Get the pack id's which failed verification
			if(failedPacks != null)
			{
				foreach(string packId in failedPacks.Keys)
				{
					// Delete the pack xml file before re-downloading the pack.
					SB_LocalPackManager.instance.DeletePackXML(packId);
					DownloadPack(packId);
				}
			}
		}
		
		void OnPackDownloadHandler (SB_PackDownloaderResponse response)
		{
			//_buttonsLocked = false;
			
			Debug.LogWarning ("OnPackDownloadHandler status: " + response.Status + ", " + response.PackId + ", QueueLength: " + QueueLength);
			Debug.LogWarning (response.Status + ", " + response.PackId);
			
			if(response.Status == SB_PackDownloaderResponse.DOWNLOAD_QUEUE_EMPTY)
			{
				// Check that the download queue is now empty
				if(QueueLength == 0)
				{
					//_shopState = STATE_READY;
					
					Debug.LogWarning ("response.Errors.Count: " + response.Errors.Count);
					if(response.Errors.Count > 0)
					{	
						// Errors were encountered!
						
						string err = "\n";
						foreach(String e in response.Errors)
						{
							err += "\n" + e;
						}
						
						
						if(OnDownloadComplete != null)
						{
							OnDownloadComplete(false, err, response.PackId);
						}
					}
					else
					{
						// No errors encountered, all packs downloaded
						if(OnDownloadComplete != null)
						{
							OnDownloadComplete(true, "", response.PackId);
						}
					}
				}
			}
			else if (response.Status == SB_PackDownloaderResponse.PACK_ALREADY_DOWNLOADED || response.Status == SB_PackDownloaderResponse.PACK_DOWNLOAD_SUCCESS) 
			{
				if(response.Status == SB_PackDownloaderResponse.PACK_DOWNLOAD_SUCCESS)
				{
					// Add the purchased sku (pack id) to the userprofile.xml key value object.
					SB_UserProfileManager.instance.CurrentProfile.installedIAPPacks.SetKeyValue(response.PackId, "true");
					SB_UserProfileManager.instance.SaveProfileData(SB_UserProfileManager.instance.CurrentProfile);

				}
			}
			else if(response.Status == SB_PackDownloaderResponse.PACK_DOWNLOAD_ERROR)
			{
				string err = "";
				foreach(string e in response.Errors)
				{
					Debug.LogError("ERROR " + e);
					if(err.Length > 0) err += ", ";
					err += e;
				}
				/*
				_infoPopup.SetInfo("");
				_infoPopup.ShowButtons(ShopInfoPopupButtonOptions.Close);
				*/
				
				if(QueueLength == 0 && OnDownloadComplete != null)
				{
					OnDownloadComplete(false, err, response.PackId);
				}
			}
			
			if (response.PackXML != null) 
			{
				Debug.LogWarning ("----------------- PACK XML -----------------");
				
				ColouringPackContents _packData = new ColouringPackContents(response.PackXML);
				//error(packData.ToString());
				//Status (packData.ToString());
			}
		}
		
		override protected void OnDestroy()
		{
			OnDownloadComplete = null;
					
			base.OnDestroy();
		}
	}
}