﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.UI.SwipeMenu;
using ScaryBeasties.InAppPurchasing.Shop;
using ScaryBeasties.SB2D;
using UnityEngine.UI;
using System.IO;
using ScaryBeasties.Utils;

namespace ScaryBeasties.Scenes.Shop
{
	public class ShopMenuItem : SwipeMenuItem 
	{
		// NOTE: one of these values will always be null, and the other should have content
		public SB_IAPItem iapItemData = null;
		public SB_PackItem packItemData = null;

		bool _readyToLoad = false;
		/**
		 * Constructor for shop IAP items (which have not yet been purchased)
		 */
		public void SetupIAPItem(SB_IAPItem iapData)
		{
			iapItemData = iapData;

			SetType (iapData.typeStr);
			SetTitle (iapData.title, iapData.subtitle);
			SetPrice (iapData.cost);

			if (iapData.type == SB_IAPItem.TYPE_COMING_SOON) {
				ShowComingSoon(iapData.typeStr);
			}

			if (iapData.toDownload) {
				ShowDownload ();
			}

			//LoadImage(iapData.image);
			_readyToLoad=true;
		}

		// Update is called once per frame
		void Update () {
			if (_readyToLoad) {
				LoadImage (iapItemData.image);
				_readyToLoad=false;
			}
		}
		
		/**
		 * Constructor for 'my purchased' items
		 */
		public void SetupPackItem(SB_PackItem pack)
		{
			packItemData = pack;

			SetType(""); // Set type to blank, for items already purchased
			//SetTitle(pack.title, pack.subtitle);
			SetPrice(""); // Set price to blank, for items already purchased
			LoadImage(pack.image);

			// Hide the 'info' icon, for items already purchased
			transform.Find("InfoIcon").gameObject.SetActive (false);
			// Background image not needed
			transform.Find("Background").gameObject.SetActive (false);

			transform.Find("Text/Title").gameObject.SetActive (false);
			transform.Find("Text/SubTitle").gameObject.SetActive (false);

			transform.Find("Text/PurchasedTitle").gameObject.SetActive (true);
			Text t = transform.Find("Text/PurchasedTitle").GetComponent<Text>();
			t.text = pack.title;

		}

		private void ShowDownload(){
			transform.Find("InfoIcon").gameObject.SetActive (false);
			transform.Find ("Text/Price").gameObject.SetActive (false);
			transform.Find ("Text/ComingSoon").gameObject.SetActive (true);
			Text t = transform.Find("Text/ComingSoon").GetComponent<Text>();
			t.text = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/buttons/button", "shop_iap_download");;
		}

		private void ShowComingSoon(string tstr){
			// Hide the 'info' icon, for items already purchased
			transform.Find("InfoIcon").gameObject.SetActive (false);
			transform.Find ("Text/Price").gameObject.SetActive (false);
			transform.Find ("Text/ComingSoon").gameObject.SetActive (true);
			Text t = transform.Find("Text/ComingSoon").GetComponent<Text>();
			t.text = tstr;

			this.enabled = false;
		}
		
		public void SetType(string str)
		{
			//Text txt = transform.Find("Text/Type").GetComponent<Text>();
			//txt.text = str;
		}
		
		public void SetTitle(string title, string subtitle)
		{
			Text t = transform.Find("Text/Title").GetComponent<Text>();
			t.text = title;
			
			Text st = transform.Find("Text/SubTitle").GetComponent<Text>();
			st.text = subtitle;
		}
		
		public void SetPrice(string str)
		{
			Text txt = transform.Find("Text/Price").GetComponent<Text>();
			
			if(str.Length == 0)
			{
				txt.text = "";
				return;
			}
			
			// Item price and currency unit are returned from the server
			txt.text = str;
		}
		
		/**
		 * Load the menu item image
		 */
		private void LoadImage(string path)
		{
			SB2DSprite spr = SB2DSpriteMaker.CreateScaledSprite(transform.Find ("Content").gameObject, "SB2DSprite", OnSpriteLoaded);
			
			string url = SB_Globals.SHOP_ASSETS_PATH + SB_Globals.LOCALE_STRING + "/" + SB_IAPManager.instance.ShopConfig.ShopImagePath + "/" + tk2dSystem.CurrentPlatform + "/" + path;
			string localShopImage = SB_ShopUtils.GetLocalShopDirectoryPath() + "/" + path;			
			if(File.Exists(localShopImage))
			{

				// Load image from local filesystem
				url = localShopImage;
				//Debug.LogWarning("---> LOCAL LOADING IMAGE FROM: " + url);
				spr.LoadReadableImageFromDisk(url);
			}
			else
			{
				//Debug.LogWarning("---> WWW LOADING IMAGE FROM: " + url);
				spr.LoadReadableImage(url);
			}
			
			GameObject itemGameObj = spr.gameObject;
			itemGameObj.transform.parent = transform.Find ("Content").gameObject.transform;
			
			SpriteRenderer trend = itemGameObj.GetComponent<SpriteRenderer>();
			trend.sortingOrder = 14;
			//Debug.LogWarning ("SORTING ORDER " + trend.sortingOrder);
			transform.Find ("Content").gameObject.transform.localScale = new Vector2 (1.0f, 1.0f);
		}
		
		private void OnSpriteLoaded(SB2DSprite spr) 
		{
			Debug.LogWarning ("SPRITE LOADING CALLBACK");
			SB_ShopUtils.SaveSpriteToDisk(spr);
		}
		
		override protected void OnDestroy()
		{
			iapItemData = null;
			packItemData = null;
			
			base.OnDestroy();
		}
	}
}