﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ScaryBeasties.Base;
using ScaryBeasties.Data;
using ScaryBeasties.InAppPurchasing.Local;
using ScaryBeasties.InAppPurchasing.Shop;
using ScaryBeasties.Popups;
using ScaryBeasties.Scenes.Shop;
using ScaryBeasties.UI;
using ScaryBeasties.Utils;
using OnePF;

/**
 * A base scene, which the shop and product details pages extend
 */
namespace ScaryBeasties.Scenes.Shop
{
	public class ShopSceneBase : SB_BaseScene
	{
		protected int _shopState = 0;
		protected const int STATE_INIT = 0; 
		protected const int STATE_LOADING_STORE = 1; 
		protected const int STATE_READY = 2; 
		protected const int STATE_PREBUY_RESTORE = 3; 
		protected const int STATE_BUYING = 4; 
		protected const int STATE_RESTORING = 5; 
		protected const int STATE_RESTORE_FAILED = 6; 
		protected const int STATE_LOADING_XML = 7;
		protected const int STATE_DOWNLOADING_PACK = 8;
		protected const int STATE_DOWNLOAD_FAILED = 9;
		protected const int STATE_FILE_VALIDATION_FAILED = 10;
		protected const int STATE_NOT_INTERNET_CONNECTED = 11;
		protected const int STATE_XML_LOAD_ERROR = 12;
		protected const int STATE_SHOP_GENERAL_ERROR = 13;
		
		protected SB_OpenIABWrapper _openIABWrapper;
		protected ShopScenePackDownloader _shopPackDownloader;
		private int _downloadProgress = 0;		
		
		private ShopSceneXMLLoader _shopXMLLoader;
		protected KeyValueObject _failedPacks;
		
		private bool _iapListReady = false;
		private int _iapListReadyCounter = 0;
		private int _iapListReadyCounterMax = 100;
		
		protected string _selectedSku = "";
		protected string _devPayload = "ScaryBeastiesPayload";

		private Transform _progressBarHolder;
		private float _progressBarWidth;
		private float _progressBarLeft;
		private Transform _progressBar;
		
		private ShopInfoMessage _shopInfoMessage;
		protected bool _shopInfoMessageVisible = false;
		
		private float _netCheckCounter = 2f;
		
		override protected void Start()
		{
			base.Start();
			_uiControls.ShowButtons(new List<string>(new string[] {
																			SB_UIControls.BTN_BACK,
																			SB_UIControls.BTN_SOUND,
																			SB_UIControls.BTN_PARENTS
																	}));
																	
			//OpenIAB.enableDebugLogging (true);
			_openIABWrapper = new SB_OpenIABWrapper();
			_openIABWrapper.OnSB_OpenIABWrapperEvent += HandleOnSB_OpenIABWrapperEvent;
			
			_shopPackDownloader = transform.GetComponent<ShopScenePackDownloader>();
			_shopPackDownloader.OnDownloadComplete = HandleOnDownloadComplete;
			
			_shopXMLLoader = transform.GetComponent<ShopSceneXMLLoader>();
			_shopXMLLoader.OnXMLLoaded = HandleOnXMLLoaded;
			
			_progressBarHolder = transform.Find("progress_bar");
			_progressBarWidth = _progressBarHolder.Find("background").localScale.x;
			_progressBarLeft = -(_progressBarWidth * 0.5f);
			_progressBar = _progressBarHolder.Find("bar");
			_progressBarHolder.gameObject.SetActive(false);
			
			_shopInfoMessage = transform.Find("ShopInfoMessage").GetComponent<ShopInfoMessage>();
			ShowInfoMessage(false);
		}
		
		override protected void Init()
		{
			if(!IsNetConnected())
			{	
				_shopState = STATE_NOT_INTERNET_CONNECTED;
				DisplayError(GetShopText("msg_error_no_internet"));
			}
			else
			{	
				ShowInfoMessage(true, GetShopText("msg_connecting_to_store"));
				
				_shopState = STATE_LOADING_XML;
				_shopXMLLoader.LoadStoreXML(true); //  Pass 'true' to forcibly load uncached shop XML
			}
		}
		
		private bool IsNetConnected()
		{
			if(Application.internetReachability == NetworkReachability.NotReachable)
			{
				return false;
			}
			
			return true;
		}
		
		/**
		 * Called when store XML has either loaded, or failed to load
		 */
		protected virtual void HandleOnXMLLoaded(bool success, string errors)
		{
			ShowInfoMessage(false);
			
			if(success)
			{
				InitStore();
			}
			else
			{
				error("HandleOnXMLLoaded() ERROR - "+errors);
				_shopState = STATE_XML_LOAD_ERROR;
				//DisplayError(GetShopText("msg_error_connection"), errors);  // <-- This version shows the actual xml files which failed to load
				DisplayError(GetShopText("msg_error_connection"));
				
				string err = errors.Replace(" ", "");
				err = errors.Replace("\n", "_");
				LogError("ShopXMLLoad_"+err);
			}
		}
		
		public void InitStore()
		{
			_shopState = STATE_LOADING_STORE;
			
			// Stop here, if init has already been done
			//if(SB_OpenIABWrapper.OPEN_IAB_INIT_DONE) return;
			
			ShowInfoMessage(true, GetShopText("msg_store_data_loading"));
			_openIABWrapper.Init();
		}
		
		/*
		 * Error Handling
		 */
		protected virtual void DisplayError(string title, string body="")
		{
			ShowAlertPopup(title, body, false);
		}
		
		protected virtual void ShowAlertPopup(string title, string body="", bool showCancelBtn=true)
		{
			ShowInfoMessage(false);
			title = SB_Utils.SanitizeString(title);
			body = SB_Utils.SanitizeString(body);
			
			SB_AlertPopupVO vo = new SB_AlertPopupVO(SB_PopupTypes.ALERT_POPUP, SB_PopupManager.PREFAB_ALERT_POPUP, title, body, showCancelBtn);
			ShowPopup(SB_PopupTypes.ALERT_POPUP, vo);
		}

		protected virtual void ShowInfoMessage(bool visible, string msg="")
		{
			_shopInfoMessageVisible = visible;
			
			msg = SB_Utils.SanitizeString(msg);
			_shopInfoMessage.gameObject.SetActive(visible);
			_shopInfoMessage.SetText(msg);

			SetUIButtonsEnabled(!visible);
		}

		void SetUIButtonsEnabled(bool val)
		{
			ButtonsEnabled(val);	
			if(_uiControls != null)
				_uiControls.ButtonsEnabled(val);

			Pause(!val);
		}

		protected virtual void UpdateInfoMessage(string msg)
		{
			msg = SB_Utils.SanitizeString(msg);
			if(_shopInfoMessage.gameObject.activeSelf)
			{
				_shopInfoMessage.SetText(msg);
			}
		}

		protected virtual void DownloadPack(string packID)
		{
			// Always check that we're connected, before commencing a download
			if(!IsNetConnected())
			{
				_shopState = STATE_NOT_INTERNET_CONNECTED;
				DisplayError(GetShopText("msg_error_no_internet"));
				return;
			}
			
			Dictionary<string, string> trackingParams =	new Dictionary<string, string>() 
			{
				{"PackID", packID}
			};
			LogSceneEvent("DownloadPack", trackingParams);
		
			warn("DownloadPack: "+packID);
			_shopState = STATE_DOWNLOADING_PACK;
			
			ShowInfoMessage(true, GetShopText("popup_msg_downloading"));
			
			// Show progress bar
			_progressBarHolder.gameObject.SetActive(true);

			_downloadProgress = 0;
			_shopPackDownloader.DownloadPack(packID);
		}
		
		/**
		 * Called whenever all packs have finished downloading
		 */
		protected virtual void HandleOnDownloadComplete(bool success, string errors, string packId)
		{
			ShowInfoMessage(false);
			_progressBarHolder.gameObject.SetActive(false);

			Dictionary<string, string> trackingParams =	new Dictionary<string, string>() 
			{
				{"PackID", packId}
			};
			
			if(success)
			{
				LogSceneEvent("PackDownloadSuccess", trackingParams);
				
				_shopState = STATE_READY;
				PackDownloadSuccess();
			}
			else
			{
				LogSceneEvent("PackDownloadFailed", trackingParams);
				
				_shopState = STATE_DOWNLOAD_FAILED;
				PackDownloadFailed(errors);
			}
		}
		
		protected virtual void PackDownloadSuccess()
		{	
			error("---------------------------------------");
			error("ALL PACKS DOWNLOADED SUCCESSFULLY");
			error("---------------------------------------");

		}

		protected virtual void PackDownloadFailed(string errors)
		{
			error("---------------------------------------");
			error("PACK DOWNLOAD ERRORS: "+errors);
			error("---------------------------------------");
			
			ShowAlertPopup(GetShopText("popup_msg_download_error"), "", false);
		}

		void OnGUI()
		{
			if(_iapListReady)
				return;
			
			if(_shopState == STATE_LOADING_STORE)
			{
				_iapListReadyCounter++;
				if(_iapListReadyCounter > _iapListReadyCounterMax)
				{
					_iapListReadyCounter = 0;
					if(SB_IAPManager.instance.IAPListReady())
					{
						//Debug.Log("OnGUI() _iapListReady " + _iapListReady);
						OnIAPListReady();
					}
				}
			}
		}
		
		override protected void Update()
		{
			base.Update();

			switch(_shopState)
			{
				case STATE_DOWNLOADING_PACK:
						
					if(_shopPackDownloader.LoadingProgress > 0)
					{
						_downloadProgress = _shopPackDownloader.LoadingProgress;
					}
						
					_progressBar.localScale = new Vector2((_progressBarWidth / 100f) * _downloadProgress, _progressBar.localScale.y);
	
					float newX = (_progressBarHolder.position.x-_progressBarWidth * 0.5f)+(_progressBar.localScale.x * 0.5f);
					_progressBar.transform.position = new Vector2(newX, _progressBar.transform.position.y);
	
					string progressText = "Downloading\n"+_downloadProgress+"%\n"+_shopPackDownloader.CurrentDownload().packId;
					progressText = SB_Utils.SanitizeString(progressText);
	
					string msg = GetShopText("popup_msg_downloading");
					//msg += "\n"+_shopPackDownloader.CurrentDownload().packId+" "+_downloadProgress+"%";
					msg += "\n"+_downloadProgress+"%";
					msg = SB_Utils.SanitizeString(msg);
					_shopInfoMessage.SetText(msg, 18);
					break;				
			}
			
			//DoNetConnectionCheck();
		}
		
		/*
		// GT - Do a net connection check, every 2 secs (incase user has disabled wifi)
		private void DoNetConnectionCheck()
		{
			_netCheckCounter -= Time.deltaTime;
			if(_netCheckCounter <= 0)
			{
				_netCheckCounter = 2f;
				
				if(!IsNetConnected())
				{
					LogSceneEvent("InternetConnected_No");
					
					_shopState = STATE_NOT_INTERNET_CONNECTED;
					
					ShowInfoMessage(false);
					DisplayError(GetShopText("msg_error_no_internet"));
				}
			}
		}
		*/
		
		protected virtual void OnIAPListReady()
		{
			_shopState = STATE_READY;
			
			_iapListReady = true;
			ShowInfoMessage(false);
		}

		override protected void HandleOnPopupEvent(SB_BasePopup popup, string eventType)
		{
			base.HandleOnPopupEvent(popup, eventType);
			
			log("------HandleOnPopupEvent "+eventType);
			switch(eventType)
			{
			case SB_AlertPopup.OPTION_OK: 
				if(_shopState == STATE_NOT_INTERNET_CONNECTED || _shopState == STATE_XML_LOAD_ERROR || _shopState == STATE_SHOP_GENERAL_ERROR)
				{
					// Return user to main menu, if we encounter an error which makes the shop unusable
					LoadScene(SB_Globals.SCENE_MAIN_MENU, null, true);
				}
				break;

			case SB_BasePopup.POPUP_CLOSED:
				if(_shopInfoMessageVisible)
				{
					SetUIButtonsEnabled(false);
				}
				break;
			}
		}

		/*
		override protected void HandleOnPopupEvent (SB_BasePopup popup, string eventType)
		{
			base.HandleOnPopupEvent (popup, eventType);

			switch(eventType)
			{
				case SB_BasePopup.POPUP_OPEN: 
					
					if(_shopState == STATE_DOWNLOADING_PACK)
					{
						//_alertPopup = _popupManager.CurrentPopupScript.transform.GetComponent<SB_AlertPopup>();
						//_alertPopup.ShowButtons(false, false);

						// Show progress bar
						//_progressBarHolder.gameObject.SetActive(true);
						//_progressBarHolder.position = _alertPopup.transform.position;
					}
					else if(_shopState == STATE_DOWNLOAD_FAILED)
					{
						//_alertPopup = _popupManager.CurrentPopupScript.transform.GetComponent<SB_AlertPopup>();
						//_alertPopup.ShowButtons(true, true);
					}
					break;
			}
		}
		*/
		
		protected void LogError(string err)
		{
			Dictionary<string, string> trackingParams =	new Dictionary<string, string>() 
			{
				{"Type", err}
			};
			LogSceneEvent("Error", trackingParams);
		}
		
		void HandleOnSB_OpenIABWrapperEvent(SB_OpenIABWrapper shopItemHandler, string eventType, object data=null)
		{
			switch(eventType)
			{
			case SB_OpenIABWrapper.BILLING_SUPPORTED:
				UpdateInfoMessage(GetShopText("msg_store_data_loaded"));
				break;
					
			case SB_OpenIABWrapper.BILLING_NOT_SUPPORTED:
				_shopState = STATE_SHOP_GENERAL_ERROR;
				DisplayError(GetShopText("msg_error_connection"));
				LogError("BillingNotSupported");
				break;
					
			case SB_OpenIABWrapper.QUERY_INVENTORY_SUCCESS:
				if(_openIABWrapper.Inventory == null)
				{
					//CloseInfoPopup();
					ShowInfoMessage(false);
				}
				break;
					
			case SB_OpenIABWrapper.QUERY_INVENTORY_FAILED:
				_shopState = STATE_SHOP_GENERAL_ERROR;
				DisplayError(GetShopText("msg_error_connection"));
				LogError("QueryInventoryFailed");
				break;
					
			case SB_OpenIABWrapper.PURCHASE_SUCCESS:	
					
				Purchase purchase = (Purchase)data;
				Debug.LogError("PURCHASE SUCCEEDED returned - "+purchase.DeveloperPayload+" local - "+_devPayload);
					//if (purchase.DeveloperPayload == _devPayload) 
					//{	
				PurchasedSuccess(purchase.Sku);
				ShowAlertPopup(GetShopText("popup_msg_bought"), "", false);
						
						// Do a batch download of packs, for this IAP item
				SB_IAPItem iap = SB_IAPManager.instance.GetIAPItemWithSku(_selectedSku);
				foreach(string packId in iap.packIDs)
				{
					DownloadPack(packId);
				}
					//}
				break;
					
			case SB_OpenIABWrapper.PURCHASE_FAILED:
				PurchaseCancelled();
				LogError("PurchaseCancelledOrFailed");
				break;
					
			case SB_OpenIABWrapper.RESTORE_SUCCESS:
				RestoreSuccess();
				LogSceneEvent("RestoreSuccess");
				break;
					
			case SB_OpenIABWrapper.RESTORE_FAILED:
				RestoreFailed();
				LogSceneEvent("RestoreFailed");
				break;
			}
		}
		
		/*
		 * Item buying
		 */
		protected virtual void BuyItem(SB_IAPItem item)
		{
			if(item == null || item.sku.Length == 0)
			{
				error("BuyItem() item is not valid!! "+item.ToString());
				return;
			}
			
			if(_shopState == STATE_READY)
			{
				string msg = GetShopText("msg_connecting_to_store"); 


				_selectedSku = item.sku;
				_shopState = STATE_PREBUY_RESTORE;
				
				if(item.isFree)
				{
					// Download all free pack items immediately
					foreach(string packId in item.packIDs)
					{
						DownloadPack(packId);
					}
				}
				else if(_openIABWrapper.Inventory != null)
				{		
					ShowInfoMessage(true, msg);
					#if UNITY_IOS

					_openIABWrapper.RestoreTransactions();
					//_infoPopup.SetInfo ("prebuy_restore");
					#else
					
					warn("HasPurchase '" + item.sku + "': " + _openIABWrapper.Inventory.HasPurchase(item.sku));
					warn("HasDetails '" + item.sku + "': " + _openIABWrapper.Inventory.HasDetails(item.sku));
					//warn("GetPurchase '" + item.sku + "': " + _openIABWrapper.Inventory.GetPurchase(item.sku));
					
					if(!_openIABWrapper.Inventory.HasPurchase(item.sku))
					{
						StartPurchase();
					}
					else
					{
						error ("ITEM HAS ALREADY BEEN PURCHASED! " + item.sku);
					}
					
					#endif
				}
				else
				{
					error("_openIABWrapper.Inventory is null!!!!!");
					
					// This message should never actually get shown, unless testing in the IDE (inventory should never be null, if response received from server)
					DisplayError(GetShopText("msg_error_null_inventory"));
					_shopState = STATE_READY;
				}
			}
			else
			{
				error("_shopState not STATE_READY!!! state: "+_shopState);
			}
		}
		
		protected virtual void StartPurchase()
		{
			Dictionary<string, string> trackingParams =	new Dictionary<string, string>() 
			{
				{"SKU", _selectedSku}
			};
			LogSceneEvent("StartPurchase", trackingParams);
			
			_shopState = STATE_BUYING;
			OpenIAB.purchaseProduct(_selectedSku, _devPayload);
			
			ShowInfoMessage(true, GetShopText("popup_msg_buy_loading"));
		}
		
		protected virtual void PurchasedSuccess(string sku)
		{
			Dictionary<string, string> trackingParams =	new Dictionary<string, string>() 
			{
				{"SKU", sku}
			};
			LogSceneEvent("PurchaseSuccess", trackingParams);
			
			_shopState = STATE_READY;
			SB_IAPManager.instance.PurchaseIAP(sku);

			//ShowCarousels ();
		}
		
		protected virtual void PurchaseCancelled()
		{
			Dictionary<string, string> trackingParams =	new Dictionary<string, string>() 
			{
				{"SKU", _selectedSku}
			};
			LogSceneEvent("PurchaseCancelled", trackingParams);
			
			_shopState = STATE_READY;
			
			ShowAlertPopup(GetShopText("popup_msg_cancelled"), "", false);
			ShowInfoMessage(false);
		}
		
		protected virtual void RestorePurchases()
		{
			if(!IsNetConnected())
			{
				_shopState = STATE_NOT_INTERNET_CONNECTED;
				DisplayError(GetShopText("msg_error_no_internet"));
				return;
			}

			#if UNITY_IOS
			// Restore transaction handling - IOS only!!!
			ShowInfoMessage(true, GetShopText("msg_connecting_to_store"));
			_shopState = STATE_RESTORING;
			_openIABWrapper.RestoreTransactions();
			//_infoPopup.SetInfo ("prebuy_restore");
			#else
			// Android does not require connecting to the store, to restore.
			// Just validate the local pack files immediately
			ValidateFiles();
			#endif
		}

		protected virtual void ItemAlreadyBought()
		{

		}
		
		protected virtual void RestoreSuccess()
		{
			if(_shopState == STATE_PREBUY_RESTORE)
			{	
				if(_openIABWrapper.RestoredTransactions)
				{
					Debug.LogError("Prebuy found restored transactions");
					//ShowCarousels ();
					//UpdateMenu();
					SB_IAPItem titem = SB_IAPManager.instance.GetIAPItemWithSku(_selectedSku);
					if(titem.availableInStore && !titem.purchased)
					{
						StartPurchase();
					}
					else
					{
						ItemAlreadyBought();
					}
				}
				else
				{
					Debug.LogError("No Prebuy found restored transactions");
					StartPurchase();
				}
			}
			else if(_shopState == STATE_RESTORING)
			{	
				if(_openIABWrapper.RestoredTransactions)
				{
					Debug.LogError("RESTORE found restored transactions");
					
					ShowAlertPopup(GetShopText("popup_msg_restore_change"), "", false);
					UpdateMenu();
				}
				else
				{
					ShowAlertPopup(GetShopText("popup_msg_restore_nochange"), "", false);
					Debug.LogError("RESTORE found no restored transactions");
				}
				
				// GT, 17.02.2016 - Ensure the shop state is set to READY, after a restore
				_shopState = STATE_READY;
			}
		}

		protected virtual void UpdateMenu()
		{

		}
		
		protected virtual void RestoreFailed()
		{
			string msg = "";
			if(_shopState == STATE_PREBUY_RESTORE)
			{
				msg = GetShopText("popup_msg_error_purchase_not_completed");
			}
			else if(_shopState == STATE_RESTORING)
			{
				msg = GetShopText("popup_msg_restore_problem");
			}
			else
			{
				msg = GetShopText("msg_error_connection");
			}

			_shopState = STATE_READY;//STATE_RESTORE_FAILED;

			ShowAlertPopup(msg, "", false);
			Debug.LogError("RestoreFailed()");
		}
		
		protected virtual void ValidateFiles(bool onlyCallbackIfErrorFound=false)
		{
			// Ignore any files which contain the following:
			string[] ignoreFiles = new string[]{"/character_thumbs/"};	// <- Do not check the 'character_thumbs' for Dr Who
		
			SB_LocalPackManager.instance.ValidateFiles(HandleOnValidateFiles, onlyCallbackIfErrorFound, ignoreFiles);
		}
		
		/**
		 * Called whenever files are validated on the local filesystem.
		 */
		private void HandleOnValidateFiles(SB_LocalPackManagerResponse response)
		{
			warn("\t\t***HandleOnValidateFiles "+response.Status);
			
			// Store any errors returned in the response
			KeyValueObject errors = null;
			string title = "";
			
			switch(response.Status)
			{
			case SB_LocalPackManagerResponse.FILE_VALIDATION_SUCCESS:
				warn("All local pack files successfully validated");
					//_infoPopup.SetInfo ("file_validation_success");
					//_infoPopup.ShowButtons(ShopInfoPopupButtonOptions.Close);
					
				title = GetShopText("popup_msg_file_validation_success");
					//ShowConfirmPopup(txt, false);
				ShowAlertPopup(title, "", false);
				break;
					
			case SB_LocalPackManagerResponse.FILE_VALIDATION_NO_PACKS_FOUND:
					//_shopState = STATE_FILE_VALIDATION_FAILED;
				error("No packs found!");
					//_infoPopup.SetInfo ("error_no_packs_installed");
					//_infoPopup.ShowButtons(ShopInfoPopupButtonOptions.CancelAndOk);
					//ShowInfoMessage(false);
					
				title = GetShopText("popup_msg_error_no_packs_installed");
					//ShowConfirmPopup(txt);
				ShowAlertPopup(title);
				LogError("NoPacksInstalled");
				break;
					
			case SB_LocalPackManagerResponse.FILE_VALIDATION_MISSING_PACK_XML:
				_shopState = STATE_FILE_VALIDATION_FAILED;
				error("Pack xml files were expected, none were found!");
				errors = (KeyValueObject)response.Data;
					//_infoPopup.SetInfo ("error_missing_pack_xml");
					//_infoPopup.ShowButtons(ShopInfoPopupButtonOptions.CancelAndOk);
					
				title = GetShopText("popup_msg_error_missing_pack_xml");
				ShowAlertPopup(title);
				LogError("MissingPackXML");
				break;
					
			case SB_LocalPackManagerResponse.FILE_VALIDATION_FAILED:
				_shopState = STATE_FILE_VALIDATION_FAILED;
				error("File validation failed! Files are missing!");
				errors = (KeyValueObject)response.Data;
					//_infoPopup.SetInfo ("error_file_validation_failed");
					//_infoPopup.ShowButtons(ShopInfoPopupButtonOptions.CancelAndOk);
					
				string str = "";
				foreach(string key in errors.Keys)
				{
					//bool val = (bool)errors.GetValue(key);
					//warn ("\t\t\t key: " + key + ", val: " + val);
					str += key+", ";
				}
					
				title = GetShopText("popup_msg_error_file_validation_failed");
				ShowAlertPopup(title, str);
				LogError("FileValidation");
				break;					
			}
			
			_failedPacks = errors;
		}
		
		/**
		 * Returns Shop text, from config.xml
		 */
		protected string GetShopText(string key)
		{
			string str = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/store/text", key);
			return SB_Utils.SanitizeString(str);
		}
		
		override protected void OnDestroy()
		{	
			_shopXMLLoader.OnXMLLoaded = null;
			_shopXMLLoader = null;
			
			_shopPackDownloader.OnDownloadComplete = null;
			_shopPackDownloader = null;
			
			_openIABWrapper.OnSB_OpenIABWrapperEvent -= HandleOnSB_OpenIABWrapperEvent;
			_openIABWrapper.Dispose();
			_openIABWrapper = null;
			
			_progressBarHolder = null;
			_progressBar = null;
			_shopInfoMessage = null;

			base.OnDestroy();
		}
	}
}