﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Scenes;
using System.Collections.Generic;
using ScaryBeasties.UI;
using ScaryBeasties.Controls;
using UnityEngine.UI;
using ScaryBeasties.SB2D;
using ScaryBeasties.InAppPurchasing.Shop;
using System.IO;
using System.Globalization;
using ScaryBeasties.Data;
using ScaryBeasties.Utils;
using ScaryBeasties.Cam;
using ScaryBeasties.Base;
using ScaryBeasties.Tweening;
using ScaryBeasties.Popups;
using ScaryBeasties.Scenes.Shop;

namespace ScaryBeasties.DrWho.Shop
{
	public class ShopItemDetails : MonoBehaviour 
	{
		//private SB_IAPItem _iapItem;
		
		private Text _productTitle;
		private Text _productDescription;
		private Text _productPrice;
		private Text _cornerText;
		private GameObject _productImage;
		private RectTransform _productContentPanel;
		private ScrollRect _scrollRect;
		private SpriteRenderer _background;
		private SB_Button _buyBtn;

		public SB_IAPItem currentItem;
		private Vector3 _productImageVec;
		
		protected virtual void Start () 
		{
			

			//_backBtn = transform.Find ("back_btn").gameObject.GetComponent<SB_Button> ();
			_buyBtn = transform.Find("buy_btn").gameObject.GetComponent<SB_Button> ();
			_buyBtn.label = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/buttons/button", "shop_iap_buy");

			_productTitle = this.transform.Find("product/title/label").GetComponent<Text>();
			_productDescription = transform.Find("product/content/Canvas/Panel/label").GetComponent<Text>();
			_productImage = transform.Find("product/image/image").gameObject;
			_productImageVec = new Vector3 (_productImage.transform.position.x, _productImage.transform.position.y+1000, _productImage.transform.position.z);
			_productPrice = _buyBtn.transform.Find("Price").GetComponent<Text>();
			_cornerText = transform.Find("corner_text/text").GetComponent<Text>();
			
			_productContentPanel = transform.Find("product/content/Canvas/Panel").GetComponent<RectTransform>();
			_scrollRect = _productContentPanel.GetComponent<ScrollRect>();
			_background = transform.Find("background").GetComponent<SpriteRenderer>();
			
			Debug.LogError ("STARTED ");
			Debug.LogError (_productTitle);
		}


		
		public void SetupProductDetails(SB_IAPItem iapItem) 
		{
			Debug.LogError ("INFO TITLE "+iapItem.infoTitle);
			currentItem = iapItem;
			_productTitle.text = iapItem.infoTitle; //SB_Utils.GetNodeValueFromConfigXML ("config/copy/shop/store/text", "title");
			_productDescription.text = iapItem.infoDescription;
			if(_productDescription.preferredHeight < _productContentPanel.sizeDelta.y) 
			{
				_scrollRect.enabled = false;
			}
			else 
			{
				_productDescription.rectTransform.sizeDelta = new Vector2(_productDescription.rectTransform.sizeDelta.x, _productDescription.preferredHeight);
				_scrollRect.verticalNormalizedPosition = 1;
			}
		
			
			if(iapItem.type == SB_IAPItem.TYPE_COMING_SOON)
			{
				// Item not yet available.  Hide the 'buy' button, show 'coming soon' message
				_buyBtn.gameObject.SetActive(false);
				_cornerText.text = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/text", "item_type_comingsoon");
			}
			else if(iapItem.type == SB_IAPItem.TYPE_FREE)
			{
				// Item is free
				_buyBtn.label = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/buttons/button", "shop_iap_download");
				_productPrice.text = SB_Utils.GetNodeValueFromConfigXML("config/copy/shop/text", "item_type_free");
				_cornerText.gameObject.SetActive(false);
			}
			else
			{
				// Item is available to download, show its price
				_productPrice.text = iapItem.cost;
				_cornerText.gameObject.SetActive(false);
			}
			
			_background.color = SB_Utils.HexToColour(iapItem.infoColourHex);
			Color alphaTextBackgroundColour = _background.color * 0.9f;
			alphaTextBackgroundColour.a = 0.6f;
			transform.Find("product/content/contentBackground").GetComponent<SpriteRenderer>().color = alphaTextBackgroundColour;

			if (_productImage.transform.Find ("Asset")!=null) {
				Destroy(_productImage.transform.Find ("Asset").gameObject);
			}
			_productImage.transform.position = _productImageVec;
			SB2DScaledSprite spr = SB2DSpriteMaker.CreateScaledSprite(_productImage, "Asset", this.OnSpriteLoaded);
			spr.GetComponent<Renderer>().sortingOrder = 30;
			string url = iapItem.detailImage;
			
			string localShopImage = SB_ShopUtils.GetLocalShopDirectoryPath() + "/" + iapItem.detailImage;

			if(File.Exists(localShopImage))
			{
				// Load image from local filesystem
				url = localShopImage;
				spr.LoadReadableImageFromDisk(url);
			}
			else if(SB_IAPManager.instance.ShopConfig != null)
			{
				// Load image from server
				url = SB_Globals.SHOP_ASSETS_PATH + SB_Globals.LOCALE_STRING + "/" + SB_IAPManager.instance.ShopConfig.ShopImagePath + "/" + tk2dSystem.CurrentPlatform + "/" + iapItem.detailImage;
				
				Debug.LogError("LOADING "+url);
				//spr.LoadImage(url);
				spr.LoadReadableImage(url);
			}
		}
		
		private void OnSpriteLoaded(SB2DSprite spr) 
		{
			Debug.LogError("LOADED");
			SB2DScaledSprite scaledSpr = (SB2DScaledSprite)spr;
			
			float newX = _productImage.transform.position.x - (scaledSpr.ScaledWidth / 2);
			float newY = _productImage.transform.position.y + (scaledSpr.ScaledHeight / 2);
			
			_productImage.transform.position = new Vector3(newX, newY, _productImage.transform.position.z);
			SpriteRenderer sprRenderer = spr.transform.GetComponent<SpriteRenderer>();
			
			Color col1 = sprRenderer.color;
			col1.a = 0.0f;
			sprRenderer.color = col1;
			
			SB_Tweener.TweenAlpha (spr.gameObject, 0.4f, 1.0f);
			
			SB_ShopUtils.SaveSpriteToDisk(spr);
		}
		
		protected void OnDestroy()
		{
			_productTitle = null;
			_productDescription = null;
			_productPrice = null;
			_cornerText = null;
			
			_productImage = null;
			_productContentPanel = null;
			_scrollRect = null;
			_background = null;
			_buyBtn = null;
			
			currentItem = null;
			
			SB_Tweener.StopAllTweens();
		}
	}
}
