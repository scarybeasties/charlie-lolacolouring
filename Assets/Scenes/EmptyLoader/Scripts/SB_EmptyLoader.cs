﻿using UnityEngine;
using System.Collections;
using ScaryBeasties;

public class SB_EmptyLoader : MonoBehaviour {

	void Awake(){

		Resources.UnloadUnusedAssets();
		Application.LoadLevel(SB_Globals.nextSceneName);
	}

}
