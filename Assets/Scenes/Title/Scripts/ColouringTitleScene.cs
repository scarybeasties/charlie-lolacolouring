﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Scenes;
using System.Collections.Generic;
using ScaryBeasties.User;
using ScaryBeasties.Sound;
using ScaryBeasties;
using UnityEngine.UI;
using ScaryBeasties.Utils;
using ScaryBeasties.Popups;
using ScaryBeasties.Base;
using ScaryBeasties.Data;
using ScaryBeasties.Games.Colouring.Menus;
using ScaryBeasties.History;
using ScaryBeasties.Controls;
using ScaryBeasties.UI;

public class ColouringTitleScene : SB_TitleScene
{
	private const int MENU_STATE_INSTALLING_DEFAULT_PACK = 0;
	private const int MENU_STATE_READY = 1;
	private int _menuState = -1;

	private float _delayTime = 0.3f;

	private SB_Button _moreGamesBtn;
	private SB_CallToAction _ctaBtn;
	private int popupSelection = -1;

	override protected void Start ()
	{
		_animationStates = new List<string> (new string [] { "Intro", "Ready", "Outro" });

		// User can't go back further than this scene, so empty out the history
		SB_SceneHistory.instance.Reset ();

		base.Start ();

		_moreGamesBtn = transform.Find ("MoreGamesBtn").GetComponent<SB_Button> ();
		_ctaBtn = _moreGamesBtn.gameObject.GetComponent<SB_CallToAction> ();

		// We only have 1 profile in this game
		SB_UserProfileManager.instance.CurrentProfile = SB_UserProfileManager.instance.GetProfileVO (1);
		SB_UserProfileManager.instance.SaveProfileData (SB_UserProfileManager.instance.CurrentProfile);


		bool defaultPackInstalled = ColouringPackManager.instance.CheckDefaultPackInstalled (this);
		if (!defaultPackInstalled) {
			// hide play button until unzipping has finished
			MenuReady (false);
		} else {
			MenuReady (true);
		}

		SB_SoundManager.instance.MuteOverride ();
	}

	override protected void Init ()
	{
		SB_SoundManager.instance.PlayNewMusic ("Audio/Music/MainTheme", 0.75f);
		SB_SoundManager.instance.PlaySoundDelayed ("Localised/VO/Charlie/vo_splash_charlielola", 0.6f);
		SB_SoundManager.instance.PlaySoundDelayed ("Localised/VO/Lola/vo_splash_charlielola", 0.5f);

		Text pleaseWait = transform.Find ("PleaseWait").GetComponent<Text> ();
		pleaseWait.text = SB_Utils.GetNodeValueFromConfigXML ("config/copy/loading/text", "wait");

		string titleText = SB_Utils.GetNodeValueFromConfigXML ("config/copy/title/text", "title");
		transform.Find ("text/title").GetComponent<Text> ().text = titleText;
	}

	void MenuReady (bool val)
	{
		//_playBtn.gameObject.SetActive(val);
		transform.Find ("PleaseWait").gameObject.SetActive (!val);

		if (val) {
			_menuState = MENU_STATE_READY;
		} else {
			_playBtn.gameObject.SetActive (false);
			_menuState = MENU_STATE_INSTALLING_DEFAULT_PACK;
		}
	}

	public void Update ()
	{
		if (_userPausedGame || _popupManager.CurrentPopupObject != null)
			return;


		if (_menuState == MENU_STATE_INSTALLING_DEFAULT_PACK) {
			transform.Find ("PleaseWait").gameObject.SetActive (true);

			_delayTime -= Time.deltaTime;
			if (_delayTime <= 0.0f) {
				if (ColouringPackManager.instance.DefaultPackInstalled ()) {
					_delayTime = 1.0f;
					//SB_SoundManager.instance.PlayNewMusic ("Audio/Music/MusThemeMusic",0.5f);
					MenuReady (true);
				} else {
					_delayTime = 0.5f;
				}
			}
		}

		base.Update ();
	}

	override public void IntroComplete ()
	{
		base.IntroComplete ();

		_ctaBtn.Show ();
		_playBtn.gameObject.SetActive (true);
	}

	override protected void StartLoadingNextScene ()
	{
		base.StartLoadingNextScene ();
		_playBtn.gameObject.SetActive (false);
	}

	override protected void OnBtnHandler (SB_Button btn, string eventType)
	{
		base.OnBtnHandler (btn, eventType);

		if (eventType == SB_Button.MOUSE_UP) {
			switch (btn.name) {
			case "MoreGamesBtn":
				SB_Analytics.instance.SceneEvent(SB_AnalyticEventList.MORE_GAMES_BTN);

				popupSelection = 1;
				ShowPopup (SB_PopupTypes.GATING_POPUP);
				break;

			case SB_UIControls.BTN_PARENTS:
				popupSelection = 2;
				// Parent class opens the gating popup for this button
				break;
			}
		}
	}

	override protected void OnPlayButton ()
	{
		SB_SoundManager.instance.ClearVOQueue ();
		PlayButtonPressSound ();
		//PlayOutroAndLoad(SB_Globals.SCENE_COLOURING_MENU_PACKS); // <-- Load this scene if we have multiple packs to choose from

		// Hey Duggee Colouring app only has the default pack, so we can go straight to the 'drawings' menu, to list its contents
		KeyValueObject kv = new KeyValueObject ();
		kv.SetKeyValue (ColouringMenuBase.MENU_ITEMS_PACK_ID_KEY, "default_pack");
		PlayOutroAndLoad (SB_Globals.SCENE_COLOURING_MENU_DRAWINGS, "Outro", kv);
	}

	/**
	 * Handle events to popups
	 */
	override protected void HandleOnPopupEvent (SB_BasePopup popup, string eventType)
	{
		base.HandleOnPopupEvent (popup, eventType);

		switch (eventType) {
		case SB_GatingPopup.GATING_SUCCESS:
			if (popupSelection == 1) {
				LogSceneEvent ("MoreGamesGatingSuccess");
			} else {
				LogSceneEvent ("GatingSuccess");
			}
			break;
		}
	}

	override protected void LoadParentsArea ()
	{
		if (popupSelection == 1) {
			LogSceneEvent ("LoadingMoreGames");
			LoadScene (SB_Globals.SCENE_WEBVIEW_GENERIC, _ctaBtn.GetKeyValueObject (), true);
		} else {
			LogSceneEvent ("LoadingParentsArea");
			LoadScene (SB_Globals.SCENE_PARENTS_AREA, null, true);
		}
	}

	#region AUDIO

	override protected void PlayIdleVO ()
	{
		PlayAudioTap ();
	}

	public void PlayAudioTap ()
	{
		SB_SoundManager.instance.PlayVoice ("Localised/VO/Charlie/vo_splash_tap");
	}

	public void PlayAudioPop ()
	{
		SB_SoundManager.instance.PlaySound ("Audio/SFX/UI/SndPop", 0.15f);
	}

	public void PlayAudioPop2 ()
	{
		SB_SoundManager.instance.PlaySound ("Audio/SFX/UI/SndPop2", 0.15f);
	}

	public void PlayAudioChime ()
	{
		SB_SoundManager.instance.PlaySound ("Audio/SFX/UI/SndChime", 0.45f);
	}

	#endregion
}
