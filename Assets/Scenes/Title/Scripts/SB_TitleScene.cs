﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ScaryBeasties.Controls;
using ScaryBeasties.Base;
using ScaryBeasties.History;
using ScaryBeasties.Popups;
using ScaryBeasties.UI;
using ScaryBeasties.Utils;
using ScaryBeasties.Sound;

namespace ScaryBeasties.Scenes
{
	public class SB_TitleScene : SB_BaseScene 
	{
		float _idleCounter = 0.0f;
		float _idleMax = 10.0f;

		protected SB_Button _playBtn;

		override protected void Start()
		{
			resetIdle();
			//SB_SoundManager.instance.PlayNewMusic("Audio/Music/MusThemeMusic");
			// User can't go back further than this scene, so empty out the history
			SB_SceneHistory.instance.Reset();

			base.Start ();

			_uiControls.ShowButtons(new List<string>(new string[] {SB_UIControls.BTN_SOUND,
																	SB_UIControls.BTN_PARENTS}));

			_playBtn = transform.Find("btn_play").GetComponent<SB_Button>();
			_playBtn.label = SB_Utils.GetNodeValueFromConfigXML ("config/buttons/button", "play");



		}
		
		override protected void OnBtnHandler(SB_Button btn, string eventType)
		{
			base.OnBtnHandler(btn, eventType);

			resetIdle ();
			if(eventType == SB_Button.MOUSE_UP)
			{
				switch(btn.name)
				{
					case "btn_play":
						OnPlayButton();
						break;
						
					case SB_UIControls.BTN_PARENTS:
						OnParentsButton();
						break;
				}
			}
		}
		
		protected virtual void OnPlayButton()
		{
			//SB_SceneHistory.instance.AddScene(Application.loadedLevelName);
			SB_SoundManager.instance.ClearVOQueue();
			PlayOutroAndLoad(SB_Globals.SCENE_PROFILE_SELECTION);
		}
		
		protected virtual void OnParentsButton()
		{
			PlayButtonPressSound();
			ShowPopup(SB_PopupTypes.GATING_POPUP);
		}

		/**
		 * Handle events to popups
		 */
		override protected void HandleOnPopupEvent (SB_BasePopup popup, string eventType)
		{
			base.HandleOnPopupEvent(popup, eventType);

			switch(eventType)
			{
				case SB_GatingPopup.GATING_SUCCESS:
					LoadParentsArea();
					break;
			}	
		}
		
		protected virtual void LoadParentsArea()
		{
			LogSceneEvent("Parents");
			LoadScene(SB_Globals.SCENE_PARENTS_AREA, null, true);
		}
		
		private void resetIdle ()
		{
			_idleCounter = _idleMax;
		}

		protected virtual void PlayIdleVO ()
		{
			// to be overridden
		}

		override protected void UpdateReady ()
		{
			_idleCounter -= Time.deltaTime;
			if (_idleCounter <= 0) {
				resetIdle ();
				PlayIdleVO ();
			}

			base.UpdateReady ();
		}
		
		public override void IntroComplete ()
		{
			base.IntroComplete ();
		}

		override protected void StartLoadingNextScene ()
		{
			transform.Find ("Loader").gameObject.SetActive (true);
			base.StartLoadingNextScene ();
		}
		
		override protected void OnDestroy ()
		{
			_playBtn = null;
			Destroy (transform.Find ("Loader").gameObject);
			base.OnDestroy ();
		}
	}
}