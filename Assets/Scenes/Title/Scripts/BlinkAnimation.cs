﻿using UnityEngine;
using System.Collections;
using ScaryBeasties.Games.Common.Sprites;


namespace ScaryBeasties.Games.Common
{
	public class BlinkAnimation : AnimatedSprite
	{
		
		public const string STATE_IDLE = "stateIdle";
		public const string STATE_BLINK = "stateBlink";
		
		public float MinIdleTimeout = 1440;
		public float MaxIdleTimeout = 2880;
		
		protected float _timeoutMax = 2880;
		protected float _timeoutCounter = 0;
		
		public string animName = "";
		
		private string _animIdle;
		private string _animBlink;
		
		
		override protected void Start ()
		{
			//_debugEnabled = false;
			_timeoutMax = MinIdleTimeout + (int)Random.Range (0, MaxIdleTimeout - MinIdleTimeout);
			
			_animIdle = animName + "/Idle";
			_animBlink = animName + "/Blink";
			base.Start ();
		}
		
		public void SetAnimName (string name)
		{
			animName = name;
			_animIdle = animName + "/Idle";
			_animBlink = animName + "/Blink";
		}

		override protected void UpdateIdle ()
		{
			_timeoutCounter += (Time.deltaTime * 1000);
			if (_timeoutCounter > _timeoutMax) {
				_timeoutCounter = 0;
				SelectRandomState ();
			}
		}
		
		/** Subclass will contain the logic for picking the next random state */
		protected virtual void SelectRandomState ()
		{
			// Create a new max timeout value
			_timeoutMax = MinIdleTimeout + (int)Random.Range (0, MaxIdleTimeout - MinIdleTimeout);

			SetState (STATE_BLINK);
			
		}
		
		
		
		override public void SetState (string state)
		{
			if (state == STATE_IDLE)
				_spriteAnimator.Play (_animIdle);
			else if (state == STATE_BLINK)
				_spriteAnimator.Play (_animBlink);
			
			base.SetState (state);
		}
	}
}

