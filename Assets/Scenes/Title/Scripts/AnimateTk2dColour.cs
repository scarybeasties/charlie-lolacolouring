﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AnimateTk2dColour : MonoBehaviour
{

	public Color _colour;
	private tk2dSprite _sprite;

	// Use this for initialization
	void Start ()
	{
		_sprite = GetComponent<tk2dSprite> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (_sprite.color != _colour) {
			_sprite.color = _colour;
		}
	}
}
